      subroutine rd_cydata(da,de,drms,nct,dh,dhe,ngp,ca,cad,cv)
*     copy data around arrays
      implicit none
      include 'vp_sizes.f'
      integer nct,ngp
      character*(*) cv(*)
      double precision da(*),de(*),drms(*),dh(*),dhe(*)
      double precision ca(*),cad(*)
*     Local:
      character*1 ic1,ic2,ic3
      integer i,iii,j,nnn
      double precision temp
*     functions:
      double precision wval
*     
      character*4 chwts
      integer nbr
      double precision ac,bc,ach,achend
      common/rdc_scwv/ac,bc,ach,achend,nbr,chwts
      integer nwco
      double precision wcf1(maxwco)
      common/vpc_wavl/wcf1,nwco
      character*8 wcftype
      character*4 vacind
      double precision helcfac
      integer noffs
      common/c8_wcftype/wcftype,helcfac,noffs,vacind
*     wavelength parameter sets
      character*8 wcfty2(maxnch)
      character*4 vacin2(maxnch)
      double precision helcf2(maxnch)
      integer nwcf2(maxnch),noffs2(maxnch)
      common/vpc_wcpars2/wcfty2,helcf2,nwcf2,noffs2,vacin2
*     multidimensional wavelength coefficients...
      double precision wcfd(maxwco,maxnch)
      common/vpc_wcfd/wcfd
*     scrunch style flag, isclog=1 for log bins
      integer isctype,isclog
      common/rdc_scflag/isctype,isclog
*
c
      write(6,*) ' Copy data array ',cv(2)
      if(cv(2)(1:1).ne.' ') then
        ic1=cv(2)(1:1)
        ic2=cv(2)(2:2)
        ic3=cv(2)(3:3)
        goto 501
      end if
c     present list of options
 1    write(6,*) ' Enter two letter code in order (from)(to) '
      write(6,*) ' continuum=c, scrunched (summed) array=s,'
     :    //' data array=d (=workspace=w)'
      write(6,*) ' e.g. cd for cont. to data (help for more info.)'
      ic3=' '
c     third character for operation sw- & sw+ only supported 
c	    combinations
      read(5,'(3a1)',end=98) ic1,ic2,ic3
 501  if(ic1.eq.ic2) goto 99
c     check letter combinations
*     C->
      if(ic1.eq.'c'.or.ic1.eq.'C') then
c       copy from continuum
        if(ic2.eq.'w'.or.ic2.eq.'W'.or.ic2.eq.'d'.or.ic2.eq.'D') then
          write(6,*) ' Continuum to workspace'
          do i=1,nct
            da(i)=ca(i)
          end do
          if(ic3.ne.'n'.and.ic3.ne.'N') then
            write(6,*) ' error set to zero'
            do i=1,nct
              de(i)=0.0d0
            end do
          end if
          goto 98
        end if
*       continuum to error
        if(ic2.eq.'e'.or.ic2.eq.'E') then
          write(6,*) ' Continuum to error'
          do i=1,nct
            if(ca(i).ge.0.0d0) then
              if(ca(i).lt.1.0d18.and.ca(i).gt.1.0d-18) then
                de(i)=ca(i)*ca(i)
               else
                de(i)=0.0d0
              end if
             else
              if(ic3.eq.'z'.or.ic3.eq.'Z') then
                de(i)=0.0d0
               else
                de(i)=-1.0d0
              end if
            end if
          end do
          goto 98
        end if
*     
        if(ic2.ne.'s'.and.ic2.ne.'S') goto 99
*       continuum to scrunched
        write(6,*) ' Continuum to scrunched array'
        do i=1,nct
          dh(i)=ca(i)
          dhe(i)=0.0d0
        end do
        write(6,*) ' error term set to zero'
        ach=wcf1(1)
        bc=wcf1(2)
        if(wcftype(1:3).eq.'log') then
          isclog=1
         else
          isclog=0
        end if
        nwco=2
        ngp=nct
        goto 98
      end if
*
*     W or D ->
      if(ic1.eq.'w'.or.ic1.eq.'W'.or.ic1.eq.'d'.or.ic1.eq.'D') goto 3
*
*     E->:
c     special function - workspace error to workspace
      if(ic1.eq.'e'.or.ic1.eq.'E') then
        if(ic2.eq.'w'.or.ic2.eq.'W'.or.ic2.eq.'d'.or.ic2.eq.'D') then
          do i=1,nct
            if(de(i).ge.0.0d0) then
              da(i)=sqrt(de(i))
             else
              da(i)=-1.0d0
            end if
          end do
          goto 98
        end if
        if(ic2.eq.'c'.or.ic2.eq.'C') then
          do i=1,nct
            if(de(i).ge.0.0d0) then
              ca(i)=sqrt(de(i))
             else
              ca(i)=-1.0d0
            end if
          end do
          goto 98
        end if
        if(ic2.eq.'r'.or.ic2.eq.'R') then
          do i=1,nct
            drms(i)=de(i)
          end do
          write(6,*) 'error -> RMS array'
          goto 98
        end if
        goto 99
      end if
*
*     RMS -> error
      if(ic1.eq.'r'.or.ic1.eq.'R') then
*       copy rms to error and vice versa
        if(ic2.eq.'e'.or.ic2.eq.'E') then
          if(ic3.eq.'s'.or.ic3.eq.'S') then
*           swap rms & error arrays
            do i=1,nct
              temp=de(i)
              de(i)=drms(i)
              drms(i)=temp
            end do
            write(6,*) 'Swapped error and RMS arrays'
            goto 98
           else
*           copy rms to error
            do i=1,nct
              de(i)=drms(i)
            end do
            write(6,*) 'RMS -> error array'
            goto 98
          end if
        end if
        goto 99
      end if

*
*     HELP
      if(ic1.eq.'h'.or.ic1.eq.'H'.or.ic1.eq.'?') then
c       help information
        write(6,*) ' additional commands and features:'
        write(6,*) 
     :  ' ew (or ed) is error to work, we (de) work to error'
        write(6,*) ' er error->rms'
        write(6,*) ' sw- or sw+ scrunch (-,+) work to work'
        write(6,*) ' swe  scrunch + workspace data to scrunch error'
        write(6,*) ' a third character is checked -- swn & wsn copy'
        write(6,*) '    data but not errors;  lw wave->work'
        write(6,*) ' fe cont->error AND 1->cont; ec err->cont'
        write(6,*) ' re rms->error; res swap rms<->error'
        goto 1
      end if
*
*     F: C->E and 1->C
      if(ic1.eq.'f'.or.ic1.eq.'F') then
        if(ic2.eq.'e'.or.ic2.eq.'E') then
*         continuum to error, continuum becomes unity
          do i=1,nct
            de(i)=ca(i)*ca(i)
            ca(i)=1.0d0
          end do
          goto 98
         else
          goto 99
        end if
      end if
*
*     L: lw or ld is wavelengths to workspace!
      if(ic1.eq.'l'.or.ic1.eq.'L') then
        if(ic2.eq.'w'.or.ic2.eq.'W'.or.ic2.eq.'d'.or.ic2.eq.'D') then
*         workspace becomes a wavelength table
          write(6,*) 'Workspace is now a wavelength table'
          do i=1,nct
            da(i)=wval(dble(i))
          end do
          goto 98
         else
          goto 99
        end if
      end if
*
*     S->:
      if(ic1.ne.'s'.and.ic1.ne.'S') goto 99
c     scrunched data copied if reaches here
      if(ic2.eq.'c'.or.ic2.eq.'C') goto 4
      if(ic2.ne.'w'.and.ic2.ne.'W'.and.ic2.ne.'d'.and.
     :             ic2.ne.'D') goto 99
c     add?
      if(ic3.eq.'+') then
        write(6,'('' Scrunched + work to workspace'')')
        do i=1,nct
          da(i)=da(i)+dh(i)
        end do
        goto 98
      end if
c     check if want to subtract
      if(ic3.eq.'-') then
        write(6,*) ' scrunched minus work to workspace'
        do i=1,nct
          da(i)=dh(i)-da(i)
          if(ic3.ne.'n') then
            if(dhe(i).lt.0.0d0.or.de(i).lt.0.0d0) then
              de(i)=-1.0
             else
              de(i)=de(i)+dhe(i)
            end if
          end if
        end do
        goto 98
      end if
c     scrunch plus workspace to scrunch error term?
      if(ic3.eq.'e'.or.ic3.eq.'E') then
        write(6,*) ' Scunched + workspace data to scrunched error'
        do iii=1,nct
          dhe(iii)=dh(iii)+da(iii)
        end do
        goto 98
      end if
c     scrunched to workspace (also the default)
      write(6,*) ' Scrunched array to workspace'
      noffs=0
*     determine scrunched data length
      nnn=ngp
      do while (dh(nnn).eq.0.0.and.dhe(nnn).le.0.0
     :            .and.nnn.gt.20)
        nnn=nnn-1
      end do
      if(nct.ne.nnn) then
        nct=nnn
        write(6,'('' Workspace array length now:'',i7)') nct
      end if
      do i=1,nct
        da(i)=dh(i)
      end do
      if(ic3.ne.'n') then
        do i=1,nct
          de(i)=dhe(i)
        end do
       else
        write(6,*) ' error not copied'
      end if
c     copy wavelength coeffts
      wcf1(1)=ach
      wcf1(2)=bc
      wcfd(1,1)=ach
      wcfd(2,1)=bc
*     wavelength type (log or otherwise)
      wcftype='poly'
      if(chwts(1:3).eq.'log') then
        wcftype='log '
       else
        wcftype(1:4)='poly'
      end if
      nwco=2
      if(maxwco.ge.3) then
        do j=3,maxwco
          wcf1(j)=0.0d0
          wcfd(j,1)=0.0d0
        end do
      end if
*     remove other correction factors
      helcfac=1.0d0
      vacind(1:3)='vac'
*     general multi coeffts
      wcfty2(1)=wcftype
      helcf2(1)=helcfac
      nwcf2(1)=nwco
      noffs2(1)=0
      vacin2(1)=vacind
      goto 98
c     scrunched to continuum
 4    write(6,*) ' Scrunched array to continuum'
      do 20 i=1,nct
        ca(i)=dh(i)
        cad(i)=dh(i)
 20   continue
*     just copying to continuum, so don't change wavelengths
      goto 98
*
*     very old section
c     copy from workspace
 3    if(ic2.eq.'s'.or.ic2.eq.'S') goto 6
      if(ic2.eq.'l'.or.ic2.eq.'L') then
*       data to wavelength table!
        write(6,*) 'over-writing wavelengths with data!'
        nwco=nct
        do i=1,nct
          wcf1(i)=da(i)
        end do
        goto 98
      end if
      if((ic2.eq.'e'.or.ic2.eq.'E').and.
     :               (ic3.ne.'n'.and.ic3.ne.'N')) then
c       workspace data to workspace errors
        write(6,*) ' workspace data to errors'
        do i=1,nct
          if(da(i).ge.0.0d0) then
            if(da(i).lt.1.0d18.and.da(i).gt.1.0d-18) then
              de(i)=da(i)*da(i)
             else
              de(i)=0.0d0
            end if
           else
            de(i)=-1.0d0
          end if
        end do
        goto 98
      end if
      if(ic2.ne.'c'.and.ic2.ne.'C') goto 99
      if(ic3.eq.'-') then
*       w-c->w
        write(6,*) 'w-c -> w'
        do i=1,nct
          da(i)=da(i)-ca(i)
        end do
        goto 98
      end if
      write(6,*) ' Workspace to continuum'
      do i=1,nct
        ca(i)=da(i)
        cad(i)=da(i)
      end do
      goto 98
c
 6    write(6,*) ' Workspace to scrunched array'
      do i=1,nct
        dh(i)=da(i)
      end do
      if(ic3.ne.'n') then
        do i=1,nct
          dhe(i)=de(i)
        end do
       else
        write(6,*) 'error array not copied'
      end if
*     just copy the first two wavelength coefficients
      ach=wcf1(1)*helcfac
      bc=wcf1(2)*helcfac
      if(nwco.gt.2) then
        write(6,*) 'WARNING: FIRST TWO WAVEL. COEFFTS ONLY'
      end if
      nwco=2
      if(maxwco.ge.3) then
        do j=3,maxwco
          wcf1(j)=0.0d0
        end do
      end if
*     set appropriate wavelength type
      chwts=wcftype(1:4)
*     and reset flag if needed
      if(wcftype(1:3).eq.'log') then
        isclog=1
       else
        isclog=0
      end if
*     wcftype(1:4)='poly'
*     remove other correction factors
      if(vacind(1:3).eq.'air') then
        write(6,*) 'WARNING: TREATING WAVELENGTHS AS VACUUM'
      end if
      helcfac=1.0d0
      vacind(1:3)='vac'
      ngp=nct
      goto 98
c     abnormal end
 99   write(6,*) ' Bad character'
 98   return
      end
