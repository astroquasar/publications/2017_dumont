      subroutine vp_archwav(wvd,ced,warry,nw)
*     given a wavelength wvd, with initial guessed channel number ced,
*     convert to channel number ced by
*     searching the wavelength array, which is assumed to be in
*     INCREASING order.
*     RFC 28.05.03 Revised to speed up grid search
      double precision wvd,ced
      double precision warry(nw)
      integer nw
*
      integer j,k,kold,m
      double precision xdisp
*     
      k=int(ced)
      if(k.le.0) k=1
      if(k.gt.nw) k=nw
*
*     Assuming that the dispersion is roughly linear, guess at a better
*     starting point
      kold=k
      if(k.le.5) then
        j=1
       else
        j=k-5
      end if
      if(k+5.gt.nw) then
        m=nw
       else
        m=k+5
      end if
      xdisp=0.1d0*(warry(m)-warry(j))
      if(xdisp.gt.0.0d0) then
        k=k+int((wvd-warry(k))/xdisp)
        k=max(1,k)
        k=min(k,nw)
      end if
      if((k-kold).ne.0) then
        xdisp=(warry(k)-warry(kold))/dble(k-kold)
        if(xdisp.gt.0.0d0) then
          k=k+int((wvd-warry(k))/xdisp)
          k=max(1,k)
          k=min(k,nw)
        end if
      end if
*
*     Simple grid search
      kinc=1
      if(wvd.gt.warry(k)) then
*       search upwards from the start point
        j=k
        do while (wvd.gt.warry(k).and.k.lt.nw)
          j=k
          k=min(k+kinc,nw)
          kinc=kinc*2
        end do
       else
*       wvd.le.warry(k), so need to search down to find j
*       such that wavelength .le. warry(j)
        j=k
        do while(wvd.le.warry(j).and.j.gt.1)
          k=j
          j=max(j-kinc,1)
          kinc=kinc*2
        end do
      end if
*     j,k now bracket solution, search for actual value now
*     k is now an element above (or equal to) wavelength, 
*     or end of range, and j is below wavelength.
      do while(k-j.gt.1)
*       need to refine search
        ltem=(j+k)/2
        if(wvd.gt.warry(ltem)) then
          j=ltem
         else
          k=ltem
        end if
      end do
      if(k.lt.2) then
        k=2
        j=1
      end if
      if(k.ge.nw) then
        k=nw
        j=nw-1
      end if
      if(j.eq.k) j=k-1
*
*     Should now just bracket wavelength, or be at end of range.
*     In either case, linearly interpolate (or extrapolate).
      ced=(warry(k)*dble(j)-warry(j)*dble(k)+wvd*dble(k-j))/
     :        (warry(k)-warry(j))
*
      return
      end
