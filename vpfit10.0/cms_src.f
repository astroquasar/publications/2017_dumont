c This file contains two algorithms for the logarithm of the gamma function.
c Algorithm AS 245 is the faster (but longer) and gives an accuracy of about
c 10-12 significant decimal digits except for small regions around X = 1 and
c X = 2, where the function goes to zero.
c The second algorithm is not part of the AS algorithms.   It is slower but
c gives 14 or more significant decimal digits accuracy, except around X = 1
c and X = 2.   The Lanczos series from which this algorithm is derived is
c interesting in that it is a convergent series approximation for the gamma
c function, whereas the familiar series due to De Moivre (and usually wrongly
c called Stirling's approximation) is only an asymptotic approximation, as
c is the true and preferable approximation due to Stirling.
c
c
c
      DOUBLE PRECISION FUNCTION as_alngam(XVALUE, IFAULT)
C
C     ALGORITHM AS245  APPL. STATIST. (1989) VOL. 38, NO. 2
C
C     Calculation of the logarithm of the gamma function
C
      IMPLICIT NONE
      INTEGER IFAULT
      DOUBLE PRECISION ALR2PI, FOUR, HALF, ONE, ONEP5, R1(9), R2(9),
     +		R3(9), R4(5), TWELVE, X, X1, X2, XLGE, XLGST, XVALUE,
     +		Y, ZERO
C
C     Coefficients of rational functions
C
      DATA R1/-2.66685 51149 5D0, -2.44387 53423 7D1,
     +        -2.19698 95892 8D1,  1.11667 54126 2D1,
     +	       3.13060 54762 3D0,  6.07771 38777 1D-1,
     +	       1.19400 90572 1D1,  3.14690 11574 9D1,
     +	       1.52346 87407 0D1/
      DATA R2/-7.83359 29944 9D1, -1.42046 29668 8D2,
     +         1.37519 41641 6D2,  7.86994 92415 4D1,
     +         4.16438 92222 8D0,  4.70668 76606 0D1,
     +         3.13399 21589 4D2,  2.63505 07472 1D2,
     +         4.33400 02251 4D1/
      DATA R3/-2.12159 57232 3D5,  2.30661 51061 6D5,
     +         2.74647 64470 5D4, -4.02621 11997 5D4,
     +        -2.29660 72978 0D3, -1.16328 49500 4D5,
     +        -1.46025 93751 1D5, -2.42357 40962 9D4,
     +        -5.70691 00932 4D2/
      DATA R4/ 2.79195 31791 8525D-1, 4.91731 76105 05968D-1,
     +         6.92910 59929 1889D-2, 3.35034 38150 22304D0,
     +         6.01245 92597 64103D0/
C
C     Fixed constants
C
      DATA ALR2PI/9.18938 53320 4673D-1/, FOUR/4.D0/, HALF/0.5D0/,
     +     ONE/1.D0/, ONEP5/1.5D0/, TWELVE/12.D0/, ZERO/0.D0/
C
C     Machine-dependent constants.
C     A table of values is given at the top of page 399 of the paper.
C     These values are for the IEEE double-precision format for which
C     B = 2, t = 53 and U = 1023 in the notation of the paper.
C
      DATA XLGE/5.10D6/, XLGST/1.D+305/
C
      X = XVALUE
      AS_ALNGAM = ZERO
C
C     Test for valid function argument
C
      IFAULT = 2
      IF (X .GE. XLGST) RETURN
      IFAULT = 1
      IF (X .LE. ZERO) RETURN
      IFAULT = 0
C
C     Calculation for 0 < X < 0.5 and 0.5 <= X < 1.5 combined
C
      IF (X .LT. ONEP5) THEN
	IF (X .LT. HALF) THEN
	  AS_ALNGAM = -LOG(X)
	  Y = X + ONE
C
C     Test whether X < machine epsilon
C
	  IF (Y .EQ. ONE) RETURN
	ELSE
	  AS_ALNGAM = ZERO
	  Y = X
	  X = (X - HALF) - HALF
	END IF
	AS_ALNGAM = AS_ALNGAM + X * ((((R1(5)*Y + R1(4))*Y + R1(3))*Y
     +                + R1(2))*Y + R1(1)) / ((((Y + R1(9))*Y + R1(8))*Y
     +                + R1(7))*Y + R1(6))
	RETURN
      END IF
C
C     Calculation for 1.5 <= X < 4.0
C
      IF (X .LT. FOUR) THEN
	Y = (X - ONE) - ONE
	AS_ALNGAM = Y * ((((R2(5)*X + R2(4))*X + R2(3))*X + R2(2))*X
     +              + R2(1)) / ((((X + R2(9))*X + R2(8))*X + R2(7))*X
     +              + R2(6))
	RETURN
      END IF
C
C     Calculation for 4.0 <= X < 12.0
C
      IF (X .LT. TWELVE) THEN
	AS_ALNGAM = ((((R3(5)*X+R3(4))*X+R3(3))*X+R3(2))*X+R3(1)) /
     +            ((((X + R3(9))*X + R3(8))*X + R3(7))*X + R3(6))
	RETURN
      END IF
C
C     Calculation for X >= 12.0
C
      Y = LOG(X)
      AS_ALNGAM = X * (Y - ONE) - HALF * Y + ALR2PI
      IF (X .GT. XLGE) RETURN
      X1 = ONE / X
      X2 = X1 * X1
      AS_ALNGAM = AS_ALNGAM + X1*((R4(3)*X2 + R4(2))*X2 + R4(1))/
     +              ((X2 + R4(5))*X2 + R4(4))
      RETURN
      END
c
c
c
c
	double precision function as_lngamma(z, ier)
c
c       Uses Lanczos-type approximation to ln(gamma) for z > 0.
c       Reference:
c            Lanczos, C. 'A precision approximation of the gamma
c                    function', J. SIAM Numer. Anal., B, 1, 86-96, 1964.
c       Accuracy: About 14 significant digits except for small regions
c                 in the vicinity of 1 and 2.
c
c       Programmer: Alan Miller
c                   CSIRO Division of Mathematics & Statistics
c
c	N.B. It is assumed that the Fortran compiler supports long
c	     variable names, including the underline character.   Some
c	     compilers will not accept the 'implicit none' statement
c	     below.
c
c       Latest revision - 17 April 1988
c
	implicit none
	double precision a(9), z, lnsqrt2pi, tmp
	integer ier, j
	data a/0.9999999999995183d0, 676.5203681218835d0,
     +         -1259.139216722289d0, 771.3234287757674d0,
     +         -176.6150291498386d0, 12.50734324009056d0,
     +         -0.1385710331296526d0, 0.9934937113930748d-05,
     +         0.1659470187408462d-06/
c
	data lnsqrt2pi/0.91893 85332 04672 7d0/
c
	as_lngamma = 0.d0
	if (z .le. 0.d0) then
	  ier = 1
	  return
	end if
	ier = 0
c
	tmp = z + 7.d0
	do 10 j = 9, 2, -1
	  as_lngamma = as_lngamma + a(j)/tmp
	  tmp = tmp - 1.d0
   10   continue
	as_lngamma = as_lngamma + a(1)
	as_lngamma = log(as_lngamma) + lnsqrt2pi - (z+6.5d0) +
     +                               (z-0.5d0)*log(z+6.5d0)
	end
c
c
c
c     UKC Netlib distribution copyright 1990 RSS
c
      double precision function as_chisqn(x, df, fl, ifault)
c
c<<<<<  Acquired in machine-readable form from 'Applied Statistics'
c<<<<<  algorithms editor, January 1983.
c
c
c        Algorithm AS 170  Appl. Statist. (1981) vol.30, no.3
c
c        The non-central chi-squared distribution.
c
c     Auxiliary routines required: AS_GAMMDS = AS147, ALOGAM = CACM 291.
c     See AS245 for an alternative to ALOGAM.
c
	integer ifault
	double precision x, df, fl
	double precision as_gammds, as_chi
	double precision df2, x2, fxp
c
	as_chisqn = 0.0d0
	ifault = 0
c
c        test for admissibility of arguments
c
	if (df.le.0.0d0) ifault = 1
	if (x.lt.0.0d0) ifault = 2
	if (fl.lt.0.0d0) ifault = 3
	if (ifault.gt.0.or.x.eq.0.0d0) return
c
	df2 = 0.5d0*df
	x2 = 0.5d0*x
	fxp = as_gammds(x2,df2,ifault)
	as_chisqn = as_chi(x2,df2,fl,fxp)
	return
	end
c
      subroutine as_chisql(x, df, fx, fl, ifault)
c
c        Algorithm AS 170.1  Appl.Statist. (1981) vol.30, no.3
c
c        Define accuracy and initialize
c
c        n should be specified such that acc is greater than
c        or equal to (au-al)/2**n
c
	double precision x,df,fx,fl
	double precision as_gammds,as_chi
	double precision acc,al,ainc,au,df2,x2,fx1,aprox
	parameter (acc = 1.0d-6, n = 30)
c
	al = 0.0d0
	ainc = 80.0d0
	au = 80.0d0
c
	ifault = 0
c
c        test for admissibility of arguments
c
	if (df.le.0.0d0) ifault = 1
	if (x.lt.0.0d0) ifault = 2
	if (fx.le.0.0d0) ifault = 3
	if (ifault.gt.0) go to 4
c
	df2 = 0.5d0*df
	x2 = 0.5d0*x
	fx1 = as_gammds(x2,df2,ifault)
1	aprox = as_chi(x2,df2,au,fx1)
	if (fx.gt.aprox) goto 2
	if (fx.lt.aprox) al = au
	au = au+ainc
	go to 1
2	do j = 1,n
          fl = 0.5d0*(al+au)
          aprox = as_chi(x2, df2, fl, fx1)
          if (dabs(fx-aprox).lt.acc) go to 4
          if (fx.lt.aprox) al = fl
          if (fx.ge.aprox) au = fl
	end do
4	return
	end
c
      double precision function as_chi(x, df, fl, fxc)
c
c        Algorithm as 170.2  Appl. Statist. (1981) vol.30, no.3
c
	double precision as_gammds
	double precision x, df, fl, fxc
	double precision df1,fl2,c,t,term,acc2
	parameter (acc2 = 1.0d-8)
c
	ifault=0
	as_chi = fxc
	df1 = df
	fl2 = 0.5d0*fl
	c = 1.0d0
	t = 0.0d0
1	t = t+1.0d0
	c = c*fl2/t
	df1 = df1+1.0d0
	term = c*as_gammds(x, df1, ifault)
	as_chi = as_chi+term
	if (term.ge.acc2) go to 1
	as_chi = as_chi*dexp(-fl2)
	return
	end
c
      double precision function as_gammds (y,p,ifault)
c
c        Algorithm AS 147  Appl. Statist. (1980) Vol. 29, No. 1
c
c        Computes the incomplete gamma integral for positive
c        parameters y,p using an infinite series
c
c        Auxiliary function required: ALNGAM = CACM algorithm 291
c
c	 AS239 should be considered as an alternative to AS147
c
	implicit none
	integer ifault
	double precision y,p
	double precision zero,one,e,arg,uflo,f,a,c
*	function declarations
	double precision as_alngam
	data e/1.0d-9/, zero/0.0d0/, one/1.0d0/, uflo/1.0d-37/
c
c        Checks admissibility of arguments and value of f
c
      ifault = 1
      as_gammds = zero
      if(y.le.zero .or. p.le.zero) return
      ifault = 2
c
c        alngam is natural log of gamma function
c
      arg = p*log(y)-as_alngam(p+one,ifault)-y
      if(arg.lt.log(uflo)) return
      f = exp(arg)
      if(f.eq.zero) return
      ifault = 0
c
c          Series begins
c
      c = one
      as_gammds = one
      a = p
    1 a = a+one
      c = c*y/a
      as_gammds = as_gammds+c
      if (c/as_gammds.gt.e) goto 1
      as_gammds = as_gammds*f
      return
      end
      double precision function pd_pchi2(x,n,ifail)
	implicit none
	integer n, ifail
	double precision x
*
*	x	value of chi^2
*	n	degrees of freedom
*	ifail	must be zero on entry, non-zero on exit if fails
*
*	pd_pchi2 is probability of chi^2.ge.x for n degrees of freedom
*	         The routine uses ONLY public domain software.
*	ACCURACY: OK to answers about 1E-8, after which the probability
*	is overestimated. If you care at that level, use something else.
*	This replaces NAG G01BCF for shared software use.
*
	double precision as_gammds
	double precision sdf,sx,c2
	sdf=0.5d0*dble(n)
	sx=0.5d0*x
	c2=as_gammds(sx,sdf,ifail)
	if(c2.eq.0.0d0.and.sx.gt.sdf) then
*	  routine failed because series inappropriate
	  sx=0.0d0
	 else
	  sx=1.0d0-c2
	end if
*	roundoff error check
	if(sx.lt.0.0d0) sx=0.0d0
*
	pd_pchi2=sx
	end
	double precision function pd_cnorm(x)
*	cumulative normal distribution
	double precision z,x,t
	z=dabs(x)/1.4142135623731d0      
	t=1.0d0/(1.0d0+0.50d0*z)
	t=t*dexp(-z*z-1.26551223d0+t*(1.00002368d0+t*(.37409196d0+
     * t*(.09678418d0+t*(-.18628806d0+t*(.27886807d0+t*(-1.13520398d0+
     * t*(1.48851587d0+t*(-.82215223d0+t*.17087277d0)))))))))/2.0d0
        if (x.gt.0.0d0) t=1.0d0-t
	pd_cnorm=t
	return
	end
	subroutine pd_srank(nn,da,irank,irank0,chx)
*	Rank sort for data da returned in irank
*	Knuth v3 pp165-166
*
*	this replaces NAG routine M01DAF, using ascending order
*	on da(1) - da(nn). Method is essentially the same.
*
*	if chx = 'i(ndex)' or 'I(NDEX)' the index array is returned
*	instead, and ranks are not calculated.
*
*	IN:
*	nn	integer	length of data array
*	da	real	array of values
*	chx	char	control character - 'i' or 'I' for index array
*	OUT:
*	irank	integer	array of ranks or indices
*	irank0	integer	index for lowest value
*	
	character*(*) chx
	real da(nn)
	integer irank(nn)
	integer pd_ipm
*
*	prepare list
	irank0=1
	irankx=2
*	nxx=nn+1
	do i=1,nn-2
	  irank(i)=-(i+2)
	end do
	irank(nn-1)=0
	irank(nn)=0
*
*	L2: new pass
2	continue
	ns=0
	nt=nn+1
	if(ns.gt.0.and.ns.le.nn) then
	  np=irank(ns)
	 else
	  if(ns.eq.0) then
	    np=irank0
	   else
	    np=irankx
	  end if
	end if
	if(nt.gt.0.and.nt.le.nn) then
	  nq=irank(nt)
	 else
	  if(nt.eq.0) then
	    nq=irank0
	   else
	    nq=irankx
	  end if
	end if
	if(nq.ne.0) then
*	  if nq.eq.0, then finished
*	  L3
3	  continue
	  if(da(np).le.da(nq)) then
*	    L4: advance np
	    if(ns.gt.0.and.ns.le.nn) then
	      irank(ns)=pd_ipm(irank(ns))*iabs(np)
	     else
	      if(ns.eq.0) then
	        irank0=pd_ipm(irank0)*iabs(np)
	       else
	        irankx=pd_ipm(irankx)*iabs(np)
	      end if
	    end if
	    ns=np
	    if(np.gt.0.and.np.le.nn) then
	      np=irank(np)
	     else
	      if(np.eq.0) then
	        np=irank0
	       else
	        np=irankx
	      end if
	    end if
	    if(np.gt.0) goto 3
*	    L5: complete the sublist
	    if(ns.gt.0.and.ns.le.nn) then
	      irank(ns)=nq
	     else
	      if(ns.eq.0) then
	        irank0=nq
	       else
	        irankx=nq
	      end if
	    end if
	    ns=nt
	    do while (nq.gt.0)
	      nt=nq
	      if(nq.gt.0.and.nq.le.nn) then
	        nq=irank(nq)
	       else
	        if(nq.eq.0) then
	          nq=irank0
	         else
	          nq=irankx
	        end if
	      end if
	    end do
	    goto 8
	  end if
*
*	  L6: Advance nq
	  if(ns.gt.0.and.ns.le.nn) then
	    irank(ns)=pd_ipm(irank(ns))*iabs(nq)
	   else
	    if(ns.eq.0) then
	      irank0=pd_ipm(irank0)*iabs(nq)
	     else      
	      irankx=pd_ipm(irankx)*iabs(nq)
	    end if
	  end if
	  ns=nq
	  if(nq.gt.0.and.nq.le.nn) then
	    nq=irank(nq)
	   else
	    if(nq.eq.0) then
	      nq=irank0
	     else
	      nq=irankx
	    end if
	  end if
	  if(nq.gt.0) goto 3
*
*	  L7: Complete the sublist
	  if(ns.gt.0.and.ns.le.nn) then
	    irank(ns)=np
	   else
	    if(ns.eq.0) then
	      irank0=np
	     else
	      irankx=np
	    end if
	  end if
	  ns=nt
	  do while (np.gt.0)
	    nt=np
	    if(np.gt.0.and.np.le.nn) then
	      np=irank(np)
	     else
	      if(np.eq.0) then
	        np=irank0
	       else
	        np=irankx
	      end if
	    end if
	  end do
*
*	  L8: End of pass?
8	  np=-np
	  nq=-nq
	  if(nq.eq.0) then
	    if(ns.gt.0.and.ns.le.nn) then
	      irank(ns) = pd_ipm(irank(ns))*iabs(np)
	     else
	      if(ns.eq.0) then
	        irank0=pd_ipm(irank0)*iabs(np)
	       else
	        irankx=pd_ipm(irankx)*iabs(np)
	      end if
	    end if
	    if(nt.gt.0.and.nt.le.nn) then
	      irank(nt)=0
	     else
	      if(nt.eq.0) then
	        irank0=0
	       else
	        irankx=0
	      end if
	    end if
	    goto 2
	  end if
	  goto 3 
	end if
*
*	irank array is now an index array, which is set up as follows:
*	irank0 is the lowest value
*	irank(i) is the array element which follows element (i)
*	highest element has irank(i)=0
*
	if(chx(1:1).ne.'i'.and.chx(1:1).ne.'I') then
*	  now want to set it up so that the value in irank(i) is the rank:
	  j=irank0
	  k=0
	  do while (j.gt.0)
	    k=k+1
	    l=irank(j)
	    irank(j)=k
	    j=l
	  end do
	end if
	return
	end
	integer function pd_ipm(n)
	if(n.ge.0) then
	  pd_ipm = 1
	 else
	  pd_ipm=-1
	end if
	return
	end
      subroutine pd_isort(nn,da,ds,irank,irank0)
*	sort data array da into ds using index array irank
*	lowest value in irank0
*
	real da(nn),ds(nn)
	integer irank(nn)
*
	j=irank0
	k=0
	do while (j.gt.0)
	  k=k+1
	  ds(k)=da(j)
	  j=irank(j)
	end do
	return
	end	  
      subroutine pd_rsort(nn,da,ds,irank)
*	sort data da into ds using rank array irank
*	note that irank is not modified by this routine
	real da(nn),ds(nn)
	integer irank(nn)
	do j=1,nn
	  ds(j)=da(irank(j))
	end do
	return
	end
