      subroutine vp_sumlinclear(nopt)
*
*     Strip out the column density sums, and tied flags, if in linear
*     mode and if the 'display error' option has been chosen.
*     otherwise - do nothing.
*     Note: this will result in the f26 summary file being written
*     Warn the user if this will occur, otherwise they might restart
*     from an inappropriately modified file.
*
*     rfc 17 Aug 2007
*
      implicit none
      include 'vp_sizes.f'
*
*     Subroutine arguments:
*     VPFIT option number
      integer nopt
*
*     Local variables
      integer i,ic,is,isc,itemp,k,kc,niter
      double precision csum
*
*     Functions
      logical nocgt
*
*     Common:
*     parameter list
      integer nlines,nn
      character*2 ion(maxnio)
      character*4 level(maxnio)
      double precision parm(maxnpa)
      common/vpc_parry/parm,ion,level,nlines,nn
*     parameter letters
      character*2 ipind(maxnpa)
      integer isod,isodh
      common/vpc_usoind/ipind,isod,isodh
*     parameter placement variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     log or linear variable indicator,1 for logN, 0 for linear
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
*     lastch tied for normal operation, later are special
      character*2 lastch,firstch
      common/vpc_ssetup/lastch,firstch
*
      if(indvar.eq.0.and.nopt.ge.9) then
        niter=0
*       Linear mode, and displaying only, so test for summed column densities
        i=0
        do while(i.lt.nlines)
          i=i+1
*         get column density first letter, and compare with lastch
          ic=(i-1)*noppsys+nppcol
          if(nocgt(ipind(ic)(1:1),lastch(1:1)).or.
     :                ipind(ic)(1:1).eq.'%') then
*           Is a summed flag, so 
*           warn the user!
            if(niter.eq.0) then
              write(6,*) 'Column density sums removed'
              write(6,*) ' as are column ties after ',lastch(1:1)
              niter=1
            end if
*           special case of '%', use next as sum flag
            if(ipind(ic)(1:1).eq.'%') then
              itemp=i*noppsys+nppcol
              ipind(ic)=ipind(itemp)
            end if
*           chase down until find the end
            if(i.lt.nlines) then
              is=i+1
              isc=(is-1)*noppsys+nppcol
              do while (ipind(isc).eq.ipind(ic).and.is.le.nlines)
                is=is+1
                isc=(is-1)*noppsys+nppcol
              end do
*             last in sequence is previous
              is=is-1
*              write(6,*) i,is
*              write(6,*) i,parm(ic)
*             convert first (a sum) to the actual value
              if(i.lt.is) then
                csum=0.0d0
                do k=i+1,is
                  kc=(k-1)*noppsys+nppcol
                  write(6,*) k,parm(kc)
                  csum=csum+parm(kc)
                  ipind(kc)='  '
                end do
*               subtract this from the first, and replace first with this new value
                parm(ic)=max(parm(ic)-csum,scalefac*1.0d8)
                ipind(ic)='  '
              end if
            end if
          end if
        end do
      end if
      return
      end
