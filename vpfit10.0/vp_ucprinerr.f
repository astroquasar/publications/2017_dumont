      subroutine vp_ucprinerr(elm,ionz,parerr,parm,ipind,np)
*
*     parameter error printout for ucoptv
*     [statistics for fit not printed here either]
*
      implicit none
      include 'vp_sizes.f'
*
*     subroutine arguments
      character*2 elm(maxnio)
      character*4 ionz(maxnio)
      character*2 ipind(maxnpa)
      double precision parerr(maxnpa),parm(maxnpa)
      integer np
*
*     Local
      logical lgcsum
      character*2 chopt,ipindc
      integer nset,ierst
      integer i,jbase,jb,jc,jz,j4
      integer jbx,iop,nopch,nexti,nextic
      double precision colpt,colept,colpcpt,vtbpt,vtbept
      double precision temppt,tempept,xtpt,xtept
      double precision bvparm,bveparm
*     
*     Functions
      logical lcase,nocgt,ucase
      double precision vpf_bvalsp
*
*     Common
*     parameter placement variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     log or linear variable indicator,1 for logN, 0 for linear
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
*     lastch tied for normal operation, later are special
*     depending on the variable involved
      character*2 lastch,firstch
      common/vpc_ssetup/lastch,firstch
*     turb/temperature special cross-reference list
      integer mtxref(maxnpa)
      common/vpc_txref/mtxref
*     atomic mass and related
      double precision vturb,atmass,temper,fixedbth
      common/vpc_ucopder/vturb(maxnio),atmass(maxnio),
     :         temper(maxnio),fixedbth(maxnio)
*     miscellaneous control variable array
      character*4 chcv(10)
      common/vpc_chcv/chcv
*     chunk list special variables
      integer linchk( maxnio )  ! ties lines to chunks
      common/vpc_linchk/linchk
      character*4 chlnk(maxnio)
      common/vpc_chlnchk/chlnk
*
      integer nopchan,nopchanh,nmonitor
      integer lt(3)             ! output channels for printing
      common/vpc_nopchan/lt,nopchan,nopchanh,nmonitor
*     temperature units
      double precision thunit,thmax
      common/vpc_thscale/thunit,thmax
*     rest wavelength stuff. Action may depend on lqmucf
      logical lqmucf,lchvsqmu
      double precision qmucf
      double precision qscale
      common/vpc_qmucf/qmucf(maxats),qscale,lqmucf,lchvsqmu
*     ucoptv format specifier
      character*132 chformuc,chformer
      character*164 chformet
      common/vpc_ucformat/chformuc,chformer,chformet
*     mimformat for da/a, I/O units
      double precision unitamim
      common/vpc_scalemim/unitamim
*
*     format should have been set in vp_ucprinit. If not, use '*'
      if(chformer(1:1).ne.'(') then
        chformuc='*'
      end if
      lgcsum=.false.
      nopch=min(nopchan,2)
*
*     prepare the parameters, and print
      nset=np/noppsys
      do i=1,nset
        jbase=noppsys*(i-1)
*       column densities
        jc=jbase+nppcol
        if(elm(i).ne.'<>'.and.elm(i).ne.'__'.and.elm(i).ne.'>>') then
*         rescale column densities into ptemcol
          if(indvar.eq.1) then
            colpt=parm(jc)/scalelog
            colept=parerr(jc)/scalelog
*           is it a summed column density?
            lgcsum=.false.
            if(nocgt(ipind(jc)(1:1),lastch).or.
     :               ipind(jc)(1:1).eq.special) then
*             this column density is possibly a sum.
              if(i.gt.1) then
                if(ipind(jc).eq.ipind(jc-noppsys)) then
                  lgcsum=.false.
                 else
                  lgcsum=.true.
                end if
               else
                lgcsum=.true.
              end if
*             if it is a sum, subtract off the rest of the block
              if(lgcsum) then
                colpcpt=10.0**colpt
                nexti=i+1
                nextic=noppsys*(nexti-1)+nppcol
                ipindc=ipind(jc)
                if(ipindc(1:1).eq.special) ipindc=ipind(jc+noppsys)
                do while(ipind(nextic).eq.ipindc.and.nexti.le.nset)
                  colpcpt=colpcpt-10.0d0**(parm(nextic)/scalelog)
                  nexti=nexti+1
                  nextic=noppsys*(nexti-1)+nppcol
                end do
              end if
            end if
           else
            colept=parerr(jc)/scalefac
            colpt=parm(jc)/scalefac
          end if
         else
          colept=parerr(jc)
          colpt=parm(jc)
        end if
*       Doppler parameters
        jb=jbase+nppbval
        vtbept=0.0d0
        tempept=0.0d0
        if(lcase(ipind(jb)(1:1))) then
*         lower case tied velocity
          if(nocgt(ipind(jb)(1:1),lastch)) then
*           special tied velocity - turb & temperature may be separable
*           find cross-referenced dataset
            jbx=mtxref(jb)
            if(jbx.gt.jb) then
              vtbpt=parm(jb)
              temppt=parm(jbx)*thunit
              vtbept=parerr(jb)
              tempept=parerr(jbx)*thunit
             else
              vtbpt=parm(jbx)
              temppt=parm(jb)*thunit
              vtbept=parerr(jbx)
              tempept=parerr(jb)*thunit
            end if
            bvparm=vpf_bvalsp(i,parm,ipind)
            bveparm=0.0d0
           else
*           old style turb/temp fixed
            vtbpt=vturb(i)
            temppt=temper(i)
            vtbept=0.0d0
            tempept=0.0d0
            bvparm=parm(jb)
            bveparm=parerr(jb)
*            values of turb, temp used
*            if(vtbpt.le.0.0d0) then
*              if(temppt.le.0.0d0) then
*               all thermal, so estimate temperature
*                xtemppt=60.135795d0*atmass(i)*bvparm**2
*               else
*               temperature given, so estimate turbulent component
*                xvtbpt=sqrt(bvparm**2-temppt/60.135795d0*atmass(i))
*              end if
*             else
*             Fixed turbulent component, ALWAYS estimate temperature
*              xtemppt=60.135795d0*atmass(i)*(bvparm**2-vtbpt**2)
*            end if
          end if
         else
*         no velocity tying, so all quantities might as well be zero
          vtbpt=0.0d0
          vtbept=0.0d0
          temppt=0.0d0
          tempept=0.0d0
          bvparm=vpf_bvalsp(i,parm,ipind)
          bveparm=parerr(jb)
        end if
*       chunk link stuff
        if(chlnk(i)(1:1).eq.'A'.or.chlnk(i)(1:1).eq.'B'.or.
     :        chlnk(i)(1:1).eq.'C'.or.chlnk(i)(1:1).eq.'D') then
          chopt=chlnk(i)(1:2)
         else
          ierst=0
          write(chopt,'(i2)',iostat=ierst) linchk(i)
          if(ierst.ne.0) chopt=' 0'
        end if
*       redshift
        jz=jbase+nppzed
*       printout section - depends on requested format & noppsys
        if(noppsys.le.3) then
*         standard printout
          if(chcv(9).ne.'c26f') then
*           old fort.13 ordering
            do iop=1,nopch
              write(lt(iop),chformer) elm(i),ionz(i),
     1          colept,ipind(jc),parerr(jz),ipind(jz),bveparm,
     2          ipind(jb),vtbept,tempept,'! ',i
            end do
           else
*           fort.26 ordering
            if(ipind(jb)(1:1).eq.' '.or.elm(i).eq.'<>'.or.
     :       elm(i).eq.'>>'.or.elm(i).eq.'__'.or.
     :       ucase(ipind(jb)(1:1))) then
*             no useful temperature information
              do iop=1,nopch
                 write(lt(iop),chformer) elm(i),ionz(i),
     :              parm(jz),ipind(jz),parerr(jz),
     :              bvparm,ipind(jb),bveparm,
     :              colpt,ipind(jc),colept,chopt
              end do
             else
*             some temperature information, so print it
              do iop=1,nopch
                 write(lt(iop),chformet) elm(i),ionz(i),
     :              parm(jz),ipind(jz),parerr(jz),
     :              bvparm,ipind(jb),bveparm,
     :              colpt,ipind(jc),colept,chopt,
     :              vtbpt,vtbept,temppt,tempept
              end do
            end if
*           was this a sum? (only for log col)
            if(lgcsum.and.(.not.ucase(ipind(jc)(1:1)))) then
              if(colpcpt.gt.0.0d0) then
                colpcpt=log10(colpcpt)
              end if
              do iop=1,nopch
                write(lt(iop),'(a,f14.5)') 
     :           '! Component column density:',colpcpt
              end do
            end if
          end if
         else
*         noppsys>3: some extra parameters may need rescaling
          j4=jbase+4
          if(ipind(j4)(1:1).eq.'q'.or.ipind(j4)(1:1).eq.'Q'.or.
     :       ipind(j4)(1:1).eq.'m'.or.ipind(j4)(1:1).eq.'M') then
            xtept=qscale*parerr(j4)
            xtpt=qscale*parm(j4)
            if(unitamim.le.1.0d-3) then
              xtpt=xtpt/unitamim
              xtept=xtept/unitamim
            end if
           else
            xtpt=parm(j4)
            xtept=parerr(j4)
          end if
*         print out
          if(chcv(9).ne.'c26f') then
*           old fort.13 ordering
            do iop=1,nopch
              write(lt(iop),chformer) elm(i),ionz(i),
     1            colept,ipind(jc),parerr(jz),ipind(jz),bveparm,
     2            ipind(jb),xtept,ipind(j4),
     3            vtbept,tempept,'! ',i
            end do
           else
*           fort.26 ordering
            if(ipind(jb)(1:1).eq.' '.or.elm(i).eq.'<>'.or.
     :       elm(i).eq.'>>'.or.elm(i).eq.'__'.or.
     :      (ucase(ipind(jb)(1:1)).and.
     :       nocgt(ipind(jb)(1:1),lastch))) then
              do iop=1,nopch
                write(lt(iop),chformer) elm(i),ionz(i),
     :              parm(jz),ipind(jz),parerr(jz),
     :              bvparm,ipind(jb),bveparm,
     :              colpt,ipind(jc),colept,
     :              xtpt,ipind(j4),xtept,chopt
              end do
             else
              do iop=1,nopch
                write(lt(iop),chformet) elm(i),ionz(i),
     :              parm(jz),ipind(jz),parerr(jz),
     :              bvparm,ipind(jb),bveparm,
     :              colpt,ipind(jc),colept,
     :              xtpt,ipind(j4),xtept,chopt,
     :              vtbpt,vtbept,temppt,tempept
              end do
            end if
          end if          
        end if
      end do
      return
      end
