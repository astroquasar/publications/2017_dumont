      subroutine vp_tieval( ion, level, np, parm, ipind, temp, 
     :                   vturb, mass, fixedbth)

      implicit none

      character*2 ion(*)
      character*4 level(*)
      integer np
      double precision parm(np)
      character*2 ipind(np)
      double precision temp(*), vturb(*)
      double precision mass(*), fixedbth(*)
      integer i,ival,j,nn
      logical lcase,nocgt
*     COMMON:
*     parameter list length & positions
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     special ipind separators
      character*2 lastch,firstch
      common/vpc_ssetup/lastch,firstch
*     
*     Make sure the Doppler parameters are positive:
      nn=np/noppsys
      do j=1,nn
        ival=(j-1)*noppsys+nppbval
*       primary b values must be at least the minimum
        if(lcase(ipind(ival)(1:1)).or.ipind(ival).eq.'  ') then
*         omit special cases, and temperature/turbulent ones:
          if(ion(j).ne.'<>'.and.ion(j).ne.'>>'
     :          .and.ion(j).ne.'__'.and.
     :          (.not.nocgt(ipind(ival),lastch))) then
            call checkb(temp(j),vturb(j),mass(j),parm(ival))
          end if
        end if
      end do
*
      do i = 1, np
        call vp_tie1val( ion, level, np, parm, ipind, temp, 
     :               vturb, mass, fixedbth, i)
      end do
      return
      end


      subroutine vp_tie1val( ion, level, np, parm, ipind, temp, 
     :                   vturb, mass, fixedbth, ival)

*     ties any values which have upper case letters which match lower 
*     case letters in this line.
      implicit none

      include 'vp_sizes.f'

*     parameters
      character*2 ion(maxnio)
      character*4 level(maxnio)
      integer np
      double precision parm(np)
      character*2 ipind(np)
      double precision temp(*), vturb(*)
      double precision mass(*), fixedbth(*)
      integer ival
*     Local variables
      integer j,jn,nn
      integer iparmx
      integer ibn,ibparm
      double precision tempref,tempel 
*     Functions
      double precision vp_abund
      logical lcase, noceqv, nocgt
      
*     Totals (col), or special (b), after character lastch
*     Form following (col) after firstch 
      character*2 lastch,firstch
      common/vpc_ssetup/lastch,firstch
*
*     element abundance and ionization information
      integer nabund
      double precision abund(120)
      character*2 chaelm(120)
      character*4 chaion(120)
      common/vpc_abund/abund,chaelm,chaion,nabund
*
*     log/linear variables
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
*     parameter list length & positions
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     x-reference tied list
      integer mtxref
      common/vpc_txref/mtxref(maxnpa)

*     set base for ion etc, and base level for parm set
      ibn=(ival-1)/noppsys
      ibparm=ibn*noppsys
      ibn=ibn+1
*     iparmx is parameter in range 1 - noppsys, gives type of parameter 
*     (col, z, bval, ...)
      iparmx=ival-ibparm
      nn=np/noppsys
*     first check for b values
      if(iparmx.eq.nppbval) then
*
*       secondary b values must be tied
        if(lcase(ipind(ival)(1:1))) then
          if(nocgt(lastch,ipind(ival)(1:1))) then
*           Not special, so only tie secondary b values
            do jn=1,nn
              j=(jn-1)*noppsys+nppbval
              if(noceqv(ipind(ival),ipind(j)).and.ival.ne.j) then
                call tiedb(ion(jn),temp(ibn), vturb(ibn),
     :                       mass(ibn), mass(jn),
     :                       fixedbth(ibn), 
     :                       fixedbth(jn),
     :                       parm(ival), parm(j))
*                if(ion(jn).eq.'>>') then
*	           >> iteration value may get reset, but converges anyway.
*                  if worried about efficiency look at this and correct [TODO]
*                  write (6,'(a,i5,f9.4,a,f9.4,i5)') 
*     1               '>>',j,parm(j), ' <- ', parm(ival),ival
*                end if
              end if
            end do
          end if
        end if
       else
*	tied z or N (if not a cumulative n)
        if(lcase( ipind(ival)(1:1)).and.
     1       (iparmx.eq.nppzed.or.iparmx.ge.4)) then
*         tied z, or extra parameter, so copy directly
          do jn=1,nn
*           was j=mod(ival,3),np,3
            j=(jn-1)*noppsys+iparmx
            if(j.ne.0) then
              if(noceqv(ipind(j),ipind(ival))) then
*               write (*,*) 'altering a tied value'
*               write (*,*) ipind(j),ipind(ival)
*               write (*,*) parm(j),' <- ',parm(ival)
                parm(j) = parm(ival)
              end if
            end if
          end do
         else
*	  N: column densities tied
          if((lcase(ipind(ival)(1:1)).and.
     :        iparmx.eq.nppcol).and.(.not. 
     :          nocgt(ipind(ival)(1:1),lastch))) then
*	    if no abundances, tie column densities directly....
            if(nabund .eq. 0) then
              do j = mod(ival, 3), np, 3
                if(noceqv(ipind(j),ipind(ival))) then
                  parm(j) = parm(ival)
                end if
              end do
*             otherwise use ionization model O/P e.g. cloudy (?)
             else
*             nvx=(ival+2)/3
*
              if(ion(ibn).eq.'__'.or.ion(ibn).eq.'<>'.or.
     :            ion(ibn).eq.'>>') then
*	        Special characters: don't worry about cloudy.. 
*	        just tie directly
                do jn = 1,nn
*                 was j=mod(ival, 3), np, 3
                  j=(jn-1)*noppsys+iparmx
                  if(noceqv(ipind(j),ipind(ival))) then
                    parm(j) = parm(ival)
                  end if
                end do
*               branch to end
                goto 9011
              end if
*
              tempref=vp_abund(ion(ibn),level(ibn))
              do jn=1,nn
                j=(jn-1)*noppsys+iparmx
                if(j.ne.0.and.j.ne.ival) then
                  if(noceqv( ipind(j), ipind(ival))) then
                    if(nabund.gt.0) then
*  	              scale by abundances/ionization
                      tempel=vp_abund(ion(jn),level(jn))
                      if(indvar.eq.1) then
                        parm(j)=parm(ival)-tempref+tempel
                       else
                        parm(j)=parm(ival)*(tempel/tempref)
                      end if
                     else
                      parm(j) = parm(ival)
                      write (6,*) ipind(j), ipind(ival),' ',
     :                                parm(j), ' <- ', parm(ival)
                    end if
                  end if
                end if
              end do
 9011         continue
            end if
          end if
        end if
      end if
      return
      end
