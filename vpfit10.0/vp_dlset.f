      subroutine vp_dlset(nchunk,xlamx,fvmah,fdlamh)
c
c     Check the spectral resolution for a spectral region, and
*     prompt for values if needed
c
c     INPUT:	
c     nchunk	spectral region number
c     xlamx	wavelength of center (*8)
c     fvmah	previous vel FWHM value
c     fdlamh	previous FWHM dlam value
c     OUTPUT:
*     None: updates common block vpc_sigwres
c     
      implicit none
      include 'vp_sizes.f'
*     
      integer nchunk
      double precision xlamx,fvmah,fdlamh
*
*     LOCAL:
*     double precision sig,dl
      integer nitcount,status
      double precision dtemp
*
*     FUNCTIONS
      integer lastchpos
      double precision vpf_dvresn
*     
*     COMMON:
*     binned profile
      character*2 chprof
      double precision pfinst
      double precision wfinstd
      integer npinst,npin2
      common/vpc_profwts/wfinstd(maxipp,maxnch),pfinst(maxipp,maxnch),
     :        npinst(maxnch),npin2(maxnch),chprof(maxnch)
*     readin workspace
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
      double precision dvstr(24)
      common/vpc_dsepspace/dvstr
*     fit to resolution, using sigmas
      integer nswres(maxnch)
      double precision swres(maxpoc,maxnch)
      common/vpc_sigwres/swres,nswres
*       
      logical verbose
      common/vp_sysout/verbose
*     input streams
      integer inputc
      common/rdc_inputc/inputc
*
      if(verbose) then
        write(6,*) 'entering vp_dlset with chprof=',chprof(nchunk)
        write(6,*) 'chunk #',nchunk
      end if
      nitcount=0
*     chprof(nchunk)='  '
*
*     Bypass this lot if pixel resolution file already read in:
      if(npinst(nchunk).gt.0) then
*       Reassure any nervous user!
        if(chprof(nchunk)(1:2).ne.'wt') then
          write(6,*) 'Using pixel instrument profile'
         else
          write(6,*) 'Interpolating to obtain resolution'
        end if
        goto 2001
      end if
*
* 1998	continue
      dtemp=vpf_dvresn(xlamx,nchunk)*2.35482d0
      if(dtemp.gt.99.0d0.or.dtemp.le.0.0d0) then
*       there may be a problem with the resolution
 1898   write(6,'(a,f6.1,f7.3,a)')  'FWHM (km/s & A) were ',
     :         fvmah,fdlamh, ' <CR> to accept, or enter values'
*       Filename of fit coeffts no longer supported, since
*       this can be in the FITS header, and flexible
*       wavelength/resolution tables now implemented.
*       write(6,*) ' >>>> NOTE IS FWHM, NOT SIGMA <<<<'
*       Echelle best approximation is constant sigma (km/s)
*       give defaults
        read(inputc,'(a)',end=9981) inchstr
        goto 9982
 9981   write(6,*) 'Input file ended -- switch to terminal'
        inputc=5
        nitcount=nitcount+1
        if(nitcount.gt.25) then
          write(6,*) 'Failed to get resolution'
          write(6,'(a,i4,a)') 'after',nitcount,' attempts.'
          write(6,*) 'PROGRAM TERMINATING !!!!!!!!!!!!'
          write(6,*) '________________________________'
          stop
        end if
        goto 1898
 9982   continue
*       pixel instrument profile allowed here
        if(inchstr(1:2).eq.'px'.or.inchstr(1:2).eq.'pf') then
          if(inchstr(3:3).eq.'=') inchstr(3:3)=' '
          call dsepvar(inchstr,2,dvstr,ivstr,cvstr,nvstr)
          status=0
          nvstr=lastchpos(cvstr(2))
          write(6,*) 'Using (sub)pixel resolution ',
     1          cvstr(2)(1:nvstr)
          call vp_getres(cvstr(2),nchunk,0,status)
          goto 2001
        end if
        call dsepvar(inchstr,3,dvstr,ivstr,cvstr,nvstr)
        if(cvstr(1)(1:1).ne.' ') then
*         need to do something
          fvmah=dvstr(1)
          fdlamh=dvstr(2)
          swres(1,nchunk)=dvstr(2)/2.35482d0
          swres(2,nchunk)=dvstr(1)/7.05957d5
          nswres(nchunk)=2
          chprof(nchunk)='  '
         else
          swres(1,nchunk)=fdlamh/2.35482d0
          swres(2,nchunk)=fvmah/7.05957d5
        end if
      end if
 2001 return
      end
