      subroutine vp_wrt25( np, ndf, hessian, chisq )

*     write the hessian matrix out to fort.25

      implicit none
      include 'vp_sizes.f'

      integer np, ndf
      double precision chisq
      double precision hessian( maxnpa, maxnpa )

      integer ier
      integer i, j

      ier = 0
*     this check now done outside the routine
*     if(chcv(2)(1:4).eq.'wr25') then

      open ( unit = 25, form = 'unformatted', status = 'unknown',
     :       iostat = ier, err = 1 )

      write ( 25, iostat = ier, err = 1 ) np
      write ( 25, iostat = ier, err = 1 ) ndf
      write ( 25, iostat = ier, err = 1 ) chisq

*     note write order
      do j = 1, np
        do i = 1, np
          write (25,iostat=ier,err=1) hessian( i, j )
        end do
      end do

 1    continue
      if ( ier .ne. 0 ) then
        write ( *, * ) ' problem writing hessian'
      end if
      close( 25, err = 2 )
 2    continue
      return
      end
