      subroutine vp_lyconte(cold,veld,flx,nw,zdp1)
*
*     Put Lyman limit in the data, for the expanded data array
*     IN:
*     cold: HI column density
*     veld: Doppler parameter
*     flx(nw): continuum for this line set
*     zdp1: 1+redshift
*     ichunk: chunk number
*     OUT:
*     flx(nw) continuum with a with a Lyman limit
*
      implicit none
      include 'vp_sizes.f'
      double precision cold,veld,zdp1
      integer nw
      double precision flx(nw)
*     
      integer i,nlast,nup,k,ncur
      double precision sflx
      double precision tzero,x
      double precision wvd,ced
      double precision tauprev,taus,wrest,wrestprev
      double precision a,v,cns,cne,wv,bl,dwrest
*     functions
      double precision dexpf
      double precision voigt
*     
*     Minimum wavelength Lyman line (for interpolation to Lyman cont.)
      double precision wlsmin,vblstar,collsmin
      common/vpc_lycont/wlsmin,vblstar,collsmin
*     control for verbose output:
      logical verbose
      common/vp_sysout/verbose
*     atomic data
      character*2 lbz
      character*4 lzz
      integer nz
      double precision alm,fik,asm 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :     fik(maxats),asm(maxats),nz
*     subdivided chunk wavelengths (v8.03 and up)
      integer npsubch
      double precision wvsubd(maxscs)
      common/vpc_wvsubd/wvsubd,npsubch
*
*
*     subroutine to take care of Lyman limit absorption. 
*     NOTE: Does not put it in
*     if formal Lyman limit falls outside the chunk
*
      if(verbose) write(6,*) ' Lyman limit absorption included'
*     wavelengths in observers frame
      wvd=911.7536d0*zdp1
*     start guess channel (can be outside range)
      ced=((wvd-wvsubd(1))*dble(npsubch)+wvsubd(npsubch)-wvd)/
     :          (wvsubd(npsubch)-wvsubd(1))
      call vp_archwav(wvd,ced,wvsubd,npsubch)
*     Lyman limit channel (which may be negative!)
      nup=int(ced)
*     If Lyman limit higher than channel maximum, set to max channel:
      nup=min0(nup,nw)
      if(nup.gt.0) then
*       set continuum absorption if range includes it:
        tzero=cold*6.30d-18
        do i=1,nup
          x=wvsubd(i)/(zdp1*911.7536d0)
          x=x*x*x
          flx(i)=exp(-tzero*x)
        end do
*       if Lyman limit wavelength is within the region, need something
*       a bit better - interpolate to the first local 
*       minimum in an array which contains only lines from this system
*       before convolution with the instrument profile.
        if(nup.lt.nw) then
*	  set up interpolation limits
*	  find first channel at local minimum
*	  step 1: find channel corresponding to lowest wavelength hydrogen line
*         since first minimum will be here or above.
          wvd=wlsmin*zdp1
*         start guess channel (can be outside range)
          ced=((wvd-wvsubd(1))*dble(npsubch)+wvsubd(npsubch)-wvd)/
     :          (wvsubd(npsubch)-wvsubd(1))
*	  step 2: search up for local flux minimum in Lyman series spectrum
*	  start at int(ced)
          nlast=int(ced)
          tauprev=0.0d0
          wrestprev=wlsmin
          wrest=wlsmin
          taus=1.0d-3
          ncur=nlast
          dwrest=(wvsubd(npsubch)-wvsubd(1))/
     :               (dble(npsubch-1)*zdp1*1.0d8)
*         need to add the search bit here
          if(tauprev.lt.taus) then
            tauprev=taus
            wrestprev=wrest
            if(ncur.le.nw) then
              wrest=wvsubd(ncur)/(zdp1*1.0d8)
             else
              wrest=wrest+dwrest
            end if          
*           calculate the optical depth at this rest wavelength
            taus=0.0d0
            do k=1,nz
              if(lbz(k).eq.'H '.and.lzz(k).eq.'I   ') then
*               is a hydrogen line, so include it
                wv=alm(k)*1.0d-8
                bl=veld*wv/2.99792458d5
                a=asm(k)*wv*wv/(3.7699d11*bl)
                cns=wv*wv*fik(k)/(bl*2.00213d12)
                cne=cold*cns
                v=wv*wv*(1.0d0/wrest-1.0d0/wv)/bl
                taus=taus+cne*voigt(v,a)
              end if
            end do
          end if
*         if reaches here, wrestprev is wavelength of minimum,
*         and it has total optical depth tauprev - interpolate flux
          sflx=dexpf(-tauprev)
          if(wrest.le.wvsubd(npsubch)/(zdp1*1.0d8)) then
            ncur=ncur-1
          end if
          if(ncur.gt.nup) then
            do k=nup+1,ncur
              wv=wvsubd(k)/(zdp1*1.0d8)
              flx(k)=flx(nup)*(wrestprev-wv)+
     :               sflx*(wv-wvsubd(nup)/(zdp1*1.0d8))
            end do
          end if
        end if
      end if
      return
      end
