      subroutine vp_nearlin(parm,ion,level,np,wlm,kclosh)
*
      implicit none
      include 'vp_sizes.f'
*
*     find the nearest line to a given wavelength, given the
*     current parameter list (excluding those with oscillator strength zero,
*     since you can't do anything with those
*
      double precision parm(maxnpa)
      character*2 ion(maxnio)
      character*4 level(maxnio)
      integer np
      double precision wlm
      integer kclosh
*
*     LOCAL:
      integer i,k
      integer jv
      double precision vclosh,veldif,wvxx,zp1
*     double precision wvxh ! RFC 4.10.02 not used.
*
*     COMMON:
*     additional info returned to calling routine via common
*     iclosh	integer	ion which is closest in parm list
*     rwclosh	dble	rest wavelength of closest line
*     fikclh	dble	oscillator strength for this line
      integer iclosh
      double precision rwclosh,fikclh
      common/vpc_closelin/rwclosh,fikclh,iclosh
*     line list
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm
      integer nz 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),nz
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*
*     parm:
*     j*noppsys+nppcol  ... are column densities (j.ge.0)
*     j*noppsys+nppzed  ... are redshifts
*     j*noppsys+nppbval ... Doppler parameters
*     
      iclosh=0
      kclosh=0
      vclosh=1.0d37
*     wvxh=0.0d0
*
      do i=1,np/noppsys
        if(ion(i).ne.'__'.and.ion(i).ne.'>>'.and.ion(i).ne.'<<'
     :       .and.ion(i).ne.'<>'.and.ion(i).ne.'??'.and.
     :       ion(i).ne.'AD') then
          jv=noppsys*(i-1)
*         jc=jv+nppcol
          zp1=parm(jv+nppzed)+1.0d0
          do k=1,nz
            if(lbz(k).eq.ion(i).and.lzz(k).eq.level(i).and.
     1         fik(k).gt.0.0d0) then
*	      same ion, and has non-zero f-value
              wvxx=alm(k)*zp1
              veldif=abs(wvxx-wlm)*2.99792458d5/wlm
              if(veldif.lt.vclosh) then
*               wvxh=wvxx
                vclosh=veldif
                kclosh=k
                iclosh=i
                rwclosh=alm(k)
                fikclh=fik(k)
              end if
            end if
          end do
        end if
      end do
      return
      end

      subroutine vp_nrlinad(wlm,fwhmm,ion,level,parm,np,
     :         wlmx,bvalx,colx,ichunk)
*
      implicit none
      include 'vp_sizes.f'
*     find the nearest strong line in the list so far to a 
*     given wavelength. For use with autofitting procedure.
*     IN:
*     wlm	maximum deviation wavelength
*
*     OUT:
*
      double precision wlm,fwhmm
      character*2 ion(maxnio)
      character*4 level(maxnio)
      double precision parm(maxnpa)
      integer np
      double precision wlmx,bvalx,colx
      integer ichunk
*
*     LOCAL:
      integer kclosh,i,k,jc,iflag
      integer jviclosh,jb,jv
      double precision bvx,colold,dwlmn,dwlmx
      double precision ptemp,sig,tcen,tcx,vclosh,veldif
      double precision woff,wofx,wvxh,wvxx,zp1
      double precision wlmn,wofn,velhwm,wledge
*
*     FUNCTIONS
      double precision vpf_dvresn
*     
*     COMMON:
*     additional info returned to calling routine via common
*     iclosh	integer	ion which is closest in parm list
*     rwclosh	dble	rest wavelength of closest line
*     fikclh	dble	oscillator strength for this line
      integer iclosh
      double precision rwclosh,fikclh
      common/vpc_closelin/rwclosh,fikclh,iclosh
*     line list
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm
      integer nz 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :     fik(maxats),asm(maxats),nz
*     Log N if indvar=1, N otherwise (-1 for emission)
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
*     Wavelengths for adding lines
      double precision whlo,whhi
      common/vpc_addln/whlo,whhi
*
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*
*     parm:
*     j*noppsys+nppcol  ... are column densities (j.ge.0)
*     j*noppsys+nppzed  ... are redshifts
*     j*noppsys+nppbval ... Doppler parameters
*     if noppsys.gt.3, then next variables are skew etc
*     
*
      iclosh=0
      kclosh=0
      vclosh=1.0d37
      wvxh=0.0d0
*
      do i=1,np/noppsys
        if(ion(i).ne.'__'.and.ion(i).ne.'>>'.and.ion(i).ne.'<<'
     :       .and.ion(i).ne.'<>'.and.ion(i).ne.'??'.and.
     :       ion(i).ne.'AD') then
          jv=noppsys*(i-1)
          zp1=parm(jv+nppzed)+1.0d0
          do k=1,nz
            if(lbz(k).eq.ion(i).and.lzz(k).eq.level(i).and.
     1         fik(k).gt.0.0d0) then
*             same ion, and fik>0
              wvxx=alm(k)*zp1
              veldif=abs(wvxx-wlm)*2.99792458d5/wlm
              if(veldif.lt.vclosh) then
                wvxh=wvxx
                vclosh=veldif
                kclosh=k
                iclosh=i
                rwclosh=alm(k)
                fikclh=fik(k)
              end if
            end if
          end do
        end if
      end do
*     closest ion number is iclosh, and atom number in table is kclosh
      jviclosh=noppsys*(iclosh-1)
      jc=jviclosh+nppcol
      jb=jviclosh+nppbval
*     compute the half power points for the closest line:
      call vp_hawid(kclosh,parm(jb),parm(jc),tcen,woff)
*     wavelength offset in observed frame
      woff=woff*(1.0d0+parm(jviclosh+nppzed))
*     now have wlm as dividing point, with nearest line wvxh, and
*     line limits wvxh-woff to wvxh+woff
*     put lines in at halfway points:
      if(wlm.gt.wvxh-woff) then
        wlmx=0.5d0*(wlm+wvxh+woff)
        dwlmx=0.5d0*abs(wvxh+woff-wlm)
        wlmn=wlmx-woff
        dwlmn=abs(woff-dwlmx)
*       Now have to guess Doppler parameters and column densities
*       do this iteratively as well, starting with guesses of 1/2
*       column density and Doppler parameter corresponding to new
*       FWHM as maxima, then step down first in Doppler parameter,
*       then in column density until there is an acceptable value.
        colx=parm(jc)
        colold=colx
        iflag=0
 201    continue
        if(indvar.eq.1) then
          colx=colx-0.5d0
          if(colx.lt.colold-2.0d0 .or. 
     :       colx.lt.12.5d0/scalelog) iflag=1
           else
            colx=colx/3.16d0
            if(colx.lt.colold/1.0d2 .or.
     :         colx.lt.3.16d12/scalefac) iflag=1
          end if  
          bvx=2.99792458d5*dwlmx/wlmx
          wofx=1.0d34
          sig=4.2397056d5*vpf_dvresn(wlmx,ichunk)
*         was = 1.41d0*(sigma(ichunk)+dlam(ichunk)/wlmx*2.99792458d5)
          if(bvx.lt.sig) bvx=sig
          do while (wofx.gt.dwlmx.and.bvx.ge.sig)
            bvx=bvx/1.4d0
            call vp_hawid(kclosh,bvx,colx,tcx,wofx)
          end do
          if(bvx.lt.sig.and.iflag.ne.1) goto 201
*	  have guesses, so plug them in:
          parm(jc)=colx
          parm(jviclosh+nppzed)=wlmx/alm(kclosh)-1.0d0
          parm(jb)=bvx
*
          write(6,*) 'Line ',iclosh,' modified'
          if(indvar.eq.1) then
            ptemp=parm(jc)/scalelog
           else
            ptemp=log10(parm(jc)/scalefac)
          end if
          write(6,'(''Logcol:'',f7.3,''  z:'',f9.6,''  b:'',f7.1)')
     :        ptemp,parm(jviclosh+nppzed),parm(jb)
*	  and suggest the new one, using the same procedure:
          colx=parm(jc)
          iflag=0
 202      continue
          if(indvar.eq.1) then
            colx=colx-0.5d0
            if(colx.lt.12.5d0/scalelog) iflag=1
           else
            colx=colx/3.16d0
            if(colx.lt.3.16d12/scalefac) iflag=1
          end if  
          bvalx=2.99792458d5*dwlmn/wlmn
          wofn=1.0d34
*	  sig=1.4d0*(sigma(ichunk)+dlam(ichunk)/wlmn*2.99792458d5) becomes
          sig=4.2397056d5*vpf_dvresn(wlmx,ichunk)
          do while (wofn.gt.dwlmn.and.bvalx.gt.sig)
            bvalx=bvalx/1.4d0
            call vp_hawid(kclosh,bvalx,colx,tcx,wofn)
          end do
          if(bvalx.lt.sig.and.iflag.ne.1) goto 202
*	  bvalx is transmitted back as b-value for new line
*	  colx is the column density, wlmx is the wavelength
          wlmx=wlmn
         else    
*	  Resort to wild guesses:
*	  leave a weaker line at the old position, and put a new one at an 
*	  equal distance on the other side of the peak is one possibility
*	  need to modify this since peak is not in centre always!
          dwlmx=abs(wlm-wvxh)
          wlmx=2.0d0*wlm-wvxh
*	  move the redshift to FWHM/2 away is another, which makes more sense
*	  if the range is too short
          velhwm=fwhmm/wlm*1.5d5
          write(6,*) '>>>> ',fwhmm,wlm,velhwm,vclosh
          wledge=alm(kclosh)*(parm(jc+1)+1.0d0)
          if(abs(wlm-wledge)*2.99792458d5/wlm.lt.velhwm) then
*	    need to move the line a bit to half power point
            if(wledge-wlm.gt.0.0d0) then
              parm(jviclosh+nppzed)=whhi/alm(kclosh)-1.0d0
              wlmx=whlo
              parm(jviclosh+nppbval)=min(20.0d0,velhwm/1.4d0)
             else
              parm(jviclosh+nppzed)=whlo/alm(kclosh)-1.0d0
              wlmx=whhi
              parm(jviclosh+nppbval)=min(20.0d0,velhwm/1.4d0)
            end if
           else
            parm(jviclosh+nppbval)=min(vclosh,20.0d0)
          end if
*	  setting the new column densities is less straightforward.
*	  This as a simple trial until we see if we need someting better:    
          if(indvar.eq.1) then
            if(parm(jc).gt.16.5d0*scalelog) then
              parm(jc)=14.75d0*scalelog
             else
              parm(jc)=min(parm(jc)-0.7d0,14.0d0*scalelog)
            end if
           else
            if(parm(jc).gt.scalefac*3.16d16) then
              parm(jc)=6.0d14*scalefac
             else
              parm(jc)=min(parm(jc)/5.0d0,1.0d14*scalefac)
            end if
          end if
          write(6,*) 'Line ',iclosh,' modified'
          if(indvar.eq.1) then
            ptemp=parm(jc)/scalelog
           else
            ptemp=log10(parm(jc)/scalefac)
          end if
          write(6,'(''Logcol:'',f7.3,''  z:'',f9.6,''  b:'',f7.1)')
     :        ptemp,parm(jviclosh+nppzed),parm(jb)
*	  and suggest a new one:
          bvalx=parm(jb)
          colx=parm(jc)
        end if
      return
      end
      subroutine vp_hawid(kclosh,bval,cxxd,tcen,woff)
      implicit none
      include 'vp_sizes.f'
*     compute half power points for line kclosh, parms bval,cxxd
      integer kclosh
      double precision bval,cxxd,tcen,woff
*     
      integer nit
      double precision a,bl,cne,cns,cold,wv
      double precision xtxx
      double precision fdiff,temp,thalf,v,vh,vm,f,fh,fm
*
*     FUNCTIONS:
      double precision voigt,dexpf
*
*     atom names for table
      character*2 lby
*     lzz is the 'level' i.e. ionization stage
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm
      integer nz 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),nz
*     indvar=1 for log N, 0 linear, -1 for emission lines (linear).
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
*
      lby=lbz(kclosh)
      wv=alm(kclosh)*1.0d-8
      bl=bval*wv/2.99792458d5
      a=asm(kclosh)*wv*wv/(3.7699d11*bl)
      cns=wv*wv*fik(kclosh)/(bl*2.0046d12)
*     antilog cxxd if necessary, but not if added component
      if(indvar.eq.1.and.lby.ne.'AD') then
        xtxx=cxxd/scalelog
        if(xtxx.gt.34.0d0.or.xtxx.lt.10.0d0) then
          xtxx=10.0d0
          cxxd=10.0d0*scalelog
        end if
        cold=10.0d0**xtxx
       else
        if(lby.eq.'AD') then
          cold=cxxd
         else
          cold=cxxd/scalefac
        end if
      end if
      cne=cold*cns
*     line center optical depth
      vm=0.0d0
      tcen=cne*voigt(vm,a)
      if(tcen.gt.0.0d0) then
*       now search upwards for the point corresponding to thalf,
*       where 1-exp(-tcen)=2*(1-exp(-thalf))
        temp=dexpf(-tcen)
        thalf=log(2.0d0/(1.0d0+temp))
        vh=0.0d0
        fh=tcen-thalf
        fm=1.0d0
        nit=0
*       guess a vm
        vm=1.0d-8/bl
        do while(fm.gt.0.0d0.and.nit.lt.150)
          vm=vm*2.0d0
          fm=cne*voigt(vm,a)-thalf
          nit=nit+1
        end do
*       development diagnostic
*       write(6,*) vm,fm
        if(nit.ge.150) write(6,*) 'vp_nrlinad: iteration failure'
*       linear interpolation for next guess
        nit=0
        fdiff=fh-fm
        do while (fdiff.gt.0.005d0.and.nit.lt.150)
*         just halving will be enough
          v=0.5d0*(vm+vh)
          f=cne*voigt(v,a)-thalf
          if(f.gt.0.0d0) then
            fh=f
            vh=v
           else
            fm=f
            vm=v
          end if
          fdiff=fh-fm
          nit=nit+1
        end do
*       wavelength offset in A
        woff=v*bl*1.0d8
*       development diagnostic
*       write(6,*) vh,vm,fh,fm,woff
       else
        woff=0.0d0
      end if
      return
      end
