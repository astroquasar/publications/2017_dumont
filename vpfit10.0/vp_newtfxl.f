      subroutine vp_newtfxl(ipind,nfunc, ipvz,ipvb,ipbvind)
*
*     search for the highest lower case letter combination, and 
*     return lower case values for the next redshift & Doppler parameter
*
*     rfc 07.03.05
*
*     IN:
*          ipind  ch*2array  tie/fix indicator
*
*     OUT: ipvz   character*2 redshift indicator letters (lower case)
*          ipbv   character*2 Doppler parameter letters
*          ipvind integer     0 if z,b are independent sequences, 
*                             1 if z,b together, so b thermal or turbulent
*
*     subroutine parameters
      integer nfunc
      character*2 ipind(*)
*     local variables
      character*2 ipvz,ipvb
*
*     Common variables:
*
*     Totals after this character 
      character*2 lastch,firstch
      common/vpc_ssetup/lastch,firstch
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*
*     initial values are lowest in allowed list
      ipvz='aa'
      ix=ichar(lastch(1:1))+1
      if(ix.le.ichar('z')) then
        ipvb=char(ix)//'a'
        ipbvind=0
       else
*       lastchtied is 'z', so can't use special temperature feature
*       so assume thermal (or whatever the default is)
        ipbvind=1
        ipvb='aa'
      end if
      do j=1,nfunc
        jz=noppsys*(j-1)
        jb=jz+nppbval
        jz=jz+nppzed
        if((ichar(ipind(jz)(1:1)).gt.ichar(ipvz(1:1))).or.
     :       (ipind(jz)(1:1).eq.ipvz(1:1).and.
     :        ichar(ipind(jz)(2:2)).gt.ichar(ipvz(2:2))) ) then
          ipvz=ipind(jz)
        end if
        if((ichar(ipind(jb)(1:1)).gt.ichar(ipvb(1:1))).or.
     :       (ipind(jb)(1:1).eq.ipvb(1:1).and.
     :        ichar(ipind(jb)(2:2)).gt.ichar(ipvb(2:2))) ) then
          ipvb=ipind(jb)
        end if
      end do
*     ipvz & ipvb are now the highest characters in the list
*     now set to the next one up!
      if(ipbvind.eq.0) then
*       redshifts and Doppler parameters form independent sequences
        l2=ichar(ipvz(2:2))
        if(l2.lt.ichar('z')) then
          ipvz=ipvz(1:1)//char(l2+1)
         else
          if(ichar(ipvz(1:1)).lt.ichar(lastch(1:1))) then
            l2=ichar(ipvz(1:1))
            ipvz=char(l2+1)//'a'
           else
*           print a warning and give up!
            write(6,*) 'vp_newtflag: z character out of range'
            stop
          end if
        end if
*       now Doppler parameters
        l2=ichar(ipvb(2:2))
        if(l2.lt.ichar('z')) then
          ipvb=ipvb(1:1)//char(l2+1)
         else
          if(ichar(ipvb(1:1)).lt.ichar('z')) then
            l2=ichar(ipvb(1:1))
            ipvb=char(l2+1)//'a'
           else
*           print a warning and give up!
            write(6,*) 'vp_newtflag: b character out of range'
            stop
          end if
        end if
       else
*       redshifts and Doppler parameters might as well be same sequence
*       see which is the highest in the list (and use ipvz as highest)
        if(ichar(ipvz(1:1)).le.ichar(ipvb(1:1))) then
          if(ichar(ipvz(1:1)).lt.ichar(ipvb(1:1))) then
            ipvz=ipvb
           else
*           first letter equality, so use second
            l2=max(ichar(ipvz(2:2)),ichar(ipvb(2:2)))
            ipvz=ipvz(1:1)//char(l2)
          end if
        end if
        l2=ichar(ipvz(2:2))
        if(l2.lt.ichar('z')) then
          ipvz=ipvz(1:1)//char(l2+1)
         else
          if(ichar(ipvz(1:1)).lt.ichar(lastch(1:1))) then
            l2=ichar(ipvz(1:1))
            ipvz=char(l2+1)//'a'
           else
*           print a warning and give up!
            write(6,*) 'vp_newtflag: z character out of range'
            stop
          end if
        end if
        ipvb=ipvz
*       now Doppler parameters
        l2=ichar(ipvb(2:2))
        if(l2.lt.ichar('z')) then
          ipvb=ipvb(1:1)//char(l2+1)
         else
          if(ichar(ipvb(1:1)).lt.ichar('z')) then
            l2=ichar(ipvb(1:1))
            ipvb=char(l2+1)//'a'
           else
*           print a warning and give up!
            write(6,*) 'vp_newtflag: b character out of range'
            stop
          end if
        end if
      end if
      return
      end
