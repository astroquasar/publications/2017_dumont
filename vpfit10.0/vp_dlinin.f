      subroutine vp_dlinin(dswk,contv,nwk,nchunk,
     :      dhccont)
*
*     put the lines included so far in the array dswk
*
*     IN:
*     contv	dble(nwk)	input continuum
*     nwk	integer		array length
*     parm	dble ()		parameter array
*     ipind	ch*2 (*)	indicator array
*     nparm	integer		number of parameters set
*     nchunk	integer		chunk number (for wavelengths etc)
*
*     OUT:
*     dswk	dble(nwk)	output continuum
*
      implicit none
      include 'vp_sizes.f'
*
      integer nwk,nchunk
      double precision dswk(nwk),contv(nwk)
      double precision dhccont(*)
*
*     LOCAL:
      integer istart,iend
      integer j
      integer n,nn,nxx
      double precision coln,zedd
*     FUNCTIONS:
      double precision calcn
*     COMMON:
*     ion information:
      integer nlines,nparm
      character*2 elmt(maxnio)
      character*4 ionzn(maxnio)
      double precision parm(maxnpa)
      common/vpc_parry/parm,elmt,ionzn,nlines,nparm
      integer isod,isodh
      character*2 ipind(maxnpa)
      common/vpc_usoind/ipind,isod,isodh
*
*     plot limit defaults
      real fampg
      integer nst,ncn,lc1,lc2
      common/ppam/nst,ncn,lc1,lc2,fampg
*     wavelength shifts, held in:
      integer lassoc
      common/vpc_asschnk/lassoc(maxnch)
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     
*     set up display chunk limits
      if(nchunk.gt.1) then
*       use wavelength ratio information used to select the data
        istart=max0(1,nst)
        iend=min0(ncn,nwk)
        if(istart.ge.iend) then
*         do the lot, and warn the user
          write(6,*) 'Inappropriate channel limits: ',
     :          istart,' -',iend
          write(6,*) 'Using whole array'
          istart=1
          iend=nwk
        end if
       else
*       in absence of other information, use the whole array
        istart=1
        iend=nwk
      end if 
      write(6,*) 'Adding the previous lines to the display'
      do n=1,nlines-1
        nn=noppsys*(n-1)
        coln=calcn(nparm,nn+nppcol,parm,ipind)
*       1=col; 2=z; 3=b as usual
*       check chunk not shifted, which is held in 'b' position
        zedd=parm(nn+nppzed)
        if(lassoc(nchunk).ne.0) then
          nxx=noppsys*(lassoc(nchunk)-1)+nppbval
          zedd=(1.0d0+zedd)*(1.0d0+parm(nxx)/2.99792458d5)-1.0d0
        end if
        if(noppsys.eq.3) then
*         standard three parameters per line
          write(6,'(i6,a1,a2,a4,f8.3,f10.6,f8.2)') 
     :          n,' ',elmt(n),ionzn(n),coln,zedd,parm(nn+nppbval)
          call spvoigt(dswk,contv,istart,iend,coln,zedd,
     :      parm(nn+nppbval),elmt(n),ionzn(n),nchunk,dhccont)
         else
          write(6,*) 'Not yet implemented'
        end if
*       copy for the next line set
        do j=1,nwk
          contv(j)=dswk(j)
        end do
      end do
*     convolve with the instrument profile for this region
      call vp_spread(contv,istart,iend,nchunk, dswk)
      return
      end
