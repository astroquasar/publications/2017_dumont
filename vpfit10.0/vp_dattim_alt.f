      subroutine vp_dattim(chdate,chtime)
*	Old date and time read routine for those where
*	date_and_time routine not yet available
*	Uses Fortran library date() and time() routines
	character*(*) chdate,chtime
	character*9 chzone
	call date(chzone)
	chdate='20'//chzone(8:9)//chzone(4:5)//chzone(1:2)
	chtime=' '
	return
	end
