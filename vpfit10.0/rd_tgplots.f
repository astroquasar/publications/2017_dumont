      subroutine rd_tgplots
*
*     Read in a file, or list of filenames, and plot them in
*     velocity space using a linelist file to get lines and
*     insert tickmarks.
*
*     Parameters: file input, type, test/notest
*
*     Individual file input format is:
*     1: QSO name / QSO datafile / fit file [may be preset]
*     2: Redshift
*     3: velocity range
*     4: Ion, rest wavelength
*     5 etc: as 4.
*     Terminates on blank or EOF
*
*     plot file is <input root>.ps
*
*     Note that plot setup file works on the plots produced here.
*
*
*     all this routine does is generate a list of rdgen commands
*     and then program redirects from main segment. It does set 
*     some variables internally, so tickmarks don't appear if raw
*     redirect is used.
*
      implicit none
      include 'vp_sizes.f'
*
      logical ltest
      character*64 chsx(25),chinfile,chparmfile,chtemp,chqdatfile
      character*64 chqsoname,chvelr,chcaption,chtkionf,chtickfl
      character*8 chzedpl
      character*24 chvv
      character*24 charz
      character*2 chat(25),chatem
      character*4 chion(25),chitem
      integer inunit,inunitd,istat,j,k,lnpar
      integer lcht,lnz,ln,lnch,lncht,lx
      integer nbest,nl,nlx,nalltick,nlin,ncount
      double precision ctem,dv6,dv7,glxtem,glx
      double precision hls,hlsoff,rzed,rzedlow,rzedhigh
      double precision trwv,temp,tcollow
      double precision vls,vlsin,vlt,vlttem,wdiff,ztem
*     plot presets file & variables
      integer nzero
      character*8 chplfile
      integer nvt
      character*64 incvt,cvt(2)
*      real rvt(2)
      double precision dvt(2)
      integer ivt(2)
*     Functions:
      integer lastchpos
*
*     string unscrambling common
      character*132 inchstr
      character*60 cv(24)
      real rv(24)
      integer iv(24)
      integer nv
      common/rd_chwork/inchstr,rv,iv,cv,nv
      double precision dv(24)
      common/vpc_dsepspace/dv
*     line list variables
      double precision zmark
      character*132 chmarkfl
      logical lmarkl,lmarkto
      common/rdc_lmarklv/zmark,chmarkfl,lmarkl,lmarkto
*     plot/print velocity scale?
      logical lvel
      real wcenpg,vellopg,velhipg,zp1refpg
      common/vpc_pgvels/wcenpg,vellopg,velhipg,lvel,zp1refpg
*     observed wavelengths?
      logical lobswav,ltick
      common/rdc_lobswav/lobswav,ltick
*     stack spectra variables
      character*2 atoms(64)
      character*4 ions(64)
      double precision rstwavs(64)
      integer nlins
      common/rdc_stplvar/atoms,ions,rstwavs,nlins
*     atomic data
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm
      integer m
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),m
*
*     make sure atomic data is in
      if(m.le.0) call vp_ewred(0)
*
      nlins=0
      chvv='A''velocity (km/s)'
      inunit=19
*
*     various presets either from file 
*     [environment variable RD_TPLOTS] or defaults:
*
*     Line ID offset relative to its continuum
      hlsoff=0.75d0
*     type of plotfile
      chplfile='ps/vcps'
*     suppress zero lines
      nzero=0
*     line parameter file
      chparmfile=' '
*     QSO data file
      chqdatfile=' '
*     QSO name
      chqsoname=' '
*     velocity limits string
      chvelr=' '
*     redshift string placement (choices: bl=bottom left; 
*     tr=top right; ca in caption.
      chzedpl='bl'
*     column density has to be above this value to get tickmark
*     and be included in the restricted fit
      tcollow=-20.0d0
*     chtkionf is list of ions and reshifts for the tickmarks.
*     this overrides the input file if it is present, otherwise
*     the temporarily generated one is used.
      chtkionf=' '
*
      call getenv('RD_TPLOTS',incvt)
      if(incvt(1:1).ne.' ') then
*       read from named file, and cycle through to end
        lx=lastchpos(incvt)
        open(unit=21,file=incvt(1:lx),status='old',err=886)
 888    read(21,'(a)',end=887) incvt
        write(6,*) incvt(1:24)
        call dsepvar(incvt,2,dvt,ivt,cvt,nvt)
        if(cvt(1)(1:4).eq.'hlso') hlsoff=dvt(2)
        if(cvt(1)(1:4).eq.'chpl') chplfile=cvt(2)(1:8)
        if(cvt(1)(1:4).eq.'zero') nzero=2
        if(cvt(1)(1:4).eq.'xzer') nzero=1
        if(cvt(1)(1:4).eq.'para') chparmfile=cvt(2)
        if(cvt(1)(1:4).eq.'objd'.or.cvt(1)(1:4).eq.'qsod')
     :                            chqdatfile=cvt(2)
        if(cvt(1)(1:4).eq.'objn') chqsoname=cvt(2)
        if(cvt(1)(1:4).eq.'zpla') chzedpl=cvt(2)(1:8)
        if(cvt(1)(1:4).eq.'tcol') tcollow=dvt(2)
        if(cvt(1)(1:4).eq.'tkio') chtkionf=cvt(2)
*       special case of velocity range - a character string with a gap
        if(cvt(1)(1:4).eq.'velr') then
*         search for first blank
          lx=lastchpos(incvt)
          j=1
          do while(j.lt.lx.and.incvt(j:j).ne.' ')
            j=j+1
          end do
*         and from there, first non-blank
          do while(j.lt.lx.and.incvt(j:j).eq.' ')
            j=j+1
          end do
          chvelr=incvt(j:lx)
        end if
        goto 888
 886    write(6,*) 'Failed to open ',incvt(1:lx)
 887    close(unit=21)
      end if
*     end presets section
*
*     fifth parameter on entry is an override tickmark file
*     or a comment flag
      if(cv(5)(1:1).ne.' '.and.cv(5)(1:1).ne.'!') then
        chtkionf=cv(5)
      end if
*     Fourth parameter on entry for test output to screen:
      if(cv(4)(1:4).eq.'test') then
        ltest=.true.
       else
        ltest=.false.
      end if
      open(unit=inunit,file=cv(2),status='old',err=902)
      chinfile=cv(2)
*     Third parameter on entry:
*     include every line with ticks ('all') nalltick=2;
*     all in fit, ticks for z-range ID subset ('tick') nalltick=1
*     include and tick only the lines ID'd in range (anything else)
*             nalltick=0
      if(cv(3)(1:3).eq.'all'.or.cv(3)(1:3).eq.'ALL') then
        nalltick=2
       else
        if(cv(3)(1:3).eq.'tic'.or.cv(3)(1:3).eq.'TIC') then
          nalltick=1
         else
          nalltick=0
        end if
      end if
*     read the tgplot file first line unless ALL superseded
      ln=0
      if(chqsoname(1:1).eq.' '.and.chqdatfile(1:1).eq.' '
     :     .and.chparmfile.eq.' ') then
        read(inunit,'(a)',end=903) inchstr
        call dsepvar(inchstr,5,dv,iv,cv,nv)
        ln=lastchpos(cv(1))
       else
        do j=1,3
          cv(j)=' '
        end do
      end if
      inunitd=19
*     preset takes precedence
      if(chparmfile(1:1).eq.' ') then
        chparmfile=cv(3)
      end if
      lnpar=lastchpos(chparmfile)
      write(6,*) 'Line data from ',chparmfile(1:lnpar)
      if(ln.gt.4) then
        if(cv(1)(ln-3:ln).eq.'.dat') then
*         have a filename list
          chinfile=cv(1)
          inunitd=20
          read(inunitd,'(a)',end=904) inchstr
          call dsepvar(inchstr,5,dv,iv,cv,nv)
        end if
      end if
*     set up output
      open(unit=21,file='tempjunk.rdg',status='unknown')
*     rd <filename> [presets take precedence]
      if(chqdatfile(1:1).eq.' ') then
        chqdatfile=cv(2)
      end if
      ln=lastchpos(chqdatfile)
      write(21,'(a)') 'rd '//chqdatfile(1:ln)
*     normalize the spectrum
      write(21,'(a)') 'dc'
*     add in the lines
      write(21,'(a)') 'gp'
      if(nalltick.ge.1) then
        ln=lastchpos(cv(3))
        write(21,'(a)') cv(3)(1:ln)
       else
*       use subset line list file tempjunk.linlist
        write(21,'(a)') 'tempjunk.linlist'
      end if
*     this is the assumed resolution, and is not always correct.
*     modify program sometime to pick up this quantity from
*     the data file (in rd_gprof, not here).
      write(21,'(a)') '6.7'
*     rescale, leaving room for tick marks
      write(21,'(a)') 'dx 0.8'
*     set up tick mark file
      if(nalltick.ge.2) then
        chmarkfl=cv(3)
        chtickfl=cv(3)
        ln=lastchpos(cv(3))
        lncht=ln
!        write(21,'(a)')'pp f '//cv(3)(1:ln)
       else
        chmarkfl='tempjunk.linlist'
        chtickfl=chmarkfl(1:64)
        ln=16
        lncht=ln
        if(chtkionf(1:2).ne.'  ') then
          lcht=lastchpos(chtkionf)
          chtickfl=chtkionf
!         write(21,'(a)') 'pp f '//chtkionf(1:lqn)
        end if
*       and open the file for writing
        open(unit=35,file='tempjunk.linlist',status='unknown',
     :         err=923)
        open(unit=36,file=chparmfile(1:lnpar),status='old',
     :         err=924)
      end if
*     plot them
      write(21,'(a)') 'pl'
      write(21,'(a)') 'co'
*     Object name preset takes precedence
      if(chqsoname(1:1).eq.' ') then
        chqsoname=cv(1)
      end if
*
*     read reference redshift
      read(inunitd,'(a)') inchstr
      ln=lastchpos(inchstr)
      call dsepvar(inchstr,2,dv,iv,cv,nv)
      rzed=dv(1)
      charz=inchstr(1:24)
      lnz=ln
      write(21,'(a)') 'rz '//inchstr(1:ln)
*
*     velocity range (from, to), unless preset
      if(chvelr(1:1).eq.' ') then
*       need to read velocities
        read(inunitd,'(a)') inchstr
       else
        inchstr=chvelr
      end if
      ln=lastchpos(inchstr)
      call dsepvar(inchstr,2,dv,iv,cv,nv)
*     set the redshift range for inclusion
      rzedlow=(1.0d0+rzed)*(1.0d0+dv(1)/2.99792458d5)-1.0d0
      rzedhigh=(1.0d0+rzed)*(1.0d0+dv(2)/2.99792458d5)-1.0d0
      write(6,*) 'redshift limits',rzedlow,rzedhigh
*     ID string locator
      vlsin=dv(1)
      if(vlsin.lt.0.0d0) then
        vls=vlsin*0.8d0
       else
        vls=vlsin*1.2d0
      end if
      vlt=dv(2)
      write(21,'(a)') 've '//inchstr(1:ln)
*     read in the line data
      nlx=0
      do while (nlx.lt.25)
 877    read(inunitd,'(a)',end=905) inchstr
*       skip blank lines
        if(inchstr(1:10).eq.'          ') goto 877
        nlx=nlx+1
        chsx(nlx)=inchstr(1:64)
      end do
 905  continue
      if(nlx.gt.25) then
        write(6,*) 'Too many lines, using only the first 25'
      end if
      if(nalltick.lt.2) then
*       write line data to the temporary file tempjunk.linlist
        do j=1,nlx
          call dsepvar(chsx(j),2,dv,iv,cv,nv)
          if(cv(1)(2:2).eq.'I'.or.cv(1)(2:2).eq.'V'.or.
     :       cv(1)(2:2).eq.'X') then
            chat(j)=cv(1)(1:1)//' '
            chion(j)=cv(1)(2:5)
           else
            if(cv(1)(3:3).eq.'I'.or.cv(1)(3:3).eq.'V'.or.
     :        cv(1)(3:3).eq.'X') then
              chat(j)=cv(1)(1:2)
              chion(j)=cv(1)(3:6)
             else
              chat(j)=cv(1)(1:2)
              chion(j)=cv(2)(1:4)
            end if
          end if
          if(j.gt.1) then
*           check this ion not in previous part of list
            do k=1,j-1
              if(chat(j).eq.chat(k).and.chion(j).eq.chion(k)) then
*               branch out
                goto 801
              end if
            end do
          end if
*         search through the redshift file for this element
*         in the acceptable redshift range
          rewind(unit=36)
          istat=0
          ncount=0
          do while(istat.eq.0)
            read(36,'(a)',end=801) inchstr
            ncount=ncount+1
            call dsepvar(inchstr,7,dv,iv,cv,nv)
            dv7=dv(7)
            dv6=dv(6)
            if(cv(1)(1:2).ne.'%%'.and.cv(1)(1:1).ne.'!') then
*             could have a redshift, check first for ion match
              if(cv(1)(2:2).eq.'I'.or.cv(1)(2:2).eq.'V'.or.
     :          cv(1)(2:2).eq.'X') then
                chatem=cv(1)(1:1)//' '
                chitem=cv(1)(2:5)
*               cater for tied flags
                if(dv(2).eq.0.0d0) then
                  lx=lastchpos(cv(2))
                  if(lx.gt.1) then
                    chtemp=cv(2)(1:lx-1)
                    call dsepvar(chtemp,1,dv,iv,cv,nv)
                    if(dv(1).eq.0.0d0.and.lx.gt.2) then
                      lx=lx-2
                      chtemp=cv(1)(1:lx)
                      call dsepvar(chtemp,1,dv,iv,cv,nv)
                    end if
                  end if
                  ztem=dv(1)
                 else
                  ztem=dv(2)
                end if
                ctem=dv6
               else
                if(cv(1)(3:3).eq.'I'.or.cv(1)(3:3).eq.'V'.or.
     :            cv(1)(3:3).eq.'X') then
                  chatem=cv(1)(1:2)
                  chitem=cv(1)(3:6)
*                 cater for tied flags
                  if(dv(2).eq.0.0d0) then
                    lx=lastchpos(cv(2))
                    if(lx.gt.1) then
                      chtemp=cv(2)(1:lx-1)
                      call dsepvar(chtemp,1,dv,iv,cv,nv)
                      if(dv(1).eq.0.0d0.and.lx.gt.2) then
                        lx=lx-2
                        chtemp=cv(1)(1:lx)
                        call dsepvar(chtemp,1,dv,iv,cv,nv)
                      end if
                    end if
                    ztem=dv(1)
                   else
                    ztem=dv(2)
                  end if
                  ctem=dv6
                 else
                  chatem=cv(1)(1:2)
                  chitem=cv(2)(1:4)
*                 cater for tied flags
                  if(dv(3).eq.0.0d0) then
                    lx=lastchpos(cv(3))
                    if(lx.gt.1) then
                      chtemp=cv(3)(1:lx-1)
                      call dsepvar(chtemp,1,dv,iv,cv,nv)
                      if(dv(1).eq.0.0d0.and.lx.gt.2) then
                        lx=lx-2
                        chtemp=cv(1)(1:lx)
                        call dsepvar(chtemp,1,dv,iv,cv,nv)
                      end if
                    end if
                    ztem=dv(1)
                   else
                    ztem=dv(3)
                  end if
                  ctem=dv7
                end if
              end if
              if(chatem.eq.chat(j).and.chitem.eq.chion(j)) then
                if(ztem.ge.rzedlow.and.ztem.le.rzedhigh) then
                  if(ctem.gt.tcollow) then
                    lx=lastchpos(inchstr)
                    write(35,'(a)') inchstr(1:lx)
                  end if
                end if
              end if
            end if
          end do
 801      continue
*          write(6,*) ncount
          rewind(unit=36)
        end do
        close(unit=35)
        close(unit=36)
      end if
      nl=lastchpos(chsx(1))
      write(21,'(a)') 'wc '//chsx(1)(1:nl)
*     vertical plot ranges
      glx=dble(nlx)+0.3d0
      write(21,'(a,f6.1)') 'yx ',glx
*     make room for redshift if bottom left
      chcaption=chqsoname
      ln=lastchpos(chqsoname)
      hls=1.0d0-hlsoff
      write(21,'(a)') 'tv '//chtickfl(1:lncht)//
     :    ' 0.85 0.95 '//chsx(1)(1:nl)
      write(21,'(a,2f9.2)') 
     :   'te '//chvv(2:2)//chsx(1)(1:nl)//chvv(2:2),vls,hls
      if(chzedpl(1:2).eq.'bl') then
        glx=-0.1d0*glx
        write(21,'(a,f6.2)') 'ym ',glx
       else
        temp=-0.1d0
        write(21,'(a,f6.2)') 'ym ',temp        
        if(chzedpl(1:2).eq.'ca') then
          chcaption=chvv(2:2)//chqsoname(1:ln)//'   z='//
     :       charz(1:lnz)//chvv(2:2)
        end if
      end if
      lnch=lastchpos(chcaption)
      write(21,'(a)') 'la '//chvv(2:17)//chvv(2:2)//','//chvv(2:2)//
     :     ' '//chvv(2:2)//','//chcaption(1:lnch)
*     suppress x & y zero lines, draw x=0 or draw both
      if(nzero.eq.0) then
        write(21,'(a)') 'zs'
       else
        if(nzero.eq.1) then
          write(21,'(a)') 'zx'
         else
          write(21,'(a)') 'zb'
        end if
      end if
*     blank line ends command stream
      write(21,'(a)') ' '
*     plot device/filename
      if(ltest) then
        write(21,'(a)') '/xw'
       else
*       use filename held in chinfile, with the extension stripped off
*       and replaced by .ps
        nlin=lastchpos(chinfile)
        if(nlin.gt.0) then
          j=nlin
          do while(j.ge.1.and.chinfile(j:j).ne.'.')
            j=j-1
          end do
          if(j.gt.0) then
            lx=lastchpos(chplfile)
            chinfile=chinfile(1:j)//chplfile(1:lx)
           else
            chinfile='/xw'
          end if
        end if
        write(21,'(a)') chinfile
      end if
*     redshift print
      if(chzedpl(1:2).ne.'ca') then
        write(21,'(a)') 'pl'
        write(21,'(a)') 'wc '//chsx(1)(1:nl)
        write(21,'(a)') 'ov 1000'
        if(chzedpl(1:2).eq.'bl') then
          glxtem=0.5d0*glx
          vlttem=vls
         else
          glxtem=dble(nlx)
          vlttem=0.55d0*(vlt-vlsin)
        end if
        write(21,'(a,f7.1,2f9.1)') 
     :  'te '//chvv(2:2)//'z = '//charz(1:lnz)//chvv(2:2),
     :      vlttem,glxtem
        write(21,'(a)') ' '
      end if
*     rest of the lines
      if(nlx.gt.1) then
        do j=2,nlx
          nl=lastchpos(chsx(j))
          hls=dble(j)-hlsoff
          write(21,'(a)') 'pl'
          write(21,'(a)') 'wc '//chsx(j)(1:nl)
          write(21,'(a)') 'tv '//chtickfl(1:lncht)//
     :    ' 0.85 0.95 '//chsx(j)(1:nl)
          write(21,'(a,2f9.2)') 
     :    'te '//chvv(2:2)//chsx(j)(1:nl)//chvv(2:2),vls,hls
          write(21,'(a,i4)') 'ov ',j-1
          write(21,'(a)') ' '
        end do
      end if
*     close plot so can go to next file, but not if you are in test mode!
      if(.not.ltest) then
        write(21,'(a)') 'pc 1,1'
      end if
*     set internal flags so that ticks appear
      lmarkl=.true.
      lmarkto=.true.
*     redshift outside sensible range => tick marks from file
      zmark=-1.0d20
*     but they don't with just the above, so need some rd_plcset presets
      lvel=.true.
      ltick=.true.
      nlins=nlx
      do k=1,nlx
        call dsepvar(chsx(k),3,dv,iv,cv,nv)     
        if(cv(1)(2:2).eq.'I'.or.cv(1)(2:2).eq.'J'.or.cv(1)(2:2).eq.'V'
     :   .or.cv(1)(2:2).eq.'X') then
          atoms(k)=cv(1)(1:1)//' '
          ions(k)=cv(1)(2:5)
          trwv=dv(2)
         else
          atoms(k)=cv(1)(1:2)
          if(cv(1)(3:3).ne.' ') then
            ions(k)=cv(1)(3:6)
            trwv=dv(2)
           else
            ions(k)=cv(2)(1:4)
            trwv=dv(3)
          end if
        end if
*       get closest rest wavelength
        wdiff=1.0d30
        nbest=0
        do j=1,m
          if(lbz(j).eq.atoms(k).and.lzz(j).eq.ions(k).and.
     :     abs(alm(j)-trwv).lt.wdiff) then
            nbest=j
          end if
        end do
        rstwavs(k)=alm(nbest)
      end do
 903  continue
 999  close(unit=21)
*     now redirect input from tempjunk.rdg
      inchstr='< tempjunk.rdg'
      return
*
*     disaster reporting:
 904  write(6,*) 'End file sequence'
      goto 999
 902  write(6,*) 'File not found:',cv(2)(1:24)
      goto 999
 923  write(6,*) 'Failed to open tickmark file'
      goto 999
 924  write(6,*) 'Line parameter file ',chparmfile(1:lnpar),
     :           ' does not exist'
      goto 999
      end


      
      

