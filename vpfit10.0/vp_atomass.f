      subroutine vp_atomass(element,atmass)

      implicit none
      include 'vp_sizes.f'
*
      double precision atmass
      character*2 element
*     complete to atomic number 36; 27/7/89, and if not in 
*     the list requests from the user.
*     Local:
      integer i
*
*     atomic mass table
      character*2 lbm
      double precision amass
      integer mmass
      common/vpc_atmass/lbm(maxats),amass(maxats),mmass
*     search input table first
      atmass=0.0d0
      if(mmass.gt.0) then
        i=1
        do while(element.ne.lbm(i).and.i.le.mmass)
          i=i+1
        end do
        if(i.le.mmass) then
          atmass=amass(i)
*         write(6,*) element,atmass
          return
        end if
      end if
*     if that has failed, try built-in values:
*     the most common ones first
      if(atmass.le.0.1d0) then
*	write(6,*) ' >> Mass not read for ',element
        if(element.eq.'H '.or.element.eq.'<<'.or.
     :      element.eq.'>>'.or.element.eq.'<>'.or.
     :      element.eq.'__'.or.element.eq.'MR'.or.
     :      element.eq.'??'.or.element.eq.'AD')then
          atmass=1.0d0
          return
        end if
        if(element.eq.'D '.or.element.eq.'H2') then
          atmass=2.0d0
          return
        end if
        if(element.eq.'C ') then
          atmass=12.011d0
          return
        end if
        if(element.eq.'N ') then
          atmass=14.0067d0
          return
        end if
        if(element.eq.'O ') then
          atmass=15.994d0
          return
        end if
        if(element.eq.'Na')then
          atmass=22.98977d0
          return
        end if
        if(element.eq.'Mg')then
          atmass=24.305d0
          return
        end if
        if(element.eq.'Al')then
          atmass=26.98154d0
          return
        end if
        if(element.eq.'Si')then
          atmass=28.0855d0
          return
        end if
        if(element.eq.'Ca')then
          atmass=40.08d0
          return
        end if
        if(element.eq.'Fe')then
          atmass=55.847d0
          return
        end if
*	now the more exotic ones
        if(element.eq.'He')then
          atmass=4.00260d0
          return
        end if
        if(element.eq.'Li')then
          atmass=6.94d0
          return
        end if
        if(element.eq.'Be')then
          atmass=9.01218d0
          return
        end if
        if(element.eq.'B ')then
          atmass=10.81d0
          return
        end if
        if(element.eq.'F ')then
          atmass=18.998403d0
          return
        end if
        if(element.eq.'Ne')then
          atmass=20.17d0
          return
        end if
        if(element.eq.'P ')then
          atmass=30.97376d0
          return
        end if
        if(element.eq.'S ')then
          atmass=32.06d0
          return
        end if
        if(element.eq.'Cl')then
          atmass=35.453d0
          return
        end if
        if(element.eq.'Ar')then
          atmass=39.948d0
          return
        end if
        if(element.eq.'K ')then
          atmass=39.0983d0
          return
        end if
        if(element.eq.'Sc')then
          atmass=44.9559d0
          return
        end if
        if(element.eq.'Ti')then
          atmass=47.9d0
          return
        end if
        if(element.eq.'V ')then
          atmass=50.941d0
          return
        end if
        if(element.eq.'Cr')then
          atmass=51.996d0
          return
        end if
        if(element.eq.'Mn')then
          atmass=54.938d0
          return
        end if
        if(element.eq.'Co')then
          atmass=58.9332d0
          return
        end if
        if(element.eq.'Ni')then
          atmass=58.71d0
          return
        end if
        if(element.eq.'Cu')then
          atmass=63.546d0
          return
        end if
        if(element.eq.'Zn')then
          atmass=65.38d0
          return
        end if
        if(element.eq.'Ga')then
          atmass=69.72d0
          return
        end if
        if(element.eq.'Ge')then
          atmass=72.59d0
          return
        end if
        if(element.eq.'As')then
          atmass=74.9216d0
          return
        end if
        if(element.eq.'Se')then
          atmass=78.96d0
          return
        end if
        if(element.eq.'Br')then
          atmass=79.904d0
          return
        end if
        if(element.eq.'Kr')then
          atmass=83.8d0
          return
        end if
*       it's a weirdo, get it from the user
        write(6,101)element
 101    format(1x,' Atomic mass of ',a2,' not in table.
     :  Enter it please:')
        read(5,*) atmass
      end if
      return
      end
