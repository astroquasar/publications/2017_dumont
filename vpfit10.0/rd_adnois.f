      subroutine rd_adnois(da,de,ca,nct)
*     commented out because rand needs to be declared for some compilers
*     implicit none
      include 'vp_sizes.f'
*     
      integer nct
      double precision da(maxfis),de(maxfis),ca(maxfis)
*
      logical logd
      integer i
      double precision cx,sig,sig2
*     Functions:
      double precision rd_gran
*     various common things:
      integer nv
      character*132 inchstr
      character*60 cv(24)
      real rv(24)
      integer iv(24)
      common/rd_chwork/inchstr,rv,iv,cv,nv
      double precision drv(24)
      common/rdc_dchwork/drv
*     input streams
      integer inputc
      common/rdc_inputc/inputc
*
*     called with 'data' flag?
      if(cv(2)(1:2).eq.'da'.or.cv(2)(1:2).eq.'DA') then
        logd=.true.
       else
        logd=.false.
      end if
      call rd_setran
      write(6,*) 's/n relative to peak continuum? <CR> = infinity'
      write(6,*) '(OR c1,c2 gives sigma=sqrt(cont*c1**2 + c2**2))'
      read(inputc,'(a)',end=44) inchstr
      call dsepvar(inchstr,2,drv,iv,cv,nv)
      if(drv(2).ne.0.0d0) then
        do i=1,nct
          sig2=ca(i)*drv(1)**2 + drv(2)**2
          sig=sqrt(sig2)
          if(logd) then
            da(i)=da(i)+rd_gran(sig,0.0d0)
            de(i)=sig2+de(i)
           else
            da(i)=ca(i)+rd_gran(sig,0.0d0)
            de(i)=sig2
          end if
        end do
       else
        if(drv(1).gt.0.0d0) then
          cx=0.0d0
          do i=1,nct
            if(cx.lt.ca(i)) cx=ca(i)
          end do
          sig=cx/drv(1)
          if(logd) then
            do i=1,nct
              da(i)=da(i)+rd_gran(sig,0.0d0)
              de(i)=sig*sig+de(i)
            end do
           else
            do i=1,nct
              da(i)=ca(i)+rd_gran(sig,0.0d0)
              de(i)=sig*sig
            end do
          end if
          write(6,*) 'work arrays replaced with synthetic spectrum'
         else
          if(logd) then
            write(6,*) 'data replaced with continuum + data noise'
            do i=1,nct
              if(de(i).gt.0.0d0) then
                sig=sqrt(de(i))
                da(i)=ca(i)+rd_gran(sig,0.0d0)
               else
                da(i)=ca(i)
              end if
            end do
           else
            write(6,*) ' Array copied directly'
            do i=1,nct
              da(i)=ca(i)
              de(i)=0.0d0
            end do
          end if
        end if
      end if
 44   return
      end
      subroutine rd_setran
*     rand needs declaration sometimes
*     implicit none
      integer ir,time,it
*     various common things
      integer nv
      character*132 inchstr
      character*60 cv(24)
      real rv(24)
      integer iv(24)
      common/rd_chwork/inchstr,rv,iv,cv,nv
      double precision drv(24)
      common/rdc_dchwork/drv
      common/ransdf/ir
*     input streams
      integer inputc
      common/rdc_inputc/inputc
*
      write(6,*) 'random number seed'
      read(inputc,'(a)',end=44) inchstr
      call dsepvar(inchstr,1,drv,iv,cv,nv)
      ir=iv(1)
      ir=2*ir+1
      if(ir.gt.2000) return
      ir=time()
      it=(ir/1000000)*1000000
      ir=ir-it
      if(ir.lt.0) ir=-ir
      if(ir.le.1) ir=3663
      if((ir/2)*2.eq.ir) then
        ir=ir+1
      end if
      write(6,*) ir
 44   drv(1)=rand(ir)
      return
      end
      double precision function rd_gran(sig,xmu)
*     declare rand or comment out 'implicit none'
*      implicit none
      double precision sig,xmu
*
      double precision r1,r2
      integer ir
      common/ransdf/ir
      r1=rand(0)
      r2=rand(0)
      rd_gran=sqrt(-2.0d0*log(r1))*sig*sin(6.2831853d0*r2) + xmu
      return
      end
