      subroutine rd_dmanip(da,de,drms,ca,nct,chact)
*     perform arithmetic operations on data
      implicit none
      integer nct
      double precision da(nct),de(nct),drms(nct),ca(nct)
      character*(*) chact
*
      integer j,k,l,len
      character*1 dto,df1,dact,df2,chd
*
*     Functions
      integer lastchpos
      if(chact(1:1).eq.'?') then
*       help then prompt
        chact(1:1)=' '
        write(6,*) 'Options are'
        write(6,*) 'd=d-c'
        write(6,*) 'd=d+c'
        write(6,*) 'd=d*c'
        write(6,*) 'd=e/c'
        write(6,*) 'd=r/c'
      end if
 1    if(chact(1:1).eq.' ') then
*       prompt for action
        write(6,*) ' what = what?'
        read(5,'(a)') chact
      end if
      len=lastchpos(chact)
      j=1
      do while(j.le.len.and.chact(j:j).ne.'=')
        j=j+1
      end do
      if(j.ge.len) goto 1
      dto=chact(1:1)
      l=j
      do while(l.le.len.and.chact(l:l).ne.'+'.and.chact(l:l).ne.'-'
     1     .and.chact.ne.'/')
        l=l+1
      end do
      if(l-j.lt.2.or.len-l.lt.1) goto 1
      dact=chact(l:l)
      df1=chact(j+1:j+1)
      df2=chact(l+1:l+1)
      chd=' '
      if(dto.eq.'d') then
*       write to data array
        if(df1.eq.'d') then
*         from data array
          if(df2.eq.'c') then
*           and continuum array
            if(dact.eq.'-') then
*             subtract
              do k=1,nct
                da(k)=da(k)-ca(k)
              end do
              chd=dact
            end if
            if(dact.eq.'+') then
*             add
              do k=1,nct
                da(k)=da(k)+ca(k)
              end do
              chd=dact
            end if
            if(dact.eq.'*') then
*             multiply
              do k=1,nct
                da(k)=da(k)*ca(k)
              end do
            end if
          end if
          goto 301
        end if
        if(df1.eq.'e') then
*         only option is divide by continuum
          do k=1,nct
            da(k)=sqrt(de(k))/ca(k)
          end do
          goto 301
        end if
        if(df1.eq.'r') then
*         only option is divide by continuum
          do k=1,nct
            da(k)=sqrt(drms(k))/ca(k)
          end do
        end if
*
 301    continue
      end if
      if(chd.eq.' ') then
        write(6,*) 'No action taken'
      end if
      return
      end


