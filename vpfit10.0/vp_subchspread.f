      subroutine vp_subchspread(flx,nlw,nw,ichunk,wvsubd,npsubch, flb)
*
      implicit none
      include 'vp_sizes.f'
*
*     Convolve array with Gaussian (or other) profile
*     
*     IN:
*     flx	r*8 array	data array for chunk
*     nlw	int		start channel for result
*     nw	int		end channel for result
*     ichunk	int		chunk number (for binned profile)
*
*     OUT:
*     flb	r*8 array	flx convolved with instrument profile
*     
*     COMMON:
*     Binned profile parameters, /vpc_profwts/, used if npinst.gt.0
*
*
      integer nlw,nw,ichunk
      double precision flx(*),flb(*)
      integer npsubch
      double precision wvsubd(npsubch)
*
*     LOCAL:
      integer i,j,jji,k,nd,nlo,nup
      double precision bwhvard,df,fsigd,sigd,xdb
*     data value variables
      double precision s,sx,y,sm,smn
*     FUNCTIONS:
      double precision vpf_dvresn,dexpf
*
*     chunk wavelengths, set up for this chunk outside this routine
*     now arguments
*      integer npsubch
*      double precision wvsubd(maxscs)
*      common/vpc_wvsubd/wvsubd,npsubch
*     binned profile
      character*2 chprof
      double precision pfinst
      double precision wfinstd
      integer npinst,npin2
      common/vpc_profwts/wfinstd(maxipp,maxnch),pfinst(maxipp,maxnch),
     :        npinst(maxnch),npin2(maxnch),chprof(maxnch)
*     chunk subdivision variables
      integer nminsubd,nmaxsubd
      double precision bindop
      common/vpc_vpsubd/bindop,nminsubd,nmaxsubd
*
*     smoothed data in array flx -> flb(nlw,nw)
*
*     use binned profile if npinst.gt.0
      if(npinst(ichunk).gt.0.and.chprof(ichunk)(1:2).ne.'wt') then
        if(nminsubd.ne.nmaxsubd) then
          write(6,*) 'Pixel profile needs constant pixel subdivision'
          write(6,*) 'nsubmin=',nminsubd,'   nsubmax=',nmaxsubd
          write(6,*) 'In VPFSETUP file set nsubmin & nsubmax'
          write(6,*) 'both to be the number of subpixels per'
          write(6,*) 'data pixel used in specifying the instrument'
          write(6,*) 'profile'
          write(6,*) '    ....   PROGRAM TERMINATES'
          stop
        end if
        do i=nlw,nw
          sm=0.0d0
          smn=0.0d0
          do j=1,npinst(ichunk)
            k=i+j-npin2(ichunk)
            if(k.ge.1.and.k.le.nw) then
              sm=sm+flx(k)*pfinst(j,ichunk)
              smn=smn+pfinst(j,ichunk)
            end if
          end do
          if(smn.gt.0.0d0) then
            flb(i)=sm/smn
           else
*           raw value if weight is zero
            flb(i)=flx(i)
          end if
        end do
       else
*       no pixel instrument profile, so use continuous function
*	gaussian smoothing  -set the local sigma
        xdb=wvsubd((nlw+nw)/2)
*       determining local sigma now from function vpf_dvresn
        sigd=vpf_dvresn(xdb,ichunk)
        if(sigd.le.0.0d0) then
*         don't smooth
          do i=nlw,nw
            flb(i)=flx(i)
          end do
         else
          do i=nlw,nw
*           smooth using this sigma
            xdb=wvsubd(i) 
            jji=max(i,nlw+1)
            jji=min(jji,nw-1)
            bwhvard=0.5d0*(wvsubd(jji+1)-wvsubd(jji-1))
            fsigd=6.0d0*sigd
            df=fsigd*xdb/bwhvard
            nd=int(df)
            if(nd.le.0) nd=1
            nlo=i-nd
            nup=i+nd
            if(nlo.lt.1) nlo=1
            if(nup.gt.nw) nup=nw
            sx=0.0d0
            s=0.0d0
            do j=nlo,nup
              y=(wvsubd(j)/xdb-1.0d0)/sigd
              y=dexpf(-y*y*0.5d0)
              sx=sx+y
              s=s+flx(j)*y
            end do
            if(sx.gt.0.0d0) then
              flb(i)=s/sx
             else
              flb(i)=flx(i)
            end if
          end do
        end if
      end if
      return
      end
