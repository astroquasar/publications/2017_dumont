      subroutine rd_ulimx(da,de,ca,cad,ds,nct,ichunk)
* was subroutine rd_ulimse(da,de,drms,ca,cad,re,ds,nct,ichunk)
*
*     establish upper limit to a column density for a given
*     redshift, bval for selected ion, given ion
*     includes only data below fit+xn*sigma, 
*     for lines +/- xb*sqrt(bvalue^2+bresn^2)
*     and with min number of pixels nminf, minimizing the normalized chi^2
*     (based on error, not rms, array) for these included lines. 
*     Sub-pixellation is used if necessary.
*     The data must be normalized to unit continuum. 
*
      implicit none
      include 'vp_sizes.f'
*
*     arguments: all in or workspace
*     da-data, de-sigma^2, ca-continuum, cad-work continuum, re-work, ds-work
      integer nct,ichunk
      double precision da(nct),de(nct),ca(nct),cad(nct)
      double precision ds(nct)
*     double precision drms(nct),re(nct)
*
*     local variables
      logical lcmon
      integer i,j,ii,jj,jnew,k1,k2,ndf,ndx,klow,khiw,numbv
      integer jis,jjs,jks,ndlen
      integer nminf,nrest,ifail,nclo,nchi,jjcmax
      integer ncmin,ncmax,ntemp,nextd,nreg
      integer nset,np
      integer k,kloc,kjb,nsubd,npwv1,nexts
      double precision brange,dddd,colmmax,tempx
      character*2 elvbref,elvb
      character*4 levbref,levb
      double precision btemp,btemplog
      double precision bval,cval,zval,cvalh,dbv,bvalref,btherm
      double precision ebvalref,bvmin,bwidth
      double precision ddmax,ddmaxh,ddtemp,chisq
      double precision bvmax,dchs,dtemp,temp,ddscl
      double precision chisqn,chisqnmin,pchisq
      double precision atmref,atmvb
      double precision qchs,pqchs,vperchx,vpctemp,wvtemp
      double precision wlodd,whidd,dwtempx
      double precision wvrest(120)
      integer nchlob(120),nchhib(120),nchlop(120),nchhip(120)
      double precision wvlob(120),wvhib(120)
      double precision diffn(10000)
      integer ipn(10000)
      double precision bvh(1000),cvh(1000),chsnh(1000)
      integer ndfh(1000)
*     functions
      integer lastchpos
      double precision pd_pchi2
      double precision vp_wval,wval
*
*     common variables
*
*     character string handling & unpacking
      character*132 inchstr
      character*60 cv(24)
      real rv(24)
      integer iv(24)
      integer nv
      common/rd_chwork/inchstr,rv,iv,cv,nv
      double precision dv(24)
      common/vpc_dsepspace/dv
*     substep workspace
      double precision casub(maxsal),cadsub(maxsal),resub(maxsal)
      common/rdc_casub/casub,cadsub,resub
*     subdivided chunk wavelengths
      integer nsublen
      double precision wvsubs(maxsal)
      common/rdc_wvsubs/wvsubs,nsublen
*     input streams
      integer inputc
      common/rdc_inputc/inputc
*     atomic data
      integer m
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),m
      integer nlines,nn
      character*2 elem(maxnio)
      character*4 ionz(maxnio)
      double precision param(maxnpa)
      common/vpc_parry/param,elem,ionz,nlines,nn
*     parameter positions and numbers
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     tied flags
      character*2 ipind(maxnpa)
      integer isod,isodh
      common/vpc_usoind/ipind,isod,isodh
*     resolution
      integer nwres(maxnch)
      double precision swres(maxpoc,maxnch)
      common/vpc_sigwres/swres,nwres
*     bin subdivision
      integer nminsubd,nmaxsubd
      double precision bindop
      common/vpc_vpsubd/bindop,nminsubd,nmaxsubd
*     wavelength stuff
      double precision dnshft2(maxnch)
      common/vpc_shft2/dnshft2
*     diagnostic print control
      logical verbose
      common/vp_sysout/verbose
*     upper limit defaults, updated by routines which use them
      character*4 ctypeul
      character*60 chwv1
      logical lcminul
      integer nminfd,nminfdef
      double precision xnul,xbul,pchslim,dfwhmkms,bvalmins 
      double precision wloddr,whiddr
      common/rdc_ulvsets/xnul,xbul,pchslim,dfwhmkms,bvalmins,
     :     wloddr,whiddr,nminfd,nminfdef,ctypeul,chwv1,lcminul
*     upper limit results
      character*6 ionul
      double precision zvalul,bvalul,cvalul
      common/rdc_ulrparm/zvalul,bvalul,cvalul,ionul
*
*     tied parameters meaningless here, so blank off
      do k=1,maxnpa
        ipind(k)='  '
      end do
*     Don't cater for more than three parameters here,
*     but make sure higher ones not set to silly values
      if(noppsys.ge.4) then
        do k=4,noppsys
          param(k)=0.0d0
        end do
      end if
*     copy continuum to workspace
      do i=1,nct
        ds(i)=ca(i)
      end do
*     extension over fit region for profile generation, 
*     channels in each direction
      nextd=25
      write(6,'(a)') 'Unit continuum assumed'
 909  write(6,'(a)') 
     :     'xn,xb,nminf,pchslim,ctype,vres(km/s),nmdef,bmin'
      write(6,'(a,2f5.1,i3,f7.3,a,f7.2,i3,f7.2,a)') '[',xnul,xbul,
     :     nminfd,pchslim,' '//ctypeul,dfwhmkms,nminfdef,bvalmins,']'
      read(inputc,'(a)') inchstr
      if(inchstr(1:1).eq.'?') then
        write(6,*) 'Determine an upper limit to the column density for'
        write(6,*) 'an ion given another ion, its b-value and redshift.'
        write(6,*) 'The data MUST be normalized to unit continuum, and'
        write(6,*) 'the error estimate represent the RMS fluctuations'
        write(6,*) 'in the data. Does it by comparing a fit profile'
        write(6,*) 'with the data, and including for chi^2 only'
        write(6,*) 'regions where the fit is below the data or'
        write(6,*) 'less than xn*sigma above it.'
        write(6,*) 'First parameters are:' 
        write(6,*) 'xn *sigma above fit for inclusion in statistic;' 
        write(6,*) 'half-width in bvals & resolution for inclusion;'
        write(6,*) 'default min channels for inclusion;'
        write(6,*) 'stopping probability (0.16 is one-sided 1-sigma);'
        write(6,*) 'flag to search for the lowest column density'
        write(6,*) 'consistent with the noise independent of the data'
        write(6,*) 'if no line is detected'
        write(6,*) '(so high continuum is not a factor) - '
        write(6,*) 'to turn this off the parameter ctype -> "nocmin";'
        write(6,*) 'instrument resolution FWHM in km/s;'
        write(6,*) 'forced min channels for statistic;'
        write(6,*) 'minimum b-value in search.' 
        goto 909
      end if
      call dsepvar(inchstr,8,dv,iv,cv,nv)
*     set values as appropriate
      if(dv(1).gt.0.0d0) then
        xnul=dv(1)
      end if
      if(dv(2).gt.0.0d0) then
        xbul=dv(2)
      end if
      if(iv(3).gt.0) then
*       minimal lower limit, else pairs with one saturated component
*       are wildly overestimated.
        nminfd=iv(3)
      end if
      if(cv(4)(1:1).ne.' ') then
        pchslim=dv(4)
      end if
      write(6,'(a,2f6.2,i4,f7.3)') 'Using:',xnul,xbul,nminfd,pchslim
      if(cv(5)(1:1).ne.' ') then
        if(cv(5)(1:1).eq.'n'.or.cv(5)(1:1).eq.'N') then
          lcminul=.false.
          ctypeul='nocm'
          write(6,*) 'with noise-based lower limit turned off'
         else
          lcminul=.true.
          ctypeul='cmin'
          write(6,*) 'noise-based lower limit on'
        end if
      end if
      if(dv(6).gt.0.0d0) then
        dfwhmkms=dv(6)
      end if
      if(iv(7).gt.0) then
        nminfdef=iv(7)
      end if
      if(dv(8).gt.0.0d0) then
        bvalmins=dv(8)
      end if
      write(6,'(a,f6.2,a)') 'FWHM = ',dfwhmkms,' km/s'
*     set resolution values
      swres(1,ichunk)=0.0d0
      swres(2,ichunk)=dfwhmkms/7.05957d5
      nwres(ichunk)=2
*     Low start column density for search
      cvalh=11.0d0
      write(6,*) 'Col density start value [',cvalh,']'
      read(inputc,'(a)') inchstr
      call dsepvar(inchstr,2,dv,iv,cv,nv)
      if(dv(1).gt.1.0d0.and.nv.ge.1) then
        cvalh=dv(1)
      end if
*     undocumented debug check - exit early
      lcmon=.false.
      if(cv(2)(1:4).eq.'cmon') then
        lcmon=.true.
      end if
*     restricted data range?
      if(whiddr.gt.1.0d24) then
        write(6,*) 'Observed wavelength range (from,to)? [CR=all]'
       else
        write(6,*) 'Observed wavelength range (from,to)?' 
        write(6,'(a,2f10.2,a)') '            [',wloddr,whiddr,']'
      end if
      read(inputc,'(a)') inchstr
      call dsepvar(inchstr,2,dv,iv,cv,nv)
      if(cv(1)(1:1).ne.' ') wloddr=dv(1)
      if(dv(2).gt.wloddr) whiddr=dv(2)
      if(whiddr.le.wloddr) goto 601      
*     Parameters for the search
 902  write(6,*) 'Ion,bval,(err),redshift,newion,(lambda,lambda...)'
      write(6,*) '                               (or filename, column)'
      read(inputc,'(a)') inchstr
      call dsepvar(inchstr,15,dv,iv,cv,nv)
      call vp_chsation(cv(1),cv(2),elvbref,levbref,nexts)
      if(nexts.gt.1) then
        write(6,*) 'No space between atom and ion please'
        goto 902
      end if
      if(elvbref(1:1).eq.' ') then
        write(6,*) 'Reference ion not given'
        goto 902
      end if
      bvalref=dv(2)
      if(dv(4).eq.0.0d0.and.cv(4)(1:1).ne.'0') then
*       no error on reference Doppler parameter
        zval=dv(3)
        ebvalref=0.0d0
        jnew=4
       else
*       third parameter is bval error estimate
        write(6,*) ' .. third parameter taken as bval error'
        ebvalref=dv(3)
        zval=dv(4)
        jnew=5
      end if
      param(nppzed)=zval
      call vp_chsation(cv(jnew),cv(jnew+1),elvb,levb,nexts)
      if(nexts.gt.1) then
        write(6,*) 'No space between new atom and ion please'
        goto 902
      end if
      if(elvb(1:1).eq.' ') then
        write(6,*) 'New ion not given'
        goto 902
      end if
*     set new element for vp_spvoigte
      elem(1)=elvb
      ionz(1)=levb
*     get rest wavelengths
*     could be over restricted range - convert observed limits to rest
      wlodd=wloddr/(1.0d0+zval)
      whidd=whiddr/(1.0d0+zval)
      if(nv.gt.jnew) then
*       character string in case it is a filename
        if(cv(jnew+1)(1:1).ne.' '.and.iv(jnew+1).le.0) then
*         is a filename
          chwv1=cv(jnew+1)
          npwv1=iv(jnew+2)
          nrest=lastchpos(chwv1)
*         open the file, and read it in, checking the z range vs 
*         data range
          if(npwv1.gt.0.and.nrest.gt.0) then
            open(unit=18,file=chwv1(1:nrest),status='old',err=961)
           else
            write(6,*) 'File/wavelength position not given'
            goto 961
          end if
*         now read in the line list
          nrest=0
919       read(18,'(a)',end=918) inchstr
          call dsepvar(inchstr,npwv1,dv,iv,cv,nv)
          dwtempx=dv(npwv1)
          if(dwtempx.gt.1.0d-3.and.dwtempx.ge.wlodd.and.
     :          dwtempx.le.whidd) then
            nrest=nrest+1
            wvrest(nrest)=dwtempx
          end if
          if(nrest.lt.120) then
            goto 919
          end if
          write(6,*) 'Max number of lines reached'
918       close(unit=18)
          write(6,'(a,i4,a)') 'Using ',nrest,' wavelengths from file'
         else
*         load wavelength list
          do j=jnew+1,nv
            wvrest(j-jnew)=dv(j)
          end do
          nrest=nv-jnew
        end if
       else
        nrest=0
      end if
*     get atomic masses
      if(m.le.0) call vp_ewred(0)
      call vp_atomass(elvbref,atmref)
      call vp_atomass(elvb,atmvb)
*     convert approximate wavelengths to accurate ones
      if(nrest.gt.0) then
*       convert to accurate table wavelengths
        do j=1,nrest
          ddmaxh=1.0d20
          ddmax=1.0d21
          do i=1,m
            if(elvb.eq.lbz(i).and.levb.eq.lzz(i)) then
              ddmax=abs(alm(i)-wvrest(j))
              if(ddmax.lt.ddmaxh) then
                ddmaxh=ddmax
                ddtemp=alm(i)
              end if
            end if
          end do
          wvrest(j)=ddtemp
        end do
       else
*       choose the first two in the atomic data file list!
        do i=1,m
          if(elvb.eq.lbz(i).and.levb.eq.lzz(i)) then
            nrest=nrest+1
            wvrest(nrest)=alm(i)
          end if
          if(nrest.ge.2) goto 901
        end do
 901    continue
        if(nrest.eq.1) then
          write(6,*) 'Using ',wvrest(1)
         else
          write(6,*) 'Using ',wvrest(1),wvrest(2)
        end if
      end if
*     thermal Doppler parameter
      if(atmref.lt.atmvb) then
*       btherm is min, btemp is max
        btherm=max(bvalref-ebvalref,bvalmins)*sqrt(atmref/atmvb)
        btemp=bvalref+ebvalref
       else
*       btherm is min, btemp is max
        btherm=(bvalref+ebvalref)*sqrt(atmref/atmvb)
        btemp=max(bvalref-ebvalref,bvalmins)
      end if
*     if btemp=bvalref then is no error case
      brange=btherm-btemp
      btemplog=log(btemp)
*     use logarimic steps to avoid program spending ages with similar
*     b-values
      dbv=log(btherm)-btemplog
*     set number of trial b-values
      if(abs(dbv).lt.1.0d-10) then
*       only one b-value needed
        numbv=1
        dbv=0.0d0
       else
        if(abs(dbv).lt.0.175d0) then
          numbv=3
         else
          if(abs(dbv).le.0.275d0) then
            numbv=5
           else
*           Use 7 log steps to cover the range
            numbv=7
          end if
        end if
        dbv=dbv/dble(numbv-1)
      end if
*     set number of substeps for profile, and use the same for each
*     rather than determining on the fly
      bvmin=min(btherm,btemp)
      bvmax=max(btherm,btemp)
      bwidth=sqrt(bvmax*bvmax+0.36d0*dfwhmkms*dfwhmkms)
      vperchx=0.0d0
      do i=1,nrest
*       set maximum velocity increment per channel
        wvtemp=(1.0d0+zval)*wvrest(i)
        call vp_chanwav(wvtemp,ddtemp,5.0d-2,20,ichunk)
        ntemp=max(int(ddtemp),2)
        ntemp=min(ntemp,nct)
        ddtemp=dble(ntemp)
        vpctemp=(2.99792458d5/wvtemp)*
     :   (vp_wval(ddtemp,ichunk)-vp_wval(ddtemp-1.0d0,ichunk))
        vperchx=max(vperchx,vpctemp)
*       also set profile limits to be maximum range (but determine
*       chi-squared limits on the fly)
        vpctemp=(1.0d0-xbul*bwidth/2.99792458d5)*wvtemp
        call vp_chanwav(vpctemp,ddtemp,5.0d-2,20,ichunk)
*       regions for profile generation wider by nextd in both directions
        nchlop(i)=max(1,int(ddtemp-nextd))
        vpctemp=(1.0d0+xbul*bwidth/2.99792458d5)*wvtemp
        call vp_chanwav(vpctemp,ddtemp,5.0d-2,20,ichunk)
        nchhip(i)=min(int(ddtemp)+nextd,nct)
      end do
*     sort out sub-pixellation
      nsubd=int(bindop*vperchx/bvmin+0.99999d0)
      if(nsubd.lt.nminsubd) nsubd=nminsubd
      nsubd=min(nsubd,nmaxsubd)
      write(6,*) 'Substeps:',nsubd
*     deal with any extended profile region overlaps
      do i=1,nrest-1
*       negative value = discarded
        if(nchlop(i).gt.0) then
          do j=i+1,nrest
            if(nchlop(j).le.nchhip(i).and.nchlop(j).ge.nchlop(i)) then
*             lower point of j within i, so copy upper j to
*             i (if it is larger), and replace the j's by -1
              if(nchhip(j).gt.nchhip(i)) then
                nchhip(i)=nchhip(j)
              end if
              nchhip(j)=-1
              nchlop(j)=-1
            end if
            if(nchhip(j).le.nchhip(i).and.nchhip(j).ge.nchlop(i)) then
*             higher point of j within i, so copy lower j to
*             i (if it is smaller), and replace the j's by -1
              if(nchlop(j).lt.nchlop(i)) then
                nchlop(i)=nchlop(j)
              end if
              nchhip(j)=-1
              nchlop(j)=-1
            end if
          end do
        end if
      end do
*     get rid of those flagged as redundant
      nreg=nrest
      i=1
      do while(i.le.nreg)
        if(nchlop(i).le.0) then
          if(i.lt.nreg) then
            do j=i+1,nreg
              nchhip(j-1)=nchhip(j)
              nchlop(j-1)=nchlop(j)
            end do
          end if
          nreg=nreg-1
        end if
        i=i+1
      end do
      write(6,*) nrest,' lines in ',nreg,' spectral regions'
*     now have a set of profile input regions
*     parameters for vp_spvoigte
      nset=1
      np=noppsys
*     cycle through trial b-values
      do jj=1,numbv
        bval=exp(btemplog+dble(jj-1)*dbv)
        param(nppbval)=bval
*       effective b-width is bval & resolution in quadrature
        bwidth=sqrt(bval*bval+0.36d0*dfwhmkms*dfwhmkms)
*       set limits for the chi-squared determination
        nminf=0
*       and for the range for smoothing
        ncmin=8000000
        ncmax=-8000000
*       set up the comparison region limits
        do i=1,nrest
          ddtemp=(1.0d0+zval)*wvrest(i)
          wvlob(i)=(1.0d0-xbul*bwidth/2.99792458d5)*ddtemp
          wvhib(i)=(1.0d0+xbul*bwidth/2.99792458d5)*ddtemp
          call vp_chanwav(wvlob(i),ddtemp,5.0d-2,20,ichunk)
          nchlob(i)=int(ddtemp)-1
          call vp_chanwav(wvhib(i),ddtemp,5.0d-2,20,ichunk)
          nchhib(i)=int(ddtemp)+1
*          write(6,*) i,nchlob(i),nchhib(i)
*         max channels available check
          if(nchhib(i).le.nct.and.nchlob(i).ge.1) then
            nminf=nchhib(i)-nchlob(i)+1+nminf
          end if
          ncmin=min(ncmin,nchlob(i))
          ncmax=max(ncmax,nchhib(i))
        end do
        nminf=min(nminfd,nminf/2)
        if(nminf.le.nminfdef) then
          write(6,'(a,i3,a)') 'Statistic may be over ',nminfdef,
     :                 ' channels only'
          nminf=nminfdef
        end if
*       for the given b-value, step up in column density to minimum affective
*       chi-squared
        chisqnmin=1.0d30
        chisqn=1.0d29
        pqchs=1.0d20
        pchisq=1.0d29
        cval=cvalh
        ndf=0
        do while ((chisqn.lt.chisqnmin.or.pchisq.gt.pchslim.or.
     :    pqchs.gt.pchslim).and.cval.lt.20.0d0)
          pqchs=0.0d0
          chisqnmin=chisqn
*         copy previous values to holding space
          bvh(jj)=bval
          cvh(jj)=cval
          chsnh(jj)=chisqn
          ndfh(jj)=ndf
          if(cval.le.13.5d0) then
            cval=cval+0.05d0
           else
            if(cval.le.14.5d0) then
              cval=cval+0.1d0
             else
              cval=cval+0.2d0
            end if
          end if
*         put the column density in
          param(nppcol)=cval
*        
          do j=1,nreg
*           make appropriate subchunks, and put lines in
*           current length
            nsublen=(nchhip(j)-nchlop(j))*nsubd
*           set up substep values if nsubd>1
            if(nsubd.gt.1) then
              do k=1,nsublen
                casub(k)=1.0d0
                cadsub(k)=1.0d0
                resub(k)=1.0d0
                kjb=(k-1)/nsubd
                kloc=k-nsubd*kjb
                dtemp=(dble(kloc)-0.5d0)/dble(nsubd)
                dchs=dble(kjb+1+nchlop(j))-0.5d0
                wvsubs(k)=(1.0d0-dtemp)*wval(dchs)+
     :                    dtemp*wval(dchs+1.0d0)
              end do
             else
*             just copy directly
              do k=1,nsublen
                casub(k)=1.0d0
                cadsub(k)=1.0d0
                resub(k)=1.0d0
                wvsubs(k)=wval(dble(k+nchlop(j)))
              end do
            end if
            klow=1
            khiw=nsublen
            call vp_spvoigte(casub, cadsub,klow,khiw,
     :                 wvsubs,nsublen,param,ipind,
     :                 elem,ionz,nset,np,ichunk,resub)
*           convolve with the instrument profile
            ndlen=nsublen
            call vp_subchspread(casub,1,ndlen,ichunk,wvsubs,
     :                nsublen,cadsub)
            do k=1,nsublen
              casub(k)=cadsub(k)
            end do
*           
*           squeeze back into the original pixels, in original arrays
*           (also fine for nsubd=1) NOTE ASSUMPTION OF UNIT CONTINUUM
            ddscl=dble(nsubd)
            do k=nchlop(j),nchhip(j)
              jks=nsubd*(k-nchlop(j)+1)
              jjs=jks-nsubd+1
              temp=0.0d0
              do jis=jjs,jks
                temp=temp+casub(jis)
              end do
              ca(k)=temp/ddscl
              cad(k)=ca(k)
            end do
          end do
*****     TEMP CHECK LINES IN CONTINUUM (exit then look)
          if(lcmon) then
            write(6,*) '0 continue, 1 exit'
            read(5,*) ndx
            if(ndx.eq.1) then
              write(6,*) '>> ',elem,ionz,param(1),param(2),param(3)
              goto 962
             else
              write(6,*) '>> ',elem,ionz,param(1),param(2),param(3)
            end if
          end if
*****     TEMPEND
*         all appropriate lines now in the continuum, so sort out 
*         the chisq values
*         compare the profile with the data, using the preset ranges
*         set up an array of differences/sigma, sort them, and use
*         those which satisfy the criteria, down to a minimum number
          ndx=0
          qchs=0.0d0
          do k1=1,nrest
            if(cval.le.14.5d0.or.elvb.ne.'H ') then
              nclo=nchlob(k1)
              nchi=nchhib(k1)
             else
              if(cval.le.18.0d0) then
                dddd=1.0d0
               else
                dddd=10.0d0**(0.5d0*(cval-18.0d0))
              end if
              tempx=(1.0d0+zval)*(wvrest(k1)-dddd)
              call vp_chanwav(tempx,ddtemp,5.0d-2,20,ichunk)
              nclo=int(ddtemp)-1
              nclo=min(nchlob(k1),nclo)
              tempx=(1.0d0+zval)*(wvrest(k1)+dddd)
              call vp_chanwav(tempx,ddtemp,5.0d-2,20,ichunk)
              nchi=int(ddtemp)+1
              nchi=max(nchhib(k1),nchi)
            end if
*           keep within range
            if(nclo.lt.1) nclo=1
            if(nchi.gt.nct) nchi=nct
*           compute statistic
            if(nchi.ge.1.and.nclo.le.nct) then
              do k2=nclo,nchi
                if(de(k2).gt.0.0d0) then
                  ndx=ndx+1
                  diffn(ndx)=(da(k2)-cad(k2))/sqrt(de(k2))
                  qchs=(da(k2)-cad(k2))**2/de(k2)+qchs
                end if
              end do
            end if
*            write(6,*) '>>> qchs',qchs
          end do
          if(ndx.lt.1) then
*           no constraints at all
            write(6,*) elvb,levb,zval,' no lines in range'
            write(26,*) '! ',elvb,levb,zval,' no lines in range'
            goto 908
          end if
*         sort pointers for this array
          call pda_qsiad(ndx,diffn,ipn)
*         compute chi^2 for the acceptable ones, which are those
*         values > -1 for 1-sigma
          k1=1
          do while(diffn(ipn(k1)).lt.-xnul.and.k1.lt.ndx-nminf-1)
            k1=k1+1
          end do
*          write(6,*) 'ndx,nminf',ndx,nminf
*         chi^2
          chisq=0.0d0
          do ii=k1,ndx
            chisq=chisq+diffn(ipn(ii))**2
          end do
          ndf=ndx-k1
          ifail=0
          pchisq=pd_pchi2(chisq,ndf,ifail)
          if(lcminul) then
            pqchs=pd_pchi2(qchs,ndx,ifail)
          end if
          chisqn=chisq/dble(ndf)
*         restore continuum for the next loop
          do k2=1,nct
            ds(k2)=ca(k2)
            cad(k2)=ca(k2)
          end do
        end do
*       write out something (overwrite mode) to reassure user
        write(6,'(i4,2f6.2,a$)') 
     :    jj,param(nppbval),param(nppcol),char(13)
        call flush(6)
      end do
*     find the maximum column density, and print out values
      jjcmax=0
      colmmax=0.0d0
      write(6,*) '  #      b     log(N)  chi2/n   n'
      do jj=1,numbv
        write(6,'(i4,f10.3,f7.2,f8.3,i5)') 
     :     jj,bvh(jj),cvh(jj),chsnh(jj),ndfh(jj)
        if(cvh(jj).gt.colmmax) then
          colmmax=cvh(jj)
          jjcmax=jj
        end if
      end do
*     write the answer in fort.26 format
      write(6,'(2x,a2,a4,f10.6,a11,f8.2,a11,8x,a1,f6.3)')
     :      elvb,levb,zval,'SZ 0.000000',bvh(jjcmax),
     :      'SB   0.00  ','<',colmmax
      write(26,'(2x,a2,a4,f10.6,a11,f8.2,a11,8x,a1,f6.3)')
     :      elvb,levb,zval,'SZ 0.000000',bvh(jjcmax),
     :      'SB   0.00  ','<',colmmax
*     store in common in case needed
      zvalul=zval
      bvalul=bvh(jjcmax)
      cvalul=colmmax
      if(elvb(2:2).eq.' ') then
        ionul=elvb(1:1)//levb//' '
       else
        ionul=elvb//levb
      end if
*
*     restore unit continuum and exit
 908  do i=1,nct
        ca(i)=1.0d0
        cad(i)=1.0d0
      end do
 962  return
 961  write(6,*) 'Rest wavelength file does not exist'
      goto 962
 601  write(6,*) 'Zero wavelength range'
      goto 962
      end
