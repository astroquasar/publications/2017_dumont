      subroutine vp_gglwcoeff(unit1,maxwco, wch,nwco,wcftype,status)
*     read in keyword data for wavelength coefficients
*     ASSUMED TO BE LINEAR, which apply to all the spectra
*     in a 2-D array. For simulations/multispec.
      implicit none

      integer unit1
      integer maxwco
*
      integer nwco,status
      character*8 wcftype
      double precision wch(maxwco)
      character*64 comment
      integer i,istat
*
      call ftgkyd(unit1,'GLWBASE', wch(1),comment,status)
      if(status.eq.0) then
        call ftgkyd(unit1,'GLWINCR', wch(2),comment,status)
*       this one does not matter, but might as well have it
        istat=0
        call ftgkys(unit1,'WCFTYPE', wcftype,comment,istat)
      end if
      nwco=2
      if(maxwco.gt.2) then
        do i=3,nwco
          wch(i)=0.0d0
        end do
      end if
*     Returns with zero status if both coefficients set
      return
      end
