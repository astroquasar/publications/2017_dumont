      subroutine vp_endrej(parm,parerr,np,istat)

      implicit none
      include 'vp_sizes.f'

*	
*     check array parm for conditions indicating system rejection
*     when solution has been achieved
*
*     INPUT:  parm(np)	dble
*             parerr(np)
*             np
*
*     OUTPUT: istat=no. of rejected systems
*     common block vpc_nrejs contains rejected system numbers
*
      integer np
      double precision parm(np)
      double precision parerr(np)
      integer istat
*
*     LOCAL:
      integer kk,kkh
      double precision temp,compval
*
      integer ndrop,kdrop,ndroptot,ndrwhy
      common/vpc_nrejs/ndrop,kdrop(maxnio),ndroptot,ndrwhy
*     
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
*     
*     end rejection by column density threshold and error
      double precision clnemin,errlnemax
      common/vpc_endrej/clnemin,errlnemax
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*
      compval=0.0d0
      kkh=0
      if(clnemin.gt.0.0d0) then
*       search by column density error
*       noppsys was 3, and -noppsys+nppcol was -2
        do kk=noppsys,np,noppsys
          if(parm(kk-noppsys+nppcol).le.clnemin) then
            if(indvar.eq.1) then
              temp=parerr(kk-noppsys+nppcol)/scalelog
             else
              temp=log10(parerr(kk-noppsys+nppcol)/
     1                     parm(kk-noppsys+nppcol))
            end if
            if(temp.gt.errlnemax.and.temp.gt.compval) then
              kkh=kk
              compval=temp
            end if
          end if
        end do
      end if
      if(kkh.gt.0.and.np.gt.noppsys) then
*       the system at kkh to be dropped, if there is
*       more than one system present.
        ndrop=ndrop+1
        kdrop(ndrop)=kkh/noppsys
        istat=ndrop
        write(6,'(a,i4,a,f8.2,a,f8.3)') 
     :         ' Found system',kdrop(ndrop),' with logN < ',
     :         clnemin,' and errlog(col) >',errlnemax
        ndrwhy=0
       else
        istat=0
        write(6,*) 'All systems accepted'
      end if
      return
      end
