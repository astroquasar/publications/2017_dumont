      subroutine vp_dattim(chdate,chtime)
*     New date and time read routine replaces old
*     Fortran library date() and time() routines
      character*(*) chdate,chtime
      character*10 chzone
      integer kdtvals(8)
      call date_and_time(chdate,chtime,chzone,kdtvals)
      return
      end
