      subroutine checkb( temp, turb, m, b )
*     ajc 31-5-92  make sure that line is at least fixed width in size
      implicit none

      double precision temp, turb, m
      double precision b
      
      double precision bmin

*     calculate the minimum width
      bmin = sqrt( turb**2 + 0.128953d0**2 * temp / m )
      b = max( b, bmin )
      return
      end
     
