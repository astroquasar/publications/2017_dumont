      program hedit
*     edit a fits header
*     needs dsepvar routine, and fitsio library
      integer ntem
      character*132 inchstr
      character*64 cv(16)
      double precision dv(16)
      integer iv(16)
      character*64 catem,ctem(1)
      integer item(1)
      double precision dtem(1)
*
      integer unit,status,readwrite,decimals,blocksize
*
*
      do i=1,4
        cv(i)=' '
        iv(i)=0
      end do
      call getarg(1, cv(1))
      if(cv(1)(1:1).eq.'?') then
        inchstr='?'
        goto 890
      end if
      if(cv(1)(1:1).ne.' ') then
        nv=1
        call getarg(2, cv(2))
        if(cv(2)(1:1).ne.' ') nv=2
*       third argument can be a number
        call getarg(3, catem)
        call dsepvar(catem,1,dtem,item,ctem,ntem)
        cv(3)=ctem(1)
        dv(3)=dtem(1)
        iv(3)=item(1)
        if(cv(3)(1:1).ne.' ') nv=3
        call getarg(4, cv(4))
        if(cv(4)(1:1).ne.' ') nv=4

        goto 889
      end if
*
 888  write(6,*) 'Filename, variable, value, action?'
      read(5,'(a)') inchstr
 890  if(inchstr(1:1).eq.'?') then
        write(6,*) 'Parameters are:'
        write(6,*) '1: full filename (including the .fits)'
        write(6,*) '2: keyword to be added/removed/updated'
        write(6,*) '3: value associated with the keyword'
        write(6,*) '    this is stored as a character string or'
        write(6,*) '    a double precision variable' 
        write(6,*) '4: update (u), delete(d) or full list (l)'
        write(6,*) '    must be lower case'
        write(6,*) ' If 4th parameter omitted, update is assumed'
        write(6,*) ' unless 2nd and 3rd omitted as well, in which'
        write(6,*) ' which case an abbreviated keyword list is given'
        write(6,*) ' (HISTORY, HIERARCH & ESO omitted)'
        goto 888
      end if
      call dsepvar(inchstr,4,dv,iv,cv,nv)
*     get a free unit number, preset to 17 in case it fails
 889  unit=17
      call ftgiou(unit, status)
*     open with readwrite access
      readwrite=1
      len=lastchpos(cv(1))
      write(6,*) 'Filename ',cv(1)(1:len)
      status=0
      call ftopen(unit,cv(1)(1:len),readwrite, blocksize,status)
      if(status.eq.0) then
*       file opened OK
        len=lastchpos(cv(2))
        if(cv(4)(1:1).eq.'u'.or.nv.eq.3.or.cv(4)(1:1).eq.'a') then
*         update/append is the default
*         need to sort out what type of parameter is being used
          if(cv(3)(1:1).ne.' '.and.dv(3).eq.0.0d0) then
*           write as a character string
            call ftukys(unit,cv(2)(1:len),cv(3),' ', status)
           else
*           double precision variable
            decimals=12
            call ftukyd(unit,cv(2)(1:len),dv(3),decimals,' ', status)
          end if
*         report any disaster
          if(status.ne.0) then
            write(6,*) 'keyword write status;',status
           else
            len2=lastchpos(cv(3))
            write(6,*) cv(2)(1:len),' ',cv(3)(1:len2),'  written'
          end if
         else if (cv(4)(1:1).eq.'d') then
*         delete a keyword
          call ftdkey(unit,cv(2)(1:len), status)
          write(6,*) cv(2)(1:len),' removed'
         else if (nv.eq.1.or.cv(4)(1:1).eq.'l') then
*         list the keywords
          i=1
          status=0
          write(6,*) 'Keyword list'
          do while (status.eq.0)
            call ftgkyn(unit,i, cv(1),cv(2),cv(3),status)
            if(status.eq.0) then
              if(nv.eq.4.or.(nv.eq.1.and.cv(1)(1:8).ne.'HIERARCH'
     :           .and.cv(1)(1:7).ne.'HISTORY'.and.
     :           cv(1)(1:3).ne.'ESO')) then
                len3=lastchpos(cv(3))
                if(len3.le.0) len3=1
                write(6,'(a16,a1,a24,a1,a)')
     :          cv(1)(1:16),' ',cv(2)(1:24),' ',cv(3)(1:len3)
              end if
            end if
            i=i+1
          end do
         else if(cv(4)(1:6).eq.'remove') then
*         remove NUMBERED keys from the list: iv(2) to iv(3) inclusive
*         Cannot be called from command line...
          if(iv(2).gt.0.and.iv(2).le.iv(3)) then
            write(6,*) 'Removing header records',iv(2),' to',iv(3)
            do i=iv(2),iv(3)
              call ftdrec(unit,i, status)
            end do
          end if
         else
          write(6,*) 'No action taken'
        end if
*       close the file
        call ftclos(unit, status)
        call ftfiou(unit, status)
       else
        write(6,*) 'Failed to open file'
      end if
      stop
      end
            
