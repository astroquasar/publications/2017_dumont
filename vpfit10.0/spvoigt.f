      subroutine spvoigt(flx,contin,nlw,nw,cxxd,zedy,velyb,
     1               lby,lzy,ichunk,cont)
*     SPVOIGT  computes Voigt profile
*
*     Input:
*               flx(nw)         continuum(emission) or anything(absn)
*		contin(nw)	continuum level
*		nlw	integer	lower channel limit for profile generation
*		nw	integer	array size for data
*		cxxd	dble	column density (may be log) (and scaled!!)
*		zedy	dble	redshift
*		velyb	dble	Doppler parameter (may be log)
*		lby  ch*2	element
*		lzy  ch*4	ion
*		ichunk		chunk number
*		cont(nw)	continuum used for zero level adjustment
*     Output:
*		flx(nw)		absorption on continuum
*
      implicit none
      include 'vp_sizes.f'
*     Wavelength scale need not be linear
      character*2 lby
      character*4 lzy
      integer nlw,nw,ichunk
      double precision contin(nw),flx(nw),cont(nw)
      double precision cxxd,zedy,velyb
*
*     LOCAL variables
      integer i,j,jmid,jstep,k,klv,nexbin
      double precision fluxcur,stau,tauchan
      double precision v,a,cne
      double precision zdp1,vely,veld,binvel
      double precision bl,ced,cns,dxd,wlod,whid
      double precision wvd,chand
      double precision awbasd,axku,vxbl
      double precision dtemp,wtemp,xd
      double precision xtxx,cold
      double precision ww, wv
*     variables for working with flux values:
      double precision binlength,e,s,subtau,sumtot,zeshft
*     
*     FUNCTIONS:
      double precision voigt,dexpf
      double precision vp_empval
*
      character*2 lbz
*     lzy is the 'level' i.e. ionization stage
      character*4 lzz
      double precision vp_wval
*     mim 13/6/00 The following variables are not needed anymore since
*     the interpolation procedure has been replaced by a subdivision of
*     pixels and the continuum is only multiplied at the end of the
*     routine.
*
*     double precision afd, bfd, cfd, ymin, xmin
*
      integer nexpd
      common / nexpd / nexpd


      double precision dnshft2(maxnch)
      common/vpc_shft2/dnshft2
      integer nz
      double precision alm,fik,asm 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),nz
*     parameters for smoothing
      double precision zed
      common/vpc_ewpms/zed
*     indvar=1 for log N, 0 linear, -1 for emission lines 
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
      integer ibdvar,izdvar
      common/vp_bzstyle/ibdvar,izdvar
*     Lyman series minimum wavelength in table
      double precision wlsmin,vblstar,collsmin
      common/vpc_lycont/wlsmin,vblstar,collsmin
*     Column density for inclusion of ion in ALL regions
      double precision fcollallzn
      common/vpc_colallzn/fcollallzn
      double precision bvalmin,bvalmax,bltdrop,bgtdrop,colltdrop,
     :                     clogltdrop,cvaldrop,clvaldrop,cvalmax,
     :                     clvalmax,bvalminh,bvalmaxh,clvalmin,
     :                     cvalmin
      common/vpc_bvallims/bvalmin,bvalmax,bltdrop,bgtdrop,colltdrop,
     :                     clogltdrop,cvaldrop,clvaldrop,cvalmax,
     :                     clvalmax,bvalminh,bvalmaxh,clvalmin,
     :                     cvalmin
*     printout variable
      logical verbose
      common/vp_sysout/verbose
*
*
*     set velocity parameter for internal use
      if(ibdvar.ne.1) then
*       straight wavelengths used in program
        vely=velyb
       else
*       1 => log Doppler parameters used
        vely=10.0d0**velyb
      end if
      veld=vely
*     antilog cxxd if necessary, but not if added component
      if(indvar.eq.1.and.lby.ne.'AD'.and.lby.ne.'__'.and.
     :       lby.ne.'<>'.and.lby.ne.'>>') then
        xtxx=cxxd/scalelog
*       mim@phys.unsw.edu.au: 17.10.01 Old default of 10.0
*       replaced by clvaldrop
        if(xtxx.gt.34.0d0.or.xtxx.lt.clvaldrop) then
          xtxx=clvaldrop
*         cxxd=clvaldrop*scalelog ! not used subsequently
        end if
        cold=10.0d0**xtxx
       else
        if(lby.eq.'AD'.or.lby.eq.'__'.or.lby.eq.'<>'.or.
     :          lby.eq.'>>') then
          cold=cxxd
         else
          cold=cxxd/scalefac
        end if
      end if
      zdp1=1.0d0+zedy
      zed=zdp1
*     set up parameters
*     ajc 3-oct-93  set earlier + passed in common block
*     nexpd=5
*
      if(indvar.ne.-1.and.lzy(4:4).ne.'e'.and.
     :         lby.ne.'AD'.and.lby.ne.'__') then
*       absorption line
        do i=1,nw
*         rfc 24.2.95: replace by unit continuum and multiply later,
*         so that can interpolate from continuous absorption to
*         highest series line:
          flx(i)=1.0d0
        end do
       else
*       emission line or added component
        do i=1,nw
          flx(i)=contin(i)
        end do
      end if
*
      if(nz.le.0) call vp_ewred(0)
*     nl=0
*     lower channel number for inclusion now a routine parameter
*     nlw=1
*
*     base wavelength in rest frame for later use
      awbasd=vp_wval(dnshft2(ichunk),ichunk)/zdp1
*     upper rest wavelength:
      axku=vp_wval(dfloat(nw)+dnshft2(ichunk),ichunk)/zdp1
      if(vely.eq.0.0d0.or.vely.eq.veld) goto 302
      veld=vely
*     
 302  vxbl=vp_wval(0.5d0*dble(nw)+dnshft2(ichunk),ichunk)/zdp1
*     want at least nexpd (default 5) sub-bins per Doppler parameter
      binvel=2.99792458d5*(axku-awbasd)/(vxbl*dble(nw-1))
*     velocity units cm/s internal, while km/s used!
      vxbl=veld*vxbl/1.0d5
*
*     now find the lines and slot them in
*
      do k=1,nz
*       ajc 8-apr-92  include high column density lines always
*       [rfc 18-Nov-97: Was as well as continuum altering thing - for 
*       that order (otherwise, this routine not called) - but now uses
*       redshift as well]
        if((lby.eq.lbz(k).and.lzy.eq.lzz(k)).and.
     :         ( (alm(k).ge.awbasd.and.alm(k).le.axku) .or.
     :           cold*fik(k).gt. fcollallzn )) then 
*         for each bin, starting from the line center
          wvd=alm(k)*zdp1
          chand=dble(nw+nlw)/2.0d0+dnshft2(ichunk)
          call vp_chanwav(wvd,chand,5.0d-2,20,ichunk)
          jmid=int(chand-dnshft2(ichunk))
          tauchan=1.0d25
          jstep=-1
          j=jmid
*
          do while (tauchan.gt.1.0d-6.and.j.le.nw)
            if(jstep.eq.-1) then
              if(j.gt.nlw) then
                j=j-1
               else
                tauchan=1.0d-7
                if(jmid.le.0) jmid=1
                goto 901
              end if
             else
              if(j.lt.nw) then
                j=j+1
               else
                tauchan=1.0d-7
                goto 901
              end if
            end if
            stau=10.0d0
*
*           is this absorption or continuum or emission?
            if ( lby .eq. '>>' ) then
*             do nothing - now wavelength shift only
              continue
*             rfc 26.9.02: '<<' option (vp_cshift) removed.
             else if ( lby .eq. '<>' ) then
*             linear function continuum adjuster
              xd=vp_wval(dble(j)+dnshft2(ichunk),ichunk)/zdp1-alm(k)
              xd=xd/alm(k)
              flx(j)=flx(j)*(cold + xd*veld)
*             ajc 18-jul-93 zero level - must be the last thing called 
*             (shift rather than scale).  
             else if ( lby .eq. '__' ) then
*	      Apply stretch and shift
              xd=vp_wval(dble(j)+dnshft2(ichunk),ichunk)/zdp1-alm(k)
*	      for this part as well, x=dlambda/lambda
              xd=xd/alm(k)
              zeshft=cold+xd*veld
              flx(j)=cont(j)*zeshft+flx(j)*(1.0d0-zeshft)
             else
*             only want to be here if normal ion 
*	      line parameters
              wv=alm(k)*1.0d-8
              bl=veld*wv/2.99792458d5
              a=asm(k)*wv*wv/(3.7673031339d11*bl)
              cns=wv*wv*fik(k)/(bl*2.00213d12)
              cne=cold*cns
*             set bin length so that nexpd Doppler parameters per bin
              dtemp=veld/binvel
              if(dtemp.gt.dble(nexpd)) then
                binlength=1.0d0
                nexbin=1
               else
                nexbin=int(dble(nexpd)*binvel/veld+0.5d0)
                if(nexbin/2*2.eq.nexbin) nexbin=nexbin+1
                binlength=1.0d0/dble(nexbin)
              end if
              ced=dble(j)
*             high and low wavelength points
              wlod=vp_wval(ced-0.5d0+dnshft2(ichunk),ichunk)*
     :                       1.0d-8/zdp1
              whid=vp_wval(ced+0.5d0+dnshft2(ichunk),ichunk)*
     :                       1.0d-8/zdp1
*             sub-divide
              dxd=(whid-wlod)/dble(nexbin)
              sumtot=0.0d0
              stau=0.0d0
              do klv=1,nexbin
                fluxcur=flx(j)
                ww=wlod+(dble(klv)-0.5d0)*dxd
                v=wv*wv*(1.0d0/ww-1.0d0/wv)/bl
                if(indvar.eq.-1.or.lby(1:2).eq.'AD'.or.
     :              lby(1:2).eq.'__'.or.lzy(4:4).eq.'e') then
*	          term to be added
                  if(lby(1:2).eq.'AD'.or.lby(1:2).eq.'__') then
                    s=fluxcur+cold*fik(k)*vp_empval(ww)
                   else
                    s=fluxcur+cold*fik(k)*dexpf(-v*v)
                  end if
                  subtau=1.0d0
                 else
*                 absorb...
*                 this seems to be voigt profile...
                  subtau=cne*voigt( v, a )
                  e = dexpf(-subtau)
                  if(e.gt.1d-30.and.fluxcur.gt.1d-30)then
                    if ( log10(e) + log10( fluxcur ) 
     :                      .lt. -30.0d0 ) then
                      s=0.0d0
                     else
                      s=fluxcur*e
                    end if 
                   else
                    s=0.0d0
                  end if
                end if
*               end of voigt profile
*	        add as simple sub-chunks
                stau=subtau+stau
                sumtot=sumtot+s
              end do
              flx(j)=sumtot*binlength
              stau=stau*binlength
            end if
            tauchan=stau
 901        continue
            if(jstep.lt.0.and.(tauchan.lt.1.0d-6.or.j.le.nlw)) then
              jstep=1
              j=jmid-1
              tauchan=1.0d25
            end if
          end do
        end if
      end do
*
*     rfc 24.2.95:
*
      if(indvar.ne.-1.and.lzy(4:4).ne.'e'.and.lby.ne.'AD'.and.
     :         lby.ne.'__') then
*       absorption line
*
*       add Lyman continuum absorption if hydrogen, and if in the
*       right wavelength region:
        if(lby.eq.'H '.and.lzy.eq.'I   ') then
*	  hydrogen, so check if need Lyman limit:
*	  wavelength limit is set by Doppler parameter (vblstar=3 default)
          wtemp=wlsmin*(1.0d0+vblstar/2.99792458d5)
*	  column density limit is logN(HI)=15.0 by default 
*	  (i.e. collsmin=1.0e15)
          if(awbasd.le.wtemp.and.dble(cold).gt.collsmin) then
            if(verbose) then
              write(6,*) 'Lycont:',zdp1,awbasd,wtemp,cold
              write(6,*) ichunk,dnshft2(ichunk)
            end if
            call vp_lycont(cold,flx,nw,zdp1,ichunk)
*            if(verbose) write(6,*) 'vp_lycont exit OK'
          end if
        end if
*
*       multiply by the continuum which came in, to get result:
        do i=1,nw
          flx(i)=flx(i)*contin(i)
        end do
      end if
*
      return
      end
