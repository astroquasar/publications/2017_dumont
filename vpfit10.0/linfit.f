
      subroutine linfit( n, nfit, x, y, e, degree, polyc, ier )

      implicit none

      include 'vp_sizes.f'

      integer maxmas2
      integer n
      integer nfit
      double precision x( n )
      double precision y( n )
      double precision e( n )
      integer degree
      double precision polyc( degree )
      integer ier

      double precision u( maxmas, maxmas )
      double precision v( maxmas, maxmas )
      double precision w( maxmas )
      double precision dum
      integer i
      integer j
      double precision chisq

      external dpoly

      if ( ier .ne. 0 ) return

      if ( nfit .gt. maxmas ) then
        ier = -1
        write ( 6, * ) 'error in curve fitting'
        stop
      end if

*     first, re-arrange so that all the points to be fitted are at the start
*     of the array
      if ( n .gt. 1 ) then
        do i = 1, n-1
          if ( e( i ) .lt. 0d0 ) then
            do j = i+1, n
              if ( e( j ) .ge. 0d0 ) then
                dum = e( i )
                e( i ) = e( j )
                e( j ) = dum
                dum = x( i )
                x( i ) = x( j )
                x( j ) = dum
                dum = y( i )
                y( i ) = y( j )
                y( j ) = dum
                go to 10
              end if
            end do
 10         continue
          end if
        end do
      end if

*     now use the singular value decomp routine
      maxmas2=maxmas
      call svdfit( x, y, e, nfit, polyc, degree, u, v, w,
     1            maxmas, maxmas2, chisq, dpoly )

      return
      end
