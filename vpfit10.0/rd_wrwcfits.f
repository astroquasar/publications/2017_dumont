      subroutine rd_wrwcfits( wch, nct, arg2, status )
*     write out FITS file of wavelengths (double prcision)
      implicit none
      include 'vp_sizes.f'
*
      integer nct
      double precision wch( nct ) ! wavelength array
      character*(*) arg2        ! file from earlier
      integer status
*     Local:
      integer i,j,naxis
      integer npixel,ngroup,nblock
      integer idum              ! junk
      character*60 file         ! output name
      character*32 errmsg
      integer psize
      parameter ( psize = 6 )   ! size of wavelength poly
*     functions
      double precision wval
*
*     common wavelength coefficients
      integer nwco
      double precision wcf1(maxwco)
      common/vpc_wavl/wcf1,nwco
*
      character*80 chstr        ! input arrays
      real rvstr( 5 )
      integer ivstr( 5 ),len(7)
      character*60 cvstr( 5 )
      double precision avfac
      common/vpc_airvac/avfac

*     common:
      character*8 wcftype
      character*4 vacind
      double precision helcfac
      integer*4 noffs
      common/c8_wcftype/wcftype,helcfac,noffs,vacind
*
*     FITSIO stuff
      integer unitout
      logical simple,extend
      integer bitpix
      integer naxes(2)

*     get the file name
      if ( arg2 .ne. ' ') then
        file = arg2
       else
        write ( 6, * ) 'data file name?'
        read ( 5, '(a)', end = 99 ) chstr
        call sepvar( chstr, 1, rvstr, ivstr, cvstr, idum )
        file = cvstr( 1 )
      end if
*     set wavelengths if necessary
      if(nwco.lt.nct) then
        do j=1,nct
          wch(j)=wval(dble(j))
        end do
      end if

*     set dimensions
      len(1)=nct
      do i=2,7
        len(i)=0
      end do
      naxis=1

*     Get an unused Logical Unit Number to use to open the FITS file.
      status=0
      call ftgiou(unitout,status)
*     create the new image
      nblock=1
      call ftinit(unitout,file,nblock, status)
      if(status.ne.0) then
        call ftgerr(status,errmsg)
        write(6,*) ' Failed to create data file: ',file(1:16)
        write(6,*) ' ',errmsg
        write(6,*) ' does it already exist?'
        goto 99
      end if

*     set up the required header elements
      simple=.true.
      bitpix=-64
      naxis=1
      naxes(1)=nct
      naxes(2)=1
      extend=.true.
      call ftphpr(unitout,simple,bitpix,naxis,naxes,0,1,extend,status)

      if(status.ne.0) then
        call ftgerr(status,errmsg)
        write(6,*) ' header write fail: ',errmsg(1:24)
        goto 99
      end if

*     write the array to it
      ngroup=1
      npixel=1
      call ftpprd(unitout,ngroup,npixel,nct,wch, status)
      if(status.ne.0) then
        call ftgerr(status,errmsg)
        write(6,*) ' data write fail: ',errmsg(1:24)
        goto 99
      end if

*     add important things like where it has come from
      call ftpkyj(unitout,'DISPAXIS',1,' ', status)
      status=0
      call ftpkys(unitout,'REFSPEC1',file(1:10),' ', status)
      status=0
*     vacuum or not?
      if ( avfac .lt. 1.00001d0 ) then
        call ftpkys(unitout,'VACUUM','yes',' ',status)
       else
        call ftpkys(unitout,'VACUUM','no',' ',status)
      end if
      status=0
*     
      call ftclos(unitout,status)
      call ftfiou(unitout,status)
*     set wch back to what it was
      if(nwco.lt.nct) then
        do j=1,nct
          wch(j)=wcf1(j)
        end do
      end if

 99   return
      end
