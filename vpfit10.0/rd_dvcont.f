      subroutine rd_dvcont(da,de,drms,ca,cad,nct,dv,nv)
*     divide the data by the continuum, unless the continuum
*     value is less than some preset threshold, when the data
*     is divided by the threshold and the error term set negative
*     i.e. is flagged as bad data.
*
      implicit none
*     Data arrays:
      integer nct,nv
      double precision da(*),de(*),drms(*),ca(*),cad(*)
*     Parameters:
      double precision dv(*)
*     Local:
      integer i
      double precision xc,drv

*
*     reset dv(2) = threshold if necessary
      drv=dv(2)
      if(drv.le.0.0d0.or.nv.lt.2) then
        xc=0.0d0
        do i=1,nct
          if(ca(i).gt.xc) xc=ca(i)
        end do
*       flag places where continuum way down [1e-3*peak is default] as bad
        if(xc.gt.0.0d0) then
          drv=0.001d0*xc
         else
          drv=0.001d0
        end if
      end if
*	    
      do i=1,nct
        if(ca(i).gt.drv) then
          da(i)=da(i)/ca(i)
          de(i)=de(i)/(ca(i)*ca(i))
          drms(i)=drms(i)/(ca(i)*ca(i))
         else
          da(i)=da(i)/drv
          de(i)=-100.0d0
          drms(i)=-100.0d0
        end if
*	reset continuum to unity
        ca(i)=1.0d0
        cad(i)=1.0d0
      end do
*
      return
      end
