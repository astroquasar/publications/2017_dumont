      subroutine rd_drebin(da,de,n,dw,ds,nb,wvdlo,wvdhi,lxa,lxb)
*
*     Rebin the data in da, and variance de, length n, from
*     wavelength wvdlo to wvdhi, into dw, variance ds, length nb,
*     using linear wavelength coefficients ac (base) and bc (increment)
*     passed as common variables. If lxa & lxb BOTH zero, then they are
*     determined here from wvdlo, wvdhi; otherwise lxa & lxb are
*     taken as channel limits in the original data.
*
*     IN:
*		da(n)	dbl	spectral data
*		de(n)	dbl	variance
*		n	i*4	data array length
*		nb	i*4	rebinned array size
*		wvdlo	dbl	low wavelength for inclusion in rebinned set
*		wvdhi	dbl	high wavelength to be rebinned
*		lxa	i*4	start channel in original data
*		lxb	i*4	end channel in original data
*	        isctype	i*4	0:sum, 1:average
*     OUT:
*		dw(nb)	dbl	rebinned data
*		ds(nb)	dbl	rebinned variance
*		nb	int	rebinned array length
*     COMMON:
*		rdc_scwv/ac,bc,ach,achend,nbr,chwts
*
*     rfc 11.11.99
*
      implicit none
      include 'vp_sizes.f'
*
      integer n,nb,lxa,lxb
      double precision da(maxfis),de(maxfis),dw(nb),ds(nb)
      double precision wvdlo,wvdhi
*
*     local variables
      integer i,j,k,nshift
      double precision ddum
      double precision x,xnorm
      double precision wa,wb,ws,wt,wlo,whi
*
*     function declarations:
      double precision wval,rd_wvscofs
*
*     common variables:
*     linear wavelength coefficients and associated variables
*     chwts may be log, for log linear
      character*4 chwts
      integer nbr
      double precision ac,bc,ach,achend
      common/rdc_scwv/ac,bc,ach,achend,nbr,chwts
*     general wavelength coefficients
      integer nwco
      double precision wcf1(maxwco)
      common/vpc_wavl/wcf1,nwco
      character*8 wcftype
      character*4 vacind
      double precision helcfac
      integer noffs
      common/c8_wcftype/wcftype,helcfac,noffs,vacind
*     scrunched data interpolation style and wavelength type
      integer isctype,isclog
      common/rdc_scflag/isctype,isclog
*
*
*     initialize dw and ds
      do i=1,nb
        dw(i)=0.0d0
        ds(i)=0.0d0
      end do
*     set start/end channel numbers if they are not set
      if(lxa.eq.0.and.lxb.eq.0) then
        ddum=wval(dble(n/2))
        call chanwav(wvdlo,ddum,0.003d0,30)
        lxa=int(ddum+0.5d0)
        call chanwav(wvdhi,ddum,0.003d0,30)
        lxb=int(ddum)
        write(6,*) 'Channel range',lxa,' -',lxb
      end if
*     restrict ranges to within limits of data arrays
      if(lxa.le.0) lxa=1
      if(lxb.le.lxa.or.lxb.gt.n) lxb=n
*
*     check if full rebinning is necessary, or if could do with 
*     bin reassignment
      if(nwco.le.2.and.wcftype(1:3).ne.'che'.and.
     :      wcftype(1:3).ne.'log'.and.chwts(1:3).ne.'log') then
*       linear, in polynomial format
*       check increments are sensibly the same
        if(dabs(bc-wcf1(2)).lt.1.0d-10*bc) then
*	  first hurdle satisfied, now need to check that
*	  base wavelengths differ by an integer multiple
*	  of the increment
          wa=(wcf1(1)-ac)/bc
*	  int truncates towards zero, so need sign-dependent condition
          if(wa.ge.0.0d0) then
            nshift=int(wa+0.5d0)
           else
            nshift=int(wa-0.5d0)
          end if
          wt=dble(nshift)
          ws=dabs(wt-wa)
*	  expected accuracy
          wb=1.0d-10*ac/bc
          if(ws.le.wb) then
*           full match, so just shift the data by nshift determined above
            do j=lxa,lxb
              k=j+nshift
              if(k.gt.0.and.k.le.nb) then
                dw(k)=da(j)
                ds(k)=de(j)
              end if
            end do
            goto 991
          end if
        end if
      end if
*     requires rebinning. Cycle through the input data and assign 
*     to output by incrementing counter.
*     k initially first bin in rebinned dataset
      k=1
*     bin edges:
      ws=rd_wvscofs(0.5d0)
*     above replacing =ac+bc*0.5D0
      wt=rd_wvscofs(1.5d0)
*     above replacing =ac+bc*1.5D0
      wb=wval(dble(lxa)-0.5d0)
*
      xnorm=0.0d0
      write(6,*) 'lxa,lxb',lxa,lxb
      do j=lxa,lxb
*       bin edges for current input data bin
        wa=wb
        wb=wval(dble(j)+0.5d0)
        do while (wa.gt.wt.and.k.le.nb) 
*         input data start > current rebinned data bin, so increment
          k=k+1
          ws=wt
          wt=rd_wvscofs(0.5d0+dble(k))
*         above replacing =ac+bc*(0.5D0+dble(k))
        end do
*       now have wt > wa, so potentially some overlap
*       but not if ws > wb, in which case need to increment
*       the input data counter by 1, so stay in loop only if
*       ws.le.wb
        if(ws.le.wb) then
*         need to sort out fraction of da(j) to go into dw(k)
*	  and this depends on how the edges align
*	  low wavelength to go in, and increment k when necessary
901       if(wa.lt.ws) then
            wlo=ws
           else
            wlo=wa
          end if
*	  high wavelength to go in
          if(wb.gt.wt) then
            whi=wt
           else
            whi=wb
          end if
*	  fractional counts to go into this rebin
          x=(whi-wlo)/(wb-wa)
          xnorm=xnorm+x
          dw(k)=dw(k)+x*da(j)
*	  negative variance=bad pixel flag
          if(de(j).gt.0.0d0.and.ds(k).ge.0.0d0) then
            ds(k)=ds(k)+x*de(j)
           else
            ds(k)=-1.0d0
          end if
*	  increment k for this j?
          if(wt.le.wb) then
            if(xnorm.gt.0.0d0.and.isctype.eq.1) then
              dw(k)=dw(k)/xnorm
              ds(k)=ds(k)/xnorm
              xnorm=0.0d0
            end if
            k=k+1
            ws=wt
            wt=rd_wvscofs(0.5d0+dble(k))
*           above replacing =ac+bc*(0.5D0+dble(k))
            goto 901
          end if
        end if
      end do
 991  return
      end
