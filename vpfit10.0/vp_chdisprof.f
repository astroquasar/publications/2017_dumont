      subroutine vp_chdisprof(nlines,ion,level,parm,
     :      num, prks )
*
      implicit none
      include 'vp_sizes.f'
*     display the profile fits on the selected graphics device, using the
*     selected chunks. Attempts to display more than an extended chunk 
*     will result in segments only. The profile fits are transferred via
*     arrays held in common (blocks: vpc_chunkvar, vpc_datch, vpc_wavch).
*     INPUT:
*     all of them
*
      integer nlines
      character*2 ion(maxnio)
      character*4 level(maxnio)
      double precision parm(*)
      integer num
*     K-S probability array
      double precision prks(maxnch)
*     number of x and y graphs
      integer npgx, npgy
*
*     LOCAL:
      logical lguess
      character*1 cans
      character*8 chinx
      character*64 name
      integer first
      integer i,ib,if,ind,it,j
      integer nnzzz,nrun,numxx
      double precision w1,w2
*
*
*     COMMON:
*     free input declarations
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     pgplot flags
      integer ipgopen,ipgflag
      common/pgplotv/ipgopen,ipgflag
*     ascii prinout instead of plot
      logical lascii,lplottoo
      common/vpc_pgascii/lascii,lplottoo
*     New 2D dataset variables
      integer numchunk,nchchtot ! # of chunks,total # data points
      integer kchstrt(maxnch),kchend(maxnch),nchlen(maxnch)
      common/vpc_chunkvar/kchstrt,kchend,nchlen,numchunk,nchchtot
      double precision dach(maxchs,maxnch),derch(maxchs,maxnch)
      double precision drmch(maxchs,maxnch),dcch(maxchs,maxnch)
      common/vpc_datch/dach,derch,drmch,dcch
*     chunk wavelengths, with wavch=center, wavchlo=lower limit of channel
      integer nwvch(maxnch)
      double precision wavch(maxchs,maxnch),wavchlo(maxchs,maxnch)
      common/vpc_wavch/wavch,wavchlo,nwvch
*     single array wavelengths
      integer nwco
      double precision wcf1(maxwco)
      common/vpc_wavl/wcf1,nwco
*
*     chunk variables:
      integer ndpts(maxnch),idrun(maxnch),icrun(maxnch)
      integer indfil(maxnch)
      character*64 filename(maxnch)
      double precision wvstrt(maxnch),wvend(maxnch)
      integer nchunk
      common/vpc_chunk/wvstrt,wvend,ndpts,filename,
     :     idrun,icrun,indfil,nchunk
*     display tick mark style
      logical lmarkl,lmarkto
      character*132 chmarkfl
      double precision zmark
      common/rdc_lmarklv/zmark,chmarkfl,lmarkl,lmarkto
*
      ind=0
*
*     set default for num here
      numxx = indfil( 1 )
      num = numxx
*     default plot is  yes
 1479 continue
      write(6,*)' Plot? y,n, c=change device, t=change ticks [y]'
      write(6,'(''> '',$)')
*     allow for extended options, in a fixed format
      read(5,'(a)') chinx
      cans=chinx(1:1)
      if(cans.eq.'?') then
        write(6,*) 
     1  '2nd parameter: a=ascii+plot, n=no ascii, s=ascii, no plot'
        write(6,*) ' blank=as previous'
        goto 1479
      end if
*     second parameter for ascii output as well a=ascii+plot
*     n=no ascii, s=supress plot, ascii o/p
      if(chinx(2:2).eq.'a'.or.chinx(3:3).eq.'a') then
        lascii=.true.
        lplottoo=.true.
      end if
      if(chinx(2:2).eq.'n'.or.chinx(3:3).eq.'n') then
        lascii=.false.
        lplottoo=.true.
      end if
      if(chinx(2:2).eq.'s'.or.chinx(3:3).eq.'s') then
        lascii=.true.
        lplottoo=.false.
      end if
*     change style of tick marks with 't', then plot
      if(cans.eq.'t'.or.cans.eq.'T') then
        cans='y'
        if(lmarkl) then
          lmarkl=.false.
         else
          lmarkl=.true.
        end if
      end if
*     
      if(cans.eq.'c'.or.cans.eq.'C') then
*       change plot device
        if ( ipgflag .eq. 1 ) then
*         cancel prompt for xdisplay
          call pgask(.false.)
          write(6,*) ' Closing plot device'
          call vp_pgend
        end if
*       multiple plots for multiple fits
        write(6,*) ' number of graphs in x, y direction [1,1]'
        write(6,'(''> '',$)')
        read(5,'(a)') inchstr
        call sepvar(inchstr,2,rvstr,ivstr,cvstr,nvstr)
        npgx = max(1,ivstr(1))
        npgy = max(1,ivstr(2))
        ipgopen=0
        call vp_pgbegin(npgx,npgy,' ')
*       replaces call pgbegin(0,'?',npgx,npgy)
      end if
      if ( cans .ne. 'n' .and. cans .ne. 'N' ) then
*       plot a region
        it=indfil(2)
        if(it.eq.0) then
*         alter so that works as other
*         sections, with num being the file number, nrun being the chunk number
          nrun = 1
          num = indfil( nrun )
*         (only one dataset used in fit)
          goto 5023
        endif
        write(6,*) ' Identify which dataset:'
        write(6,1521)
 1520   format(1x,i2,1x,i2,4x,a14,2i4,2x,f9.2,1x,f7.4,1x,
     :                 i6,4x,f5.3)
 1521   format(1x,' # ID   Filename       data cont    lamcoeffs
     :      nchans  KS prob')
        do i=1,nchunk
          if=indfil(i)
          first = 1
          if ( index( filename( i )( first : ), '/' ) .ne. 0 ) then
            do while ( index( filename( i )( first : ), '/' ) 
     :                   .ne. 0 ) 
              first = index( filename( i )( first : ), '/' ) + first
            end do
          end if
          name = filename(i)(first:)
          w1=wavch(1,i)
          w2=wavch(2,i)-w1
          write(6,1520) i,if,name,idrun(i),icrun(i),w1,w2,
     :           ndpts(i),prks(i)
        end do
        nrun = 0
 9891   write(6,'('' Which # ? ['',i2,'' ]'')') num
        write(6,'(''> '',$)')
        read(5,'(a)',err=9891) inchstr
        call sepvar(inchstr,4,rvstr,ivstr,cvstr,nvstr)
        numxx=ivstr(1)
        if(numxx.gt.nchunk) goto 9891
        if(numxx.gt.0) num=numxx
*       check that lines get put in the right places
        j=num
        it=indfil(j)
*       save the chunk number
        nrun = j
        goto 5023
*
 5023   continue
*
*       set all the wavelength variables for local use
        nwco=nchlen(nrun)
        do j=1,nwco
          wcf1(j)=wavch(j,nrun)
        end do
*
        lguess=.true.
        call vp_linlim(nchlen(nrun),nrun,lguess)
*       reset tick marks
        if(nlines.le.0) then
          write(6,*) 'vp_disprof: nlines = ',nlines
        end if
        if(nlines.gt.1) then
          do ib = 1, nlines
            nnzzz=(ib-1)*noppsys+nppzed
            call tikset(ion(ib),level(ib),parm(nnzzz),ib)
          end do
        end if
*
        call vp_chsplot(num,ind,
     :                  ion,level,parm,nlines)
*       increment default display chunk number
        num=num+1
        if(num.gt.nchunk) num=1
        goto 1479
      end if

      return
      end

      subroutine vp_chsplot(ncol,ind,
     :                    ion,level,parm,nlines)
*     plot section from a 2-d array set
      implicit none
      include 'vp_sizes.f'
*
      integer ncol,ind,nlines
      character*2 ion(maxnio)
      character*4 level(maxnio)
      double precision parm(*)
*     Local:
      character*18 fopascii
      integer i,is
      integer j,kaxis
      integer lm,loffs
      integer nv,nchanf
      double precision fl,fin
      double precision xmd,ymd,xhd,yhd
      double precision xtem1d,xtem2,xtem3,xtem4
      real rthpg,rtepg,ytempg
*
*     rfc 2Jul98 zero line array (if needed)
      real xzropg(2),yzropg(2)
*
      integer nst,ncn,lc1,lc2
      real fampg
      common/ppam/nst,ncn,lc1,lc2,fampg
*     text label for a curve
      real xtextpg,ytextpg
      character*24 cpgtext
      integer icoltext
      common/pgtexts/xtextpg,ytextpg,cpgtext,icoltext
*     pgplot attributes: data, error, continuum, axes, ticks, RESIDUAL
*     1 color indx; 2 line style; 3 line width; 
*     4 curve(0), hist(1); 5 - 10 reserved.
      integer ipgatt(10,9)
      common/vpc_pgattrib/ipgatt
*     plot array pointers (consistent with ipgatt)
*     0=don't,1=do for 1:data,2:error,3:cont,6:resid,9:rms
      integer ksplot(9)
      common/vpc_ksplot/ksplot
*
      real residpg,rshifpg
      common/vpc_resid/residpg,rshifpg
*     This needed to delineate chunks:
*     chunk variables:
      integer ndpts(maxnch),idrun(maxnch),icrun(maxnch)
      integer indfil(maxnch)
      character*64 filename(maxnch)
      double precision wvstrt(maxnch),wvend(maxnch)
      integer nchunk
      common/vpc_chunk/wvstrt,wvend,ndpts,filename,
     :     idrun,icrun,indfil,nchunk
*     
*     min and max yscales by hand, and default lower level
      integer nyuse
      real ylowhpg,yhihpg,yminsetpg
      common/pgylims/ylowhpg,yhihpg,yminsetpg,nyuse
*     pgplot flags
*      integer ipgopen,ipgflag
*      common/pgplotv/ipgopen,ipgflag
*     single precision workspace
      real dspg,repg
      common/vpc_snglwork/dspg(maxfis),repg(maxfis)
*     pgplot overplot with bias
      integer ipgov
      real pgbias
      logical plgscl
      common/pgover/ipgov,pgbias,plgscl
      integer ipgnx,ipgny
      character*60 pglchar(3)
      common/pgnumba/ipgnx,ipgny,pglchar
*     plot/print velocity scale (yes if lvel=.true.)
      logical lvel
      real  wcenpg,vellopg,velhipg,zp1refpg
      common/vpc_pgvels/wcenpg,vellopg,velhipg,lvel,zp1refpg
*     ascii prinout instead of plot
      logical lascii,lplottoo
      common/vpc_pgascii/lascii,lplottoo
*     tick positions for ASCII printout
      integer nred
      integer ntick
      double precision tikmk,rstwav
      common/vpc_ticks/tikmk(maxnio,maxnio+1),ntick(maxnio+1),
     :     nred,rstwav(maxnio,maxnio+1)
*     plot maxima and minima
      real xmpg,xhpg,ympg,yhpg
      common/vpc_splcur/xmpg,xhpg,ympg,yhpg
*     New 2D dataset variables 
      integer numchunk,nchchtot ! # of chunks,total # data points
      integer kchstrt(maxnch),kchend(maxnch),nchlen(maxnch)
      common/vpc_chunkvar/kchstrt,kchend,nchlen,numchunk,nchchtot
      double precision dach(maxchs,maxnch),derch(maxchs,maxnch)
      double precision drmch(maxchs,maxnch),dcch(maxchs,maxnch)
      common/vpc_datch/dach,derch,drmch,dcch
*     chunk wavelengths
      integer nwvch(maxnch)
      double precision wavch(maxchs,maxnch),wavchlo(maxchs,maxnch)
      common/vpc_wavch/wavch,wavchlo,nwvch
*
*     chunk extension
      integer nrextend
      common/vpc_chunkext/nrextend
*     fitted profiles (from ucoptv/deriv)
      double precision func(maxscs),phi(maxchs,maxnch)
      common/vpc_der/ func,phi
      character*8 wcftype
      character*4 vacind
      double precision helcfac
      integer*4 noffs
      common/c8_wcftype/wcftype,helcfac,noffs,vacind
*	  
      ind=1
*     set scale parameters
      nst=1
      ncn=nchlen(ncol)
      lc1=nst
      lc2=ncn
      fampg=1.333333
*     preset hand limits
      ylowhpg=-1.0e25
      yhihpg=-1.0e25
      lm=0
*     set plot control variables
      call plset(ind)
*     flag requested quit
      if(ind.eq.-1) return
*     set plot scales
      yhd=-1.0d30
      ymd=1.0d30
      do i=lc1,lc2
        if(dach(i,ncol).gt.yhd) then
          yhd=dach(i,ncol)
        end if
        if(dach(i,ncol).lt.ymd) then
          ymd=dach(i,ncol)
        end if
      end do
      ympg=real(ymd)
      yhpg=real(yhd)
      if(yhihpg.gt.-1.0e24) then
        yhpg=yhihpg
       else
        yhpg=yhpg*1.015
        if(yhpg.le.0.) yhpg=1.
      end if
      if(ylowhpg.gt.-1.0e24) then
        ympg=ylowhpg
       else
        if(ympg.gt.yminsetpg) then
          ympg=yminsetpg
         else
          if(ympg.lt.0.0) then
            ympg=1.3*ympg
            if(ympg.lt.-0.2*yhpg) ympg=-0.2*yhpg
           else
            ympg=0.95*ympg
          end if
        end if
      end if
*     open plot device if necessary
      call vp_pgbegin(1,1,' ')
*
      yhpg=yhpg*fampg
      xhd=wavch(ncn,ncol)
      xhpg=real(xhd)
      xmd=wavch(nst,ncol)
      xmpg=real(xmd)
      if(lm.lt.0) lm=0
      nv=ncn
*
*     add-in for write to ascii file
      if(lascii) then
        if(lplottoo) then
*         write to named file
          close(unit=17)
          if(ncol.lt.10) then
            write(fopascii,'(a13,i1,a4)') 'vpfit_chunk00',ncol,'.txt'
           else
            if(ncol.lt.100) then
              write(fopascii,'(a12,i2,a4)')'vpfit_chunk0',ncol,'.txt'
             else
              write(fopascii,'(a11,i3,a4)') 'vpfit_chunk',ncol,'.txt'
            end if
          end if
          write(6,'(a,a)') 'wavelengths, data, error, continuum -> ',
     1       fopascii
          open(unit=17,file=fopascii,status='unknown',err=997)
          rewind(unit=17)
         else
          write(6,*) 'wavelengths, data, error, continuum -> fort.17'
        end if
        write(17,'(a)') '! -------------------------'
        loffs=kchstrt(ncol)-1
        do j=1,nv
          xtem2=dach(j,ncol)
          if(j.ge.kchstrt(ncol).and.j.le.kchend(ncol)) then
            xtem3=phi(j-loffs,ncol)
           else
            xtem3=dcch(j,ncol)
          end if
          if(derch(j,ncol).gt.0.0d0) then
            xtem4=sqrt(derch(j,ncol))
           else
            xtem4=0.0d0
          end if
          if(lvel.and.(wcenpg.gt.0.0)) then
*	    convert to velocity if that is what was requested:
            xtem1d=(wavch(j,ncol)/dble(wcenpg)-1.0d0)*2.99792458d5
            if(real(xtem1d).ge.vellopg.and.real(xtem1d).le.velhipg)
     :                             then 
              write(17,*) xtem1d,xtem2,xtem4,xtem3
            end if
           else
*	    normal wavelengths
            xtem1d=wavch(j,ncol)
*	    rfc 16.08.01: Order/format changed for compatibility with rdgen
            write(17,*) xtem1d,xtem2,xtem4,xtem3
          end if
        end do
*       tick mark positions, if any
        if(nred.gt.0) then
          write(17,'(a)') '!  ---- tick mark positions for above ----'
          fl=wavch(nst,ncol)
          fin=wavch(ncn,ncol)
          do j=1,nred
            do i=1,ntick(j)
              if(tikmk(i,j).ge.fl.and.tikmk(i,j).le.fin) then
                write(17,'(a,f15.5,2i6)') '! ',tikmk(i,j),i,j
              end if
            end do
          end do
        end if
*	branch to exit
        if(.not.lplottoo) goto 998
      end if
*
*     plot to pgplot device
      if(lvel.and.(wcenpg.gt.0.0)) then
*       convert to velocity if that is what was requested:
        do i=1,nv
          repg(i)=real((wavch(i,ncol)/dble(wcenpg)-1.0d0)
     :                *2.99792458d5)
          dspg(i)=real(dach(i,ncol))
        end do
*       reset limits
        xmpg=vellopg
        xhpg=velhipg
       else
*	standard wavelength scale
        do i=1,nv
          repg(i)=real(wavch(i,ncol))
          dspg(i)=real(dach(i,ncol))
        end do
      end if
*
      if(xhpg.le.xmpg) goto 999
      if(ipgov.le.0) then
        call pgsci(ipgatt(1,4))
        call pgsls(ipgatt(2,4))
        call pgslw(ipgatt(3,4))
        kaxis=1
        if(ipgatt(10,4).ne.11) then
          kaxis=0
        end if
        call pgenv(xmpg,xhpg,ympg,yhpg,0,kaxis)
        call pglabel(pglchar(1),pglchar(2),pglchar(3))
        if(ipgatt(10,4).eq.10) then
*         draw x=0 line
          xzropg(1)=repg(1)
          xzropg(2)=repg(nv)
          yzropg(1)=0.0
          yzropg(2)=0.0
          call pgline(2,xzropg,yzropg)
        end if
       else
        do is=1,nv
          dspg(is)=dspg(is)+pgbias
        end do
      end if
*     1 color indx; 2 line style; 3 line width; 
*     4 curve(0), hist(1); 5 - 9 reserved.
      call pgsci(ipgatt(1,1))
      call pgsls(ipgatt(2,1))
      call pgslw(ipgatt(3,1))
      if(ipgatt(4,1).ne.1) then
*       standard curve
        call pgline(nv,repg,dspg)
       else
*	binned data
        call pgbin(nv,repg,dspg,.true.)
      end if
*     set text on curve
      if(cpgtext(1:1).ne.' ') then
        if(icoltext.eq.0) then
*         Use the axis colour
          call pgsci(ipgatt(1,4))
         else
          if(icoltext.lt.0) then
*	    Use the curve colour
            call pgsci(ipgatt(1,1))
           else
*	    Use locally defined colour
            call pgsci(icoltext)
          end if
        end if
        call pgsls(ipgatt(2,4))
        call pgslw(ipgatt(3,4))
        call pgtext(xtextpg,ytextpg,cpgtext)
*       clear so don't have same annotation again
        cpgtext=' '
      end if
*
*     other curves
*     error
      if(ksplot(2).gt.0) then
*       plot error
        call pgsci(ipgatt(1,2))
        call pgsls(ipgatt(2,2))
        call pgslw(ipgatt(3,2))
        do i=1,nv
          j=lm+i
          if(derch(j,ncol).gt.0.0d0) then
            dspg(i)=real(sqrt(derch(j,ncol)))
           else
            if(derch(j,ncol).lt.0.0d0) dspg(i)=yhpg
          end if
        end do
        if(ipgatt(4,2).ne.1) then
          call pgline(nv,repg,dspg)
         else
          call pgbin(nv,repg,dspg,.true.)
        end if
      end if
*     rms array
      if(ksplot(9).gt.0) then
        call pgsci(ipgatt(1,9))
        call pgsls(ipgatt(2,9))
        call pgslw(ipgatt(3,9))
        do i=1,nv
          j=lm+i
          if(drmch(j,ncol).gt.0.0d0.and.derch(j,ncol).gt.0.0d0) then
            dspg(i)=real(sqrt(drmch(j,ncol)))
           else
            if(drmch(j,ncol).lt.0.0d0) then 
              dspg(i)=yhpg
             else
              dspg(i)=0.0
            end if
          end if
        end do
        if(ipgatt(4,9).ne.1) then
          call pgline(nv,repg,dspg)
         else
          call pgbin(nv,repg,dspg,.true.)
        end if      
      end if
*     continuum
      if(ksplot(3).gt.0) then
*	plot fit on continuum over the range for which it is available
        call pgsci(ipgatt(1,3))
        call pgsls(ipgatt(2,3))
        call pgslw(ipgatt(3,3))
        loffs=kchstrt(ncol)-1
        nchanf=kchend(ncol)-loffs
        do i=1,nchanf
          dspg(i)=real(phi(i,ncol))
          repg(i)=real(wavch(i+loffs,ncol))
        end do
*       bias up if the data was
        if(ipgov.gt.0) then
          do is=1,nv
            dspg(is)=dspg(is)+pgbias
          end do
        end if
        if(ipgatt(4,3).ne.1) then
*	  standard line
          call pgline(nchanf,repg,dspg)
         else
          call pgbin(nchanf,repg,dspg,.true.)
        end if
      end if
*     residuals against RMS array
      if(ksplot(6).gt.0) then
*       but only if residpg positive
        if ( residpg .gt. 0.0 ) then
          call pgsci( ipgatt( 1, 6 ) ) 
          call pgsls( ipgatt( 2, 6 ) ) 
          call pgslw( ipgatt( 3, 6 ) ) 
          loffs=kchstrt(ncol)-1
          nchanf=kchend(ncol)-loffs
          do i=1,nchanf
            j=i+loffs 
            if (drmch(j,ncol).gt.0.0d0) then
              dspg(i)=residpg*real((dach(j,ncol)-phi(i,ncol))/
     :           sqrt(drmch(j,ncol)))+0.5*(ympg+yhpg)+rshifpg
             else
*             display zero residuals for bad pixels (since it
*             is easier than displaying nothing at all)
              dspg(i)=0.5*(ympg+yhpg)+rshifpg
            end if
            repg(i)=real(wavch(i+loffs,ncol))
          end do
          call pgline( nchanf, repg, dspg )
*         one sigma deviation line
          call pgmove(xmpg, 0.5*(ympg+yhpg)+residpg+rshifpg)
          call pgdraw(xhpg, 0.5*(ympg+yhpg)+residpg+rshifpg)
          call pgmove(xmpg, 0.5*(ympg+yhpg)-residpg+rshifpg)
          call pgdraw(xhpg, 0.5*(ympg+yhpg)-residpg+rshifpg)
*         ajc 6-aug-91  mark the ranges of each chunk
          do i = 1, nchunk
            if(indfil(i).eq.ncol) then
*	      rfc 26.6.95:
              rthpg=real(wavch(kchend(i),i))
              ytempg=ympg+(0.1+0.01*real(i))*(yhpg-ympg)
              call pgpoint(1,rthpg,ytempg,17)
              rtepg=real(wavch(kchstrt(i),i))
              call pgpoint(1,rtepg,ytempg,17)
              call pgmove(rthpg,ytempg)
              call pgdraw(rtepg,ytempg)
            end if
          end do
        end if
      end if
*
*     set up for the tick marks if this is not an overlaid plot
      if(ipgov.le.0) then
        call pgsci(abs(ipgatt(1,5)))
        call pgsls(ipgatt(2,5))
        call pgslw(ipgatt(3,5))
        wcftype='array   '
        helcfac=1.0d0
        noffs=0
        vacind='vac '
        call vp_dotick(xmpg,xhpg,ympg,yhpg,(ipgatt(1,5).gt.0),
     :                 ion,level,parm,nlines)
      end if
 998  return
 999  write(6,*) ' Zero wavelength range'
      goto 998
 997  write(6,*) ' Failed to open ASCII output file'
      goto 998
      end
