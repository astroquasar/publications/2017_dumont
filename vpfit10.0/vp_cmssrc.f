c This file contains two algorithms for the logarithm of the gamma function.
c Algorithm AS 245 is the faster (but longer) and gives an accuracy of about
c 10-12 significant decimal digits except for small regions around X = 1 and
c X = 2, where the function goes to zero.
c The second algorithm is not part of the AS algorithms.   It is slower but
c gives 14 or more significant decimal digits accuracy, except around X = 1
c and X = 2.   The Lanczos series from which this algorithm is derived is
c interesting in that it is a convergent series approximation for the gamma
c function, whereas the familiar series due to De Moivre (and usually wrongly
c called Stirling's approximation) is only an asymptotic approximation, as
c is the true and preferable approximation due to Stirling.
c
c
c
      DOUBLE PRECISION FUNCTION as_alngam(XVALUE, IFAULT)
C
C     ALGORITHM AS245  APPL. STATIST. (1989) VOL. 38, NO. 2
C
C     Calculation of the logarithm of the gamma function
C
      IMPLICIT NONE
      INTEGER IFAULT
      DOUBLE PRECISION ALR2PI, FOUR, HALF, ONE, ONEP5, R1(9), R2(9),
     +      R3(9), R4(5), TWELVE, X, X1, X2, XLGE, XLGST, XVALUE,
     +      Y, ZERO
C
C     Coefficients of rational functions
C
      DATA R1/-2.66685 51149 5D0, -2.44387 53423 7D1,
     +        -2.19698 95892 8D1,  1.11667 54126 2D1,
     +         3.13060 54762 3D0,  6.07771 38777 1D-1,
     +         1.19400 90572 1D1,  3.14690 11574 9D1,
     +         1.52346 87407 0D1/
      DATA R2/-7.83359 29944 9D1, -1.42046 29668 8D2,
     +         1.37519 41641 6D2,  7.86994 92415 4D1,
     +         4.16438 92222 8D0,  4.70668 76606 0D1,
     +         3.13399 21589 4D2,  2.63505 07472 1D2,
     +         4.33400 02251 4D1/
      DATA R3/-2.12159 57232 3D5,  2.30661 51061 6D5,
     +         2.74647 64470 5D4, -4.02621 11997 5D4,
     +        -2.29660 72978 0D3, -1.16328 49500 4D5,
     +        -1.46025 93751 1D5, -2.42357 40962 9D4,
     +        -5.70691 00932 4D2/
      DATA R4/ 2.79195 31791 8525D-1, 4.91731 76105 05968D-1,
     +         6.92910 59929 1889D-2, 3.35034 38150 22304D0,
     +         6.01245 92597 64103D0/
C
C     Fixed constants
C
      DATA ALR2PI/9.18938 53320 4673D-1/, FOUR/4.D0/, HALF/0.5D0/,
     +     ONE/1.D0/, ONEP5/1.5D0/, TWELVE/12.D0/, ZERO/0.D0/
C
C     Machine-dependent constants.
C     A table of values is given at the top of page 399 of the paper.
C     These values are for the IEEE double-precision format for which
C     B = 2, t = 53 and U = 1023 in the notation of the paper.
C
      DATA XLGE/5.10D6/, XLGST/1.D+305/
C
      X = XVALUE
      AS_ALNGAM = ZERO
C
C     Test for valid function argument
C
      IFAULT = 2
      IF (X .GE. XLGST) RETURN
      IFAULT = 1
      IF (X .LE. ZERO) RETURN
      IFAULT = 0
C
C     Calculation for 0 < X < 0.5 and 0.5 <= X < 1.5 combined
C
      IF (X .LT. ONEP5) THEN
        IF (X .LT. HALF) THEN
          AS_ALNGAM = -LOG(X)
          Y = X + ONE
C
C     Test whether X < machine epsilon
C
          IF (Y .EQ. ONE) RETURN
         ELSE
          AS_ALNGAM = ZERO
          Y = X
          X = (X - HALF) - HALF
        END IF
        AS_ALNGAM = AS_ALNGAM + X * ((((R1(5)*Y + R1(4))*Y + R1(3))*Y
     +                + R1(2))*Y + R1(1)) / ((((Y + R1(9))*Y + R1(8))*Y
     +                + R1(7))*Y + R1(6))
        RETURN
      END IF
C
C     Calculation for 1.5 <= X < 4.0
C
      IF (X .LT. FOUR) THEN
        Y = (X - ONE) - ONE
        AS_ALNGAM = Y * ((((R2(5)*X + R2(4))*X + R2(3))*X + R2(2))*X
     +              + R2(1)) / ((((X + R2(9))*X + R2(8))*X + R2(7))*X
     +              + R2(6))
        RETURN
      END IF
C
C     Calculation for 4.0 <= X < 12.0
C
      IF (X .LT. TWELVE) THEN
        AS_ALNGAM = ((((R3(5)*X+R3(4))*X+R3(3))*X+R3(2))*X+R3(1)) /
     +            ((((X + R3(9))*X + R3(8))*X + R3(7))*X + R3(6))
        RETURN
      END IF
C
C     Calculation for X >= 12.0
C
      Y = LOG(X)
      AS_ALNGAM = X * (Y - ONE) - HALF * Y + ALR2PI
      IF (X .GT. XLGE) RETURN
      X1 = ONE / X
      X2 = X1 * X1
      AS_ALNGAM = AS_ALNGAM + X1*((R4(3)*X2 + R4(2))*X2 + R4(1))/
     +              ((X2 + R4(5))*X2 + R4(4))
      RETURN
      END
c
c
c
      double precision function as_gammds (y,p,ifault)
c
c        Algorithm AS 147  Appl. Statist. (1980) Vol. 29, No. 1
c
c        Computes the incomplete gamma integral for positive
c        parameters y,p using an infinite series
c
c        Auxiliary function required: ALNGAM = CACM algorithm 291
c
c	 AS239 should be considered as an alternative to AS147
c
      implicit none
      integer ifault
      double precision y,p
      double precision zero,one,e,arg,uflo,f,a,c
*     function declarations
      double precision as_alngam
      data e/1.0d-9/, zero/0.0d0/, one/1.0d0/, uflo/1.0d-37/
c
c     Checks admissibility of arguments and value of f
c
      ifault = 1
      as_gammds = zero
      if(y.le.zero .or. p.le.zero) return
      ifault = 2
c
c     alngam is natural log of gamma function
c
      arg = p*log(y)-as_alngam(p+one,ifault)-y
      if(arg.lt.log(uflo)) return
      f = exp(arg)
      if(f.eq.zero) return
      ifault = 0
c
c     Series begins
c
      c = one
      as_gammds = one
      a = p
 1    a = a+one
      c = c*y/a
      as_gammds = as_gammds+c
      if (c/as_gammds.gt.e) goto 1
      as_gammds = as_gammds*f
      return
      end
      double precision function pd_pchi2(x,n,ifail)
      implicit none
      integer n, ifail
      double precision x
*
*     x	value of chi^2
*     n	degrees of freedom
*     ifail	must be zero on entry, non-zero on exit if fails
*
*     pd_pchi2 is probability of chi^2.ge.x for n degrees of freedom
*     The routine uses ONLY public domain software.
*     ACCURACY: OK to answers about 1E-8, after which the probability
*     is overestimated. If you care at that level, use something else.
*     This replaces NAG G01BCF for shared software use.
*
      double precision as_gammds
      double precision sdf,sx,c2
      sdf=0.5d0*dble(n)
      sx=0.5d0*x
      c2=as_gammds(sx,sdf,ifail)
      if(c2.eq.0.0d0.and.sx.gt.sdf) then
*       routine failed because series inappropriate
        sx=0.0d0
       else
        sx=1.0d0-c2
      end if
*     roundoff error check
      if(sx.lt.0.0d0) sx=0.0d0
*
      pd_pchi2=sx
      end
