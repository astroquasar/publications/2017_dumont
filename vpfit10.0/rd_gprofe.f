      subroutine rd_gprofe(ca,cad,nct,lsmth,nsubstep)
*
*     Put model absorption profiles into the continuum array
*     using subpixels if necessary
*     IN:
*     ca(nct)    dble array  continuum
*     cad(nct)   dble array  continuum copy
*     re(nct)    dble array  workspace
*     nct        integer     array length used
*     lsmth      logical     false: don't smooth, otherwise do.
*     nsubstep   integer     number of substeps/pixel (.le.1 for no substeps)
* 
      implicit none
*     array sizes file
      include 'vp_sizes.f'
*
*     returned array is input continuum
      integer nct
      double precision ca(nct)
      double precision cad(nct)
*     double precision re(nct)
*     smoothing by instrument profile?
      logical lsmth
*     substep?
      integer nsubstep
*
*
      logical loguc
      logical llist,lsums
*     free input format stuff
      character*132 chstr
      character*2 indcol,indb,indz
*
*     variables for setting options
      logical laonly
      logical lnosp,lnomet,lconly,lcremove,lchion,lnohyd
      logical lallmetz
      character*2 chpelm
      character*4 chpion
*     LOCAL variables
      integer np,nset
      character*4 chlnkv
      double precision param(5)
      character*2 ipind(5)
      character*2 ion
      character*4 level
      character*2 chsumcol
      integer i,ichunk,ifst,ierr,j,jjj,jpin
      integer klow,khiw,lnk,nstar
      integer nchyd,ncall,nccv,next
      integer jis,jjs,jks
      integer k,kjb,kloc,nsstartm,nsend,ndlen
      double precision chand,dchs,ddscl
      double precision collo,colhi,zedlo,zedhi
      double precision bvallo,bvalhi,bvall
      double precision bval,col,wavel,zed
      double precision bvalsh,colsh,zedsh,sumcol
      double precision teff,vtb
      double precision dtemp,temp,xlamx
*     functions
      logical nocgt,lcase
      integer lastchpos
      double precision wval
*
*     COMMON variables
*     substep workspace
      double precision casub(maxsal),cadsub(maxsal),resub(maxsal)
      common/rdc_casub/casub,cadsub,resub
*     resolution previous values:
      double precision sigmah,dlamh
      common/rdc_poldres/sigmah,dlamh
*
*     fitted regions (each of the %% lines in fort.26 style)
      integer nreg
      integer kalow(maxpreg),kahiw(maxpreg)
      double precision wvlo(maxpreg),wvhi(maxpreg)
      common/rdc_regions/wvlo,wvhi,kalow,kahiw,nreg
*     atomic data
      integer nz
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),nz
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     scalefactors for column density
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
*
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
      double precision dvstr(24)
      common/vpc_dsepspace/dvstr
*     Lyman limit variables
      double precision wlsmin,vblstar,collsmin
      common/vpc_lycont/wlsmin,vblstar,collsmin
*     line list variables
      double precision zmark
      character*132 chmarkfl
      logical lmarkl,lmarkto
      common/rdc_lmarklv/zmark,chmarkfl,lmarkl,lmarkto
*     subdivided chunk wavelengths
      integer nsublen
      double precision wvsubs(maxsal)
      common/rdc_wvsubs/wvsubs,nsublen
*     fit to resolution, using sigmas
      integer nswres(maxnch)
      double precision swres(maxpoc,maxnch)
      common/vpc_sigwres/swres,nswres
*     input streams
      integer inputc
      common/rdc_inputc/inputc
*     character collating sequence limits
      integer nchlims
      common/vpc_charlims/nchlims(6)
      character*2 lastch,firstch
      common/vpc_ssetup/lastch,firstch
*     monitoring variable
      integer ncallset
      common/rdc_ncallset/ncallset
*     upper limit defaults, updated by routines which use them
      character*4 ctypeul
      character*60 chwv1
      logical lcminul
      integer nminfd,nminfdef
      double precision xnul,xbul,pchslim,dfwhmkms,bvalmins 
      double precision wloddr,whiddr
      common/rdc_ulvsets/xnul,xbul,pchslim,dfwhmkms,bvalmins,
     :     wloddr,whiddr,nminfd,nminfdef,ctypeul,chwv1,lcminul
*     upper limit results
      character*6 ionul
      double precision zvalul,bvalul,cvalul
      common/rdc_ulrparm/zvalul,bvalul,cvalul,ionul
*     binned profile
      character*2 chprof
      double precision pfinst
      double precision wfinstd
      integer npinst,npin2
      common/vpc_profwts/wfinstd(maxipp,maxnch),pfinst(maxipp,maxnch),
     :        npinst(maxnch),npin2(maxnch),chprof(maxnch)
*
*     only add lines to one region, so:
      ichunk=1
*     not a summed column density initially
      lsums=.false.
      chsumcol='  '
*     need to set wavelength coefficients so that vp_wval
*     will give the wavelength -- since it is the routine 
*     used by pvoigt.
*
*     add in the lines
*     ca comes out, cad,re go in
*     
*     Default limits:
      nsstartm=1
      nsend=nct
      collo=0.0d0
      colhi=1.0d25
      zedlo=-1000.0d0
      zedhi=1.0d25
      bvallo=0.0d0
      bvalhi=1.0d25
      bvall=0.0d0
*     counter for number of regions dealt with
      nreg=0
*     upper limit presets (def=no)
      loguc=.false.
*     steering variables, modified by character variable 10 in list
      lnohyd=.false.
      lnomet=.false.
      laonly=.false.
      lnosp=.false.
      lconly=.false.
      lcremove=.false.
      lchion=.false.
*     put in all metals independent of z
      lallmetz=.true.
*     line counters:
      nchyd=0
      ncall=0
*     absorption default
      indvar=1
*     set up the substeps, if requested
      if(nsubstep.gt.1) then
*       presets for vp_spvoigte
        if(noppsys.gt.3) then
          do jis=4,noppsys
            param(jis)=0.0d0
          end do
          do jis=1,noppsys
           ipind(jis)='  '
          end do
        end if
        nset=1
        np=3
*       subsection to use:
        write(6,*) 'wavelength region (from, to), nsubstep (or <uc)'
*       <uc does not work yet....
        read(inputc,'(a)',end=9981) chstr
        if(chstr(1:1).eq.'<') then
*         use upper limit variables
          loguc=.true.
          dvstr(1)=wloddr
          dvstr(2)=whiddr
          ivstr(3)=0
          write(6,'(a,2f11.2)') 'wavelength limits:',wloddr,whiddr
         else
          call dsepvar(chstr,3,dvstr,ivstr,cvstr,nvstr)
        end if
        call vp_chanwav(dvstr(1),chand,5.0d-2,20,1)
        nsstartm=int(chand+0.5d0)-1
        call vp_chanwav(dvstr(2),chand,5.0d-2,20,1)
        nsend=int(chand+0.5d0)
        if(ivstr(3).gt.1) nsubstep=ivstr(3)
*       length of subdivided array
        nsublen=(nsend-nsstartm)*nsubstep
        write(6,*) 'Substepped array length',nsublen
        if(nsublen.gt.maxsal) then
          nsubstep=maxsal/(nsend-nsstartm)
          write(6,*) 'Substep array length', nsublen,' >',
     :        maxsal,', the maximum allowed'
          write(6,'(a,i5)') 'Adjusting nsubstep to',nsubstep
          write(6,*) '"q" or "Q" to quit routine,'
          write(6,*) '      anything else or <CR> to continue'
          read(inputc,'(a)',end=9981) chstr
          if(chstr(1:1).eq.'q'.or.chstr(1:1).eq.'Q') then
            write(6,*) 'exiting routine'
            goto 9980
          end if
        end if
*       set up substep wavelengths
        do k=1,nsublen
          casub(k)=1.0d0
          cadsub(k)=1.0d0
          resub(k)=1.0d0
          kjb=(k-1)/nsubstep
          kloc=k-nsubstep*kjb
          dtemp=(dble(kloc)-0.5d0)/dble(nsubstep)
          dchs=dble(kjb+1+nsstartm)-0.5d0
          wvsubs(k)=(1.0d0-dtemp)*wval(dchs)+
     :        dtemp*wval(dchs+1.0d0)
        end do
       else
*       just do a direct copy
        nsublen=nct
        do k=1,nsublen
          wvsubs(k)=wval(dble(k))
          casub(k)=1.0d0
          cadsub(k)=1.0d0
          resub(k)=1.0d0
        end do
      end if
*     read in atomic data if necessary
      if(nz.le.0) call vp_ewred(0)
*
*     if uc set, bypass most of this, and just set what you want
      if(loguc) then
        col=cvalul
        zed=zvalul
        bval=bvalul
*       separate element/ionization level
        chstr=' '
        call vp_chsation(ionul,chstr,ion,level,next)
        klow=1
        khiw=nsublen
*       set elsewhere: ipind, nset=1, np=3, ichunk=1        
*       branch to profile generator
        chstr=ionul
*       get element and ionization separately:
        call vp_chsation(chstr,cvstr(24),ion,level,next)
        ncall=1
        jpin=5
        llist=.false.
        goto 991
      end if
*
 9998 write(6,*) 'ion,col,bval,zed?    ..  or ...'
*     list indicator (.true. if filename contains list of files)
      llist=.false.
      write(6,*) '<filename> (fmt,clo,chi,zlo,zhi,blo,bhi,binc,type)'
            write(6,*) '              [26,everything,everywhere]'
*     if input redirected, use it
      read(inputc,'(a)',end=9981) chstr
      if(chstr(1:1).eq.'?') then
        call rd_gphelp
        goto 9998
      end if
*     store for use as source of tick marks
      chmarkfl=chstr
*     attempt to split this
      call dsepvar(chstr,15,dvstr,ivstr,cvstr,nvstr)
*     check that file does not contain a list of filenames!
      if(cvstr(2)(1:4).eq.'list') then
        llist=.true.
        open(unit=20,file=cvstr(1),status='old',err=9998)
       else
        llist=.false.
      end if
 979  continue
      if(llist) then
        read(20,'(a)',end=980) cvstr(1)
      end if
      open(unit=13,file=cvstr(1),status='old',err=997)
      rewind(unit=13)
*     could open the file, so use it
      write(6,*) 'File opened: ',cvstr(1)(1:20)
      nstar=0
*     check for type:
      if(ivstr(2).eq.13) then
        write(6,*) 'fort.13 style file'
        ifst=13
       else
        write(6,*) 'fort.26 style file'
        ifst=26
      end if
*     further control variables may be present:
      if(nvstr.ge.2) then
*	minimum (log) column density for inclusion
        if(dvstr(3).gt.0.0d0) collo=dvstr(3)
*	and maximum
        if(dvstr(4).gt.0.0d0) colhi=dvstr(4)
*	minimum redshift included
        if(dvstr(5).gt.0.0d0) zedlo=dvstr(5)
*	and maximum
        if(dvstr(6).gt.0.0d0) zedhi=dvstr(6)
*	minimum b value included
        if(dvstr(7).gt.0.0d0) bvallo=dvstr(7)
*	and maximum
        if(dvstr(8).gt.0.0d0) bvalhi=dvstr(8)
*	with the opportunity of including every line if b < bvall
        if(dvstr(9).gt.0.0d0) bvall=dvstr(9)
*	line selection flags
        jjj=10
        do while(jjj.gt.1.and.ivstr(jjj).eq.0.and.
     :             cvstr(jjj)(1:1).eq.' ')
          jjj=jjj-1
        end do
        write(6,*) 'Preset continuum flags: ',cvstr(jjj)(1:4)
        if(cvstr(jjj)(1:1).ne.' ') then
*         how many characters?
          nccv=lastchpos(cvstr(jjj))
          j=0
          do while (j.lt.nccv)
            j=j+1
            if(cvstr(jjj)(j:j).eq.'c'.or.cvstr(jjj)(j:j).eq.'C') then
*	      plot ONLY the continuum adjustments
              lconly=.true.
            end if
            if(cvstr(jjj)(j:j).eq.'d'.or.cvstr(jjj)(j:j).eq.'D') then
*	      plot ALL BUT the continuum adjustments
              lcremove=.true.
            end if
            if(cvstr(jjj)(j:j).eq.'i'.or.cvstr(jjj)(j:j).eq.'I') then
*	      ignore special characters flag
              lnosp=.true.
            end if
            if(cvstr(jjj)(j:j).eq.'l'.or.cvstr(jjj)(j:j).eq.'L') then
*	      Ly-a only in regions specified (to speed things up)
              laonly=.true.
*	      need to suppress Lyman limit absorption with this option,
*	      so reset the internal parameter for this
              collsmin=1.0d25
            end if
            if(cvstr(jjj)(j:j).eq.'m'.or.cvstr(jjj)(j:j).eq.'M') then
*	      metals only flag
              lnohyd=.true.
            end if
            if(cvstr(jjj)(j:j).eq.'n'.or.cvstr(jjj)(j:j).eq.'N') then
*	      ignore metals flag
              lnomet=.true.
            end if
            if(cvstr(jjj)(j:j).eq.'s'.or.cvstr(jjj)(j:j).eq.'S') then
*	      choose a particular species
              lchion=.true.
*	      ask which one:
              write(6,*) 'Which ion?'
              read(5,'(a)') inchstr
              if(inchstr(1:1).eq.' ') then
*	        turn it off again!
                lchion=.false.
               else
*               determine the element 
*               nchlims used, with HD, CO exceptions allowed for
                if(ichar(inchstr(2:2)).ge.nchlims(3).and.
     :             ichar(inchstr(2:2)).le.nchlims(4).and.
     :             inchstr(1:2).ne.'HD'.and.
     :             inchstr(1:2).ne.'CO') then
                  chpelm=inchstr(1:1)//' '
                  chpion=inchstr(2:5)
                 else
                  chpelm=inchstr(1:2)
                  chpion=inchstr(3:6)
                end if
              end if
            end if
            if(cvstr(jjj)(j:j).eq.'z'.or.cvstr(jjj)(j:j).eq.'Z') then
*	      everything strictly selected by redshift
              lallmetz=.false.
            end if
            if(cvstr(jjj)(j:j).eq.'e'.or.cvstr(jjj)(j:j).eq.'E') then
*	      use emission lines only
              write(6,*) 'Emission lines'
              indvar=-1
            end if
          end do
        end if
      end if
 801  read(13,'(a)',end=9997) chstr
      if(chstr(1:1).eq.'!') goto 801
      jpin=13
*     special bit for fort.13 filenames:
      call dsepvar(chstr,1,dvstr,ivstr,cvstr,nvstr)
      if(ifst.eq.13.and.nstar.eq.0.and.cvstr(1)(1:1).eq.'*') then
        nstar=nstar+1
 909    read(13,'(a)',end=9997) chstr
        if(chstr(1:1).eq.'!') goto 909
        call dsepvar(chstr,15,dvstr,ivstr,cvstr,nvstr)
        if(cvstr(1)(1:1).ne.'*'.and.nreg.lt.1500) then
          nreg=nreg+1
          wvlo(nreg)=dvstr(3)
          wvhi(nreg)=dvstr(4)
*         channel numbers for region limits
          dtemp=dble(nct/2)
          call chanwav(wvlo(nreg),dtemp,5.0d-2,20)
          kalow(nreg)=int(dtemp+1.0d0)
          call chanwav(wvhi(nreg),dtemp,5.0d-2,20)
          kahiw(nreg)=int(dtemp)
          write(6,'(a)') chstr(1:60)
          goto 909
        end if
        nstar=nstar+1
      end if
      goto 991
*     type in line parameters
 997  jpin=5
      write(6,*) 'Terminates on <CR>'
 991  continue
      do while (chstr.ne.' ')
        if(loguc) goto 601
*	long process, so just to monitor progress:
        if(ncallset.gt.0) then
          if(jpin.eq.13.and.(ncall/ncallset)*ncallset.eq.ncall.and.
     :       ncall.gt.0.and.chstr(2:2).ne.'%') then
            write(6,'(i4,2x,a)') ncall,chstr(1:60)
          end if
         else
          write(6,'(i4,2x,a)') ncall,chstr(1:60)
        end if
*
        if(jpin.eq.5) then
          call vp_initval(chstr,ion,level,wavel,col,indcol,bval,
     1               indb,zed,indz)
         else
          inchstr=chstr
          call dsepvar(inchstr,15,dvstr,ivstr,cvstr,nvstr)
          if(cvstr(1)(1:2).eq.'%%') then
*           is filename, order number, wavelength limits
            if(nreg.eq.1500) then
*            if this happens, message is worth repeating!
              write(6,*) 'Exhausted region space'
              write(6,*) ' .. no more copied'
             else
              nreg=nreg+1
              wvlo(nreg)=dvstr(4)
              wvhi(nreg)=dvstr(5)
*             channel numbers for region limits
              dtemp=dble(nct/2)
              call chanwav(wvlo(nreg),dtemp,5.0d-2,20)
              kalow(nreg)=int(dtemp+1.0d0)
              call chanwav(wvhi(nreg),dtemp,5.0d-2,20)
              kahiw(nreg)=int(dtemp)
            end if
            goto 996
           else
*           uncomment if want to check if gets here:
            if(ncallset.le.0) write(6,*) col,zed,bval
            call vp_f13fin(ion,level,col,indcol,zed,indz,bval,indb,
     :               vtb,teff,lnk,chlnkv,ierr,ifst)
*           internal checks are logs, so..
            if(col.gt.35.0.and.indvar.ge.0) col=log10(col)
*           special summed column densities need to be unravelled
*           but not if zero level, continuum or velocity shift
            if((indcol(1:1).eq.'%'.or.(lcase(indcol(1:1)).and.
     :                              nocgt(indcol(1:1),lastch)))
     :           .and.indcol.ne.chsumcol.and.ion.ne.'__'.and.
     :           ion.ne.'<>'.and.ion.ne.'>>') then
*             is a summed column density - hold this one until
*             block completed, then subtract off the sum of the rest
*             of the block
              write(6,*) 'Summed column density',col
              lsums=.true.
              bvalsh=bval
              colsh=col
              zedsh=zed
              sumcol=0.0
              if(indcol(1:1).ne.'%') then
                chsumcol=indcol
              end if
            end if
            if(ierr.ne.0) then
              write(6,*) ' Unrecognized format:',chstr(1:40),'....'
              goto 996
            end if
          end if
        end if
*
*       continuum only? If so, skip if not '<>'
        if(lconly.and.(ion.ne.'<>')) goto 996
*       Or let through all which is not continuum:
        if(lcremove.and.(ion.eq.'<>')) goto 996
*
*       see if 'no special' flag set, and if it is, and the ion is
*       a reserved one, skip it
        if(lnosp) then
          if(ion.eq.'<>'.or.ion.eq.'__') goto 996
        end if
*       If hydrogen to be skipped, do so
        if(ion.eq.'H '.and.lnohyd) goto 996
*       If metals to be skipped, do so:
        if(ion.ne.'H '.and.ion.ne.'<>'.and.ion.ne.'__'.and.
     :        lnomet) goto 996
*
*       if reaches here, and not HI, put the line in anyway
        if(ion.ne.'H '.and.lallmetz) goto 981
*       check the parameters are within the limits, and skip if they are not
        if(bval.ge.bvall.and.(zed.lt.zedlo.or.zed.gt.zedhi.or.
     :       bval.lt.bvallo.or.bval.gt.bvalhi.or.col.lt.collo.or.
     :       col.gt.colhi)) goto 996
 
*       limits for range
 981    continue
*       skip if particular ion flag set, and ion not the one
        if(lchion.and.((chpelm.ne.ion).or.(chpion.ne.level))) goto 996
        klow=1
        khiw=nsublen
        if(ion.eq.'<>'.or.ion.eq.'__'.or.
     :               (ion.eq.'H '.and.laonly)) then
*         apply only to the region containing the "line"
          temp=1215.67d0*(1.0d0+zed)
          dtemp=dble(nct/2)
          do i=1,nreg
            if(temp.gt.wvlo(i).and.temp.lt.wvhi(i)) then
              klow=kalow(i)
              khiw=kahiw(i)
            end if
          end do
        end if
*       write out limits for fitting
*       write(6,*) ion,'  ',klow,khiw
        if(ion.eq.'H ') nchyd=nchyd+1
        ncall=ncall+1
*       uncomment if happy that lines are in
        if(ncallset.le.0) write(6,*) col,zed,bval
*       replacing:
*       call spvoigt(ca,cad,klow,khiw,
*     :            col,zed,
*     :            bval,ion,level,ichunk,re)
*       replaced by
*       noppsys etc set in vp_startval, so set for here
601     param(nppcol)=col
        param(nppzed)=zed
        param(nppbval)=bval
*        write(6,*) '>>',klow,khiw,nsublen,col,zed,bval,ion,level
*        write(6,*) '>>',nset,np,ichunk,nswres(1),'<<'
        call vp_spvoigte(casub, cadsub,klow,khiw,
     :            wvsubs,nsublen,param,ipind,
     :            ion,level,nset,np,ichunk,resub)
*       copy continuum for next line to go in
        do i=1,nsublen
          cadsub(i)=casub(i)
        end do
*       more data?
        if(loguc) goto 998
 996    read(jpin,'(a)',end=998) chstr
        if(chstr(1:1).eq.'!') goto 996
        goto 990
 998    chstr=' '
 990    continue
      end do
*     close file if open
      if(jpin.ne.5) close(unit=jpin)
      if(llist) then
        goto 979
      end if
*     instrument profile convolution
 978  write(6,*) ncall,' ions, ',nchyd,' HI'
      if(lsmth) then
        dlamh=0.0d0
*	suppress local polynomial fit by setting terms to zero:
        xlamx=5000.d0
        swres(2,ichunk)=0.0d0
        nswres(ichunk)=2
        if(loguc) then
*          swres(1,ichunk)=dfwhmkms
          swres(1,ichunk)=0.0d0
          npinst(ichunk)=0
         else
          swres(1,ichunk)=0.0d0
        end if
*	get instrument profile if needed:
        call vp_dlset(ichunk,xlamx,sigmah,dlamh)
*
        ndlen=nsublen
*        write(6,*) swres(1,1),swres(2,1),sigmah,nswres(1),ichunk
        call vp_subchspread(casub,1,ndlen,ichunk,wvsubs,nsublen,
     :      cadsub)
        do i=1,ndlen
          casub(i)=cadsub(i)
        end do
       else
*       don't smooth - just leave continuum as is, and say so
        write(6,*) 'No convolution with instrument profile'
        do i=1,nct
          cadsub(i)=casub(i)
        end do
      end if
*     Now need to copy back to the original bins if necessary
      if(nsubstep.gt.1) then
*       squeeze back in to original pixels
        ddscl=dble(nsubstep)
        do k=nsstartm+1,nsend
          jks=nsubstep*(k-nsstartm)
          jjs=jks-nsubstep+1
          temp=0.0d0
          do jis=jjs,jks
            temp=temp+casub(jis)
          end do
          ca(k)=ca(k)*temp/ddscl
          cad(k)=ca(k)
        end do
       else
*       just copy back, multiplying by the original continuum
        do k=1,nct
          ca(k)=casub(k)*ca(k)
          cad(k)=ca(k)
        end do
      end if
*     reset nsubstep to zero
 9980 nsubstep=0
*     remove pixel profile
      npinst(1)=0
      return
 9981 write(6,*) 'End of input stream - reset to terminal'
      inputc=5
      goto 9980
 9997 write(6,*)' Empty parameter file'
      goto 9998
 980  write(6,*) 'End of file list'
      close(unit=20)
      goto 978
      end
