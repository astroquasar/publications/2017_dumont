      subroutine vp_vreset(totit,num,nchunk,lcpend,nfile,parerr)
*     reset variables for a new fit to a new dataset
*     without resetting the variables controlling
*     dataset numbers
*
      implicit none
*
      include 'vp_sizes.f'
      double precision parerr(maxnpa)
      logical lcpend
*     logical lstopit
*     number of iterations
      integer totit,num
*     number of fitted regions
      integer nchunk
      integer nfile
*
*     Local:
      integer i
*
*     Common:
*     line add/removal flags and variables
      integer nnold,nnoldh,nnvold
      double precision chisqvcold
      common/vpc_prevstat/chisqvcold,nnold,nnoldh,nnvold
*     ill-conditioned flag
      integer isod,isodh
      character*2 ipind(maxnpa)
      common/vpc_usoind/ipind,isod,isodh
*     parameter array
      integer nlines,nn
      character*2 elem(maxnio)
      character*4 ionz(maxnio)
      double precision parm(maxnpa)
      common/vpc_parry/parm,elem,ionz,nlines,nn
*     rejected systems
      integer ndrop,kdrop,ndroptot,ndrwhy
      common/vpc_nrejs/ndrop,kdrop(maxnio),ndroptot,ndrwhy
*
      double precision chisqvh
      double precision probchb,probksb
      integer nladded
      common/vpc_problims/chisqvh,probchb,probksb,nladded
*     iteration and statistics for fit, for fort.26
      double precision chisqvc,prtotc
      integer iterc,nptsc,ndftotc
      common/vpc_smry/chisqvc,prtotc,iterc,nptsc,ndftotc
*
      logical literok
      double precision prtot
      common/vpc_endstat/prtot,literok
      logical literokold
      double precision prtotcold
      common/vpc_oldendstat/prtotcold,literokold
      double precision rlim
      common/vpc_svd /rlim
*     current data file and order number
      integer idrn,icrn
      character*64 filnm
      common/vpc_file14/filnm,idrn,icrn
*
      rlim=-1.0d0
*     number of iterations
      totit=0
      num=1
*     fit regions
      nchunk=0
*     ill-conditioned stuff, set isod to start value
      isod=isodh
*     tied stuff, and parameter values
      do i=1,maxnpa
        ipind(i)='  '
*       parameter errors
        parerr(i)=0.0d0
*       parameters
        parm(i)=0.0d0
      end do
*     parameter stuff
      nlines=0
      nn=0
*     internal loop check variables
      ndroptot=0
      ndrop=0
      ndrwhy=0
      ndroptot=0
      chisqvh=1.0d37
      nladded=0
*     fort.26 - changed 7.Nov.07 - was 1.020
      chisqvc=1.0d20
*
      nnold=-1
      nnvold=-1
      nnoldh=0
      chisqvcold=1.0d20
      literok=.false.
      literokold=.false.
*      lstopit=.false.
      lcpend=.false.
      nfile=1
*     data file associations
      idrn=0
      icrn=0
*     and refresh the atomic data, since might have been truncated.
      call vp_ewred(0)
      return
      end
