      subroutine rd_plcset(chc,ngp,da,de,drms,ca)
*
*     interpret single character from cursor window for interactive plotting
*     
*     IN:
*     ngp	integer	data array length
*     da	dble	data array
*     de	dble	error array
*     drms      dble    fluctuations array
*     ca        dble    continuum
*     OUT
*     chc	char*1	plot control (n if replot, y if OK)
*
*
      implicit none
      include 'vp_sizes.f'
*
      character*1 chc
      integer ngp
      double precision da(ngp),de(ngp),drms(ngp),ca(ngp)
*
      character*64 inchstr
      character*1 chcx,chvx
      character*8 chtemp
      character*2 ch2
      character*4 ch4
*     separated string
      integer nvs
      character*4 cvs(1)
      real rvs(1)
      integer ivs(1)
*
      integer i,j,jj,jcp,jlins,jlo,jhi
      integer igrwrad,m1p,m2m
      integer lch1,lch2,le1,le2,lencap
      integer nlen,ntot,ntx,nlstr,nllo,nxlen
      integer m1,m2,mg,mvals,mless,mtemp
      real rfacpg,y2pg
      double precision errest,bg,colg
      double precision c1,c2,slope
      double precision ddum,dtemp,dm,sm,dmall
      double precision flx,temp1,thres,temp2
      double precision resl,rms,sigd,sigt,sscal,taut,tol,temp
      double precision sumd,sumwd,sumt,sumwt,vdd,vdt,wtemp
      double precision xtemp1,xtemp2,ytemp1,ytemp2,wvmid,wvrest
      double precision wl,wl2,wtem,wdm,wtm,xtemp,yval,zloc,zla
*     for EW determination
      double precision ewobs,ewerr,wculo,wcuhi
      double precision wavrng,wtemplo,wtemphi,wxlo,wxhi
*     Functions
      integer lastchpos
      double precision wval,vpf_dvresn
*     Common:
      integer nwco
      double precision ac(maxwco)
      common/vpc_wavl/ac,nwco
      real x1pg,y1pg            ! pgplot variables
      common/vpc_gcurv/x1pg,y1pg
      integer l1,l2,lc1,lc2
      real fampg
      common/ppam/l1,l2,lc1,lc2,fampg
*     atomic data
      integer mnats
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),mnats
*     stack spectra variables
      integer nlins
      character*2 atom(64)
      character*4 ion(64)
      double precision rstwav(64)
      common/rdc_stplvar/atom,ion,rstwav,nlins
*     text label for a curve
      integer icoltext
      real xtextpg,ytextpg
      character*24 cpgtext
      common/pgtexts/xtextpg,ytextpg,cpgtext,icoltext
*     min and max yscales by hand, and default lower level
      integer nyuse
      real ylowhpg,yhihpg,yminsetpg
      common/pgylims/ylowhpg,yhihpg,yminsetpg,nyuse
      real yhihpgheld
      common/rdc_pgyscales/yhihpgheld
*     pgplot overplot with bias
      logical plgscl
      integer ipgov
      real pgbias
      common/pgover/ipgov,pgbias,plgscl
c     pgplot attributes: data, error, continuum, axes, ticks, RESIDUAL
c     1 color indx; 2 line style; 3 line width; 
c     4 curve(0), hist(1), bars(2); 5 - 10 reserved.
*     (10,1) cursor type
      integer ipgatt(10,9)
      common/vpc_pgattrib/ipgatt
*     plot array pointers (consistent with ipgatt)
*     0=don't,1=do for 1:data,2:error,3:cont,6:resid,9:rms
      integer ksplot(9)
      common/vpc_ksplot/ksplot
*     COMMON/PCTR/IW,IZ,IFO,IP  ! none of these used
      integer ipgopen,ipgflag
      common/pgplotv/ipgopen,ipgflag
*
*     ipgtgrk=1 if Greek letters, else 0 (-1 no symbols); 
*     yuptkf(def=0.9) is fract y-axis for top of tick,
*     ylentkf (def=0.07) fract y for length of tick
      integer ipgtgrk
      real yuptkpg,ylentkpg
      common/vpc_tick/ipgtgrk,yuptkpg,ylentkpg
*
      character*60 pglcapt
      real chszoldpg,chszpg
      common/rdc_pglcapt/pglcapt,chszoldpg,chszpg
      integer ipgnx,ipgny
      character*60 pglchar(3)
      common/pgnumba/ipgnx,ipgny,pglchar
*     plot/print velocity scale?
      logical lvel
      real wcenpg,vellopg,velhipg,zp1refpg
      common/vpc_pgvels/wcenpg,vellopg,velhipg,lvel,zp1refpg
*     ascii prinout instead of plot
      logical lascii,lplottoo
      common/vpc_pgascii/lascii,lplottoo
*     redirect next plot variables
      logical lsnap
      integer nsnap
      character*8 pgsfile,pghfile,pglfile
      common/rdc_pgsnap/lsnap,nsnap,pgsfile,pghfile,pglfile
      character*4 plsnaptype
      common/rdc_snaptype/plsnaptype
*     normalized residual plot
      real residpg, rshifpg
      common/vpc_resid/residpg,rshifpg
*     list of (up to 35) filenames with line lists for stacked plots
      integer ncufil,jcufil
      character*72 cufilnm(35)
      common/rdc_cufilnm/cufilnm,ncufil,jcufil
*     last file name
      character*80 cflname
      integer lastord
      logical lgflnm
      common/vpc_lastfn/cflname,lastord,lgflnm
*     region variables
      logical lmarkreg
      common/vpc_markreg/lmarkreg
*     line list variables
      double precision zmark
      character*132 chmarkfl
      logical lmarkl,lmarkto
      common/rdc_lmarklv/zmark,chmarkfl,lmarkl,lmarkto
*     file variables for O/P for subsequent vpfit run:
      logical lchg13
      character*80 chg13file
      common/rdc_chg13/chg13file,lchg13
      integer lrformt,lr1,lr2,lr3 
      common/rdc_lintvar/lrformt,lr1,lr2,lr3
*     maximum redshift for table match
      double precision zidmax
      common/rdc_zidmax/zidmax
*     observed wavelengths?
      logical lobswav,ltick
      common/rdc_lobswav/lobswav,ltick
*     resolution previous values:
      double precision sigmah,dlamh
      common/rdc_poldres/sigmah,dlamh
*     last line
      character*2 prevatom
      character*4 previon
      double precision prevrestwave
      common/vpc_previons/prevrestwave,previon,prevatom
*     ion/wavelength list as typed in
      integer ntrwave
      character*2 tatom(16)
      character*4 tion(16)
      double precision trwave(16)
      common/rdc_intvtab/trwave,ntrwave,tion,tatom
*     default field length
      nlen=0
      jlins=0
*
      errest=0.0
*
 1    call vp_pgcurs(x1pg,y1pg,chc)
*
*     help requested, so:
      if(chc.eq.'?') then
        write(6,*) 'Left mouse button, or "e", expands plot'
        write(6,*) 'Center button, or "r" replots'
        write(6,*) 'Right button, "q" or "Q" to exit'
        write(6,*) '" " print wavelength, flux at cursor position'
        write(6,*) '"." shift range up'
        write(6,*) '"," shift range down'
        write(6,*) '"=" next velocity plot to a postscript file'
        write(6,*) '"-" flag as bad data'
        write(6,*) '"{" Ly-a max redshift at cursor position'
        write(6,*) '"[" table match (short)'
        write(6,*) '"@" show/hide ref. wavelengths'
        write(6,*) '"a" plot whole array'
        write(6,*) '"b" mark region boundaries (B,o)'
        write(6,*) '"d" demagnify by factor 2'
*	              "e" mark edges
        write(6,*) '"f" fraction of pixels above cursor'
        write(6,*) '"g" next plot data, cont, error+rms, '
        write(6,*) '     or just data, cont, error (toggles)'
        write(6,*) '"h" print line parameters for Ly-a (H)'
        write(6,*) '"i" interpolate error'
        write(6,*) '"j" change y of x-point nearest cursor'
        write(6,*) '"k" change error to cursor y-value at that point'
        write(6,*) '"l" print line parameters (L)' 
        write(6,*) '"m" wavc, me/rms, mean, rms, mean error'
*	              "o" as "b", with output to file
        write(6,*) '"t" velocity ticks on/off [M overrides "on"]'
        write(6,*) '"u" renormalized velocity scale'
        write(6,*) '"v" velocity scale about cursor position'
        write(6,*) '"w" wavelength scale'
        write(6,*) '"x" interpolate data'
        write(6,*) '"y" max y from cursor'
        write(6,*) '"z" change continuum at point'
        write(6,*) '"*" print a *'
        write(6,*) '"<CR>" print a blank line'
*	              "A" is left button
*	              "B" as "b", but in f26 format
        write(6,*) '"C" overplot continuum'
*	              "D" is center button
        write(6,*) '"E" replace error'
        write(6,*) '"F" change data format'
        write(6,*) '"G" plot data, continuum and error'
*	              "H" line parameters as Ly-a
        write(6,*) '"I" interpolate continuum'
        write(6,*) '"J" change y of continuum point nearest cursor'
        write(6,*) '"K" suppress velocity plot caption (toggles)'
*                   "L" line parameters
        write(6,*) '"M" mark lines'
        write(6,*) '"N" no line marking'
*	              "O" as "B", with output to file
        write(6,*) '"P" command line prompt'
        write(6,*) '"R" replace continuum by mean'
        write(6,*) '"S" suppress error, plot data only'
        write(6,*) '"T" tick mark lines'
        write(6,*) '"U" flag as bad data with errors above cursor'
        write(6,*) '"V" velocity scale, using old redshift'
*	              "X" is right button
        write(6,*) '"W" estimate equivalent width'
        write(6,*) '"Z" mark lines at reference redshift'
        write(6,*) '"0" default/extended cursor type (toggles)'
        write(6,*) '"1" extended table search'
        write(6,*) '"7" extended cross-hair cursor'
        write(6,*) '"!" unknown line'
*                     ":" fraction of pixels below 1st y-cursor level
*                     "#" Heckman et al ApJ 577, 691, 2002 b estimate
        write(6,*) '"~" snap to .gif'
*                     "_" baseline shift
*                     "/" lower limit for plot (and plot straight away)
        write(6,*) '"}" indicate wavelength regions used (toggles)'
        write(6,*) '            (uses same color as residual plot)'
        write(6,*) 
     :       '.. any other lower case letter for command line prompt'
*       still available: c g n p s  
*	upper case: (W)
*	A D X are left, center right mouse buttons
*
*	Others used: = ? * . , [ ^ @ ~ : ! # _ / { }
        goto 1
      end if
*     replot is centre button
      if(chc.eq.'D'.or.chc.eq.'r') then
        chc='n'
        goto 3
      end if
*     Exit is right button:
      if(chc.eq.'X'.or.chc.eq.'q'.or.chc.eq.'Q') then
        chc='y'
        goto 3
      end if
*     
*     e: set edges of display from cursor & left button, and replot
      if(chc.eq.'A'.or.chc.eq.'e') then
*       wavelength to channel conversion, rfc 13.4.92
*       modified for velocity input rfc 02.11.99
        if(lvel.or.x1pg.lt.0.0) then
*         convert velocity to wavelength first, using wcentral
*         determined from y-position
          lvel=.true.
          jlins=int(y1pg+1.0)
          if(jlins.le.0) jlins=1
          if(jlins.gt.nlins) jlins=nlins
          wcenpg=real(rstwav(jlins))*zp1refpg
          wl=wcenpg*sqrt((2.99792458e5+x1pg)/(2.99792458e5-x1pg))
*         and set last atom/ion/wavelength
          prevatom=atom(jlins)
          previon=ion(jlins)
          prevrestwave=rstwav(jlins)
         else
*         was wavelength plot anyway
          wl=x1pg
        end if
        if(ac(2).ge.0.0d0) then
          ddum=(wl-ac(1))/ac(2)
         else
          ddum=1.0d3
        end if
        call chanwav(wl,ddum,1.0d-3,100)
        l1 = nint(ddum)
        write(6,*) ' Channel',l1
        if(chc.eq.'A') then
*	  set scale range as well
          lc1=l1
        end if
        write(6,*) 'right edge..'
        call vp_pgcurs(x1pg,y1pg,chcx)
        if(lvel) then
*         use previous velocity parameters
          wl=wcenpg*sqrt((2.99792458e5+x1pg)/(2.99792458e5-x1pg))
         else
          wl=x1pg
        end if
        if(ac(2).ge.0.0d0) then
          ddum=(wl-ac(1))/ac(2)
         else
          ddum=1.0d3
        end if
        call chanwav(wl,ddum,1.0d-3,100)
        l2 = nint( ddum )
        write(6,*) ' Channel',l2
        if(chc.eq.'A') then
*	  set max for scaling as well
          lc2=l2
        end if
        chc='n'
*       reset caption & turn off velocity information
        if(pglcapt(1:1).ne.' ') then
          write(pglchar(3),'(a)') pglcapt
         else
          if(nlen.gt.0) then
            write(pglchar(3),'(a)') cflname(1:nlen)
          end if
        end if
        lvel=.false.
*	reset colour
        ipgatt(1,1)=1
        goto 3
      end if
*
*     other commands similar to IRAF splot
*
*     print wavelength, flux
      if(chc.eq.' '.or.chc.eq.'{') then
        if(lvel) then
*         velocity to wavelength conversion as well
          jlins=int(y1pg+1.0)
          if(jlins.le.0) jlins=1
          if(jlins.gt.nlins) jlins=nlins
          wcenpg=rstwav(jlins)*zp1refpg
          wl=wcenpg*sqrt((2.99792458e5+x1pg)/(2.99792458e5-x1pg))
*         redshift as well, just for info
          zloc=wl/rstwav(jlins)-1.0d0
          zla=wl/1215.6701d0-1.0d0
          write(6,*) wl,'   z=',zloc,'  ',x1pg,y1pg,'   zla= ',zla
          if(chc.eq.'{') then
            zidmax=zla
          end if
         else
          write(6,*) x1pg,y1pg
          if(chc.eq.'{') then
            zidmax=x1pg/1215.67-1.0
            write(6,*) 'zla= ',zidmax
          end if
        end if
        goto 1
      end if
*
*     .: step up
      if(chc.eq.'.') then
        if(l2.ge.ngp) then
*         no point in doing this, so don't
          write(6,*) 'Max channel already plotted'
         else
          if(lvel) then
*	    velocity plot -- just use the cursor position
            chc='v'
            goto 393
           else
            ntot=l2-l1
            ntx=ntot/10
            l1=l2-ntx
            l2=l1+ntot
            if(l2.gt.ngp) then
              l2=ngp
              l1=l2-ntot
              write(6,*) 'Max channel displayed'
            end if
            lc1=l1
            lc2=l2
          end if
        end if
        chc='n'
        goto 3
      end if
*
*     ,: step down
      if(chc.eq.',') then
        if(l1.le.1) then
*	  no point in doing this, so don't
          write(6,*) 'Min channel already plotted'
         else
          if(lvel) then
*	    velocity plot -- just use the cursor position
            chc='v'
            goto 393
           else
            ntot=l2-l1
            ntx=ntot/10
            l2=l1+ntx
            l1=l2-ntot
            if(l1.lt.1) then
              l1=1
              l2=ntot+1
              write(6,*) 'Minimum channel displayed'
            end if
            lc1=l1
            lc2=l2
          end if
        end if
        chc='n'
        goto 3
      end if
*
*     =: .snap the next velocity plot, to pgp1nn.ps as /vcps
      if(chc.eq.'=') then
        lsnap=.true.
        plsnaptype='ps  '
        chc='n'
        goto 1
      end if
*     ~: snap the next velocity plot, to pgp1nn.gif as /gif
      if(chc.eq.'~') then
        lsnap=.true.
        plsnaptype='gif '
        chc='n'
        goto 1
      end if
*
*     -: bad data flag
      if(chc.eq.'-') then
*       wavelength to channel conversion, rfc 13.4.92
*       modified for velocity input rfc 02.11.99
        if(lvel.or.x1pg.lt.0.0d0) then
*         convert velocity to wavelength first, using wcentral
*         determined from y-position
          lvel=.true.
          jlins=int(y1pg+1.0d0)
          if(jlins.le.0) jlins=1
          if(jlins.gt.nlins) jlins=nlins
          wcenpg=rstwav(jlins)*zp1refpg
          wl=wcenpg*sqrt((2.99792458e5+x1pg)/(2.99792458e5-x1pg))
         else
*         was wavelength plot anyway
          wl=x1pg
        end if
        if(ac(2).ge.0.0d0) then
          ddum=(wl-ac(1))/ac(2)
         else
          ddum=1.0d3
        end if
        call chanwav(wl,ddum,1.0d-3,100)
        le1 = nint(ddum)
        write(6,*) ' Wavelength',wl,', Channel',le1
        write(6,*) 'right edge..'
        call vp_pgcurs(x1pg,y1pg,chcx)
        if(lvel) then
*	  use previous velocity parameters
          wl=wcenpg*sqrt((2.99792458e5+x1pg)/(2.99792458e5-x1pg))
         else
          wl=x1pg
        end if
        if(ac(2).ge.0.0d0) then
          ddum=(wl-ac(1))/ac(2)
         else
          ddum=1.0d3
        end if
        call chanwav(wl,ddum,1.0d-3,100)
        le2 = nint( ddum )
        write(6,*) ' Wavelength',wl,', Channel',le2
*       set errors negative
        do jj=le1,le2
          de(jj)=-1.0d0
          drms(jj)=-1.0d0
        end do
        chc='n'
        goto 1
      end if
*     <CR> is merely echoed (with a space)
      if(ichar(chc).eq.13) then
        write(6,*) ' '
        goto 1
      end if
*     '*' is echoed
      if(chc.eq.'*') then
        write(6,*) '*'
        goto 1
      end if
*
*     Letter commands
*
*     a: plot full range
      if(chc.eq.'a') then
        l1=1
        lc1=1
        l2=ngp
        lc2=ngp
*       turn off velocity information
        if(pglcapt(1:1).ne.' ') then
          write(pglchar(3),'(a)') pglcapt
         else
          write(pglchar(3),'(a)') cflname(1:nlen)
        end if
*       reset colour
        ipgatt(1,1)=1
        lvel=.false.
        chc='n'
        goto 3
      end if
*
*     b: boundaries listed from velocity plot
      if(chc.eq.'b'.or.chc.eq.'B'.or.chc.eq.'o'.or.
     :    chc.eq.'O') then
        if(lrformt.eq.0) then
*         change case of this letter
          if(chc.eq.'b'.or.chc.eq.'o') then
            if(chc.eq.'b') then
              chc='B'
             else
              chc='O'
            end if
           else
            if(chc.eq.'B') then
              chc='b'
             else
              chc='o'
            end if
          end if
        end if
*
        if(lvel.or.x1pg.lt.0.0) then
*	  convert velocity to wavelength first, using wcentral
*	  determined from y-position
          lvel=.true.
          jlins=int(y1pg+1.0)
          if(jlins.le.0) jlins=1
          if(jlins.gt.nlins) jlins=nlins
          wcenpg=rstwav(jlins)*zp1refpg
          wl=wcenpg*sqrt((2.99792458e5+x1pg)/(2.99792458e5-x1pg))
         else
*	  was wavelength plot anyway
          wl=x1pg
        end if
        call vp_pgcurs(x1pg,y1pg,chcx)
        if(lvel) then
*	  use previous velocity parameters
          wl2=wcenpg*sqrt((2.99792458e5+x1pg)/(2.99792458e5-x1pg))
         else
          wl2=x1pg
        end if
        nlen=80
        do while(cflname(nlen:nlen).eq.' '.and.nlen.gt.1)
          nlen=nlen-1
        end do
*       switch order if wl2 less than wl
        if(wl.gt.wl2) then
          temp=wl
          wl=wl2
          wl2=temp
        end if
        if(chc.eq.'o') then
          write(6,*) '*'
        end if
        if(chc.eq.'B'.or.chc.eq.'O') then
          write(6,'(a,a,a,i4,2f12.4)') 
     1          '%% ',cflname(1:nlen),' ',lastord,wl,wl2
         else
          write(6,'(a,a,i4,2f12.4)') 
     1         cflname(1:nlen),' ',lastord,wl,wl2
        end if
        if(chc.eq.'o') then
          write(6,*) '*'
          write(6,*) ' '
        end if
        if(chc.eq.'o'.or.chc.eq.'O') then
          write(6,*) ' '
        end if
*	O/P to file
        if(chg13file(1:1).ne.' ') then
          if(.not.lchg13) then
*	    open the file
            open(unit=22,file=chg13file,status='unknown',err=944)
            lchg13=.true.
          end if
          goto 945
 944      write(6,*) 'VPFIT start file not opened!'
 945      if(lchg13) then
            write(22,'(a,a,a,i4,2f12.4)') 
     1       '%% ',cflname(1:nlen),' ',lastord,wl,wl2
          end if
        end if
        chc='n'
        goto 1
      end if
*
*     d: plot twice the range, centered on old value
      if(chc.eq.'d') then
        lc1=l1
        l1=3*l1/2-l2/2
        if(l1.lt.1) l1=1
        l2=3*l2/2-lc1/2+2
        if(l2.gt.ngp) l2=ngp
        lc1=l1
        lc2=l2
*       turn off velocity information
        nlen=80
        do while(cflname(nlen:nlen).eq.' '.and.nlen.gt.1)
          nlen=nlen-1
        end do
        if(pglcapt(1:1).ne.' ') then
          write(pglchar(3),'(a)') pglcapt
         else
          write(pglchar(3),'(a)') cflname(1:nlen)
        end if
*       reset colour
        ipgatt(1,1)=1
        lvel=.false.
        chc='n'
        goto 3
      end if
*
*     f: fraction of data above the cursor value, in range set
*        by two cursor positions
      if(chc.eq.'f') then
        wl=x1pg
        flx=y1pg
        if(ac(2).ge.0.0d0) then
          ddum=(wl-ac(1))/ac(2)
         else
          ddum=1.0d3
        end if
        call chanwav(wl,ddum,1.0d-3,100)
        m1 = nint(ddum)
        if(m1.le.0) m1=1
        write(6,*) 'right edge..'
        call vp_pgcurs(x1pg,y1pg,chcx)
        wl=x1pg
        if(ac(2).ge.0.0d0) then
          ddum=(wl-ac(1))/ac(2)
         else
          ddum=1.0d3
        end if
        call chanwav(wl,ddum,1.0d-3,100)
        m2 = nint( ddum )
        if(m2.gt.ngp) m2=ngp
*       swap if necessary
        if(m2.lt.m1) then
          mvals=m2
          m2=m1
          m1=mvals
        end if
        mg=0
        do j=m1,m2
          if(da(j).gt.flx) then
            mg=mg+1
          end if
        end do
        mvals=m2-m1+1
        x1pg=real(mg)/real(mvals)
        write(6,*) 'Fraction ',x1pg,' above ',flx
        write(6,*) mg,' channels out of ',mvals
        goto 1
      end if
*
*     g: plot everything: data, continuum, error, rms
*           toggles back to date, error, continuum
      if(chc.eq.'g') then
*       set steering variables
        ksplot(1)=1
        ksplot(2)=1
        ksplot(3)=1
        if(ksplot(9).ne.1) then
          ksplot(9)=1
          write(6,*) 'Plotting error and RMS'
         else
          write(6,*) 'Plotting error, not RMS'
          ksplot(9)=0
        end if
        goto 1
      end if
*
*     h: print line information as Ly-a (velocity plot)
      if(chc.eq.'h'.or.chc.eq.'H') then
        if(lrformt.eq.0) then
*	  change case of this letter
          if(chc.eq.'h') then
            chc='H'
           else
            chc='h'
          end if
        end if
        if(lvel) then
*	  velocity to wavelength conversion as well
          jlins=int(y1pg+1.0)
          if(jlins.le.0) jlins=1
          if(jlins.gt.nlins) jlins=nlins
          wcenpg=rstwav(jlins)*zp1refpg
          wl=wcenpg*sqrt((2.99792458e5+x1pg)/(2.99792458e5-x1pg))
*	  redshift as well, just for info
          zloc=wl/1215.6701d0-1.0d0
          call rd_estcolb(da,de,ca,ngp,'H ','I   ',
     :         1215.6701d0,wl,colg,bg)
          if(colg.gt.0.0d0) then
            if(chc.eq.'h') then
*	      fort.13 style
              write(6,'(a2,a4,f7.3,f10.6,f8.2)') 'H ','I   ',
     :            colg,zloc,bg
             else
*	      fort.26 style
              write(6,'(a2,a4,2f10.6,1x,2f8.2,2f7.3)')
     :            'H ','I   ',zloc,errest,bg,errest,colg,errest
            end if
            if(lchg13) then
              write(22,'(a2,a4,2f10.6,1x,2f8.2,2f7.3)')
     :            'H ','I   ',zloc,errest,bg,errest,colg,errest
            end if
          end if
         else
*	  HI specified, so can estimate
          wl=x1pg
          zloc=wl/1215.6701d0-1.0d0
          call rd_estcolb(da,de,ca,ngp,'H ','I   ',
     :         1215.6701d0,wl,colg,bg)
          if(colg.gt.0.0d0) then
            if(chc.eq.'h') then
*	      fort.13 style
              write(6,'(a2,a4,f7.3,f10.6,f8.2)') 'H ','I   ',
     :            colg,zloc,bg
             else
*	      fort.26 style, with errors as zeros for now
              write(6,'(a2,a4,2f10.6,1x,2f8.2,2f7.3)')
     :            'H ','I   ',zloc,errest,bg,errest,colg,errest
            end if
            if(lchg13) then
              write(22,'(a2,a4,2f10.6,1x,2f8.2,2f7.3)')
     :            'H ','I   ',zloc,errest,bg,errest,colg,errest
            end if
           else
            write(6,*) x1pg,y1pg
          end if
        end if
        goto 1
      end if
*
*     j: change data value
      if(chc.eq.'j'.or.chc.eq.'J'.or.chc.eq.'k') then
        if(lvel) then
          write(6,*) 'Replot on wavelength scale first'
*	  turn off velocity information
          nlen=80
          do while(cflname(nlen:nlen).eq.' '.and.nlen.gt.1)
            nlen=nlen-1
          end do
          if(pglcapt(1:1).ne.' ') then
            write(pglchar(3),'(a)') pglcapt
           else
            write(pglchar(3),'(a)') cflname(1:nlen)
          end if
          lvel=.false.
         else
*	  wavelength to channel conversion
          wl=x1pg
          if(ac(2).ge.0.0d0) then
            ddum=(wl-ac(1))/ac(2)
           else
            ddum=1.0d3
          end if
          call chanwav(wl,ddum,1.0d-3,100)
          m1 = nint( ddum )
          if(chc.eq.'j') then
*	    change the data
            da(m1)=y1pg
           else
            if(chc.eq.'k') then
*             change error               
              de(m1)=y1pg**2
              if(yhihpgheld.gt.0.0) then
                yhihpg=yhihpgheld
                nyuse=1
              end if
             else
*	      change the continuum
              ca(m1)=y1pg
            end if
          end if
        end if
        chc='n'
        goto 3
      end if
*
*     l: print line information (velocity plot)
      if(chc.eq.'l'.or.chc.eq.'L') then
        if(lrformt.eq.0) then
*         change case of this letter
          if(chc.eq.'l') then
            chc='L'
           else
            chc='l'
          end if
        end if
        if(lvel) then
*	  velocity to wavelength conversion as well
          jlins=int(y1pg+1.0)
          if(jlins.le.0) jlins=1
          if(jlins.gt.nlins) jlins=nlins
          wcenpg=rstwav(jlins)*zp1refpg
          wl=wcenpg*sqrt((2.99792458e5+x1pg)/(2.99792458e5-x1pg))
*	  redshift as well, just for info
          zloc=wl/rstwav(jlins)-1.0d0
          zla=wl/1215.6701d0-1.0d0
          call rd_estcolb(da,de,ca,ngp,atom(jlins),ion(jlins),
     :         rstwav(jlins),wl,colg,bg)
          if(colg.gt.0.0d0) then
            if(chc.eq.'l') then
*	      fort.13 style
              write(6,'(a2,a4,f7.3,f10.6,f8.2)') atom(jlins),
     :            ion(jlins),colg,zloc,bg
             else
*	      fort.26 style
              write(6,'(a2,a4,2f10.6,1x,2f8.2,2f7.3)')
     :          atom(jlins),ion(jlins),zloc,errest,bg,errest,
     :            colg,errest
            end if
            if(lchg13) then
              write(22,'(a2,a4,2f10.6,1x,2f8.2,2f7.3)')
     :          atom(jlins),ion(jlins),zloc,errest,bg,errest,
     :          colg,errest
            end if
          end if
         else
*         use the last line
          if(prevatom.eq.'  ') then
            write(6,*) 'No line set - using Ly-a'
            prevatom='H '
            previon='I   '
            prevrestwave=1215.6701
          end if
          wl=x1pg
          zloc=wl/prevrestwave-1.0d0
          zla=wl/1215.6701d0-1.0d0
          call rd_estcolb(da,de,ca,ngp,prevatom,previon,
     :         prevrestwave,wl,colg,bg)
          if(colg.gt.0.0d0) then
            if(chc.eq.'l') then
*	      fort.13 style
              write(6,'(a2,a4,f7.3,f10.6,f8.2)') prevatom,
     :            previon,colg,zloc,bg
             else
*	      fort.26 style
              write(6,'(a2,a4,2f10.6,1x,2f8.2,2f7.3)')
     :          prevatom,previon,zloc,errest,bg,errest,
     :            colg,errest
            end if
            if(lchg13) then
              write(22,'(a2,a4,2f10.6,1x,2f8.2,2f7.3)')
     :          prevatom,previon,zloc,errest,bg,errest,
     :          colg,errest
            end if
           else
            write(6,*) x1pg,y1pg
          end if
        end if
        goto 1
      end if
*
*     m: wavc, me/rms, mean, rms, mean error
*     E: replace error by value at that point
*     R: replace continuum in the range by mean value
      if(chc.eq.'m'.or.chc.eq.'E'.or.chc.eq.'R') then
        wl=x1pg
        if(ac(2).ge.0.0d0) then
          ddum=(wl-ac(1))/ac(2)
         else
          ddum=1.0d3
        end if
        call chanwav(wl,ddum,1.0d-3,100)
        m1 = nint( ddum )
        if(m1.le.0) m1=1
        write(6,*) 'right edge..'
        call vp_pgcurs(x1pg,y1pg,chcx)
        wl=x1pg
        if(ac(2).ge.0.0d0) then
          ddum=(wl-ac(1))/ac(2)
         else
          ddum=1.0d3
        end if
        call chanwav(wl,ddum,1.0d-3,100)
        m2 = nint( ddum )
        if(m2.gt.ngp) m2=ngp
*       swap if necessary
        if(m2.lt.m1) then
          mvals=m2
          m2=m1
          m1=mvals
        end if
*       compute mean etc
        dm=0.0d0
        sm=0.0d0
        dmall=0.0d0
*       remember that de is VARIANCE
        mvals=0
        do j=m1,m2
          dmall=dmall+da(j)
          if(de(j).gt.0.0d0) then
            dm=dm+da(j)
            sm=sm+sqrt(de(j))
            mvals=mvals+1
          end if
        end do
        if(mvals.gt.1) then
*	  mean value and mean error
          dm=dm/dble(mvals)
          sm=sm/dble(mvals)
          if(mvals*2.lt.m2-m1) then
            write(6,*) 'Over half the data in this region is bad'
          end if
          rms=0.0d0
          do j=m1,m2
            if(de(j).gt.0.0d0) then
              rms=(da(j)-dm)**2+rms
            end if
          end do
          rms=rms/dble(mvals)
         else
          write(6,*) 'No good data in region'
          dm=dmall/dble(m2+1-m1)
          sm=-1.0d0
          rms=0.0d0
          do j=m1,m2
            rms=(da(j)-dm)**2+rms
          end do
          rms=rms/dble(m2+1-m1)
        end if
        rms=sqrt(rms)
        if(rms.gt.0.0d0) then
          sscal=sm/rms
         else
          sscal=1.0
        end if
        wvmid=wval(dble(m1+m2)/2.0d0)
        if(chcx.eq.'h'.or.chcx.eq.'H') then
*         write out a fuller description 
          write(6,*) 'Mean wavelength, sigma scale, mean value,'//
     :                 '  measured rms, mean sigma' 
        end if
        write(6,*) wvmid,sscal,dm,rms,sm
        if(chc.eq.'E') then
*         error array replacement
          ddum=1.0d3
          call chanwav(wvmid,ddum,1.0d-3,100)
          m1 = nint( ddum )
          if(m1.le.0) m1=1
          de(m1)=rms*rms
        end if
        if(chc.eq.'R') then
*         continuum replacement
          do j=m1,m2
            ca(j)=dm
          end do
        end if
        chc='n'
        goto 1
      end if
*
*     s: substitute line in list with ID from this region as plotted
      if(chc.eq.'s') then
        goto 1
      end if
*
*     t: velocity tick suppression
      if(chc.eq.'t') then
        if(ltick) then
          ltick=.false.
         else
          ltick=.true.
        end if
        goto 1
      end if
* 
*     u: renormalized velocity scale
      if(chc.eq.'u') then
        plgscl=.true.
        write(6,*) 'velocity plot rescaled'
        chc='v'
      end if
*
*     v: velocity scale, centered on cursor position
*     V:    "       "      "       " given redshift
*     for the wavelength range currently plotted
 393  continue
      if(chc.eq.'v'.or.chc.eq.'V') then
        if(ncufil.le.0.and.ntrwave.le.0) then
          write(6,*) 'No velocity information'
          goto 1
        end if
        if(lvel) then
*	  have a velocity plot already, so need a different approach
*	  determine the line from the vertical cursor position:
          jlins=int(y1pg+1.0)
          if(jlins.le.0) jlins=1
          if(jlins.gt.nlins) jlins=nlins
*	  and redshift from  x-cursor position
          rfacpg=sqrt((2.99792458e5+x1pg)/(2.99792458e5-x1pg))
          if(abs(x1pg).lt.2.995E5.and.chc.eq.'v') then
            zp1refpg=rfacpg*zp1refpg
          end if
          wcenpg=rstwav(jlins)*zp1refpg
          wvrest=rstwav(jlins)
*	  determine the channel numbers corresponding to vello and velhi
          dtemp=dble(wcenpg*sqrt((2.99792458e5+vellopg)/
     :            (2.99792458e5-vellopg)))
          ddum=dble(l1)
          call chanwav(dtemp,ddum,1.0d-3,100)
          l1=int(ddum+0.5d0)
          dtemp=dble(wcenpg*sqrt((2.99792458e5+velhipg)/
     :             (2.99792458e5-velhipg)))
          ddum=dble(l1)
          call chanwav(dtemp,ddum,1.0d-3,100)
          l2=int(ddum+0.5d0)
          if (.not.lgflnm) then
            pglchar(3)=' '
          end if
         else
          wcenpg=x1pg
*	  further characters to represent files
          write(6,*) 'File ID?'
          do j=1,ncufil
            nlen=72
            do while(cufilnm(j)(nlen:nlen).eq.' '.and.nlen.gt.1)
              nlen=nlen-1
            end do
*           strip off the path bit
            nllo=nlen
            do while(nllo.gt.0.and.cufilnm(j)(nllo:nllo).ne.'/')
              nllo=nllo-1
            end do
            nllo=nllo+1
            nllo=min(nllo,nlen)
            nllo=max(nllo,1)
            if(j.le.9) then
              write(6,'(i3,3x,a)') j,cufilnm(j)(nllo:nlen)
             else
              chvx=char(j-10+ichar('a'))
              write(6,'(2x,a1,3x,a)')chvx,cufilnm(j)(nllo:nlen)
            end if
          end do
          write(6,*) ' .. in plot window'
          nxlen=1
          call rd_cuchstr(inchstr,nlstr,nxlen)
          jcufil=1
          if(nlstr.le.0.or.inchstr(1:4).eq.'    ') then
*           default is set 1
            jcufil=1
           else
*	    rfc 8.6.00: string separator changed from internal read
            call sepvar(inchstr,1,rvs,ivs,cvs,nvs)
            jcufil=ivs(1)
            if(cvs(1)(1:1).ne.' '.and.cvs(1)(1:1).ne.'0'
     :              .and.jcufil.le.0) then
              jcufil=ichar(cvs(1)(1:1))-ichar('a')+10
            end if
            if(jcufil.le.0) jcufil=0
            if(jcufil.gt.ncufil) jcufil=max0(ncufil,1)
          end if
          call rd_cuwvrest(wvrest)
          zp1refpg=wcenpg/real(wvrest)
        end if
*       write(6,*) 'jcufil',jcufil
        vellopg=zp1refpg-1.0
*       write(6,*) 'Redshift = ',vellopg
        nlen=80
        do while(cflname(nlen:nlen).eq.' '.and.nlen.gt.1)
          nlen=nlen-1
        end do
        if(lgflnm) then
          if(pglcapt(1:1).ne.' ') then
            lencap=lastchpos(pglcapt)
            write(pglchar(3),'(a,'' z ='',f9.6)') 
     :         pglcapt(1:lencap),vellopg
           else
            write(pglchar(3),'(a,'' z ='',f9.6)') 
     :       cflname(1:nlen),vellopg
          end if
         else
          pglchar(3)=' '
        end if
        vellopg=(real(wval(dble(l1)))/wcenpg)**2
        velhipg=(real(wval(dble(l2)))/wcenpg)**2
        vellopg=2.99792458e5*(vellopg-1.0)/(vellopg+1.0)
        velhipg=2.99792458e5*(velhipg-1.0)/(velhipg+1.0)
        lvel=.true.
        goto 3
      end if
*
*     w: wavelength scale plot
      if(chc.eq.'w') then
        if(lvel) then
*	  get plot limits appropriate for the line chosen
          jlins=int(y1pg+1.0)
          if(jlins.le.0) jlins=1
          if(jlins.gt.nlins) jlins=nlins
          wcenpg=rstwav(jlins)*zp1refpg
          wtem=wcenpg*sqrt((2.99792458e5+vellopg)/
     :           (2.99792458e5-vellopg))
          if(ac(2).ge.0.0d0) then
            ddum=(wtem-ac(1))/ac(2)
           else
            ddum=1.0d3
          end if
          call chanwav(wtem,ddum,1.0d-3,100)
          l1 = nint( ddum )
*         set scale range as well
          lc1=l1
          wtem=wcenpg*sqrt((2.99792458e5+velhipg)/
     :           (2.99792458e5-velhipg))
          if(ac(2).ge.0.0d0) then
            ddum=(wtem-ac(1))/ac(2)
           else
            ddum=1.0d3
          end if
          call chanwav(wtem,ddum,1.0d-3,100)
          l2 = nint( ddum )
*         set scale range as well
          lc2=l2
*         reset atom, ion, rest wavelength in case 'l' is used in this mode
          prevatom=atom(jlins)
          previon=ion(jlins)
          prevrestwave=rstwav(jlins)
        end if
*	turn off velocity information
        nlen=80
        do while(cflname(nlen:nlen).eq.' '.and.nlen.gt.1)
          nlen=nlen-1
        end do
        if(pglcapt(1:1).ne.' ') then
          write(pglchar(3),'(a)') pglcapt
         else
          write(pglchar(3),'(a)') cflname(1:nlen)
        end if
*	reset colour
        ipgatt(1,1)=1
        lvel=.false.
        chc='n'
        goto 3
      end if
*
*     x: interpolate between two marked continuum points
      if(chc.eq.'x'.or.chc.eq.'I'.or.chc.eq.'i') then
        if(lvel) then
          write(6,*) 'Replot on wavelength scale first'
*	  turn off velocity information
          nlen=80
          do while(cflname(nlen:nlen).eq.' '.and.nlen.gt.1)
            nlen=nlen-1
          end do
          if(pglcapt(1:1).ne.' ') then
            write(pglchar(3),'(a)') pglcapt
           else
            write(pglchar(3),'(a)') cflname(1:nlen)
          end if
          lvel=.false.
         else
*         wavelength to channel conversion
          wl=x1pg
          if(ac(2).ge.0.0d0) then
            ddum=(wl-ac(1))/ac(2)
           else
            ddum=1.0d3
          end if
          call chanwav(wl,ddum,1.0d-3,100)
          m1 = nint( ddum )
          write(6,*) 'wavelength ',wl,', channel ',m1
          if(chc.eq.'I') then
            c1=ca(m1)
           else
            if(chc.eq.'i') then
              c1=sqrt(de(m1))
             else
              c1=da(m1)
            end if
          end if
*         second point
          write(6,*) 'second position'
          call vp_pgcurs(x1pg,y1pg,chcx)
          wl=x1pg
*         might as well start search from previous point
          call chanwav(wl,ddum,1.0d-3,100)
          m2 = nint( ddum )
          write(6,*) 'wavelength ',wl,', channel ',m2
          if(chc.eq.'I') then
            c2=ca(m2)
           else
            if(chc.eq.'i') then
              c2=sqrt(de(m2))
             else
              c1=da(m2)
              c2=c1
            end if
          end if
          if(m1.gt.m2) then
*           for simplicity rather than necessity
            m1p=m2
            m2=m1
            m1=m1p
            slope=c2
            c2=c1
            c1=slope
          end if
          m1p=m1+1
          m2m=m2-1
          if(m1p.le.m2m) then
            slope=(c2-c1)/dble(m2-m1)
            do j=m1p,m2m
              if(chc.eq.'I') then
                ca(j)=slope*dble(j-m1)+c1
               else
                if(chc.eq.'i') then
                  de(j)=(slope*dble(j-m1)+c1)**2
                 else
                  da(j)=slope*dble(j-m1)+c1
                end if
              end if
            end do
          end if
        end if
        chc='n'
        goto 3
      end if
*
*     y: set y max scale
      if(chc.eq.'y') then
        yhihpg=y1pg
        yhihpgheld=yhihpg
        nyuse=1
        chc='n'
        goto 3
      end if
*
*     z: reset continuum at a point
      if(chc.eq.'z') then
        if(lvel) then
          write(6,*) 'Replot on wavelength scale first'
*         turn off velocity information
          nlen=80
          do while(cflname(nlen:nlen).eq.' '.and.nlen.gt.1)
            nlen=nlen-1
          end do
          if(pglcapt(1:1).ne.' ') then
            write(pglchar(3),'(a)') pglcapt
           else
            write(pglchar(3),'(a)') cflname(1:nlen)
          end if
          lvel=.false.
         else
*	  wavelength to channel conversion
          wl=x1pg
          if(ac(2).ge.0.0d0) then
            ddum=(wl-ac(1))/ac(2)
           else
            ddum=1.0d3
          end if
          call chanwav(wl,ddum,1.0d-3,100)
          m1 = nint( ddum )
          ca(m1)=y1pg
        end if
        chc='n'
        goto 3
      end if
*
*     UPPER CASE ONES:
*     
*     C: plot data and continuum
      if(chc.eq.'C') then
*       no residuals
        ksplot(6)=0
        residpg=0.0
*	other curve indicator
        ksplot(3)=1
        ksplot(2)=0
        ksplot(9)=0
        chc='n'
        goto 3
      end if
*
*     F: format for data print swapped (13 <-> 26)
      if(chc.eq.'F') then
*       changes value of variable lrformt, used with b,o,h,l
        if(lrformt.eq.0) then
          lrformt=1
         else
          lrformt=0
        end if
        goto 1
      end if
*
*     G: plot data, continuum and error
      if(chc.eq.'G') then
        ksplot(6)=0
        residpg=0.0
        ksplot(2)=1
        ksplot(3)=1
        ksplot(9)=0
        chc='n'
        goto 3
      end if
*
*     K: set caption logical variable
      if(chc.eq.'K') then
*       just changes a logical variable lgflnm
        if(lgflnm) then
          lgflnm=.false.
         else
          lgflnm=.true.
        end if
        goto 1
      end if
*
*     M: mark lines in current region
      if(chc.eq.'M') then
*       just sets a flag here
        lmarkl=.true.
*       lmarkto true means just ticks, no ID's
        lmarkto=.false.
        zmark=-1.0d20
        goto 1
      end if
*
*     N: no mark lines in current region
      if(chc.eq.'N') then
*       just sets a flag here
        lmarkl=.false.
        lmarkto=.false.
        goto 1
      end if
*
*     P: command line prompt (drops through anyway - this
*     just reserves a letter for it)
      if(chc.eq.'P') goto 3
*
*     S: plot data only
      if(chc.eq.'S') then
        ksplot(1)=1
        do j=2,9
          ksplot(j)=0
        end do
        residpg=0.0
        chc='n'
        goto 3
      end if
*
*     T: tick marks over lines in current region
      if(chc.eq.'T') then
*       just sets a flag here
        lmarkl=.true.
        lmarkto=.true.
        zmark=-1.0d20
        goto 1
      end if
*
*     U: exclude points with data values above a cursor-set
*     value (by setting the error array negative there)
*     Command line input required -- grow radius about high points
      if(chc.eq.'U') then
*       wavelength to channel conversion
        wl=x1pg
        thres=y1pg
        if(ac(2).ge.0.0d0.and.ac(2).lt.ac(1)) then
          ddum=(wl-ac(1))/ac(2)
         else
          ddum=1.0d3
        end if
        call chanwav(wl,ddum,1.0d-3,100)
        m1 = nint( ddum )
        write(6,*) 'wavelength ',wl,', channel ',m1
*       request end of x-range
        write(6,*) 'second position'
        call vp_pgcurs(x1pg,y1pg,chcx)
        wl=x1pg
*       might as well start search from previous point
        call chanwav(wl,ddum,1.0d-3,100)
        m2 = nint( ddum )
        write(6,*) 'wavelength ',wl,', channel ',m2
        if(m2.lt.m1) then
          mtemp=m2
          m2=m1
          m1=mtemp
        end if
        write(6,*) 'Grow radius (<10, in graphics window)? [3]'
        call vp_pgcurs(x1pg,y1pg,chcx)
        if(chcx.eq.' ') then
          igrwrad=3
         else
          chtemp=chcx//'       '
          read(chtemp,'(i1)',err=698) igrwrad
          write(6,*) 'Grow radius',igrwrad
        end if
        goto 697
 698    igrwrad=3
 697    continue
        if(m1.lt.1) m1=1
        if(m2.gt.ngp) m2=ngp
*       now search from m1 to m2, and replace de(i) where da(i).gt.thres
        jlo=0
        jhi=0
        j=m1
        do while (j.le.m2)
          if(da(j).gt.thres.and.jlo.eq.0) jlo=j
          if(jlo.gt.0.and.da(j).lt.thres) then
            jhi=j
*           have found a spike, with width
            jlo=jlo-igrwrad
            if(jlo.lt.m1) jlo=m1
            jhi=jhi+igrwrad
            if(jhi.gt.m2) jhi=m2
            do i=jlo,jhi
              de(i)=-1.0d0
            end do
            j=jhi+1
            jlo=0
           else
            j=j+1
          end if
        end do
        goto 1
      end if
*
*     W: estimate equivalent width
      if(chc.eq.'W') then
        if(lvel.or.x1pg.lt.0.0) then
*	  convert velocity to wavelength first, using wcentral
*	  determined from y-position
          lvel=.true.
          jlins=int(y1pg+1.0)
          if(jlins.le.0) jlins=1
          if(jlins.gt.nlins) jlins=nlins
          wcenpg=rstwav(jlins)*zp1refpg
          wl=wcenpg*sqrt((2.99792458e5+x1pg)/
     :                (2.99792458e5-x1pg))
         else
*	  was wavelength plot anyway
          wl=x1pg
        end if
        y2pg=y1pg
        call vp_pgcurs(x1pg,y2pg,chcx)
        if(lvel) then
*	  use previous velocity parameters
          wl2=wcenpg*sqrt((2.99792458e5+x1pg)/
     :                (2.99792458e5-x1pg))
         else
          wl2=x1pg
        end if
*       now have wavelength, flux values at two positions
*       convert wavelengths to channels
        dtemp=wl
        wculo=wl
        if(ac(2).ge.0.0d0.and.ac(2).lt.ac(1)) then
          ddum=(dtemp-ac(1))/ac(2)
         else
          ddum=1.0d3
        end if
        call chanwav(dtemp,ddum,1.0d-3,100)
*       channel is integer at middle of the pixel
        lch1=int(ddum+0.5d0)
        dtemp=wl2
        wcuhi=wl2
        call chanwav(dtemp,ddum,1.0d-3,100)
        lch2=int(ddum+0.5d0)
        if(lch1.gt.lch2) then
          write(6,*) 'Limits wrong?'
          goto 1
        end if
        temp1=lch1
        wl=wval(dble(lch1))
        temp2=lch2
        wl2=wval(dble(lch2))
*       wavelength units per channel in temp1. Assume
*       that local linear is fine
        if(lch2.gt.lch1) then
          temp1=(wl2-wl)/(temp2-temp1)
         else
          xtemp=wval(dble(lch1+1))
          temp1=dble(x1pg)-wl
        end if
*       if a velocity plot, use the continuum
        temp2=0.0d0
        ewobs=0.0d0
        ewerr=0.0d0
        if(lvel) then
          do i=lch1,lch2
            if(ca(i).gt.0.0d0) then
*             fractional bin wavelength limits
              wxlo=wval(dble(i)-0.5d0)
              wtemplo=wxlo
              if(i.eq.lch1.and.wculo.gt.wxlo) then
                wtemplo=wculo
              end if
              wxhi=wval(dble(i)+0.5d0)
              wtemphi=wxhi
              if(i.eq.lch2.and.wcuhi.lt.wxhi) then
                wtemphi=wcuhi
              end if
              wavrng=wtemphi-wtemplo
              ewobs=ewobs+(1.0d0-da(i)/ca(i))*wavrng
*              write(6,*) i,yfrac,wavrng,ewobs
              ewerr=ewerr+de(i)*(wavrng/ca(i))**2
              temp2=temp2+1.0d0-da(i)/ca(i)
            end if
          end do
          wavrng=0.5d0*(wl+wl2)
          ddum=sqrt(ewerr)
          write(6,'(a,f10.4,1pe14.4,e14.4)') 
     1         'With fractional pixels',wavrng,ewobs,ddum
         else
          write(6,*) 'y-cursor defines continuum'
*         use y-cursor to define continuum
          if(lch1.eq.lch2) then
*           single channel case
            temp2=1.0d0-da(lch1)/dble(y1pg)
           else
*	    more than one pixel
            xtemp=lch2-lch1
            xtemp2=dble(y1pg)/xtemp
            xtemp1=dble(y2pg)/xtemp
            do i=lch1,lch2
              ytemp1=dble(i-lch1)
              ytemp2=dble(lch2-i)
              ytemp1=ytemp1*xtemp1+ytemp2*xtemp2
              if(ytemp1.gt.0.0d0) temp2=temp2+1.0d0-da(i)/ytemp1
            end do
          end if
        end if
        temp2=temp2*temp1
        temp1=0.5d0*(wl+wl2)
        nlen=80
        do while(cflname(nlen:nlen).eq.' '.and.nlen.gt.1)
          nlen=nlen-1
        end do
        write(6,*) cflname(1:nlen),temp1,temp2
        chc='n'
        goto 1
      end if
*
*     Z: mark lines at given redshift in current region
      if(chc.eq.'Z') then
*	just sets a flag here
        lmarkl=.true.
        zmark=dble(zp1refpg)-1.0d0
        goto 1
      end if
*
*     Numbers:
*
*     0: default pgplot cursor, or extended (toggles)
      if(chc.eq.'0') then
        if(ipgatt(10,1).eq.0) then
          ipgatt(10,1)=7
         else
          ipgatt(10,1)=0
        end if
        chc='n'
        goto 1
      end if
*     
*     7: forces extended cross-hair pgplot cursor
      if(chc.eq.'7') then
        ipgatt(10,1)=7
        chc='n'
        goto 1
       end if

*     Assorted extras, based on random spare characters
*
*     [: List possible identifications based on doublet
      if(chc.eq.'['.or.chc.eq.'1') then
        xtemp=x1pg
*       set tolerance=(wmax-wmin)/300.
        tol=(wval(dble(l2))-wval(dble(l1)))/300.0D0
        if(tol.le.0.1d0) tol=0.1d0
        write(6,*) x1pg
        write(6,*) 'Second line position'
        call vp_pgcurs(x1pg,y1pg,chc)
        write(6,*) x1pg
        call rd_dblid(dble(x1pg),xtemp,tol,chc)
        goto 1
      end if
*
*     ^ & _: continuum & zero level parameter printout
      if(chc.eq.'^'.or.chc.eq.'_') then
*	print out parameters for continuum adjustment (26, no slope)
*       or zero level adjustment (also no slope)
        if(lvel) then
*	  velocity to wavelength conversion
          jlins=int(y1pg+1.0)
          if(jlins.le.0) jlins=1
          if(jlins.gt.nlins) jlins=nlins
          wcenpg=rstwav(jlins)*zp1refpg
          wl=wcenpg*sqrt((2.99792458e5+x1pg)/(2.99792458e5-x1pg))
         else
          wl=x1pg
        end if
*	search for wavelength of the '<>' line, or '__' as appropriate
        jcp=1
        if(chc.eq.'_') then
          do while(jcp.lt.mnats.and.lbz(jcp).ne.'__')
            jcp=jcp+1
          end do
         else
          do while(jcp.lt.mnats.and.lbz(jcp).ne.'<>')
            jcp=jcp+1
          end do
        end if
        if(lbz(jcp).eq.'<>'.or.lbz(jcp).eq.'__') then
*	  redshift
          zloc=wl/alm(jcp)-1.0d0
         else
          zloc=wl/1215.6701d0-1.0d0
        end if
        if(chc.eq.'_') then
*	fort.26 style
          write(6,'(a6,f10.6,a2,f10.6,1x,a8,f8.2,a7,f7.3)')
     :          '__    ',zloc,'SZ',errest,'  0.00SB',errest,
     :          '  0.000',errest
          if(lchg13) then
*	  file write if wanted
            write(22,'(a6,f10.6,a2,f10.6,1x,a8,f8.2,a7,f7.3)')
     :          '<>    ',zloc,'SZ',errest,'  0.00SB',errest,
     :          '  0.000',errest
          end if
         else
          write(6,'(a6,f10.6,a2,f10.6,1x,a8,f8.2,a7,f7.3)')
     :          '<>    ',zloc,'SZ',errest,'  0.00SB',errest,
     :          '  1.000',errest
          if(lchg13) then
*	  file write if wanted
            write(22,'(a6,f10.6,a2,f10.6,1x,a8,f8.2,a7,f7.3)')
     :          '<>    ',zloc,'SZ',errest,'  0.00SB',errest,
     :          '  1.000',errest
          end if
        end if
        goto 1
      end if
*
*     @: observed wavelengths shown/not shown
      if(chc.eq.'@') then
*	change display flag for observed wavelengths on velocity plots
        if(lobswav) then
          lobswav=.false.
         else
          lobswav=.true.
        end if
        goto 1
      end if
*
*     /: lower y limit set for next plot
      if(chc.eq.'/') then
        ylowhpg=y1pg
        nyuse=1
        goto 1
      end if
*
*     : pixels below threshold in region
      if(chc.eq.':') then
        wl=x1pg
        if(ac(2).ge.0.0d0) then
          ddum=(wl-ac(1))/ac(2)
         else
          ddum=1.0d3
        end if
        call chanwav(wl,ddum,1.0d-3,100)
        m1 = nint( ddum )
        if(m1.le.0) m1=1
        yval=y1pg
        write(6,*) 'right edge..'
        call vp_pgcurs(x1pg,y1pg,chcx)
        wl=x1pg
        if(ac(2).ge.0.0d0) then
          ddum=(wl-ac(1))/ac(2)
         else
          ddum=1.0d3
        end if
        call chanwav(wl,ddum,1.0d-3,100)
        m2 = nint( ddum )
        if(m2.gt.ngp) m2=ngp
*       remember that de is VARIANCE
        mvals=0
        mless=0
        do j=m1,m2
          if(de(j).gt.0.0d0) then
            mvals=mvals+1
            if(da(j).lt.yval) then
              mless=mless+1
            end if
          end if
        end do
        if(mvals.gt.1) then
          temp=dble(mless)/dble(mvals)
          write(6,*) mless,' pixels out of ',mvals,' < ',yval
          write(6,*) ' i.e. fraction ',temp
         else
          write(6,*) 'No useful data points in range'
        end if
        chc='n'
        goto 1
      end if
*
*     ! : print line information for unknown line (normally first in
*         atom.dat list)
      if(chc.eq.'!') then
*       read atomic data if necessary
        if(mnats.le.0) call vp_ewred(0)
        jcp=1
        do while(jcp.lt.mnats.and.lbz(jcp).ne.'??')
          jcp=jcp+1
        end do
        ch2=lbz(jcp)
        ch4=lzz(jcp)
        temp=alm(jcp)
*       jcp is array location for the '??' line
        if(lbz(jcp).eq.'??') then
*         can print out unknown line parameters
          if(lvel) then
*           velocity to wavelength conversion needed
            jlins=int(y1pg+1.0)
            if(jlins.le.0) jlins=1
            if(jlins.gt.nlins)jlins=nlins
            wcenpg=rstwav(jlins)*zp1refpg
            wl=wcenpg*sqrt((2.99792458e5+x1pg)/(2.99792458e5-x1pg))
*	    redshift as well, just for inf
            zloc=wl/temp-1.0d0
            call rd_estcolb(da,de,ca,ngp,ch2,ch4,
     :         temp,wl,colg,bg)
            if(colg.gt.0.0d0) then
*	      fort.26 style only one supported for this
              write(6,'(a2,a4,2f10.6,1x,2f8.2,2f7.3)')
     :            ch2,ch4,zloc,errest,bg,errest,
     :            colg,errest
              if(lchg13) then
                write(22,'(a2,a4,2f10.6,1x,2f8.2,2f7.3)')
     :            ch2,ch4,zloc,errest,bg,errest,
     :            colg,errest
              end if
            end if
           else
*           wavelength plot
            wl=x1pg
            zloc=wl/temp-1.0d0 
            zla=wl/1215.6701-1.0d0
            call rd_estcolb(da,de,ca,ngp,ch2,ch4,
     :         temp,wl,colg,bg)
            if(colg.gt.0.0d0) then
*             fort.26 style
              write(6,'(a2,a4,2f10.6,1x,2f8.2,2f7.3)')
     :            ch2,ch4,zloc,errest,bg,errest,
     :            colg,errest
            end if
            if(lchg13) then
              write(22,'(a2,a4,2f10.6,1x,2f8.2,2f7.3)')
     :            ch2,ch4,zloc,errest,bg,errest,
     :            colg,errest
            end if
          end if
        end if
        goto 1
      end if
*
*     # Heckman b estimate from cursor positions
      if(chc.eq.'#') then
        wl=x1pg
        if(ac(2).ge.0.0d0) then
          ddum=(wl-ac(1))/ac(2)
         else
          ddum=1.0d3
        end if
        call chanwav(wl,ddum,1.0d-3,100)
        m1 = nint( ddum )
        if(m1.le.0) m1=1
        yval=y1pg
        write(6,*) 'right edge..'
        call vp_pgcurs(x1pg,y1pg,chcx)
        wl=x1pg
        if(ac(2).ge.0.0d0) then
          ddum=(wl-ac(1))/ac(2)
         else
          ddum=1.0d3
        end if
        call chanwav(wl,ddum,1.0d-3,100)
        m2 = nint( ddum )
        if(m2.gt.ngp) m2=ngp
*       calculate optical depth weighted mean
        sumt=0.0d0
        sumwt=0.0d0
        sumd=0.0d0
        sumwd=0.0d0
*       find mean
        do j=m1,m2
          temp=da(j)/ca(j)
          if(temp.lt.1.0d0) then
            if(temp.le.0.01d0) then
              write(6,*) 'Optically thick line, tau uncertain'
              temp=0.01d0
            end if
            wtemp=wval(dble(j))
            taut=-log(temp)
            temp=1.0d0-temp
            sumt=sumt+taut
            sumwt=sumwt+wtemp*taut
            sumd=sumd+temp
            sumwd=sumwd+wtemp*temp
          end if
        end do
*       two wavelength estimators, optical depth weighted and flux weighted
        wtm=sumwt/sumt
        wdm=sumwd/sumd
*       assume the line is narrow, so just use v/c=dl/l
        sigd=0.0d0
        sigt=0.0d0
        do j=m1,m2
          temp=da(j)/ca(j)
          if(temp.lt.1.0d0) then
            if(temp.le.0.01d0) then
              write(6,*) 'Optically thick line, tau uncertain'
              temp=0.01d0
            end if
            wtemp=wval(dble(j))
            taut=-log(temp)
            temp=1.0d0-temp
            vdt=1.0d0-wtemp/wtm
            vdd=1.0d0-wtemp/wdm
            sigt=taut*vdt*vdt+sigt
            sigd=sigd+vdd*vdd*temp
          end if
        end do
        sigt=sqrt(2.0d0*sigt/sumt)*2.99792458d5
        sigd=sqrt(2.0d0*sigd/sumd)*2.99792458d5
        resl=vpf_dvresn(wdm,1)*2.99792458d5
*       was=sigmah+dlamh/wdm*2.997925E5
        temp=sqrt(sigt*sigt-2.0*resl*resl)
        write(6,'(a,f6.2,a)') 'Estimated b with tau centroid =',
     :                         sigt,' km/s'
        write(6,'(a,f6.2)')   '   resolution corrected value =',temp
        write(6,'(a,f6.2)') '   b (flux centroid) =',sigd
        temp=sqrt(sigd*sigd-2.0d0*resl*resl)
        write(6,'(a,f6.2)') 'resolution corrected =',temp
        chc='n'
        goto 1
      end if
*
*     }: wavelength regions shown/not shown
      if(chc.eq.'}') then
*	change display flag for observed wavelengths on velocity plots
        if(lmarkreg) then
          lmarkreg=.false.
         else
          lmarkreg=.true.
        end if
        goto 1
      end if
*
*     exit section
 3    continue
      return
      end
