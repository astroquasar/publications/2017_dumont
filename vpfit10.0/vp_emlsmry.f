      subroutine vp_emlsmry(lt,nopchan,ion,level,parm,parerr,np)
*
*     print out summary of emission line characteristics 
*     in a useful form.. so, for the first line in the
*     atom.dat list which matches the ID, it gives the
*     integrated flux and error estimate. Equivalent widths
*     you have to get yourself from local continuum measures.
*
*     INPUT: all the variables
*     lt	I-array	output channels open
*     nopchan	int	number of output channels
*     parm	real	parameter array
*     ipind	c*2	indicator array [no longer used - removed]
*     parerr	real	parameter error array
*     np	int	# parameters
*     
*     this file has the basic sizes for various common arrays etc
*
      implicit none
      include 'vp_sizes.f'
*     
      integer np
      integer lt(*),nopchan
      character*2 ion(*)
      character*4 level(*)
      double precision parm(np)
      double precision parerr(np)
*     Local:
      integer i,jb,k,l,nn
      double precision wvl,flx,relerr,flxerr,fwhm,fwerr
*     Common:
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm
      integer m
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),m
      logical lrescale
      double precision dscale
      common/vpc_dscale/dscale,lrescale
*     parameter list length & positions
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*
*     routine called ONLY if indvar.eq.-1
*
      do i=1,nopchan
        write(lt(i),'(a)') ' '
        write(lt(i),'(a)') 'Emission line parameters:'
        write(lt(i),'(a)') ' '
        write(lt(i),'(a)') '  n  Ion            z      +/-'//
     :     '        peak        flux        +/-       FWHM    +/-'
      end do
      nn=np/noppsys
      do i=1,nn
        jb=(noppsys*(i-1))
*       search vp_ewred list until a match is obtained:
        k=1
        do while ((ion(i).ne.lbz(k).or.level(i).ne.lzz(k))
     :              .and.k.lt.m)
          k=k+1
        end do
*       observed wavelength:
        wvl=alm(k)*(1.0d0+parm(jb+nppzed))
        flx=5.9082d-6*parm(jb+nppcol)*fik(k)*
     :          parm(jb+nppbval)*wvl/dscale
        relerr=sqrt((parerr(jb+nppcol)/parm(jb+nppcol))**2 + 
     :               (parerr(jb+nppbval)/parm(jb+nppbval))**2)
        flxerr=flx*relerr
        fwhm=1.66d0*parm(jb+nppbval)
        fwerr=1.66d0*parerr(jb+nppbval)
        do l=1,nopchan
          write(lt(l),
     :      '(i3,1x,a2,a4,f6.1,2f9.6,1pe12.4,2e12.4,0pf7.0,f7.0)')
     :        i,lbz(k),lzz(k),alm(k),
     :        parm(jb+nppzed),parerr(jb+nppzed),parm(jb+nppcol),
     :        flx,flxerr,fwhm,fwerr
        end do
      end do
      return
      end
