      subroutine vp_minquad(n,x,y,xmin,ymin)
*
*     Assuming that the n points x(i),y(i) are fitted by a quadratic,
*     find the minimum value
      implicit none
      integer n,nhi
      integer i
      double precision x(n),y(n),xmin,ymin
      double precision xd,yd
      double precision x2,x3,x4,y1,y2,dn
      double precision xtem,ytem,ylo,yhi,xlo,xhi,xmean,ymean
      double precision c1,c2,c3
*
*     Range of variables
      if(n.le.2) then
        ymin=y(1)
        xmin=x(1)
        if(n.eq.2) then
          if(ymin.gt.y(2)) then
            ymin=y(2)
            xmin=x(2)
          end if
        end if
       else
*       three or more points, so fit
        dn=dble(n)
        xmean=dble(x(1))
        ymean=dble(y(1))
        ylo=ymean
        yhi=ylo
        xlo=xmean
        xhi=xlo
        nhi=1
        do i=2,n
          ytem=dble(y(i))
          if(ytem.lt.ylo) then
            ylo=ytem
          end if
          if(ytem.gt.yhi) then
            yhi=ytem
            nhi=i
          end if
          ymean=ymean+ytem
          xtem=dble(x(i))
          xlo=min(xlo,xtem)
          xhi=max(xhi,xtem)
          xmean=xmean+xtem
        end do
        xmean=xmean/dn
        ymean=ymean/dn
*       arbitrary ranges for bizarre cases
        if(xlo.ge.xhi) xlo=xhi-0.0001d0
        if(ylo.ge.yhi) ylo=yhi-0.0001d0    
*       rescale variables, and work out sums
        x2=0d0
        x3=0d0
        x4=0d0
        y1=0d0
        y2=0d0
        do i=1,n
          xd=(dble(x(i))-xmean)/(xhi-xlo)
          yd=(dble(y(i))-ymean)/(yhi-ylo)
          x2=x2+xd**2
          x3=x3+xd**3
          x4=x4+xd**4
          y1=y1+yd*xd
          y2=y2+yd*xd*xd
        end do
        x2=x2/dn
        x3=x3/dn
        x4=x4/dn
        y1=y1/dn
        y2=y2/dn
        xtem=x3*x3-(x4-x2*x2)*x2
        if(dabs(xtem).gt.1.0d-33) then
*         this works for constant, linear or quadratic without overflow
          c3=(y1*x3-y2*x2)/xtem
          if(c3.ne.0.0d0) then
            c2=y1/x2-c3*x3/x2
            c1=-c3*x2
*           write(6,*) c1,c2,c3
            xd=-0.5d0*c2/c3
            yd=c1+c2*xd+c3*xd*xd
            ymin=(yhi-ylo)*yd+ymean
            xmin=(xhi-xlo)*xd+xmean
           else
*           ymean is same as all values, no sensible minimum
*           return high-x point
            write(6,*) 'vp_minquad fail : all values the same'
            ymin=yhi
            xmin=x(nhi)
          end if
         else
*         blows up - return maximum so it will use other routine
          write(6,*) 'vp_minquad fail : max value returned'
          ymin=yhi
          xmin=x(nhi)
        end if
      end if
      return
      end
