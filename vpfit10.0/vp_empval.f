      double precision function vp_empval(ww)
*     interpolate the profile which is held in the top position 
*     in the data arrays to the wavelength position ww (perversely, 
*     in cm, to be compatible with the Voigt profile routine).
*     used by spvoigt.
*	
*     This function also modifies the common variable lchemp,
*     the array position for a close wavelength
*
      implicit none
      include 'vp_sizes.f'
      double precision ww
*
*     Local
      double precision wwa,wlo,whi,xlo,xhi
*
*     position pointers for template array
      integer lchemp,lentempl
      common/vpc_empval/lchemp,lentempl
*     data arrays
*     current 1-D spectrum data, err, fluctuations, continuum -length ngp
      integer ngp
      double precision dhdata(maxfis),dherr(maxfis)
      double precision dhrms(maxfis),dhcont(maxfis)
      common/vpc_su1d/dhdata,dherr,dhrms,dhcont,ngp
*     emission profile array
      integer ngpf
      double precision wvempf(maxfis),daempf(maxfis)
      common/vpc_emprof/wvempf,daempf,ngpf
*
*     convert to Angstroms
      wwa=ww*1.0d8
*     search for wavelengths in array below and above wwa
      wlo=wvempf(lchemp)
      whi=wvempf(lchemp+1)
*     write(6,*) 'vp_emp:',wwa,wlo,whi
*     check if channel needs to go down
      do while (wlo.gt.wwa.and.lchemp.gt.0) 
        lchemp=lchemp-1
        whi=wlo
        wlo=wvempf(lchemp)
      end do
*     or may need to go up
      do while (whi.le.wwa.and.lchemp.lt.lentempl)
        lchemp=lchemp+1
        wlo=whi
        whi=wvempf(lchemp)
      end do
      if(lchemp.le.0.or.lchemp.gt.lentempl-1) then
        vp_empval=0.0d0
       else
*	interpolate for value
        xlo=(wwa-wlo)/(whi-wlo)
        xhi=(whi-wwa)/(whi-wlo)
        vp_empval=daempf(lchemp)*xhi+daempf(lchemp+1)*xlo
      end if
*     write(6,*) 'vp_em:',lchemp,vp_empval
      return
      end
