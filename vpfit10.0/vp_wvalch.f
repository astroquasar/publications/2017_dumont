      double precision function vp_wvalch(tin,l)
*     convert chunk channel number (double precision) to wavelength using  
*     wavelengths wavch(n,m), using the lth wavelength set.
*     no need for air<->vacuum, heliocentric etc - was done before wavch
*     was set up.
      implicit none
      include 'vp_sizes.f'
      integer i,l
      double precision tin,tx
*     chunk wavelengths
      integer nwvch(maxnch)
      double precision wavch(maxchs,maxnch),wavchlo(maxchs,maxnch)
      common/vpc_wavch/wavch,wavchlo,nwvch
*
*     wavelength array
      i=int(tin)
*     linearly interpolate for non-integer tin, making sure don't go beyond
*     ends. If tin outside range, extrapolate from nearest two points.
      if(i.ge.nwvch(l)) then
        i=i-1
       else
        if(i.le.0) then
          i=i+1
        end if
      end if
      tx=tin-dble(i)
      vp_wvalch=wavch(i,l)*(1.0d0-tx) + wavch(i+1,l)*tx
      return
      end
