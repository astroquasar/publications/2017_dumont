      subroutine rd_dblid(w1,w2,tol,chc)
*     print a list of possible identifications for a pair
*     of lines at wavelengths w1 & w2, tolerance tol
      implicit none
      include 'vp_sizes.f'
*
      double precision w1,w2,tol
      character*1 chc
*     Local:
      integer i,j,llq
      double precision r1,r2,x,z1
*     Functions:
      logical lcase
      integer lastchpos
*     Common:
      integer nz
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :     fik(maxats),asm(maxats),nz
*     atomic mass table
      integer mmass
      character*2 lbm
      double precision amass
      common/vpc_atmass/lbm(maxats),amass(maxats),mmass
*     maximum redshift for table match
      double precision zidmax
      common/rdc_zidmax/zidmax

*     Get atomic data if necessary
      if(nz.le.0) call vp_ewred(1)
*
*     wavelengths & tolerance
      if(w1.gt.w2) then
        r1=w1
        w1=w2
        w2=r1
      end if
      r1=w2/w1
      write(6,*) 'Ratio:',r1
      r1=(w2-tol)/w1
      r2=(w2+tol)/w1
      if(r1.gt.0.0d0) then
        do i=1,nz
*         don't bother if too far down the list:
          if(chc.eq.'['.and.(lbz(i).eq.'H2'.or.lbz(i).eq.'HD'.or.
     :         lbz(i).eq.'CO')) goto 201
          do j=1,nz
            if(alm(i).lt.alm(j)) then
              x=alm(j)/alm(i)
              if(x.gt.r1.and.x.lt.r2) then
*               work out mean redshift
                z1=0.5d0*(w1/alm(i)+w2/alm(j))-1.0d0
*               filter out special ion labels
                llq=lastchpos(lzz(i))
                if(llq.le.0) llq=1
                if(z1.le.zidmax.and.lbz(i).ne.'<>'.and.
     :               lbz(i).ne.'??'.and.lbz(i).ne.'__'.and.
     :               lbz(i).ne.'>>'.and.lbz(i).ne.'<<'.and.
     :               (.not.lcase(lzz(i)(llq:llq)))) then
*	          don't want lbz(j).eq.anything strange either:
                  llq=lastchpos(lzz(j))
                  if(llq.le.0) llq=1
                  if(lbz(j).ne.'<>'.and.
     :               lbz(j).ne.'??'.and.lbz(j).ne.'__'.and.
     :               lbz(j).ne.'>>'.and.lbz(j).ne.'<<'.and.
     :               (.not.lcase(lzz(j)(llq:llq)))) then
                    write(6,
     :             '(f9.6,2x,f8.6,3x,2(2x,a2,a4,1x,f8.3,1x,f8.6))')
     :                 z1,x,lbz(i),lzz(i),alm(i),fik(i),
     :                 lbz(j),lzz(j),alm(j),fik(j)
                  end if
                end if
              end if
            end if
          end do
201       continue
        end do
      end if
      return
      end
