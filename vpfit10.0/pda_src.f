      SUBROUTINE PDA_QSIAD( EL, X, IP )
*+
*  Name:
*     PDA_QSIAD

*  Purpose:
*     Sort an array of pointers to access a DOUBLE PRECISION  array 
*     in ascending order.

*  Language:
*     Fortran 77

*  Invocation:
*     CALL CCD1_QSIAD( EL, X, IP )

*  Description:
*     The routine uses the QUICKSORT algorithm to permute an array of
*     pointers so that they access an associated array of values in
*     ascending order. The "median of three" modification is included
*     to reduce the likelihood of encountering the worst-case behaviour
*     of QUICKSORT.

*  Arguments:
*     EL = INTEGER (Given)
*        The number of elements of X to sort.
*     X( EL ) = DOUBLE PRECISION (Given and Returned)
*        The array to be sorted.
*     IP( EL ) = INTEGER (Returned)
*        The indices of the elements of X in sorted order (i.e. IP( 1 )
*        gives the index into X of the lowest value).

*  References:
*     -  Sedgwick, R., 1988, "Algorithms" (Addison-Wesley).

*  Timing:
*     If N elements are to be sorted, the average time goes as N.ln(N).
*     The worst-case time goes as N**2.

*  Copyright:
*     Copyright (C) 1992 Science & Engineering Research Council

*  Authors:
*     MJC: Malcolm J. Currie (STARLINK)
*     RFWS: R.F. Warren-Smith (STARLINK, RAL)
*     PDRAPER: P.W. Draper (STARLINK, Durham University)
*     {enter_new_authors_here}

*  History:
*     11-Jan-1991 (MJC):
*        Original version.
*     29-MAY-1992 (RFWS):
*        Modified to permute a separate pointer array. Also added the
*        "median of three" enhancement and the use of a sentinel value
*        to avoid having to check array bounds within the innermost
*        loop.
*     1-JUN-1992 (RFWS):
*        Added exchange sort to find the median of three partition
*        element.
*     6-AUG-1992 (RFWS):
*        Rationalised the inner loop to improve performance.
*     8-NOV-1995 (PDRAPER):
*        Renamed to PDA_QSIA (Quick Sort Indexed Ascending) for 
*        the PDA library. Now returns the index of the first 1..N
*        elements of X.
*     {enter_further_changes_here}

*  Bugs:
*     {note_any_bugs_here}

*-
      
*  Type Definitions:
      IMPLICIT NONE              ! No implicit typing

*  Arguments Given:
      INTEGER EL
      DOUBLE PRECISION X( EL )

*  Arguments Returned:
      INTEGER IP( EL )

*  Local Constants:
      INTEGER MXSTK             ! Size of the recursion stack (log
                                ! base 2 of the maximum number of
                                ! elements to be sorted)
      PARAMETER ( MXSTK = 64 )
      
*  Local Variables:
      DOUBLE PRECISION XPART    ! Partition value
      INTEGER I                 ! Index to pointer array
      INTEGER I1                ! Pointer index of partition value?
      INTEGER I2                ! Pointer index of partition value?
      INTEGER I3                ! Pointer index of partition value?
      INTEGER ITMP              ! Temporary store to swap pointers
      INTEGER J                 ! Index to pointer array
      INTEGER L                 ! Left hand pointer element in sub-file
      INTEGER LL( MXSTK )       ! Stack for left sub-file limits
      INTEGER R                 ! Right pointer element in sub-file
      INTEGER RR( MXSTK )       ! Stack for right sub-file limits
      INTEGER STK               ! Recursion stack pointer
      
*.

*  Initialise.
      STK = 1
      LL( 1 ) = 1
      RR( 1 ) = EL
      DO 5 I = 1, EL
         IP( I ) = I
 5    CONTINUE

*  Loop until the stack is empty.
 1    CONTINUE                   ! Start of 'DO WHILE' loop
      IF ( STK .GT. 0 ) THEN

*  If the current sub-file is sorted, then pop the recursion stack.
         IF ( LL( STK ) .GE. RR( STK ) ) THEN
            STK = STK - 1

*  Otherwise, partition the current sub-file.
         ELSE

*  Set the sub-file limits.
            L = LL( STK )
            R = RR( STK )

*  Find the index of a pointer to a suitable partition value (the
*  median of three possible elements) by performing an elementary
*  exchange sort.
            I1 = L
            I2 = ( L + R ) / 2
            I3 = R
            IF ( X( IP( I1 ) ) .GT. X( IP( I2 ) ) ) THEN
               ITMP = I1
               I1 = I2
               I2 = ITMP
            END IF
            IF ( X( IP( I1 ) ) .GT. X( IP( I3 ) ) ) THEN
               ITMP = I1
               I1 = I3
               I3 = ITMP
            END IF
            IF ( X( IP( I2 ) ) .GT. X( IP( I3 ) ) ) THEN
               ITMP = I2
               I2 = I3
               I3 = ITMP
            END IF
            
*  Store the partition value.
            XPART = X( IP( I2 ) )

*  Initialise for partitioning.
            I = L
            J = R

*  Loop to partition the subfile, incrementing I and decrementing J
*  until an exchange of values is indicated. Note we need not check the
*  array bounds as XPART is known to be present and acts as a sentinel.
 2          CONTINUE             ! Start of 'DO WHILE' loop

 3          CONTINUE             ! Start of 'DO WHILE' loop
            IF ( X( IP( I ) ) .LT. XPART ) THEN
               I = I + 1
               GO TO 3
            END IF           

 4          CONTINUE             ! Start of 'DO WHILE' loop
            IF ( X( IP( J ) ) .GT. XPART ) THEN
               J = J - 1
               GO TO 4
            END IF           

*  Exchange pairs of values when necessary by interchanging their
*  pointers.
            IF ( I .LT. J ) THEN
               ITMP = IP( I )
               IP( I ) = IP( J )
               IP( J ) = ITMP

*  Return to locate another pair of values to exchange,
               I = I + 1
               J = J - 1
               GO TO 2
            END IF

*  Push sub-file limits on to the stacks to further subdivide this
*  region, retaining the smaller part (which will be partitioned next).
            IF ( ( J - L ) .LT. ( R - I ) ) THEN
               LL( STK + 1 ) = LL( STK )
               RR( STK + 1 ) = J
               LL( STK ) = J + 1
            ELSE
               LL( STK + 1 ) = I
               RR( STK + 1 ) = RR( STK )
               RR( STK ) = I - 1
            END IF

*  Increment the stack counter.
            STK = STK + 1
         END IF

*  Iterate until the stack is empty.
         GO TO 1
      END IF

      END
* @(#)pda_qsiad.f   1.2   95/11/10 10:03:23   95/11/10 10:03:52
*
*
      SUBROUTINE PDA_RINPD( PERM, N, X, IFAIL )
*+
*  Name:
*     PDA_RINPD

*  Purpose:
*     Reorder an array in place using a permutation index.

*  Language:
*     Fortran 77.

*  Invocation:
*     CALL PDA_RINPD( PERM, N, X, IFAIL )

*  Description:
*     This routine reorders an array (in place) using an permutation 
*     vector. This is most likely the output from one of the sorting 
*     routines PDA_QSI[A|D]D.

*  Arguments:
*     PERM( N ) = INTEGER (Given)
*        The index vector. Note this is modified but should be returned
*        in the same state as when input. Indices may not be negative.
*     N = INTEGER (Given)
*        Number of elements.
*     X( N ) = DOUBLE PRECISION (Given and Returned)
*        The array to reorder.
*     IFAIL = INTEGER (Returned)
*        Status flag. Set 0 for success, otherwise the permutation isn't
*        correct.

*  Notes: 
*     - Re-ordering is trivial if two arrays are available.
*          DO I = 1, N
*             XX( I ) = X( PERM( I ) )
*          END DO
*       The XX array contains the sorted values on completion.
*
*     - There is a routine for each of the data types integer, real and
*       double precision; replace "x" in the routine name by I, R or D as
*       appropriate. The data type of the X argument should match the
*       routine being used.


*  Timing:
*      Proportional to N.

*  Authors:
*     PDRAPER: Peter Draper (STARLINK - Durham University)
*     {enter_new_authors_here}

*  History:
*     31-AUG-1996 (PDRAPER):
*        Original version.
*     {enter_changes_here}

*  Bugs:
*     {note_any_bugs_here}

*-

*  Type Definitions:
      IMPLICIT NONE             ! No implicit typing

*  Arguments Given:
      INTEGER N
      INTEGER PERM( N )
      
*  Arguments Given and Returned:
      DOUBLE PRECISION X( N )
      INTEGER IFAIL
      
*  Local Variables:
      INTEGER I                 ! Loop variable
      INTEGER IAT               ! Current index
      INTEGER IAT0              ! Previous index
      DOUBLE PRECISION XTEMP              ! Value from start of cycle
*.

*  Check that PERM is a valid permutation
      IFAIL = 0
      DO 1 I = 1, N
         IAT = ABS( PERM( I ) )

*  Make sure index is within the range of the array to be reordered.
*  Then mark the place we're looking at by changing it's sign. We can
*  use this position again (hence the ABS() above), but may not look at
*  it again (as the result of another index dereference elsewhere) as
*  this would indicate that the permutation had a repeat value.
         IF ( ( IAT .GE. 1 ) .AND. ( IAT .LE. N ) ) THEN
            IF( PERM( IAT ) .GT. 0 ) THEN
               PERM( IAT ) = -PERM( IAT )
            ELSE 
               IFAIL = 1
            END IF
         ELSE 
            IFAIL = 1
         END IF
         IF ( IFAIL .NE. 0 ) GO TO 99
 1    CONTINUE

*  Now rearrange the values. All the permutation indices are negative at
*  this point and serve as an indicator of which values have been moved
*  (these become the positive ones). Typically the permutation will
*  consist of many sub-cycles where we chose the starting point and
*  return to it later, we just need to travel each sub-cycle, record
*  were we have been and terminate the inner loop when at the end of
*  the cycle. We then test for another cycle etc.
      DO 2 I = 1, N
         IF ( PERM( I ) .LT. 0 ) THEN 

*  Remember this position keep a copy of its value.
            IAT = I
            IAT0 = IAT
            XTEMP = X( I )

*  Loop while we have a permutation index that is negative, overwriting
*  values until we hit an index we've already done. This terminates the
*  current sub-cycle.
 3          CONTINUE
            IF ( PERM( IAT ) .LT. 0 ) THEN 
               X( IAT ) = X( -PERM( IAT ) )
               IAT0 = IAT
               PERM( IAT ) = -PERM( IAT )
               IAT = PERM( IAT )
               GO TO 3
            END IF

*  Back to start of sub-cycle. Overwrite with initial value.
            X( IAT0 ) = XTEMP
         END IF
 2    CONTINUE

*  Exit quick label.
 99   CONTINUE
      END
*
*
*DECK PDA_DPOLFT
      SUBROUTINE PDA_DPOLFT (N, X, Y, W, MAXDEG, NDEG, EPS, R, IERR, A,
     +   STATUS)
C***BEGIN PROLOGUE  PDA_DPOLFT
C***PURPOSE  Fit discrete data in a least squares sense by polynomials
C            in one variable.
C***LIBRARY   SLATEC
C***CATEGORY  K1A1A2
C***TYPE      DOUBLE PRECISION (POLFIT-S, PDA_DPOLFT-D)
C***KEYWORDS  CURVE FITTING, DATA FITTING, LEAST SQUARES, POLYNOMIAL FIT
C***AUTHOR  Shampine, L. F., (SNLA)
C           Davenport, S. M., (SNLA)
C           Huddleston, R. E., (SNLL)
C***DESCRIPTION
C
C     Abstract
C
C     Given a collection of points X(I) and a set of values Y(I) which
C     correspond to some function or measurement at each of the X(I),
C     subroutine  PDA_DPOLFT  computes the weighted least-squares polynomial
C     fits of all degrees up to some degree either specified by the user
C     or determined by the routine.  The fits thus obtained are in
C     orthogonal polynomial form.  Subroutine  PDA_DP1VLU  may then be
C     called to evaluate the fitted polynomials and any of their
C     derivatives at any point.  The subroutine  PDA_DPCOEF  may be used to
C     express the polynomial fits as powers of (X-C) for any specified
C     point C.
C
C     The parameters for  PDA_DPOLFT  are
C
C     Input -- All TYPE REAL variables are DOUBLE PRECISION
C         N -      the number of data points.  The arrays X, Y and W
C                  must be dimensioned at least  N  (N .GE. 1).
C         X -      array of values of the independent variable.  These
C                  values may appear in any order and need not all be
C                  distinct.
C         Y -      array of corresponding function values.
C         W -      array of positive values to be used as weights.  If
C                  W(1) is negative,  PDA_DPOLFT  will set all the weights
C                  to 1.0, which means unweighted least squares error
C                  will be minimized.  To minimize relative error, the
C                  user should set the weights to:  W(I) = 1.0/Y(I)**2,
C                  I = 1,...,N .
C         MAXDEG - maximum degree to be allowed for polynomial fit.
C                  MAXDEG  may be any non-negative integer less than  N.
C                  Note -- MAXDEG  cannot be equal to  N-1  when a
C                  statistical test is to be used for degree selection,
C                  i.e., when input value of  EPS  is negative.
C         EPS -    specifies the criterion to be used in determining
C                  the degree of fit to be computed.
C                  (1)  If  EPS  is input negative,  PDA_DPOLFT  chooses the
C                       degree based on a statistical F test of
C                       significance.  One of three possible
C                       significance levels will be used:  .01, .05 or
C                       .10.  If  EPS=-1.0 , the routine will
C                       automatically select one of these levels based
C                       on the number of data points and the maximum
C                       degree to be considered.  If  EPS  is input as
C                       -.01, -.05, or -.10, a significance level of
C                       .01, .05, or .10, respectively, will be used.
C                  (2)  If  EPS  is set to 0.,  PDA_DPOLFT  computes the
C                       polynomials of degrees 0 through  MAXDEG .
C                  (3)  If  EPS  is input positive,  EPS  is the RMS
C                       error tolerance which must be satisfied by the
C                       fitted polynomial.  PDA_DPOLFT  will increase the
C                       degree of fit until this criterion is met or
C                       until the maximum degree is reached.
C
C     Output -- All TYPE REAL variables are DOUBLE PRECISION
C         NDEG -   degree of the highest degree fit computed.
C         EPS -    RMS error of the polynomial of degree  NDEG .
C         R -      vector of dimension at least NDEG containing values
C                  of the fit of degree  NDEG  at each of the  X(I) .
C                  Except when the statistical test is used, these
C                  values are more accurate than results from subroutine
C                  PDA_DP1VLU  normally are.
C         IERR -   error flag with the following possible values.
C             1 -- indicates normal execution, i.e., either
C                  (1)  the input value of  EPS  was negative, and the
C                       computed polynomial fit of degree  NDEG
C                       satisfies the specified F test, or
C                  (2)  the input value of  EPS  was 0., and the fits of
C                       all degrees up to  MAXDEG  are complete, or
C                  (3)  the input value of  EPS  was positive, and the
C                       polynomial of degree  NDEG  satisfies the RMS
C                       error requirement.
C             2 -- invalid input parameter.  At least one of the input
C                  parameters has an illegal value and must be corrected
C                  before  PDA_DPOLFT  can proceed.  Valid input results
C                  when the following restrictions are observed
C                       N .GE. 1
C                       0 .LE. MAXDEG .LE. N-1  for  EPS .GE. 0.
C                       0 .LE. MAXDEG .LE. N-2  for  EPS .LT. 0.
C                       W(1)=-1.0  or  W(I) .GT. 0., I=1,...,N .
C             3 -- cannot satisfy the RMS error requirement with a
C                  polynomial of degree no greater than  MAXDEG .  Best
C                  fit found is of degree  MAXDEG .
C             4 -- cannot satisfy the test for significance using
C                  current value of  MAXDEG .  Statistically, the
C                  best fit found is of order  NORD .  (In this case,
C                  NDEG will have one of the values:  MAXDEG-2,
C                  MAXDEG-1, or MAXDEG).  Using a higher value of
C                  MAXDEG  may result in passing the test.
C         A -      work and output array having at least 3N+3MAXDEG+3
C                  locations
C         STATUS - Returned error status.
C                  The status must be zero on entry. This
C                  routine does not check the status on entry.
C
C     Note - PDA_DPOLFT  calculates all fits of degrees up to and including
C            NDEG .  Any or all of these fits can be evaluated or
C            expressed as powers of (X-C) using  PDA_DP1VLU  and  PDA_DPCOEF
C            after just one call to  PDA_DPOLFT .
C
C***REFERENCES  L. F. Shampine, S. M. Davenport and R. E. Huddleston,
C                 Curve fitting by polynomials in one variable, Report
C                 SLA-74-0270, Sandia Laboratories, June 1974.
C***ROUTINES CALLED  PDA_DP1VLU, PDA_XERMSG
C***REVISION HISTORY  (YYMMDD)
C   740601  DATE WRITTEN
C   890531  Changed all specific intrinsics to generic.  (WRB)
C   891006  Cosmetic changes to prologue.  (WRB)
C   891006  REVISION DATE from Version 3.2
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900315  CALLs to XERROR changed to CALLs to PDA_XERMSG.  (THJ)
C   900911  Added variable YP to DOUBLE PRECISION declaration.  (WRB)
C   920501  Reformatted the REFERENCES section.  (WRB)
C   920527  Corrected erroneous statements in DESCRIPTION.  (WRB)
C   950404  Implement status.  (HME)
C   950517  Return immediately if PDA_DP1VLU returns a status.  (HME)
C***END PROLOGUE  PDA_DPOLFT
      INTEGER STATUS
      INTEGER I,IDEGF,IERR,J,JP1,JPAS,K1,K1PJ,K2,K2PJ,K3,K3PI,K4,
     * K4PI,K5,K5PI,KSIG,M,MAXDEG,MOP1,NDEG,NDER,NFAIL
      DOUBLE PRECISION TEMD1,TEMD2
      DOUBLE PRECISION A(*),DEGF,DEN,EPS,ETST,F,FCRIT,R(*),SIG,SIGJ,
     * SIGJM1,SIGPAS,TEMP,X(*),XM,Y(*),YP,W(*),W1,W11
      DOUBLE PRECISION CO(4,3)
      SAVE CO
      DATA  CO(1,1), CO(2,1), CO(3,1), CO(4,1), CO(1,2), CO(2,2),
     1      CO(3,2), CO(4,2), CO(1,3), CO(2,3), CO(3,3),
     2  CO(4,3)/-13.086850D0,-2.4648165D0,-3.3846535D0,-1.2973162D0,
     3          -3.3381146D0,-1.7812271D0,-3.2578406D0,-1.6589279D0,
     4          -1.6282703D0,-1.3152745D0,-3.2640179D0,-1.9829776D0/
C***FIRST EXECUTABLE STATEMENT  PDA_DPOLFT
      M = ABS(N)
      IF (M .EQ. 0) GO TO 30
      IF (MAXDEG .LT. 0) GO TO 30
      A(1) = MAXDEG
      MOP1 = MAXDEG + 1
      IF (M .LT. MOP1) GO TO 30
      IF (EPS .LT. 0.0D0 .AND.  M .EQ. MOP1) GO TO 30
      XM = M
      ETST = EPS*EPS*XM
      IF (W(1) .LT. 0.0D0) GO TO 2
      DO 1 I = 1,M
        IF (W(I) .LE. 0.0D0) GO TO 30
 1      CONTINUE
      GO TO 4
 2    DO 3 I = 1,M
 3      W(I) = 1.0D0
 4    IF (EPS .GE. 0.0D0) GO TO 8
C
C DETERMINE SIGNIFICANCE LEVEL INDEX TO BE USED IN STATISTICAL TEST FOR
C CHOOSING DEGREE OF POLYNOMIAL FIT
C
      IF (EPS .GT. (-.55D0)) GO TO 5
      IDEGF = M - MAXDEG - 1
      KSIG = 1
      IF (IDEGF .LT. 10) KSIG = 2
      IF (IDEGF .LT. 5) KSIG = 3
      GO TO 8
 5    KSIG = 1
      IF (EPS .LT. (-.03D0)) KSIG = 2
      IF (EPS .LT. (-.07D0)) KSIG = 3
C
C INITIALIZE INDEXES AND COEFFICIENTS FOR FITTING
C
 8    K1 = MAXDEG + 1
      K2 = K1 + MAXDEG
      K3 = K2 + MAXDEG + 2
      K4 = K3 + M
      K5 = K4 + M
      DO 9 I = 2,K4
 9      A(I) = 0.0D0
      W11 = 0.0D0
      IF (N .LT. 0) GO TO 11
C
C UNCONSTRAINED CASE
C
      DO 10 I = 1,M
        K4PI = K4 + I
        A(K4PI) = 1.0D0
 10     W11 = W11 + W(I)
      GO TO 13
C
C CONSTRAINED CASE
C
 11   DO 12 I = 1,M
        K4PI = K4 + I
 12     W11 = W11 + W(I)*A(K4PI)**2
C
C COMPUTE FIT OF DEGREE ZERO
C
 13   TEMD1 = 0.0D0
      DO 14 I = 1,M
        K4PI = K4 + I
        TEMD1 = TEMD1 + W(I)*Y(I)*A(K4PI)
 14     CONTINUE
      TEMD1 = TEMD1/W11
      A(K2+1) = TEMD1
      SIGJ = 0.0D0
      DO 15 I = 1,M
        K4PI = K4 + I
        K5PI = K5 + I
        TEMD2 = TEMD1*A(K4PI)
        R(I) = TEMD2
        A(K5PI) = TEMD2 - R(I)
 15     SIGJ = SIGJ + W(I)*((Y(I)-R(I)) - A(K5PI))**2
      J = 0
C
C SEE IF POLYNOMIAL OF DEGREE 0 SATISFIES THE DEGREE SELECTION CRITERION
C
      IF (EPS) 24,26,27
C
C INCREMENT DEGREE
C
 16   J = J + 1
      JP1 = J + 1
      K1PJ = K1 + J
      K2PJ = K2 + J
      SIGJM1 = SIGJ
C
C COMPUTE NEW B COEFFICIENT EXCEPT WHEN J = 1
C
      IF (J .GT. 1) A(K1PJ) = W11/W1
C
C COMPUTE NEW A COEFFICIENT
C
      TEMD1 = 0.0D0
      DO 18 I = 1,M
        K4PI = K4 + I
        TEMD2 = A(K4PI)
        TEMD1 = TEMD1 + X(I)*W(I)*TEMD2*TEMD2
 18     CONTINUE
      A(JP1) = TEMD1/W11
C
C EVALUATE ORTHOGONAL POLYNOMIAL AT DATA POINTS
C
      W1 = W11
      W11 = 0.0D0
      DO 19 I = 1,M
        K3PI = K3 + I
        K4PI = K4 + I
        TEMP = A(K3PI)
        A(K3PI) = A(K4PI)
        A(K4PI) = (X(I)-A(JP1))*A(K3PI) - A(K1PJ)*TEMP
 19     W11 = W11 + W(I)*A(K4PI)**2
C
C GET NEW ORTHOGONAL POLYNOMIAL COEFFICIENT USING PARTIAL DOUBLE
C PRECISION
C
      TEMD1 = 0.0D0
      DO 20 I = 1,M
        K4PI = K4 + I
        K5PI = K5 + I
        TEMD2 = W(I)*((Y(I)-R(I))-A(K5PI))*A(K4PI)
 20     TEMD1 = TEMD1 + TEMD2
      TEMD1 = TEMD1/W11
      A(K2PJ+1) = TEMD1
C
C UPDATE POLYNOMIAL EVALUATIONS AT EACH OF THE DATA POINTS, AND
C ACCUMULATE SUM OF SQUARES OF ERRORS.  THE POLYNOMIAL EVALUATIONS ARE
C COMPUTED AND STORED IN EXTENDED PRECISION.  FOR THE I-TH DATA POINT,
C THE MOST SIGNIFICANT BITS ARE STORED IN  R(I) , AND THE LEAST
C SIGNIFICANT BITS ARE IN  A(K5PI) .
C
      SIGJ = 0.0D0
      DO 21 I = 1,M
        K4PI = K4 + I
        K5PI = K5 + I
        TEMD2 = R(I) + A(K5PI) + TEMD1*A(K4PI)
        R(I) = TEMD2
        A(K5PI) = TEMD2 - R(I)
 21     SIGJ = SIGJ + W(I)*((Y(I)-R(I)) - A(K5PI))**2
C
C SEE IF DEGREE SELECTION CRITERION HAS BEEN SATISFIED OR IF DEGREE
C MAXDEG  HAS BEEN REACHED
C
      IF (EPS) 23,26,27
C
C COMPUTE F STATISTICS  (INPUT EPS .LT. 0.)
C
 23   IF (SIGJ .EQ. 0.0D0) GO TO 29
      DEGF = M - J - 1
      DEN = (CO(4,KSIG)*DEGF + 1.0D0)*DEGF
      FCRIT = (((CO(3,KSIG)*DEGF) + CO(2,KSIG))*DEGF + CO(1,KSIG))/DEN
      FCRIT = FCRIT*FCRIT
      F = (SIGJM1 - SIGJ)*DEGF/SIGJ
      IF (F .LT. FCRIT) GO TO 25
C
C POLYNOMIAL OF DEGREE J SATISFIES F TEST
C
 24   SIGPAS = SIGJ
      JPAS = J
      NFAIL = 0
      IF (MAXDEG .EQ. J) GO TO 32
      GO TO 16
C
C POLYNOMIAL OF DEGREE J FAILS F TEST.  IF THERE HAVE BEEN THREE
C SUCCESSIVE FAILURES, A STATISTICALLY BEST DEGREE HAS BEEN FOUND.
C
 25   NFAIL = NFAIL + 1
      IF (NFAIL .GE. 3) GO TO 29
      IF (MAXDEG .EQ. J) GO TO 32
      GO TO 16
C
C RAISE THE DEGREE IF DEGREE  MAXDEG  HAS NOT YET BEEN REACHED  (INPUT
C EPS = 0.)
C
 26   IF (MAXDEG .EQ. J) GO TO 28
      GO TO 16
C
C SEE IF RMS ERROR CRITERION IS SATISFIED  (INPUT EPS .GT. 0.)
C
 27   IF (SIGJ .LE. ETST) GO TO 28
      IF (MAXDEG .EQ. J) GO TO 31
      GO TO 16
C
C RETURNS
C
 28   IERR = 1
      NDEG = J
      SIG = SIGJ
      GO TO 33
 29   IERR = 1
      NDEG = JPAS
      SIG = SIGPAS
      GO TO 33
 30   IERR = 2
      CALL PDA_XERMSG ('SLATEC', 'PDA_DPOLFT',
     +   'INVALID INPUT PARAMETER.', 2, 1, STATUS)
      GO TO 37
 31   IERR = 3
      NDEG = MAXDEG
      SIG = SIGJ
      GO TO 33
 32   IERR = 4
      NDEG = JPAS
      SIG = SIGPAS
C
 33   A(K3) = NDEG
C
C WHEN STATISTICAL TEST HAS BEEN USED, EVALUATE THE BEST POLYNOMIAL AT
C ALL THE DATA POINTS IF  R  DOES NOT ALREADY CONTAIN THESE VALUES
C
      IF(EPS .GE. 0.0  .OR.  NDEG .EQ. MAXDEG) GO TO 36
      NDER = 0
      DO 35 I = 1,M
        CALL PDA_DP1VLU (NDEG,NDER,X(I),R(I),YP,A,STATUS)
        IF (STATUS .NE. 0) RETURN
 35     CONTINUE
 36   EPS = SQRT(SIG/XM)
 37   RETURN
      END
*
*
*DECK PDA_DPCOEF
      SUBROUTINE PDA_DPCOEF (L, C, TC, A, STATUS)
C***BEGIN PROLOGUE  PDA_DPCOEF
C***PURPOSE  Convert the PDA_DPOLFT coefficients to Taylor series form.
C***LIBRARY   SLATEC
C***CATEGORY  K1A1A2
C***TYPE      DOUBLE PRECISION (PCOEF-S, PDA_DPCOEF-D)
C***KEYWORDS  CURVE FITTING, DATA FITTING, LEAST SQUARES, POLYNOMIAL FIT
C***AUTHOR  Shampine, L. F., (SNLA)
C           Davenport, S. M., (SNLA)
C***DESCRIPTION
C
C     Abstract
C
C     PDA_DPOLFT  computes the least squares polynomial fit of degree  L  as
C     a sum of orthogonal polynomials.  PDA_DPCOEF  changes this fit to its
C     Taylor expansion about any point  C , i.e. writes the polynomial
C     as a sum of powers of (X-C).  Taking  C=0.  gives the polynomial
C     in powers of X, but a suitable non-zero  C  often leads to
C     polynomials which are better scaled and more accurately evaluated.
C
C     The parameters for  PDA_DPCOEF  are
C
C     INPUT -- All TYPE REAL variables are DOUBLE PRECISION
C         L -      Indicates the degree of polynomial to be changed to
C                  its Taylor expansion.  To obtain the Taylor
C                  coefficients in reverse order, input  L  as the
C                  negative of the degree desired.  The absolute value
C                  of L  must be less than or equal to NDEG, the highest
C                  degree polynomial fitted by  PDA_DPOLFT .
C         C -      The point about which the Taylor expansion is to be
C                  made.
C         A -      Work and output array containing values from last
C                  call to  PDA_DPOLFT .
C
C     OUTPUT -- All TYPE REAL variables are DOUBLE PRECISION
C         TC -     Vector containing the first LL+1 Taylor coefficients
C                  where LL=ABS(L).  If  L.GT.0 , the coefficients are
C                  in the usual Taylor series order, i.e.
C                    P(X) = TC(1) + TC(2)*(X-C) + ... + TC(N+1)*(X-C)**N
C                  If L .LT. 0, the coefficients are in reverse order,
C                  i.e.
C                    P(X) = TC(1)*(X-C)**N + ... + TC(N)*(X-C) + TC(N+1)
C         STATUS - Returned error status.
C                  The status must be zero on entry. This
C                  routine does not check the status on entry.
C
C***REFERENCES  L. F. Shampine, S. M. Davenport and R. E. Huddleston,
C                 Curve fitting by polynomials in one variable, Report
C                 SLA-74-0270, Sandia Laboratories, June 1974.
C***ROUTINES CALLED  PDA_DP1VLU
C***REVISION HISTORY  (YYMMDD)
C   740601  DATE WRITTEN
C   890531  Changed all specific intrinsics to generic.  (WRB)
C   891006  Cosmetic changes to prologue.  (WRB)
C   891006  REVISION DATE from Version 3.2
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   920501  Reformatted the REFERENCES section.  (WRB)
C   950404  Implement status.  (HME)
C   950517  Return immediately if PDA_DP1VLU returns a status.  (HME)
C***END PROLOGUE  PDA_DPCOEF
C
      INTEGER STATUS
      INTEGER I,L,LL,LLP1,LLP2,NEW,NR
      DOUBLE PRECISION A(*),C,FAC,SAVE,TC(*)
C***FIRST EXECUTABLE STATEMENT  PDA_DPCOEF
      LL = ABS(L)
      LLP1 = LL + 1
      CALL PDA_DP1VLU (LL,LL,C,TC(1),TC(2),A,STATUS)
      IF (STATUS .NE. 0) RETURN
      IF (LL .LT. 2) GO TO 2
      FAC = 1.0D0
      DO 1 I = 3,LLP1
        FAC = FAC*(I-1)
 1      TC(I) = TC(I)/FAC
 2    IF (L .GE. 0) GO TO 4
      NR = LLP1/2
      LLP2 = LL + 2
      DO 3 I = 1,NR
        SAVE = TC(I)
        NEW = LLP2 - I
        TC(I) = TC(NEW)
 3      TC(NEW) = SAVE
 4    RETURN
      END
*
*
      SUBROUTINE PDA_CHE2D( NPTS, XMIN, XMAX, X, YMIN, YMAX, Y, XDEG,
     :                      YDEG, NCOEF, CC, NW, WORK, EVAL, IFAIL )
*+
*  Name:
*     PDA_CHE2X

*  Purpose:
*     Evaluates a 2-dimensional Chebyshev polynomial.

*  Language:
*     Starlink Fortran 77

*  Invocation:
*     CALL PDA_CHE2X( NPTS, XMIN, XMAX, X, YMIN, YMAX, Y, XDEG,
*                     YDEG, NCOEF, CC, NW, WORK, EVAL, IFAIL )

*  Description:
*     This routine evaluates a two-dimensional Chebyshev polynomial for
*     one or more arguments.  It uses Clenshaw's recurrence
*     relationship twice.

*  Arguments:
*     XMIN = DOUBLE PRECISION (Given)
*        The lower endpoint of the range of the fit along the first
*        dimension.  The Chebyshev series representation is in terms of
*        a normalised variable, evaluated as (2x - (XMAX + XMIN) ) /
*        (XMAX - XMIN), where x is the original variable.  XMIN must be
*        less than XMAX.
*     XMAX = DOUBLE PRECISION (Given)
*        The upper endpoint of the range of the fit along the second
*        dimension.  See XMIN.
*     X( NPTS ) = DOUBLE PRECISION (Given)
*        The co-ordinates along the first dimension for which the
*        Chebyshev polynomial is to be evaluated.
*     YMIN = DOUBLE PRECISION (Given)
*        The lower endpoint of the range of the fit along the first
*        dimension.  The Chebyshev series representation is in terms of
*        a normalised variable, evaluated as (2y - (YMAX + YMIN) ) /
*        (YMAX - YMIN), where y is the original variable.  YMIN must be
*        less than YMAX.
*     YMAX = DOUBLE PRECISION (Given)
*        The upper endpoint of the range of the fit along the second
*        dimension.  See YMIN.
*     Y = DOUBLE PRECISION (Given)
*        The co-ordinate along the second dimension for which the
*        Chebyshev polynomial is to be evaluated.
*     XDEG = INTEGER (Given)
*        The degree of the polynomial along the first dimension.
*     YDEG = INTEGER (Given)
*        The degree of the polynomial along the second dimension.
*     MCOEF = INTEGER (Given)
*        The number of coefficients.  This must be at least the product
*        of (XDEG+1) * (YDEG+1).
*     CC( MCOEF ) = DOUBLE PRECISION (Given)
*        The Chebyshev coefficients.  These should be the order such
*        that CCij is in CC( i*(YDEG+1)+j+1 ) for i=0,XDEG; j=0,YDEG.
*        In other words the opposite order to Fortran standard.
*     NW = INTEGER (Given)
*        The number of elements in the work array.  It must be at least
*        XDEG + 1.
*     WORK( NW ) = DOUBLE PRECISION (Returned)
*        Workspace.
*     EVAL( NPTS ) = DOUBLE PRECISION (Returned)
*        The evaluated polynomial for the supplied arguments.  Should an
*        element of argument X lie beyond the range [XMIN,XMAX], IFAIL=7
*        is reutrned.
*     IFAIL = INTEGER (Returned)
*        The status.  A value of 0 indicates that the routine completed
*        successfully.  Positive values indicate the following errors:
*
*           IFAIL = 1    XMAX less than or equal to XMIN
*           IFAIL = 2    YMAX less than or equal to YMIN
*           IFAIL = 3    NCOEF less than 1.
*           IFAIL = 4    XDEG or YDEG less than 1.
*           IFAIL = 5    Number of coefficients is too great, namely
*                        (XDEG+1)*(YDEG+1) is greater than NCOEF.
*           IFAIL = 6    Y lies outside the range YMIN to YMAX.
*           IFAIL = 7    An element of X lies outside the range XMIN to
*                        XMAX.

*  Notes:
*     - A single precision version of this function is available, named
*     PDA_CHE2R.

*  [optional_subroutine_items]...

*  Authors:
*     MJC: Malcolm J. Currie (STARLINK)
*     {enter_new_authors_here}

*  History:
*     1997 February 28 (MJC):
*        Original version based upon KPG1_CHE2x.
*     {enter_changes_here}

*  Bugs:
*     {note_any_bugs_here}

*-
      
*  Type Definitions:
      IMPLICIT NONE              ! No implicit typing

*  Arguments Given:
      INTEGER NPTS
      DOUBLE PRECISION XMIN
      DOUBLE PRECISION XMAX
      DOUBLE PRECISION X( NPTS )
      DOUBLE PRECISION YMIN
      DOUBLE PRECISION YMAX
      DOUBLE PRECISION Y
      INTEGER XDEG
      INTEGER YDEG
      INTEGER NCOEF
      DOUBLE PRECISION CC( NCOEF )
      INTEGER NW

*  Arguments Returned:
      DOUBLE PRECISION WORK( NW )
      DOUBLE PRECISION EVAL( NPTS )

*  Status:
      INTEGER IFAIL              ! Returned status

*  Local Variables:
      INTEGER IOFFCC             ! Index offset of Chebyshev coefficient
      DOUBLE PRECISION D                  ! Summation to current order
      DOUBLE PRECISION DP1                ! Summation to current order plus one
      DOUBLE PRECISION DP2                ! Summation to current order plus two
      INTEGER ECOEF              ! Expected number of Chebyshev
                                 ! coefficients
      INTEGER I                  ! Loop counter for summation over
                                 ! second axis
      INTEGER K                  ! Loop counter for co-ordinates
      INTEGER ORDER              ! Loop counter for polynomial order
      DOUBLE PRECISION XN2                ! Twice the normalised first-axis
                                 ! co-ordinate
      DOUBLE PRECISION XNORM              ! Normalised first-axis co-ordinate
      DOUBLE PRECISION XR                 ! First-axis co-ordinate range
      DOUBLE PRECISION YN2                ! Twice the normalised second-axis
                                 ! co-ordinate
      DOUBLE PRECISION YNORM              ! Normalised second-axis co-ordinate
      DOUBLE PRECISION YR                 ! Second-axis co-ordinate range

*.

*  Initialsie the status.
      IFAIL = 0

*  Validate the data limits.
      IF ( XMAX .LE. XMIN ) THEN
         IFAIL = 1
         GOTO 999
      END IF

*  Validate the data limits.
      IF ( YMAX .LE. YMIN ) THEN
         IFAIL = 2
         GOTO 999
      END IF

*  Validate the number of coefficients.
      IF ( NCOEF .LT. 1 ) THEN
         IFAIL = 3
         GOTO 999
      END IF

*  Validate the orders.
      IF ( XDEG .LT. 1 .OR. YDEG .LT. 1 ) THEN
         IFAIL = 4
         GOTO 999
      END IF

*  Check that the number of coefficients is not excessive.
      ECOEF = ( XDEG + 1 ) * ( YDEG + 1 )
      IF ( ECOEF .GT. NCOEF ) THEN
         IFAIL = 5
         GOTO 999
      END IF

*  Test for an invalid Y co-ordinate.
      IF ( ( Y - YMAX ) * ( Y - YMIN ) .GT. 0.0D0 ) THEN
         IFAIL = 6
         GOTO 999
      END IF

*  Loop for each point to be evaluated.
      DO K = 1, NPTS

*  Check that the x co-ordinate is in range.  Exit with a failure if any
*  of the x co-ordinates lie out of the Chebyshev range. 
         IF ( ( X( K ) - XMAX ) * ( X( K ) - XMIN ) .GT. 0.0D0 ) THEN
            IFAIL = 7
            GOTO 999
         END IF
      END DO

*  Evaluate results.
*  =================

*  Define two useful variables.
      XR = XMAX - XMIN
      YR = YMAX - YMIN

*  Normalise the variable to lie in the range -1 to +1.  This form of
*  the expression guarantees that the computed normalised value lies
*  no more than four times the machine precision from its true value.
      YNORM = ( ( Y - YMIN ) - ( YMAX - Y ) ) / YR

*  Initialise variable for recurrence relationship.
      YN2 = 2.0D0 * YNORM

*  Sum the coefficients times the second-axis Chebyshev polynomials.
*  =================================================================

*  Apply Clenshaw's recurrence relationship for efficiency.  For terms
*  greater than NCOEF the value is zero.  Note the
      DO I = 1, XDEG + 1
         IOFFCC = ( I - 1 ) * ( YDEG + 1 )
         DP2 = 0.0D0
         DP1 = 0.0D0
         IF ( YDEG .GT. 1 ) THEN
            DO ORDER = YDEG + 1, 2, -1
               D = DP1
               DP1 = YN2 * DP1 - DP2 + CC( IOFFCC + ORDER )
               DP2 = D
            END DO
         END IF

*  The final iteration is different.  The constant term is half of the
*  coefficient in the Chebyshev series.
         WORK( I ) = DP1 * YNORM - DP2 + CC( IOFFCC + 1 ) / 2.0D0
      END DO

*  Loop for each point to be evaluated.
      DO K = 1, NPTS

*  Normalise the variable to lie in the range -1 to +1.  This form of
*  the expression guarantees that the computed normalised value lies
*  no more than four times the machine precision from its true value.
         XNORM = ( ( X( K ) - XMIN ) - ( XMAX - X( K ) ) ) / XR

*  Sum the C(I)s times the first-axis Chebyshev polynomials.
*  =========================================================

*  Initialise variable for recurrence relationship.
         XN2 = 2.0D0 * XNORM

*  Apply Clenshaw's recurrence relationship for efficiency.  For terms
*  greater than NCOEF the value is zero.
         DP2 = 0.0D0
         DP1 = 0.0D0
         IF ( XDEG .GT. 1 ) THEN
            DO ORDER = XDEG + 1, 2, -1
               D = DP1
               DP1 = XN2 * DP1 - DP2 + WORK( ORDER )
               DP2 = D
            END DO
         END IF

*  The final iteration is different.  The constant term is half of the
*  coefficient in the Chebyshev series.
         EVAL( K ) = DP1 * XNORM - DP2 + WORK( 1 ) / 2.0D0
      END DO

  999 CONTINUE

      END
