      subroutine vp_chanwav(wvd,ced,errtol,nitnx,nord)
*     Purpose: determine the value CED satisfying 
*     WVD=wlamd(CED)
*     wlamd is usually either a straight or Cheb poly.
*     INPUT:
*     wvd	dble	value
*     ced	dble	guesses solution
*     errtol	dble	tolerance
*     nitnx	integer	max number of iterations
*     nord	int	order number for coeffts
*     OUTPUT:
*     ced	dble	solution
*     
      include 'vp_sizes.f'
      double precision vp_wval,chebd
      double precision wvd,ced,errtol,cex
      double precision dxd,derav,t1,t2,t3,t4
      double precision cprime(maxwco),dnorm
*     general wavelength coefficients
      double precision wcfd(maxwco,maxnch)
      common/vpc_wcfd/wcfd
*     wavelength parameter sets
      character*8 wcfty2(maxnch)
      character*4 vacin2(maxnch)
      double precision helcf2(maxnch)
      integer nwcf2(maxnch),noffs2(maxnch)
      common/vpc_wcpars2/wcfty2,helcf2,nwcf2,noffs2,vacin2
*
*     solve directly, or use Newton-Raphson
*     on wvd=f(ced)*helfac*vacair(wvd)
*     so Dwvd/Dced=helfac*Df/Dced*(vacair+Dvacair/Df), and
*     make the approx that f=wvd for the (vac...   ) term.
*     Also, precise channel number not needed, so Edlen formula
*     (with coeffts used by Morton) will be adequate.
*
      n=nwcf2(nord)
      if(vacin2(nord)(1:3).eq.'air') then
*       set up air term (vacair+dvacair/dlamda)
        t1=1.0d4/wvd
        t2=t1*t1
        t3=2.94981d-2/(146d0-t2)
        t4=2.554d-4/(41.0d0-t2)
        derav=1.0d0 + 6.4328d-5 + 
     :            t3*(1.0d0+2.0d0*t2/((146.0d0-t2)*wvd)) +
     :            t4*(1.0d0+2.0d0*t2/((41.0d0-t2)*wvd))
       else
        derav=1.0d0
      end if
      if((wcfty2(nord)(1:3).ne.'log'.and.
     1   wcfty2(nord)(1:4).ne.'arra'.and.
     2   wcfty2(nord)(1:4).ne.'cheb').and.n.eq.2) then
*       linear: don't interpolate, just do it and exit
        ced=(wvd-wcfd(1,nord))/wcfd(2,nord)
*       write(6,*) 'Linear:',wvd,wcfd(1,nord),wcfd(2,nord),ced
        goto 901
      end if
*
      if(wcfty2(nord)(1:3).eq.'log'.or.
     :          wcfty2(nord)(1:4).eq.'arra') then
        if(wcfty2(nord)(1:3).eq.'log') then
*         log linear wavelengths 
          t1=helcf2(nord)
          if(vacin2(nord)(1:3).eq.'air') then
            t2=(1.0d4/wvd)**2
            t1=t1*(1.0d0 + 6.4328d-5 +
     :            2.94981d-2/(146.0d0-t2) + 2.554d-4/(41.0d0-t2))
          end if
*         can convert straight to channel number
*         by inverting wav = vacair*helcf* 10**(cf1 + cf2*n)
          ced=(dlog10(wvd/t1)-wcfd(1,nord))/wcfd(2,nord) +
     :         dble(noffs2(nord))
         else
*         straight array interpolation
          call vp_archwav(wvd,ced,wcfd(1,nord),n)
*         write(6,*) 'archwav',ced
        end if
       else
*       actual wavelengths, not logs
        dxd=1.0d0
        nitn=0
        if(wcfty2(nord)(1:4).eq.'cheb') then
*         set up cheb derivative coeffts as for NAG routine E02AHF
*         for method see Modern Comp Methods Ch8, NPL Notes on Applied Science
*         No 16 (2nd ed) HMSO 1961. Numerical recipes is very similar.
          nprime=n-2
          cprime(n)=0.0d0
          dnorm=2.0d0/(wcfd(2,nord)-wcfd(1,nord))
          cprime(n-1)=dble(2*(nprime-1))*wcfd(n,nord)*dnorm
          if(nprime.ge.3) then
*           use recursion
            do i=nprime-2,1,-1
              j=i+2
              cprime(j)=cprime(j+2)+dble(2*i)*wcfd(j+1,nord)*dnorm
            end do
          end if
*         first two are endpoints
          cprime(2)=wcfd(2,nord)
          cprime(1)=wcfd(1,nord)
        end if
*
        do while (dabs(dxd).gt.errtol.and.nitn.le.nitnx)
          nitn=nitn+1
          cex=ced-dble(noffs2(nord))
          if(wcfty2(nord)(1:4).eq.'cheb') then
*           Chebyschev polynomial evaluation
            t1=chebd(cprime,n,cex)
           else
*           assume a polynomial fit of order nwcf2(nord)-1
            t1 = wcfd(n,nord)*dble(n-1)
            do i = n-1, 2, -1
              t1 = t1 * cex + wcfd(i,nord)*dble(i-1)
            end do
          end if
          dxd=(vp_wval(ced,nord)-wvd)/(t1*helcf2(nord)*derav)
          ced=ced-dxd
        end do
        if(nitn.ge.nitnx) then
          write(6,
     1 '(''VP_CHANWAV: >'',i4,'' iterations, w='',3f10.2)') 
     2        nitnx,wvd,ced,dxd
        end if
      end if
 901  return
      end
