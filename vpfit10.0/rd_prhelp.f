      subroutine rd_prhelp(chstr)
*     Merely lists the commands available
*     from rd_prhelp.dat, if it can find it!
*     If file not there, then no help is printed.
      implicit none
      character*(*) chstr
      integer len,lcsrc
      character*132 inchstr
*
      integer lastchpos
      character*132 csrcpath
      common/vpc_srcpath/csrcpath
*
      inchstr=' '
*     if environment variable set, use that
      call getenv('RD_PRSETUP',inchstr)
      if(inchstr.eq.' ') then
*       Local defaults
*       try as a local file in source directory
        if(csrcpath(1:1).ne.' ') then
          lcsrc=lastchpos(csrcpath)
          if(csrcpath(lcsrc:lcsrc).eq.'/') then
            inchstr=csrcpath(1:lcsrc)//'rd_prhelp.dat'
           else
            inchstr=csrcpath(1:lcsrc)//'/rd_prhelp.dat'
          end if
          open(unit=8,file=inchstr,status='old',err=99)
         else
*         forget it, don't know where to look
          goto 99
        end if
       else
*       use environment variable filename
        open(unit=8,file=inchstr,status='old',err=99)
      end if
 1    read(8,'(a)',end=98) inchstr
*     shortened form default
      if(chstr(1:1).ne.'a'.and.chstr(1:1).ne.'A'.and.
     :     chstr(1:1).ne.'x'.and.inchstr(1:4).eq.'OTHE') goto 98
*     sort out the length required, and then print the line
      len=132
      do while(inchstr(len:len).eq.' '.and.len.gt.1)
        len=len-1
      end do
      if(inchstr(1:1).ne.'*') then
        write(6,'(a)') inchstr(1:len)
       else
        if(chstr(1:1).eq.'x') then
*         experimental commands
          write(6,'(a)') inchstr(2:len)//'  ****'
        end if
      end if
      goto 1
 98   close(unit=8)
      return
 99   continue
      write(6,*) 'Failed to find help file'
      goto 98
      end
