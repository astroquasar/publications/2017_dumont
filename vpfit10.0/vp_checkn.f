
      subroutine vp_checkn( np, parm, ipind )

*     have to make sure that if total drops below what all lines add to,
*     that all lines are reduced.

*     ajc 21 dec 93 made a lot simpler by forcing total tied blocks to
*     be contiguous - necessary for form following

*     ajc 12 mar 94 made more complicated - a & is the same as a % but
*     means that this + the following column density are equal(!)

      implicit none

      include 'vp_sizes.f'

*     Totals after lastch, column form following without sums after firstch 
      character*2 lastch,firstch
      common/vpc_ssetup/lastch,firstch

      integer np
      double precision parm( np )
      character*2 ipind( np )
*     logical functions:
      logical nocgt, noceq, form
      double precision temp
      integer top, bot, j, k, second, nuntie, ntie
      character*2 mainchar, secchar
      double precision total, subtotal, untietot, tieval

*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     Variables used: indvar=1 for logN, 0 for N, -1 for emission
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar

      top = 1
*     initialise to silly number [always reset]
      tieval=0.0d0
 10   continue
*        write ( 6, * ) 'checkn ', top
      bot = top
      form = .false.

*     is this entry special?
      if ( ipind( top )(1:1) .eq. special .or.
     :       ipind( top )(1:1) .eq. vspecial .or.
     :       nocgt( ipind( top )(1:1), lastch ) ) then 
*       find bottom of block
 20     continue
*       at bottom of all entries?
        if ( bot + noppsys .gt. np ) go to 30
*       jump down one if ok
        if ( ipind( bot + noppsys )(1:1) .eq. special ) then
          go to 30
        end if
        if ( ipind( bot + noppsys )(1:1) .eq. vspecial ) then
          if ( ipind( bot )(1:1) .eq. vspecial ) then
            bot = bot + noppsys
            go to 20
           else
            go to 30
          end if
        end if
*       next must be a char
        if ( ipind( bot )(1:1) .eq. special .or.
     :           ipind( bot )(1:1) .eq. vspecial ) then
          if ( nocgt( ipind(bot+noppsys)(1:1),lastch ) ) then
            bot = bot + noppsys
            go to 20
           else
            write(6,*) 'CHECKN: no label for block'
            stop
          end if
         else
          if ( ipind( bot ) .eq. ipind( bot + noppsys ) ) then
            bot = bot + noppsys
            go to 20
           else
            go to 30
          end if
        end if
 30     continue

*       now find the main character + check syntax
        mainchar = '  '
        j = top
 40     continue
        if ( j .gt. bot ) then
          write ( *, * ) 'CHECKN: no label in totalled chunk'
          stop
        end if
        if ( ipind( j )(1:1) .eq. special ) then
          form = .true.
          if ( j .eq. top ) then
            j = j + noppsys
            go to 40
           else
            write ( *, * ) 'CHECKN: special character not at top'
            stop
          end if
        end if
        if ( ipind( j )(1:1) .eq. vspecial ) then
          form = .true.
          j = j + noppsys
          go to 40
        end if
        mainchar = ipind( j )
        if ( .not. nocgt( mainchar(1:1), lastch ) ) then
          write ( *, * ) 'CHECKN: special block with ordinary label'
        end if

*       if this is form-following then find main block
        if ( form ) then
          second = nppcol
 60       continue
*         start of a new block?
          secchar = '  '
          j = second
          if ( ipind( second )(1:1) .eq. special .or.
     :              ipind( second )(1:1) .eq. vspecial .or.
     :              nocgt( ipind( second )(1:1), lastch ) ) then
 70         continue
            if ( j .gt. np ) then
              write ( *, * ) 'no end to referenced chunk'
              stop
            end if
            if ( ipind( j )(1:1) .eq. special ) then
              if ( j .eq. second ) then
                j = j + noppsys
                go to 70
               else
                write ( *, * ) 'special character not at top'
                stop
              end if
            end if
            if ( ipind( j )(1:1) .eq. vspecial ) then
              j = j + noppsys
              go to 70
            end if
            secchar = ipind( j )
            if ( .not. nocgt( secchar(1:1), lastch ) ) then
              write ( *, * ) 'special block with ordinary label'
            end if
          end if

*         if here then found a label or not special
          if ( .not. noceq( secchar(1:1), mainchar(1:1) ) ) then
            second = j + noppsys
            if ( second .gt. np ) then
              write ( *, * ) 'have not found reference'
              stop
            end if
            go to 60
          end if
          continue
        end if

*       if here than found a matching label or not form-following

*       if form following just adjust to (earlier + so correct) reference
*          write ( *, * ) 'top ', top
*          write ( *, * ) 'bot ', bot
*          write ( *, * ) 'second ', second
*          write ( *, * ) 'form ', form
*          write ( *, * ) 'chars ', mainchar, secchar
          
        if ( form .and. second .ne. top ) then
          do j = top + noppsys, bot, noppsys
            k = j + second - top
            parm( j ) = parm( k ) + 
     :                    parm( top ) - parm( second )
          end do
         else

*         otherwise find total and subtotal
          if(indvar.eq.1) then
            total = 10d0 ** parm( top )
           else
            total=parm(top)
          end if
          untietot = 0d0
          if ( ipind( top )(1:1) .eq. vspecial ) then
            nuntie = 0
           else
            nuntie = 1
          end if
          do j = top + noppsys, bot, noppsys
            if ( ipind( j - noppsys )(1:1) .ne. vspecial ) then
              if(indvar.eq.1) then
                untietot = untietot + 10d0 ** parm( j )
               else
                untietot = untietot + parm(j)
              end if
              nuntie = nuntie + 1
            end if
          end do

*         calculate tied value
          ntie = ( bot - top ) / noppsys + 1 - nuntie
*         write ( *, *  ) 'ntie ', ntie
          if ( ntie .gt. 0 ) then
            tieval = ( total - untietot ) / ntie
            if ( tieval .lt. 0.0d0 ) then
              tieval = 1d7
            end if
            subtotal = untietot + ( ntie - 1 ) * tieval
            total = subtotal + tieval
           else
            subtotal = untietot
            if ( subtotal .gt. total ) then
              total = total + 1d7
            end if
          end if

*         and set values
*            write ( *, * ) 'total ', log10( total )
*            write ( *, * ) 'tieval ', log10( tieval )
          if(indvar.eq.1) then
            parm( top ) = log10( total )
           else
            parm(top)=total
          end if
          do j = top + noppsys, bot, noppsys
            if ( ipind( j - noppsys )(1:1) .eq. vspecial ) then
              if(indvar.eq.1) then
                temp=tieval
                parm(j) = log10( temp )
               else
                parm(j)=tieval
              end if
            end if
          end do
        end if
      end if

*     next entry
      top = bot + noppsys
      if ( top .lt. np ) go to 10

      end

