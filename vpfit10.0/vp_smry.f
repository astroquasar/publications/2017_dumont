      subroutine vp_smry(ion,level,parm,parerr,ipind,np)
*     print out summary information on fort.26, using
*     some information set in common in ucoptv
*     IN:
*     ion
*     level
*     parm	d
*     parerr	d
*     ipind
*     np	integer	number of parameters in the fit
*
      implicit none
*
      include 'vp_sizes.f'
*
      character*2 ion(*)
      character*4 level(*)
      double precision parm(*),parerr(*)
      character*2 ipind(*)
      integer np
*
*     LOCAL:
      integer i,ilen,iostat,lenchx,lenkey,nlcvstr
      integer jv,k,lfl,lt1,nop1,ntemps,ndrptt
      double precision dminerr
      character*16 chxpp
      character*4 chcv9h
      character*132 chucfmh
      character*10 chdate,chtime
*
      integer lastchpos
*
*     COMMON:
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     iteration and statistics for fit
      double precision chisqvc,prtotc
      integer iterc,nptsc,ndftotc
      common/vpc_smry/chisqvc,prtotc,iterc,nptsc,ndftotc
*     line to chunk link, as character
      character*4 chlnk(maxnio)
      common/vpc_chlnchk/chlnk
*     rfc 6.7.06: replaces:
      integer linchk( maxnio )
      common/vpc_linchk/linchk
*     probability limits for adding extra lines
      integer nladded
      double precision chisqvh,probchb,probksb
      common/vpc_problims/chisqvh,probchb,probksb,nladded
*
      integer kdrop,ndrop,ndroptot,ndrwhy
      common/vpc_nrejs/ndrop,kdrop(maxnio),ndroptot,ndrwhy
*     Variables used: indvar=1 for logN, 0 for N, -1 for emission
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
      integer nptype
      double precision csiglfac
      common/vpc_nptype/csiglfac,nptype
*
*     printout control variables, 13 style
      logical lwrsum,lwropen,lerrsum
      character*60 cwrsum
      character*16 chext13
      integer nwrsum,lext13
      common/vpc_f13hout/cwrsum,nwrsum,lwrsum,lwropen,lerrsum,
     :     chext13,lext13
*     printout 26 - with errors
      logical lwr26s,lwr26open
      character*60 cwr26s
      integer nwr26s,ndp26s,len26tem
      common/vpc_f26hout/cwr26s,nwr26s,lwr26s,lwr26open,ndp26s,
     :     len26tem
*     old data file and parameters
      integer ncumset
      character*132 comchstr
      logical ldate26
      logical lcuminc
      common/vpc_comchstr/comchstr,ldate26,lcuminc,ncumset
*     common character handling variables
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
*     x-reference tied list
      integer mtxref(maxnpa)
      common/vpc_txref/mtxref
*     Next block not used here
*     Totals (col), or special (b), after this character 
*      character*2 lastch,firstch
*      common/vpc_ssetup/lastch,firstch
*     general control variables
      character*4 chcv(10)
      common/vpc_chcv/chcv
*     other rest wavelength stuff, if needed
      logical lqmucf,lchvsqmu
      double precision qscale
      double precision qmucf
      common/vpc_qmucf/qmucf(maxats),qscale,lqmucf,lchvsqmu
*     ucoptv o/p channels
      integer nopchan,nopchanh,nmonitor
      integer lt(3)             ! output channels for printing
      common/vpc_nopchan/lt,nopchan,nopchanh,nmonitor
*     ucoptv format specifier
      character*132 chformuc,chformer
      character*164 chformet
      common/vpc_ucformat/chformuc,chformer,chformet
*     first key word for each input line
      character*32 chkey(maxnch)
      common/vpc_chunkey/chkey
*     summary printout precision thresholds
      double precision dmer6,dmer8
      common/vpc_dminerval/dmer6,dmer8
*     parameter error estimates rescaled, or not? Default is yes
      logical lrsparmerr,luvespop
      common/vpc_rsparmerr/lrsparmerr,luvespop
*     chunk variables:
      integer ndpts(maxnch),idrun(maxnch),icrun(maxnch)
      integer indfil(maxnch)
      character*64 filename(maxnch)
      double precision wvstrt(maxnch),wvend(maxnch)
      integer nchunk
      common/vpc_chunk/wvstrt,wvend,ndpts,filename,
     :      idrun,icrun,indfil,nchunk
*     chunk start and end wavelengths
      double precision wvchlo(maxnch),wvchhi(maxnch)
      common/vpc_chunklims/wvchlo,wvchhi
*     probability estimates for each chunk
      integer indprobchk
      integer nprchk(maxnch)
      double precision probchk(maxnch)
      common/vpc_probchk/probchk,nprchk,indprobchk
*
      if(ndroptot.lt.0) then
*       just for consistency if file re-read:
        ndrptt=0
       else
        ndrptt=ndroptot
      end if
*     store print control
      lt1=lt(1)
      nop1=nopchan
      chcv9h=chcv(9)
*     and store format
      chucfmh=chformer
      if(lwrsum) then
*       old style 13 format
        write(19,*) ' *'
*       shuffle o/p channel variables, and use vp_ucprinit
        nopchan=1
        lt(1)=19
        chcv(9)=' '
        call vp_ucprinit(ion,level,parm,ipind,np)
*       error information
        write(19,*) 'Errors:'
        call vp_ucprinerr(ion,level,parerr,parm,ipind,np)
        write(19,*) ' '
*       and some statistical information:
        write(19,*) 'chi^2/N   npts    ndf    prob' 
        write(19,'(f8.4,i7,i7,f10.6)') chisqvc,
     :               nptsc,ndftotc,prtotc
        write(19,*) ' '
        close(unit=19)
        lwropen=.false.
      end if
*
*     fort.26 style and related
      do k=1,nchunk
*       length filename string
        lfl=len(filename(k))
        do while (lfl.gt.12.and.filename(k)(lfl:lfl).eq.' ')
          lfl=lfl-1
        end do
        if(chkey(k)(1:1).ne.' ') then
          lenkey=lastchpos(chkey(k))
         else
          lenkey=1
        end if
        if(k.eq.1.and.ldate26) then
*         get date and time
          call vp_dattim(chdate,chtime)
          if(indprobchk.ge.1) then
            if(indprobchk.ge.2) then
              write(26,'(a3,a,a1,i5,2f12.4,'' '',a,'' '','//
     :        'a3,f6.3,i6,'' '',a4,a1,a2,a1,a2)') 
     :        '%% ',filename(k)(1:lfl),' ',idrun(k),
     :        wvchlo(k),wvchhi(k),chkey(k)(1:lenkey),' ! ',
     :        probchk(k),nprchk(k),
     :        chdate(1:4),'/',chdate(5:6),'/',chdate(7:8)
             else
              write(26,'(a3,a,a1,i5,2f12.4,'' '',a,'' '','//
     :        'a3,f6.3,'' '',a4,a1,a2,a1,a2)') 
     :        '%% ',filename(k)(1:lfl),' ',idrun(k),
     :        wvchlo(k),wvchhi(k),chkey(k)(1:lenkey),
     :        ' ! ',probchk(k),
     :        chdate(1:4),'/',chdate(5:6),'/',chdate(7:8)
            end if
           else
            write(26,'(a3,a,a1,i5,2f12.4,'' '',a,'' '','//
     :      'a3,'' '',a4,a1,a2,a1,a2)') 
     :      '%% ',filename(k)(1:lfl),' ',idrun(k),
     :      wvchlo(k),wvchhi(k),chkey(k)(1:lenkey),' ! ',
     :      chdate(1:4),'/',chdate(5:6),'/',chdate(7:8)
          end if
         else
          if(indprobchk.ge.1) then
            if(indprobchk.ge.2) then
              write(26,'(a3,a,a1,i5,2f12.4,'' '',a,'' '','//
     :        'a3,f6.3,i6,i4)') 
     :        '%% ',filename(k)(1:lfl),' ',idrun(k),
     :        wvchlo(k),wvchhi(k),chkey(k)(1:lenkey),
     :        ' ! ',probchk(k),nprchk(k),k
             else
              write(26,'(a3,a,a1,i5,2f12.4,'' '',a,'' '','//
     :        'a3,f6.3,i4)') 
     :        '%% ',filename(k)(1:lfl),' ',idrun(k),
     :        wvchlo(k),wvchhi(k),chkey(k)(1:lenkey),
     :        ' ! ',probchk(k),k
            end if
           else
            write(26,'(a3,a,a1,i5,2f12.4,'' '',a,'' '',a3,i4)') 
     :      '%% ',filename(k)(1:lfl),' ',idrun(k),
     :      wvchlo(k),wvchhi(k),chkey(k)(1:lenkey),' ! ',k
          end if
        end if
      end do
*     if upper limits, say what they are
      if(nptype.ne.0.and.csiglfac.gt.1.001d0) then
        write(26,'(a,f4.1,a)') '! Upper limits are ',csiglfac,'-sigma'
      end if
*     global fit statistics etc output as a separate comment line to 26
      if(prtotc.ge.probchb) then
        chxpp=' '
        lenchx=1
        if(chisqvc.le.0.8d0) then
          if(lrsparmerr.and.(.not.luvespop)) then
            chxpp=' ! errs rescaled'
            lenchx=16
           else
            chxpp=' ! raw errors'
            lenchx=13
          end if
        end if
       else
        chxpp='  ! BAD'
        lenchx=7
      end if
      if(chisqvc.gt.99999.0d0) then
        write(26,'(a,i4,1pe14.5,2i5,0pf7.3,1x,i2,a8,a)') '! Stats:',
     :          iterc,chisqvc,nptsc,ndftotc,prtotc,ndrptt,
     :          chxpp(1:lenchx)
       else      
        write(26,'(a,i4,f14.7,2i5,f7.3,1x,i2,a8,a)') '! Stats:',
     :          iterc,chisqvc,nptsc,ndftotc,prtotc,ndrptt,
     :          chxpp(1:lenchx)
      end if
*
      lt(1)=26
      chcv(9)='c26f'
*     determine no. of significant figures needed for redshifts
      dminerr=1.0d25
      ntemps=np/noppsys
      do i=1,ntemps
        jv=noppsys*(i-1)+nppzed
        if(parerr(jv).gt.0.0d0) then
          dminerr=min(dminerr,parerr(jv))
        end if
      end do
*     set format according to size of dminerr
      if(indvar.ne.1) then
*       linear format:
        if(noppsys.le.3) then
          chformer='(1x,a2,a4,1x,f10.6,a2,1x,f9.6,1x,f8.2,a2,'//
     :           '1x,f7.2,1x,1pe11.3,a2,e11.3,1x,a2,'' !'')'
          chformet='(1x,a2,a4,1x,f10.6,a2,1x,f9.6,1x,f8.2,a2,'//
     :           '1x,f7.2,1x,1pe11.3,a2,e11.3,1x,a2,'' [ '','//
     :           'f7.3,1x,f7.3,1pe10.3,e10.3)'
         else
          chformer='(1x,a2,a4,1x,f10.6,a2,1x,f9.6,1x,f8.2,a2,'//
     :        '1x,f7.2,1x,1pe11.3,a2,e11.3,1x,e10.2,a2,e10.2,'//
     :        '1x,a2,'' ! '')'
          chformet='(1x,a2,a4,1x,f10.6,a2,1x,f9.6,1x,f8.2,a2,'//
     :        '1x,f7.2,1x,1pe11.3,a2,e11.3,1x,e10.2,a2,e10.2,'//
     :        '1x,a2,'' [ '',f7.3,1x,f7.3,1pe10.3,e10.3)'
        end if
        goto 301
      end if
*     if reaches here, is using log column densities
      if(dminerr.gt.dmer6.or.chcv(8)(1:4).ne.'vfor') then
        if(noppsys.le.3) then
          chformer='(1x,a2,a4,1x,f10.6,a2,1x,f9.6,1x,f8.2,a2,'//
     :      '1x,f7.2,1x,f7.3,a2,f7.3,1x,a2'//
     :      ','' ! '')'
          chformet='(1x,a2,a4,1x,f10.6,a2,1x,f9.6,1x,f8.2,a2,'//
     :      '1x,f7.2,1x,f7.3,a2,f7.3,1x,a2'//
     :      ','' [ '',f7.3,1x,f7.3,1pe10.3,e10.3)'
         else
          chformer='(1x,a2,a4,1x,f10.6,a2,1x,f9.6,1x,f8.2,a2,'//
     :      '1x,f7.2,1x,f7.3,a2,f7.3,1x,1pe10.2,a2,e10.2,1x,a2'//
     :      ','' ! '')'
          chformet='(1x,a2,a4,1x,f10.6,a2,1x,f9.6,1x,f8.2,a2,'//
     :      '1x,f7.2,1x,f7.3,a2,f7.3,1x,1pe10.2,a2,e10.2,1x,a2'//
     :      ','' [ '',f7.3,1x,f7.3,1pe10.3,e10.3)'
        end if
       else
        if(dminerr.gt.dmer8) then
          if(noppsys.le.3) then
            chformer='(1x,a2,a4,1x,f12.8,a2,1x,f11.8,1x,f9.3,a2,'//
     :       '1x,f8.3,1x,f8.4,a2,f8.4,1x,a2'//
     :       ','' ! '')'
            chformet='(1x,a2,a4,1x,f12.8,a2,1x,f11.8,1x,f9.3,a2,'//
     :       '1x,f8.3,1x,f8.4,a2,f8.4,1x,a2'//
     :       ','' [ '',f7.3,1x,f7.3,1pe10.3,e10.3)'
           else
            chformer='(1x,a2,a4,1x,f12.8,a2,1x,f11.8,1x,f9.3,a2,'//
     :        '1x,f8.3,1x,f8.4,a2,f8.4,1x,1pe11.3,a2,e11.3,1x,a2'//
     :        ','' ! '')'
            chformet='(1x,a2,a4,1x,f12.8,a2,1x,f11.8,1x,f9.3,a2,'//
     :        '1x,f8.3,1x,f8.4,a2,f8.4,1x,1pe11.3,a2,e11.3,1x,a2'//
     :        ','' [ '',f7.3,1x,f7.3,1pe10.3,e10.3)'
          end if
         else
          if(noppsys.le.3) then
            chformer='(1x,a2,a4,1x,f14.10,a2,1x,f13.10,1x,f11.5,a2,'//
     :         '1x,f10.5,1x,f10.6,a2,f10.6,1x,a2'//
     :         ','' ! '')'
            chformet='(1x,a2,a4,1x,f14.10,a2,1x,f13.10,1x,f11.5,a2,'//
     :         '1x,f10.5,1x,f10.6,a2,f10.6,1x,a2'//
     :         ','' [ '',f7.3,1x,f7.3,1pe10.3,e10.3)'
           else
            chformer='(1x,a2,a4,1x,f14.10,a2,1x,f13.10,1x,f11.5,a2,'//
     :        '1x,f10.5,1x,f10.6,a2,f10.6,1x,1pe12.4,a2,e12.4,1x,a2'//
     :        ','' ! '')'
            chformet='(1x,a2,a4,1x,f14.10,a2,1x,f13.10,1x,f11.5,a2,'//
     :        '1x,f10.5,1x,f10.6,a2,f10.6,1x,1pe12.4,a2,e12.4,1x,a2'//
     :        ','' [ '',f7.3,1x,f7.3,1pe10.3,e10.3)'
          end if
        end if
      end if
 301  call vp_ucprinerr(ion,level,parerr,parm,ipind,np)
*     restore old values
      chformer=chucfmh
      lt(1)=lt1
      nopchan=nop1
      chcv(9)=chcv9h
*     if fort.26 written to a named file, close it
      if(lwr26open) then
        close(unit=26)
        lwr26open=.false.
*       If want to accumulate results, do it here
        if(lcuminc) then
*         separate filename and parameters
*         Attempt to separate filename and parameters into
*         <filename> (fmt,clo,chi,zlo,zhi,blo,bhi,binc,laonly)
          call sepvar(comchstr,15,rvstr,ivstr,cvstr,nvstr)
*         get length
          nlcvstr=lastchpos(cvstr(1))
          if(nlcvstr.gt.0) then
            ilen=lastchpos(cwr26s)
            if(ncumset.le.0) then
              call system('cat '//cwr26s(1:ilen)//' > '//
     1          cvstr(1)(1:nlcvstr))
              ncumset=1
             else
              call system('cat '//cwr26s(1:ilen)//' >> '//
     1          cvstr(1)(1:nlcvstr))
            end if
           else
*         old code:
*         check that file does not contain a list of filenames!
          open(unit=20,file=cvstr(1)(1:nlcvstr),status='old',
     1           err=9997)
          write(6,*) 'Appending results to ',cvstr(1)(1:nlcvstr)
*         read in all the old stuff first
          iostat=0
          do while (iostat.eq.0)
            read(20,'(a)',iostat=iostat) cvstr(15)
            write(6,*) '>>> in ',cvstr(15)(1:20)
          end do
          goto 9998
*         open as a new file
 9997     open(unit=20,file=cvstr(1)(1:nlcvstr),status='new',err=9098)
          write(6,*) 'Writing results to ',cvstr(1)(1:nlcvstr)
          goto 9998
 9098     write(6,*) '*** Failed to open file *** ',cvstr(1)(1:32)
          write(6,*) 'Writing to fort.20'
*
 9998     continue
*         write the new stuff
          if(cvstr(2)(1:4).eq.'list') then
            ilen=lastchpos(cwr26s)
            if(ilen.le.0) ilen=1
            write(20,'(a)') cwr26s(1:ilen)
           else
*           copy all the data from current fort.26 to the
*           end of the cumulative filename
            open(unit=26,file=cwr26s,status='old',err=9901)
            iostat=0
            do while (iostat.eq.0)
              read(26,'(a)',err=9902,end=9902) inchstr
              ilen=lastchpos(inchstr)
              if(ilen.le.0) ilen=1
              write(6,'(a,a)') '>>> ',inchstr(1:ilen)
              write(20,'(a)') inchstr(1:ilen)
            end do
 9902       close(unit=26)
          end if
 9901     close(unit=20)
          end if
        end if
      end if
*     write(6,*) 'EXIT vp_smry'
      return
      end


      subroutine vp_f13hout(filename,lxx,idrun,k,wastart,waend)
*
*     write out header for fort.13 and fort.26 style files
*
      implicit none
*
      include 'vp_sizes.f'
      character*12 chstr
      character*64 filename(*)
      integer lxx,k
      integer idrun(*)
      double precision wastart
      double precision waend
*
*     Local
      integer kmean,len,lenkey
      double precision wmean
*
*     Functions
      integer lastchpos
*
*     Common
*
*     printout control variables
      character*132 p13file
      common/vpc_p13out/p13file
      logical lwrsum,lwropen,lerrsum
      character*60 cwrsum
      character*16 chext13
      integer nwrsum,lext13
      common/vpc_f13hout/cwrsum,nwrsum,lwrsum,lwropen,lerrsum,
     :                chext13,lext13
      logical lwr26s,lwr26open
      character*60 cwr26s
      integer nwr26s,ndp26s,len26tem
      common/vpc_f26hout/cwr26s,nwr26s,lwr26s,lwr26open,ndp26s,
     :       len26tem
*     key word may be present
      character*32 chkey(maxnch)
      common/vpc_chunkey/chkey
*
*     fort.26 file:
      if(lwr26s) then
*       open a file name if not already open
        if(.not.lwr26open) then
*         generate filename
*         mean chunk wavelength for the current chunk (which will 
*         be the first one normally)
          wmean=0.5d0*(wastart+waend)
          if(ndp26s.gt.0) then
            if(ndp26s.eq.1) then
              write(chstr,'(f10.1)') wmean
              len=10
             else
              write(chstr,'(f11.2)') wmean
              len=11
            end if
           else
            kmean=int(wmean+0.5d0)
            write(chstr,'(i8)') kmean
            len=8
          end if
          do while(len.gt.1.and.chstr(1:1).eq.' ')
            chstr(1:12)=chstr(2:12)//' '
            len=len-1
          end do
          cwr26s=cwr26s(1:nwr26s)//chstr(1:len)
          len=len+nwr26s
*         check this file is not the same as fort.13, which
*         could already be open!
          if(cwr26s(1:len).eq.p13file(1:len)) then
*           need to change the output file name:
            if(cwr26s(nwr26s:nwr26s).ne.'w') then
              cwr26s(nwr26s:nwr26s)='w'
             else
              cwr26s(nwr26s:nwr26s)='x'
            end if
*           and warn the user
          end if
          write(6,*) ' '
          write(6,*) 'fort.26 summary to: ',cwr26s(1:len)
          write(6,*) ' '
          len26tem=len
          open(unit=26,file=cwr26s,status='unknown',err=965)
*         if can't open filename, then write to fort.19
 965      lwr26open=.true.
        end if
      end if
      if(lwrsum) then
*       open file if not already done
        if(.not.lwropen) then
*         generate filename
          if(chext13(1:1).eq.' ') then
*           mean chunk wavelength for the current chunk (which will 
*           be the first one normally)
            wmean=0.5d0*(wastart+waend)
            kmean=int(wmean+0.5d0)
            if(ndp26s.gt.0) then
              if(ndp26s.eq.1) then
                write(chstr,'(f10.1)') wmean
                len=10
               else
                write(chstr,'(f11.2)') wmean
                len=11
              end if
             else
              kmean=int(wmean+0.5d0)
              write(chstr,'(i8)') kmean
              len=8
            end if
            do while(len.gt.1.and.chstr(1:1).eq.' ')
              chstr(1:12)=chstr(2:12)//' '
              len=len-1
            end do
            cwrsum=cwrsum(1:nwrsum)//chstr(1:len)
            len=len+nwrsum
           else
            cwrsum=cwrsum(1:nwrsum)//chext13(1:lext13)
            len=lext13+nwrsum
          end if
*         check this file is not the same as fort.13, which
*         could already be open!
          if(cwrsum(1:len).eq.p13file(1:len)) then
*           need to change the output file name:
            if(cwrsum(nwrsum:nwrsum).ne.'w') then
              cwrsum(nwrsum:nwrsum)='w'
             else
              cwrsum(nwrsum:nwrsum)='x'
            end if
          end if
*         and warn the user
          write(6,*) ' '
          write(6,*) 'Summary to: ',cwrsum(1:len)
          write(6,*) ' '
*         unit 19 is used in vp_rdspecial, right at the beginning,
*         and then closed, so no conflict in using it again here:
          open(unit=19,file=cwrsum,status='unknown',err=966)
*         if can't open filename, then write to fort.19
 966      lwropen=.true.
          write(19,*) '  *'
        end if
        if(chkey(k)(1:1).eq.' ') then
          write(19,'(a,a1,i4,2f10.3)') filename(k)(1:lxx),' ',
     :        idrun(k),wastart,waend
         else
          lenkey=lastchpos(chkey(k))
          write(19,'(a,a1,i4,2f10.3,a2,a)') filename(k)(1:lxx),' ',
     :        idrun(k),wastart,waend,'  ',chkey(k)(1:lenkey)
        end if            
      end if
      return
      end
