      subroutine vp_parmcest(parm,nn,trstw,fval,obswv,nchunk)
*     determine values of parm array from cursor input
*     
*     IN:
*     parm	array	parameter values
*     nn	integer	base level for parm (uses nn+nppcol, .. etc)
*     trstw	dble	rest wavelength for the line
*     nchunk	integer	chunk number
*
*     OUT:
*     parm	array	parameters nn+nppcol & nn+nppbval may be updated
*
      implicit none
      include 'vp_sizes.f'
*
      double precision parm(*)
      integer nn
      double precision trstw,fval,obswv
      integer nchunk
*
*     LOCAL:
      integer intch
      integer j
      double precision wv2db,y1db
      double precision binst,ddum,dfac,dfacd,tau0,vsig,vstemp
      double precision contval
*
*     functions
      double precision vpf_dvresn
*
*     COMMON:
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     continuum and data 
*     current 1-D spectrum data, err, fluctuations, continuum -length ngp
      integer ngp
      double precision dhdata(maxfis),dherr(maxfis)
      double precision dhrms(maxfis),dhcont(maxfis)
      common/vpc_su1d/dhdata,dherr,dhrms,dhcont,ngp
*     pgplot CURSOR VARIABLES
      character*1 chc
      real wv1pg,y1pg               ! pgplot variables
      common/vpc_gcurv/wv1pg,y1pg

*
      write(6,*) 'set cursor x- wavelength, y- base of feature'
      call vp_pgcurs(wv1pg,y1pg,chc)
      obswv=dble(wv1pg)
      y1db=dble(y1pg)
*     channel corresponding to the wavelength
      ddum=1.0d3
      call vp_chanwav(obswv,ddum,3.0d-2,100,nchunk)
*     write(6,*) 'channel number: ',ddum
      intch=int(ddum)
      contval=dhcont(intch)
*     line center optical depth
      if(y1db.gt.0.0d0) then
        if(contval.gt.0.0d0) then
          tau0=log(contval/y1db)
         else
          tau0=1.0d0
        end if
       else
        tau0=10.0d0
      end if
*     might as well return cursor at half tau point
      dfac=0.5d0*tau0
      y1pg=real(contval*exp(-dfac))
*
      write(6,*) ' .. and now half width at half (optical) depth'
      call vp_pgcurs(wv1pg,y1pg,chc)
*
*     Go from width to b estimate
*     (Could use y values and gaussian, but not worth it)
      wv2db=dble(wv1pg)
      vsig=((wv2db-obswv)/obswv)*2.99792458d5
      if(vsig.lt.0.0d0) vsig=-vsig
*     sigma units
      vsig=0.85d0*vsig
*     subtract off chunk resolution at the central wavelength
*     in quadrature
*     and check if the value is less than 0.2*instrumental sigma
      binst=vpf_dvresn(obswv,nchunk)*2.99792458d5
      vstemp=vsig*vsig-binst*binst
      if(vstemp.gt.0.0d0) then
        vsig=sqrt(vstemp)
       else
        vsig=0.0d0
      end if
*     write(6,*) 'Inst sigma is ',binst
      if(vsig.le.0.2d0*binst) then
        vsig=0.2d0*binst
      end if
      parm(nn+nppbval)=1.414213562d0*vsig
*     optical depth determined from y1/cont(wv1) if parm(nn+nppcol)
*     has no sensible value
      if(parm(nn+nppcol).le.5.0d0) then
*       central depth/width 
        dfacd=1.4974d-15*fval*trstw/parm(nn+nppbval)
        if(dfacd.gt.0.0d0) then
          parm(nn+nppcol)=log10(dble(tau0)/dfacd)
         else
          parm(nn+nppcol) = 13.75d0
        end if
*       write(6,*) 'logN set to ',parm(nn+nppcol)
      end if
      if(noppsys.ge.4) then
*       extended parameter set - take defaults to be zero
        do j=4,noppsys
          parm(nn+j)=0.0d0
        end do
      end if            
      return
      end
