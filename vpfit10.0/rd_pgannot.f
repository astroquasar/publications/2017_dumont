      subroutine rd_pgannot(filename)
*     Use commands in filename to add various things to a plot
*     which will be expanded as we go along....
      implicit none
      character*(*) filename
      real pgchh
*     common string separation variables
      character*132 inchstr
      character*60 cv(24)
      real rv(24)
      integer iv(24)
      integer nv
      common/rd_chwork/inchstr,rv,iv,cv,nv
      double precision dv(24)
      common/vpc_dsepspace/dv
*
      open(unit=29,file=filename,status='old',err=987)
      call pgsave
*     store default character height in case you want it internally
*     (initialize to keep compiler happy)
      pgchh=1.0
      call pgqch(pgchh)
 981  read(29,'(a)',end=986) inchstr
      call sepvar(inchstr,24,rv,iv,cv,nv)
*     various pgplot commands (alphabetically)
      if(cv(1)(1:4).eq.'chsf'.and.iv(2).ge.1.and.iv(2).le.4) then
*       character font i1
        call pgscf(iv(2))
      end if
      if(cv(1)(1:4).eq.'chsh') then
*       character height r1, note changes tick spacing as well
        if(cv(2)(1:4).eq.'defa'.or.cv(2)(1:4).eq.'rest'.or.
     1     cv(2)(1:4).eq.'orig'.or.rv(2).le.0.0) then
*         restore character size to entry value
          call pgsch(pgchh)
         else
          call pgsch(rv(2))
        end if
      end if
      if(cv(1)(1:4).eq.'colo') then
*       color i1: set color index to i1
        call pgsci(iv(2))
        goto 981
      end if
      if(cv(1)(1:4).eq.'line') then
*       line x1 y1 (to) x2 y2
        call pgmove(rv(2),rv(3))
        call pgdraw(rv(4),rv(5))
        goto 981
      end if
      if(cv(1)(1:2).eq.'po'.or.cv(1)(1:2).eq.'pt') then
        if(iv(4).eq.0) then
*         this is normally an open box, but could
*         be character descriptor
          if(cv(4)(1:4).eq.'fbox') iv(4)=16
          if(cv(4)(1:4).eq.'fdot') iv(4)=17
          if(cv(4)(1:4).eq.'fsta') iv(4)=18
          if(cv(4)(1:4).eq.'ftri') iv(4)=13
          if(cv(4)(1:4).eq.'oc2 ') iv(4)=22
        end if
        call pgpt1(rv(2),rv(3),iv(4))
        goto 981
      end if
      if(cv(1)(1:4).eq.'styl') then
*       line style
        if(iv(2).le.0) then
*         descriptors allowed
          if(cv(2)(1:4).eq.'full') iv(2)=1
          if(cv(2)(1:4).eq.'dash') iv(2)=2
          if(cv(2)(1:3).eq.'dot') iv(2)=4
          if(cv(2)(1:5).eq.'dot-d') iv(2)=3
          if(cv(2)(1:6).eq.'dash-d') iv(2)=5
        end if
        if(iv(2).le.0.or.iv(2).gt.5) iv(2)=1
        call pgsls(iv(2))
        goto 981
      end if
      if(cv(1)(1:4).eq.'text') then
*       x,y,angle,just(0=left,1=right),text
        call pgptxt(rv(2),rv(3),rv(4),rv(5),cv(6))
        goto 981
      end if
      if(cv(1)(1:4).eq.'thic'.or.cv(1)(1:4).eq.'widt') then
*       set line thickness to i1
        call pgslw(iv(2))
        goto 981
      end if
      if(cv(1)(1:1).eq.'?') then
*       print a help list
        write(6,*) 'plot annotation: available options:'
        write(6,*) 'chsf i1: set font 1=normal, 2=rm, 3=it, 4=sc'
        write(6,*) 'chsh r1: set character height = r1*normal'
        write(6,*) 'color (or colour) i1: set pgplot color'
        write(6,*) 'point (or pt) x y symbol#: single pgplot marker'
        write(6,*) 'style i1: pgplot curve/line style'
        write(6,*) 'text x y angle just text_string: write text'
        write(6,*) 'width (or thickness) i1: line width'
      end if
*     nothing recognized, just loop back
      goto 981
 986  close(unit=29)
*     restore character size to entry value done by pgunsa
*     call pgsch(pgchh)
      call pgunsa
 985  return
 987  write(6,*)'File not opened: ',filename
      goto 985
      end
