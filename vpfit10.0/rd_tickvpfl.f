      subroutine rd_tickvpfl(chsxx)
*
      implicit none
      include 'vp_sizes.f'
*     put tick marks over velocity plot, using redshifts and ions from a
*     named file. Parameters handed over from chsxx (this
*     INCLUDES the letter code which initiates the calling sequence).

      character*(*) chsxx
      double precision zedd,zeddp1,dwave
      integer j
      real velspg,yltickpg,yhtickpg
      character*2 chtt
      character*2 atom,atomv
      character*4 ion,ionv
*     common string separation variables
      character*132 inchstr
      character*60 cv(24)
      real rv(24)
      integer iv(24)
      integer nv
      common/rd_chwork/inchstr,rv,iv,cv,nv
      double precision dv(24)
      common/vpc_dsepspace/dv
*     where:
*     cv(1) is the tv calling command from plot setup
*     cv(2) is the filename for the redshifts
*     rv(3) is lower end of tick (no offset)
*     rv(4) is upper end of tick
*     cv(5) is ion for inclusion (if absent, all are)
*     cv(6) may be the ionization level.
*
*     common variables used:
*     input streams
      integer inputc
      common/rdc_inputc/inputc
*     pgplot overplot with bias
      integer ipgov
      real pgbias
      logical plgscl
      common/pgover/ipgov,pgbias,plgscl
*     plot/print velocity scale?
      logical lvel
      real wcenpg,vellopg,velhipg,zp1refpg
      common/vpc_pgvels/wcenpg,vellopg,velhipg,lvel,zp1refpg
*     Central wavelength parameters
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm
      integer nz
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),nz
*
      call sepvar(chsxx,6,rv,iv,cv,nv)
      do j=1,nv
        dv(j)=dble(rv(j))
      end do
      if(cv(2)(1:1).ne.' ') then
*       file containing ion, redshift, ....
        open(unit=29,file=cv(2),status='old',err=987)
        call vp_ationsep(cv,dv,iv,15,5,atom,ion)
*       dv,iv not used here, moved in vp_ationsep
        yltickpg=rv(3)
        if(yltickpg.le.0.0) yltickpg=0.85
        yhtickpg=rv(4)
        if(yhtickpg.le.yltickpg) yhtickpg=yltickpg+0.10
*       cycle through the contents of the file, extracting the
*       redshifts, putting in lines of the atoms in that file
*       if atom is blank, or only those of atom & ion where it
*       is not AND they match the data in the file.
*       [have now finished with cv etc, so can reuse space]
 986    read(29,'(a)',end=987) inchstr
        if(inchstr(1:1).eq.'!') goto 986
        call vp_stripcmt(inchstr,'!')
        call dsepvar(inchstr,12,dv,iv,cv,nv)
        call vp_ationsep(cv,dv,iv,12,1,atomv,ionv)
        if(atom.eq.'  '.or.(atom.eq.atomv.and.ion.eq.ionv)) then
*         put in a tickmark for this ion, if wavelength within range
*         ..  so get redshift, from stripped character
          call vp_dtstrip(cv(2),zedd,chtt)
          zeddp1=zedd+1.0d0
*         and cycle through the lines from this ion from atom.dat
          do j=1,nz
            if(atomv.eq.lbz(j).and.ionv.eq.lzz(j)) then
*             observed wavelength
              dwave=zeddp1*alm(j)
*             convert this to a velocity
              dwave=(dwave/dble(wcenpg))**2
              velspg=real(2.99792458d5*(dwave-1.0d0)/(dwave+1.0d0))
*             is velocity in range?
              if(velspg.ge.vellopg.and.velspg.le.velhipg) then
*               put a tick mark in
                call pgmove(velspg,pgbias+yltickpg)
                call pgdraw(velspg,pgbias+yhtickpg)
              end if
            end if
          end do
        end if
        goto 986
 987    close(unit=29)
      end if
      return
      end
