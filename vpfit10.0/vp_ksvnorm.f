      subroutine vp_ksvnorm(n,x,d,p,sx,ifail)
*     replacement for NAG routine g08cbf,
*     for comparison with normal distribution.
      implicit none
      integer n,ifail
      double precision x(n),d,p,sx(n)
*     local:
      integer j,ncount
      double precision fdata,fnorm,dloc,flast,dtem,dblen
      double precision gerf
*     See NAG documentation for parameters and their meanings
*
      ifail=0
*     copy array to workspace
      do j=1,n
        sx(j)=x(j)
      end do
      call dsort(sx,x,n,1)
*     look for maximum deviation d, stepping through the data
*     and remembering that the maximum deviation will be just before,
*     or just at, that data step.
      d=0.0d0
*     start at real data value zero
      flast=0.0d0
      dblen=dble(n)
      do j=1,n
        fdata=dble(j)/dblen
        fnorm=gerf(sx(j))
        dloc=dmax1(dabs(fdata-fnorm),dabs(flast-fnorm))
        if(dloc.gt.d) then
          d=dloc
        end if
        flast=fdata
      end do
*     compute probability of this deviation now you've got it,
*     using the alternating series
      dloc=dsqrt(dblen)
      dloc=(dloc+0.12d0+0.11d0/dloc)*d
      flast=-2.0d0*dloc*dloc
      fnorm=2.0d0
      p=0.0d0
      dtem=0.0d0
      ncount=1
*     arbitrary start so condition satisfied
      fdata=1.0d0
      do while(ncount.lt.100.and.dabs(fdata).gt.1.0d-8*p.and.
     :         dabs(fdata).gt.1.0d-3*dtem)
        dtem=dabs(fdata)
        fdata=fnorm*dexp(flast*dble(ncount**2))
        p=p+fdata
        fnorm=-fnorm
        ncount=ncount+1
      end do
*     Getout section
      if(ncount.ge.100) then
        p=1.0d0
      end if
      return
      end

      double precision function gerf(x)
*
      double precision x,z,t,erfc
*     cumulative normal distribution
      z=dabs(x/1.41421356237D0)
      t=1.0d0/(1.0d0+0.3275911d0*z)
*     Approximation good to 1.5E-7, should be enough (Abramowitz & Stegun)
      erfc=dexp(-z*z)*t*(0.254829592d0+t*(-0.284496736d0 + t*(
     :       1.421413741d0 + t*(-1.453152027d0 + t*1.061405429d0))))
      if(x.gt.0.0d0) then
        gerf=1.0d0-0.5d0*erfc
       else
        gerf=0.5d0*erfc
      end if
      return
      end

      subroutine dsort(x,y,n,kflag)
*
*     Fullsource for module SSORT from package CMLIB.
*     Retrieved from CAMSUN on Mon Jul 28 09:03:09 1997.
*
*     Written by Rondall E. Jones
*     Modified by John A. Wisniewski to use the Singleton quicksort
*     algorithm.  Date 18 November 1976.
*
*     Abstract
*     DSORT sorts array X and optionally makes the same
*     interchanges in array Y.  The array X may be sorted in
C     increasing order or decreasing order.  A slightly modified
C     quicksort algorithm is used.
*
*     Reference
C     Singleton, R. C., Algorithm 347, An Efficient Algorithm for
C     Sorting with Minimal Storage, CACM,12(3),1969,185-7.
C
C     Description of Parameters
C     X - array of values to be sorted   (usually abscissas)
C     Y - array to be (optionally) carried along
C     N - number of values in array X to be sorted
C     KFLAG - control parameter
C     =2  means sort X in increasing order and carry Y along.
C     =1  means sort X in increasing order (ignoring Y)
C     =-1 means sort X in decreasing order (ignoring Y)
C     =-2 means sort X in decreasing order and carry Y along.
*     
*     Routines called:  xerror
*
*
      implicit none
      integer n
      double precision x(n),y(n)
      integer il(21),iu(21),kflag
*     local variables
      real r
      double precision t,ty,tt,tty
      integer nn,kk,i,j,k,l,m,ij
*
*
      nn = n
      if (nn.lt.1) then
        write(6,*) 'DSORT- NUMBER OF VALUES TO BE SORTED .LE. 0'
        return
      end if
      if (kflag.lt.-2.or.kflag.eq.0.or.kflag.gt.2) then
        write(6,*) 'DSORT- CONTROL PARAMETER NOT 2,1,-1, OR -2'
        return
      end if
*
*     alter array x to get decreasing order if needed
*
      if (kflag.lt.0) then
        do i=1,nn
          x(i) = -x(i)
        end do
      end if
      kk=iabs(kflag)
      go to (100,200),kk
*
*     sort x only
*
 100  continue
      m=1
      i=1
      j=nn
      r=0.375
 110  if (i .eq. j) go to 155
      if (r .gt. 0.5898437) go to 120
      r=r+3.90625e-2
      go to 125
 120  r=r-0.21875
 125  k=i
*                                  Select a central element of the
*                                  array and save it in location t
      ij = i + ifix (real (j-i) * r)
      t=x(ij)
*                                  If first element of array is greater
*                                  than t, interchange with t
      if (x(i) .le. t) go to 130
      x(ij)=x(i)
      x(i)=t
      t=x(ij)
 130  l=j
*                                  If last element of array is less than
*                                  t, interchange with t
      IF (X(J) .GE. T) GO TO 140
      X(IJ)=X(J)
      X(J)=T
      T=X(IJ)
C                                  IF FIRST ELEMENT OF ARRAY IS GREATER
C                                  THAN T, INTERCHANGE WITH T
      IF (X(I) .LE. T) GO TO 140
      X(IJ)=X(I)
      X(I)=T
      T=X(IJ)
      GO TO 140
 135  TT=X(L)
      X(L)=X(K)
      X(K)=TT
C                                  FIND AN ELEMENT IN THE SECOND HALF OF
C                                  THE ARRAY WHICH IS SMALLER THAN T
 140  L=L-1
      IF (X(L) .GT. T) GO TO 140
C                                  FIND AN ELEMENT IN THE FIRST HALF OF
C                                  THE ARRAY WHICH IS GREATER THAN T
 145  K=K+1
      IF (X(K) .LT. T) GO TO 145
C                                  INTERCHANGE THESE ELEMENTS
      IF (K .LE. L) GO TO 135
C                                  SAVE UPPER AND LOWER SUBSCRIPTS OF
C                                  THE ARRAY YET TO BE SORTED
      IF (L-I .LE. J-K) GO TO 150
      IL(M)=I
      IU(M)=L
      I=K
      M=M+1
      GO TO 160
 150  IL(M)=K
      IU(M)=J
      J=L
      M=M+1
      GO TO 160
C                                  BEGIN AGAIN ON ANOTHER PORTION OF
C                                  THE UNSORTED ARRAY
 155  M=M-1
      IF (M .EQ. 0) GO TO 300
      I=IL(M)
      J=IU(M)
 160  IF (J-I .GE. 1) GO TO 125
      IF (I .EQ. 1) GO TO 110
      I=I-1
 165  I=I+1
      IF (I .EQ. J) GO TO 155
      T=X(I+1)
      IF (X(I) .LE. T) GO TO 165
      K=I
 170  X(K+1)=X(K)
      K=K-1
      IF (T .LT. X(K)) GO TO 170
      X(K+1)=T
      GO TO 165
C
C     SORT X AND CARRY Y ALONG
C
 200  CONTINUE
      M=1
      I=1
      J=NN
      R=.375
 210  IF (I .EQ. J) GO TO 255
      IF (R .GT. .5898437) GO TO 220
      R=R+3.90625E-2
      GO TO 225
 220  R=R-.21875
 225  K=I
C                                  SELECT A CENTRAL ELEMENT OF THE
C                                  ARRAY AND SAVE IT IN LOCATION T
      IJ = I + IFIX (FLOAT (J-I) *R)
      T=X(IJ)
      TY= Y(IJ)
C                                  IF FIRST ELEMENT OF ARRAY IS GREATER
C                                  THAN T, INTERCHANGE WITH T
      IF (X(I) .LE. T) GO TO 230
      X(IJ)=X(I)
      X(I)=T
      T=X(IJ)
      Y(IJ)= Y(I)
      Y(I)=TY
      TY= Y(IJ)
 230  L=J
C                                  IF LAST ELEMENT OF ARRAY IS LESS THAN
C                                  T, INTERCHANGE WITH T
      IF (X(J) .GE. T) GO TO 240
      X(IJ)=X(J)
      X(J)=T
      T=X(IJ)
      Y(IJ)= Y(J)
      Y(J)=TY
      TY= Y(IJ)
C                                  IF FIRST ELEMENT OF ARRAY IS GREATER
C                                  THAN T, INTERCHANGE WITH T
      IF (X(I) .LE. T) GO TO 240
      X(IJ)=X(I)
      X(I)=T
      T=X(IJ)
      Y(IJ)= Y(I)
      Y(I)=TY
      TY= Y(IJ)
      GO TO 240
 235  TT=X(L)
      X(L)=X(K)
      X(K)=TT
      TTY= Y(L)
      Y(L)= Y(K)
      Y(K)=TTY
C                                  FIND AN ELEMENT IN THE SECOND HALF OF
C                                  THE ARRAY WHICH IS SMALLER THAN T
 240  L=L-1
      IF (X(L) .GT. T) GO TO 240
C                                  FIND AN ELEMENT IN THE FIRST HALF OF
C                                  THE ARRAY WHICH IS GREATER THAN T
 245  K=K+1
      IF (X(K) .LT. T) GO TO 245
C                                  INTERCHANGE THESE ELEMENTS
      IF (K .LE. L) GO TO 235
C                                  SAVE UPPER AND LOWER SUBSCRIPTS OF
C                                  THE ARRAY YET TO BE SORTED
      IF (L-I .LE. J-K) GO TO 250
      IL(M)=I
      IU(M)=L
      I=K
      M=M+1
      GO TO 260
 250  IL(M)=K
      IU(M)=J
      J=L
      M=M+1
      GO TO 260
C                                  BEGIN AGAIN ON ANOTHER PORTION OF
C                                  THE UNSORTED ARRAY
 255  M=M-1
      IF (M .EQ. 0) GO TO 300
      I=IL(M)
      J=IU(M)
 260  IF (J-I .GE. 1) GO TO 225
      IF (I .EQ. 1) GO TO 210
      I=I-1
 265  I=I+1
      IF (I .EQ. J) GO TO 255
      T=X(I+1)
      TY= Y(I+1)
      IF (X(I) .LE. T) GO TO 265
      K=I
 270  X(K+1)=X(K)
      Y(K+1)= Y(K)
      K=K-1
      IF (T .LT. X(K)) GO TO 270
      X(K+1)=T
      Y(K+1)=TY
      GO TO 265
C
C CLEAN UP
C
 300  IF (KFLAG.GE.1) RETURN
      do I=1,NN
        X(I) = -X(I)
      end do
      RETURN
      END
