      subroutine vp_cfcopy(ichunk,idest)
*     copy wavelength coeffts from 1d-2d or vv
*     idest is the dimensionality of the destination
*     ichunk is the chunk number
      implicit none
      include 'vp_sizes.f'
      integer ichunk,idest
      integer i
*     general wavelength coefficients
      integer nwco
      double precision wcf1(maxwco)
      common/vpc_wavl/wcf1,nwco
*
      character*8 wcftype
      character*4 vacind
      double precision helcfac
      integer*4 noffs
      common/c8_wcftype/wcftype,helcfac,noffs,vacind
*     wavelength parameter sets
      character*8 wcfty2(maxnch)
      character*4 vacin2(maxnch)
      double precision helcf2(maxnch)
      integer nwcf2(maxnch),noffs2(maxnch)
      common/vpc_wcpars2/wcfty2,helcf2,nwcf2,noffs2,vacin2
*     general wavelength coefficients
      double precision wcfd(maxwco,maxnch)
      common/vpc_wcfd/wcfd
*
      if(idest.eq.2) then
*       1 -> 2
        do i=1,maxwco
          wcfd(i,ichunk)=wcf1(i)
        end do
        nwcf2(ichunk)=nwco
        wcfty2(ichunk)=wcftype
        vacin2(ichunk)=vacind
        helcf2(ichunk)=helcfac
        noffs2(ichunk)=noffs
       else
*       2 -> 1
        do i=1,maxwco
          wcf1(i)=wcfd(i,ichunk)
        end do
        nwco=nwcf2(ichunk)
        wcftype=wcfty2(ichunk)
        vacind=vacin2(ichunk)
        helcfac=helcf2(ichunk)
        noffs=noffs2(ichunk)
      end if
      return
      end
      subroutine vp_chgs1(wv,ced)
*     guess channel number ced based on wavelength wv
*     for 1-d wavelength set
      include 'vp_sizes.f'
      double precision wv,ced
*     general wavelength coefficients
      integer nwco
      double precision wcf1(maxwco)
      common/vpc_wavl/wcf1,nwco
      character*8 wcftype
      character*4 vacind
      double precision helcfac
      integer*4 noffs
      common/c8_wcftype/wcftype,helcfac,noffs,vacind
*
      if(wcftype(1:3).eq.'log') then
        ced=(dlog10(wv)-wcf1(1))/wcf1(2)+dfloat(noffs)
       else
        if(wcftype(1:3).eq.'cheb') then
          ced=500.0d0+dfloat(noffs)
         else
          ced=(wv-wcf1(1))/wcf1(2)+dfloat(noffs)
        end if
      end if
      return
      end
