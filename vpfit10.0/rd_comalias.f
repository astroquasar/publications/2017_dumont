      subroutine rd_comalias(chstr)
*
*     convert long form command names (which might be memorable)
*     to short form (which might not)
*     IN:    chstr string
*     OUT:   chstr string: 2 char equivalent
      implicit none
      character*(*) chstr
*
*     local
      integer nl
*
*     functions
      integer lastchpos
*
      nl=lastchpos(chstr)
*     do nothing if a two-character command
      if(nl.le.2) return
*     Alphabetical list (by long name)
*
*     COPY
      if(chstr(1:3).eq.'cop'.or.chstr(1:3).eq.'COP') then
        chstr(3:nl)=' '
        chstr(1:2)='cy'
        return
      end if
*     FITS_QUERY
      if(chstr(1:3).eq.'fit'.or.chstr(1:3).eq.'FIT') then
        chstr(3:nl)=' '
        chstr(1:2)='fq'
        return
      end if
*     GENERATE_PROFILES = Voigt_profiles
      if(chstr(1:3).eq.'gen'.or.chstr(1:3).eq.'GEN'.or.
     :   chstr(1:3).eq.'voi'.or.chstr(1:3).eq.'VOI'.or.
     :   chstr(1:3).eq.'Voi') then
        chstr(3:nl)=' '
        chstr(1:2)='gp'
        return
      end if
*     PLOT_CURSOR (CURSOR_PLOT)
      if(nl.ge.6) then
        if(chstr(1:6).eq.'plot_c'.or.chstr(1:6).eq.'PLOT_C'.or.
     :     chstr(1:3).eq.'cur'.or.chstr(1:3).eq.'CUR') then
          chstr(3:nl)=' '
          chstr(1:2)='pg'
          return
        end if
       else
        if(chstr(1:3).eq.'cur'.or.chstr(1:3).eq.'CUR') then
          chstr(3:nl)=' '
          chstr(1:2)='pg'
          return
        end if          
      end if
*     READ
      if(chstr(1:3).eq.'rea'.or.chstr(1:3).eq.'REA') then
        chstr(3:nl)=' '
        chstr(1:2)='rd'
        return
      end if
*     if reaches here, no match
      return
      end

