      subroutine vp_ationsep(cv,dv,iv,len,np,atom,ion)
*
*     take cv(np) and, if necessary, cv(np+1) and return element and
*     ionization level, moving subsequent values (and dv, iv) down 
*     one position in the array if necessary. Note uses DOUBLE PRECISION
*     not real.
*
*     IN:
*     len    int    data array length/no of values
*     np     int    position of atom identifier
*     IN & OUT:
*     cv     ch array   array containing atom/ion etc
*     rv     dbl array  corresponding float values
*     iv     int array  corres. integer values
*     OUT:
*     atom   ch*2       element
*     ion    ch*4       ionization level
*
      implicit none
*
      integer len,np
      character*(*) cv(len)
      double precision dv(len)
      integer iv(len)
      character*2 atom
      character*4 ion
*
*     Local
      integer j,ln
*
*     Functions
      logical ucase
      integer lastchpos
*
      ln=lastchpos(cv(np))
      if(cv(np)(1:2).eq.'<>'.or.cv(np)(1:2).eq.'__'.or.
     :   cv(np)(1:2).eq.'>>'.or.cv(np)(1:2).eq.'??')then
*       special characters
        atom=cv(np)(1:2)
        ion='    '
*       and the rest are in position
       else
        if(ucase(cv(np)(2:2)).and.cv(np)(1:2).ne.'HD'.and.
     :     cv(np)(1:2).ne.'CO') then
          atom=cv(np)(1:1)//' '
          ion=cv(np)(2:ln)
         else
*         either 2 character atom or separated element/ion
*         or the exceptional ones - currently HD and CO
          atom=cv(np)(1:2)
          if(ln.gt.2) then
            ion=cv(np)(3:ln)
           else
            if(np.lt.len) then
              ion=cv(np+1)(1:4)
              cv(np)=atom//ion
              if(np+1.lt.len) then
*               move the rest of the array down
                do j=np+2,len
                  cv(j-1)=cv(j)
*                 rfc 02.06.05: move everything down
                  dv(j-1)=dv(j)
                  iv(j-1)=iv(j)
                end do
              end if
*             blank off the last one, since first field > 2 char
              cv(len)=' '
              dv(len)=0.0d0
              iv(len)=0
            end if
          end if
        end if
      end if
      return
      end
      subroutine vp_dtstrip(chstr,dtstrip,chtt)
*     strip of up to two characters from the end of a string (chstr)
*     returning the remainder as a double precision variable (dtstrip)
*     and the characters at the end as the first one or two
*     characters in chtt
*
      implicit none
*
      character*(*) chstr,chtt
      double precision dtstrip
*     local variables
      integer jm,len,nvv
      character*64 chstem
      character*1 ichar
      double precision dvv(1)
      integer ivv(1)
      character*32 cvv(1)
*
*     Functions
      integer lastchpos
*
      chtt='  '
      len=lastchpos(chstr)
      if(len.gt.0) then
        ichar=chstr(len:len)
*       0 is normally end of character number sequence, but this won't hurt..
        if(ichar.eq.'.'.or.ichar.eq.'0'.or.
     :         (ichar.ge.'1'.and.ichar.le.'9')) then
*         is a number, so use it
          call dsepvar(chstr,1,dvv,ivv,cvv,nvv)
          dtstrip=dvv(1)
         else
*         last character is not a number
          chtt=ichar
          if(len.gt.1) then
            jm=len-1
            chstem=chstr(1:jm)
            ichar=chstr(jm:jm)
            if(len.gt.2) then
*             check second last character
              if(ichar.eq.'.'.or.ichar.eq.'0'.or.
     :           (ichar.ge.'1'.and.ichar.le.'9')) then
*               is a number, so use it
                call dsepvar(chstem,1,dvv,ivv,cvv,nvv)
                dtstrip=dvv(1)
               else
                chtt=chstr(jm:len)
                chstem=chstr(1:jm-1)
                call dsepvar(chstem,1,dvv,ivv,cvv,nvv)
                dtstrip=dvv(1)
              end if
             else
*             remaining string one character, so use it
              call dsepvar(chstem,1,dvv,ivv,cvv,nvv)
              dtstrip=dvv(1)
            end if
           else
*           string only one character long, so have to use it
            call dsepvar(chstr,1,dvv,ivv,cvv,nvv)
            dtstrip=dvv(1)
          end if
        end if
       else
        dtstrip=0.0d0
      end if
      return
      end
