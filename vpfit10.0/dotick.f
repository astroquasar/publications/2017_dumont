      subroutine dotick(xmpg,xhpg,ympg,yhpg,donum)
*
      implicit none
      include 'vp_sizes.f'
*
      integer i,j,ios
      real xmpg,xhpg,ympg,yhpg
      real xpg,ypg,xdpg,ylopg,yupg
      logical donum
*
      character*3 ic
      character*2 chbs
      double precision fl,fin
      real deltxpg
      real xxpg(2),yypg(2)
      double precision wval
*
      integer nst,ncn,lc1,lc2
      real fampg
      common/ppam/nst,ncn,lc1,lc2,fampg
*     ajc 9-may-92  unsure of dimensions here - best guess
      integer ntick,nred
      double precision tikmk,rstwav
      common/vpc_ticks/tikmk(maxnio,maxnio+1),ntick(maxnio+1),
     :               nred,rstwav(maxnio,maxnio+1)
*     ipgtgrk=1 if Greek letters, else 0; yuptkf(def=0.9) is fract y-axis
*     for top of tick, ylentkf (def=0.07) fract y for length of tick
      integer ipgtgrk
      real yuptkpg,ylentkpg
      common/vpc_tick/ipgtgrk,yuptkpg,ylentkpg
*     velocity variables

*     plots tickmarks on pgplot device

*     ajc 5-aug-91  include tick marks
*     if no tick marks, exit
      if(nred.eq.-1000) then
*       tick marks from a file
        call tickpg(xmpg,xhpg,ympg,yhpg,donum)
        nred=0
      end if
*     backslash is an escape on some compilers, not on others.
*     to get around this:
      chbs='\\'
*     ajc 5-aug-91  do not return on non-linear wavelength scale
*     if(ac(3).ne.0.0d0.or.ntick(1).le.0.or.nred.lt.1) return
      if ( ntick( 1 ) .le. 0 .or. nred .lt. 1 ) return
      fl=wval(dfloat(nst))
      fin=wval(dfloat(ncn))
      deltxpg=(xhpg-xmpg)*0.0125
*     tick ranges
      if(ipgtgrk.eq.-1 .or. .not. donum) then
*     no symbols above tick marks, so move them up
        yupg=0.96*(yhpg-ympg)+ympg
       else
        yupg=yuptkpg*(yhpg-ympg)+ympg
      end if
      ylopg=yupg-ylentkpg*(yhpg-ympg)

      do j=1,nred
        do i=1,ntick(j)
          if(tikmk(i,j).le.fl.or.tikmk(i,j).ge.fin) goto 10
          xxpg(1)=real(tikmk(i,j))
          xxpg(2)=xxpg(1)
          yypg(1)=yupg
          yypg(2)=ylopg
          call pgline(2,xxpg,yypg)
c         identify line by number in list, or, if ipgtgrk=1, greek letter
          xpg=xxpg(1)
          if(ipgtgrk.ne.-1) then
c	    numbers over tick marks
            if(ipgtgrk.ne.1.or.i.gt.5) then
              ic='   '
              if(i.lt.10) then
                write(ic(1:1),'(i1)',iostat=ios) i
                xdpg=xpg-deltxpg
               else
                write(ic(1:2),'(i2)',iostat=ios) i
                xdpg=xpg-2.0*deltxpg
              end if
              if(ios.ne.0) ic='  '
             else
              if(i.eq.1) ic=chbs(1:1)//'ga'
              if(i.eq.2) ic=chbs(1:1)//'gb'
              if(i.eq.3) ic=chbs(1:1)//'gg'
              if(i.eq.4) ic=chbs(1:1)//'gd'
              if(i.eq.5) ic=chbs(1:1)//'ge'
              if(i.eq.6) ic=chbs(1:1)//'gz'
              if(i.eq.7) ic=chbs(1:1)//'gy'
              if(i.eq.8) ic=chbs(1:1)//'gh'
              if(i.eq.9) ic=chbs(1:1)//'gi'
              if(i.eq.10) ic=chbs(1:1)//'gk'
              if(i.eq.11) ic=chbs(1:1)//'gl'
              if(i.eq.12) ic=chbs(1:1)//'gm'
              if(i.eq.13) ic=chbs(1:1)//'gn'
              if(i.eq.14) ic=chbs(1:1)//'gc'
              if(i.eq.15) ic=chbs(1:1)//'go'
              if(i.eq.16) ic=chbs(1:1)//'gp'
              xdpg=xpg-deltxpg
            end if
            ypg=yupg+0.01*(yhpg-ympg)
            if ( donum ) call pgtext(xdpg,ypg,ic)
            ic='   '
            if(j.lt.10) then
              write(ic(1:1),'(i1)',iostat=ios) j
              xpg=xpg-deltxpg
             else
              write(ic(1:2),'(i2)',iostat=ios) j
              xpg=xpg-2.0*deltxpg
            end if
            if(ios.ne.0) ic='  '
            ypg=yupg+0.5*(1.0-yuptkpg)*(yhpg-ympg)
            if (donum) call pgtext(xpg,ypg,ic)
          end if
 10       continue
        end do
      end do
      return
      end
