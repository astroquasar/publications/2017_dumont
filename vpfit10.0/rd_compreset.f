      subroutine rd_compreset
*     preset variables which are in common
*
      implicit none
      include 'vp_sizes.f'
*
*     Local:
      integer j
*     Common:
*     resolution previous values:
      double precision sigmah,dlamh
      common/rdc_poldres/sigmah,dlamh
*     file variables for O/P for subsequent vpfit run:
      logical lchg13
      character*80 chg13file
      common/rdc_chg13/chg13file,lchg13
*     flux and wavelength unit indicator variables
      integer mftyp,mxtyp
      double precision yscale
      common/dunit/mftyp,mxtyp,yscale
*     wavelength fit RMS
      double precision ssqrd,ssqrh
      common/wavrms/ssqrd,ssqrh
*     monitoring variable
      integer ncallset
      common/rdc_ncallset/ncallset
*     atomic data
      integer m
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),m
*     general wavelength coefficients
      integer nwco
      double precision wcf1(maxwco)
      common/vpc_wavl/wcf1,nwco
      character*8 wcftype
      character*4 vacind
      double precision helcfac
      integer*4 noffs
      common/c8_wcftype/wcftype,helcfac,noffs,vacind
*     last line
      character*2 prevatom
      character*4 previon
      double precision prevrestwave
      common/vpc_previons/prevrestwave,previon,prevatom
*     upper limit defaults, updated by routines which use them
*     common block order: dble, int, char. logical
      character*4 ctypeul
      character*60 chwv1
      logical lcminul
      integer nminfd,nminfdef
      double precision xnul,xbul,pchslim,dfwhmkms,bvalmins
      double precision wloddr,whiddr
      common/rdc_ulvsets/xnul,xbul,pchslim,dfwhmkms,bvalmins,
     :      wloddr,whiddr,nminfd,nminfdef,ctypeul,chwv1,lcminul
      xnul=1.0d0
      xbul=2.0d0
      nminfd=5
      pchslim=0.16d0
      ctypeul='cmin'
      lcminul=.true.
      dfwhmkms=6.7d0
      nminfdef=3
      bvalmins=0.05d0
      wloddr=0.0d0
      whiddr=1.0d25
      chwv1=' '
*     
      prevatom='  '
      previon='    '
      prevrestwave=0.0d0
*
*     atomic data not yet read
      m=0
*     file variables for O/P for subsequent vpfit run:
      chg13file=' '
      lchg13=.false.
*     flux and wavelength unit indicator variables
      mftyp=1
      mxtyp=1
      yscale=1.0
*     wavelength fit RMS
      ssqrd=0.0
      ssqrh=0.0
      sigmah=0.0d0
      dlamh=0.0d0
*     rd_gprof monitor
      ncallset=100
*     wavelength coefficients not set
      wcf1(1)=-1.0d0
      wcf1(2)=1.0d0
      if(maxwco.gt.2) then
        do j=3,maxwco
          wcf1(j)=0.0d0
        end do
      end if
      nwco=2
      wcftype='undefine'
      vacind='vacu'
      helcfac=1.0d0
      noffs=0
      return
      end
