      subroutine vp_emtset(ch)
*     setup for reading a template containing an emission line
*     this involves having the template filename, and changing the
*     effective line list to include a special value.
*     
      implicit none
      include 'vp_sizes.f'
      character*2 ch
*
*     Local
      integer ii,ichunk,ki
      integer ncv,ng,norx,numdat
      double precision werr
*
*     Functions
      double precision wval
*     atom.dat line list variables:
      integer nz
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),nz
*
*     template variables
      character*60 filtempl
*
*     position pointers for template array
      integer lchemp,lentempl
      common/vpc_empval/lchemp,lentempl
*     data arrays
*     current 1-D spectrum data, err, fluctuations, continuum -length ngp
      integer ngp
      double precision dhdata(maxfis),dherr(maxfis)
      double precision dhrms(maxfis),dhcont(maxfis)
      common/vpc_su1d/dhdata,dherr,dhrms,dhcont,ngp
*     emission profile array
      integer ngpf
      double precision wvempf(maxfis),daempf(maxfis)
      common/vpc_emprof/wvempf,daempf,ngpf
      integer ngd,npexsm
      integer numpts(maxnch)
      double precision wcoeff(maxnch)
      common/vpc_jkw3/wcoeff,ngd,npexsm,numpts
*     local wavelength coefficients
      integer nwco
      double precision wcf1(maxwco)
      common/vpc_wavl/wcf1,nwco
*     general wavelength coefficients
      double precision wcfd(maxwco,maxnch)
      common/vpc_wcfd/wcfd
*     wavelength parameter sets
      character*8 wcfty2(maxnch)
      character*4 vacin2(maxnch)
      double precision helcf2(maxnch)
      integer nwcf2(maxnch),noffs2(maxnch)
      common/vpc_wcpars2/wcfty2,helcf2,nwcf2,noffs2,vacin2
*     workspace
      integer idwk(74)
      common/works/idwk
      character*8 wcftype
      character*4 vacind
      double precision helcfac
      integer*4 noffs
      common/c8_wcftype/wcftype,helcfac,noffs,vacind
*
*     character handling
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
      double precision dvstr(24)
      common/vpc_dsepspace/dvstr
*
*     starter for search when interpolating
      lchemp=1
*     readin the atomic data if it has not happened already
      if(nz.le.0) call vp_ewred(0)
*     Add in a false line
      if(ch.ne.'__') then
        write(6,
     :    '('' Filename for line template? [blank = unity]'')')
        write(6,'(''> '',$)')
        read (5,'(a)') filtempl
       else
        filtempl(1:2)='  '
      end if
      if(filtempl(1:2).eq.'  ') then
*       set unity default
        ngd=2
        nwco=0
        wcftype='linear'
        vacind='vacu'
        noffs=0
        helcfac=1.0d0
        wcf1(1)=10.0d0-1.0d5
        wcf1(2)=1.0d5
        do ii=3,maxwco
          wcf1(ii)=0.0d0
        end do
        dhdata(1)=1.0d0
        dhdata(2)=1.0d0
       else
*       read the data into the top of the data array for later use
*	want only the data, so:
        numdat=1
*       presets for vp_readfs
        norx=0
        ng=maxfis
        cvstr(1)=' '
        cvstr(2)=' '
        ncv=2
*       ichunk needed only because using standard data readin
        ichunk=1
        call vp_readfs(dhdata,dherr,dhrms,dhcont,ng,ngd,werr,
     1        norx,filtempl,numdat,ichunk)
      end if
*
      lentempl=ngd
      if(ngd.le.0) then
        write(6,*) ' ** Zero length data, line omitted!!'
        goto 99
      end if
c     copy to data space
      do ki=1,ngpf
        daempf(ki)=dhdata(ki)
        wvempf(ki)=wval(dble(ki))
      end do
*
*     extend the line list array
      if(nz.lt.maxats) then
        if(ch.eq.'__') goto 99
        nz=nz+1
        if(filtempl(1:2).eq.'  ') then
*         unit level added
          alm(nz)=1215.67d0
         else
          write(6,*) ' Rest wavelength for template [1215.67]?'
          write(6,'(''> '',$)')
          read (5,'(a)') inchstr
          call dsepvar(inchstr,1,dvstr,ivstr,cvstr,nvstr)
          if(nvstr.le.0.or.cvstr(1)(1:1).eq.' ') then
            alm(nz)=1215.67d0
           else
            alm(nz)=dvstr(1)
          end if
        end if
        fik(nz)=1.0d0
        asm(nz)=1.00d8
        lbz(nz)='AD'
        lzz(nz)='    '
       else
        write(6,*) 'Not enough space to insert AD line'
        write(6,*) ' .. remove a line from atom.dat,'
        write(6,*) '    or increase MAXATS and recompile'
        goto 99
      end if
*
 99   return
      end

