      subroutine vp_lycont(cold,flx,nw,zdp1,ichunk)
*     
*     IN:
*     cold: HI column density
*     zdp1: 1+redshift
*     flx(nw): data array(length)
*     ichunk: chunk number
*     OUT:
*     flx(nw) with a Lyman limit
*
      implicit none
      include 'vp_sizes.f'
      double precision cold,zdp1
      integer nw,ichunk
      double precision flx(nw)
*
      integer i,na,nb,nlast,nlp,nup
      double precision fxx,fxy,temp1,temp2,tzero,x,wval
      double precision wvd,dxd,ced,dfd,dem
*     
      double precision dnshft2(maxnch)
      common/vpc_shft2/dnshft2
*     Minimum wavelength Lyman line (for interpolation to Lyman cont.)
      double precision wlsmin,vblstar,collsmin
      common/vpc_lycont/wlsmin,vblstar,collsmin
*     control for verbose output:
      logical verbose
      common/vp_sysout/verbose
*     Following not needed here except for diagnostic reasons:
*     general wavelength coefficients
      integer nwco
      double precision wcf1(maxwco)
      common/vpc_wavl/wcf1,nwco
      character*8 wcftype
      character*4 vacind
      double precision helcfac
      integer*4 noffs
      common/c8_wcftype/wcftype,helcfac,noffs,vacind
*     wavelength parameter sets
      character*8 wcfty2(maxnch)
      character*4 vacin2(maxnch)
      double precision helcf2(maxnch)
      integer nwcf2(maxnch),noffs2(maxnch)
      common/vpc_wcpars2/wcfty2,helcf2,nwcf2,noffs2,vacin2
*     general wavelength coefficients
      double precision wcfd(maxwco,maxnch)
      common/vpc_wcfd/wcfd
*
c
c     subroutine to take care of Lyman limit absorption
c
      if(verbose) write(6,*) ' Lyman limit absorption included'
*     copy coeffts to single array
      call vp_cfcopy(ichunk,1)
*     wavelengths in observers frame
      wvd=911.7536d0*zdp1
*     guess channel number ced based on wavelength wvd
      call vp_chgs1(wvd,ced)
      call chanwav(wvd,ced,5.0d-2,20)
      ced=ced-dnshft2(ichunk)
*     Lyman limit channel (which may be negative!)
      nup=int(ced)
*     You may wish to include:	if(nup.le.0) return
*     If Lyman limit higher than channel maximum, set to max channel:
      nup=min0(nup,nw)
      tzero=cold*6.30d-18
      if(nup.gt.0) then
*       set continuum absorption if range includes it:
        do i=1,nup
          x=wval(dfloat(i)+dnshft2(ichunk))/(zdp1*911.7536d0)
          x=x*x*x
          flx(i)=flx(i)*exp(-tzero*x)
        end do
      end if
      if(nup.lt.nw) then
*       set up interpolation limits
*       find first channel at local minimum
*
*       step 1: find channel corresponding to lowest wavelength hydrogen line
        wvd=wlsmin*zdp1
*       guess channel number ced based on wavelength wvd
        call vp_chgs1(wvd,ced)
        call chanwav(wvd,ced,5.0d-2,20)
        ced=ced-dnshft2(ichunk)
*       step 2: search up for local flux minimum in Lyman series spectrum
*       start at int(ced)
        nlast=int(ced)
        if(nlast.gt.nw) then
*         lowest line outside region, so just extrapolate the cubic
*         since there is not much else you can do..
          nlast=nw
          x=wval(dfloat(nlast)+dnshft2(ichunk))/(zdp1*911.7536d0)
          x=x*x*x
          flx(nlast)=exp(-tzero*x)
        end if
 1      nlp=nlast+1
        if(nlp.gt.nw) then
*         write(6,*) 'WARNING: High order Lyman series minimum flux'
*         write(6,*) '         is outside wavelength range for spectral'
*         write(6,*) '         region -- try expanding the region'
          nlp=nw
          goto 2
        end if
        if(flx(nlp).gt.flx(nlast)) goto 2
        nlast=nlp
        goto 1
 2      continue
c       linearly interpolate in wavelength from nup to nlast
        na=nup+1
        nb=nlast-1
        if(nb.lt.na) return
*       interpolate linearly:
        if(nup.gt.0) then
          fxy=flx(nup)
         else
          x=wval(dble(nup)+dnshft2(ichunk))/(zdp1*911.7536d0)
          x=x*x*x
          fxy=exp(-tzero*x)
        end if
        fxx=dble(flx(nlast))
*       interpolation uses wavelength ratios, so no need for zdp1 correction
        dfd=wval(dble(nup)+dnshft2(ichunk))
        dxd=wval(dble(nlast)+dnshft2(ichunk))
        dem=dxd-dfd
        if(na.lt.1) then
          na=1
        end if    
        do i=na,nb
          wvd=wval(dble(i)+dnshft2(ichunk))
          temp1=(wvd-dfd)/dem
          temp2=(dxd-wvd)/dem
          flx(i)=fxx*temp1+fxy*temp2
        end do
      end if
      return
      end
