      subroutine vp_asciin(da,de,drms,ca,nct,wch,nwco,ichunk,norx)
*
      implicit none
      include 'vp_sizes.f'
      integer nct,ichunk,norx,nwco
      double precision da(*),de(*),drms(*),ca(*)
      double precision wch(*)
*     local
      character*60 chname
      integer i,j,jwv,jda,jer,jco,jrms
      integer llch,maxnvstr,istatus
      double precision contdeflt,sigdeflt2
*     Functions:
      integer lastchpos
*     
*     readin workspace
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
      double precision dvstr(24)
      common/vpc_dsepspace/dvstr
*     wavelength correction variables
      character*8 wcftype
      character*4 vacind
      double precision helcfac
      integer noffs
      common/c8_wcftype/wcftype,helcfac,noffs,vacind
*     fit to resolution, using sigmas
      integer nswres(maxnch)
      double precision swres(maxpoc,maxnch)
      common/vpc_sigwres/swres,nswres
*
*     gets here only if data file exists, and is not a .imh or .fits
      nct=0
*     default columns
      jwv=1
      jda=2
      jer=3
      jco=4
      jrms=5
*     defaults
      sigdeflt2=0.0025d0
      contdeflt=1.0d0
      vacind='vacu'
      helcfac=1.0d0
      noffs=0
      wcftype='array'
      maxnvstr=0
*
 911  read(17,'(a)',end=991) inchstr
*     skip comment lines
      if(inchstr(1:1).eq.'!'.or.inchstr(1:1).eq.'#') goto 911
*     wavelengths as double precision
      call dsepvar(inchstr,5,dvstr,ivstr,cvstr,nvstr)
      maxnvstr=max0(maxnvstr,nvstr)
      if(nvstr.ge.2.and.cvstr(1)(1:1).ne.' '.and.
     :               dvstr(1).ne.0.0d0) then
        nct=nct+1
        if(nct.eq.1) then
          if(ivstr(1).eq.1.and.cvstr(1)(2:2).ne.'.') then
*           some systems number the lines
            jwv=2
            jda=3
            jer=4
            jco=5
            jrms=1000
*           MIDAS tables have 5th column equal to the resolution, so
            write(6,*) '>>> Assumes continuum=',contdeflt,
     :                   ' everywhere <<<'
            jco=1000
           else
            jwv=1
            jda=2
            jer=3
            jco=4
            jrms=5
          end if
        end if
        if(nct.gt.maxwco.or.nct.gt.maxfis) goto 992
*       wavelengths by table, read in directly:
        wch(nct)=dvstr(jwv)
*       data, error, continuum, drms
        da(nct)=dvstr(jda)
        if(nvstr.ge.jer) then
*         de is variance
          if(dvstr(jer).gt.0.0d0) then
            de(nct)=dvstr(jer)**2
           else
            de(nct)=dvstr(jer)
          end if
         else
          de(nct)=sigdeflt2
        end if
        if(nvstr.ge.jco) then
          ca(nct)=dvstr(jco)
         else
          ca(nct)=contdeflt
        end if
        if(nvstr.ge.jrms) then
          if(dvstr(jrms).gt.0.0d0) then
            drms(nct)=dvstr(jrms)**2
           else
            drms(nct)=dvstr(jrms)
          end if
         else
*         set drms=de, since don't know any better
          drms(nct)=de(nct)
        end if
       else
*       see if it is a key set variable
*       CONT (continuum level)
        if(cvstr(1)(1:4).eq.'cont'.or.cvstr(1)(1:4).eq.'CONT') then
          contdeflt=dvstr(2)
        end if
*       RESVEL
        if(cvstr(1)(1:4).eq.'resv'.or.cvstr(1)(1:4).eq.'RESV') then
          write(6,*) 'Resolution',dvstr(2),' km/s'
          swres(1,ichunk)=0.0d0
*         velocity reolution /(2.35482d0*c) in sigma units
          swres(2,ichunk)=dvstr(2)/7.05957d5
          nswres(ichunk)=2
          do i=3,maxpoc
            swres(i,ichunk)=0.0d0
          end do
        end if
*       RESFILE
        if(cvstr(1)(1:4).eq.'resf'.or.cvstr(1)(1:4).eq.'RESF') then
          write(6,*) 'Resolution file ',cvstr(2)(1:20)
*         check that the name is not too long!
          llch=lastchpos(cvstr(2))
          if(llch.lt.60) then
            istatus=0
            chname=cvstr(2)
            call vp_getres(chname,ichunk,norx,istatus)
           else
            write(6,*) ' .. must be < 60 characters!'
          end if
        end if
      end if
      goto 911
 991  nwco=nct
      if(maxnvstr.le.2) then
*       request error file
        close(unit=17)
        write(6,*) 'Error file?'
        read(5,'(a)') inchstr
        open(unit=17,file=inchstr,status='old',err=9000)
        do j=1,nct
          read(17,'(a)',end=9000) inchstr
          call dsepvar(inchstr,5,dvstr,ivstr,cvstr,nvstr)
          de(j)=dvstr(2)**2
        end do
      end if
 9000 return
 992  write(6,*) 'Max file size exceeded'
      nct=nct-1
      write(6,*) ' Highest wavelength read is ',wch(nct)
      goto 991
      end
