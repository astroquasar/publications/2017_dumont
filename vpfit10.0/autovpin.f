*	autovpin
*	take a fort.9 linelist file from rd_gen, and turn it into
*	a 26-format fort.13 file for vpfit.
*	There is no helpfile for this, but it asks the obvious
*	questions, and hitting return should give reasonably sensible
*	defaults for the parameters. Try running it, and then look at 
*	the output file to see if it is what you expected.
	implicit none
	include 'vp_sizes.f'
	logical linord
	character*132 inchstr, chold
	double precision dv(24)
	integer iv(24)
	character*24 cv(24)
	double precision wrlo(5000),wrhi(5000),sigr(5000)
	double precision pbr(5000),aperch(5000)
	double precision wlout(5000),whout(5000)
*       Lyman series limits
	double precision wslow(50),wshigh(50)
*
	integer il,kl,lenin,nha,nhb,nla,nlb,nreg,nv,nlzon
	integer lench,lhatmin,lhatmax,maxext,minext
	integer ireg,jreg,j,k,kout,nrglist,nsep,nout
	double precision pbtymx,sepmax,sigmx,xtemp
	double precision sigthres,pthres,sepmin,wqn,wqx
	double precision wrlast,wlotem,whitem,wvhit,wvlot
	double precision wbmin,wbmax
*
	integer lastchpos
*       atomic data
	integer mz
	character*2 lbz
	character*4 lzz
	double precision alm,fik,asm 
	common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),mz
*
*
*       character bounds for this machine
	call vp_charlims
*
	write(6,*) 'Spectral data filename?'
	read(5,'(a)') chold
	lench=lastchpos(chold)
	chold='%% '//chold(1:lench)//'  1 '
	lench=lench+7
*
101	write(6,*) 'linelist filename? [fort.9]'
	read(5,'(a)') inchstr
	lenin=lastchpos(inchstr)
	if(inchstr(1:1).eq.' ') then
	  inchstr='fort.9'
	  lenin=6
	end if
	open(unit=9,file=inchstr(1:lenin),status='old',err=101)
102	write(6,*) 'output filename? [vpin.dat]'
	read(5,'(a)') inchstr
	lenin=lastchpos(inchstr)
	if(inchstr(1:1).eq.' ') then
	  inchstr='vpin.dat'
	  lenin=8
	end if
	open(unit=13,file=inchstr(1:lenin),status='unknown',err=102)
*
*	presets:
*	number of regions
	nreg=0
	write(6,*) 'List order as input, or reversed? (i/r) [r]'
	read(5,'(a)') inchstr
	if(inchstr(1:1).ne.'r'.and.inchstr(1:1).ne.'R'.and.
     :      inchstr(1:1).ne.' ') then
	  linord=.true.
	 else
	  linord=.false.
	end if
*	
*	read in the line list
	pbtymx=0.0
	sigmx=0.0
	write(6,*) 'sigmathres,pbtythres,maxext,minext'
	write(6,*) '[4.5,0.0,20,5]'
	read(5,'(a)') inchstr
	call dsepvar(inchstr,4,dv,iv,cv,nv)
	if(cv(1)(1:1).ne.' ') then
	  sigthres=dv(1)
	 else
	  sigthres=4.5d0
	end if
	if(cv(2)(1:1).ne.' ') then
	  pthres=dv(2)
	 else
	  pthres=0.0d0
	end if
	if(cv(3)(1:1).ne.' ') then
	  maxext=iv(3)
	  sepmax=2.0d0*dble(maxext)
	 else
	  maxext=20
	  sepmax=40.0d0
	end if
	if(cv(4)(1:1).ne.' ') then
	  minext=iv(4)
	  sepmin=2.0d0*dble(minext)
	 else
	  minext=5
	  sepmin=10.0d0
	end if
	write(6,*) 'Maximum redshift, # Lyman series lines?'
	write(6,*) '(leave second one blank if just want Ly-alpha)'
	read(5,'(a)') inchstr
	call dsepvar(inchstr,4,dv,iv,cv,nv)
	lhatmin=1
	lhatmax=1
	if(cv(1)(1:1).ne.' ') then
	  nlzon=max(iv(2),1)
*	  write(6,*) nlzon
*         get atomic data
	  call vp_ewred(0)
	  write(6,*) 'Atomic data table contains',mz,' lines'
	  if(mz.gt.0) then
	    do while((lbz(lhatmin).ne.'H '.or.lzz(lhatmin).ne.'I   ').and.
     :                lhatmin.lt.mz)
	      lhatmin=lhatmin+1
	    end do
	    lhatmax=lhatmin
	    do while(lbz(lhatmax).eq.'H '.and.lzz(lhatmax).eq.'I   '.and.
     :               lhatmax-lhatmin+1.lt.nlzon.and.lhatmax.lt.mz)
	      lhatmax=lhatmax+1
	    end do
	    nlzon=lhatmax-lhatmin+1
	    write(6,*) nlzon,' Lyman series lines used'
	  end if
*         work out minimum wavelength for Ly-a clear of Ly-b etc
	  wbmin=(dv(1)+1.0d0)*1025.72d0
*         and maximum wavelength
	  wbmax=(dv(1)+1.0d0)*1215.67d0
	 else
	  wbmin=0.0d0
	  wbmax=1.0d30
	  nlzon=1
	end if
*       end setup section
*
*       read in the line list data
	nreg=0
903	read(9,'(a)',end=902) inchstr
	call dsepvar(inchstr,16,dv,iv,cv,nv)
*	look for wavelength limits
	if(cv(1)(1:4).eq.'wave') then
	  wvlot=dv(3)
	  wvhit=dv(5)
	  goto 903
	end if	
	if(cv(1)(1:3).ne.'abs'.or.dv(15).le.0.000001) goto 903
*	Have an absorption line, store variables
	nreg=nreg+1
	if(nreg.le.5000) then
	  wrlo(nreg)=dv(14)
	  wrhi(nreg)=dv(15)
	  sigr(nreg)=dv(8)
	  pbr(nreg)=dv(13)
	  aperch(nreg)=dv(9)
	  goto 903
	 else
	  write(6,*) 'Only first 5000 regions used!'
	end if
*	All lines now in, so sort out the ones you want
902	close(unit=9)
*	do i=1,nreg
*	  write(6,*) i,wrlo(i),wrhi(i),sigr(i),pbr(i)
*	end do
	jreg=1
	nout=0
	do while (jreg.le.nreg)
*	  Is region significance above selection limit?
	  if(sigr(jreg).ge.sigthres.and.pbr(jreg).ge.pthres) then
*	    Have a significant line
	    nout=nout+1
	    wlotem=wrlo(jreg)
	    whitem=wrhi(jreg)
*	    Search down until adequate continuum, and include regions
*	    which are too close together with this
	    ireg=jreg-1
	    do while (ireg.gt.0.and.
     :         (wlotem-wrhi(ireg)).lt.aperch(jreg)*sepmin) 
	      wlotem=wrlo(ireg)
	      ireg=ireg-1
	    end do
*	    have determined the lowest region wavelength
	    if(ireg.gt.0) then
	      wrlast=wrhi(ireg)
	     else
	      wrlast=wvlot
	    end if
	    nsep=int((wlotem-wrlast)/aperch(jreg))
	    nsep=min0(20,nsep/2)
	    wlout(nout)=wlotem-dble(nsep)*aperch(jreg)
*	    now the upper wavelength
	    ireg=jreg+1
	    do while (ireg.le.nreg.and.
     :         (wrlo(ireg)-whitem).lt.aperch(jreg)*sepmin) 
	      whitem=wrhi(ireg)
	      ireg=ireg+1
	    end do
*	    have determined the highest region wavelength
	    if(ireg.le.nreg) then
	      wrlast=wrlo(ireg)
	     else
	      wrlast=wvhit
	    end if
	    nsep=int((wrlast-whitem)/aperch(jreg))
	    nsep=min0(20,nsep/2)
	    whout(nout)=whitem+dble(nsep)*aperch(jreg)
*	    carry on where left off, i.e. at ireg
	    jreg=ireg
	   else
*	    line not significant, so go on to next
	    jreg=jreg+1
	  end if
	end do
*	now write out the results
	if(linord) then
	  nrglist=nout
	  do j=1,nout
	    write(13,*) chold(1:lench),wlout(j),whout(j)
	    write(13,*) ' '
	  end do
	 else
	  nrglist=0
	  kout=nout+1
	  do k=1,nout
	    j=kout-k
	    if(wlout(j).lt.wbmax.and.whout(j).gt.wbmin) then
*             is a system of interest, so list it
	      nrglist=nrglist+1
	      if(nlzon.le.1) then
	        write(13,*) chold(1:lench),wlout(j),whout(j)
	       else
*               include regions for higher Lyman series lines
		do il=1,nlzon-1
		  wqn=wlout(j)*alm(lhatmin+il)/alm(lhatmin)
		  wqx=whout(j)*alm(lhatmin+il)/alm(lhatmin)
*                 wavelengths in the list below wqn, and above wqx
*                 highest upper limit below mapped first region
		  nhb=1
*                 lowest upper limit above mapped region (use the ordering!)
		  nha=nout
*                 highest lower limit below mapped region
		  nlb=1
*                 lowest lower limit above mapped region
		  nla=nout
		  do kl=1,nout
		    xtemp=wqn-wlout(kl)
		    if(xtemp.gt.0.0.and.xtemp.lt.wqn-wlout(nlb)) then
		      nlb=kl
		    end if
		    xtemp=wlout(kl)-wqx
		    if(xtemp.gt.0.0.and.xtemp.lt.wlout(nla)-wqx) then
		      nla=kl
		    end if
		    xtemp=wqn-whout(kl)
		    if(xtemp.gt.0.0.and.xtemp.lt.wqn-whout(nhb)) then
		      nhb=kl
		    end if
		    xtemp=whout(kl)-wqx
		    if(xtemp.gt.0.0.and.xtemp.lt.whout(nha)-wqx) then
		      nha=kl
		    end if
		  end do
		  if(nrglist.le.4) then
		    write(6,*) 'nrg',nrglist
		    write(6,*) nlb,nhb,nla,nha
		    write(6,*) wlout(nlb),whout(nhb),wlout(nla),whout(nha)
		  end if
*                 decide which you want
		  if(whout(nhb).gt.wlout(nlb)) then
		    wslow(il)=whout(nhb)
		   else
		    wslow(il)=wlout(nlb)
		  end if
		  if(whout(nha).lt.wlout(nla)) then
		    wshigh(il)=whout(nha)
		   else
		    wshigh(il)=wlout(nla)
		  end if
		end do
*               there will be some overlaps which will need to be removed
*               made simpler by the fact that all are in decreasing wavelength order
		do il=1,nlzon-1
	     	  write(13,*) chold(1:lench),wslow(il),wshigh(il)
		end do
	        write(13,*) chold(1:lench),wlout(j),whout(j)
	      end if
	      write(13,*) ' '
	    end if
	  end do
	end if
	write(6,*) nrglist,' regions given from a total of ',nout
	close(unit=13)
	stop
	end


