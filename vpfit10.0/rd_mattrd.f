      subroutine rd_mattrd(da,nct,nspec,filnm)
      
      implicit none
      include 'vp_sizes.f'
      integer nofpixels,nofspec
      parameter (nofpixels = 1000,nofspec = 1000)
      double precision flux(nofspec,nofpixels)

      integer nspec,nct,i
      double precision da(*)
      character*(*) filnm

*     general wavelength coefficients
      integer nwco
      double precision wcf1(maxwco)
      common/vpc_wavl/wcf1,nwco
      character*8 wcftype
      character*4 vacind
      double precision helcfac
      integer*4 noffs
      common/c8_wcftype/wcftype,helcfac,noffs,vacind


      open (10,file=filnm,form='unformatted',status='old')
      read (10) flux
*
*     copy spectrum to data array
      nct=nofpixels
      do i=1,nofpixels
        da(i)=flux(nspec,i)
      enddo

      close(10)
*
*     wavelength info z=3.0 Ly-a base, increment 1.564915 km/s
      wcf1(1)=3.68687569d0
      wcf1(2)=5.21999459d-6
      nwco=2
      wcftype='log'
      helcfac=1.0d0
      noffs=0
      vacind='vac'
      return
      end
