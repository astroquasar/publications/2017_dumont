      subroutine vp_scontf(lcadd,chisqv,ibck,parm,ipind,np,nfunc,
     :               ion,level, wvregion)
*
*     OUT:
*     wvregion	central wavelength for region with continuum
*			   to be added
      implicit none
      include 'vp_sizes.f'
      logical lcadd
      integer ibck,np,nfunc
      character*2 ion(*)
      character*4 level(*)
      character*2 ipind(maxnpa)
      double precision parm(maxnpa)
      double precision chisqv,wvregion
*
*     Local:
      integer i,j,jcx,nfion,nlo
      double precision bmax,colmax,wavel
      logical ltemp
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     vp_scontf parameters
      logical lcontv
      character*4 chcontc
      integer nconn
      double precision conchst,conbval,conlhden
      common/vpc_scontf/conchst,conbval,conlhden,lcontv,
     :         chcontc,nconn
*     chunk variables:
      integer ndpts(maxnch),idrun(maxnch),icrun(maxnch)
      integer indfil(maxnch)
      character*64 filename(maxnch)
      double precision wvstrt(maxnch),wvend(maxnch)
      integer nchunk
      common/vpc_chunk/wvstrt,wvend,ndpts,filename,
     :      idrun,icrun,indfil,nchunk
*     Log N if indvar=1, N otherwise (-1 for emission)
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
      double precision vturb,atmass,temper,fixedbth
      common/vpc_ucopder/vturb(maxnio),atmass(maxnio),
     :                 temper(maxnio),fixedbth(maxnio)
*     region flags
      integer linchk( maxnio )
      common/vpc_linchk/linchk
*     probability limits for adding lines
      integer nladded
      double precision chisqvh,probchb,probksb
      common/vpc_problims/chisqvh,probchb,probksb,nladded
*     reserved character
      character*4 specchar
      common/vpc_specchar/specchar
*     last added ion
      character*2 lastaddion,chaddion
      common/vpc_lastaddion/lastaddion,chaddion
*
      nladded=0
      ltemp=.false.
*     are either of the conditions satisfied for adding continuum?
      if(chisqv.lt.conchst) then
        ltemp=.true.
       else
*       search back down the parameter list for run of nconn
        if(nconn.lt.nfunc) then
*         there are enough to make it worth doing!
          nlo=nfunc-nconn+1
          colmax=-1.0d37
          bmax=-1.0d37
          do j=nlo,nfunc
            i=noppsys*(j-1)
*           col is base+nppcol
            if(parm(i+nppcol).gt.colmax) colmax=parm(i+nppcol)
*           b is base+nppbval
            if(bmax.lt.parm(i+nppbval)) bmax=parm(i+nppbval)
          end do
          if(bmax.le.conbval.and.colmax.le.conlhden) then
            ltemp=.true.
          end if
        end if
      end if
*     convert colmax to log cgs if necessary
      if(indvar.ne.0) then
        colmax=colmax/scalelog
       else
        if(indvar.eq.0.and.colmax.gt.0.0d0) then
          colmax=log10(colmax/scalefac)
*         indvar negative is emission, and this test does not apply
        end if
      end if
*     
      if(ltemp) then
*       now search to see if there is a continuum variable in the range
*       covered by the worst region
        j=1
*       ltemp is now true if there is no continuum yet found in range
        do while (j.le.nfunc.and.ltemp)
          if(ion(j).eq.'<>') then 
*           check redshift is within the range
            wavel=(1.0d0+parm(noppsys*(j-1)+nppzed))*1215.67d0
            if(wvstrt(ibck).lt.wavel.and.wvend(ibck).gt.wavel) then
              ltemp=.false.
            end if
          end if
          j=j+1
        end do
      end if
*
*     need to add continuum factor if ltemp is true still,
*     and parameter array limit not exceeded
      if(ltemp.and.np.lt.maxnpa) then
        nfion=nfunc
        nfunc=nfunc+1
        ion(nfunc)='<>'
        level(nfunc)='    '
        parm(np+nppcol)=1.00d0
        parm(np+nppbval)=0.00d0
*       write(6,*) 'SCONTF:',ibck,wvstrt(ibck),wvend(ibck)
        wvregion=0.5d0*(wvstrt(ibck)+wvend(ibck))
        parm(np+nppzed)=wvregion/1215.67d0-1.0d0
        ipind(np+nppcol)='  '
        ipind(np+nppzed)=specchar(1:1)//'Z'
        if(chcontc(1:1).eq.'c') then
          ipind(np+nppbval)=specchar(1:1)//'X'
         else
          ipind(np+nppbval)='  '
        end if
        if(noppsys.ge.4) then
*         set values to zero, and fix them
          do jcx=4,noppsys
            parm(np+jcx)=0.0d0
            ipind(np+jcx)=specchar(1:1)//'E'
          end do
        end if
*       these are unnecessary, but might as well fill them in:
        atmass(nfunc)=1.0d0
        linchk(nfunc)=0
        fixedbth(nfunc)=0.0d0
        vturb(nfunc)=0.0d0
        temper(nfunc)=0.0d0
        np=np+noppsys
        lcadd=.true.
        nladded=1
*       put in the last added parameter type
        lastaddion='<>'
        chaddion='<>'
*       sort out the end ones in case there is a baseline adjustment
        call vp_endorder(nfion,nfunc,ion,level,parm,ipind,np)
      end if
      return
      end
  
