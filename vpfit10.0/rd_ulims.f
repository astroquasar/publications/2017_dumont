      subroutine rd_ulims(da,de,ca,cad,re,ds,nct,ichunk)
*
*     establish upper limit to a column density for a given
*     redshift, bval for selected ion, given ion
*     includes only data below fit+xn*sigma, for lines +/- xb*bvalue
*     and with min number of pixels nminf, minimizing the normalized chi^2
*     for these included lines.
*     The data must be normalized to unit continuum.
*
      implicit none
      include 'vp_sizes.f'
*
*     arguments: all in or workspace
*     da-data, de-sigma^2, ca-continuum, cad-work continuum, re-work, ds-work
      integer nct,ichunk
      double precision da(nct),de(nct),ca(nct),cad(nct)
      double precision re(nct),ds(nct)
*     local variables
      logical lcmin
      integer i,j,ii,jj,jnew,k1,k2,ndf,ndx,klow,khiw,numbv
      integer nminf,nminfd,nrest,ifail,nclo,nchi,jjcmax
      integer ncmin,ncmax
      integer nminfdef
      double precision xdb
      double precision temp,dddd,colmmax
      character*2 elvbref,elvb
      character*4 levbref,levb
      double precision btemp,btemplog,dfwhmkms
      double precision bval,cval,zval,cvalh,dbv,bvalref,btherm
      double precision ebvalref,bvalmins
      double precision ddmax,ddmaxh,ddtemp,xb,xn,chisq
      double precision chisqn,chisqnmin,pchisq,pchslim
      double precision atmref,atmvb
      double precision qchs,pqchs
      double precision wvrest(12)
      integer nchlob(12),nchhib(12)
      double precision wvlob(12),wvhib(12)
      double precision diffn(10000)
      integer ipn(10000)
      double precision bvh(1000),cvh(1000),chsnh(1000)
      integer ndfh(1000)
*     functions
      double precision pd_pchi2
      double precision vpf_dvresn,vp_wval
*     common variables
*
*     character string handling & unpacking
      character*132 inchstr
      character*60 cv(24)
      real rv(24)
      integer iv(24)
      integer nv
      common/rd_chwork/inchstr,rv,iv,cv,nv
      double precision dv(24)
      common/vpc_dsepspace/dv
*     input streams
      integer inputc
      common/rdc_inputc/inputc
*     atomic data
      integer m
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),m
*     wavelength stuff
      double precision dnshft2(maxnch)
      common/vpc_shft2/dnshft2
*     diagnostic print control
      logical verbose
      common/vp_sysout/verbose
*
*     copy continuum to workspace
      do i=1,nct
        ds(i)=ca(i)
      end do
      pchslim=0.16d0
      nminfd=5
      dfwhmkms=6.7d0
      nminfdef=3
*     minimum bvalue hardwired to 0.5 km/s
      bvalmins=0.5d0
 909  write(6,'(a,i3,f7.3,a,f7.2,i3,f7.2,a)') 
     :     'xn,xb,nminf,pchslim,ctype,vres(km/s),nmdef,bmin [1  2 ',
     :        nminfd,pchslim,' cmin',dfwhmkms,nminfdef,bvalmins,']'
      read(inputc,'(a)') inchstr
      if(inchstr(1:1).eq.'?') then
        write(6,*) 'Determine an upper limit to the column density for'
        write(6,*) 'an ion given another ion, its b-value and redshift.'
        write(6,*) 'The data MUST be normalized to unit continuum, and'
        write(6,*) 'the error estimate represent the RMS fluctuations'
        write(6,*) 'in the data. Does it by comparing a fit profile'
        write(6,*) 'with the data, and including only regions where'
        write(6,*) 'the fit is below the data or less than xn-sigma'
        write(6,*) 'above it.'
        write(6,*) 'First parameters are: xn-sigma above fit for' 
        write(6,*) 'inclusion in statistic, half-width in bvals for'
        write(6,*) 'inclusion, default min channels for inclusion,'
        write(6,*) 'stopping probability (0.16 is one-sided 1-sigma).'
        write(6,*) 'The lowest column density is that consistent with'
        write(6,*) 'the noise independent of the data if no line is'
        write(6,*) 'detected (so high continuum is not a factor).'
        write(6,*) 'To turn this off the parameter ctype -> "nocmin"'
        goto 909
      end if
      call dsepvar(inchstr,8,dv,iv,cv,nv)
*     set values or defaults as appropriate
      if(cv(1)(1:1).eq.' ') then
        xn=1.0d0
       else
        xn=dv(1)
      end if
      if(cv(2)(1:1).eq.' ') then
        xb=2.0d0
       else
        xb=dv(2)
      end if
      if(iv(3).gt.0) then
*       minimal lower limit, else pairs with one saturated component
*       are wildly overestimated.
        nminfd=iv(3)
      end if
      if(cv(4)(1:1).ne.' ') then
        pchslim=dv(4)
      end if
      write(6,'(a,2f6.2,i4,f7.3)') 'Using:',xn,xb,nminfd,pchslim
      lcmin=.true.
      if(cv(5)(1:1).eq.'n'.or.cv(5)(1:1).eq.'N') then
        lcmin=.false.
        write(6,*) 'with noise-based lower limit turned off'
      end if
      if(dv(6).gt.0.0d0) then
        dfwhmkms=dv(6)
      end if
      if(iv(7).gt.0) then
        nminfdef=iv(7)
      end if
      if(dv(8).gt.0.0d0) then
        bvalmins=dv(8)
      end if
      write(6,'(a,f6.2,a)') 'FWHM = ',dfwhmkms,' km/s'
*     Parameters for the search
 902  write(6,*) 'Ion,bval,(err),redshift,newion,(lambda,lambda...)'
      read(inputc,'(a)') inchstr
      call dsepvar(inchstr,15,dv,iv,cv,nv)
      if(cv(1)(2:2).eq.'I'.or.cv(1)(2:2).eq.'V'.or.
     :   cv(1)(2:2).eq.'X') then
        elvbref=cv(1)(1:1)//' '
        levbref=cv(1)(2:5)
       else
        elvbref=cv(1)(1:2)
        levbref=cv(1)(3:6)
      end if
      if(elvbref(1:1).eq.' ') goto 902
      bvalref=dv(2)
      if(dv(4).eq.0.0d0.and.cv(4)(1:1).ne.'0') then
*       no error on reference Doppler parameter
        zval=dv(3)
        ebvalref=0.0d0
        jnew=4
       else
*       third parameter is bval error estimate
        write(6,*) ' .. third parameter taken as bval error'
        ebvalref=dv(3)
        zval=dv(4)
        jnew=5
      end if
      if(cv(jnew)(2:2).eq.'I'.or.cv(jnew)(2:2).eq.'V'.or.
     :   cv(jnew)(2:2).eq.'X') then
        elvb=cv(jnew)(1:1)//' '
        levb=cv(jnew)(2:5)
       else
        elvb=cv(jnew)(1:2)
        levb=cv(jnew)(3:6)
      end if
      if(elvb(1:1).eq.' ') goto 902
*     get rest wavelengths
      if(nv.gt.jnew) then
        do j=jnew+1,nv
          wvrest(j-jnew)=dv(j)
        end do
        nrest=nv-jnew
       else
        nrest=0
      end if
*     get atomic masses
      if(m.le.0) call vp_ewred(0)
      call vp_atomass(elvbref,atmref)
      call vp_atomass(elvb,atmvb)
*     convert approximate wavelengths to accurate ones
      if(nrest.gt.0) then
        do j=1,nrest
          ddmaxh=1.0d20
          ddmax=1.0d21
          do i=1,m
            if(elvb.eq.lbz(i).and.levb.eq.lzz(i)) then
              ddmax=abs(alm(i)-wvrest(j))
              if(ddmax.lt.ddmaxh) then
                ddmaxh=ddmax
                ddtemp=alm(i)
              end if
            end if
          end do
          wvrest(j)=ddtemp
        end do
       else
*       choose the first two in the list!
        do i=1,m
          if(elvb.eq.lbz(i).and.levb.eq.lzz(i)) then
            nrest=nrest+1
            wvrest(nrest)=alm(i)
          end if
          if(nrest.ge.2) goto 901
        end do
 901    continue
        if(nrest.eq.1) then
          write(6,*) 'Using ',wvrest(1)
         else
          write(6,*) 'Using ',wvrest(1),wvrest(2)
        end if
      end if
*     thermal Doppler parameter
      if(atmref.lt.atmvb) then
        btherm=max(bvalref-ebvalref,bvalmins)*sqrt(atmref/atmvb)
        btemp=bvalref+ebvalref
       else
        btherm=(bvalref+ebvalref)*sqrt(atmref/atmvb)
        btemp=max(bvalref-ebvalref,bvalmins)
      end if
*      btemp=bvalref ! no error case
      if(elvb.eq.'H ') then
*       choose minimum temperature 10,000K
        btemp=sqrt(bvalref**2+165.1225d0*(1.0d0/atmvb-1.0d0/atmref))
        if(btemp.gt.btherm) then
          btherm=btemp
        end if
      end if
      temp=btherm-btemp
      btemplog=log(btemp)
*     use logarimic steps to avoid program spending ages with similar
*     b-values
      dbv=log(btherm)-btemplog
      if(abs(temp).lt.1.0d0) then
        if(abs(temp).lt.1.0d-4) then
          numbv=1
         else
          numbv=2
        end if
       else
        if(abs(dbv).lt.0.175d0) then
          numbv=3
         else
          if(abs(dbv).le.0.275d0) then
            numbv=5
           else
*           Use 7 log steps to cover the range
            numbv=7
          end if
        end if
        dbv=dbv/dble(numbv-1)
      end if
*      write(6,*) btherm,btemp,temp,numbv
      do jj=1,numbv
        bval=exp(btemplog+dble(jj-1)*dbv)
*       set limits for the chi-squared determination
        nminf=0
*       and for the range for smoothing
        ncmin=8000000
        ncmax=-8000000
        do i=1,nrest
          ddtemp=(1.0d0+zval)*wvrest(i)
          wvlob(i)=(1.0d0-xb*bval/2.99792458d5)*ddtemp
          wvhib(i)=(1.0d0+xb*bval/2.99792458d5)*ddtemp
          call vp_chanwav(wvlob(i),ddtemp,5.0d-2,20,ichunk)
          nchlob(i)=int(ddtemp)-1
          call vp_chanwav(wvhib(i),ddtemp,5.0d-2,20,ichunk)
          nchhib(i)=int(ddtemp)+1
*         max channels available
          if(nchhib(i).le.nct.and.nchlob(i).ge.1) then
            nminf=nchhib(i)-nchlob(i)+1+nminf
          end if
          ncmin=min(ncmin,nchlob(i))
          ncmax=max(ncmax,nchhib(i))
        end do
        nminf=min(nminfd,nminf/2)
        if(nminf.le.nminfdef) then
          write(6,'(a,i3,a)') 'Statistic may be over ',nminfdef,
     :                 ' channels only'
          nminf=nminfdef
        end if
*       for the given b-value, step up in column density to minimum affective
*       chi-squared
        chisqnmin=1.0d30
        chisqn=1.0d29
        pqchs=1.0d20
        pchisq=1.0d29
        cvalh=11.0
        cval=cvalh
        ndf=0
        do while ((chisqn.lt.chisqnmin.or.pchisq.gt.pchslim.or.
     :    pqchs.gt.pchslim).and.cval.lt.20.0)
*          write(6,*) chisqn,chisqnmin,pchisq,pchslim,pqchs
          pqchs=0.0d0
          chisqnmin=chisqn
*         copy previous values to holding space
          bvh(jj)=bval
          cvh(jj)=cval
          chsnh(jj)=chisqn
          ndfh(jj)=ndf
          if(cval.le.13.5) then
            cval=cval+0.05
           else
            if(cval.le.14.5) then
              cval=cval+0.1
             else
              cval=cval+0.2
            end if
          end if
*         put the line in, takes cad in, output to ca
*         need to set klow,khiw
          klow=1
          khiw=nct
          call spvoigt(ds,cad,klow,khiw,
     :            cval,zval,bval,elvb,levb,ichunk,re)
*          dlam=0.0d0
*         sigma hard-wired to 6.7 km/s for now <-------
*          sigma=dfwhmkms/2.35d0
*         print out resolution at central wavelength
          xdb=vp_wval(dble((ncmax+ncmin)/2)+dnshft2(ichunk),ichunk)
          xdb=vpf_dvresn(xdb,ichunk)*7.05957d5
*          write(6,'(''Resolution FWHM '',f8.2,'' km/s'')') xdb
*         output now to cad. For speed should restrict convolution range...
*         rfc 15.07.04: replace 1 by ncmin-40, nct by ncmax+40
          ncmin=max(ncmin-40,1)
          ncmax=min(ncmax+40,nct)
          call vp_spread(ds,ncmin,ncmax,ichunk, cad)
*         compare the profile with the data, using the preset ranges
*         set up an array of differences/sigma, sort them, and use
*         those which satisfy the criteria, down to a minimum number
          ndx=0
          qchs=0.0
          do k1=1,nrest
            if(cval.le.14.5d0.or.elvb.ne.'H ') then
              nclo=nchlob(k1)
              nchi=nchhib(k1)
             else
              if(cval.le.18.0d0) then
                dddd=1.0d0
               else
                dddd=10.0d0**(0.5d0*(cval-18.0d0))
              end if
              temp=(1.0d0+zval)*(wvrest(k1)-dddd)
              call vp_chanwav(temp,ddtemp,5.0d-2,20,ichunk)
              nclo=int(ddtemp)-1
              nclo=min(nchlob(k1),nclo)
              temp=(1.0d0+zval)*(wvrest(k1)+dddd)
              call vp_chanwav(temp,ddtemp,5.0d-2,20,ichunk)
              nchi=int(ddtemp)+1
              nchi=max(nchhib(k1),nchi)
            end if
*           keep within range
            if(nclo.lt.1) nclo=1
            if(nchi.gt.nct) nchi=nct
*           compute statistic
            if(nchi.ge.1.and.nclo.le.nct) then
              do k2=nclo,nchi
                if(de(k2).gt.0.0) then
                  ndx=ndx+1
                  diffn(ndx)=(da(k2)-cad(k2))/sqrt(de(k2))
                  qchs=(ca(k2)-cad(k2))**2/de(k2)+qchs
                end if
              end do
            end if
          end do
          if(ndx.lt.1) then
*           no constraints at all
            write(6,*) elvb,levb,zval,' no lines in range'
            write(26,*) '! ',elvb,levb,zval,' no lines in range'
            goto 908
          end if
*         sort pointers for this array
          call pda_qsiad(ndx,diffn,ipn)
*         compute chi^2 for the acceptable ones, which are those
*         values > -1 for 1-sigma
          k1=1
          do while(diffn(ipn(k1)).lt.-xn.and.k1.lt.ndx-nminf-1)
            k1=k1+1
          end do
*          write(6,*) 'ndx,nminf',ndx,nminf
*         chi^2
          chisq=0.0d0
          do ii=k1,ndx
            chisq=chisq+diffn(ipn(ii))**2
          end do
          ndf=ndx-k1
          ifail=0
          pchisq=pd_pchi2(chisq,ndf,ifail)
          if(lcmin) then
            pqchs=pd_pchi2(qchs,ndx,ifail)
          end if
          chisqn=chisq/dble(ndf)
*         restore continuum for the next loop
          do k2=1,nct
            ds(k2)=ca(k2)
            cad(k2)=ca(k2)
          end do
        end do
      end do
*     find the maximum column density, and print out values
      jjcmax=0
      colmmax=0.0
      do jj=1,numbv
        write(6,*) jj,bvh(jj),cvh(jj),chsnh(jj),ndfh(jj)
        if(cvh(jj).gt.colmmax) then
          colmmax=cvh(jj)
          jjcmax=jj
        end if
      end do
*     write the answer in fort.26 format
      write(6,'(2x,a2,a4,f10.6,a11,f8.2,a11,8x,a1,f6.3)')
     :      elvb,levb,zval,'SZ 0.000000',bvh(jjcmax),
     :      'SB   0.00  ','<',colmmax
      write(26,'(2x,a2,a4,f10.6,a11,f8.2,a11,8x,a1,f6.3)')
     :      elvb,levb,zval,'SZ 0.000000',bvh(jjcmax),
     :      'SB   0.00  ','<',colmmax
 908  return
      end
