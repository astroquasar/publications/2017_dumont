      subroutine rd_pmpset(cv,dv,nv)
*     preset some plotting parameters
*     fed in via the command line
      implicit none
      integer nv
      character*(*) cv(*)
*      integer iv(*)
      double precision dv(*)
*     plot/print velocity scale
      logical lvel
      real wcenpg,vellopg,velhipg,zp1refpg
      common/vpc_pgvels/wcenpg,vellopg,velhipg,lvel,zp1refpg
*     line list variables
      double precision zmark
      character*132 chmarkfl
      logical lmarkl,lmarkto
      common/rdc_lmarklv/zmark,chmarkfl,lmarkl,lmarkto
*     file variables for O/P for subsequent vpfit run:
      logical lchg13
      character*80 chg13file
      common/rdc_chg13/chg13file,lchg13
*
      if(nv.le.1.or.cv(2)(1:1).eq.' ') then
        write(6,*) 'format is "pp (f, s or z) parameter"'
       else
*       have something to do
        if(cv(2)(1:1).eq.'z'.or.cv(2)(1:1).eq.'Z') then
*         preset reference redshift
          zmark=dv(3)
          zp1refpg=zmark+1.0d0
*         set flags so default is to plot the tick marks & labels
          lmarkl=.true.
          lmarkto=.false.
         else if(cv(2)(1:1).eq.'f'.or.cv(2)(1:1).eq.'F') then
          chmarkfl=cv(3)
          zmark=-1000.d0
          lmarkto=.false.
*         presumably want to see the result!
          lmarkl=.true.
         else if(cv(2)(1:1).eq.'s'.or.cv(2)(1:1).eq.'S') then
*         close the old file if it was open
          if(lchg13) then
            if(chg13file(1:7).eq.'fort.13'.and.cv(3)(1:1).eq.' ') then
              write(6,*) 'fort.13 default -- old fort.13 not closed'
              goto 997
            end if
            close(unit=22)
          end if
          if(cv(3)(1:1).ne.' ') then
            chg13file=cv(3)
           else
            chg13file='fort.13'
          end if
          lchg13=.false.
         else
          write(6,*) cv(2)(1:1),' option not supported'
        end if
      end if
 997  continue
      return
      end
