      subroutine vp_ucoptv(  fitret,
     :                    ndch, parerr, idiag,  
     :                    nchunk, nopt, ialfind, prbks, totit )
*
*     Modifications by Julian King included 01.03.07
*
*     first parameters WERE data, error, contin
*     now uses the common area 2D dataset variables
*
*     unconstrained n dimensional optimisation using Gauss-Newton 
*     method with parameter search. JK: also uses Levenberg-Marquardt
*     method if the going gets tough..
*     OUT:
*     fitret is returned as the fitted data
*
      implicit none
*     this file has the basic sizes for various common arrays etc
      include 'vp_sizes.f'
*
      integer ndch(maxnch)
      integer idiag,nchunk,nopt,ialfind
      double precision parerr(maxnpa)
      double precision prbks(maxnch)
      integer totit
*
      double precision sgrad(maxnpa),grad(maxnpa)
      double precision hessian(maxnpa,maxnpa)
      double precision sumtot,sumnew
      double precision wtst,vacst,vacend,wvregion
      double precision bmincur,bxcur
      double precision fitret(maxchs,maxnch)
      double precision bb(maxnpa,maxnpa)
      integer jjmax,jjmaxsz
*     JAK was jjmax=20, jjmaxsz=21
      parameter ( jjmax = 30 )
      parameter (jjmaxsz = 31)
      double precision xdat(jjmaxsz),xcor(jjmaxsz),lxcor(jjmaxsz)
*
*     LOCAL variables
      integer i,ii,is,iop,istat,iter,itlim,ichunk,ichdum,ifail,ibck
      integer j,jj,j1,j2,jbase,jn,jnbv,jnc
      integer jnz,jsx,jxr,jxhi
      integer k,ki,kjb,klen,kloc,kphi,ktempl,ktempv
      integer lkoffs,lowpnt
      integer n,nn,nchcen,nfunc,npts,ndftot,nratio,nsubd
      integer ntps,nvbopchan,nplus,npair,nrun,ndf,nppxt
      integer nchzero,nalwaysLM
      logical lrms
      double precision alpha,aln,bval
      double precision chisqo,chisqv,convtst
      double precision cs1t,cs2t,cs3t,prks
*     double precision cs1sig,cs2sig,cs3sig
      double precision dummy
      double precision ratio,step,steplow,sumlow
      double precision tempp
      double precision velpch,dtemp,bvtem
      double precision wstxx,wendxx
      double precision prtotx,probacc,probmin,probmax
      double precision pbgdev
      double precision chsq(maxnch)
      double precision dks(maxnch)
*
*     Functions
      logical lcase,nocgt
      double precision pd_pchi2 
      double precision vpf_bvalsp
      double precision vp_wval
*
*     COMMON:
*     subchunk workspace
      double precision contpass(maxscs),fconv(maxscs)
      common/vpc_duwork/fconv,contpass
*     fit & line parameters
      character*2 ion(maxnio)
      character*4 level(maxnio)
      double precision parm(maxnpa)
      integer nlines,np
      common/vpc_parry/parm,ion,level,nlines,np
      integer isod,isodh
      character*2 ipind(maxnpa)
      common/vpc_usoind/ipind,isod,isodh
*     Constraints for chunks (limits)
      integer npftr(maxnch),npfmax(maxnch)
      double precision prun
      integer ndcount
      logical steep
      logical forwrd
      logical final
      logical lfirst
      logical lcadd
      double precision puni(maxnpa)
      integer metric, oldmet
      logical even
      logical tied, varythis
      integer olddct
      integer rank( maxnpo )
      double precision dtot, kstot
*      double precision chi27    ! cs for constrained params
*      integer ndf27             ! ndf for constrained params
*
      double precision zed
      common/vpc_ewpms/zed
      common / metric / metric, even
      double precision pardif( maxnpa )
      common /vpc_pardif / pardif
*     chunk list special variables
      integer linchk( maxnio )  ! ties lines to chunks
      common/vpc_linchk/linchk
      character*4 chlnk(maxnio)
      common/vpc_chlnchk/chlnk
*     func may be subchunk, phi is chunk
      double precision func(maxscs),phi(maxchs,maxnch)
      common/vpc_der/func,phi
*     
      double precision wcoeff(maxnch)
      integer*4 numpts(maxnch)
      integer ngd,npexsm
      common/vpc_jkw3/wcoeff,ngd,npexsm,numpts
*     channel limits for fitting regions
      integer*4 ichstrt(maxnch),ichend(maxnch)
      common/jkw4/ichstrt,ichend
*     sigma scale now handled on input, from drms, de separation
*      double precision smultd
*      common/vpc_jkw5/smultd
*
      integer nminchsqit
      double precision chsqdif
      double precision cdstsf,zstep,bstep,chsqdif1,chsqdnth
      double precision chsqdif2
      common/vpc_jkw2/cdstsf,zstep,bstep,chsqdif1,chsqdnth,
     :      chsqdif2,nminchsqit
*
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm
      integer nz
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),nz
*     Log N if indvar=1, N otherwise (-1 for emission)
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
      double precision vturb,atmass,temper,fixedbth
      common/vpc_ucopder/vturb(maxnio),atmass(maxnio),
     :                 temper(maxnio),fixedbth(maxnio)
*     probability limits for adding lines
      double precision chisqvh
      double precision probchb,probksb
      integer nladded
      common/vpc_problims/chisqvh,probchb,probksb,nladded
*     b-parameter and column density limits
      double precision bvalmin,bvalmax,bltdrop,bgtdrop,colltdrop,
     :                     clogltdrop,cvaldrop,clvaldrop,cvalmax,
     :                     clvalmax,bvalminh,bvalmaxh,clvalmin,
     :                     cvalmin
      common/vpc_bvallims/bvalmin,bvalmax,bltdrop,bgtdrop,colltdrop,
     :                     clogltdrop,cvaldrop,clvaldrop,cvalmax,
     :                     clvalmax,bvalminh,bvalmaxh,clvalmin,
     :                     cvalmin
*     workspace from deriv
      double precision f(maxnpo,maxnpa)
      common/vpc_biggy/f
*     rejection for svd routine
      double precision rlim
      common /vpc_svd / rlim
      double precision dnshft2(maxnch)
      common/vpc_shft2/dnshft2
*     rfc 1.5.97 rejection of ill-constrained variables
      logical nasty
      integer knasty
      common/vpc_nasty/knasty,nasty
*     printout variable
      logical verbose
      common/vp_sysout/verbose
*     monitoring level 0:minimal - 16:everything
      integer nverbose
      common/vp_sysmon/nverbose
*     iteration and statistics for fit
      double precision chisqvc,prtotc
      integer iterc,nptsc,ndftotc
      common/vpc_smry/chisqvc,prtotc,iterc,nptsc,ndftotc
*     vp_scontf parameters
      logical lcontv
      character*4 chcontc
      integer nconn
      double precision conchst,conbval,conlhden
      common/vpc_scontf/conchst,conbval,conlhden,lcontv,
     :         chcontc,nconn
*     general control variables
      character*4 chcv(10)
      common/vpc_chcv/chcv
*     chcv(3) = 'citn' keep current iteration in junk.dat,
*     and remove the file on satisfactory exit.
*     Zero level limits
      double precision zlevmin,zlevmax
      common/vpc_zlevlims/zlevmin,zlevmax
*     wavelength shifts, held in:
*      common/vpc_asschnk/lassoc(maxnch)
*     x-reference tied list
      integer mtxref(maxnpa)
      common/vpc_txref/mtxref
*     Totals (col), or special (b), after this character 
      character*2 lastch,firstch
      common/vpc_ssetup/lastch,firstch
*     end statistics
      logical literok
      double precision prtot
      common/vpc_endstat/prtot,literok
*     output channels:
      integer lt(3)             ! output channels for printing
      integer nopchan,nopchanh,nmonitor
      common/vpc_nopchan/lt,nopchan,nopchanh,nmonitor
*     debug information -2x for ucoptv
      integer ndbranch
      common/vpc_debug1/ndbranch
*     last added ion
      character*2 lastaddion,chaddion
      common/vpc_lastaddion/lastaddion,chaddion
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     temperature units
      double precision thunit,thmax
      common/vpc_thscale/thunit,thmax
*     binned profile, or resolution as a function of wavelength
*     what it does for that chunk steered by chprof
      character*2 chprof
      double precision pfinst
      double precision wfinstd
      integer npinst,npin2
      common/vpc_profwts/wfinstd(maxipp,maxnch),pfinst(maxipp,maxnch),
     :        npinst(maxnch),npin2(maxnch),chprof(maxnch)
*     chunk subset variables (rfc: 24.08.05)
*     only preset is nrextend - number of pixels to extend a chunk by
*     so that convolution does not have a jump at the end
      integer nrextend
      common/vpc_chunkext/nrextend
*     chunk subdivision variables
      integer nminsubd,nmaxsubd
      double precision bindop
      common/vpc_vpsubd/bindop,nminsubd,nmaxsubd
*
      integer npsubch
      double precision wvsubd(maxscs)
      common/vpc_wvsubd/wvsubd,npsubch
*     chunk FWHM well sampled by pixels (if this variable = .true.)
      logical lresint(maxnch)
      double precision dlgres
      common/vpc_lresint/dlgres,lresint
*     New 2D dataset variables
      integer numchunk,nchchtot ! # of chunks,total # data points
      integer kchstrt(maxnch),kchend(maxnch),nchlen(maxnch)
      common/vpc_chunkvar/kchstrt,kchend,nchlen,numchunk,nchchtot
      double precision dach(maxchs,maxnch),derch(maxchs,maxnch)
      double precision drmch(maxchs,maxnch),dcch(maxchs,maxnch)
      common/vpc_datch/dach,derch,drmch,dcch
*     chunk wavelengths
      integer nwvch(maxnch)
      double precision wavch(maxchs,maxnch),wavchlo(maxchs,maxnch)
      common/vpc_wavch/wavch,wavchlo,nwvch
*     fit to resolution, using sigmas
      integer nswres(maxnch)
      double precision swres(maxpoc,maxnch)
      common/vpc_sigwres/swres,nswres
*     iteration parameter limits, effectively a machine precision
      double precision chsqdlim1
      common/vpc_jkw2lim/chsqdlim1
*     extra ion list & parameters
      integer nionxt
      character*2 elmxt(maxxpi)
      character*4 ionxt(maxxpi)
      character*2 ipinxt(maxxpp)
      double precision bvxmin
      double precision parmxt(maxxpp)
      common/vpc_parmxt/bvxmin,parmxt,ionxt,elmxt,nionxt,ipinxt
*     dropped systems
      integer ndrop,kdrop,ndroptot,ndrwhy
      common/vpc_nrejs/ndrop,kdrop(maxnio),ndroptot,ndrwhy
*     parameter error estimates rescaled, or not? Default is yes
      logical lrsparmerr,luvespop
      common/vpc_rsparmerr/lrsparmerr,luvespop
*     probability estimates for each chunk (currently probmax)
      integer indprobchk
      integer nprchk(maxnch)
      double precision probchk(maxnch)
      common/vpc_probchk/probchk,nprchk,indprobchk
*
*     LM method steering variables
*     forceLM=.true. allows LM method to be forced if no GN improvement
*     otherwise just ends iteration
      logical forceLM,neverLM
      common/vpc_LMvars/forceLM,neverLM
*     JAK INSERTION------------
c     JAK LM stuff
      integer LMcounter,LMmaxit
      logical doLM,lastitGNstep,alwaysLM
*     logical LMfinish
      double precision lmchisqv
      double precision LMparm(maxnpa),GNparm(maxnpa)
      double precision LMlambda
      double precision gnchisqv, gnsumnew, lmsumnew
c     The doLM flag below determines whether the pass through ucoptv.f
c     is the GN method or the LM method. The GN method is done first;
c     After it is complete, doLM is set to .true. , and the whole
c     iteration is run again. Most of the code is oblivious to the
c     change, except for:
c      - The bit commented "did we just do GN or LM step" near the
c        end
c      - the lm_udchole routine, which is udchole with an additional
c        argument (lambda), which is used to multiply the hessian
c        diagonal by (1+lambda). This then obtains the LM 
c        search direction as grad
c     
      doLM = .false.
*      LMfinish = .false.
      lastitGNstep = .true.
      alwaysLM = .false.
      nalwaysLM=0
      LMcounter=0
      LMmaxit=30
c     Starting value of lambda = 1.0d-4 (essentially GN method)
      LMlambda=1.0d-4
      if(probchb .gt. 0.0d0) isod=1
*     Make a note in the fort.18 file that we are using JAK
*     version of VPFIT (i.e. including LM and GN). You probably
*     want to remove this for a release version (JAK)
*      write(18,*) '---JKPHDVER---'
      gnchisqv=-999.0d0
      lmchisqv=-999.0d0
      gnsumnew=0.0d0
      lmsumnew=0.0d0
*     END JAK INSERTION---------
*
*     initialise a few counters etc
      iter = -1
      nchzero=0
      if(nopt.eq.9) then
        nopchan=1
       else
        nopchan=nopchanh
      end if
      nvbopchan=min(2,nopchan)
      itlim = 125
      nfunc = np/noppsys
      npts = 0
      chisqo=0.0d0
      chisqv=0.0d0
      ndftot=0
      sumnew=0.0d0
*     at very start, newton rather than steepest
      steep=.false.
      final=.false.
*     with increasing jump size
      forwrd=.true.
*     re-set b parameters, tie tied parameters, etc.
      call vp_tieval( ion, level, np, parm, ipind, temper,
     :               vturb, atmass, fixedbth )
      call vp_checkn( np, parm, ipind )

*     start display of results, if you want them
      if(nopchan.gt.0) then
        do iop=1,nopchan
          if(chcv(9).eq.'c26f') then
            write(lt(iop),'(//,a,a,/)') '  ion        z         ',
     :        '      b              N' 
           else
            write(lt(iop),1600)
          end if
        end do
      end if
 1600 format ( //,3x,'ion',10x,'N',12x,'z',12x,' b ',
     :        8x,'bturb',3x,'temp', / )
*
*
      lfirst = .true.
      lrms=.true.
*     MAIN ITERATION REPEATS HERE
 50   continue
*     save previous sum - used as the first point in finding 
*     convergence variable alpha
      sumtot = sumnew
*     put latest into junk.dat - this overwrites previous
      if ( .not. lfirst ) then
        iter=iter+1
*	Current iteration to junk.dat, if requested:
        if(chcv(3).eq.'citn') then
          open ( unit=15, file='junk.dat', status='old', err=1066 )
          go to 1067
 1066     open ( unit=15, file='junk.dat', status='new' )
 1067     continue
        end if
        if(nopchan.eq.0.and.nmonitor.eq.1) then
          write(6,1001) iter,totit,chisqv,sumnew,ndftot
 1001     format( ' iteration: ',i3,' (', i2,' )',/,
     :           ' chi-squared : ',f14.3, ' (',f15.4,', ', i5,' )')
        end if
        if(nopchan.gt.0) then
          do iop=1,nopchan
            write(lt(iop),2600) iter,totit,chisqv,
     :              sumnew,ndftot
 2600       format( ' iteration   : ', i3, ' (', i2, ' )', /,
     :        ' chi-squared : ', f14.3,' (', f15.4, ', ', i5, ' ) ')
            write(lt(iop),'(a)') ' '
          end do
*         print out current iteration parameters
          call vp_ucprinit(ion,level,parm,ipind,np)
          if(nopchan.eq.3) then
            close(unit=15)
          end if
        end if
*       flush the output buffer to log file 
        call flush( 6 )
      end if

*     view file only?
      if (.not.lfirst.and.nopt.ge.9) goto 500

*     testing for delta chisquared - only allow finishing after an even 
*     number of steps if using alternating metrics
      if ( .not. lfirst ) then
        if ( iter .ge. itlim ) goto 500
*       if alwaysLM and more than three attempts, then finish
        if(alwaysLM .and. (nalwaysLM .gt. 3)) goto 500
        if ( .not. ( metric .eq. 2 .and. .not. even ) ) then
          convtst = ( chisqo - chisqv ) / chisqv
          if(chisqv.gt.chsqdnth) then
            chsqdif=chsqdif2
           else
            chsqdif=chsqdif1
          end if
*         Inserted by JAK-------(small mod by RFC)
*         Display LM and GN chisq on screen, and which path is chosen
*         Also write information to fort.18 file, if wanted
          if (iter .ne. 0.and.nopchan.gt.0.and.nverbose.ge.6) then
            if (lastitGNstep) then
              do iop=1,nvbopchan
                write(lt(iop),*) 'Last step: Gauss-Newton'
              end do
             else
              do iop=1,nvbopchan
                write(lt(iop),*) 'Last step: Levenberg-Marquardt ',
     :                  '(lambda: ', LMlambda*100.0D0, ')'
              end do
            end if
            do iop=1,nvbopchan
              write(lt(iop),*) 'GNchisq: ',gnchisqv, ' (', gnsumnew,
     :               ')    LMchisqv: ',lmchisqv, ' (',
     :               lmsumnew, ')'
              write(lt(iop),*) ' '
              write(lt(iop),*) 'chisq now: ', chisqv, ' (',
     :                sumnew, ')    Iteration: ', iter
              write(lt(iop),*) 'Convergence ratio',
     :              ' (will terminate if < 1): ',
     :              convtst/chsqdif
              write(lt(iop),*) ' '
            end do
          end if
*         End JAK insertion --------
*
*         Another JAK alteration below
          if ( convtst.lt.chsqdif.and.abs(convtst).lt.1.0d-10) then
*           If we get to here, it means that the GN method didn't produce any
*           descent at all (not even a small one), and so the method failed.
*           We might as well just switch to the LM method from here on,
*           unless flags have turned this possibility off (JAK)
            nchzero=nchzero+1
            if(nchzero.gt.5.or.(.not.forceLM).or.neverLM) goto 500
            if(nopchan.gt.0) then
              if(nverbose.gt.3) then
                write (6,*) 'ZERO CHANGE IN CHISQ DETECTED',nchzero
              end if
              write (6,*) 'Using Levenberg-Marquardt method hereafter'
            end if
            alwaysLM = .true.
            nalwaysLM=0
          end if
          if (alwaysLM.and.nopchan.gt.0.and.nverbose.gt.2) then
c           User will be informed that the LM method is being used at
c           every iteration (JAK), if alwaysLM is true
            write(6,*) 'Always using Levenberg-Marquardt'
          end if
*         test for end iteration..
          if (convtst.lt.chsqdif.and.convtst.gt.1.0d-10.and.
     :           iter.ge.nminchsqit) then
            nchzero=0
            goto 500
          end if
*         End JAK alteration ---------
*
*         check to see if the file 'stop' has been created, if so 
*         then exit loop
          open(20,file='stop',status='old',err=501)
            close(20)
            if(nopchan.gt.0) then
              do iop=1,nvbopchan
                write(lt(iop),*) ' iterations stopped early'
              end do
            end if
            nchzero=0
*           delete file so that it isn't there next time...
            call system('/bin/rm stop')
            go to 500
 501      continue
          chisqo = chisqv
        end if          
      end if
*
*     compute search direction
      if ( .not. lfirst ) then
*       rfc 25.04.02: modified argument list
*           24.11.06: removed contin, since now in common
        call vp_deriv(parm,ipind,np,grad,hessian,
     :                idiag,nchunk,ion,level,lrms)
*       If any of the diagonal terms of the hessian are zero (not including 
*       tied or fixed parameters - perhaps because one or more z's 
*       have shot out of range), this will cause a crash in UDCHOLE;
*       first see if we can recover with a new set of first guess parameters
        istat=0
        call vp_ucerck(parm,np,istat,ipind,ion)
*       branch out if systems have been omitted
        if(istat.gt.0) goto 998
*       store gradient for later use
        do i=1,np
*          write(18,*) '>>>',i,parm(i),grad(i),nionxt
          sgrad(i)=grad(i)
        end do
*       ajc 17-feb-92  svd replaces udchole if necessary
        nasty = (rlim.gt.0.0d0)
        knasty=0
        do ki=1,np
*         ajc 19-nov-91  altered for any upper case label (secondary
*              systems have zero elements in hessianfit)
          if(hessian(ki,ki).eq.0.0d0.and.varythis(ki,ipind)) then
*           provide some diagnostic information (ktempl was (ki+2)/3)
            ktempl=(ki-1)/noppsys
*           was *(ktempl-1)
            ktempv=ki-noppsys*ktempl
            ktempl=ktempl+1
            if(nopchan.gt.0.or.nmonitor.gt.0) then
              do iop=1,nvbopchan
                write(lt(iop),'('' Hessian diagonal '',i4,
     :                     '' zero'')') ki
                write(lt(iop),'('' Line #'',i3,''  variable #'',i3,
     :                     1h(,a2,a4,1h))') 
     :                     ktempl,ktempv,ion(ktempl),level(ktempl)
              end do
            end if
*           store the line in knasty
            knasty=ktempl
            nasty = .true.
*           set variables so this one will be dropped [RFC 01.05.07]
            nladded=0
            ndrop=1
            kdrop(1)=knasty
            ndroptot=1
            ndrwhy=1
            goto 998
*           end force drop section [RFC 01.05.07]
          end if
        end do
        if(idiag.gt.0.and.nopchan.gt.0) then
          if(ndbranch.eq.0.or.ndbranch.eq.21) then
            do iop=1,nvbopchan
*             write out the hessian matrix (is renormalized in udchole)
              write(lt(iop),*) 'np =',np,' nopt=',nopt,' npts=',npts,
     :              ' isod=',isod
              write(lt(iop),'(/a)') 'Hessian matrix'
              do i=1,np
                write(lt(iop),'(10e9.2)') (hessian(i,j),j=1,np)
              end do
              write(lt(iop),'(/a)') 'Gradient vector'
              write(lt(iop),3500) (grad(i),i=1,np)
            end do
          end if
        end if
*       returns in grad as -ve direction
        if ( nasty ) then
          call svdajc( hessian, grad, np, nopt )
         else
          call udchole(hessian,grad,np,nopt,npts,idiag)
        end if
*
        if(idiag.eq.1.and.nopchan.gt.0) then
          if(ndbranch.ge.20.or.ndbranch.eq.0) then
*           diagnostic printout
            do iop=1,nvbopchan
              write(lt(iop),*) ' '
              write(lt(iop),*) 'nasty=',nasty
              write(lt(iop),*) 'npts =',npts,'  nopt=',nopt
              write(lt(iop),3000)
 3000         format(/' Newton gradient vector')
              write(lt(iop),3500) (grad(i),i=1,np)
 3500         format(3e9.2)
              write(lt(iop),4500)
 4500         format(/' Steepest descent gradient vector')
              write(lt(iop),3500) (sgrad(i),i=1,np)
            end do
          end if
        end if
*       compute step length  -  alpha = 1.0 is quadratic model version
*       option of fixed alpha:
        if(ialfind.eq.1)then
          alpha=1.0d0
          goto 295
        endif
       else
*       rfc 11.12.01: is first, so gradient not known. Set to zero,
*       so that not undefined with 'e' option
        do i=1,np
          grad(i)=0.0d0
        end do
      end if
*
      if ( lfirst ) then
        step = 0.0d0
       else
*       initially newton rather than steepest
        steep = .false.
        final = .false.
*       increasing jump size
        forwrd = .true.
*       lfirst position
        xdat(1) = sumtot
        xcor(1) = 0.0d0
        step = 1.0d0
        jj = 1
      end if

*     Reset LM stuff
      LMcounter=0
      jj=1
*
*     REPEAT here for univariate Gauss-Newton/ LM loop
*     repeat from here for different steps
 130  continue
*     Are we always using LM method? (JAK insert)
      if (alwaysLM) then
        gnchisqv=chisqv
        gnsumnew=sumnew
        doLM = .true.
*       increment alwaysLM counter
        nalwaysLM=nalwaysLM+1
      end if
*     end JAK insert

      if ( .not. lfirst ) then 
        jj = jj + 1
      end if

*     more diagnostic printout, but not if LM
      if(idiag.eq.1.and.nopchan.gt.0.and.(.not.doLM))then
        do iop=1,nvbopchan
          write(lt(iop),*) ' '
          write(lt(iop),*) ' Chi-squared, step length'
          write(lt(iop),*) sumnew,step
        end do
      endif

*     **JAK insert
      if (doLM) then
        step=1.0d0
        do i=1,np
          grad(i)=sgrad(i)
        end do
        call lm_udchole(hessian,grad,np,nopt,npts,idiag,LMlambda)
      end if
*     **end JAK insert
*
*     update parameters as necessary 
*     only unconstrained settings here will be kept - constrained
*     parameters set later
      do n = 1, np
        puni(n) = parm(n)
      end do
      do n = 1, np
        tied=.false.
        jbase=(n-1)/noppsys
        jbase=jbase*noppsys
        j2=jbase+noppsys
        j1=jbase+1
        do j=j1,j2
          if(ipind(j).ne.'  ') then
            tied=.true.
          end if
        end do
*       if straight metric, or straight when alternating, or z, or part of
*       tied system (primary or secondary)
*       JAK looked at this bit, but left it alone
        if(metric.eq.0.or.(metric.eq.2.and. .not. even).or.
     1    (n-jbase.ne.nppcol.and.n-jbase.ne.nppbval).or.tied) then
           puni(n) = puni(n)-step*grad(n)
         else
*         replace n by b+n, b by n-b
*         note the sign change and scaling reflecting the actual changes used
*         (transforming the result back to the separate variables)
          if(n-jbase.eq.nppcol) then
            puni(n) = puni(n)-step*grad(n)
            puni(jbase+nppbval) = puni(jbase+nppbval)- 
     :                step*grad(n)*pardif(jbase+nppbval)/pardif(n)
           else
            puni(n) = puni(n)+step*grad(n)
            puni(jbase+nppcol) = puni(jbase+nppcol) -
     :                step*grad(n)*pardif(jbase+nppcol)/pardif(n)
          end if
        end if
      end do
*     tie any parameters that need it
      call vp_tieval(ion, level, np, puni, ipind, temper,
     1                vturb, atmass, fixedbth )

*     adjustments necessary to keep total
*     column density correct (if total goes too low)
      call vp_checkn( np, puni, ipind )
*     set parameter limits, but not if template intensity
      do n = 1, nfunc
        nn=noppsys*(n-1)
        jnbv=nn+nppbval
        jnc=nn+nppcol
        jnz=nn+nppzed
*       exclude added components (emission flagged, indvar=-1, AD and __) 
*       and linear continuum from range check
        if(ion(n).ne.'AD'.and.ion(n).ne.'__'.and.
     1           ion(n).ne.'<>'.and.ion(n).ne.'>>'.and.
     2           indvar.ne.-1.and.level(n)(4:4).ne.'e') then
*         minimum redshift is -1.0, so force it
          if(puni(jnz).le.-0.999d0) then
            puni(jnz)=-0.999d0
          end if
          if(indvar.eq.1) then
*           log column densities
            if(puni(jnc).lt.clvalmin*scalelog) 
     :                    puni(jnc)=clvalmin*scalelog
            if(puni(jnc).gt.clvalmax*scalelog) then
*             could print a warning here, but not very useful
*             write(6,*)' adjusting column density # ',n,nn
              puni(jnc)=clvalmax*scalelog
            end if
           else
*           linear column densities
            if(puni(jnc).lt.cvalmin*scalefac) 
     :                    puni(jnc)=cvalmin*scalefac
            if(puni(jnc).gt.cvalmax*scalefac) 
     :                    puni(jnc)=cvalmax*scalefac
          end if
         else
          if(indvar.eq.-1.or.level(n)(4:4).eq.'e') then
*           keep emission lines positive
            if(puni(jnc).lt.1.0d-28*scalefac) then
             puni(jnc)=1.0d-28*scalefac
            end if
          end if
*         Zero level constraints on constant term:
          if(ion(n).eq.'__') then
            if(zlevmin.gt.puni(jnc)) puni(jnc)=zlevmin
            if(zlevmax.lt.puni(jnc)) puni(jnc)=zlevmax
          end if 
        end if
*       exclude continuum, shift and zero level variables from 
*       Doppler parameter range check
        if ( ion( n ) .ne. '<<' .and.
     :           ion( n ) .ne. '<>' .and.
     :           ion( n ) .ne. '>>' .and.
     :           ion( n ) .ne. '__' ) then
          jnbv=nn+nppbval
          if(nocgt(ipind(jnbv)(1:1),lastch)) then
*           check that it is positive
            if(puni(jnbv).le.0.0d0) then
              puni(jnbv)=0.0d0
            end if
*           and total bval is in range
*           (don't separate H here as is the lightest element)
            bvtem=vpf_bvalsp(n,puni,ipind)
            if(bvtem.lt.bvalmin) then
*             need to check xref parameter set as well
              if(lcase(ipind(jnbv)(1:1))) then
                jsx=jnbv
               else
*               uppercase, so tied/fixed. mtxref(nset) is base
                jsx=mtxref(jnbv)
              end if
              jxr=mtxref(jsx)
              if(jxr.lt.jsx) then
                jxhi=jsx
               else
                jxhi=jxr
                jxr=jsx
              end if
*             is temperature below max permitted?
              if(parm(jxhi).gt.thmax) then
                parm(jxhi)=thmax
              end if
*             adjust turbulent component, since it is easier
              bvtem=bvalmin**2-parm(jxhi)*thunit/
     :               (60.135795d0*atmass(n))
              if(bvtem.gt.0.0d0) then
                parm(jxr)=sqrt(bvtem)
               else
*               this possibility excluded by above, but does not hurt
                parm(jxr)=0.0d0
              end if
            end if
           else
*           make sure within limits
            if(ion(n).ne.'H ') then
              if(puni(jnbv).lt.bvalmin) then
                puni(jnbv)=bvalmin
              end if
              if(puni(jnbv).gt.bvalmax) then
                puni(jnbv)=bvalmax
              end if
             else
*             special limits for HI, which may be same as above.
              if(puni(jnbv).lt.bvalminh) then
                puni(jnbv)=bvalminh
              end if
              if(puni(jnbv).gt.bvalmaxh) then
                puni(jnbv)=bvalmaxh
              end if
            end if
          end if
        end if
      end do
*     
      if(idiag.ne.0.and.nopchan.gt.0.and.(.not.doLM)) then
*       targeted diagnostics, but not for LM
        if(ndbranch.eq.0.or.ndbranch.eq.22) then
          do iop=1,nvbopchan
            write(lt(iop),459)
 459        format(/' Univariate minimisation; updated parameters'/)
            do ii=1,nfunc
              is=noppsys*(ii-1)
              if(noppsys.le.3) then
                write(lt(iop),*) puni(is+1),puni(is+2),puni(is+3)
               else
                write(lt(iop),*) puni(is+1),puni(is+2),puni(is+3),
     :                             puni(is+4)
              end if
            end do
            write(lt(iop),3512)
 3512       format(/' Chi-squared, step length'/)
          end do
        end if
      endif
      
*     For each chunk, generate the profile and calculate chi-squared
*     minimum Doppler width
*     
*     first get current minimum Doppler parameter - bmincur - so can
*     determine subdivisions needed. 
      bmincur=1.0d30
      do n=1,nfunc
*       skip special cases
        if ( ion( n ) .ne. '<<' .and.
     :       ion( n ) .ne. '<>' .and.
     :       ion( n ) .ne. '>>' .and.
     :       ion( n ) .ne. '__' ) then
          bxcur=vpf_bvalsp(n,puni,ipind)
          bmincur=min(bmincur,bxcur)
          if(nionxt.gt.0) then
*           if any preset ions, need to include them as well
            bmincur=min(bmincur,bvxmin)
          end if
        end if
      end do
      do ichunk=1,nchunk
        ichdum=ichunk
*       set subdivision value from chunk velocity interval near middle
        nchcen=nchlen(ichunk)/2
        velpch=0.5d0*2.99792458d5*
     :          (wavch(nchcen+1,ichunk)-wavch(nchcen-1,ichunk))/
     :           wavch(nchcen,ichunk)
*       want at least bindop bins per Doppler parameter, so min binsize
        dtemp=bmincur/bindop
        nsubd=int(velpch/dtemp+0.9999d0)
        if(nsubd.lt.nminsubd) nsubd=nminsubd
        nsubd=min(nsubd,maxscs/nchlen(ichunk))
        nsubd=min(nsubd,nmaxsubd)
        zed = 0.0d0
*       length of subdivided continuum array:
        klen=nchlen(ichunk)*nsubd
        npsubch=klen
        do k=1,klen
          contpass(k)=1.0d0
          func(k)=1.0d0
          fconv(k)=1.0d0
          kjb=(k-1)/nsubd
          kloc=k-nsubd*kjb
          dtemp=(dble(kloc)-0.5d0)/dble(nsubd)
          wvsubd(k)=(1.0d0-dtemp)*wavchlo(kjb+1,ichunk)+
     :                   dtemp*wavchlo(kjb+2,ichunk)
        end do
*       Any preset extras also included: 
        if(nionxt.gt.0) then
          nppxt=nionxt*noppsys
          do i=1,nionxt
            k=(i-1)*noppsys
            call vp_spvoigte(func,contpass,1,klen,wvsubd,npsubch,
     :        parmxt,ipinxt,elmxt,ionxt,i,nppxt,ichdum,fconv)
            do k=1,klen
              contpass(k)=func(k)
            end do
          end do
        end if
*       Now the sytems where the parameters vary:
        do n=1,nfunc
*         Include only if chunk number matches or is zero 
          if ( linchk( n ) .eq. 0 .or.
     :             linchk( n ) .eq. ichunk ) then
            call vp_spvoigte(func,contpass,1,klen,wvsubd,npsubch,
     :            puni,ipind,ion,level,n,np,ichdum,fconv)
            do k=1,klen
              contpass(k)=func(k)
            end do
          end if
        end do
*
*       Convolve func with instrument profile & rebin
*       fconv is subbin convolution; phi is binned as for original data
*       =fconv rebinned
        call vp_chipconv(func,klen,nsubd,ichdum, fconv,phi)
*     
*       calculate chi-squared on a chunk by chunk basis
        sumnew = 0.0d0
        do k=kchstrt(ichunk),kchend(ichunk)
*         ignore flagged bad points - either sigma or rms .le. 0
          if (derch(k,ichunk).gt.0.0d0.and.
     :          drmch(k,ichunk).gt.0.0d0)  then
            lkoffs=kchstrt(ichunk)-1
            kphi=k-lkoffs
            sumnew=sumnew+
     :          (phi(kphi,ichunk)-dach(k,ichunk))**2/drmch(k,ichunk)
*           only count points once
            if ( lfirst ) then
              npts = npts + 1
            end if
          end if
        end do
        chsq(ichunk) = sumnew
      end do
*     
      sumnew = 0.0d0
      do ichunk = 1, nchunk
        sumnew = sumnew+chsq(ichunk)
      end do
*     calculate predicted chi-squared from constraints
*      call chicon( np, puni, chi27, ndf27 )
*      sumnew = sumnew + chi27

*     more diagnostics
      if(idiag.eq.1.and.nopchan.gt.0)then
        do iop=1,nvbopchan
          write(lt(iop),*) sumnew,step
        end do
      endif

*     if first pass, have now calculated chi-squared - restart
      if ( lfirst ) then 
*       number of degrees of freedom
        ntps=0
        if(nopt.gt.0) then
          do n=1,nfunc
            nn=noppsys*(n-1)
*           ntps is incremented for upper case (tied or
*           fixed) values, and not free ones
            do jn=1,noppsys
              if(.not.varythis(nn+jn,ipind)) ntps=ntps+1
            end do
          end do
        endif
*	ndftot = npts - np + ntps + ndf27
        ndftot = npts - np + ntps
        chisqv = sumnew / dble( ndftot )
        chisqo = chisqv*(1.0d0 + 2.0d0*max(chsqdif1,chsqdif2))
        lfirst = .false.
        go to 50
      end if

*ccccccccccccccccccccccc
*     JAK - stuff below here updates the step size based on 
*     certain values of chisq. 
*cccccccccccccccccccccc
*     initial jump is 1 - as expected from algebra
*     if this is downhill (forwrd) then jump further until uphill
*     otherwise try smaller jumps for a while,
*     then resort to steepest descent
*     either jump to 130 - another attempt
*                    295 - finished
      xcor(jj)=step
      xdat(jj)=sumnew

*     ***Decide what to do based upon change in chisq***
*     (We don't need to do the line minimisation if we're
*     taking the LM path)
*     ***Take the Gauss-newton path***
      if (.not. doLM) then
*       check for final evaluation before anything else
        if(final) then
          final=.false.
*         save current point
          sumlow = sumnew
          lowpnt = jj
          steplow = step
*         check all other steps to make sure lowest is used
          do k = 1, jj-1
            if((sumlow-xdat(k))/sumlow.gt.chsqdlim1) then
              sumlow=xdat(k)
              lowpnt=k
              steplow=xcor(k)
            end if
          end do
*         now is the lowest point still the final one?
          if(lowpnt.eq.jj) then
*           yes - finish
            alpha = step
            go to 295
           else
*           no - print warning if in verbose mode, swap current    
*           with lowest, and re-evaluate at lowest point
            if(verbose.and.nopchan.gt.0) then
              do iop=1,nvbopchan
                write(lt(iop),*) ' interpolated minimum failed'
                write(lt(iop),*) ' initial : ', xdat(1)
                write(lt(iop),*) ' final   : ', xcor( jj ),
     :                                          xdat( jj )
                write(lt(iop),*) ' minimum : ', xcor( lowpnt ),
     :                                          xdat( lowpnt )
              end do
            end if
*           swap points
            dummy = xdat( jj ) 
            xdat( jj ) = xdat( lowpnt )
            xdat( lowpnt ) = dummy
            dummy = xcor( jj )
            xcor( jj ) = xcor( lowpnt )
            xcor( lowpnt ) = dummy
*           and run through again (so that hessian is correct)
            jj = jj - 1
            step = steplow
            final = .true.
            go to 130
          end if
        end if
*       first of all, are we doing newton rather than steepest descent?
        if( .not. steep ) then
*         increasing step size?
          if ( forwrd ) then
*           hit an up-turn?
            if ( sumnew .gt. xdat( jj - 1 ) ) then
*             if not the first, then a low point exists - good!
              if (jj .gt. 2) then
                call vp_minpol( jj, xcor, xdat, step )
                final = .true.
                go to 130
               else
*               otherwise, try smaller step
                forwrd = .false.
                step = step/2.0d0
                goto 130
              end if
             else
*             haven't found an up-turn yet - use bigger step if room
              if ( jj .lt. jjmax ) then
                step = step*2.0d0
                goto 130
               else
                 call vp_minpol( jj, xcor, xdat, step )
                 final = .true.
                 goto 130
              end if
            end if
           else
*           decreasing step size - found a dip?
            if ( sumnew .lt. xdat( 1 ) ) then
              call vp_minpol( jj, xcor, xdat, step )
              final = .true.
              go to 130
             else
*             smaller step still, if room remains
              if ( jj .lt. jjmax ) then
                step = step / 2.0d0
                go to 130
               else
*               otherwise, have to re-try with steepest descent
                steep = .true.
                if(nverbose.ge.6) then
                  write (6,*) ' trying steepest descent...'
                end if
                do i=1,np
                  grad(i)=sgrad(i)
                end do
                ratio=0.0d0
                nratio=0
                do i=1,np
                  if(parm(i).ne.0.0d0) then
                    nratio=nratio+1
                    ratio=ratio+abs(grad(i)/parm(i))
                  end if
                end do
                ratio=ratio/dble(nratio)
                step = 1.0d-3 / ratio
                jj = 1
                go to 130
              end if
            end if
          end if
*         must be steepest descent - try three times 
*         then calculate minimum
         else
*         have we measured enough points?
          if ( jj .eq. 4 ) then
*           fit to log of x coordinate for equal spacing
            do i = 1, jj
              lxcor( i ) = log10( xcor( i ) + 1d-30 )
            end do
            call vp_minpol( jj, lxcor, xdat, step )
*           convert from log to normal
            step = 10.0d0 ** step
            final = .true.
            go to 130
*           try a smaller step
           else
            step = step / 1.0d3
            if((nopchan.gt.0.or.nmonitor.gt.0)
     :                 .and.nverbose.ge.6) then
              write (6, * ) ' smaller step...'
            end if
            go to 130
          end if
        end if
      end if
*
*     If we are here, end taking the GN path      
*     ****LM DECISION TREE FOLLOWS***
      if(doLM) then
*       Is the next point higher? 
*       RFC 25.03.07: Don't increase lambda beyond 1D7
        if ( sumnew .gt. sumtot
     :         .and. LMlambda.lt.2.0d6 ) then
*         Update lambda and go to next iteration 
*         IF we have more iterations left
          if (LMcounter .lt. LMmaxit) then
            LMlambda=LMlambda*10.d0
            if(nverbose.ge.8) then
              write(6,*) 'Increasing lambda by 10 to',LMlambda
              write(6,*) 'sumnew=',sumnew,'  sumtot=',sumtot
            end if
            LMcounter=LMcounter+1
            goto 130
           else
c           No more iterations left
            goto 295
          end if
         else
c         Found a lower point! Copy new parameter values
c         NOTE: using modified form -- divide by 100. That is, 
c         force a return to much closer to the GN step. At worst, 
c         we'll only need an additional increase in lambda to 
c         get to a point that decreases.
          LMlambda=LMlambda/100.d0
          if(nverbose.ge.8) then
            write(6,*) 'Decreasing lambda by 100 to',LMlambda
            write(6,*) 'sumnew=',sumnew,'  sumtot=',sumtot
          end if
          do i=1,np
            LMparm(i)=puni(i)
          end do
c         Set chisqv for comparison & finish
          lmchisqv = sumnew / dble( ndftot )
          lmsumnew = sumnew
*          LMfinish = .true.
          goto 295
        end if
      end if
c     ****UNIVARIATE MINIMISATION COMPLETE***

 295  continue
*
      if(nopchan.gt.0.and.nverbose.ge.3) then
        do iop=1,nvbopchan
          write(lt(iop),*)'alpha: ',alpha,' evaluations: ',jj
        end do
        if(nverbose.ge.9) then
          do iop=1,nvbopchan
            write(lt(iop),*)'metric: ',metric
            write(lt(iop),*)' '
          end do
        end if
      end if
*     Did we just to a G-N or L-M step?
*     above makes evaluation at 'minimum' part of previous loop
      if(.not.doLM) then
c       Store good parameters from the univariate minimisation
c       parm( n ) = puni( n ) <-- was this previously
c       Keep good GN parameters for after LM
        do n=1,np
          GNparm(n) = puni(n)
        end do
*            
*       Keep chisqv from GN for after LM.
        gnchisqv = sumnew / dble( ndftot )
        gnsumnew = sumnew
*       previously had
*       do n = 1, np
*         parm( n ) = puni( n )
*       end do
*       chisqv = sumnew / dble( ndftot )
*
*       alternate flag
        even = .not. even
      end if
*     Univariate minimisation complete. All variables stored; now
*     go and do LM minimisation, unless option excluded
*     CHECK NO INFINITE LOOP HERE. Nope, just does not iterate!
      if(neverLM) goto 50
      if (.not. doLM) then 
c       Reset parameters
        doLM = .true.
        steep = .false.
        final = .false.
        forwrd = .true.
        xdat(1) = sumtot
        xcor(1) = 0.0d0
        step = 1.0d0
        jj = 1
        LMcounter=0
        if(nopchan.gt.0.and.nverbose.ge.8) then
          write (6,*) 'GN branch finished. Starting LM branch...'
        end if
        goto 130
      end if
c     If we get here, must be doing LM minimisation -- 
*     compare two chisqv values
      doLM = .false.
      steep = .false.
      final = .false.
      forwrd = .true.
      xdat(1) = sumtot
      xcor(1) = 0.0d0
      step = 1.0d0
      jj = 1
      LMcounter=0
*---  JAK insert 15/05/07
c     Also check that the LM method didn't give rubbish for some reason.
c     This usually occurs when the whole fit is "bad", and nasty gets
c     turned on. If this is the case, simply go to line 500
c     and vpfit.f should restart the whole run, avoiding the problem.
      if(nverbose.ge.4) then
        write(6,*) 'gnchisqv: ',gnchisqv, '  lmchisqv: ',lmchisqv
      end if
      if (lmchisqv .ne. lmchisqv) then
        write(6,*) 'WARNING: lmchisqv is NaN. Restarting.'
c       Force GN decision path by making LM numbers really big
        lmchisqv=1.0d5
        lmsumnew=1.0d6
        LMlambda=1.0d-4
        goto 500
      end if
      if (lmchisqv .lt. 0.001d0) then
c       Some other failure of LM method. In particular, the 
c       method has returned a chisq of zero or a negative number.
c       Again, this only seems to happen with bad fits.
        write(6,*) 'WARNING: lmchisqv too small. Restarting.'
c       Force GN decision path by making LM numbers really big
        lmchisqv=1.0d5
        lmsumnew=1.0d6
        LMlambda=1.0d-4
        goto 500
      end if
*---
      write (6,*) ' '
      if (.not. alwaysLM) then 
        if (gnchisqv .lt. lmchisqv) then
          do n=1,np
            parm(n)=GNparm(n)
          end do
          chisqv = gnchisqv
          sumnew = gnsumnew
          lastitGNstep = .true.
         else
          do n=1,np
            parm(n)=LMparm(n)
          end do
          chisqv = lmchisqv
          sumnew = lmsumnew
          lastitGNstep = .false.
        end if
       else
        do n=1,np
          parm(n)=LMparm(n)
        end do
        chisqv = lmchisqv
        sumnew = lmsumnew
        lastitGNstep = .false.
      end if

c     NEXT LOWER POINT FOUND: GOTO MAIN ITERATION
      goto 50


*     loop exits to here
c     **************SOLUTION REACHED**************
 500  continue
      nchzero=0
      ndcount = 0
      do ichunk=1,nchunk
*       only fitted region - not extra - is displayed for fit
        lkoffs=kchstrt(ichunk)-1
        do k=1,nchlen(ichunk)
          if ( k - lkoffs .gt. 0 .and. 
     :           k .le. kchend(ichunk) ) then
            fitret(k,ichunk) = phi(k-lkoffs,ichunk)
           else
*           was contin, so need an offset here
*           but this is only for display, so don't bother
            fitret(k,ichunk) = dcch(k,ichunk)
          end if
        end do

*       calculate residuals more explicitly for final display
*       ndcount counts all entries, olddct marks start of this chunk
*       KS test against rms as well
        olddct = ndcount + 1
        do k=kchstrt(ichunk),kchend(ichunk)
          if(derch(k,ichunk).gt.0.0d0.and.
     :       drmch(k,ichunk).gt.0.0d0) then
            ndcount=ndcount+1
*           save normalised value for ks test, absolute value 
*           for maxdev test
            kphi=k-nrextend
*           drmch is rms^2, not 1/rms^2, so / not *
            f(ndcount,1) = dble(phi(kphi,ichunk)-dach(k,ichunk))/ 
     :            dble(sqrt(drmch(k,ichunk)))
            f(ndcount,2) = dble(dach(k,ichunk))
            f(ndcount,3) = dble(drmch(k,ichunk))
            f(ndcount,4) = dble(phi(kphi,ichunk))
            f(ndcount,5) = dble(k)
            f(ndcount,6)=wavch(k,ichunk)
          end if
        end do

*       use maxdev test(s)
        if ( ndcount - olddct .gt. 5 ) then
          call tstfit3d(ndcount-olddct+1,f(olddct,2),
     :            f(olddct,3),f(olddct,4),0,  pbgdev,dks(ichunk))
*         return probability for later
          prbks( ichunk ) = pbgdev
         else
*         insufficient data
*         -ve probability to signal no statistics to line insertion algorithm
          prbks( ichunk ) = -1.0d0
        end if
      end do

*     overall probability from ks-test
*     rfc 11.11.98: NAG routine g08cbf replacement:
      call vp_ksvnorm(ndcount,f(1,1),dtot,kstot,f(1,8),ifail)
*     write(6,*) 'NEW: Dtot ',dtot,'   KStot ',kstot
*
*     overall probability from runs test
*     sort by wavelength beforehand for extra resolution
*     rfc 29.07.97 replace NAG call by pda routine call
*     pda=public domain routines, so no distribution problems
      call pda_qsiad(ndcount,f(1,6),rank)
      do i = 1, 6
        call pda_rinpd(rank,ndcount,f(1,i),ifail)
      end do
      call vp_runstst(f(1,2),f(1,4),ndcount,nplus,npair,nrun,
     :               prun,ifail)
*     write(6,*) 'NEW RUNS:', ndcount,npair,nplus,prun


*     skip errors calculation for option 9
      if ( nopt .ne. 9 ) then
*       compute error function unless some systems have been thrown out
        call vp_ucerck(parm,np,istat,ipind,ion)
        if(istat.gt.0) goto 998
*       force normal metric
        oldmet = metric
        if ( nasty.and.nopchan.gt.0) then
          do iop=1,nvbopchan
            write(lt(iop),*)' badly conditioned matrix: no errors'
          end do
         else
          metric = 0
*         rfc 18.10.05: modified argument list 24.11.06 removed contin
          lrms=.false.
          call vp_deriv(parm,ipind,np,grad,hessian,
     :                 idiag,nchunk,ion,level,lrms)
          lrms=.true.
          call dcholin( hessian, bb, np, nopt )
          metric = oldmet
        end if
      end if

      do k=1,np
        if ( nasty ) then
          parerr( k ) = -1.0d0
         else
*         Multiplying by chisqv inappropriate if the error estimates are OK
          if(lrsparmerr.and.(.not.luvespop)) then
            tempp=chisqv*bb(k,k)
           else
            tempp=bb(k,k)
          end if
*         smultd removed 30.11.06. Now handled by drms/de adjustment
          parerr(k)=sqrt(abs(tempp))
        end if
      end do

*     probabilities
      ifail=0
      prtot=pd_pchi2( sumnew, ndftot, ifail)
*
*     rfc 18.12.95 : Replace NAG calls by gaussian approximation
*     to chi^2 (sqrt(2*chi^2) has mean sqrt(2*ndftot-1), variance 1)
*     cs1t=real( G01CCF(0.68d0,ndftot,ifail) )
      cs1t=0.5d0*(sqrt(2.0d0*dble(ndftot)-1.0d0)+0.47d0)**2
      cs2t=0.5d0*(sqrt(2.0d0*dble(ndftot)-1.0d0)+1.645d0)**2
      cs3t=0.5d0*(sqrt(2.0d0*dble(ndftot)-1.0d0)+2.33d0)**2

      if(nopt.ne.9) then
*       common variables for results record
*       printed by vp_smry (called from vpfit)
        iterc=iter
        nptsc=npts
*        ndftotc=ndftot+ndf27
        ndftotc=ndftot
        prtotc=prtot
        chisqvc=chisqv

        if(chcv(2)(1:4).eq.'wr25') then
*         write out hessian matrix, if wanted
*          call vp_wrt25(np,ndftot+ndf27,hessian,sumnew)
          call vp_wrt25(np,ndftot,hessian,sumnew)
        end if
*       write implied constraints - not now needed
*        call wrt24(np,parm)
      end if

*     write out errors
      if(nopchan.gt.0) then
        do iop=1,nvbopchan
          if(chcv(9).eq.'c26f') then
            if(lrsparmerr.and.(.not.luvespop)) then
              write(lt(iop),'('' Parameters & rescaled errors:'')')
             else
              write(lt(iop),'('' Parameters & error estimates:'')')
            end if
           else
            if(lrsparmerr.and.(.not.luvespop)) then
              write(lt(iop),'('' Rescaled parameter errors:'')')
             else
              write(lt(iop),'('' Parameter errors:'')')
            end if
          end if
        end do
        if(nverbose.ge.3) then
          if(doLM) then
            write(6,*) 'Last step: LM'
           else
            write(6,*) 'Last step: GN'
          end if
        end if
        call vp_ucprinerr(ion,level,parerr,parm,ipind,np)
      end if
*
*     emission line summary, if appropriate:
      if(indvar.eq.-1) then
        call vp_emlsmry(lt,nopchan,ion,level,
     :                      parm,parerr,np)
      end if

*     drop large error systems if requested
      call vp_errej(parm,parerr,np,istat)

*     stats for whole fit
      if(nopchan.gt.0.or.nmonitor.eq.1) then
        do iop=1,max(nvbopchan,nmonitor)
          write(lt(iop),2227)
          write(lt(iop),2228)
          if(kstot.ge.1.0d-4) then
            write(lt(iop),2229) prun,kstot,sumnew,npts,ndftot,
     :                 prtot, cs1t, cs2t, cs3t
           else
            write(lt(iop),
     :     '(3x,f7.5,1x,1pe10.2,1x,0pf8.2,2x,i6,i5,3x,f5.3,3f8.2,/)')
     :        prun,kstot,sumnew,npts,ndftot,prtot,cs1t,cs2t,cs3t
          end if
        end do
      end if
 2227 format(/,' statistics for whole fit:')
 2228 format('  Runs test  K-S test Chi-squared  Chans ndf   APr',
     :'   Xp(.68) Xp(.95) Xp(.99)')
 2229 format(3x,f7.5,3x,f7.5,2x,f8.2,2x,i6,i5,3x,f5.3,3f8.2,/)
*
*     ndf27 ALWAYS zero, so drop this
*
*     write out constraints from fort.27 if used
*      if ( ndf27 .gt. 0.and.nopchan.gt.0 ) then
*        probacc = pd_pchi2(chi27,ndf27,ifail)
*       rfc 18.12.95 : Replace NAG calls by gaussian approximation
*	to chi^2 (sqrt(2*chi^2) has mean sqrt(2*ndftot-1), variance 1)
*        cs1sig=0.5d0*(sqrt(2.0d0*dble(ndf27)-1.0d0)+0.47d0)**2
*        cs2sig=0.5d0*(sqrt(2.0d0*dble(ndf27)-1.0d0)+1.645d0)**2
*        cs3sig=0.5d0*(sqrt(2.0d0*dble(ndf27)-1.0d0)+2.33d0)**2
*        do iop=1,nvbopchan
*          write( lt(iop),*) ' statistics for fort.27 and data : '
*          write(lt(iop),*) '                      Chi-squared'//
*     :                    '        ndf   APr   Xp(.68)'//
*     :                    ' Xp(.95) Xp(.99)'
*          write(lt(iop),'(22x,f8.2,8x,i5,3x,f5.3,3(f8.2))')
*     :          chi27, ndf27, probacc, cs1sig, cs2sig, cs3sig
*        end do
*
*        probacc = pd_pchi2( sumnew - chi27, 
*     :                    ndftot - ndf27, ifail )
*       rfc 18.12.95: NAG G01CCF replaced by approximation:
*       cs1sig=0.5d0*
*     :         (sqrt(2.0d0*dble(ndftot-ndf27)-1.0d0)+0.47d0)**2 
*        cs2sig=0.5d0* 
*     :         (sqrt(2.0d0*dble(ndftot-ndf27)-1.0d0)+1.645d0)**2 
*        cs3sig =0.5d0* 
*     :         (sqrt(2.0d0*dble(ndftot-ndf27)-1.0d0)+2.33d0)**2 
*
*        do iop=1,nvbopchan
*          write(lt(iop),'(22x,f8.2,8x,i5,3x,f5.3,3(f8.2),/)')
*     :          sumnew - chi27, ndftot - ndf27,
*     :          probacc, cs1sig, cs2sig, cs3sig
*        end  do
*      end if

*     next section works out how many parameters associated with each
*     region, hence no. of degrees of freedom for chi-squared test
      do k=1,nchunk
        npftr(k)=0
        npfmax(k)=0
      end do
      do j=1,nfunc
        jj=noppsys*(j-1)
        do i=1,nz
          if(lbz(i).eq.ion(j).and.lzz(i).eq.level(j))then
            do ichunk=1,nchunk
              wtst=dble((parm(jj+nppzed)+1.0d0)*alm(i))
              dtemp=dble(ichstrt(ichunk))
              vacst=vp_wval(dtemp,ichunk)
              dtemp=dble(ichend(ichunk))
              vacend=vp_wval(dtemp,ichunk)
              if(wtst.ge.vacst.and.wtst.le.vacend) then
                npftr(ichunk)=npftr(ichunk)+noppsys
                npfmax(ichunk)=npfmax(ichunk)+noppsys
                if(nopt.gt.0)then
                  if(ipind(jj+nppzed).ne.'  ') then
                    npftr(ichunk)=npftr(ichunk)-1
                  end if
                  if(ipind(jj+nppbval).ne.'  ') then
                    npftr(ichunk)=npftr(ichunk)-1
                  end if
                end if
              end if
            end do
          end if
        end do
      end do

*     statistics for individual chunks
      if(nopchan.ge.0.or.nmonitor.eq.1) then
*       writes info even if nopchan=0; negative suppresses it
        do iop=1,max(nvbopchan,nmonitor,1)
          write(lt(iop),'('' Statistics for each region :'')')
          write(lt(iop),2223)
        end do
      end if
 2223 format(1x,'   Start      End    Chi-squared  Chans df?')
*     :   APr   Xp(.68) Xp(.95) Xp(.99)')

      ifail=0
      ibck=0
      prks=1.0d0

      do ichunk=1,nchunk
        wstxx=vp_wval(1.0d0+dnshft2(ichunk),ichunk)
        wendxx=vp_wval(dble(numpts(ichunk))+dnshft2(ichunk),
     :                        ichunk)
        ndf=ndch(ichunk)-npftr(ichunk)
        if(ndf.le.0) then
          ndf=1
        end if
        probacc=pd_pchi2(chsq(ichunk),ndf,ifail)
*	rfc 15.8.95 Realistic lower and upper limits to the probabilities
*	for each chunk: Lower if all constraints from this chunk, and all
*	parameters free; Upper if all constraints elsewhere. g is the
*	guess if all unconstrained b & z determined by this chunk.
        probmin=pd_pchi2(chsq(ichunk),
     :       max0(1,ndch(ichunk)-npfmax(ichunk)),ifail)
        probmax=pd_pchi2(chsq(ichunk),
     :       max0(1,ndch(ichunk)),ifail)
        probchk(ichunk)=probmax
        nprchk(ichunk)=ndch(ichunk)
        if(nopchan.ge.0) then
          do iop=1,max(nvbopchan,1)
            write(lt(iop),1992) wstxx,wendxx,chsq(ichunk),
     :            ndch(ichunk),ndf,probmin,probmax,probacc,ichunk
            write(lt(iop),1902) wstxx,dks(ichunk),prbks(ichunk)
          end do
        end if
 1992   format(3(2x,f8.2),2x,i6,i5,3x,f5.3,' < Prob < ',f5.3,
     :            ' g= ',f5.3,i4)
 1902   format (2x,f8.2,'   maxdev     ',f8.4,14x,f5.3,
     :         '   0.956   1.358   1.627',/)

        call flush( 6 )

*       RFC 19.3.97: store KS worst fit: prbks emerges from tstfit3d  
*       (ajc, so I don't understand it) and is not consistent with 
* 	kstot. Try using highest chisq estimate per chunk instead, 
*	so replace prbks(ichunk) by probmax.
        if ( probmax .lt. prks .and.
     :         probmax .ge. 0.0d0 ) then
          ibck=ichunk
          prks=probmax
        end if
      end do
*     Make sure the file 'stop' does not exist here, or the
*     next iteration may be terminated straight away, which
*     can be a pain.
      open (20,file='stop',status='old',err=591)
      close (20)
*     delete file
      call system( '/bin/rm stop' )
*
*     Add/remove lines?
 591  continue
*
*     rfc 12.09.00: Chi-squared threshold end now included
      prtotx=prtot
      if(probchb.gt.1.0d0) then
*	change variables so uses normalized chi-squared threshold
*	easiest to do the test here
        if(chisqv.gt.probchb) then
*	  still need to add lines, so change prtot to less than probchb
          prtotx=0.999d0
         else
*	  chisquared condition satisfied, so make prtotx big
          prtotx=1.0d20
        end if
      end if
*     Converged previously, so branch out
      if(literok) goto 998
*     check probabilities, and that the fit is not getting worse!
*     If the last thing added was a continuum, don't stop.
      if ( (prtotx .lt. probchb .or. kstot .lt. probksb ) .and.
     :    (chisqv.le.chisqvh.or.lastaddion.eq.'<>').and.ibck.gt.0) then
*       should a continuum flag be added?
        lcadd=.false.
        if(lcontv) then
*	  check if appropriate to vary continuum, do it and
*	  set lcadd to .true. in the subroutine
          call vp_scontf(lcadd,chisqv,ibck,parm,ipind,np,nfunc,
     :               ion,level,wvregion)
        end if
*       add a line in the worst region
*       and try to establish a new fit with the extra line
*       in the actual (rather than the extended) data area
        if(lcadd) then
          write(6,'(a,i4,a,f10.2)') 
     :           'Continuum parameters inserted, region ',
     :           ibck,'   wcentral: ',wvregion
         else
*	  call vp_addline(ibck,contpass,zed,bval,aln,
*     :       data,error,contin,    ! now common, &
*     :       dach,derch,dcch, .... ! these are in common, becomes:
          call vp_addline(ibck,contpass,zed,bval,aln,
     :       phi,
     :       ion,level,parm,ipind,np,nfunc,chisqv)
*     :  WAS     phi,ndch(ibck),npexsm,
*     :       ion,level,parm,ipind,np,nfunc,chisqv)
        end if
        if(nladded.gt.0.and.np.lt.maxnpa) then
*	  parameter change moved to within vp_addline
*         update chisqvh
          chisqvh=chisqv
        end if
      end if
 998  continue
      return
      end

