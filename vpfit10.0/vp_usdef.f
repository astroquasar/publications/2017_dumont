      subroutine vp_usdef
*     print out a header
      common/vpc_uscont/indus
      common/vpch_uscont/indush
*     readin workspace
      character*132 inchstr
      character*60 cvstr(24)
      integer ivstr(24)
      real rvstr(24)
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
*
      indus=1
      indush=1
      call getenv('USER',inchstr)
*     copyright message to all but the copyright holders!
      if(inchstr(1:3).ne.'rfc'.and.inchstr(1:3).ne.'jkw'.and.
     :     inchstr(1:3).ne.'mr '.and.inchstr(1:3).ne.'mim') then
        write(6,*) '(C) 2009 R.F.Carswell, J.K. Webb'
        write(6,*) ' .. please acknowledge the use of this program '
        write(6,*) ' (and the authorship) in resulting publications.'
      end if
      return
      end
