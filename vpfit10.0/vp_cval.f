      subroutine vp_wvacurs(vacwav)
*     return vacuum wavelength from cursor position
*
      implicit none
      double precision vacwav
*     LOCAL:
      double precision wavair
*     FUNCTIONS:
      double precision wcor
      character*1 chc
*     Common:
      integer ipgopen,ipgflag
      common/pgplotv/ipgopen,ipgflag
      double precision avfac
      common/vpc_airvac/avfac
*
      real xapg,yapg         ! pgplot variables
      common/vpc_gcurv/xapg,yapg
*
      write(6,*)' Put cursor on line centre and hit spacebar'
      if(ipgopen.eq.1)then
        xapg=real(vacwav)
        call vp_pgcurs(xapg,yapg,chc)
        vacwav=dble(xapg)
        vacwav=vacwav*wcor(vacwav)
       else
        if(avfac.gt.1.0d0) then
          write(6,'('' Air wavelength?'')')
          write(6,'(''> '',$)')
          read(5,'(f10.0)') wavair
*         convert to vacuum wavelength
          vacwav=wavair*wcor(wavair)
         else
          write(6,'('' Vacuum wavelength?'')')
          write(6,'(''> '',$)')
          read(5,'(f10.0)') wavair
          vacwav=wavair
        end if
      end if
      return
      end
 
      subroutine cvalset(nopt,istch)
*
*     set variables for the fitting procedure
*
      implicit none
      include 'vp_sizes.f'
*     Subroutine arguments
      integer nopt,istch
*
*     LOCAL:
      character*1 icx
      character*1 stch
      character*120 instr
      character*60 cvstr(6)
      integer nvstr
      integer ivstr(6)
      double precision dvstr(6)
      double precision scl
      double precision temp
c
*     ajc 25-nov-91  alternate metric
      integer metric
      logical even
      common / metric / metric, even
      integer nminchsqit
      double precision cdstsf,zstep,bstep,chsqdif1,chsqdnth
      double precision chsqdif2
      common/vpc_jkw2/cdstsf,zstep,bstep,chsqdif1,chsqdnth,
     :       chsqdif2,nminchsqit
*     iteration parameter limits
      double precision chsqdlim1
      common/vpc_jkw2lim/chsqdlim1
      double precision step4,step5
      common/vpc_xvstep/step4,step5
      integer isod,isodh
      character*2 ipind(maxnpa)
      common/vpc_usoind/ipind,isod,isodh
*     log or linear variable indicator
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar

*     ajc 3-oct-93  number of sub-bins
      integer nexpd
      common / nexpd / nexpd
*     ajc 25-oct-93 rejection limit for svd
      double precision rlim
      common /vpc_svd / rlim
      logical verbose
      common/vp_sysout/verbose
*
*     general control variables
      character*4 chcv(10)
      common/vpc_chcv/chcv

*
*     min, max b, and conditions for automatic drop systems
      double precision bvalmin,bvalmax,bltdrop,bgtdrop,colltdrop,
     :                     clogltdrop,cvaldrop,clvaldrop,cvalmax,
     :                     clvalmax,bvalminh,bvalmaxh,clvalmin,
     :                     cvalmin
      common/vpc_bvallims/bvalmin,bvalmax,bltdrop,bgtdrop,colltdrop,
     :                     clogltdrop,cvaldrop,clvaldrop,cvalmax,
     :                     clvalmax,bvalminh,bvalmaxh,clvalmin,
     :                     cvalmin
*
      integer nptype
      double precision csiglfac
      common/vpc_nptype/csiglfac,nptype
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     chunk subdivision variables
      integer nminsubd,nmaxsubd
      double precision bindop
      common/vpc_vpsubd/bindop,nminsubd,nmaxsubd
*     rest wavelength stuff. Action may depend on lqmucf
      logical lqmucf,lchvsqmu
      double precision qmucf
      double precision qscale
      common/vpc_qmucf/qmucf(maxats),qscale,lqmucf,lchvsqmu
*     log or linear Doppler parameters ibvar=1 for logb, 0 linear
*     redshift z: 0-norm, 1-log(1+z), 2 velocity bins
      integer ibdvar,izdvar
      common/vp_bzstyle/ibdvar,izdvar
*     finite difference default stepsizes (used in cvalset)
      double precision cdsteplogh,cdsteplinh,zsteph,bsteph
      double precision step4h,step5h
      common/vpc_fdsteps/cdsteplogh,cdsteplinh,zsteph,bsteph,
     1   step4h,step5h  
*
*     set up default, or otherwise, parameter derivative details
      if(indvar.eq.1) then
        cdstsf=cdsteplogh
       else
        cdstsf=cdsteplinh
      end if
*     ajc 25-nov-91  initially an odd loop
      even = .false.
      zstep=zsteph
      bstep=bsteph
*     supplementary variables for higher order functions
      step4=step4h
      step5=step5h
*     rfc 17.3.98: options except for continue as before (nopt=8)
 533  if (nopt.ne.8) then

*       change options ('p' and 'v' effective for nopt.ge.9)
*
        write (6,*)
        if(chcv(4)(1:4).eq.'    ') then
*         suppress option list 
          isod=1
          isodh=1
*         and branch out of this lot
          goto 903
        end if

        if(nopt.lt.9) then
          write(6,*) ' setup: ? print values, <CR> OK, n,z,'//
     :                 'b,x4,cs,sf,il,w,me,p,d,v to change'
         else
          write(6,*) ' setup: ? list values, <CR> OK, p,v to change'
        end if
        write(6,'(''> '',$)')
        read(5,'(a)') instr
        write (*,*) 
        call dsepvar(instr,3,dvstr,ivstr,cvstr,nvstr)
        stch=cvstr(1)(1:1)
        if(stch.eq.'?') then
*         give list of options and values
          if(nopt.lt.8) then
            if(noppsys.le.3) then
              if ( indvar .eq. 0 ) then
                write (*,*) ' finite difference intervals   '//
     :             '   N x         z +         b +'
               else
                write (*,*) ' finite difference intervals   '//
     :             'log( N ) +     z +         b +'
              end if
              write (*,'(33x,f5.3,5x,f8.6,6x,f4.2 )') 
     :              cdstsf, zstep, bstep
             else
*             extra variable if set up for it
              if ( indvar .eq. 0 ) then
                write (*,*) ' finite difference intervals   '//
     :             '   N x         z +         b +     x4 +'
               else
                write (*,*) ' finite difference intervals   '//
     :             'log( N ) +     z +         b +     x4+'
              end if
              write(6,*) 'qscale:',qscale
              temp=step4*qscale
              write (*,'(33x,f5.3,5x,f8.6,6x,f4.2,3x,1pe12.3 )') 
     :              cdstsf, zstep, bstep, temp
            end if
            write ( *, * )
            write ( *, '( 1x, a, f10.7,1x,f8.2,1x,f10.7 )' ) 
     :         'relative chi-squared decrement for stopping (cs) ', 
     :                           chsqdif1,chsqdnth,chsqdif2
            write ( *, * )
            if ( istch .eq. 1 ) then
              write ( *, * ) ' step length fixed (sl to minimize)'
             else
              write ( *, * ) ' step length minimized (sf to fix)'
            end if
            write ( *, * )
            if ( isod .eq. 1 ) then
              write ( *, * ) ' matrix calculations damped by '//
     :                       'min( 1, len(grad) )'
             else
              write ( *, * ) ' matrix calculation not damped (il)'
            end if
            write ( *, * ) 
            if ( rlim .le. 0.0d0 ) then
              write (6, * ) ' standard matrix decomposition (d)'
             else
              write (6, '(1x,a,e10.4)' ) 'svd rejection (d) ', rlim
            end if
            write ( *, * )
            write ( *, '(1x,a,i1,a)' ) 'metric ', metric,
     :                  '  ( 0: b,N  1: b+N, b-N  2: alternate ) (me n)'
            write ( 6, * )
          end if

*         number of sub-bins added
          write(6,'(1x,a,2i3)') ' min, max sub-bins (p nmin nmax)',
     :            nminsubd,nmaxsubd
*
          if(verbose) then
            write(6,*) ' (v) Full details printed'
           else
            write(6,*) ' (v) Abbreviated commentary on input files'
          end if
          write (6,*)
          goto 533
        end if
*
*       4th parameter finite difference step
        if(noppsys.ge.4.and.(stch.eq.'x'.or.stch.eq.'X')) then
          if(dvstr(2).le.0.0d0) then
            write(6,*) 'New value for parameter 4 step?'
            write(6,'(''> '',$)')
            read(5,*) temp
           else
            temp=dvstr(2)
          end if
          step4=temp/qscale
          goto 533
        end if
*       N: column density step
        if(stch.eq.'N'.or.stch.eq.'n') then
          if(dvstr(2).le.0.0d0) then
            write(6,*)' New value?'
            write(6,'(''> '',$)')
            read(5,*) cdstsf
           else
            cdstsf=dvstr(2)
          end if
          goto 533
        end if
*       P: number of sub-bins
        if(stch.eq.'p'.or.stch.eq.'P') then
          if(ivstr(2).le.0) then
            write(6,*)' New values?'
            write(6,'(''> '',$)')
            read(5,*) nminsubd,nmaxsubd
           else
            nminsubd=ivstr(2)
            if(ivstr(3).ge.nminsubd) then
              nmaxsubd=ivstr(3)
             else
              nmaxsubd=max(nmaxsubd,nminsubd)
            end if
          end if
          goto 533
        endif
*       D:  force svd
        if(stch.eq.'d'.or.stch.eq.'D') then
          if ( nvstr .eq. 1 ) then
            write(*,*) ' -ve to turn off, or rejection level'
            write(6,*) '(typically 1e-12 or less)'
            write(6,'(''> '',$)')
            read(5,*) rlim
           else
            rlim=dvstr(2)
          end if
          goto 533
        endif

*       M: alternate metric
        if (stch.eq.'m' .or. stch.eq.'M') then
          if ( ivstr( 2 ) .ge. 0 .and. ivstr( 2 ) .le. 2 ) then
            metric = ivstr( 2 )
           else
            write ( 6, * ) ' Parameter not given -- ignored'
          end if
          go to 533
        end if
*       Z: redshift step
        if(stch.eq.'z'.or.stch.eq.'Z')then
          if(dvstr(2).le.0.0d0) then
            write(6,*)' New value?'
            write(6,'(''> '',$)')
            read(5,*)zstep
           else
            zstep=dvstr(2)
          end if
          goto 533
        endif
*       B:  Doppler parameter step
        if(stch.eq.'b'.or.stch.eq.'B')then
          if(dvstr(2).le.0.0d0) then
            write(6,*)' New value?'
            write(6,'(''> '',$)')
            read(5,*) bstep
           else
            bstep=dvstr(2)
          end if
          goto 533
        endif
*       C for chisq:
        if(stch.eq.'c'.or.stch.eq.'C')then
          if(dvstr(2).le.0.0d0) then
            write(6,*)' New values? [need 3]'
            write(6,'(''> '',$)')
            read(5,*) chsqdif1,chsqdnth,chsqdif2
           else
            chsqdif1=dvstr(2)
            if(dvstr(3).gt.1.0d0) then
              chsqdnth=dvstr(3)
            end if
            if(dvstr(4).gt.0.0d0) then
              chsqdif2=dvstr(4)
            end if
          end if
*         roundoff error sets in if value set too low, so set to
*         a minimum if necessary
          if(chsqdif1.lt.chsqdlim1) then
            chsqdif1=chsqdlim1
*           and warn the user you have done it
            write(6,'(a,1pe12.3)') 'Stopping Delta-Chi^2 reset to',
     :                              chsqdif1
          end if
          goto 533
        endif
*       S:
        if(stch.eq.'s'.or.stch.eq.'S') then
          if(cvstr(2).eq.' ') then
            write(6,*)' Fixed alpha?'
            write(6,'(''> '',$)')
            read(5,'(a)') stch
            if(stch.eq.'y')istch=1
            if(stch.ne.'y')istch=0
           else
            if(cvstr(2)(1:1).eq.'f'.or.cvstr(2)(1:1).eq.'F') then
              istch=1
             else
              istch=0
            end if
          end if
          goto 533
        endif
*       I: ill-conditioned
        if(stch.eq.'i'.or.stch.eq.'I') then
          isod=1
          isodh=1
*	  write(6,*) ' 1 to be added to diagonal terms of Hessian'
          if(cvstr(2)(1:1).ne.' ') then
            if(ivstr(2).eq.0) then
              write(6,*) 'No convergence damping'
              isod=0
              isodh=0
            end if
          end if
          goto 533
        endif
*
*       Full info output?
        if(stch.eq.'v'.or.stch.eq.'V') then
          if(verbose) then
            verbose=.false.
           else
            verbose=.true.
          end if
          goto 533
        end if
*       skip to end label:
 903    continue
      end if
c     check if you want log variables 
      write(6,'('' Column density (n), logN (l)'',
     1  '' or emission (e), scalefactor [l, 1.0]'')')
      write(6,'(''> '',$)')
      read(5,'(a)') instr
      call dsepvar(instr,2,dvstr,ivstr,cvstr,nvstr)
      icx=cvstr(1)(1:1)
      scl=dvstr(2)
      if(cvstr(1)(2:3).eq.'ad'.or.cvstr(1)(2:3).eq.'AD') then
*       set up emission line template add-in, via separate routine
        call vp_emtset('AD')
      end if
**
c     set icx to 'l' (logs) if previous was logs (indvar=1)
c     and icx blank
      if(icx.eq.' '.and.indvar.eq.1) icx='l'
      if(icx.eq.'n'.or.icx.eq.'N') then
        indvar=0
        if(cvstr(1)(2:2).eq.'<') then
          nptype=1
          if(cvstr(1)(3:3).ne.'2'.and.cvstr(1)(3:3).ne.'3') then
            write(6,*) 'Prints 1-sigma upper limits'
            csiglfac=1.0d0
           else
            if(cvstr(1)(3:3).ne.'3') then
              csiglfac=2.0d0
             else
              csiglfac=3.0d0
            end if
            write(6,'(a,a1,a)') 'Prints ',cvstr(1)(3:3),
     :         '-sigma upper limits'
          end if
          if(cvstr(1)(4:4).eq.'x') then
*           adjust thresholds so no lines are rejected
            write(6,*) 'Line rejection turned off'
            clvalmin=clvaldrop+0.01d0
            cvalmin=cvaldrop*1.01d0
          end if
         else
          nptype=0
        end if
        if(scl.gt.0.0d0) scalefac=scl
        if(verbose) then
          write(6,*) ' N scalefactor is ',scalefac
        end if
       else
*	logs or emission
        if(icx.eq.'e'.or.icx.eq.'E') then
*	  emission lines 
          write(6,*) ' Emission lines used...'
          indvar=-1
          scalelog=1.0d0
          scalefac=1.0d0
         else
*         defaults to log column densities
          indvar=1
          if(scl.ne.0.0d0) scalelog=10.0d0**scl
          if(verbose) then
            write(6,*) ' logN scalefactor is ',scalelog
          end if
        end if
      end if
*     if other variables unusual, tell the user
      if(ibdvar.eq.1) write(6,*) 'Log b used'
      if(izdvar.eq.1) write(6,*) 'Log 1+z used'
      if(izdvar.eq.2) write(6,*) 'velocity used instead of z'
*     read in the abundance data
      call vp_abundset
*
      return
      end
