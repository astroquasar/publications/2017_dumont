      subroutine rd_scrn(da,de,n,ds,re,nb,dh,dhe,ng,ngp,ind,
     :            cinpar,drv)
*
*     Rebin the data in DA variance in DE on to a uniform
*     wavelength scale, with the results in DH and DHE.
*     If DH and DHE already contain data and the user does not request 
*     that it be overwritten with new data, then the degree of
*     overlap is determined, and, in the overlapping region,
*     the new data is rescaled to the old and then a sum
*     (weighted by the inverse of the variances) is performed.
*     29.9.87 Option added so that the weights can be the sums of
*     the variances over the overlap regions - to avoid biassing
*     towards low count levels.
*     Modified to be vpfit compatible
*     
*     The rebinning of the first dataset is arranged so as to 
*     conserve counts.
*     
*     INPUT:	da	data array (sky subtracted, fluxed)
*               de	channel by channel variance (=s.e.**2) in da
*		n	array size for da and de
*		dh	rebinned data (may be zero)
*		dhe	variance for dh
*		ng	dh,dhe array size
*		cinpar	character array: 'n'-new, 'a'-auto add, else blank
*
*     OUTPUT:	dh	rebinned summed data
*		dhe	variance in dh
*		ngp	dh, dhe array length used
*
*     WORK:	ds	array for interim rebinned data
*		re	and the variance
*		nb	ds, re size
*		ind	indicates if the was data to add to in dh
*			(if 1); an argument for historical reasons only!
*
*     COMMON:	/rdc_scwv/
*		ach	base wavelength for rebinned data
*		bc	increment wavelength
*			so lambda = ach + bc * channel number
*		ac	interim base - used when da rebinned
*		achend	maximum wavelength
*		nbr	pointer to position in dh where interim
*			 rebinned data (in ds) starts
*               chwts   character*4 (logarithm or linear)
*
      implicit none
      include 'vp_sizes.f'
      integer n,nb,ng,ngp,ind
      character*(*) cinpar
      double precision drv(4)
      double precision da(maxfis),de(maxfis),ds(nb),re(nb)
      double precision dh(*),dhe(*)
*
*     local variables
      character*4 icx
      logical intr
      integer i,j,k1,k2,kd
      integer nend
      double precision wvdlo,wvdhi
      double precision ddum,dtemp
      double precision sa,sb,sva,svb,t,td,tvab
*     Functions:
      double precision wval,rd_wvsc
*
*     common variables
*     linear wavelength coefficients
      character*4 chwts
      integer nbr
      double precision ac,bc,ach,achend
      common/rdc_scwv/ac,bc,ach,achend,nbr,chwts
      double precision ssqrd,ssqrh
      common/wavrms/ssqrd,ssqrh
*     lxa,lxb are start and finish channels in the input data
*     (da) to be added in to dh
      integer lxa,lxb
      common/vpc_misc/lxa,lxb
      integer lxaheld,lxbheld
      common/rd_addv/lxaheld,lxbheld
*     character string input and separation
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
      double precision dvstr(24)
      common/vpc_dsepspace/dvstr
*
*     regions
      integer kalow(maxpreg),kahiw(maxpreg),nreg
      double precision wvlo(maxpreg),wvhi(maxpreg)
      common/rdc_regions/wvlo,wvhi,kalow,kahiw,nreg
*     scrunch style flag, isclog=1 for log bins
      integer isctype,isclog
      common/rdc_scflag/isctype,isclog
*
*     input streams
      integer inputc
      common/rdc_inputc/inputc
*
      ind=0
      nend=maxfis
      if(cinpar(1:1).eq.'n'.or.cinpar(1:1).eq.'a') then
*	called with internal parameters
        icx=cinpar
*	set flag so does not prompt later
        intr=.true.
       else
*	rfc 1.10.99: allow list of parameters on one line
        write(6,*)' Local options:'
        write(6,*)' New object, total -> const dlam bins (new);'
        write(6,*)' New object, mean -> constant dlam (mean);'
        write(6,*)' New object, mean -> log wvl (log);'
        write(6,*)' add with individual weights (add);'
        write(6,*)' add with summed weights (s)? [a]'
        read(inputc,'(a)') inchstr
        icx=inchstr(1:4)
        if(icx(1:1).eq.'S') icx(1:1)='s'
        intr=.false.
      end if
*
*     Initial setup section:
      if(icx(1:1).eq.'m'.or.icx(1:1).eq.'M') then
        isctype=1
        isclog=0
        icx(1:1)='m'
      end if
      if(icx(1:1).eq.'n'.or.icx(1:1).eq.'N') then
        isctype=0
        isclog=0
        icx(1:1)='n'
      end if
      if(icx(1:1).eq.'l'.or.icx(1:1).eq.'L') then
        isctype=1
        isclog=1
        icx(1:1)='l'
        chwts='log '
        write(6,*)' Logarithmic wavelength coefficients'
*       Not yet written, remove this line when it is!
        write(6,*)' ... it will probably fail'
      end if
*
      if(icx(1:1).eq.'n'.or.icx(1:1).eq.'m'.or.
     :     icx(1:1).eq.'l') then
*       flag for sum/mean now set, so set new indicator
        icx(1:1)='n'
*       new sum .. compute defaults
*	base is start of current data
        if(cinpar(2:2).ne.'r'.and.cinpar(2:2).ne.'R') then
*	  wavelength default
          ach=wval(0.0d0)
*         base wavelength, increment, end wavelength logs if icslog is 1
          if(chwts(1:3).eq.'log') ach=log10(ach)
          if(bc.le.0.0d0) then
*	    increment not yet set, so guess one
            if(chwts(1:3).eq.'log') then
              bc=(log10(wval(2.048d3))-ach)/2.048d3
              dtemp=dble(maxfis)*bc
              if(dtemp.le.20.0d0) then
                achend=ach+dtemp
               else
                achend=20.0d0
              end if
             else
              bc=(wval(2.048d3)-ach)/2.048d3
              achend=ach+bc*dble(maxfis)
            end if
          end if
        end if
*       request values for wavelength & increment (linear scale)
*	unless passed via cinpar
        if(cinpar(1:1).ne.'n') then
*	  drv values need to be given
          if(chwts(1:3).eq.'log') then
            dtemp=10.0d0**ach
            write(6,*) '(base wavelength is ',dtemp,' A)'
            write(6,*) 'If base > 30.0, Angstroms assumed'
            dtemp=(10.0d0**bc-1.0d0)*2.99792458d5
            write(6,*) '(increment corresponds to',dtemp,' km/s)'
            write(6,*) 'If increment > 0.01, km/s ASSUMED'
          end if
          write(6,*) ' start wavelength, increment, (end wavelength)'
          write(6,*) '[',ach,', ',bc,',(set by array length)]'
          read(inputc,'(a)') inchstr
          call dsepvar(inchstr,3,drv,ivstr,cvstr,nvstr)
         else
*         if cinpar used, linear wavelengths only, so to make sure:
          isclog=0
        end if
*	if new values input, use them
        if(drv(1).gt.0.0d0) then
          ach=drv(1)
          if(chwts(1:3).eq.'log'.and.ach.gt.30.0d0) then
            ach=log10(ach)
          end if
        end if
        if(drv(2).ne.0.0d0) then
          bc=drv(2)
          if(bc.gt.0.01d0.and.chwts(1:3).eq.'log') then
            bc=log10(1.0d0+bc/2.99792458d5)
          end if
        end if
        if(drv(3).gt.drv(1)) then
          achend=drv(3)
          if(chwts(1:3).eq.'log'.and.achend.gt.30.0d0) then
            achend=log10(achend)
          end if
         else
          if(chwts(1:3).eq.'log') then
            dtemp=dble(maxfis)*bc
            dtemp=min(20.0d0,dtemp)
            achend=ach+dtemp
           else
            achend=ach+bc*dble(maxfis)
          end if
        end if
*       work out maximum array length for scrunched array
*       given the wavelength limits
        if(chwts(1:3).eq.'log') then
          nend=int((achend-ach)/bc+0.5d0)
         else
          nend=int((achend-ach)/bc+0.5d0)
        end if
        if(nend.gt.maxfis) nend=maxfis
        ind=1
        write(6,*) 'Wavelength coeffts:',ach,bc,' for',
     1               nend,' pixels'
*       zero data arrays
        do i=1,maxfis
          dh(i)=0.0d0
          dhe(i)=0.0d0
        end do
*
*	for wavelength uncertainties from the comparison arc fitting
*	adopt first sigma - it will be sufficient
        ssqrh=ssqrd
      end if
*
*     Add data section:
      if(intr) then
*	include whole spectrum for internal calls if nreg=0
*	if(nreg.le.0) then
        wvdlo=wval(dble(1))
        wvdhi=wval(dble(n))
*	else
*	  search for the region containing the Ly-a line
*	  needs to be written
*	  write(6,*) 'not yet available'
*	end if
       else
 231    write(6,*) ' wavelengths included (from,to) [all]'
        write(6,*) '   ..or (wave,from,to) or (chans,from,to)'
*	rfc 4.10.99 was channel numbers lxa,lxb
        read(inputc,'(a)') inchstr
        call dsepvar(inchstr,3,dvstr,ivstr,cvstr,nvstr)
        if(nvstr.gt.2) then
          if(cvstr(1)(1:1).eq.'w'.or.cvstr(1)(1:1).eq.'W') then
            dvstr(1)=dvstr(2)
            dvstr(2)=dvstr(3)
           else
            if(cvstr(1)(1:1).eq.'c'.or.cvstr(1)(1:1).eq.'C') then
*             use channels from the input spectrum
              dvstr(1)=wval(dvstr(2))
              dvstr(2)=wval(dvstr(3))
              write(6,*) 'Wavelength limits',dvstr(1),dvstr(2)
             else
              goto 231
            end if
          end if
        end if
        wvdlo=rd_wvsc(1.0d0)
        if(dvstr(1).gt.wvdlo) then
          wvdlo=dvstr(1)
        end if
        dtemp=achend
        if(chwts(1:3).eq.'log') then
          dtemp=10.0d0**dtemp
        end if
        if(dvstr(2).le.wvdlo) then
          wvdhi=dtemp
         else
          wvdhi=dvstr(2)
          if(wvdhi.gt.dtemp) wvdhi=dtemp
        end if
      end if
*     convert to channel number
      ddum=wval(dble(nend/2))
      call chanwav(wvdlo,ddum,0.003d0,30)
      lxa=int(ddum+0.5d0)
      call chanwav(wvdhi,ddum,0.003d0,30)
      lxb=int(ddum)
*     restrict ranges to within limits of data arrays
      if(lxa.le.0) lxa=1
      if(lxb.le.lxa.or.lxb.gt.n) lxb=n
*
*     wavelength offset to channel offset
      if(chwts(1:3).eq.'log') then
        td=wval(dble(lxa))/rd_wvsc(1.0d0)
        nbr=int(log10(td)/bc)
       else
        td=wval(dble(lxa))-rd_wvsc(1.0d0)
        nbr=int(td/bc)
      end if
      if(td.le.0.0d0) nbr=0
      ac=rd_wvsc(dble(nbr))
      if(chwts(1:3).eq.'log') ac=log10(ac)
*
*     rebin the new data
      call rd_drebin(da,de,n,ds,re,nb,wvdlo,wvdhi,lxa,lxb)
      if(chwts(1:3).eq.'log') then
        td=0.0d0
        do i=1,nb
          if(ds(i).gt.td) td=ds(i)
        end do
        write(6,*) 'ds(max)=',td
        td=0.0d0
        do i=1,nb
          if(re(i).gt.td) td=ds(i)
        end do
        write(6,*) 're(max)=',td
      end if
*
      if(icx(1:1).eq.'n'.or.icx(1:1).eq.'m'.or.icx(1:1).eq.'l') then
*       new object, straight add to zero
        ngp=ng
        do i=1,nb
          j=i+nbr
          if(j.gt.ng) goto 21
          if(j.gt.ngp) ngp=j
          dh(j)=dh(j)+ds(i)
          dhe(j)=dhe(j)+re(i)
        end do
*        ngp=j
 21     return
      end if
*
*     later runs added in to previous ones
      k1=1
*     determine overlap
      k2=nb
      write(6,*) 'k1,k2',k1,k2
 4    j=k1+nbr
      if(j.ge.ng) goto 3
      if(k1.ge.nb) goto 3
      if(dh(j).ne.0.0d0.and.ds(k1).ne.0.0d0) goto 3
      k1=k1+1
      goto 4
 3    j=k2+nbr
      if(k2.le.1) goto 6
      if(j.ge.ng) goto 5
      if(dh(j).ne.0.0d0.and.ds(k2).ne.0.0d0) goto 6
 5    k2=k2-1
      goto 3
 6    sa=0.0d0
      sb=0.0d0
      sva=0.0d0
      svb=0.0d0
      write(6,*) 'k1,k2',k1,k2
      if(k2.gt.k1) then
        do i=k1,k2
          j=nbr+i
          if(dhe(j).le.0.0.or.re(i).le.0.0) goto 30
          sa=sa+dh(j)
          sb=sb+ds(i)
          sva=sva+dhe(j)
          svb=svb+re(i)
 30       continue
        end do
        sva=sva/dble(k2-k1+1)
        svb=svb/dble(k2-k1+1)
      end if
*
*     renormalize to first data
      if(intr) then
*       force unit scaling
        sa=1.0d0
        sb=1.0d0
      end if
      if(sb.ne.0.0d0.and.sa.ne.0.0d0) then
        if(.not.intr) then
          sa=sa/sb
          write(6,*) ' Scale factor for new data:',sa
          write(6,*) ' <CR> if ok, enter scalefactor otherwise'
          read(inputc,'(a)') inchstr
          call dsepvar(inchstr,1,dvstr,ivstr,cvstr,nvstr)
          if(dvstr(1).gt.0.0d0) sa=dvstr(1)
        end if
        do i=1,nb
          ds(i)=ds(i)*sa
          re(i)=re(i)*sa*sa
        end do
*
*       set weights for 's' option
        svb=svb*sa*sa
        if(sva.gt.0.0d0) then
          sva=1.0d0/sva
          if(svb.gt.0.0d0) then
            svb=1.0d0/svb
            tvab=sva+svb
           else
            tvab=sva
          end if
         else
          if(svb.gt.0.0d0) then
            svb=1.0d0/svb
            tvab=svb
           else
*           nothing to add in
            tvab=1.0d0
          end if
        end if
       else
        write(6,*) ' No overlap -- add without scaling? [n]'
        read(5,'(a)',end=11) icx
        if(icx(1:1).ne.'y'.and.icx(1:1).ne.'Y') goto 11
        do i=1,nb
*	  copy ds to dh where error re gt 0
          if(re(i).gt.0.0) then
            j=nbr+i
            dh(j)=ds(i)
            dhe(j)=re(i)
          end if
        end do
        goto 11
      end if
*
*     add this to the earlier stuff...
      do i=k1,k2
        j=nbr+i
        sa=dhe(j)
        sb=re(i)
        if(sa.gt.0.0d0) then
          if(sb.gt.0.0d0) then
*	    check for addition type
            if(icx(1:1).eq.'s') then
*	      use summed weights
              if(sva.gt.0.0d0) then
                if(svb.gt.0.0d0) then
                  dh(j)=(sva*dh(j)+svb*ds(i))/tvab
                  dhe(j)=(sva*sva*sa+svb*svb*sb)/
     :                           (tvab*tvab)
                end if
               else
                dh(j)=ds(i)
                dhe(j)=re(i)
              end if
             else
              sa=1.0d0/sa
              sb=1.0d0/sb
              t=sa+sb
              dhe(j)=1.0d0/t
              dh(j)=(sa*dh(j)+sb*ds(i))/t
            end if
          end if
         else
          dh(j)=ds(i)
          dhe(j)=re(i)
        end if
      end do
*
*     non-overlapping data added since at least one lot zero
      if(k1.le.1) goto 9
      kd=k1-1
      do i=1,kd
        j=nbr+i
        dh(j)=dh(j)+ds(i)
        dhe(j)=dhe(j)+re(i)
      end do
 9    kd=k2+1
      if(kd.gt.nb) goto 11
      do i=kd,nb
        j=nbr+i
        if(j.gt.ng) goto 11
        dh(j)=dh(j)+ds(i)
        dhe(j)=dhe(j)+re(i)
      end do
 11   continue
      return
      end
