      subroutine vp_fgf13write
*
*     writes out filename list and first guesses at parameters
*     from interactive startup. Separated from vpfit.f to make
*     that routine less cluttered.
*
*     IN:
*     several common variables
*     OUT:
*     nothing internally, just writes to files
*
      implicit none
      include 'vp_sizes.f'
*
*     local variables
      integer k,lxx,lll,lfl,lensig
      integer l,ip
      double precision wastart,waend,ptempt
      double precision temp4
      double precision sigmak,dlamk
      character*10 chpdate,chdate,chtime
*
*     functions
      integer lastchpos
      double precision vp_wval
*
*     Common variables
*
*     Option number
      integer nopt
      common/vpc_option/nopt
*     chunk variables:
      integer ndpts(maxnch),idrun(maxnch),icrun(maxnch)
      integer indfil(maxnch)
      character*64 filename(maxnch)
      double precision wvstrt(maxnch),wvend(maxnch)
      integer nchunk
      common/vpc_chunk/wvstrt,wvend,ndpts,filename,
     :      idrun,icrun,indfil,nchunk
*     old data file and parameters (blank if not used), including
*     accumulate flag lcuminc (07.03.01)
      integer ncumset
      character*132 comchstr
      logical ldate26
      logical lcuminc
      common/vpc_comchstr/comchstr,ldate26,lcuminc,ncumset
*     output channels for printing:
      integer nopchan,nopchanh,nmonitor
      integer lt(3)
      common/vpc_nopchan/lt,nopchan,nopchanh,nmonitor
*     start and end channels for each chunk, for regions fitted over
      integer ichstrt(maxnch),ichend(maxnch)
      common/jkw4/ichstrt,ichend
*     fit to resolution, using sigmas
      integer nswres(maxnch)
      double precision swres(maxpoc,maxnch)
      common/vpc_sigwres/swres,nswres
*     separation of variables string space
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
*     component ions and parameters
      integer nlines,nn
      character*2 ion(maxnio)
      character*4 level(maxnio)
      double precision parm(maxnpa)
      common/vpc_parry/parm,ion,level,nlines,nn
*     log or linear variable indicator,1 for logN, 0 for linear
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
*     tied indicators
      integer isod,isodh
      character*2 ipind(maxnpa)
      common/vpc_usoind/ipind,isod,isodh
*     turbulent, temperature and associated parameters
      double precision vturb,atmass,temper,fixedbth
      common/vpc_ucopder/vturb(maxnio),atmass(maxnio),
     :                 temper(maxnio),fixedbth(maxnio)
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     version number
      character*8 chvpvnum
      common/vpc_vpvnum/chvpvnum
*
*     Starting separator
      write(13,*) '  *'
*     write file use information to for018 file
      if(nopt.ne.9.and.nopchan.ge.2) then
        write(18,'('' *****'')')
        write(18,*) nchunk
      end if
*
      do k=1,nchunk
*       start, end wavelengths for the chunk	  
        wastart = vp_wval(dble(ichstrt(k)-1),k)
        waend   = vp_wval(dble(ichend(k)+1),k)
*       trim filename
        lxx=len(filename(k))
        do while (lxx.gt.1.and.filename(k)(lxx:lxx).eq.' ')
          lxx=lxx-1
        end do
        write(13,'(a,a1,i6,2f10.2)') filename(k)(1:lxx),' ',
     :            idrun(k),wastart,waend
*	If requested, to fort.13 style summary file:
        call vp_f13hout(filename,lxx,idrun,k,wastart,waend)
*	RFC 20.3.98: Same info to fort.26, with %% precursor 
        lll=lastchpos(comchstr)
        if(ldate26) then
*	  get date and time
          call vp_dattim(chdate,chtime)
          chpdate=chdate(1:4)//'/'//chdate(5:6)//'/'//chdate(7:8)
        end if
        if(k.eq.1.and.lll.gt.0) then
*	  have preset data, so note it in O/P file
*	  truncating info if necessary
          if(ldate26) then
            if(lll.gt.40) then
              lll=30
            end if
            write(26,'(a3,a,a1,i4,2f10.2,a,a)') 
     :          '%% ',filename(k)(1:lxx),' ',idrun(k),
     :          wastart,waend,
     :          ' ! '//comchstr(1:lll)//' ',chpdate
           else
            if(lll.gt.40) lll=40
            write(26,'(a3,a,a1,i4,2f10.2,a)') 
     :          '%% ',filename(k)(1:lxx),' ',idrun(k),
     :          wastart,waend,
     :          '  ! '//comchstr(1:lll)
          end if
         else
          if(ldate26.and.k.eq.1) then
            write(26,'(a3,a,a1,i4,2f10.2,a,a,a,a)') 
     :          '%% ',filename(k)(1:lxx),' ',idrun(k),
     :          wastart,waend,' ! ',chpdate,'  ',chvpvnum
           else
            write(26,'(a3,a,a1,i4,2f10.2,a,a)') 
     :          '%% ',filename(k)(1:lxx),' ',idrun(k),
     :          wastart,waend,'  ',chvpvnum
          end if
        end if
*
        if(nopt.ne.9) then
*	  write out file info unless just displaying data (option 9)
*	    -- this check unnecessary here at present
          lfl=max0(12,lxx)
*	  resolution stuff for output:
          sigmak=swres(2,k)*2.99792458d5
          dlamk=swres(1,k)
          call vp_hpakres(sigmak,dlamk,inchstr,lensig)
*	    
          if(nopchan.ge.2) then
            write(18,'(a,a1,i4,2f10.2,a)') 
     :           filename(k)(1:lfl),' ',idrun(k),wastart,
     :           waend,' '//inchstr(1:lensig)
          end if
*
        end if
      end do
*
*     ' *' to head file, for compatibility with old stuff.
      write(13,'(''  *'')')
*     ions and parameters
      do l=1,nn/noppsys
        ip=noppsys*(l-1)
*       RFC 20.3.98: don't rescale the continuum or zero adjustments
        if(ion(l).ne.'<>'.and.ion(l).ne.'__'.and.
     :      ion(l).ne.'>>'.and.ion(l).ne.'{}') then
          if(indvar.eq.1) then
            ptempt=parm(ip+nppcol)/scalelog
           else
            ptempt=parm(ip+nppcol)/scalefac
          end if
         else
          ptempt=parm(ip+nppcol)
        end if
        if(nopt.eq.1.or.nopt.eq.3.or.nopt.eq.6) then
          if(noppsys.le.3) then
            write(13,1902) ion(l),level(l),ptempt,ipind(ip+nppcol),
     :        parm(ip+nppzed),ipind(ip+nppzed),parm(ip+nppbval),
     :        ipind(ip+nppbval),vturb(l),temper(l)
 1902       format(3x,a2,a4,1x,1pe9.3,a2,0pf10.6,a2,f9.2,a2,f9.2,
     :           1x,1pe10.2)
           else
*           extra variable, so print it, rescaled back to true value 
            temp4=1.0d-6*parm(ip+4)
            write(13,1903) ion(l),level(l),ptempt,ipind(ip+nppcol),
     :        parm(ip+nppzed),ipind(ip+nppzed),parm(ip+nppbval),
     :        ipind(ip+nppbval),temp4,ipind(ip+4),
     :        vturb(l),temper(l)
 1903       format(3x,a2,a4,1x,1pe9.3,a2,0pf10.6,a2,f9.2,a2,
     :           1pe11.3,a2,0pf9.2,1x,1pe10.2)
          end if  
        end if
      end do
*     flush buffer
      call flush(13)
      return
      end
