      subroutine vp_blfixf(ion,ipind,nsys)
*
*     remove any non-special fixed flags i.e. those which do not
*     have a lower case version somewhere. 
*
      implicit none
      integer nsys
      character*2 ion(*),ipind(*)
*
*     Local:
      integer j,jb,jz,k,kz,match
*
*     functions
      logical lcase,ucase,noceqv
*
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     general control variables 
*        chcv(6)='nofi' for unfixing parameters
      character*4 chcv(10)
      common/vpc_chcv/chcv
*
      if(chcv(6).eq.'nofi') then
        do j=1,nsys
          if(ion(j).ne.'__'.and.ion(j).ne.'<>'.and.ion(j).ne.'>>'.and.
     :       ion(j).ne.'AD') then
*           check if this has a fixed redshift associated
            jb=noppsys*(j-1)
            jz=jb+nppzed
            if(ucase(ipind(jz)(1:1))) then
              match=0
              do k=1,nsys
                if(k.ne.j) then
                  kz=noppsys*(k-1)+nppzed
                  if(lcase(ipind(kz)(1:1)).and.
     :               noceqv(ipind(kz),ipind(jz))) then
*                   have a lower case match
                    match=1
                  end if
                end if
              end do
              if(match.eq.0) then
*               blank off indicators for this set
                do k=1,noppsys
                  ipind(jb+k)='  '
                end do
              end if
            end if
          end if
        end do
      end if
      return
      end

                 
