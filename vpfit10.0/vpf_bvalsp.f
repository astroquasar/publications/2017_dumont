      double precision function vpf_bvalsp(ns,parm,ipind)
*     return Dopple parameter for this ion
      implicit none
      include 'vp_sizes.f'
*
*     ns is system number
      integer ns
      double precision parm(*)
      character*2 ipind(*)
*
*     Local:
      integer jsx,jbbv,jxr,jxhi
*
*     Functions
      logical lcase,nocgt
*
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     Special conditions after this character (totals for column,
*     temperature/b for bval)
      character*2 lastch,firstch
      common/vpc_ssetup/lastch,firstch
*     output array from here is x-reference tied list
      integer mtxref
      common/vpc_txref/mtxref(maxnpa)
*     atomic mass data for each ion
      double precision vturb,atmass,temper,fixedbth
      common/vpc_ucopder/vturb(maxnio),atmass(maxnio),
     :     temper(maxnio),fixedbth(maxnio)
*     temperature units
      double precision thunit,thmax
      common/vpc_thscale/thunit,thmax
*
      jbbv=(ns-1)*noppsys+nppbval
*     if ipind > lastch then using temp/turb as primary variables
*     otherwise just as the parameter
      if(nocgt(ipind(jbbv)(1:1),lastch)) then
*       unscramble Doppler velocity. Need xref set.
        if(lcase(ipind(jbbv)(1:1))) then
          jsx=jbbv
         else
*         uppercase, so tied/fixed. mtxref(jbbv) is base
          jsx=mtxref(jbbv)
        end if
        jxr=mtxref(jsx)
        if(jxr.lt.jsx) then
          jxhi=jsx
         else
          jxhi=jxr
          jxr=jsx
        end if
        vpf_bvalsp=sqrt(parm(jxr)**2+parm(jxhi)*thunit/(
     :        60.135795d0*atmass(ns)))
*        write(6,*) 'b',jxr,parm(jxr),jxhi,parm(jxhi),ns,atmass(ns),
*     :             vpf_bvalsp
       else
        vpf_bvalsp=parm(jbbv)
      end if
      return
      end
