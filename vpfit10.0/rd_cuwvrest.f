      subroutine rd_cuwvrest(wvrest)
*     return rest wavelength for velocity stack plot
*
      implicit none
      include 'vp_sizes.f'
      double precision wvrest
*     local variables
      character*1 chc
      character*2 indcol,indb,indz
      integer i,j
      real x1pg,y1pg,ybasepg,yincrpg
      double precision bval,col,trstw,zval
      double precision ctwrst,dwrst
*     stack spectra variables
      integer nlins
      character*2 atom(64)
      character*4 ion(64)
      double precision rstwav(64)
      common/rdc_stplvar/atom,ion,rstwav,nlins
*     current plot range maxima and minima
      real xmpg,xhpg,ympg,yhpg
      common/vpc_splcur/xmpg,xhpg,ympg,yhpg
*     plot/print velocity scale?
      logical lvel
      real wcenpg,vellopg,velhipg,zp1refpg
      common/vpc_pgvels/wcenpg,vellopg,velhipg,lvel,zp1refpg
*     text label for a curve
      integer icoltext
      real xtextpg,ytextpg
      character*24 cpgtext
      common/pgtexts/xtextpg,ytextpg,cpgtext,icoltext
*     list of (up to 35) filenames with line lists for stacked plots
      integer ncufil,jcufil
      character*72 cufilnm(35)
      common/rdc_cufilnm/cufilnm,ncufil,jcufil
*     atomic data
      integer nz
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),nz
*     plot attributes
      integer ipgatt(10,9)
      common/vpc_pgattrib/ipgatt
*     readin workspace
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
*     ion/wavelength internal list
      integer ntrwave
      character*2 tatom(16)
      character*4 tion(16)
      double precision trwave(16)
      common/rdc_intvtab/trwave,ntrwave,tion,tatom

*
*     start by setting default
      if(nz.le.0) call vp_ewred(0)
      wvrest=1215.6701d0
      if(jcufil.gt.0) then
        open(unit=7,file=cufilnm(jcufil),status='old',err=991)
*       read in the number of lines
        nlins=0
 1      read(7,'(a)',end=992) inchstr
        if(inchstr(1:8).ne.'        ') then
          nlins=nlins+1
          call vp_initval(inchstr,atom(nlins),ion(nlins),trstw,
     :                       col,indcol,bval,indb,zval,indz)
*	  correct trstw to nearest wavelength from the table
          dwrst=1.0d20
          rstwav(nlins)=trstw
          do i=1,nz
            if(lbz(i).eq.atom(nlins).and.
     :               lzz(i).eq.ion(nlins)) then
              ctwrst=abs(alm(i)-trstw)
              if(ctwrst.lt.dwrst) then
                rstwav(nlins)=alm(i)
                dwrst=ctwrst
              end if
            end if
          end do
        end if
        if(nlins.lt.50) goto 1
        write(6,*) ' Only first 50 lines used'
*       end of file
 992    close(unit=7)
       else
        if(ntrwave.gt.0) then
          nlins=ntrwave
          do i=1,ntrwave
            atom(i)=tatom(i)
            ion(i)=tion(i)
            rstwav(i)=trwave(i)
          end do
        end if
      end if
*     write out the options in the plot window
      yincrpg=(yhpg-ympg)/real(nlins)
      ybasepg=ympg-0.7*yincrpg
      xtextpg=0.9*xmpg+0.1*xhpg
*     use text color
      call pgsci(abs(ipgatt(1,4)))
      do j=1,nlins
        ytextpg=ybasepg+real(j)*yincrpg
*       lvel high scale option
        write(cpgtext,'(f8.2)') rstwav(j)
        cpgtext=atom(j)//ion(j)//cpgtext(1:8)
        call pgtext(xtextpg,ytextpg,cpgtext)
      end do
*     read the cursor:
      call pgsci(abs(ipgatt(1,8)))
      call vp_pgcurs(x1pg,y1pg,chc)
*     convert the y position to line pointed to
      j=int((y1pg-ybasepg)/yincrpg)
      if(j.le.0) j=1
      if(j.gt.nlins) j=nlins
      wvrest=rstwav(j)
      write(6,'(a,f14.4)') 'Rest wavelength:',wvrest
*
 991  continue
      return
      end
