      subroutine rd_hdust(da,re,nc,dh,ng,chwhat)
*     estimate the dust reddening based on the Hb (da) to Ha (dh)
*     ratio. Result returned in da
*     It is assumed that the data are wavelength registered, so
*     corresponding bins can be used to determine Hb/Ha. The
*     result is returned in re
      implicit none
      integer nc,ng
      double precision da(*),dh(*),re(*)
      character*(*) chwhat
*     Local:
      integer i,lx
      double precision hahb,paha,rmax,temp,coef
*     Common:
      integer nv
      character*132 inchstr
      character*60 cv(24)
      real rv(24)
      integer iv(24)
      common/rd_chwork/inchstr,rv,iv,cv,nv
      double precision dvstr(24)
      common/vpc_dsepspace/dvstr
*
      lx=min0(nc,ng)
*     absorption at Pa 0.09, Ha 0.74, Hb 1.09 for exp(-c*f)
*     Ha/Hb prototype 2.85, Pa/Hb 0.33 (CaseB 10^4K, ne=10^4)
*     here interpolated for 10^9.5 T=20000
      hahb=2.66d0
      paha=0.098d0
      write(6,*) 'Case B: 2*10**4 K, ne=10**9.5 as default'
      write(6,*) 'T?, or Ha/Hb, Pa/Hb'
      read(5,'(a)') inchstr
      call dsepvar(inchstr,2,dvstr,iv,cv,nv)
      if(nv.eq.2.and.iv(1).lt.100) then
*       entering Ha/Hb and Pa/Hb
        hahb=dvstr(1)
        paha=dvstr(2)/hahb
        write(6,*) 'Ha/Hb=',hahb,' Pa/Hb=',rv(2),' Pa/Ha=',paha
      end if
      if(iv(1).eq.5000) then
        hahb=3.00d0
        paha=0.132d0
      end if
      if(iv(1).eq.10000) then
        hahb=2.85d0
        paha=0.116d0
      end if
      if(iv(1).eq.20000) then
        hahb=2.74d0
        paha=0.103d0
      end if
*
      rmax=0.0d0
*     form ratios
      do i=1,lx
        if(dh(i).ne.0.0d0) then
          re(i)=da(i)/dh(i)
          if(rmax.lt.re(i)) rmax=re(i)
*	  If Balmer decrement > 30*caseB, don't want to know
          if(re(i).lt.0.01d0) re(i)=0.01d0
         else
          re(i)=0.01d0
        end if
      end do
*     take care of any zeros
      rmax=2.0d0*rmax
      do i=1,lx
        if(dh(i).eq.0.0d0) then
          if(da(i).ne.0.0d0) then
*           set to 2*max if infinity, leave as 0.01 if da(i)=0
            re(i)=rmax
          end if
        end if
      end do
*
*     exp(0.35*c)=1.0/(2.85*re(i)) (default #)
      if(chwhat(1:2).eq.'Pa') then
*       compute Paschen alpha/H-alpha
        do i=1,lx
          temp=1.0d0/(hahb*re(i))
          if(temp.le.1.0) then
            coef=0.0d0
           else
            coef=log(temp)/0.35d0
          end if
*         now set up predicted P-a flux/H-a flux
          re(i)=paha*exp(coef*0.26d0)
          if(chwhat(1:4).eq.'Pa-p') then
*           P-a profile
            re(i)=re(i)*dh(i)
          end if
        end do
      end if
      return
      end
