      subroutine vp_spdatin(nfile,ichunk)
*
*     data read routine
*     rfc 22.5.99: parameter arrays passed as common
*
      implicit none
      include 'vp_sizes.f'
*
*     Subroutine:
      integer nfile,ichunk
*
*     LOCAL:
      integer i,ii,item,itemp,intsys,intwav,ielmnt
      integer idrnv,j
      integer ncv,ng,nions,nposch,nlfilnm
      double precision ctwrst,dwrst,zed
      double precision werr
*
*     Function:
      integer lastchpos
*
*     COMMON:
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*
      character*2 ion(maxnio)
      character*4 level(maxnio)
      double precision parm(maxnpa)
      integer nlines,nn
      common/vpc_parry/parm,ion,level,nlines,nn
      character*60 cv(2)
      character*132 pchstr
**
*     current 1-D spectrum data, err, fluctuations, continuum -length ngp
      integer ngp
      double precision dhdata(maxfis),dherr(maxfis)
      double precision dhrms(maxfis),dhcont(maxfis)
      common/vpc_su1d/dhdata,dherr,dhrms,dhcont,ngp
*     current original continuum
      double precision dhccont(maxfis)
      common/vpc_su1dcont/dhccont
*     2D dataset
*     real datss(maxfis,maxnfi),derss(maxfis,maxnfi)
*     real dconss(maxfis,maxnfi)
*     real origcntss(maxfis,maxnfi)
*     common/vpc_su2d/datss,derss,dconss,origcntss
*
      integer nvstr
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
      double precision dvstr(24)
      common/vpc_dsepspace/dvstr
*
      character*8 wcftype
      character*4 vacind
      double precision helcfac
      integer*4 noffs
      common/c8_wcftype/wcftype,helcfac,noffs,vacind
*
*     chunk variables:
      integer ndpts(maxnch),idrun(maxnch),icrun(maxnch)
      integer indfil(maxnch)
      character*64 filename(maxnch)
      double precision wvstrt(maxnch),wvend(maxnch)
      integer nchunk
      common/vpc_chunk/wvstrt,wvend,ndpts,filename,
     :      idrun,icrun,indfil,nchunk
*
*     binned profile (npin2 is effective useful midpoint)
      character*2 chprof
      double precision pfinst
      double precision wfinstd
      integer npinst,npin2
      common/vpc_profwts/wfinstd(maxipp,maxnch),pfinst(maxipp,maxnch),
     :        npinst(maxnch),npin2(maxnch),chprof(maxnch)
*     fit to resolution, using sigmas since they are in expression used
      integer nswres(maxnch)
      double precision swres(maxpoc,maxnch)
      common/vpc_sigwres/swres,nswres
*     individual file scale coeffts
      integer nscl
      double precision ssclcf,wvalsc
      common/sigsclcft/ssclcf(maxtas,maxnch),wvalsc(maxtas,maxnch),
     1                   nscl(maxnch)

*     general wavelength coefficients
      double precision wcfd(maxwco,maxnch)
      common/vpc_wcfd/wcfd
*     wavelength parameter sets
      character*8 wcfty2(maxnch)
      character*4 vacin2(maxnch)
      double precision helcf2(maxnch)
      integer nwcf2(maxnch),noffs2(maxnch)
      common/vpc_wcpars2/wcfty2,helcf2,nwcf2,noffs2,vacin2
      integer ngd,npexsm
      integer numpts(maxnch)
      double precision wcoeff(maxnch)
      common/vpc_jkw3/wcoeff,ngd,npexsm,numpts
*     double precision smultd
*     common/vpc_jkw5/smultd
*     number wavelength coeffts used
      integer nwco
      double precision wcf1(maxwco)
      common/vpc_wavl/wcf1,nwco
*     workspace variables
      integer idwk(74)
      common/works/idwk
*     current data file and order number
      integer idrn,icrn
      character*64 filnm
      common/vpc_file14/filnm,idrn,icrn
*     plot range and type parameters
      integer lxa,lxb
      common/vpc_misc/lxa,lxb
*     old file name and order number
      integer irunold
      character*64 oldfilnm
      common/vpc_oldfilnm/oldfilnm,irunold
*     Current line parameters for guess (default = Ly-a)
      logical lgs
      character*2 ings
      character*4 lvgs
      double precision wvgs,fkgs,algs,amgs
      common/vpc_pargs/wvgs,fkgs,algs,amgs,lgs,ings,lvgs
*     default line parameters
      character*2 ingsdef
      character*4 lvgsdef
      double precision wvgsdef,fkgsdef,algsdef,amgsdef
      common/vpc_pargsdef/wvgsdef,fkgsdef,algsdef,amgsdef,
     :                       ingsdef,lvgsdef
*
      integer nz
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),nz
*     old data file and parameters
      integer ncumset
      character*132 comchstr
      logical ldate26
      logical lcuminc
      common/vpc_comchstr/comchstr,ldate26,lcuminc,ncumset
*     plot control
      integer ksplot(9)
      common/vpc_ksplot/ksplot
*
*     ind=4
      ielmnt=0
      ng=maxfis
      cv(2)=' '
      ncv=2
 3142 continue
      if(idrn.ne.0) then
*       Running from an input file
        nlfilnm=lastchpos(filnm)
        write(6,'( 1x, 2a )') 'filename       : ',filnm(1:nlfilnm)
        write(6,'( 1x, a, i4 )' ) 'spectrum number: ', idrn
       else
*       determine position of last non-blank
        nposch=64
        do while (nposch.gt.5.and.oldfilnm(nposch:nposch).eq.' ')
          nposch=nposch-1
        end do
*
        filnm='?'
        do while(filnm(1:1).eq.'?')
          write(6,*) ' Filename ',nfile,' or ? (lam,n) [',
     1              oldfilnm(1:nposch),']:'
          write(6,'(''> '',$)')
          read(5,'(a)') filnm
          if(filnm(1:1).eq.'?') then
*           list from vp_files.dat
            call dsepvar(filnm,3,dvstr,ivstr,cvstr,nvstr)
*           presets for vp_listfiles to cause prompting:
            intwav=0 
            pchstr=' '
            zed=0.0d0
*           write(6,*) nn
            if(nvstr.gt.2) then
*             wavelength and system number entered, change presets
              intwav=ivstr(2)
              intsys=ivstr(3)
              nions=nn/noppsys
              if(intsys.le.nions) then
*               convert system number (intsys) to ion, redshift
                pchstr=ion(intsys)(1:2)//level(intsys)(1:4)
*               get appropriate redshift
                item=noppsys*(intsys-1)+nppzed
                zed=parm(item)
                write(6,'(a,i8,f14.6)') pchstr(1:6),intwav,zed
              end if
            end if
            call vp_listfiles(pchstr,intwav,zed,oldfilnm,idrnv)
          end if
        end do
        if(filnm(1:3).eq.'   ') then
*         use the old filename and order number
          filnm=oldfilnm
          write(6,*) ' Using filename: ',filnm(1:20),'...'
          idrn=idrnv
*         revert to defaults for guessed lines
          ings=ingsdef
          lvgs=lvgsdef
          wvgs=wvgsdef
          fkgs=fkgsdef
          algs=algsdef
          amgs=amgsdef
          lgs=.true.
         else
          call dsepvar(filnm,5,dvstr,ivstr,cvstr,nvstr)
          if(nvstr.gt.1) then
*           extra information on the input line
            filnm=cvstr(1)
*           key letter to tell it what information
            if(cvstr(2)(1:1).eq.'G'.or.cvstr(2)(1:1).eq.'g') then
*             guesses for this region now need to be unscrambled
              lgs=.true.
              call vp_ationsep(cvstr,dvstr,ivstr,5,3,ings,lvgs)
*             cvstr(3) is now atom//ion
*             dvstr(4) is the rest wavelength guess
*             now get the correct wavelength by comparing with the table
              dwrst=1.0d20
              do i=1,nz
                if(lbz(i).eq.ings.and.lzz(i).eq.lvgs)then
                  ctwrst=abs(alm(i)-dvstr(4))
                  if(ctwrst.lt.dwrst) then
*                   and keep the table position for the other variables
                    ielmnt=i
                    wvgs=alm(i)
                    dwrst=ctwrst
                  end if
                end if
              end do
              fkgs=fik(ielmnt)
              algs=asm(ielmnt)
*             amgs is not needed, so any arbitrary value will do
              amgs=1.00794d0
*             Check that some sensible result obtained
              if(nvstr.le.2.or.dwrst.gt.1.0d19) then
*               revert to defaults
                ings='H '
                lvgs='I   '
                wvgs=1215.6701d0
                fkgs=0.4164d0
                algs=6.265d8
                amgs=1.00794d0
              end if
              write(6,*) 'Guessed lines are ',ings,lvgs,' ',wvgs
*
            end if
            if(cvstr(2)(1:1).eq.'S'.or.cvstr(2)(1:1).eq.'s') then
*             suppress guesses for this region
              lgs=.false.
            end if
           else
*           revert to defaults
            ings='H '
            lvgs='I   '
            wvgs=1215.6701d0
            fkgs=0.4164d0
            algs=6.265d8
            amgs=1.00794d0
            lgs=.true.
          end if  
          oldfilnm=filnm
          idrn=0
        end if
      endif
*
*     rfc 14.6.95: rationalized data input
      if(idrn.ne.irunold.or.filnm.ne.oldfilnm.or.ichunk.le.1
     1      .or.idrn.le.500) then
        if(idrn.lt.0) idrn=-idrn
        if(idrn.eq.0) idrn=1
        call vp_readfs(dhdata,dherr,dhrms,dhcont,ng,ngd,werr,
     :        idrn,filnm,3,ichunk)
       else
        if(idrn.gt.500) idrn=idrn-500
*       if you want a sped-up readin, then add 500 to spectrum # in file
        ngd=ngp
        itemp=ichunk-1
        do j=1,ngp
*         put original continuum back
          dhcont(j)=dhccont(j)
*         was =origcntss(j,itemp)
        end do
        write(6,*) 'using previously read data'
*       so need to copy some arrays which are set in vp_readfs
*       sigma scale variables depend only on data file
        nscl(ichunk)=nscl(itemp)
        do j=1,nscl(ichunk)
          ssclcf(j,ichunk)=ssclcf(j,itemp)
          wvalsc(j,ichunk)=wvalsc(j,itemp)
        end do
*       resolution, swres is sigma (wavelength diff polynomial)
*       nswres is number of coeffts UNLESS OTHERWISE SPECIFIED (TBD)
        nswres(ichunk)=nswres(itemp)
        do j=1,maxpoc
          swres(j,ichunk)=swres(j,itemp)
        end do
*       resolution, pixel profile:
        npinst(ichunk)=npinst(itemp)
        npin2(ichunk)=npin2(itemp)
        chprof(ichunk)=chprof(itemp)
        if(npinst(ichunk).ge.1) then
          do j=1,npinst(ichunk)
            pfinst(j,ichunk)=pfinst(j,itemp)
            wfinstd(j,ichunk)=wfinstd(j,itemp)
          end do
        end if
      end if
*     if managed to read some data, or was the same as the old stuff
*     then ngd (array length) is > 0
*
      if(ngd.gt.0) then
*
*     copy all the single array stuff to multifile area
*     WAVELENGTHS:
        if(nwco.le.0) nwco=maxwco
        nwcf2(ichunk)=nwco
        wcfty2(ichunk)=wcftype
        vacin2(ichunk)=vacind
        noffs2(ichunk)=noffs
        helcf2(ichunk)=helcfac
        do ii=1,maxwco
          wcfd(ii,ichunk)=wcf1(ii)
        end do
        ngp=ngd
*       read FITS data file OK
*       getscl has a check against failure to read coeffts, so
*       don't need to check again here. 
*       miscellaneous bits
        idwk(4)=ngd
        icrun(ichunk)=idrn
        idrun(ichunk)=idrn
        oldfilnm=filnm
        irunold=idrn
       else
        write(6,*) ' ** Zero length data!!'
        if(idrn.eq.0) goto 3142
        goto 991
      end if
*
*     set to plot continuum, not error
      ksplot(3)=1
      ksplot(2)=0
      ksplot(9)=0
*     reset scaling range min and max
      lxa=1
      lxb=ngp
*     ngp is the no. of data points in the continuum
      do ii=1,ngp
*       Keep a copy of the original continuum so it can be restored
        dhccont(ii)=dhcont(ii)
*       origcntss(ii,nfile)=dhcont(ii)
      end do
 991  return
      end
