*	FILE LIST GENERATOR
*
*	Take a concatenated Ly-a only fort.26 file, and generate
*	Lyman series fit files.
*	Uses the Ly-a region data for splitting the Ly-a line on the
*	basis of higher order fits.
*
*	User specifies which of a list he wants to make a file for, or 
*	automake for all above a given threshold.
*
*	Uses a collection of vp_ and rd_ routines
*
*
	include 'vp_sizes.f'
*
*	main variables
	parameter (kelm=5000)
	character*2 elm(kelm)
	character*4 lion(kelm)
	real zed(kelm),ezed(kelm),bval(kelm),ebval(kelm)
	real col(kelm),ecol(kelm)
	logical ldn(kelm),keep(kelm)
	integer ipelm(kelm)
	character*2 cpz(kelm),cpb(kelm),cpc(kelm)
	parameter (kreg=500)
	character*64 filnm(kreg)
	integer nord(kreg)
	real wlo(kreg),whi(kreg)
	integer ipreg(kreg)
	parameter (krout=20)
	integer kregout(krout)
*	workspace
	logical lfix
	character*1 cbase
	character*2 jpc,jpz,jpb
	character*2 ctemp,ctemp2
	double precision dtemp(kelm)
*
*       character input and separation variables
        character*60 cvstr(24)
        real rvstr(24)
        integer ivstr(24)
        character*132 inchstr
        common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
*	atomic data
	character*2 lbz
	character*4 lzz
	double precision alm,fik,asm 
	common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),nats
*	
*
*	Fix lower Ly-a option 
	lfix=.false.
	cbase='J'
	incha=ichar('A')
	inchz=ichar('Z')
	jpc='  '
	jpz='  '
	jpb='  '
*
*	read in the atomic data
	call vp_ewred(0)
*	
101	write(6,*) 'File with Ly-a fits? [f26.lis]'
	read(5,*) inchstr
	if(inchstr(1:1).eq.' ') then
	  inchstr='f26.lis'
	end if
	open(unit=25,file=inchstr,status='old',err=101)
	nreg=0
	nelm=0
*	read the whole file in one go
102	read(25,'(a)',err=199) inchstr
	call sepvar(inchstr,8,rvstr,ivstr,cvstr,nvstr)
*	bypass blank lines
	if(cvstr(1)(1:1).eq.' ') goto 102
*	something there, so process it
	if(cvstr(1)(1:2).eq.'%%') then
*	  Filename and region limit information
	  nreg=nreg+1
	  filnm(nreg)=cvstr(2)
	  nord(nreg)=ivstr(3)
	  wlo(nreg)=rvstr(4)
	  whi(nreg)=rvstr(5)
	 else
*	  Element, redshift etc
	  nelm=nelm+1
*	  Unpack ion if necessary
	  if(ichar(cvstr(1)(2:2)).ge.incha.and.
     :       ichar(cvstr(1)(2:2)).le.inchz) then
	    elm(nelm)=cvstr(1)(1:1)//' '
	    lion(nelm)=cvstr(1)(2:5)
	   else
	    if(cvstr(1)(2:2).eq.' ') then
	      elm(nelm)=cvstr(1)(1:1)//' '
	      lion(nelm)=cvstr(2)
*	      and shift the rest of the variables down one place
	      nvstr=nvstr-1
	      do j=2,nvstr
	        rvstr(j)=rvstr(j+1)
	        cvstr(j)=cvstr(j+1)
	        ivstr(j)=ivstr(j+1)
	      end do
	     else
	      elm(nelm)=cvstr(1)(1:2)	      
	      lion(nelm)=cvstr(1)(3:6)
	    end if
	  end if
*	  set z,b,n values
*
	  call vp_seppch(cvstr(2),zed(nelm),cpz(nelm))
*	  zed(nelm)=rvstr(2)
	  ezed(nelm)=rvstr(3)
	  call vp_seppch(cvstr(4),bval(nelm),cpb(nelm))
*	  bval(nelm)=rvstr(4)
	  if(cvstr(5)(1:4).ne.'****') then
	    ebval(nelm)=rvstr(5)
	   else
*	    overflow output format
	    ebval(nelm)=bval(nelm)
	    if(ebval(nelm).lt.15.0) ebval(nelm)=15.0
	  end if
	  call vp_seppch(cvstr(6),col(nelm),cpc(nelm))
*	  convert column densities to logs
	  if(col(nelm).lt.30.0) then
	    ecol(nelm)=rvstr(7)
	   else
	    temp=col(nelm)
	    col(nelm)=log10(temp)
*	    to an adequate approximation
	    ecol(nelm)=log10(temp+rvstr(7))-col(nelm)
	  end if
	end if
*	read another line from file
	goto 102
*
*	File exhausted
199	write(6,*) 'Input file read OK'
	if(nreg.le.0) goto 902
*	sort the element arrays by redshift
	do i=1,nelm
	  dtemp(i)=zed(i)
	end do
	call pda_qsiad(nelm,dtemp,ipelm)
*	ipelm(i) contains pointer to array element corresponding to ith value
*	in ascending order
*
*	sort regions by minimum wavelength
	do i=1,nreg
	  dtemp(i)=wlo(i)
	end do
	call pda_qsiad(nreg,dtemp,ipreg)
*
*	Minimum wavelength and redshift
	wvmin=wlo(ipreg(1))
	zmin=wvmin/1215.6701 - 1.0
*
*	Maximum redshift, and set ldn array to .true. (for not yet processed)
	zmax=-1.0e25
	do j=1,nelm
	  ldn(j)=.true.
	  keep(j)=.true.
	  if(zed(j).gt.zmax) zmax=zed(j)
	end do
	wvmax=(zmax+1.0)*1215.6701
*	now want minimum upper wavelength limit which is above wvmax
	temp=1.0e25
	do j=1,nreg
	  if(whi(j).gt.wvmax) then
	    if(whi(j).lt.temp) temp=whi(j)
	  end if
	end do
	wvmax=temp
	zmax=wvmax/1215.6701 - 1.0
*	Is it worth doing anything?
	rmin=1215.6701/1025.7223
	if(wvmax/wvmin.lt.rmin) goto 98
*
	write(6,*) 
     :    'HI column density limit, beta control? [14.5, free, J]'
	read(5,'(a)') inchstr
	call sepvar(inchstr,3,rvstr,ivstr,cvstr,nvstr)
	if(cvstr(1)(1:1).eq.' ') then
	  collim=14.5
	 else
	  collim=rvstr(1)
	end if
	if(cvstr(2)(1:2).eq.'fi'.or.cvstr(2)(1:2).eq.'FI') then
	  lfix=.true.
	 else
	  lfix=.false.
	end if
	if(cvstr(3)(1:1).ne.' '.and.nvstr.ge.3) then
	  cbase=cvstr(3)(1:1)
	  if(ichar(cbase).lt.incha.or.ichar(cbase).gt.inchz) then
	    cbase='J'
	  end if
	 else
	  cbase='J'
	end if
	inch1=ichar(cbase)
	inch2=incha
	write(6,*) 'Output filename? [f13.lis]'
	read(5,'(a)') inchstr
	if(inchstr(1:1).eq.' ') then
	  inchstr='f13.lis'
	end if
	open(unit=13,file=inchstr,status='unknown',err=900)
*	list candidates
	do jj=1,nelm
*	  might as well do it in ascending order
	  j=ipelm(jj)
*	  Need to be above column density limit, and redshift high
*	  enough that at least Ly-b included
	  if(col(j).gt.collim.and.(zed(j)+1.0)/rmin-1.0.gt.zmin.and.
     :      elm(j).eq.'H '.and.lion(j).eq.'I   '.and.ldn(j)) then
	    write(6,'(i6,2x,a2,a4,f10.6,f6.2,f7.3)')
     :        j,elm(j),lion(j),zed(j),bval(j),col(j)
*	    have a suitable line, so determine which chunk
*	    (if more than one, take the last)
	    ldn(j)=.false.
	    wrest=(1.0+zed(j))*1215.6701
	    jreg=0
	    do i=1,nreg
	      if(wlo(i).le.wrest.and.whi(i).ge.wrest) then
	        jreg=i
	      end if
	    end do
	    write(6,*) 'Region number ',jreg
*	    find range of index for lines in the chunk
	    jjlo=jj
	    zlo1=wlo(jreg)/1215.6701 - 1.0
	    write(6,*) 'Z low ',zlo1
	    do while(jjlo.gt.1.and.zed(ipelm(jjlo)).gt.zlo1)
	      jjlo=jjlo-1
	    end do
	    if(zed(ipelm(jjlo)).lt.zlo1) jjlo=jjlo+1
	    jjhi=jj
	    zhi1=whi(jreg)/1215.6701 - 1.0
	    write(6,*) 'Z hi ',zhi1
	    do while(jjhi.lt.nelm.and.zed(ipelm(jjhi)).lt.zhi1)
	      jjhi=jjhi+1
	    end do
	    if(zed(ipelm(jjhi)).gt.zhi1) jjhi=jjhi-1
	    write(6,*) 'Line numbers ',jjlo,jjhi
*	    see if there are other candidates in the chunk
	    wclo=wrest
	    wchi=wrest
	    do i=1,nelm
	      temp2=(1.0+zed(i))*1215.6701
	      if(col(i).gt.collim.and.(zed(i)+1.0)/rmin-1.0.gt.zmin.and.
     :           elm(i).eq.'H '.and.lion(i).eq.'I   '.and.ldn(i).and.
     :           temp2.ge.wlo(jreg).and.temp2.le.whi(jreg)) then
	        ldn(i)=.false.
	        write(6,'(i6,2x,a2,a4,f10.6,f6.2,f7.3)')
     :          i,elm(i),lion(i),zed(i),bval(i),col(i)
	        if(wclo.gt.temp2) wclo=temp2
	        if(wchi.lt.temp2) wchi=temp2
	      end if
	    end do
	    write(6,*) 'Region',jreg,wclo,wchi
	    nrout=1
	    kregout(1)=jreg
*	    redshift limits if fix option used:
	    zfxlo=wlo(jreg)/(rmin*1215.6701)-1.0
	    zfxhi=whi(jreg)/(rmin*1215.6701)-1.0
*	    now select Ly-b regions to span this lot
	    wclo=wclo/rmin
	    wchi=wchi/rmin
	    write(6,*) ' .. maps to',wclo,wchi
	    do i=1,nreg
	      if((wlo(i).le.wclo.and.whi(i).gt.wclo).or.
     :           (wlo(i).gt.wclo.and.whi(i).lt.wchi).or.
     :           (wlo(i).lt.wchi.and.whi(i).ge.wchi)) then
*	        stick it in
	        nrout=nrout+1
	        kregout(nrout)=i
	        write(6,*) i,wlo(i),whi(i)
	      end if
	    end do
*	    write out to screen and file
	    write(13,'(a)') ' *'
	    do i=1,nrout
	      kk=kregout(i)
	      nch=64
	      do while(filnm(kk)(nch:nch).eq.' '.and.nch.gt.2)
	        nch=nch-1
	      end do
	      write(6,*) ' %% ',filnm(kk)(1:nch),nord(kk),
     :           wlo(kk),whi(kk)
	      write(13,*) filnm(kk)(1:nch),nord(kk),
     :           wlo(kk),whi(kk)
	    end do
	    write(13,'(a)') ' *'
*	    now the lines in those regions, throwing out Ly-a's in the
*	    short wavelength regions which are likely to be Ly-b's
*	    corresponding to Ly-a's in the long wavelength region.
*
*	    range of jj corresponding to real Ly-a range
	    zlo2=(1.0+zlo1)/rmin-1.0
	    jjlo2=1
	    do while(zed(ipelm(jjlo2)).lt.zlo2.and.jjlo2.lt.nelm)
	      jjlo2=jjlo2+1
	    end do
	    zhi2=(1.0+zhi1)/rmin-1.0
	    jjhi2=jjlo2
	    do while(zed(ipelm(jjhi2)).lt.zhi2.and.jjlo2.lt.nelm)
	      jjhi2=jjhi2+1
	    end do
	    jjhi2=jjhi2-1
	    if(jjhi2.lt.jjlo2) then
*	      No Ly-b lines in the list .. curious.
	      write(6,*) 'No Ly-b lines found'
	     else
*	      go through the list
	      do jx=jjlo2,jjhi2
	        ix=ipelm(jx)
	        ztemp=(1.0+zed(ix))*rmin - 1.0
	        eztemp=ezed(ix)*rmin
	        if(eztemp.gt.0.00005) eztemp=0.00005
*	        estimated Ly-a column density from Ly-b: +log(fa/fb)
	        coltemp=col(ix)+0.7212
	        do jy=jjlo,jjhi
	          iy=ipelm(jy)
*	          is this line within tolerances?
	          zdiff=abs(ztemp-zed(iy))
	          eztmp2=ezed(iy)
	          if(eztmp2.gt.0.00005) eztmp2=0.00005
	          eztol=1.5*sqrt(eztemp**2+eztmp2**2)
	          if(zdiff.lt.eztol) then
*	            may have a match, check column density, width
	            ctol=1.5*sqrt(ecol(ix)**2+ecol(iy)**2)
	            cdiff=abs(coltemp-col(iy))
	            btol=1.5*sqrt(ebval(ix)**2+ebval(iy)**2)
	            bdiff=abs(bval(ix)-bval(iy))
	            if(bdiff.lt.btol.and.cdiff.lt.ctol) then
*	              drop the Ly-b
	              keep(ix)=.false.
	              write(6,*) 'Drop',zed(ix)
	            end if
	          end if
	        end do
	      end do
	    end if
*	    having done this, print out values
*	    Ly-a lines in long wavelength region
	    do jx=jjlo,jjhi
	      ix=ipelm(jx)
	      if(elm(ix).eq.'H '.and.keep(ix)) then
	       write(13,'(1x,a2,a4,f8.3,2x,f12.6,2x,f11.2)') 
     :          elm(ix),lion(ix),col(ix),zed(ix),bval(ix)
	      end if
	    end do
*	    and shorter wavelength regions
	    if(nrout.gt.1) then
	      do k=2,nrout
	        kr=kregout(k)
	        zxlo=wlo(kr)/1215.6701 - 1.0
	        zxhi=whi(kr)/1215.6701 - 1.0
	        ipx=1
	        do while(zed(ipelm(ipx)).lt.zxlo.and.ipx.lt.nelm)
	          ipx=ipx+1
	        end do
	        do while(zed(ipelm(ipx)).le.zxhi.and.ipx.lt.nelm)
	          ix=ipelm(ipx)
	          if(elm(ix).eq.'H '.and.keep(ix)) then
	            if(.not.lfix.or.(zed(ix).ge.zfxlo.and.
     :                  zed(ix).le.zfxhi)) then
	              jpc='  '
	              jpz='  '
	              jpb='  '
	             else
	              jpc=char(inch1)//char(inch2)
	              inch2=inch2+1
	              if(inch2.gt.inchz) then
	                inch2=incha
	                inch1=inch1+1
	              end if
	              jpz=char(inch1)//char(inch2)
	              inch2=inch2+1
	              if(inch2.gt.inchz) then
	                inch2=incha
	                inch1=inch1+1
	              end if
	              jpb=char(inch1)//char(inch2)
	              inch2=inch2+1
	              if(inch2.gt.inchz) then
	                inch2=incha
	                inch1=inch1+1
	              end if
	            end if
	            write(13,'(1x,a2,a4,f8.3,a2,f12.6,a2,f11.2,a2)') 
     :              elm(ix),lion(ix),col(ix),jpc,zed(ix),jpz,
     :              bval(ix),jpb
	          end if
	          ipx=ipx+1
	        end do
*	        just in case reaches the end of the list
	        if(ipx.eq.nelm.and.zed(ipelm(ipx)).le.zxhi) then
	          ix=ipelm(ipx)
	          if(.not.lfix.or.(zed(ix).ge.zfxlo.and.
     :                zed(ix).le.zfxhi)) then
	            jpc='  '
	            jpz='  '
	            jpb='  '
	           else
	            jpc=char(inch1)//char(inch2)
	            inch2=inch2+1
	            if(inch2.gt.inchz) then
	              inch2=incha
	              inch1=inch1+1
	            end if
	            jpz=char(inch1)//char(inch2)
	            inch2=inch2+1
	            if(inch2.gt.inchz) then
	              inch2=incha
	              inch1=inch1+1
	            end if
	            jpb=char(inch1)//char(inch2)
	            inch2=inch2+1
	            if(inch2.gt.inchz) then
	              inch2=incha
	              inch1=inch1+1
	            end if
	          end if
	          if(elm(ix).eq.'H '.and.keep(ix)) then
	            write(13,'(1x,a2,a4,f8.3,a2,f12.6,a2,f11.2,a2)') 
     :              elm(ix),lion(ix),col(ix),jpc,zed(ix),jpz,
     :              bval(ix),jpb
	          end if
	        end if
	      end do
	    end if
*	    contimuum parameters for the regions involved:
	    do k=1,nrout
	      kk=kregout(k)
	      ll=1
	      l=ipelm(ll)
	      do while ((elm(l).ne.'<>'.or.
     :                (1.0+zed(l))*1215.67.le.wlo(kk))
     :                     .and.ll.lt.nelm)
	        ll=ll+1
	        l=ipelm(ll)
	      end do
*	      gets here if jj.eq.nelm, or elm(l).eq.'<>' and wavelength
*	      above lower limit, so if have a continuum value in region, 
*             write it out:
	      if(elm(l).eq.'<>'.and.(1.0+zed(l))*1215.67.lt.whi(kk))
     :            then
	        if(lfix.and.k.ne.1) then
*	          must fix continuum, since lower redshift Ly-a's fixed
	          ctemp=cpz(l)
	          ctemp2=cpz(l)
	         else
	          ctemp=cpc(l)
	          ctemp2=cpb(l)
	        end if 
*	        write out continuum line, with the fixed conditions intact
	        write(13,'(1x,a2,a4,f8.3,a2,f12.6,a2,f11.2,a2)') 
     :          elm(l),lion(l),col(l),ctemp,zed(l),cpz(l),
     :          bval(l),ctemp2
	      end if
	    end do
	    write(13,*) ' '	    	      
	  inch1=ichar('J')
	  inch2=incha
	  end if
	end do
*      
98	continue
	write(6,*) 'Wavelength limits',wvmin,wvmax
*	might add heavy element overlap if you want to	  	  
909	stop
900	write(6,*) 'Unable to open output file'
	goto 909
902	write(6,*) 'No spectral region information in input file'
	goto 909
	end	
	subroutine vp_seppch(inchstr,vreal,ichar)
	character*(*) inchstr
	character*(*) ichar
	lvstr=len(inchstr)
*	seek last non-space character 
	jx=lvstr
	do while (inchstr(jx:jx).eq.' '.and.jx.gt.1)
	  jx=jx-1
	end do
*
*	split into parameter and flag (up to two characters)
	if(jx.ge.2) then
*	  ipind may be two characters
	  ichar=inchstr(jx-1:jx)
	  jlast=jx-2
	  if(ichar(1:1).eq.'.'.or.ichar(1:1).eq.'0'.or.
     :       ichar(1:1).eq.'1'.or.ichar(1:1).eq.'2'.or.
     :       ichar(1:1).eq.'3'.or.ichar(1:1).eq.'4'.or.
     :       ichar(1:1).eq.'5'.or.ichar(1:1).eq.'6'.or.
     :       ichar(1:1).eq.'7'.or.ichar(1:1).eq.'8'.or.
     :       ichar(1:1).eq.'9') then
*	    move up a character
	    ichar=ichar(2:2)//' '
	    jlast=jlast+1
*	    check if it is a number
	    if(ichar(1:1).eq.'.'.or.ichar(1:1).eq.'0'.or.
     :         ichar(1:1).eq.'1'.or.ichar(1:1).eq.'2'.or.
     :         ichar(1:1).eq.'3'.or.ichar(1:1).eq.'4'.or.
     :         ichar(1:1).eq.'5'.or.ichar(1:1).eq.'6'.or.
     :         ichar(1:1).eq.'7'.or.ichar(1:1).eq.'8'.or.
     :         ichar(1:1).eq.'9') then
	      ichar='  '
	      jlast=jx
	    end if
	  end if
	  read(inchstr(1:jlast),*,err=999) vreal
	 else
	  if(jx.eq.1) then
*	    either a single character or a number
	    ichar=inchstr(1:1)//' '
	    if(ichar(1:1).eq.'.'.or.ichar(1:1).eq.'0'.or.
     :         ichar(1:1).eq.'1'.or.ichar(1:1).eq.'2'.or.
     :         ichar(1:1).eq.'3'.or.ichar(1:1).eq.'4'.or.
     :         ichar(1:1).eq.'5'.or.ichar(1:1).eq.'6'.or.
     :         ichar(1:1).eq.'7'.or.ichar(1:1).eq.'8'.or.
     :         ichar(1:1).eq.'9') then
	      read(inchstr,'(i1)',err=999) item
	      vreal=item
	      ichar='  '
	     else
	      vreal=0.0
	    end if
	   else
*	    this bit should never be used, unless line shortened:
	    vreal=0.0
	    ichar='  '
	  end if
	end if
100	return
999	vreal=0.0
	ichar=inchstr(1:1)//' '
	goto 100
	end
