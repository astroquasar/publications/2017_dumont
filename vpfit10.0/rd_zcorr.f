      subroutine rd_zcorr(zed,cind)
      implicit none
*     corrects the wavelength coefficients to rest given the redshift
*     note that the coefficients are held in common, and the 
*     routine parameter zed is DOUBLE PRECISION
      include 'vp_sizes.f'
*     In:
      double precision zed
      character*(*) cind
*     Local
      integer i
      double precision zdp1
*     wavelength parameter sets
      character*8 wcfty2(maxnch)
      character*4 vacin2(maxnch)
      double precision helcf2(maxnch)
      integer nwcf2(maxnch),noffs2(maxnch)
      common/vpc_wcpars2/wcfty2,helcf2,nwcf2,noffs2,vacin2
*     general wavelength coefficients
      double precision wcfd(maxwco,maxnch)
      common/vpc_wcfd/wcfd
*     
      integer nwco
      double precision wcf1(maxwco)
      common/vpc_wavl/wcf1,nwco
*     
*     backed up wavelength coefficients
      double precision bwcfd(maxwco),bwcf1(maxwco)
      common/rd_wbak/bwcfd,bwcf1
*
      zdp1=zed+1.0d0
      if(cind(1:1).eq.'b') then
*       use backup coefficients
        do i=1,maxwco
          wcf1(i)=bwcf1(i)
          wcfd(i,1)=bwcfd(i)
        end do
      end if
      if(cind(1:1).eq.'n') then
*       make backup coefficients
        do i=1,maxwco
          bwcf1(i)=wcf1(i)
          bwcfd(i)=wcfd(i,1)
        end do
      end if
*     otherwise ignore backup
      if(zdp1.le.0.0d0) then
        write(6,*) 'Redshift ',zed,' ignored'
        zdp1=1.0d0
      end if
      if(wcfty2(1)(1:3).eq.'log') then
        wcfd(1,1)=wcfd(1,1)-dlog10(zdp1)
        wcf1(1)=wcf1(1)-dlog10(zdp1)
       else
        if(wcfty2(1)(1:4).eq.'cheb') then
          do i=3,nwcf2(1)
            wcfd(i,1)=wcfd(i,1)/zdp1
          end do
          if(nwco.gt.2) then
            do i=3,nwco
              wcf1(i)=wcf1(i)/zdp1
            end do
          end if
         else
*         assume polynomial
          do i=1,nwcf2(1)
            wcfd(i,1)=wcfd(i,1)/zdp1
          end do
          do i=1,nwco
            wcf1(i)=wcf1(i)/zdp1
          end do
        end if
      end if
      return
      end
