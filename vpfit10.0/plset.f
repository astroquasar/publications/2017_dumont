      subroutine plset(ind)
      implicit none
      include 'vp_sizes.f'
*
      integer ind
*     local:
      integer inp
      integer i,j,jb,jj,k
      integer natv,lcsrc
      real rpg
      double precision ddum,rdb
      double precision wavxx,col,bval,zval
      double precision dwrst,rstwxx,ctwrst
      double precision wl
*
      character*132 chxx
      character*2 ixc
      character*1 ixc1,ixc2,chtm
* 
      integer nvxx
      character*60 cvxx(8)
      integer ivxx(8)
      real rvpg(8)
*     functions
      integer lastchpos
*     Common:
      integer nwco
      double precision wcf1(maxwco)
      common/vpc_wavl/wcf1,nwco
      integer l1,l2,lc1,lc2
      real fampg
      common/ppam/l1,l2,lc1,lc2,fampg
*     input streams
      integer inputc
      common/rdc_inputc/inputc
*     text label for a curve
      integer icoltext
      character*24 cpgtext
      real xtextpg,ytextpg
      common/pgtexts/xtextpg,ytextpg,cpgtext,icoltext
*     min and max yscales by hand, and default lower level 
*     nyuse is no times to use set before reset (cursor use)
      integer nyuse
      real ylowhpg,yhihpg,yminsetpg
      common/pgylims/ylowhpg,yhihpg,yminsetpg,nyuse
*     pgplot overplot with bias
      integer ipgov
      logical plgscl
      real pgbias
      common/pgover/ipgov,pgbias,plgscl
*     pgplot attributes: data, error, continuum, axes, ticks, residual
*	  1 color indx; 2 line style; 3 line width; 
*	  4 curve(0), hist(1), bars(2); 5 - 9 reserved;
*	  10 special features:
*         (10,1) = PGPLOT mode for PGBAND (0 is default)
*	  (10,4) = 0 suppress x=0,y=0; 10 plot x=0, not y; 11 - plot x=0 & y=0
*	  (10,5) = 0  normal plot; 10 with offset residuals; 
*                  11 residuals & ticks [when written]
      integer ipgatt(10,9)
      common/vpc_pgattrib/ipgatt
*     plot array pointers (consistent with ipgatt)
*     0=don't,1=do for 1:data,2:error,3:cont,6:resid,9:rms
      integer ksplot(9)
      common/vpc_ksplot/ksplot
*     plot channel open flags
      integer ipgopen,ipgflag
      common/pgplotv/ipgopen,ipgflag
*
*     ipgtgrk=1 if Greek letters, else 0 (-1 no symbols); 
*     yuptkpg(def=0.9) is fract y-axis for top of tick,
*     ylentkpg (def=0.07) fract y for length of tick
      integer ipgtgrk
      real yuptkpg,ylentkpg
      common/vpc_tick/ipgtgrk,yuptkpg,ylentkpg
*
      character*60 pglcapt
      real chszoldpg,chszpg
      common/rdc_pglcapt/pglcapt,chszoldpg,chszpg
      integer ipgnx,ipgny
      character*60 pglchar(3)
      common/pgnumba/ipgnx,ipgny,pglchar
*     plot/print velocity scale?
      logical lvel
      real wcenpg,vellopg,velhipg,zp1refpg
      common/vpc_pgvels/wcenpg,vellopg,velhipg,lvel,zp1refpg
*     ascii prinout instead of plot
      logical lascii,lplottoo
      common/vpc_pgascii/lascii,lplottoo
      integer itflag
      common/tickfile/itflag
      real residpg, rshifpg
      common / vpc_resid / residpg, rshifpg
*     Central wavelength parameters
      character*2 atom
      character*4 ion
      character*2 indcol,indb,indz
      integer nz
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),nz
*     line list variables
      double precision zmark
      character*132 chmarkfl
      logical lmarkl,lmarkto
      common/rdc_lmarklv/zmark,chmarkfl,lmarkl,lmarkto
*     file steered tick marks
      logical ltvtick
      character*132 chtvtick
      common/rdc_tvtick/chtvtick,ltvtick
*     line on plot?
      logical lgline
      real pglxy(4)
      integer kpglxy(3)
      common/vpc_plline/lgline,pglxy,kpglxy
*
      integer nverbose
      common/vp_sysmon/nverbose
*     light velocity
      real ccvacpg
      common/vpc_sgconst/ccvacpg
*     file steered extras
      character*64 pgfxfile
      common/rdc_pgfxfile/pgfxfile
*     source path
      character*132 csrcpath
      common/vpc_srcpath/csrcpath
*
*
      ixc1=' '
      ixc2=' '
      lgline=.false.
*     initially input from a file of plot parameters
*     try to open 'vp_splot.dat' file
      chxx=' '
      call getenv('VPFPLOTS',chxx)
      if(chxx(1:1).eq.' ') then
        if(csrcpath(1:1).eq.' ') then
          chxx='vp_splot.dat'
         else
*         use the distibuted defaults
          lcsrc=lastchpos(csrcpath)
          if(csrcpath(lcsrc:lcsrc).eq.'/') then
            chxx=csrcpath(1:lcsrc)//'vp_splot.dat'
           else
            chxx=csrcpath(1:lcsrc)//'/vp_splot.dat'
          end if
        end if
      end if
      open(unit=30,file=chxx,status='old',err=971)
*     set input unit to 30, so reads the file
      inp=30
      goto 972
*     no VPFPLOTS or vp_splot.dat file
 971  inp=inputc
 972  continue

c     general overplot values (ipgov=0 no overplot)
      ipgov=0
      pgbias=0.0
      if(ipgopen.le.0) chszpg=1.0
c     tick file flag reset
      itflag=0
*
      ind=1
*     this structure switches input if necessary
*                (it works now!)
 773  continue
 1    if(inp.ne.30) then
        write(6,*) 'plot parameter? (type he for options list)'
        write(6,'(''> '',$)')
      end if
      read(inp,'(a)',end=771) chxx
*     ignore comment lines
      if(chxx(1:1).eq.'!') goto 773
*     and blank lines IF inp.eq.30 - don't want unconstrained non-plot
      if(inp.eq.30.and.chxx(1:1).eq.' ') goto 773
      go to 772
 771  continue
      if ( inp .eq. 30 ) then
        close ( unit=30 )
        inp = inputc
        go to 773
       else
        if(inp.ne.5) then
          inputc=5
          inp=5
          goto 773
         else
          go to 915
        end if
      end if
 772  continue
      call sepvar(chxx,8,rvpg,ivxx,cvxx,nvxx)
      ixc=cvxx(1)(1:2)
      ixc1=cvxx(1)(1:1)
      ixc2=cvxx(1)(2:2)
      if(inp.ne.30.or.nverbose.gt.4) then
*       suppress writeout of presets, unless in moderate verbose mode
        write(6,'(1x,a2,7(1x,a4))') (cvxx(jj)(1:4),jj=1,nvxx)
      end if
*
*     AL: plot all (data,cont,rms)
      if(ixc.eq.'al'.or.ixc.eq.'AL') then
        residpg=0.0
        ksplot(1)=1
        ksplot(9)=1
        ksplot(3)=1
        goto 1
      end if
*
*     AS: ASCII file out instead of plot
      if(ixc.eq.'as'.or.ixc.eq.'AS') then
        if(lascii) then
          lascii=.false.
          write(6,*) ' PGPLOT used'
         else
          lascii=.true.
          lplottoo=.true.
          write(6,*) ' ASCII output to fort.17'
        end if
        goto 1
      end if
*
*     AT: line attributes
      if(ixc.eq.'at'.or.ixc.eq.'AT') then
        if(nvxx.le.1) then
*         give help list
          write(6,*) ' at (curve) (attribute) (value)'
          write(6,*) ' curve = data, error, rms, continuum, axes,',
     :                ' ticks, residual, (fit region)'
          write(6,*) ' attribute = colour, style, width of lines'
          write(6,*) ' value = pgplot value'
*          write(6,*) ' attr = type, value = bars for ranges'
          write(6,*) ' attr = type, value = curve or line for lines'
          write(6,*) ' attr = type, value = hist for histograms'
          goto 1
        end if
*       default to data if something wrong, user will notice soon enough!
        natv=1
        if(cvxx(2)(1:1).eq.'d'.or.cvxx(2)(1:1).eq.'D')  natv=1
        if(cvxx(2)(1:1).eq.'e'.or.cvxx(2)(1:1).eq.'E')  natv=2
        if(cvxx(2)(1:1).eq.'c'.or.cvxx(2)(1:1).eq.'C')  natv=3
        if(cvxx(2)(1:1).eq.'a'.or.cvxx(2)(1:1).eq.'A')  natv=4
        if(cvxx(2)(1:1).eq.'t'.or.cvxx(2)(1:1).eq.'T')  natv=5
        if(cvxx(2)(1:1).eq.'r'.or.cvxx(2)(1:1).eq.'R')  natv=6
*       fit region marker (if used)
        if(cvxx(2)(1:1).eq.'f'.or.cvxx(2)(1:1).eq.'F')  natv=7
*       reset color for cursor
        if(cvxx(2)(1:1).eq.'x'.or.cvxx(2)(1:1).eq.'X')  natv=8
*       fluctuation array is 9
        if(cvxx(2)(1:2).eq.'rm'.or.cvxx(2)(1:2).eq.'RM'.or.
     :     cvxx(2)(1:1).eq.'s'.or.cvxx(2)(1:1).eq.'S'.or.
     :     cvxx(2)(1:1).eq.'9') natv=9
        if(nvxx.lt.3) then
*         set mindless defaults
          do j=1,3
            ipgatt(j,natv)=1
          end do
          ipgatt(4,natv)=0
         else
*         unscramble list
          do k=3,nvxx,2
            if(cvxx(k)(1:1).eq.'c'.or.cvxx(k)(1:1).eq.'C') then
*	      colour index
*	      ajc 15-nov-93  allow zero - then background (invisible)
*	      -ve for ticks will then give no numbers...
*	      ipgatt(1,natv)=max0(1,ivxx(k+1))
              if(cvxx(k+1)(1:1).eq.'?') then
                write(6,*) ' 0 - background (b or w)'
                write(6,*) ' 1 - foreground (w or b)'
                write(6,*) ' 2 - red'
                write(6,*) ' 3 - green'
                write(6,*) ' 4 - dark blue'
                write(6,*) ' 5 - turquoise'
                write(6,*) ' 6 - lilac'
                write(6,*) ' 7 - yellow'
                write(6,*) ' 8 - orange'
                write(6,*) ' 9 - light green'
                write(6,*) '10 - blue green'
                write(6,*) '11 - blue'
                write(6,*) '12 - purple'
                write(6,*) '13 - magenta'
                write(6,*) '14 - dark grey'
                write(6,*) '15 - light grey'
                goto 1
              end if
              ipgatt(1,natv)=ivxx(k+1)
            end if
            if(cvxx(k)(1:1).eq.'t'.or.cvxx(k)(1:1).eq.'T') then
*             bars, histogram or line data
              chtm=cvxx(k+1)(1:1)
              if(chtm.eq.'h'.or.chtm.eq.'H') then
                ipgatt(4,natv)=1
               else
                if(chtm.eq.'b'.or.chtm.eq.'B') then
*                 bars at data positions
                  ipgatt(4,natv)=2
                 else
*	          default is lines
                  ipgatt(4,natv)=0
                end if
              end if
            end if
            if(cvxx(k)(1:1).eq.'L'.or.cvxx(k)(1:1).eq.'l') then
              cvxx(k)(1:1)=cvxx(k)(2:2)
            end if
            if(cvxx(k)(1:1).eq.'s'.or.cvxx(k)(1:1).eq.'S') then
*	      line style
              if(ivxx(k+1).eq.0.and.cvxx(k+1)(1:1).ne.' ') then
                if(cvxx(k+1)(1:2).eq.'co') ivxx(k+1)=1
                if(cvxx(k+1)(1:2).eq.'da') ivxx(k+1)=2
                if(cvxx(k+1)(1:2).eq.'dd') ivxx(k+1)=3
                if(cvxx(k+1)(1:2).eq.'do') ivxx(k+1)=4
                if(cvxx(k+1)(1:2).eq.'ds') ivxx(k+1)=5
              end if
              ipgatt(2,natv)=max0(1,ivxx(k+1))
            end if
            if(cvxx(k)(1:1).eq.'w'.or.cvxx(k)(1:1).eq.'W') then
c             line width
              ipgatt(3,natv)=max0(1,ivxx(k+1))
            end if
          end do
        end if
        goto 1
      end if
*     C:
      if(ixc(1:1).eq.'c'.or.ixc(1:1).eq.'C') then
*
*       CA - caption
        if(ixc(2:2).eq.'a'.or.ixc(2:2).eq.'A') then
          pglcapt=cvxx(2)
          pglchar(3)=cvxx(2)
          goto 1
        end if
*
*       CH for character size as multiple default
        if(ixc(2:2).eq.'h'.or.ixc(2:2).eq.'H') then
          chszoldpg=chszpg
          if(rvpg(2).gt.0.0) then
            chszpg=rvpg(2)
           else
            chszpg=1.0
          end if
          goto 1
        end if
*
*       CM cursor mode
        if(ixc(2:2).eq.'M'.or.ixc(2:2).eq.'m') then
          if(ivxx(2).lt.0.or.ivxx(2).gt.7) ivxx(2)=7
          ipgatt(10,1)=ivxx(2)
          goto 1
        end if
*
*       CO plot continuum
        if(ixc(2:2).eq.'o'.or.ixc(2:2).eq.'O') then
*          residpg=0.0
          ksplot(3)=1
          goto 1
        end if
      end if
*
c     ER plot error
      if(ixc.eq.'er'.or.ixc.eq.'ER') then
        residpg=0.0
        ksplot(2)=1
        goto 1
      end if
*     FX file steered extras
      if(ixc.eq.'fx'.or.ixc.eq.'FX'.or.ixc.eq.'an'.or.
     :          ixc.eq.'AN') then
        pgfxfile=cvxx(2)
        goto 1
      end if
*     HE help list
 916  if(ixc.eq.'he'.or.ixc.eq.'HE'.or.ixc(1:1).eq.'?') then
        write(6,*) 'format is cc.. a..a b..b ...'
        write(6,*) 'sc - set scale; lo - low chan; hi - high chan'
        write(6,*) 'sn - scale range min chan; sx - max; ln; hx'
        write(6,*) 'ym - min y; yx - max y; yb - default baseline'
        write(6,*) 'qu(it); al(l); co(ntinuum); er(ror);',
     :       ' re(sidual); rm(s array)'
        write(6,*) 'no co (er, rm, re) - do not plot cont (etc)'
        write(6,*) 'wl - low wavelength; wh - high wavelength',
     1       ' (wn,wx scale ranges also)'
        write(6,*) 'nu(ll), nx - nx/page, ny - ny/page'
        write(6,*) 'la(bel); ca(ption); te(xt); ch(aracter) size'
        write(6,*) 'sp - spectrum only, ov # - overplot (bias=#)'
        write(6,*) 'at - set attributes (type at for help);'
        write(6,*) 'tf - tick marks from file'
        write(6,*) 'gk - greek symbols over ticks; ng - numbers'
        write(6,*) 'ns - no symbols, tl - tick top, len (fract)'
        write(6,*) 've - velocity limits; wc - central wavelength'
        write(6,*) 'rz - reference redshift for velocity plots'
        write(6,*) 'wa - plot on wavelength scale'
        write(6,*) 'as - ASCII to fort.17 (toggles back to PGPLOT)'
        write(6,*) 'zs - suppress zero lines; zx - x=0 line;'//
     :      ' zy y=0 line; zb - draw both'
        write(6,*) 'cm (n) - cursor style (when used)'
        write(6,*) '<CR> - plot on last device used'
        goto 1
      end if
*     KI : totally destroy plot window
      if(ixc.eq.'ki'.or.ixc.eq.'KI') then
        if(ipgopen.ne.0) then
*         close plot device
          call pgask(.false.)
          call vp_pgend
*         and tell the rest of the program you have done so
          ipgopen=0
          ind=-1
          goto 3
        end if
      end if
*
*     LA label for plot
      if(ixc.eq.'la'.or.ixc.eq.'LA') then
        if(nvxx.le.1) then
          write(6,'(''x-label? > '',$)')
          read(inp,'(a)') pglchar(1)
          write(6,'(''y-label? > '',$)')
          read(inp,'(a)') pglchar(2)
          write(6,'(''Caption? > '',$)')
          read(inp,'(a)') pglchar(3)
         else
          pglchar(1)=cvxx(2)
          pglchar(2)=cvxx(3)
          pglchar(3)=cvxx(4)
        end if
        goto 1
      end if
*     LI: draw a line on the plot, from (1,2) to (3,4), style 5, color 6
      if(ixc.eq.'li'.or.ixc.eq.'LI') then
        if(nvxx.gt.1) then
          lgline=.true.
          do jj=1,4
            pglxy(jj)=rvpg(jj+1)
          end do
          kpglxy(1)=max(1,ivxx(6))
          kpglxy(2)=max(1,ivxx(7))
          kpglxy(3)=max(1,ivxx(8))
        end if
        goto 1
      end if
*     GK for greek symbols over ticks
      if(ixc.eq.'gk'.or.ixc.eq.'GK') then
        ipgtgrk=1
        goto 1
      end if
c     HI high channel set for plot
      if(ixc.eq.'hi'.or.ixc.eq.'HI'.or.
     :     ixc.eq.'hx'.or.ixc.eq.'HX') then
        l2=ivxx(2)
        if(ixc.eq.'hx'.or.ixc.eq.'HX') then
          lc2=l2
        end if
        goto 1
      end if
c     LO low channel set for plot
      if(ixc.eq.'lo'.or.ixc.eq.'LO'.or.
     :     ixc.eq.'ln'.or.ixc.eq.'LN' ) then
        l1=ivxx(2)
        if(ixc.eq.'ln'.or.ixc.eq.'LN' ) then
          lc1=l1
        end if
        goto 1
      end if
c     NG for not greek = numbers
      if(ixc.eq.'ng'.or.ixc.eq.'NG') then
        ipgtgrk=0
        goto 1
      end if
*     No da,er,co,res,rms
      if(ixc.eq.'no'.or.ixc.eq.'NO') then
        if(cvxx(2)(1:1).eq.'d') ksplot(1)=0
        if(cvxx(2)(1:1).eq.'e') ksplot(2)=0
        if(cvxx(2)(1:1).eq.'c') ksplot(3)=0
        if(cvxx(2)(1:1).eq.'re') ksplot(6)=0
        if(cvxx(2)(1:1).eq.'rm') ksplot(9)=0
        goto 1
      end if
c     NS for no symbols
      if(ixc.eq.'ns'.or.ixc.eq.'NS') then
        ipgtgrk=-1
        goto 1
      end if
c
c     NU(LL) no operation 
      if(ixc.eq.'nu'.or.ixc.eq.'NU') goto 1
c
c     NX, NY set
      if(ixc.eq.'nx'.or.ixc.eq.'NX') then
        ipgnx=max0(1,ivxx(2))
        goto 1
      end if
      if(ixc.eq.'ny'.or.ixc.eq.'NY') then
        ipgny=max0(1,ivxx(2))
        goto 1
      end if
c     OV(erplot the data array)
      if(ixc.eq.'ov'.or.ixc.eq.'OV') then
        ipgov=1
        pgbias=rvpg(2)
        goto 1
      end if
*
*     PG pgplot used
 1607 if(ixc.eq.'pg'.or.ixc.eq.'PG'.or.ixc.eq.'  ') then
        if(lascii) goto 3
*
        if (lvel) then
*         sort out plot limits for the velocity parameters
          rpg=vellopg/ccvacpg
          rdb=dble(wcenpg*sqrt((1.0+rpg)/(1.0-rpg)))
          call chanwav(rdb,ddum,1.0d-3,100)
          l1=int(ddum)
          rpg=velhipg/ccvacpg
          rdb=dble(wcenpg*sqrt((1.0+rpg)/(1.0-rpg)))
          call chanwav(rdb,ddum,1.0d-3,100)
          l2=int(ddum)
        end if
*
        if(ipgopen.le.0) then
*         open plot device
          call vp_pgbegin(ipgnx,ipgny,' ')
          call pgask(.false.)
        end if
*       make sure size is not too small
        if(chszpg.lt.1.0) chszpg=1.0
        if(abs(chszpg-chszoldpg).gt.0.01) then
*         change character size
          call pgsch(chszpg)
        end if
        ipgopen=1
        ipgflag=1
        goto 3
      end if
*     close PGPLOT
 1619 if(ixc.eq.'ps'.or.ixc.eq.'PS') then
        if(ipgopen.gt.0) then
*         switch off prompt - for some reason xwindows advances to
*         a new page before quitting!
          call pgask( .false. )
          call vp_pgend
        end if
        ipgopen=-1
        ipgflag=0
        goto 3
      end if
c     QU set quit flag if required
      if(ixc.eq.'qu'.or.ixc.eq.'QU') then
        ind=-1
        goto 3
      end if
*
*     RE plot residuals
      if( ixc .eq. 're' .or. ixc .eq. 'RE' ) then
*       no value switches on unit scaling, +ve gives scaling, -ve turns off
*	if zero entered explicitly, rather than blank, turns off
        if (rvpg(2) .eq. 0.0 .and. cvxx(2)(1:1) .eq. '0') then
          rvpg(2) = -1.0
        end if
        if ( rvpg( 2 ) .ne. 0.0 ) then
          residpg = rvpg( 2 )
          rshifpg = rvpg( 3 )
         else
          residpg = 0.1
          rshifpg = 0.8
        end if
        ksplot(6)=1
        go to 1
      end if
*
*     RM: plot RMS
      if(ixc.eq.'rm'.or.ixc.eq.'RM') then
        ksplot(9)=1
        goto 1
      end if
*     RZ: reference redshift for stacked line plots
      if(ixc.eq.'rz'.or.ixc.eq.'RZ') then
*       RFC 21.04.04 allow for blueshifts
        if(rvpg(2).le.-1.0) then
          zp1refpg=1.0
         else
          if(rvpg(2).gt.50.0) then
*	    assume it is a wavelength, and Ly-a for now
            zp1refpg=rvpg(2)/1215.6701
           else
            zp1refpg=rvpg(2)+1.0
          end if
        end if
        goto 1
      end if
*
*     SC scale factor for plot
      if(ixc.eq.'sc'.or.ixc.eq.'SC') then
        if(rvpg(2).le.0.0) then
          fampg=1.0
         else
          fampg=rvpg(2)
        end if
        goto 1
      end if
c     SN scale channel minimum
      if(ixc.eq.'sn'.or.ixc.eq.'SN') then
        lc1=ivxx(2)
        goto 1
      end if
c     SP suppress error plot
      if(ixc.eq.'sp'.or.ixc.eq.'SP') then
        residpg=0.0
        ksplot(1)=1
        do j=2,9
          ksplot(j)=0
        end do
        goto 1
      end if
c     SX scale channel maximum
      if(ixc.eq.'SX'.or.ixc.eq.'sx') then
        lc2=ivxx(2)
        goto 1
      end if
c     TE: set text value, and position for labelling curve
      if(ixc.eq.'TE'.or.ixc.eq.'te') then
        cpgtext=cvxx(2)(1:24)
        xtextpg=rvpg(3)
        ytextpg=rvpg(4)
        icoltext=ivxx(5)
        if(cpgtext(1:1).eq.' ') then
          write(6,*) ' Text cleared'
          write(6,*) ' to reset use TE (text) (xstart) (ystart)'
       end if
       goto 1
      end if
c     TF: tick marks from file
      if(ixc.eq.'tf'.or.ixc.eq.'TF') then
c       set a tick flag
        itflag=1
*       and need to over-ride previous tick variables, if set
        lmarkl=.false.
        ltvtick=.false.
        goto 1
      end if
*
*     TL: tick mark top and length
      if(ixc.eq.'TL'.or.ixc.eq.'tl') then
        if(rvpg(2).gt.0.0) then
          yuptkpg=rvpg(2)
         else
          yuptkpg=0.90
        end if
        if(rvpg(3).gt.0.0) then
          ylentkpg=rvpg(3)
         else
          ylentkpg=0.07
       end if
       goto 1
      end if
*     TV: tickmarks from named file, on a velocity plot
      if(ixc.eq.'tv'.or.ixc.eq.'TV') then
        if(cvxx(2)(1:1).ne.' ') then
*         make sure there is a filename
          chtvtick=chxx
          ltvtick=.true.
         else
*         disable ticks
          chtvtick=' '
          ltvtick=.false.
        end if
        goto 1
      end if
*
*     VE set flag to plot on velocity scale:
      if(ixc.eq.'ve'.or.ixc.eq.'VE') then
        lvel=.true.
*       check for lack of space between command and value
        if(rvpg(2).eq.0.0) then
          chxx=chxx(3:132)
          call sepvar(chxx,2,rvpg,ivxx,cvxx,nvxx)
          if(rvpg(1).ne.0.0.and.rvpg(2).ne.0.0) then
            rvpg(3)=rvpg(2)
            rvpg(2)=rvpg(1)
          end if
        end if
        if(rvpg(2).ne.0.0.and.rvpg(3).ne.0.0) then
          vellopg=rvpg(2)
          if(vellopg.gt.0.0) then
            write(6,*) 'WARNING: Lower velocity limit is positive'
          end if
          velhipg=rvpg(3)
          if(velhipg.lt.vellopg) then
*           swap the two
            velhipg=vellopg
            vellopg=rvpg(3)
          end if      
         else
          write(6,*) ' Velocity limits (km/s) (min,max)?'
          write(6,*) ' Defaults:',vellopg,',',velhipg
          write(6,'(''> '',$)')
          read(inp,'(a)',end=6771) chxx
          call sepvar(chxx,2,rvpg,ivxx,cvxx,nvxx)
          if(nvxx.gt.0) then
            vellopg=rvpg(1)
            if(nvxx.eq.2) then
              velhipg=rvpg(2)
            end if
          end if
 6771     continue
        end if
        goto 1
      end if
*
*     WA set flag to plot on wavelength scale
      if(ixc.eq.'wa'.or.ixc.eq.'WA') then
        lvel=.false.
        goto 1
      end if
*
*     WC set central wavelength for plot (for velocity plots only)
      if(ixc.eq.'wc'.or.ixc.eq.'WC') then
        write(6,*) ' .. for velocity plots only'
        wcenpg=rvpg(2)
*       check for ion, rest wavelength format:
        if(wcenpg.eq.0.0) then
          jb=1
          do while(chxx(jb:jb).ne.' '.and.jb.lt.len(chxx))
            jb=jb+1
          end do
          chxx=chxx(jb:len(chxx))
          if(nz.le.0) call vp_ewred(0)
          call vp_initval(chxx,atom,ion,wavxx,col,indcol,
     :               bval,indb,zval,indz)
*	  correct wavxx to nearest wavelength from table
*	  result in rstwxx
          dwrst=1.0d20
          rstwxx=wavxx
          do i=1,nz
            if(lbz(i).eq.atom.and.lzz(i).eq.ion)then
              ctwrst=abs(alm(i)-wavxx)
              if(ctwrst.lt.dwrst) then
                rstwxx=alm(i)
                dwrst=ctwrst
              end if
            end if
          end do
          wcenpg=real(rstwxx)*zp1refpg
          write(6,*) 'Rest wavelength ',rstwxx,
     :                 ' shifted to ',wcenpg
        end if
        goto 1
      end if
*
*     WH is high wavelength limit, WX to set scale max as well
      if(ixc.eq.'wh'.or.ixc.eq.'WH'.or.
     1     ixc.eq.'wx'.or.ixc.eq.'WX') then
        wl=dble(rvpg(2))
*       check for lack of space between command and value
        if(wl.eq.0.0d0) then
          chxx=chxx(3:132)
          call sepvar(chxx,1,rvpg,ivxx,cvxx,nvxx)
          if(rvpg(1).ne.0.0) then
            wl=dble(rvpg(1))
          end if
        end if
*
        if(wcf1(2).ge.0.0d0) then
          ddum=(wl-wcf1(1))/wcf1(2)
         else
          ddum=1.0d3
        end if
        call chanwav(wl,ddum,1.0d-3,100)
        l2 = nint( ddum )
        if(l2.le.0) l2=1
        write(6,*) ' Channel',l2
        if(ixc.eq.'wx'.or.ixc.eq.'WX') then
*         set max for scaling as well
          lc2=l2
          if(lc2.gt.maxfis) lc2=maxfis
        end if
        goto 1
      end if
c     WL is low wavelength limit, WM or WN if scale min set at same time
      if(ixc.eq.'wl'.or.ixc.eq.'WL'.or.
     1     ixc.eq.'wm'.or.ixc.eq.'WM'.or.
     2     ixc.eq.'wn'.or.ixc.eq.'WN') then
c       wavelength to channel conversion, rfc 13.4.92
        wl=dble(rvpg(2))
*       check for lack of space between command and value
        if(wl.eq.0.0d0) then
          chxx=chxx(3:132)
          call sepvar(chxx,1,rvpg,ivxx,cvxx,nvxx)
          if(rvpg(1).ne.0.0) then
            wl=dble(rvpg(1))
          end if
        end if
        if(wcf1(2).ge.0.0d0) then
          ddum=(wl-wcf1(1))/wcf1(2)
         else
          ddum=1.0d3
        end if
        call chanwav(wl,ddum,1.0d-3,100)
        l1 = nint( ddum )
        write(6,*) ' Channel',l1
        if(ixc.eq.'wm'.or.ixc.eq.'WM'.or.
     1     ixc.eq.'wn'.or.ixc.eq.'WN') then
*	  set scale range as well
          lc1=l1
          if(lc1.le.0) lc1=1
        end if
        goto 1
      end if
c     Y: yscale variables
      if(ixc1.eq.'y'.or.ixc1.eq.'Y') then
c       YB: reset default baseline level 
        if(ixc2.eq.'b'.or.ixc2.eq.'B') then
          yminsetpg=rvpg(2)
          goto 1
        end if
c	YM: set minimum value
        if(ixc2.eq.'m'.or.ixc2.eq.'M') then
          ylowhpg=rvpg(2)
          goto 1
        end if
c	YX: set maximum value
        if(ixc2.eq.'x'.or.ixc2.eq.'X') then
          yhihpg=rvpg(2)
          goto 1
        end if
      end if
c     Z: zero axis plots
      if(ixc1.eq.'z'.or.ixc1.eq.'Z') then
c       ZB: plot both x and y zero lines 
        if(ixc2.eq.'b'.or.ixc2.eq.'B') then
          ipgatt(10,4)=11
          goto 1
        end if
c	ZS: suppress zero lines 
        if(ixc2.eq.'s'.or.ixc2.eq.'S') then
          ipgatt(10,4)=0
          goto 1
        end if
c	ZX: draw line at x=zero
        if(ixc2.eq.'x'.or.ixc2.eq.'X') then
          ipgatt(10,4)=9
          goto 1
        end if
c	ZY: draw y=0 line 
        if(ixc2.eq.'y'.or.ixc2.eq.'Y') then
          ipgatt(10,4)=10
          goto 1
        end if
      end if
c
c     unrecognized command treated as <CR>, i.e. end parameter list
      if(ipgflag.eq.1) then
        ixc='pg'
        goto 1607
       else
        ixc='ps'
        goto 1619
      end if
 3    return
*     read error
 915  ixc='he'
      goto 916
      END
