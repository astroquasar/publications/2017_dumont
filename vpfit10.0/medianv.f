      subroutine medianv(xbuf,ybuf,npt,nfilt,var)
      implicit none
      include 'vp_sizes.f'
*
*     from Mike Irwin, IoA
*
*     median filter array XBUF (length NPT) returning result in XBUF, 
*     using running median of NFILT data values.
*     Needs as workspace YBUF (LENGTH > NPT + NFILT).
*     VAR=0.5 for a true median, but can have any value between
*     0 and 1, when a fraction VAR of the NFILT values used in XBUF 
*     will be less than the result deposited in YBUF(I)
*
      integer npt,nfilt
      double precision xbuf(npt),ybuf(*)
      double precision var
*
*     Local:
      double precision array(1024)
      integer point(1024)
      integer i,iarg,il,ilow
      integer j,jl,jh
      integer l,nfo2p1
      double precision xmnf,xmns
*
      if(npt+nfilt.gt.maxfis)  goto 99
      if(nfilt.gt.1023) goto 97
      if((nfilt/2)*2.eq.nfilt)nfilt=nfilt+1
      nfo2p1=nfilt/2+1
      iarg=int(var*nfilt+0.5)
*     set first and last edges equal
      il=nfilt/2
      ilow=max0(3,nfilt/4)
      do i=1,ilow
        array(i)=xbuf(i)
      end do
      call sortm(array,point,ilow)
      xmns=array(ilow/2+1)
      do i=1,ilow
        array(i)=xbuf(npt+1-i)
      end do
      call sortm(array,point,ilow)
      xmnf=array(ilow/2+1)
*     reflect edges before filtering
      do i=1,il
        ybuf(i)=2.0d0*xmns-xbuf(il+1-i)
        ybuf(npt+i+il)=2.0d0*xmnf-xbuf(npt-i+1)
      end do
      do i=1,npt
        ybuf(i+il)=xbuf(i)
      end do
*     do median filtering on rest
      do i=1,nfilt
        array(i)=ybuf(i)
        point(i)=i
      end do
      call sortm(array,point,nfilt)
      xbuf(1)=array(nfo2p1)
      jl=nfilt+1
      jh=nfilt+npt-1
      do j=jl,jh
        do i=1,nfilt
          if(point(i).eq.1) then
            point(i)=nfilt
            array(i)=ybuf(j)
            l=i
           else
            point(i)=point(i)-1
          end if
        end do
        call mvqsort(array,point,l,nfilt)
        xbuf(j-jl+2)=array(nfo2p1)
      end do
*     same again except this time v% median filter
      xmns=xbuf(1)
      xmnf=xbuf(npt)
      do i=1,il
        ybuf(i)=ybuf(i)-2.0d0*xmns+xbuf(il+1-i)
        ybuf(npt+i+il)=ybuf(npt+i+il)-2.0d0*xmnf+xbuf(npt-i+1)
      end do
      do i=1,npt
        ybuf(i+il)=ybuf(i+il)-xbuf(i)
      end do
      do i=1,nfilt
        array(i)=ybuf(i)
        point(i)=i
      end do
      call sortm(array,point,nfilt)
      xbuf(1)=xbuf(1)+array(iarg)
      do j=jl,jh
        do i=1,nfilt
          if(point(i).eq.1) then 
            point(i)=nfilt
            array(i)=ybuf(j)
            l=i
           else
            point(i)=point(i)-1
          end if
        end do
        call mvqsort(array,point,l,nfilt)
        xbuf(j-jl+2)=xbuf(j-jl+2)+array(iarg)
      end do
 98   return
 99   write(6,*) ' Data array too large - max is 19488'
      goto 98
 97   write(6,*) ' Filter too large     - max is 1023'
      goto 98
      end

      subroutine sortm(ia,ib,n)
*
      integer n
      double precision ia(n)
      integer ib(n)
*
      integer iu,int,ifin,ii,j
      real it
*
      int=2
   10 int=2*int
      if(int.lt.n)goto 10
      int=min0(n,(3*int)/4-1)
   20 int=int/2
      ifin=n-int
      do 70 ii=1,ifin
      i=ii
      j=i+int
      if(ia(i).le.ia(j))goto 70
      it=ia(j)
      iu=ib(j)
   40 ia(j)=ia(i)
      ib(j)=ib(i)
      j=i
      i=i-int
      if(i.le.0)goto 60
      if(ia(i).gt.it)goto 40
   60 ia(j)=it
      ib(j)=iu
   70 continue
      if(int.gt.1)goto 20
      return
      end

      subroutine mvqsort(x,point,l,nfilt)
*
      implicit none
      integer l,nfilt
      double precision x(nfilt)
      integer point(nfilt)
*
      integer i,ii,j,it,npt
      double precision test,temp
      test=x(l)
      do 100 i=1,nfilt
        if(i.eq.l)goto 100
        if(test.gt.x(i))goto 100
        j=i
        goto 200
  100 continue
      j=nfilt+1
  200 if(j-1.eq.l) return
      if(j-l)300,500,400
  300 temp=x(l)
      it=point(l)
      npt=l-j
      do 350 i=1,npt
      ii=l-i
      x(ii+1)=x(ii)
  350 point(ii+1)=point(ii)
      x(j)=temp
      point(j)=it
      return
  400 temp=x(l)
      it=point(l)
      j=j-1
      npt=j-l
      if(npt.eq.0)goto 475
      do 450 i=1,npt
      ii=l+i
      x(ii-1)=x(ii)
  450 point(ii-1)=point(ii)
  475 x(j)=temp
      point(j)=it
  500 return
      end
