      subroutine vp_endorder(nfion,nfunc,ion,level,parm,ipind,np)
*
*     re-order the end of the element list, so as to keep the last
*     group of __ indicators at the end -- so they apply to all relevant 
*     systems after lines added.
      implicit none
      include 'vp_sizes.f'
*
      integer nfion,nfunc,np
*
*     local variables:
      character*4 chtemp,chtemp2
*     character*2 chm0,chm1,chm2
      integer i,j,k,itemp,ii,iip
      double precision tempa
*     double precision vm0,vm1,vm2
*     variable swap holdspace
      character*2 chma(maxpps)
      double precision vma(maxpps)
*     array sizes:
      double precision parm(maxnpa)
      character*2 ipind(maxnpa)
      character*2 ion(maxnio)
      character*4 level(maxnio)
*
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     region flags
      integer linchk( maxnio )
      common/vpc_linchk/linchk
      double precision vturb,atmass,temper,fixedbth
      common/vpc_ucopder/vturb(maxnio),atmass(maxnio),
     :                 temper(maxnio),fixedbth(maxnio)
*
*     Note that new variables don't have vturb, temper, associated, so
*     don't bother to swap those.
*     search for last non __ or AD record, since if these were
*     at the end they need to stay there.
      do while (nfion.gt.1.and.(ion(nfion).eq.'AD'.or.
     :              ion(nfion).eq.'__'))
        nfion=nfion-1
      end do
*     nfion is last multiplicative parameter, apart from added-in
*     line at nfunc. Now want to put nfunc at nfion+1, and move
*     nfion+1 to nfunc-1 -> nfion+2 to nfunc.
*
*     New last multiplicative parameter will be:
      nfion=nfion+1 
      if (nfion.lt.nfunc) then
*       do need to shuffle around:
        chtemp=ion(nfunc)
        chtemp2=level(nfunc)
        tempa=atmass(nfunc)
*       dataset number
        itemp=linchk(nfunc)
*       indicator characters & variables
        do i=1,noppsys
          j=np+i-noppsys
          k=noppsys+1-i
*         so, if noppsys=3, vma(3)=parm(np-2), then vma(2)=parm(np-1), then
*         vma(1)=parm(np), same order as was done explicitly.
          chma(k)=ipind(j)
          vma(k)=parm(j)
        end do
*
        ii=nfunc
        do while(ii.gt.nfion)
          ion(ii)=ion(ii-1)
          level(ii)=level(ii-1)
          atmass(ii)=atmass(ii-1)
          linchk(ii)=linchk(ii-1)
          iip=noppsys*ii
*         copy up, lowest first
          do i=1,noppsys
            j=iip-noppsys+i
            ipind(j)=ipind(j-noppsys)
            parm(j)=parm(j-noppsys)
          end do
          ii=ii-1
        end do
        ion(nfion)=chtemp(1:2)
        level(nfion)=chtemp2
        atmass(nfion)=tempa
        linchk(nfion)=itemp
        iip=nfion*noppsys
*       copy chma, vma into the ipind, parm arrays
        do i=1,noppsys
          j=iip-noppsys+i
          k=noppsys+1-i
          ipind(j)=chma(k)
          parm(j)=vma(k)
        end do
      end if
      return
      end
