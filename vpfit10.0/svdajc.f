      subroutine svdajc( a, b, n, nopt )

* singular value decomposition routine to replace udchole in vpfit
* andrew cooke 17-feb-92

* solves a.x = b returning x in b
* n is dimension of a, b
* nopt is vpfit option number (main menu)

* see numerical recipes, page 56ish for basis of this

      implicit none

      include 'vp_sizes.f'

      integer size
      parameter ( size = maxmas )
      double precision a( size, size ), b( size )
      integer n, nopt

      integer nn
      double precision aa( size, size ), bb( size )
      double precision vv( size, size ), ww( size )
      double precision x( size )
      logical ok( size )

      double precision wmax, wmin
      integer i, j, ii, jj
      integer count

      character*2 ipind( maxnpa )
      integer isod,isodh
      common/vpc_usoind/ipind,isod,isodh

      double precision factor
      
      double precision rlim
      common /vpc_svd / rlim

      logical varythis

*     data name / 'n', 'z', 'b' /

*     first remove rows and columns corresponding to fixed parameters if
*     necessary - otherwise direct copy to workspace
      if ( nopt .le. 0 ) then
        nn = n
        do j = 1, n
          bb( j ) = b( j )
          do i = 1, n
            aa( i, j ) = a( i, j )
          end do
        end do
       else
        jj = 0
        nn = 0
        do j = 1, n
          if ( varythis( j, ipind ) ) then
            jj = jj + 1
            nn = nn + 1
            bb( jj ) = b( j )
            ii = 0
            do i = 1, n
              if ( varythis( i, ipind ) ) then
                ii = ii + 1
                aa( ii, jj ) = a( i, j )
              end if
            end do
          end if
        end do
      end if

*     now solve aa.x = b for the nn size matrix

*     first decompose
      call svdcmpd( aa, nn, nn, size, size, ww, vv )

*     then look for nicest bit
      wmax = 0d0
      do i = 1, nn
        wmax = max( wmax, ww( i ) )
      end do

*     and chop out nasty bits
      if ( rlim .gt. 0.0d0 ) then
        factor = dble( rlim )
       else
        factor = 1d-12
      end if
      wmin = wmax * factor
      count = 0
      do i = 1, nn
        if ( ww( i ) .lt. wmin ) then
          ww( i ) = 0d0
          ok( i ) = .false.
          count = count + 1
        else
          ok( i ) = .true.
        end if
      end do
      if ( count .gt. 0 ) then
        write ( *, * ) count, ' squidgy dimensions'
        write ( *, * ) ' '
      end if

*	before calculating the answer
      call svbksbd( aa, ww, vv, nn, nn, size, size, bb, x )

*     and putting everyting back in order - if not squidgy
      if ( nopt .le. 0 ) then
        do j = 1, n
          if ( ok( j ) ) then
            b( j ) = x( j )
          end if
        end do
      else
        jj = 0
        do j = 1, n
          b( j ) = 0d0
          if ( varythis( j, ipind ) ) then
            jj = jj + 1
            if ( ok( jj ) ) then
              b( j ) = x( jj )
            end if
          end if
        end do
      end if
      return
      end


