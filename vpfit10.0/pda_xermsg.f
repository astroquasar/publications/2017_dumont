      subroutine pda_xermsg(lib,sub,messg,nerr,lvl,istat)
*     replaces STARLINK's version, which seems to need
*     YET ANOTHER library!
      character*(*) lib,sub,messg
      istat=1
      write(6,*) messg
*     and in case it helps:
      write(6,*) lib(1:4),' ',sub(1:4),nerr,lvl
      return
      end
