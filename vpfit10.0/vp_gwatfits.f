      subroutine vp_gwatfits(im,wcft,maxwco,n,iord,wcftype,ier)
*
*     get the wavelength coefficients from IRAF 2.10 
*     assumed linear. From FITS files 
*
*     rfc 4.11.92, with GMW mod 26.2.98 FITS rfc 18.06.01
*
*     IN:
*     im	integer	unit number 
*     iord	integer	order number
*
*     OUT:
*     wcft	array*8	wavelength coefficients
*     n	integer	# of coeffts
*     ier	integer	status OK = 0 
*
      double precision wcft(maxwco)
*
      character*8 wcftype
      character*7 apname
      character*8 watname
      character*132 chstr,chspare
*
      character*132 oldchstr 
      character*264 dblchstr
*                              ! keeps track of previous string in case
*                              ! "specnn" is split across two lines
*                           ! fix applied by G. Williger, NASA/GSFC 26 Feb 1998
*
      character*512 xchstr
      logical look
      double precision dv(40)
      integer iv(40)
      character*32 cv(40)
      logical verbose
      common/vp_sysout/verbose

*     initialise old character string for first iteration
      oldchstr = ' '
*
*     check that it is an IRAF v2.10 file by checking WAT0_001
      ier=0
      call ftgkys(im,'WAT0_001', oldchstr,chstr,ier)
      if(ier.ne.0) goto 99
*     write(6,*) ' Wavelengths read OK'
*
*     set up pattern to search for
      write(apname(4:7),'(i4)') 1000+iord
      lenpat=7
      do ii=1,3
        if(apname(5:5).eq.'0') then
          apname(5:7)=apname(6:7)
          lenpat=lenpat-1
        end if
      end do
      apname(1:4)='spec'
*
*     and now look for it
      ier=0
      look=.true.
      nwat=0
*     check on string length
      nclen=0
*
      do while (ier.eq.0.and.look)
        nwat=nwat+1
        if (nwat.eq.60) then
          continue
        end if
        write(watname(5:8),'(i4)') 1000+nwat
        watname(1:5)='WAT2_'
        call ftgkys(im,watname, chstr,chspare,ier)
*
        if(ier.eq.0) then
*         establish length
          nlen=132
          do while(chstr(nlen:nlen).eq.' ')
            nlen=nlen-1
          end do
          nclen=max0(nlen,nclen)
*	  search for pattern
          lend=nclen-lenpat+1
          ii=1
          do while(ii.le.lend.and.
     1        apname(1:lenpat).ne.chstr(ii:ii+lenpat-1))
            ii=ii+1
          end do
*
* fix by G. Williger 26 Feb 1998
* look for pattern split across two lines on second and subsequent iterations
          if (ii.eq.lend+1.and.oldchstr.ne.' ') then 
            dblchstr = ' '
            dblchstr(1:nclen) = oldchstr(1:nclen)
            dblchstr(nclen+1:nclen+nclen) = chstr(1:nclen)
            ii = -lenpat
            do while (ii.le.lend.and.
     1          apname(1:lenpat).ne.
     1          dblchstr(ii+nclen:ii+lenpat-1+nclen))
              ii=ii+1
            end do
* if string is split across two lines, set ii = 1
            if (ii.lt.1) ii = 1
          end if


*
*	  check if pattern found -- if not, go back to start of loop
          if(ii.le.lend) then
*	    pattern found
            look=.false.
*	    now search for the required values, enclosed by "
            do while(chstr(ii:ii).ne.'"'.and.ii.le.nclen)
              ii=ii+1
            end do
*           if at end of string, need the next one
* fix tried by GMW 10 Aug 1997
* chstr is 
* "1. 2000. 4818.54401826193 33.4965814970696 -0.569878960250291" spec1
* but need spec16 (continued on next line)
* this is the old line (from Carswell et al.):
*	      if(ii.gt.nclen) then
* this is the suggested fix, as lend=63, ii=64 and nclen=68 so ii can NEVER
* be greater than nclen
*              if(ii.gt.lend) then
* old line replaced (though this branch loop can never be reached as if
* ii.le.lend then ii will never be .gt. nclen)
*
            if(ii.gt.nclen) then

*	      try the next string for the completion of the quoted one
              nwat=nwat+1
              write(watname(5:8),'(i4)') 1000+nwat
              watname(1:5)='WAT2_'
              ier=0
              call ftgkys(im,watname, chstr,chspare,ier)
              ii=1
              do while(chstr(ii:ii).ne.'"'.and.ii.le.nclen)
                ii=ii+1
              end do
            end if
*	    put part of string in holding location
            xchstr=chstr(ii+1:nclen)
            lxchstr=nclen-ii
*	    search for matching end "
            ie=ii+1
            do while(chstr(ie:ie).ne.'"'.and.ie.le.nclen)
              ie=ie+1
            end do
            do while (ie.gt.nclen) 
*	      try the next string for the completion of the quoted one
              nwat=nwat+1
              write(watname(5:8),'(i4)') 1000+nwat
              watname(1:5)='WAT2_'
              call ftgkys(im,watname, chstr,chspare,ier)
*	      copy to holding string
              xchstr(lxchstr+1:lxchstr+nclen)=chstr
              lxchstr=lxchstr+nclen
*	      search for "
              ie=1
              do while(chstr(ie:ie).ne.'"'.and.ie.le.nclen)
                ie=ie+1
              end do
            end do
*	    position of quote in extended string is lxchstr-nclen+ie
            ie=lxchstr-nclen+ie
            xchstr(ie:ie)=' '
            lxchstr=ie-1
            if (verbose) then
*             write out entire string:
              ih=0
              write(6,*) 'WAT_ string:'
              do j=1,8
                il=ih+1
                ih=ih+64
                if(ih.lt.lxchstr) then
                  write(6,*) xchstr(il:ih)
                 else
                  write(6,*) xchstr(il:lxchstr)
                  goto 917
                end if
              end do
 917          write(6,*) '------'
            end if 
*	    21 input gives 6 Cheb coeffts:
            call dsepvar(xchstr(1:lxchstr),40,dv,iv,cv,nv)
            if(nv.le.15) then
*             linear wavelengths
              n=2
              wcft(1)=dv(4)
              wcft(2)=dv(5)
              if(wcft(1).le.5.0d0) then
*	        assume log wavelengths
                wcftype='loglin'
               else
                wcftype='poly'
              end if
*	      IRAF -> internal base shift
              wcft(1)=wcft(1)-wcft(2)
             else
              if(nv.ge.40.or.nv.gt.maxwco+13) then
                write(6,*) ' WARNING: Chebyschev order too high!!'
              end if
*             get Chebyschev coefficients, 1: xlow; 2: xhigh; 3 etc Cheb 
*	      coeffts
              n=nv-13
              if(verbose) write(6,*) 'Chebyschev, order ',n-2
              do jj=1,n
                wcft(jj)=dv(13+jj)
              end do
*             go to half c0 convention:
              wcft(3)=2.0d0*wcft(3)
              wcftype='cheb'
            end if
*           error flag
            ier=0
*           output for information
            if (verbose) then
              write(6,*) ' WAT2 length is: ',nclen
              write(6,*) 'Nos: ',cv(1)(1:10),dv(2),dv(3),dv(4),dv(5)
            end if
          end if
        end if
        oldchstr = chstr
      end do
*
 99   return
      end

