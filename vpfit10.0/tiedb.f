      subroutine tiedb(elx, temp,turb, m1,m2, bth1,bth2, b1,b2)

*     ajc 30-may-92 
*     subroutine to tie line widths

      implicit none
*     elx is element, for exception conditions
      character*2 elx
      double precision temp, turb, m1, m2, bth1, bth2
      double precision b1, b2

      double precision rturb, rtemp

*     temp and turb are for the reference ion
*     m is the mass (1: ref, 2: tied)
*     bth is the thermal width
*     b is the parameter

*     check the primary system
      if(elx.ne.'>>') then
        call checkb( temp, turb, m1, b1 )
*       if temp is non-zero, and turbulent zero, vary turbulent component
        if ( temp .ne. 0.0d0 .and. turb .eq. 0.0d0 ) then
*         first the thermal components of the widths
          bth1 = 0.128953d0 / sqrt( m1 / temp )
          bth2 = 0.128953d0 / sqrt( m2 / temp )
*         now update in quadrature
*         residual turbulent velocity (squared)
          rturb = b1**2 - bth1**2
*         if turbulent component negative, set to zero!
          if(rturb.lt.0.0d0) rturb=0.0d0
*         add turbulent component to thermal component for b2
          b2 = sqrt( rturb + bth2**2 )
*
*         otherwise assume that the temperature varies but the turbulent
*         component is the same 
         else
*         residual thermal velocity (squared)
          rtemp = b1**2 - turb**2
*         this should not be negative!!!!
          if(rtemp.lt.0.0d0) rtemp=0.0d0
*         scale squared residual thermal component by atomic masses
          rtemp = rtemp * m1 / m2
*         add thermal component to turbulent component
          b2 = sqrt( rtemp + turb**2 )
        end if
       else
        b2=b1
      end if
      return
      end
        
