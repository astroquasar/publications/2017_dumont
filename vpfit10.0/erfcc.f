      double precision function erfcc(x)
*     complementary error function
      implicit none
      double precision z,x,t
      z=abs(x)      
      t=1.d0/(1.d0+0.5d0*z)
      erfcc=t*exp(-z*z-1.26551223D0+t*(1.00002368D0+t*(0.37409196D0+
     *  t*(.09678418D0+t*(-.18628806D0+t*(.27886807D0+t*(-1.13520398D0+
     *  t*(1.48851587D0+t*(-.82215223D0+t*.17087277D0)))))))))
      if (x.lt.0.0d0) erfcc=2.0d0-erfcc
      return
      end
