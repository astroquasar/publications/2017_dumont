      subroutine rd_stackz(da,de,nct,ds,re,dh,dhe,ng,ngp)
*     take a file of redshifts (fort.26) and stack them at zero
*     redshift, taking account of the error array. Assume that the
*     continuum is normalized to unity, otherwise you'll never know
*     what is going on. The (normalized) data is assumed to be in the
*     workspace da.
*
*     region specific variables are ignored, so isolated region lines
*     (lnk) and part continuum (chlnkv) not catered for.
*
      implicit none
      include 'vp_sizes.f'
*
      integer nct,ng,ngp
      double precision da(*),de(*),dh(*),dhe(*)
      double precision ds(*),re(*)
*
*     LOCAL variables
      double precision bval,zed,col
      double precision teff,vtb
      double precision drv(4),dtemp
      character*2 chcon
      character*4 chlnkv
      character*2 indcol,indz,indb
      integer ifst,jpin
      integer ind,ierr,lnk
      integer nladded,ncmax
      double precision bvall
      double precision bvallo,bvalhi,collo,colhi,zedlo,zedhi
      double precision xstart,xinc
*
*     FUNCTIONS:
      double precision wval
*
*     common variables
      character*132 chstr
      character*2 ion,chion
      character*4 level,chlevl
*
      integer nz
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),nz
*     scalefactors for column density
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
*
*     common variables
*     character string separation:
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
      double precision dvstr(24)
      common/vpc_dsepspace/dvstr
*
*     regions
      integer kalow(maxpreg),kahiw(maxpreg),nreg
      double precision wvlo(maxpreg),wvhi(maxpreg)
      common/rdc_regions/wvlo,wvhi,kalow,kahiw,nreg
*
*     Default limits:
      collo=0.0d0
      colhi=1.0d25
      zedlo=-1000.0d0
      zedhi=1.0d25
      bvallo=0.0d0
      bvalhi=1.0d25
      bvall=0.0d0
      chion='H '
      chlevl='I   '
      xstart=0.0d0
      xinc=0.0d0
*
      nladded=0
      ncmax=maxfis
 997  write(6,*) '<filename> '//
     :     '(fmt,elm,ion,clo,chi,zlo,zhi,blo,bhi,binc)'
      read(5,'(a)') chstr
*     attempt to split this
      call dsepvar(chstr,15,dvstr,ivstr,cvstr,nvstr)
      open(unit=13,file=cvstr(1),status='old',err=997)
*     could open the file, so use it
      write(6,*) 'File opened: ',cvstr(1)(1:20)
*     check for type:
      if(ivstr(2).eq.13) then
        write(6,*) 'fort.13 style file'
        ifst=13
       else
        write(6,*) 'fort.26 style file'
        ifst=26
      end if
*     further control variables may be present:
      if(nvstr.gt.2) then
        if(cvstr(3)(1:2).ne.'  ') chion=cvstr(3)(1:2)
        if(cvstr(4)(1:4).ne.'    ') chlevl=cvstr(4)(1:4) 
*       minimum (log) column density for inclusion
        if(dvstr(5).gt.0.0d0) collo=dvstr(5)
*       and maximum
        if(dvstr(6).gt.0.0d0) colhi=dvstr(6)
*       minimum redshift included
        if(dvstr(7).gt.0.0d0) zedlo=dvstr(7)
*       and maximum
        if(dvstr(8).gt.0.0d0) zedhi=dvstr(8)
*       minimum b value included
        if(dvstr(9).gt.0.0d0) bvallo=dvstr(9)
*       and maximum
        if(dvstr(10).gt.0.0d0) bvalhi=dvstr(10)
*       with the opportunity of including every line if b < bvall
        if(dvstr(11).gt.0.0d0) bvall=dvstr(11)
*       and restricting the wavelength range (for H I)
        if(dvstr(12).gt.0.0d0.and.chion.eq.'H ') then
          xstart=dvstr(12)
          if(dvstr(13).gt.0.0d0) then
            xinc=dvstr(13)
          end if
        end if
      end if
 801  read(13,'(a)',end=9997) chstr
      if(chstr(1:1).eq.'!') goto 801
      jpin=13
      do while (chstr.ne.' ')
*       long process, so just to monitor progress:
        if(jpin.eq.13) write(6,'(a)') chstr(1:60)
        inchstr=chstr
        call dsepvar(inchstr,15,dvstr,ivstr,cvstr,nvstr)
*       skip fort.26 filename indicators:
        if(cvstr(1)(1:2).eq.'%%') goto 996
        call vp_f13fin(ion,level,col,indcol,zed,indz,bval,indb,
     :               vtb,teff,lnk,chlnkv,ierr,ifst)
        if(ierr.ne.0) then
          write(6,*) ' Unrecognized format:',chstr(1:40),'....'
          goto 996
        end if
*       check the parameters are within the limits
        if(bval.ge.bvall.and.(zed.lt.zedlo.or.zed.gt.zedhi.or.
     :       bval.lt.bvallo.or.bval.gt.bvalhi.or.col.lt.collo.or.
     :       col.gt.colhi)) goto 996
        if(ion.ne.chion.or.level.ne.chlevl) goto 996
        if(nladded.eq.0) then
*         first line:
          call rd_zcorr(dble(zed),'n')
          if(xstart.gt.0.0d0.and.xstart.lt.2.995d5) then
            if(xinc.gt.0.0d0) then
*             interpret as wavelength & increment
              drv(1)=xstart
              drv(2)=xinc
*             make symmetric about Ly-a
              if(drv(1).lt.1215.67d0) then
                drv(3)=2.0d0*drv(1)-1215.67d0
               else
                drv(1)=wval(0.0d0)
                write(6,*) 'Inappropriate wavelengths?'
                write(6,*) ' 2048 channel spectrum assumed'
                drv(3)=wval(2.048d0)
                drv(2)=(drv(3)-drv(1))/2.048d0
              end if
             else
              drv(1)=sqrt((2.99792458d5-xstart)/
     :                      (2.99792458d5+xstart))
              drv(3)=1215.6701d0/drv(1)
              drv(1)=1215.6701d0*drv(1)
              dtemp=1.0d3
              call vp_chanwav(drv(3),dtemp,5.0d-2,20,1)
              drv(2)=(drv(3)-drv(1))/dtemp
            end if
*           indicate that range is set
            chcon='nr'
           else
            chcon='  '
          end if
          call rd_scrn(da,de,nct,ds,re,ncmax,dh,dhe,ng,ngp,ind,
     :            chcon,drv)
          nladded=1
         else
*         redshift correct the same dataset
          call rd_zcorr(dble(zed),'b')
          call rd_scrn(da,de,nct,ds,re,ncmax,dh,dhe,ng,ngp,ind,
     :            'a ',drv)
          nladded=nladded+1
        end if
*       more data?
 996    read(jpin,'(a)',end=998) chstr
        goto 990
 998    chstr=' '
        if(jpin.ne.5) close(unit=jpin)
 990    continue
      end do
      write(6,*) nladded,' lines summed'
 9998 return
 9997 write(6,*)' Empty parameter file'
      goto 9998
      end
