      subroutine rd_extabs(da,de,ca,cad,re,ds,hc,nct)
*     
*     take a pixel-based cube (space*space*velocity) of column densities
*     or surface brightnesses or optical depths, put it as a screen
*     in front of an extended source, and determine the resulting absorption
*     line profile.
*
*     Somewhat assumption based, but is the best we can do.
*
      implicit none
      include 'vp_sizes.f'
      include 'rd_sizes.f'
*
      integer nct
      double precision da(*),de(*),ca(*),cad(*)
      double precision re(*),ds(*),hc(*)
*     local
      integer naxis,nj,nkk,nspec
      integer i,ic,id,ix,iy,ixcen,iycen,i1,iounit,istat
      integer j,ja,jj,jmax,jn,jx,kpcen,lnch,nspecref
      integer nbx,nby,nxt,nyt,nxst,nyst,nxend,nyend
      double precision smax
      character*4 chwhat
*     was 3,3,3,120
      integer naxes(4),ifpixels(4),lpixels(4)
      double precision sdat(640)
      integer incs(4)
      logical anyf
      integer naxst(100),nayst(100)
      double precision zedb,abund,col,zed,bval
      double precision cdelt1,cdelt2,cdelt3,crpix3,dtemp
      character*64 comment
      character*6 chionh
      double precision dvabh,dvzedh
      integer nvx
      integer ivx(15)
      double precision dvx(15)
      character*64 cvx(15)
*     local multipath section
      double precision dnlsmult,dnlstot
*     ion variables
      character*2 catom
      character*4 cionz
*     functions
      logical ucase
      integer lastchpos
*
*     command input stream
      integer inputc
      common/rdc_inputc/inputc
*     character handling
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
      double precision dvstr(24)
      common/vpc_dsepspace/dvstr
*     general 1-D wavelength coefficients
      double precision wcf1(maxwco)
      common/wavl/wcf1
      integer nwco
      double precision wcfx1(maxwco)
      common/vpc_wavl/wcfx1,nwco
      character*8 wcftype
      character*4 vacind
      double precision helcfac
      integer noffs
      common/c8_wcftype/wcftype,helcfac,noffs,vacind
*     multichunkwavelength parameter sets
      character*8 wcfty2(maxnch)
      character*4 vacin2(maxnch)
      double precision helcf2(maxnch)
      integer nwcf2(maxnch),noffs2(maxnch)
      common/vpc_wcpars2/wcfty2,helcf2,nwcf2,noffs2,vacin2
*     general chunk wavelength coefficients
      double precision wcfd(maxwco,maxnch)
      common/vpc_wcfd/wcfd
*     atomic parameters
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm
      integer nz 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),nz
*
*     prompts for the information. There is too much needed for
*     the command line to be appropriate. Contained in RDGEN only
*     because the line profile routines, FITS reader, display routines
*     etc are already there.
*
*     some arbitrary defaults
      if(nz.le.0) call vp_ewred(0)
      dvabh=-10.5d0
      dvzedh=2.3d0
      chionh='SiII'
      nspecref=1
      kpcen=-1000000
*     make sure there is a wavelength scale, and some data!
      if(nct.le.0.or.wcftype.eq.'undefine') then
*       set wavelength info
        write(6,*) 'Wavelength base, increment, array length'
        read(inputc,'(a)') inchstr
        call dsepvar(inchstr,3,dvstr,ivstr,cvstr,nvstr)
        if(dvstr(1).eq.0.0d0.and.dvstr(2).eq.0.0d0) then
          wcf1(1)=3200.0d0
          wcf1(2)=0.04d0
         else
          wcf1(1)=dvstr(1)
          wcf1(2)=dvstr(2)
        end if
        wcfd(1,1)=wcf1(1)
        wcfd(2,1)=wcf1(2)
        if(maxwco.gt.2) then
          do j=3,maxwco
            wcf1(j)=0.0d0
            wcfd(j,1)=0.0d0
          end do
        end if
        nct=ivstr(3)
        if(nct.le.0) nct=100000
        nwco=2
        nwcf2(1)=nwco
        wcftype='poly'
        wcfty2(1)=wcftype
        helcfac=1.0d0
        helcf2(1)=helcfac
        vacind='vacu'
        vacin2(1)=vacind
        noffs=0
        noffs2(1)=noffs
      end if
*     LMC data, or whatever. Some LMC values are hardwired at this stage
*     but that could be changed.
 987  write(6,*) 'Data cube file [Emma''s], (action) (nref)'
      read(inputc,'(a)') inchstr
      call dsepvar(inchstr,3,dvstr,ivstr,cvstr,nvstr)
      if(cvstr(1)(1:1).eq.' ') then
        cvstr(1)='/home/rfc1/data/qsos/LMC/pg.merge.sub.fits'
      end if
      lnch=lastchpos(cvstr(1))
      istat=0
      write(6,*) 'file: ',cvstr(1)(1:lnch)
      chwhat=cvstr(2)(1:4)
      if(ivstr(3).gt.0) then
        nspecref=ivstr(3)
      end if
*
*     ASCII input file section
      if(chwhat(1:2).eq.'as'.or.chwhat(1:2).eq.'AS') then
        nspec=0
*       set base continuum to unity, and summed spectrum to zero
        do i=1,nct
          re(i)=1.0
          ds(i)=0.0
        end do
*       just ascii line input, not fits file, so just read in
*       v,logN,b
        open(unit=18,file=cvstr(1)(1:lnch),status='old',err=987)
        write(6,*) 'ASCII file input'
        write(6,*) 'Ion, ref z, col offset,(multiline format)'
        read(inputc,'(a)') inchstr
        call dsepvar(inchstr,5,dvstr,ivstr,cvstr,nvstr)
        call vp_ationsep(cvstr,dvstr,ivstr,5,1,catom,cionz)
*       This assumes vp_ationsep moves all arrays down if necessary
        zedb=dvstr(2)
        dtemp=dvstr(3)
        if(cvstr(4)(1:1).eq.' ') then
*         old ascii file format
          write(6,*) 'Lines of sight to be included? [1]'
          read(inputc,'(a)') inchstr
          call dsepvar(inchstr,15,dvx,ivx,cvx,nvx)
          if(cvx(1)(1:1).eq.' ') then
            nkk=1
           else
            nkk=nvx
            do i=1,nkk
              naxst(i)=ivx(i)
            end do
          end if
          write(6,*) nkk,' lines of sight'
          do ic=1,100
 818        read(18,'(a)',end=986) inchstr
*           ignore comment lines
            if(inchstr(1:1).eq.'!') goto 818
            do id=1,nkk
              if(naxst(id).eq.ic) goto 820
            end do
*           bypass this one, not to be included
            goto 821
 820        write(6,*) ic,' ',inchstr(1:32)
            call dsepvar(inchstr,15,dvx,ivx,cvx,nvx)
            if(cvstr(1)(1:2).eq.'  ') goto 986
*           local continuum to unity
            do i=1,nct
              cad(i)=1.0
            end do
            nj=nvx/3
            do jj=1,nj
              zed=(1.0d0+zedb)*
     :          (1.0d0+dvx(3*jj-2)/2.99792458d5)-1.0d0
              col=dvx(3*jj-1)+dtemp
              bval=dvx(3*jj)
              write(6,*) ic,jj,zed,col,bval
*             cad is input continuum, ca is output with absorption lines
*             re is base continuum used for zero level adjustment
              call spvoigt(ca,cad,1,nct,col,zed,bval,
     :          catom,cionz,1,re)
*             set input for next velocity to output here
              do i=1,nct
                cad(i)=ca(i)
              end do
            end do
*           add into summed spectrum
            do i=1,nct
              ds(i)=ds(i)+cad(i)
            end do
*           update total spectrum numbers in sum
            nspec=nspec+1
*           first to data array 
            if(nspec.eq.nspecref) then
              do i=1,nct
                da(i)=cad(i)
                de(i)=0.0000001
              end do
            end if
 821        continue
          end do
*         copy sum to continuum, renormalizing
 986      continue
          do i=1,nct
            ca(i)=ds(i)/dble(nspec)
          end do
          close(unit=18)
          goto 409
         else
*         multiline format is:
*         x <factor> [def=1]      \  Repeated blocks, terminates on <EOF>
*         delta-v, col, bvalue     | or blank line.
*         .......                  |
*         delta-v, col, bvalue    /  
*         copy input continuum
          do i=1,nct
            hc(i)=ca(i)
          end do
          write(6,*) 'Continuum copied to swap space'
          dnlstot=0.0d0
          ic=0
          do while(ic.gt.-1)
 721        read(18,'(a)',end=701) inchstr
            if(inchstr(1:1).eq.'!') goto 721
            call dsepvar(inchstr,5,dvstr,ivstr,cvstr,nvstr)
*           terminate everything on a blank
            if(cvstr(1)(1:1).eq.' ') goto 701
            if(cvstr(1)(1:1).eq.'x') then
*             if not first time through add into summed spectrum
              if(ic.gt.0) then
                do i=1,nct
                  ds(i)=ds(i)+cad(i)*dnlsmult
                end do
*               update total spectrum numbers in sum
                dnlstot=dnlstot+dnlsmult
              end if
*             prepare for next set
              if(dvstr(2).gt.0.0d0) then
                dnlsmult=dvstr(2)
               else
                dnlsmult=1.0d0
              end if
              ic=ic+1
*             set to original continuum
              do i=1,nct
                cad(i)=hc(i)
              end do
             else
*             line parameters until hit the next 'x', same format as ASCII
*             i.e. delta-v, logN, bvalue
              zed=(1.0d0+zedb)*
     :          (1.0d0+dvstr(1)/2.99792458d5)-1.0d0
              col=dvstr(2)+dtemp
              bval=dvstr(3)
              write(6,*) ic,zed,col,bval,dnlsmult
*             cad is input continuum, ca is output with absorption lines
*             re is base continuum used for zero level adjustment
              call spvoigt(ca,cad,1,nct,col,zed,bval,
     :          catom,cionz,1,re)
*             set input for next velocity to output here
              do i=1,nct
                cad(i)=ca(i)
              end do
            end if             
          end do
 701      continue
*         put in the last lot
          do i=1,nct
            ds(i)=ds(i)+cad(i)*dnlsmult
          end do
*         update total spectrum numbers in sum
          dnlstot=dnlstot+dnlsmult
*         copy sum to continuum, renormalizing
          if(dnlstot.gt.0.0d0) then
            do i=1,nct
              ca(i)=ds(i)/dnlstot
            end do
           else
            write(6,*) 'rd_extabs: Total weight is zero'
          end if
          close(unit=18)
          goto 409
        end if
      end if
*
*     FITS input file section
      call ftgiou( iounit,istat)
      if(istat.ne.0) goto 401
      call ftnopn(iounit,cvstr(1)(1:lnch),0, istat)
      if(istat.ne.0) goto 402
      call ftgidm(iounit, naxis,istat)
      if(istat.ne.0) goto 403
      call ftgisz(iounit,naxis, naxes,istat)
      if(istat.ne.0) goto 403
*     Other information:
*     pixels size:
      call ftgkyd(iounit,'CDELT1', cdelt1,comment,istat)
      istat=0
      call ftgkyd(iounit,'CDELT2', cdelt2,comment,istat)
      istat=0
      cdelt1=cdelt1*60.0d0
      cdelt2=cdelt2*60.0d0
      write(6,*) 'Pixel size is ',cdelt1,' x',cdelt2,' arcmin'
*     velocity increment
      call ftgkyd(iounit,'CDELT3', cdelt3,comment,istat)
*     it is in m/s, so convert to km/s
      cdelt3=cdelt3/1.0d3
      if(istat.ne.0) then
        istat=0
        call ftgkyd(iounit,'LWIDTH', cdelt3,comment,istat)
        if(istat.eq.0) then
*         LMC header, so get base
          call ftgkyd(iounit,'LSTART', crpix3,comment,istat)
          if(istat.ne.0) then
*           put zero velocity in middle of range
            crpix3=-cdelt3*0.5d0*dble(naxes(3))
            write(6,*) 'No velocity base in header, assuming',crpix3
            istat=0
          end if
         else
*         LMC default
          cdelt3=1.65d0
*         put zero velocity in middle of range
          crpix3=-cdelt3*0.5d0*dble(naxes(3))
          write(6,*) 'No velocity step in header, assuming',cdelt3
          write(6,*) ' and base',crpix3
          istat=0
        end if
       else
*       put zero velocity in middle of range
        kpcen=naxes(3)/2
        crpix3=-cdelt3*dble(kpcen)
        istat=0
      end if
      dtemp=crpix3+dble(naxes(3)-1)*cdelt3
      write(6,*) 'velocity range',crpix3,' to',dtemp
*
*     LMC data cube: each spatial pixel is 20x20 arcsec, which, at 50kpc
*     corresponds to 4.8 parsecs. The resolution is 3x3 pixels. The velocity
*     range is from 190 to 286.23 km/s, so, for 120 pixels, 1.65 km/s/pixel
*     The values S are Jy/beam(of 60x60 arcsec)/velocity bin, so
*     N(HI)=1.36E3 (21.1)**2 S/(60*60) 1.65 1.83e18 per velocity bin
*          =5.1E20*S
*     You'll need to adjust this constant for anything else!!!!!
*
*     Decide on size of background light (box for now), and where it is
      write(6,*) 'Background length scale (x,y pix), and where (px,py)?'
      write(6,*) ' (may have multiple single points)       max',
     :           naxes(1),naxes(2)
      read(inputc,'(a)') inchstr
      call dsepvar(inchstr,10,dvstr,ivstr,cvstr,nvstr)
      if(nvstr.eq.2) then
*       if just coordinates, then single pixel
        write(6,*) 'Single pixel at',ivstr(1),ivstr(2)
        ivstr(3)=ivstr(1)
        ivstr(4)=ivstr(2)
        ivstr(1)=1
        ivstr(2)=1
        nvstr=4
      end if
      nbx=ivstr(1)
      if(ivstr(2).le.0) ivstr(2)=ivstr(1)
      nby=ivstr(2)
      if(nbx.lt.1) nbx=1
      if(nby.lt.1) nby=1
      write(6,*) 'Pixel box is ',nbx,' x',nby
*     px,py are positions of centre x, y in the data cube, or x,y values
*     just less than centre if there are an even number of pixels. Background
*     object extends from px-nbx/2 to px-nbx/2+nbx-1, py to py+nby-1
      if(nbx.eq.1.and.nby.eq.1) then
*       series of specified points rather than a box
        nkk=(nvstr-2)/2
        nkk=min(nkk,8)
        write(6,*) 'Sample points'
        do j=1,nkk
          naxst(j)=ivstr(2*j+1)
          nayst(j)=ivstr(2*j+2)
          write(6,*) j,naxst(j),nayst(j)
        end do
       else
*       average over a box
        nxst=max(ivstr(3)-nbx/2,1)
        nyst=max(ivstr(4)-nby/2,1)
*        write(6,*) 'Center is at (',nxst,',',nyst,')'
        nxend=nxst+nbx-1
        if(nxend.gt.naxes(1)) then
          nxend=naxes(1)
          nxst=nxend-nbx+1
        end if
        nyend=nyst+nby-1
        if(nyend.gt.naxes(2)) then
          nyend=naxes(2)
          nyst=nyend-nby+1
        end if
        ixcen=(nxst+nxend)/2
        iycen=(nyst+nyend)/2
        write(6,*) 'Center at (',ixcen,',',iycen,')'
*       fill the position array
        nkk=0
        nxt=nxend-nxst+1
        nyt=nyend-nyst+1
        do i1=1,nxt
          do j=1,nyt
            nkk=nkk+1
            naxst(nkk)=nxst+i1-1
            nayst(nkk)=nyst+j-1
          end do
        end do
        write(6,*) nkk,' data points'
      end if
*     sampling increments always 1
      incs(1)=1
      incs(2)=1
      incs(3)=1
*     need line characteristics. Use 250 km/s as reference for LMC data
*     Typical HI column/velocity bin is 5E19 - 1E20. Solar Si/H 
      write(6,'(a,a6,a1,f6.1,a1,f8.4,a1)') 
     :    'ion, abundance, base redshift? [',chionh,',',dvabh,',',
     :    dvzedh,']'
      read(inputc,'(a)') inchstr
      call dsepvar(inchstr,4,dvstr,ivstr,cvstr,nvstr)
      if(cvstr(1)(1:1).eq.' ') then
*       use previous values
        cvstr(1)=chionh
        dvstr(2)=dvabh
        dvstr(3)=dvzedh
       else
        chionh=cvstr(1)(1:6)
        dvabh=dvstr(2)
        dvzedh=dvstr(3)
      end if
      if(ucase(cvstr(1)(2:2)).and.cvstr(2)(1:2).ne.'HD'
     :   .and.cvstr(2)(1:2).ne.'CO') then
        catom=cvstr(1)(1:1)//' '
        cionz=cvstr(1)(2:5)
       else
        catom=cvstr(1)(1:2)
        if(ucase(cvstr(2)(1:1))) then
          cionz=cvstr(2)(1:4)
          dvstr(2)=dvstr(3)
          dvstr(3)=dvstr(4)
         else
          cionz=cvstr(1)(3:6)
        end if
      end if
      zedb=dvstr(3)
      if(dvstr(2).gt.0.0d0) then
        abund=dvstr(2)
       else
        abund=10.0d0**dvstr(2)
      end if
      write(6,*) catom,' ',cionz,' abund:',abund,' zedb:',zedb
*     choose an arbitrary Doppler parameter for now
      bval=3.0d0
*     set base continuum to unity, and summed spectrum to zero
      do i=1,nct
        re(i)=1.0
        ds(i)=0.0
      end do
      nspec=0
*
      do ja=1,nkk
*      do ix=nxst,nxend
*        do iy=nyst,nyend
          ix=naxst(ja)
          iy=nayst(ja)
*         set local continuum to unity
          do i=1,nct
            cad(i)=1.0
          end do
          ifpixels(1)=ix
          ifpixels(2)=iy
          ifpixels(3)=1
          lpixels(1)=ix
          lpixels(2)=iy
          lpixels(3)=naxes(3)
          if(kpcen.lt.-900000) then
            kpcen=(naxes(3)+1)/2
          end if
          ifpixels(4)=1
          lpixels(4)=1
          anyf=.false.
          call ftgsvd(iounit,0,naxis,naxes,ifpixels,lpixels,incs,0.0,
     :          sdat,anyf,istat)
*         look for maximum
          jmax=0
          smax=-1.0d30
          do j=1,naxes(3)
            if(sdat(j).gt.smax) then
              smax=sdat(j)
              jmax=j
            end if
          end do
*         and last positive values stepping out from maximum
          jn=jmax
          do while(jn.gt.1.and.sdat(jn).gt.0.0d0)
            jn=jn-1
          end do
          jn=jn+1
          jx=jmax
          do while(jx.lt.naxes(3).and.sdat(jx).gt.0.0)
            jx=jx+1
          end do
          jx=jx-1
*         noise spike from zero is maximum if next line is needed:
          if(jx.lt.jn) jx=jn
*         put in velocity components
          write(6,*) 'using velocity pixels ',jn,' to',jx
          do jj=jn,jx
            zed=(1.0d0+zedb)*
     :          (1.0d0+cdelt3*dble(jj-kpcen)/2.99792458d5)-1.0d0
            col=log10(5.1d20*abund*sdat(jj))
*            write(6,*) jj,zed,col,bval
*           cad is input continuum, ca is output with absorption lines
*           re is base continuum used for zero level adjustment
            call spvoigt(ca,cad,1,nct,col,zed,bval,
     :      catom,cionz,1,re)
*           set input for next velocity to output here
            do i=1,nct
              cad(i)=ca(i)
            end do
          end do
*         add into summed spectrum
          do i=1,nct
            ds(i)=ds(i)+cad(i)
          end do
*         centre to data array 
          if(ix.eq.ixcen.and.iy.eq.iycen) then
            do i=1,nct
              da(i)=cad(i)
              de(i)=0.0000001d0
            end do
          end if
*         update total spectrum numbers in sum
          nspec=nspec+1
*        end do
      end do
*     copy sum to continuum, renormalizing
      do i=1,nct
        ca(i)=ds(i)/dble(nspec)
      end do
*     simply copy 21cm profile to data array if that was requested
*     and if only one pixel used
      if(nbx.eq.1.and.nby.eq.1.and.
     :  (chwhat(1:4).eq.'vplo'.or.chwhat(1:4).eq.'test')) then
        do j=1,naxes(3)
          da(j)=sdat(j)
        end do
        nct=naxes(3)
        nwco=2
        wcf1(1)=crpix3-cdelt3
        wcf1(2)=cdelt3
        wcftype='poly'
        helcfac=1.0d0
        noffs=0
        vacind='vac'
      end if
 400  call ftclos(iounit, istat)
 409  return
 401  write(6,*) 'Could not allocate input unit number'
      goto 400
 402  write(6,*) 'Failed to open data cube ',cvstr(1)(1:lnch)
      goto 400
 403  write(6,*) 'Data axis information not read'
      goto 400
      end
