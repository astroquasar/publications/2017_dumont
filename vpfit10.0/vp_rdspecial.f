      subroutine vp_rdspecial(nopx)
*
*     read in special constraints, or set defaults if file
*     vp_setup.dat does not exist in the current directory
*
      implicit none
      include 'vp_sizes.f'
*
*     IN: nopx  integer  controls printout: nopx=0: none, 1 - restricted,
*                          2 - all
      integer nopx
*
*     Local:
      integer i,j,jdiff,jxl,lcsrc
      double precision wmdiff
*     double precision prevtot,totflux
*
*     function declarations
      logical nocgt,ucase
*     bgtdrop set?
      logical lbgtdrop
      integer lastchpos
*
*     variables in common blocks to be set:
      double precision bvalmin,bvalmax,bltdrop,bgtdrop,colltdrop,
     :                     clogltdrop,cvaldrop,clvaldrop,cvalmax,
     :                     clvalmax,bvalminh,bvalmaxh,clvalmin,
     :                     cvalmin
      common/vpc_bvallims/bvalmin,bvalmax,bltdrop,bgtdrop,colltdrop,
     :                     clogltdrop,cvaldrop,clvaldrop,cvalmax,
     :                     clvalmax,bvalminh,bvalmaxh,clvalmin,
     :                     cvalmin
*
      double precision sigsclx
      common/vpc_sigsclx/sigsclx
*     Totals (col) after lastch, forms after firstch to lastch
*     Temperatures (b) after lastch
      character*2 lastch,firstch
      common/vpc_ssetup/lastch,firstch
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     temperature units
      double precision thunit,thmax
      common/vpc_thscale/thunit,thmax
*     mimformat for da/a, I/O units
      double precision unitamim
      common/vpc_scalemim/unitamim
*
*     Column density for inclusion of ion in ALL regions
      double precision fcollallzn
      common/vpc_colallzn/fcollallzn
*     general control variables
      character*4 chcv(10)
      common/vpc_chcv/chcv
*     chcv(1) controls use of cursor for input guesses N,b,z
*     chcv(2) = 'wr25' write Hessian matrix to fort.25
*     chcv(3) = 'citn' keep current iteration in junk.dat,
*       and remove the file on satisfactory exit.
*     chcv(4) = 'pcvalsetup' (or not blank) prompt for setup parameters,
*       If blank, prompt skipped. Note then 'il' set automatically.
*     chcv(5) = 'oldcont?' prompt to restore continuum, else
*       skip it.
*     chcv(6) = 'nofix' will remove fixed redshifts from the list
*       after lines rejected.
*     chcv(7) = 'turb' xlinked lines linked turbulently (temperature=0.1K)
*     chcv(8) = 'vform' for fort.26 file with variable format set
*       by size of minimum error
*     chcv(9) = 'c26form' for fort.26 style o/p to screen as iterations
*       progress
*
*     nvdots is number of variables (=3*nlines) for dot display
      integer nvdots
      common/vpc_dots6/nvdots
*     max number of add/remove lines iterations, bval adjust preprof.
      integer maxadrit
      logical lbadj
      common/vpc_maxits/maxadrit,lbadj
*
*     workspace for character unscrambling:
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
      double precision dvstr(24)
      common/vpc_dsepspace/dvstr
*     Lyman series minimum wavelength in table (determined in ewprg),
*     vblstar=no Doppler parameters above min wavelength (def 3.0),
*     collsmin=min HI column for Ly cont inclusion (1.0e15) NOT LOG!!
      double precision wlsmin,vblstar,collsmin
      common/vpc_lycont/wlsmin,vblstar,collsmin
*     email notification
      logical enotify
      common/vpc_enotify/enotify
*     display column density option
      double precision dispcolf
      common/vpc_dispcolf/dispcolf
*     vp_abfind parameters
      integer minsm
      double precision bsep,sigl,siglxd,sigpeak,bminshi
      common/vpc_abfpm/bsep,sigl,siglxd,sigpeak,bminshi,minsm
*     vp_addline parameters
      double precision addrat
      common/vp_adprm/addrat
*     vp_scontf parameters
      logical lcontv
      character*4 chcontc
      integer nconn
      double precision conchst,conbval,conlhden
      common/vpc_scontf/conchst,conbval,conlhden,lcontv,
     :         chcontc,nconn
*     chunk FWHM well sampled by pixels (if this variable = .true.)
      logical lresint(maxnch)
      double precision dlgres
      common/vpc_lresint/dlgres,lresint
*     chunk subdivision variables
      integer nminsubd,nmaxsubd
      double precision bindop
      common/vpc_vpsubd/bindop,nminsubd,nmaxsubd
      integer nxsubcht
      double precision subchthres
      common/vpc_vpsubd2/subchthres,nxsubcht
*     chunk list special variables
      integer linchk( maxnio )
      common/vpc_linchk/linchk
      character*4 chlnk(maxnio)
      common/vpc_chlnchk/chlnk
*     printout control variables
      logical lwrsum,lwropen,lerrsum
      character*60 cwrsum
      character*16 chext13
      integer nwrsum,lext13
      common/vpc_f13hout/cwrsum,nwrsum,lwrsum,lwropen,lerrsum,
     :                chext13,lext13
      logical lwr26s,lwr26open
      character*60 cwr26s
      integer nwr26s,ndp26s,len26tem
      common/vpc_f26hout/cwr26s,nwr26s,lwr26s,lwr26open,ndp26s,
     :       len26tem
*     append output summary file
      logical lwrapp
      character*60 cwrapp
      common/vpc_f26app/lwrapp,cwrapp
*
      double precision colglo
      common/vpc_abguess/colglo
*     Zero level limits
      double precision zlevmin,zlevmax
      common/vpc_zlevlims/zlevmin,zlevmax
*     ajc 3-oct-93  number of sub-bins (per bval)
      integer nexpd
      common / nexpd / nexpd
*     old data file and parameters (blank if not used)
      integer ncumset
      character*132 comchstr
      logical ldate26
      logical lcuminc
      common/vpc_comchstr/comchstr,ldate26,lcuminc,ncumset
*
      logical lflrdin
      character*132 ch26flln
      common/vpc_f26rmult/ch26flln,lflrdin
*
*     reserved character
      character*4 specchar
      common/vpc_specchar/specchar
*     upper limit allowed for guessed b value
      double precision bmaxest
      common/vp_bmaxest/bmaxest
*     interactive or batch (controls prompting when things go wrong)
      logical linteract
      common/vpc_linteract/linteract
*     iteration parameters
      integer nminchsqit
      double precision cdstsf,zstep,bstep,chsqdif1,chsqdnth
      double precision chsqdif2
      common/vpc_jkw2/cdstsf,zstep,bstep,chsqdif1,chsqdnth,
     :       chsqdif2,nminchsqit
*     iteration parameter limits
      double precision chsqdlim1
      common/vpc_jkw2lim/chsqdlim1
*     Current line parameters for guess (default = Ly-a)
      logical lgs
      character*2 ings
      character*4 lvgs
      double precision wvgs,fkgs,algs,amgs
      common/vpc_pargs/wvgs,fkgs,algs,amgs,lgs,ings,lvgs
*     default line parameters
      character*2 ingsdef
      character*4 lvgsdef
      double precision wvgsdef,fkgsdef,algsdef,amgsdef
      common/vpc_pargsdef/wvgsdef,fkgsdef,algsdef,amgsdef,
     :                       ingsdef,lvgsdef
*     forced cross-linked ions, with given guessed column density offsets
      integer nionxl
      character*2 ielxl(8)
      character*4 ionxl(8)
      double precision crlogxl(8)
      common/vpc_xlion/ielxl,ionxl,crlogxl,nionxl
*     atomic data table
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm
      integer m
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),m
*     atomic mass table
      character*2 lbm
      double precision amass
      integer mmass
      common/vpc_atmass/lbm(maxats),amass(maxats),mmass
*     verbose mode
      logical verbose
      common/vp_sysout/verbose
*     monitoring level 0:minimal 1:skeletal 2:standard 3: alpha
*        4: ...      16:everything
      integer nverbose
      common/vp_sysmon/nverbose
*     output channels:
      integer lt(3)                     ! output channels for printing
      integer nopchan,nopchanh,nmonitor
      common/vpc_nopchan/lt,nopchan,nopchanh,nmonitor
*     variable fwhm limits
      double precision vfwlow,vfwup
      common/vpc_limvfwhm/vfwlow,vfwup
*     summary printout precision thresholds
      double precision dmer6,dmer8
      common/vpc_dminerval/dmer6,dmer8
*     parameter error estimates rescaled, or not? Default is yes
      logical lrsparmerr,luvespop
      common/vpc_rsparmerr/lrsparmerr,luvespop
*     log or linear variable indicator, 1 for logN, 0 for linear
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
*     Variables which apply to fitted chunks of spectrum
*     wavelength coefficients or array
      integer ngd,npexsm
      integer numpts(maxnch)
      double precision wcoeff(maxnch)
      common/vpc_jkw3/wcoeff,ngd,npexsm,numpts
*     probability estimates for each chunk, and printout indicator
      integer indprobchk
      integer nprchk(maxnch)
      double precision probchk(maxnch)
      common/vpc_probchk/probchk,nprchk,indprobchk
*     Fine structure & me/mp:  lqmucf if on; lchvsqmu true for region stats
      logical lqmucf,lchvsqmu
      double precision qscale
      double precision qmucf
      common/vpc_qmucf/qmucf(maxats),qscale,lqmucf,lchvsqmu
*     LM method steering variables
*     forceLM=.true. allows LM method to be forced if no GN improvement
*             otherwise just ends iteration
      logical forceLM,neverLM
      common/vpc_LMvars/forceLM,neverLM
*     finite difference default stepsizes (used in cvalset)
      double precision cdsteplogh,cdsteplinh,zsteph,bsteph
      double precision step4h,step5h
      common/vpc_fdsteps/cdsteplogh,cdsteplinh,zsteph,bsteph,
     1   step4h,step5h  
*     source path
      character*132 csrcpath
      common/vpc_srcpath/csrcpath
*
*     debugging variables -- these will change
*     VPFIT has same common block,  and others as below
      integer ndbranch
      common/vpc_debug1/ndbranch
      ndbranch=0
*
*
*     set defaults
*
      forceLM=.true.
      neverLM=.false.
*     no chunk probability printout in summary
      indprobchk=0
*     finite difference stepsizes
      cdsteplogh=0.005d0
      cdsteplinh=0.01d0
      zsteph=1.0d-5
      bsteph=0.125d0
      step4h=1.0d0
      step5h=1.0d0
*
      linteract=.false.
*     rescale parameter error estimates using final chi^2 value?
      lrsparmerr=.true.
*     UVES_popler input? [Reset in vp_readfs]
      luvespop=.false.
*     output channels, default to screen (6) & fort.18
*     fort.15 is last iteration if wanted
      lt(1)=6
      lt(2)=18
      lt(3)=15
      nopchan=2
      nopchanh=2
      nmonitor=0
      nminchsqit=0
*     no cross-linked ions
      nionxl=0
*     npexsm is the no. of extra points to be used, either side of the
*     fitting region, when calculating the model function.  This is so 
*     that convolution with the instrumental profile works properly
      npexsm=10
*     default to logN (indvar=1), for N indvar=0, emission indvar=-1
      indvar=1
*     column density scalefactors for internal use
      scalelog=1.0d0
      scalefac=1.0d-14
*     printout to fort.13 style file at end
      nexpd=5
      lbadj=.true.
      lwropen=.false.
      lwrsum=.false.
      lerrsum=.false.
      lwr26s=.false.
      lwrapp=.false.
      lwr26open=.false.
      cwrsum=' '
      ndp26s=0
      len26tem=0
*     repeat fort.26 read parameters
      ch26flln=' '
      lflrdin=.false.
*     fort.26 datestamp defaults to none
      ldate26=.false.
*     default is don't accumulate to input file
      lcuminc=.false.
*
      colglo=0.0d0
      zlevmin=-1.0d30
      zlevmax=1.0d30
      chext13=' '
      lext13=0
*
      do i=1,10
        chcv(i)='    '
      end do
*     rfc 15.9.98 cursor first guesses in interactive mode now default
      chcv(1)='gcur'
*
*     maximum number of add/remove iterations:
      maxadrit=25
*     display dots for long interactive  calculations:
      nvdots=30000
*
      lbgtdrop=.false.
      enotify=.false.
      vblstar=3.0d0
      collsmin=1.0d15
      bvalmin=0.05d0
      bvalmax=5.0d3
      bvalminh=bvalmin
      bvalmaxh=bvalmax
      cvaldrop=1.0d8
      clvaldrop=8.0d0
      cvalmin=9.99d7
      clvalmin=7.99d0
      bltdrop=0.050001d0
      bgtdrop=4.99999d3
      colltdrop=1.0d14
      clogltdrop=14.0d0
      clvalmax=30.0d0
      cvalmax=1.0d30
      sigsclx=1.0d0
      lastch='z'
      firstch='z'
      fcollallzn=4.162d18
*     maximum estimated b value - best to start small rather
*     than too big
      bmaxest=35.0d0
*     default dispcolf (display line cutoff for estimates only)
*     corresponds to Ly-a logN(HI)=11.7
      dispcolf=2.081d12
*     default bias of absorption/emission significance for vp_addline
      addrat=1.75d0
*     ------------------------
*     defaults for autoline stuff (vp_abfind)
*     minimum line separation
      bsep=20.0d0
*     channel smoothing
      minsm=3
*     significance limits (*sigma)
*     sigl = significance limit for whole line
*     siglxd = sign. for each component
*     sigpeak= sign. for peak in component relative zero
      sigl=5.0d0
      siglxd=5.0d0
      sigpeak=4.5d0
*     minimum b-value for guessed single weak hydrogen
      bminshi=25.0d0
*     ------------------------
*     continuum estimator defaults
*     if in doubt, don't:
      lcontv=.false.
      conchst=1.80d0
      nconn=10
      conbval=5.0d0
      conlhden=12.3d0
      chcontc=' '
*     ------------------------
*     chunk list special variables defaults
      do i=1, maxnio
        chlnk(i)=' '
        linchk(i)=0
      end do
*     ------------------------
*     reserved character for internally set constraints
      specchar='S'
*     ------------------------
*     variable FWHM limits, as multiples of the local default
      vfwlow=0.95d0
      vfwup=1.0d0/vfwlow
*     ------------------------
*
*     try to open file
      inchstr=' '
      call getenv('VPFSETUP',inchstr)
      if(inchstr(1:1).eq.' ') then
        if(csrcpath(1:1).eq.' ') then
          write(6,*) '------------------------'
          write(6,*) 'Using local vp_setup.dat'
          write(6,*) '------------------------'
          inchstr='vp_setup.dat'
         else
*         use the distibuted defaults
          lcsrc=lastchpos(csrcpath)
          if(csrcpath(lcsrc:lcsrc).eq.'/') then
            inchstr=csrcpath(1:lcsrc)//'vp_setup.dat'
           else
            inchstr=csrcpath(1:lcsrc)//'/vp_setup.dat'
          end if
          write(6,*) '------------------------'
          write(6,*) 'Using supplied vp_setup.dat'
          write(6,*) '------------------------'
        end if        
      end if
      open(unit=19,file=inchstr,status='old',err=1066)
*     read in lines and change variables as appropriate
*     finish on <EOF> (so you can have blank or nonsense lines anywhere)
12    read(19,'(a)',end=11) inchstr
*     skip lines with first character = '!' or '#' as comments
      if(inchstr(1:1).eq.'!'.or.inchstr(1:1).eq.'#') goto 12
      call dsepvar(inchstr,8,dvstr,ivstr,cvstr,nvstr)
*
*     DEBUG variables [used in main segment VPFIT,and as below]
      if(cvstr(1)(1:5).eq.'DEBUG') then
*       this variable used for targeted diagnostics
*        0: print everything
*       1x: DERIV: 10 parameters & derivatives
*                  11 eigenvalues & eigenvectors
*       2x: UCOPTV: 21
*                   22 Univariate minimization updated parameters
*       3x: UDCHOLE: 31 Lower triangular decomposition
*
        ndbranch=ivstr(2)
      end if
*     List is partly alphabetical
*
*     BGTDROP  -- b value above which lines are checked
*     to see if they should be dropped
      if(cvstr(1)(1:7).eq.'bgtdrop'.or.
     :         cvstr(1)(1:7).eq.'BGTDROP') then
        bgtdrop=dvstr(2)
        lbgtdrop=.true.
          goto 12
      end if
*
*     BLTDROP  -- b value below which lines are checked against COLLTDROP
*     to see if they should be dropped
      if(cvstr(1)(1:7).eq.'bltdrop'.or.
     :           cvstr(1)(1:7).eq.'BLTDROP') then
        bltdrop=dvstr(2)
          goto 12
      end if
*
*     BVALMIN -- minimum b value
      if(cvstr(1)(1:7).eq.'bvalmin'.or.
     1                  cvstr(1)(1:7).eq.'BVALMIN') then
        bvalmin=dvstr(2)
        if(bvalmin.lt.1.0d-5) bvalmin=1.0d-5
*       Now allows an additional parameter for HI
        if(dvstr(3).gt.bvalmin) then
          bvalminh=dvstr(3)
         else
          bvalminh=bvalmin
        end if
          goto 12
      end if
*
*     BVALMAX -- maximum b value
      if(cvstr(1)(1:7).eq.'bvalmax'.or.
     1                   cvstr(1)(1:7).eq.'BVALMAX') then
        bvalmax=dvstr(2)
        if(bvalmax.gt.1.0d5) bvalmax=1.0d5
        if(bvalmax.lt.1.0d-4) bvalmax=1.0d-4
*       Now allows an additional parameter for HI
        if(dvstr(3).gt.0.0d0) then
          bvalmaxh=dvstr(3)
         else
          bvalmaxh=bvalmax
        end if
*       reset bgtdrop if necessary
        if(.not.lbgtdrop) then
*         bgtdrop not set, so set to value just below max:
          bgtdrop=0.99999d0*bvalmax
        end if
          goto 12
      end if
*
*     CVALMAX - maximum column density value
      if(cvstr(1)(1:7).eq.'cvalmax') then
        if(dvstr(2).lt.25.0d0) then
          cvalmax=10.0d0**dvstr(2)
          clvalmax=dvstr(2)
         else
          cvalmax=dvstr(2)
          clvalmax=log10(cvaldrop)
        end if
          goto 12
      end if
*
*     CVALMIN - maximum column density value
      if(cvstr(1)(1:7).eq.'cvalmin') then
        if(dvstr(2).lt.25.0d0) then
          cvalmin=10.0d0**dvstr(2)
          clvalmin=dvstr(2)
         else
          cvalmin=dvstr(2)
          clvalmin=log10(cvaldrop)
        end if
          goto 12
      end if
*
*     CVALDROP - value below which column density dropped
      if(cvstr(1)(1:7).eq.'cvaldro') then
        if(dvstr(2).lt.20.0d0) then
          cvaldrop=10.0d0**dvstr(2)
          clvaldrop=dvstr(2)
         else
          cvaldrop=dvstr(2)
          clvaldrop=log10(cvaldrop)
        end if
          goto 12
      end if
*
*     CHISQTHRES -- relative chi^2 decrement for stopping
      if(cvstr(1)(1:6).eq.'chisqt'.or.
     :          cvstr(1)(1:6).eq.'CHISQT')then
        chsqdif1=dvstr(2)
        if(chsqdif1.le.0.0d0) then
          chsqdif1=0.001d0
        end if
*       roundoff error sets in if value set too low, so set to
*       a minimum if necessary
        if(chsqdif1.lt.chsqdlim1) then
          chsqdif1=chsqdlim1
*         and warn the user you have done it
          write(6,'(a,1pe12.3)') 'Stopping Delta-Chi^2 reset to',
     :                              chsqdif1
        end if
        if(dvstr(3).gt.1.0d0) then
          chsqdnth=dvstr(3)
         else
          chsqdnth=4.0d0
        end if
        if(dvstr(4).gt.0.0d0) then
          chsqdif2=max(chsqdif1,dvstr(4))
         else
          chsqdif2=1.0d-2
        end if
*       minimum number chisq iterations
        if(ivstr(5).gt.0) then
          nminchsqit=ivstr(5)
        end if
        goto 12
      end if
*
*     COLGLO -- column density below which guesses ignored
      if(cvstr(1)(1:6).eq.'colglo'.or.
     1                  cvstr(1)(1:6).eq.'COLGLO') then
        colglo=dvstr(2)
        if(colglo.gt.34.0d0) then
          colglo=log10(colglo)
        end if
          goto 12
      end if
*
*     COLLTDROP -- column density below which narrow lines are dropped
      if(cvstr(1)(1:7).eq.'colltdr'.or.cvstr(1)(1:7).eq.'clogltd'.or.
     :        cvstr(1)(1:7).eq.'COLLTDR'.or.cvstr(1)(1:7).eq.'CLOGLTD')
     :        then
        colltdrop=dvstr(2)
        if(colltdrop.gt.34.0d0) then
          clogltdrop=log10(colltdrop)
         else
          clogltdrop=colltdrop
          colltdrop=10.0d0**colltdrop
        end if
          goto 12
      end if
*
*     SIGSCALE -- sigma /rms scale
      if(cvstr(1)(1:7).eq.'sigscal'.or.
     1                      cvstr(1)(1:7).eq.'SIGSCAL') then
        sigsclx=dvstr(2)
        if(sigsclx.eq.0.0d0) sigsclx=1.0d0
          if(nopx.ge.2.and.abs(sigsclx-1.0d0).gt.1.0d-4) then
*           warn user that something unusual is happening
          write(6,'(''  '')')
          write(6,'(''*****************************************'')')
            write(6,'(''* IF RMS FILE ABSENT, AND NO SCLFILE,   *'')')
          write(6,'(a,f7.3,a)') '* EFFECTIVE SIGMAS SCALED DOWN ',
     :        sigsclx,'  *'
          write(6,'(''* SEE VP_SETUP.DAT FILE                 *'')')
          write(6,'(''*****************************************'')')
          write(6,'(''  '')')
          end if
      end if
*
*     SETSPECCHAR  - set special character for internally fixed
*     variables:
      if(cvstr(1)(1:7).eq.'setspec'.or.
     1                      cvstr(1)(1:7).eq.'SETSPEC') then
        if(cvstr(2)(1:1).ne.' '.and.dvstr(2).eq.0.0d0) then
          specchar=cvstr(2)(1:1)
         else
          if(nopx.ge.2) then
            write(6,*) 'Default fixed parameter flag: ',specchar(1:1)
          end if
        end if
      end if
*
*     COLFORMCH - last character for use as CLOUDY rather than block
*     form following for cloumnd densities in a complex
      if(cvstr(1)(1:6).eq.'colfor'.or.
     1                      cvstr(1)(1:6).eq.'COLFOR') then
        firstch=cvstr(2)(1:1)//' '
        if(firstch.eq.'  ') firstch='z'
        if ( nocgt( 'z', firstch ) ) then
          write(6,*) 'Block form column density after:   ', firstch
        end if
      end if
**
*     LASTCHTIED - last character for used as tied rather than total
*     properties for a complex (later ones are for total N)
      if(cvstr(1)(1:6).eq.'lastch'.or.
     1                      cvstr(1)(1:6).eq.'LASTCH') then
        lastch=cvstr(2)(1:1)//' '
        if(lastch.eq.'  ') lastch='z'
        if ( nocgt( 'z', lastch ) ) then
          write(6,*) 'Total column density after:   ', lastch
        end if
      end if
*
*     MAXADREM - max no of add/remove cycles before it gives up
*
      if(cvstr(1)(1:6).eq.'maxadr'.or.
     :       cvstr(1)(1:6).eq.'MAXADR') then
        if(ivstr(2).gt.0) then
          maxadrit=ivstr(2)
         else
          maxadrit=25
        end if
      end if
*
*     NCHEXTEND - number of channels to extend by for instrument profile
*                 convolution (default is 10)
*
      if(cvstr(1)(1:6).eq.'nchext'.or.
     1        cvstr(1)(1:6).eq.'NCHEXT') then
        if(ivstr(2).gt.0) then
          npexsm=ivstr(2)
          write(6,*) 'Each region will be extended by',
     1               npexsm,' channels'
        end if
      end if
*
*     NOVARSYS - number of variables per system
*
      if(cvstr(1)(1:6).eq.'novars'.or.
     1        cvstr(1)(1:6).eq.'NOVARS') then
        if(ivstr(2).gt.1.and.ivstr(2).le.maxpps) then
          noppsys=ivstr(2)
          write(6,*) noppsys,' parameters per ion'
         else
          noppsys=3
        end if
      end if
*
*       ---------------------------------------------------------------
*
*       INTERNAL VARIABLES (should not normally be reset)
*
      if(cvstr(1)(1:6).eq.'intern'.or.
     :        cvstr(1)(1:6).eq.'INTERN') then
*
*         THUNIT - internal temperature units (default is 1000K)
        if(cvstr(2)(1:6).eq.'thunit'.or.
     :        cvstr(2)(1:6).eq.'THUNIT') then
          if(dvstr(3).gt.0.0d0) then
*           Need to allow for THMAX being set earlier
            if(thmax.lt.9.99d24) then
              thmax=thmax*thunit/dvstr(3)
            end if
            thunit=dvstr(3)
          end if
*         can get here unchanged if dvstr(3).le.0.0d0
          goto 12
        end if
*
        if(cvstr(2)(1:5).eq.'thmax'.or.
     :        cvstr(2)(1:5).eq.'THMAX') then
          if(dvstr(3).gt.0.0d0) then
            thmax=dvstr(3)/thunit
          end if
*         can get here unchanged if dvstr(3).le.0.0d0
          goto 12
        end if
*
*       LOGNSCALE - scalelog value reset to  input number
        if(cvstr(2)(1:6).eq.'lognsc'.or.
     :        cvstr(2)(1:6).eq.'LOGNSC') then
          if(dvstr(3).gt.0.0d0) then
            scalelog=dvstr(3)
          end if
          goto 12
        end if
*
*       FACNSCALE - scalelog value reset to  input number
        if(cvstr(2)(1:6).eq.'facnsc'.or.
     :        cvstr(2)(1:6).eq.'FACNSC') then
          if(dvstr(3).gt.0.0d0) then
            scalefac=dvstr(3)
          end if
          goto 12
        end if
        goto 12
      end if
*
*       ---------------------------------------------------------------
*
*     4th parameter stuff
*
*     DAOAUNIT - I/O units for da/a
*
      if(cvstr(1)(1:6).eq.'daoaun'.or.
     :        cvstr(1)(1:6).eq.'DAOAUN') then
        if(dvstr(2).ge.1.0d4) then
*         in case somebody thinks it means divide instead of multiply
          unitamim=1.0d0/dvstr(2)
         else
          if(dvstr(2).gt.0.0d0) then
            unitamim=dvstr(2)
            write(6,*) 'Parameter 4 units for da/a are',unitamim
          end if
        end if
*       can get here unchanged if dvstr(2).le.0.0d0
      end if
*
*
*        ----------------------------------------------------------------
*
*     NFWHMPIX - number of bins required per FWHM for original
*                  pixellation in instrument profile convolution
*                  0=always, 1.0d20=never do this
*
      if(cvstr(1)(1:6).eq.'nfwhmp'.or.
     1        cvstr(1)(1:6).eq.'NFWHMP') then
        dlgres=dvstr(2)
        if(dvstr(2).le.0.0d0) then
*         might as well set the logical variable anyway
          do j=1,maxnch
            lresint(j)=.true.
          end do
        end if
      end if

*
*     NSUBBIN - number of subbins per bval width for each line
*
      if(cvstr(1)(1:6).eq.'nsubbi'.or.
     1        cvstr(1)(1:6).eq.'NSUBBI') then
        if(ivstr(2).gt.1) then
          nexpd=ivstr(2)
            bindop=dvstr(2)
          if(nexpd.gt.25) then
            write(6,*) nexpd,' sub-bins per b-value!!!!'
            write(6,*) 'THIS MIGHT BE VERY SLOW'
          end if
        end if
      end if
*
*     NSUBMIN - minimum number of subbins per pixel (default=1)
*
      if(cvstr(1)(1:6).eq.'nsubmi'.or.
     1        cvstr(1)(1:6).eq.'NSUBMI') then
        if(ivstr(2).ge.1) then
          nminsubd=ivstr(2)
          write(6,*) 'minimum',nminsubd,' sub-bins per pixel'
        end if
        if(nxsubcht.lt.nminsubd) nxsubcht=nminsubd
      end if
*
*     NSUBMAX - maximum number of subbins per pixel (default=11)
*
      if(cvstr(1)(1:6).eq.'nsubma'.or.
     1        cvstr(1)(1:6).eq.'NSUBMA') then
        if(ivstr(2).ge.1) then
          nmaxsubd=ivstr(2)
          write(6,*) 'maximum',nmaxsubd,' sub-bins per pixel'
        end if
      end if
*
*     SUBCHTHRES - Chi^2 threshold when full substepping cuts in
*                    (default is 2.0, with nsubmax=1 above that)
*
*     YET TO BE IMPLEMENTED <<<<
      if(cvstr(1)(1:6).eq.'subcht'.or.
     1        cvstr(1)(1:6).eq.'SUBCHT') then
        if(dvstr(2).gt.0.0d0) then
          subchthres=dvstr(2)
         else
          subchthres=2.0d0
        end if
*       there is an optional #substep parameter here as well
        if(ivstr(3).gt.0) then
          nxsubcht=ivstr(3)
         else
          nxsubcht=max(1,nminsubd)
        end if
        write(6,*) 'For chi^2 > ',subchthres,
     :               ' max subpixels = ',nxsubcht
      end if
*
*       -----------------------------------------------------------
*
*     LLSBMULT - multiply the b-parameter for HI by this quantity
*     for LLS check. Done if wavelength region includes points
*     less than wlsmin*(1+LLSBMULT*b/3.0e5)
      if(cvstr(1)(1:6).eq.'LLSBMU'.or.
     1        cvstr(1)(1:6).eq.'llsbmu') then
        vblstar=dvstr(2)
      end if
*
*     LLSCOLLIM - minimum HI column density for inclusion of Lyman limit
*        note: is HI column, not the log!
      if(cvstr(1)(1:6).eq.'LLSCOL'.or.
     1        cvstr(1)(1:6).eq.'llscol') then
        collsmin=dvstr(2)
        if(collsmin.lt.30.0d0) collsmin=10.0d0**collsmin
      end if
*
*     NOBADJ - don't adjust bvalues if outside range in the
*     preliminary profile stage
      if(cvstr(1)(1:6).eq.'nobadj'.or.
     1        cvstr(1)(1:6).eq.'NOBADJ') then
        lbadj=.false.
      end if
*
*       PERRSCALE - parameter error scaling using final chi^2?
      if(cvstr(1)(1:6).eq.'perrsc'.or.
     1        cvstr(1)(1:6).eq.'PERRSC') then
        if(cvstr(2)(1:1).eq.'n'.or.cvstr(2)(1:1).eq.'N') then
          lrsparmerr=.false.
         else
          lrsparmerr=.true.
        end if
      end if

*
*     ENOTIFY - execute ./vpgti.done on completion
      if(cvstr(1)(1:6).eq.'enotif'.or.
     1        cvstr(1)(1:6).eq.'ENOTIF') then
        enotify=.true.
      end if
*
*     CHCV (character variable flags) checks:
*
*     CHCV(1): GCURSOR - use cursor in preference to keyboard
      if(cvstr(1)(1:6).eq.'gcurso'.or.
     1        cvstr(1)(1:6).eq.'GCURSO') then
        chcv(1)='gcur'
      end if
*     CHCV(1): BLANK -- revert to keyboard input for guesses
      if(cvstr(1)(1:6).eq.'keyboa'.or.
     1        cvstr(1)(1:6).eq.'KEYBOA') then
        chcv(1)='    '
      end if
*
*     CHCV(4): PCVALS to prompt for metric, ill-conditioned, verbose ...
*     initially
      if(cvstr(1)(1:6).eq.'pcvals'.or.
     1        cvstr(1)(1:6).eq.'PCVALS') then
        chcv(4)='pcva'
      end if
*
*     CHCV(5): OLDCONT? to prompt restoration of continuum at end
*     initially
      if(cvstr(1)(1:6).eq.'oldcon'.or.
     1        cvstr(1)(1:6).eq.'OLDCON') then
        chcv(5)='oldc'
      end if
*
*       CHCV(6): NOFIX to remove fixed flags (except for special variables)
*
      if(cvstr(1)(1:5).eq.'nofix'.or.
     :        cvstr(1)(1:5).eq.'NOFIX') then
        chcv(6)='nofi'
      end if
*
*     CHCV(8): VFORM for variable format depending on minimum error
*
      if(cvstr(1)(1:5).eq.'vform'.or.
     :        cvstr(1)(1:5).eq.'VFORM') then
        chcv(8)='vfor'
*       there may be a second parameter forcing long version output
        if(cvstr(2)(1:1).ne.' ') then
*         want high precision summary file
          dmer6=1.0d-3
          dmer8=1.0d-3
        end if
      end if
*
*       CHCV(9): C26FORM for current iteration to screnn in fort.26 style
*
      if(cvstr(1)(1:4).eq.'c26f'.or.
     :        cvstr(1)(1:4).eq.'C26F') then
        chcv(9)='c26f'
      end if
*
*
*
*     FCOLLALLZN: Column density limit for putting line in all chunks
      if(cvstr(1)(1:8).eq.'fcollall'.or.
     1       cvstr(1)(1:8).eq.'FCOLLALL') then
        fcollallzn=dvstr(2)
*       check this is sensible, and convert to linear if necessary!
        if(fcollallzn.lt.34.0d0) fcollallzn=10.0d0**fcollallzn
        if(fcollallzn.lt.cvaldrop) fcollallzn=cvaldrop
      end if
*
*     DATESTAMP: print date in comment part of f26.summary file
      if(cvstr(1)(1:4).eq.'date'.or.
     1       cvstr(1)(1:4).eq.'DATE') then
        ldate26=.true.
      end if
*
*     DOTSNO: number of systems at which Andrew's dots appear
      if(cvstr(1)(1:4).eq.'dots'.or.
     1       cvstr(1)(1:4).eq.'DOTS') then
        nvdots=ivstr(2)
*       check this is sensible
        if(nvdots.lt.1) nvdots=10
        nvdots=nvdots*3
      end if
*
*     DISPCOLF: Display estimates only when col*f > number
*              (used by vp_dlinin)
      if(cvstr(1)(1:8).eq.'dispcolf'.or.
     1       cvstr(1)(1:8).eq.'DISPCOLF') then
        dispcolf=dvstr(2)
*       check this is sensible, and convert to linear if necessary!
        if(dispcolf.lt.34.d0) dispcolf=10.0d0**dispcolf
      end if
*
*     INTERACTIVE (So prompts rather than bails out if it hits a
*         problem with a file, or has some unusual question).
      if(cvstr(1)(1:8).eq.'interact'.or.
     1       cvstr(1)(1:8).eq.'INTERACT') then
        linteract=.true.
      end if
*
*--------------------------------------------------------------------
*
*     Finite difference step sizes
      if(cvstr(1)(1:6).eq.'fdbste'.or.
     1      cvstr(1)(1:6).eq.'FDBSTE') then
        if(dvstr(2).gt.0.0d0) then
          bsteph=dvstr(2)
        end if
      end if
      if(cvstr(1)(1:6).eq.'fdclog'.or.
     1      cvstr(1)(1:6).eq.'FDCLOG') then
        if(dvstr(2).gt.0.0d0) then
          cdsteplogh=dvstr(2)
        end if
      end if
      if(cvstr(1)(1:6).eq.'fdclin'.or.
     1      cvstr(1)(1:6).eq.'FDCLIN') then
        if(dvstr(2).gt.0.0d0) then
          cdsteplinh=dvstr(2)
        end if
      end if
      if(cvstr(1)(1:6).eq.'fdzste'.or.
     1      cvstr(1)(1:6).eq.'FDZSTE') then
        if(dvstr(2).gt.0.0d0) then
          zsteph=dvstr(2)
        end if
      end if
      if(cvstr(1)(1:6).eq.'fd4vst'.or.
     1      cvstr(1)(1:6).eq.'FD4VST') then
        if(dvstr(2).gt.0.0d0) then
          step4h=dvstr(2)
        end if
      end if
*
*--------------------------------------------------------------------
*
*     Auto absorption line finder parameters:
*             (used by vp_abfind)
*
*     ABMINS: Minimum smoothing (chan) for line separation estimator
      if(cvstr(1)(1:6).eq.'abmins'.or.
     1       cvstr(1)(1:6).eq.'ABMINS') then
        minsm=ivstr(2)
        if(minsm.le.0) minsm=3
      end if
*
*     ABMNHB: Minimum Doppler parameter for weak HI
      if(cvstr(1)(1:6).eq.'abmnhb'.or.
     1       cvstr(1)(1:6).eq.'ABMNHB') then
        bminshi=dvstr(2)
        if(bminshi.le.0.0d0) bminshi=25.0d0
      end if
*
*     ABBSEP: Minimum Doppler parameter for line separation estimator
      if(cvstr(1)(1:6).eq.'abbsep'.or.
     1       cvstr(1)(1:6).eq.'ABBSEP') then
        bsep=dvstr(2)
        if(bsep.le.0.0d0) bsep=20.0d0
      end if
*
*     ABSIGL: Significance limit for a line detection
      if(cvstr(1)(1:6).eq.'absigl'.or.
     1       cvstr(1)(1:6).eq.'ABSIGL') then
        sigl=dvstr(2)
        if(sigl.le.0.0d0) sigl=5.0d0
      end if
*
*     ABSIGP: Significance peak in component relative to zero level
      if(cvstr(1)(1:6).eq.'absigp'.or.
     1       cvstr(1)(1:6).eq.'ABSIGP') then
        sigpeak=dvstr(2)
        if(sigpeak.le.0.0d0) sigpeak=4.5d0
      end if
*
*     ABSIGX: Significance limit for component detection
      if(cvstr(1)(1:6).eq.'absigx'.or.
     1       cvstr(1)(1:6).eq.'ABSIGX') then
        siglxd=dvstr(2)
        if(siglxd.le.0.0d0) siglxd=5.0d0
      end if
*
*--------------------------------------------------------------------
*
*     ADSPLIT: significance ratio spike/absn for splitting as
*     opposed to adding an absorption line
      if(cvstr(1)(1:6).eq.'adspli'.or.
     1       cvstr(1)(1:6).eq.'ADSPLI') then
        addrat=dvstr(2)
        if(addrat.le.0.5d0) addrat=1.0d0
      end if
*
*     ADCONTF: add in continuum factor parameters when the
*     chisquared gets low enough or there are lots of low b/low N systems
      if(cvstr(1)(1:6).eq.'adcont'.or.
     :       cvstr(1)(1:6).eq.'ADCONT') then
        lcontv=.true.
*       parameters are: Include continuum when normalized chisq
*       goes below #1 OR the last #2 entries in a row have b < #3
*       and log col < #4
        if(dvstr(2).gt.1.0d0) conchst=dvstr(2)
        if(ivstr(3).gt.0) nconn=ivstr(3)
        if(dvstr(4).gt.0.0d0) conbval=dvstr(4)
        if(dvstr(5).gt.0.0d0) conlhden=dvstr(5)
        chcontc=cvstr(6)(1:1)
        if(chcontc(1:1).eq.'C'.or.chcontc(1:1).eq.'f'.or.
     :         chcontc(1:1).eq.'F') chcontc='c'
      end if
*
*       GUESSLINE: line parameters for a guessed line (default is Ly-a)
*       (rfc 02/03/05)
*
      if(cvstr(1)(1:6).eq.'guessl'.or.
     :       cvstr(1)(1:6).eq.'GUESSL') then
*       Bypass this if somebody is silly enough to want to use
*       something other than an element. Some may want to use
*       the unknown line though
        if(ucase(cvstr(2)(1:1)).or.cvstr(2)(1:2).eq.'??') then
          lgs=.true.
*         parameter order is atom, ion, wavelength - rest pulled in
*         from atomic data table
          if(cvstr(2)(1:2).eq.'??') then
            ingsdef='??'
            lvgsdef='    '
            if(dvstr(3).le.0.0d0) then
              wvgsdef=1215.67d0
             else
              wvgsdef=dvstr(3)
            end if
           else
            if(ucase(cvstr(2)(2:2)).and.cvstr(2)(1:2).ne.'HD'
     :           .and.cvstr(2)(1:2).ne.'CO') then
*             one letter ion, with level run together
              ingsdef=cvstr(2)(1:1)//' '
              lvgsdef=cvstr(2)(2:5)
              wvgsdef=dvstr(3)
             else
              if(ucase(cvstr(2)(3:3))) then
                ingsdef=cvstr(2)(1:2)
                lvgsdef=cvstr(2)(3:6)
                wvgsdef=dvstr(3)
               else
                ingsdef=cvstr(2)(1:2)
                lvgsdef=cvstr(3)(1:4)
                wvgsdef=dvstr(4)
              end if
            end if
          end if
*         read in atomic data if necessary
          if(m.le.0) call vp_ewred(0)
*         now search for closest wavelength match
          wmdiff=1.0d20
          jdiff=0
*         write(6,*) m,'  ',ingsdef,lvgsdef,wvgsdef
          do j=1,m
            if(ingsdef.eq.lbz(j).and.lvgsdef.eq.lzz(j)) then
              if(abs(alm(j)-wvgsdef).lt.wmdiff) then
                wmdiff=abs(alm(j)-wvgsdef)
                jdiff=j
              end if
            end if
          end do
          if(jdiff.ne.0) then
            wvgsdef=alm(jdiff)
            fkgsdef=fik(jdiff)
            algsdef=asm(jdiff)
            amgsdef=max(1.0d0,amass(jdiff))
            write(6,*) 'Default line is ',ingsdef,lvgsdef,wvgsdef
           else
            write(6,*) ingsdef,lvgsdef,' not in atom.dat file'
            write(6,*) 'Using Ly-a as default line'
            ingsdef='H '
            lvgsdef='I   '
            wvgsdef=1215.6701d0
            fkgsdef=0.4164d0
            algsdef=6.265d8
            amgsdef=1.00794d0
            end if
          ings=ingsdef
          lvgs=lvgsdef
          wvgs=wvgsdef
          fkgs=fkgsdef
          algs=algsdef
          amgs=amgsdef
        end if
      end if
*
*     cross-link ions automatically using list provided
*
      if(cvstr(1)(1:4).eq.'xlin'.or.cvstr(1)(1:4).eq.'XLIN') then
        jxl=2
        do while(cvstr(jxl)(1:1).ne.' '.and.jxl.le.6.and.
     :    nionxl.lt.8)
          nionxl=nionxl+1
          if(ucase(cvstr(jxl)(2:2)).and.cvstr(jxl)(1:2).ne.'HD'
     :         .and.cvstr(jxl)(1:2).ne.'CO') then
            ielxl(nionxl)=cvstr(jxl)(1:1)
            ionxl(nionxl)=cvstr(jxl)(2:5)
           else
            ielxl(nionxl)=cvstr(jxl)(1:2)
            ionxl(nionxl)=cvstr(jxl)(3:6)
          end if
          crlogxl(nionxl)=dvstr(jxl+1)
          jxl=jxl+2
        end do
      end if
*
*     cross-link ions linked with same Doppler parameters (i.e. turbulent)
*
      if(cvstr(1)(1:4).eq.'turb'.or.cvstr(1)(1:4).eq.'TURB') then
        chcv(7)='turb'
      end if
*
*-------------------------------------------------------------------------
*
*     L-M method not forced if no GN convergence
      if(cvstr(1)(1:5).eq.'nofLM'.or.cvstr(1)(1:5).eq.'NOFLM') then
        forceLM=.false.
      end if
*     Always bypass LM method:
      if(cvstr(1)(1:5).eq.'LMnev'.or.cvstr(1)(1:5).eq.'LMNEV') then
        neverLM=.true.
      end if
*
*-------------------------------------------------------------------------
*
*     VERBOSE - verbose mode level
      if(cvstr(1)(1:7).eq.'verbose'.or.
     :       cvstr(1)(1:7).eq.'VERBOSE') then
        nverbose=ivstr(2)
        if(nverbose.gt.12.or.cvstr(2)(1:1).eq.' ') then
          verbose=.true.
          if(nverbose.eq.0) nverbose=12
        end if
      end if
*
*     NOPCHAN: output control for each iteration
      if(cvstr(1)(1:5).eq.'nopch'.or.
     :       cvstr(1)(1:5).eq.'NOPCH') then
        nopchan=ivstr(2)
        if(nopchan.gt.3) nopchan=3
        if(nopchan.lt.0) nopchan=0
        nopchanh=nopchan
        if(cvstr(3)(1:1).ne.' ') then
*         monitor selected output if nopchan=0?
          nmonitor=1
         else
          nmonitor=0
        end if
      end if
*
*     WR13SUM: write out summary in file with rootname given here
      if(cvstr(1)(1:5).eq.'wr13s'.or.
     :       cvstr(1)(1:5).eq.'WR13S') then
        lwrsum=.true.
*       parameter is the root filename, which defaults to f13.w
        if(cvstr(2)(1:1).ne.' ') then
          cwrsum=cvstr(2)
          nwrsum=60
          do while (nwrsum.gt.1.and.cwrsum(nwrsum:nwrsum).eq.' ')
            nwrsum=nwrsum-1
          end do
         else
          cwrsum='f13.w'
          nwrsum=5
        end if
*       extended precision wavelength set filename?
        if(ivstr(3).gt.0) then
          ndp26s=ivstr(3)
        end if
*       give error information as well?
        if(cvstr(4)(1:3).eq.'err'.or.cvstr(4)(1:3).eq.'ERR') then
          lerrsum=.true.
        end if
      end if
*
*     WR26SUM: write out standard summary in file with rootname given here
      if(cvstr(1)(1:5).eq.'wr26s'.or.
     :       cvstr(1)(1:5).eq.'WR26S') then
        lwr26s=.true.
*       parameter is the root filename, which defaults to f26.w
        if(cvstr(2)(1:1).ne.' ') then
          cwr26s=cvstr(2)
          nwr26s=60
          do while (nwr26s.gt.1.and.cwr26s(nwr26s:nwr26s).eq.' ')
            nwr26s=nwr26s-1
          end do
         else
          cwr26s='f26.w'
          nwr26s=5
        end if
        if(ivstr(3).gt.0) then
          ndp26s=ivstr(3)
        end if
      end if
*
*     WR26PROB: include chunk probability estimates in output summary
*     [numbers are unlabelled, so this makes sure the user is aware]
      if(cvstr(1)(1:6).eq.'wr26pr'.or.
     :       cvstr(1)(1:6).eq.'WR26PR') then
        if(ivstr(2).gt.0) then
          indprobchk=ivstr(2)
         else
          indprobchk=1
        end if
      end if
*
*     WR26FSTAT: include chunk flux statistics in output summary
*     Used for da/a errors, so happens only for 4 parameters/line
      if(cvstr(1)(1:6).eq.'wr26fs'.or.
     :       cvstr(1)(1:6).eq.'WR26FS') then
        lchvsqmu=.true.
      end if

*
*     WRAPP26: append standard summary in file with name given here
      if(cvstr(1)(1:5).eq.'wrapp'.or.
     :       cvstr(1)(1:5).eq.'WRAPP') then
        lwrapp=.true.
*       parameter is the root filename, which defaults to f26.list
        if(cvstr(2)(1:1).ne.' ') then
          cwrapp=cvstr(2)
         else
          cwrapp='f26.list'
        end if
      end if
*
*     WR25: write out Hessian matrix to fort.25
      if(cvstr(1)(1:4).eq.'wr25'.or.
     :       cvstr(1)(1:4).eq.'WR25') then
        chcv(2)='wr25'
      end if
*
*     WRCITN: write out current iteration to junk.dat,
*        and remove it if program exits properly.
      if(cvstr(1)(1:6).eq.'WRCITN'.or.
     :       cvstr(1)(1:6).eq.'wrcitn') then
        chcv(3)='citn'
      end if
*
*     ZEROLEVELS: set limits for range of __ constant (nn+1) term
      if(cvstr(1)(1:6).eq.'ZEROLE'.or.
     :       cvstr(1)(1:6).eq.'zerole') then
        if(cvstr(2)(1:1).ne.' ') then
          zlevmin=dvstr(2)
          if(zlevmin.lt.-1.0d30) zlevmin=-1.0d30
        end if
        if(cvstr(3)(1:1).ne.' ') then
          zlevmax=dvstr(3)
          if(zlevmax.gt.1.0d30) zlevmax=1.0d30
        end if
        if(zlevmax.le.zlevmin) then
*         write out a warning, and rest to unconstrained
          write(6,*) 'ZERO LEVEL CONSTRAINTS IGNORED <<<<<<'
          zlevmax=1.0d30
          zlevmin=-1.0d30
        end if
      end if
*
*     Used by RDGEN alone:
*     BMAXEST: maximum estimated b-value from cursor
      if(cvstr(1)(1:6).eq.'BMAXES'.or.
     :       cvstr(1)(1:6).eq.'bmaxes') then
        if(dvstr(2).gt.0.0d0) then
          bmaxest=dvstr(2)
        end if
      end if
*
*
      goto 12

11    continue
*     check values make sense
      if(bvalmin.gt.bvalmax) then
*       revert to defaults
        bvalmin=0.05d0
        bvalmax=5.0d3
      end if
*     and print out values
      if(nopx.ge.1) then
        write(6,'(1pe9.2,a,e9.2)') bvalmin,'< b <',bvalmax
      end if
      if(nopx.ge.2) then
        write(6,'(a,1pe9.2,a,0pf8.3)')
     :        ' Drop system if b <',bltdrop,' & logN <',clogltdrop
        write(6,'(a,f8.3)') '  or if logN <',clvaldrop
        write(6,'(a,1pe9.2)') '  or if b >',bgtdrop
      end if
      close(unit=19)
1066  continue
      return
      end


