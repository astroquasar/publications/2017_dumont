      subroutine rd_estcolb(da,de,ca,n,atom,ion,rstw,wl, colg,bg)
*
*     estimate the column density and Doppler parameter for a line
*     returned as colg & bg
*
      implicit none
      include 'vp_sizes.f'
      integer n
      double precision da(n),de(n),ca(n)
      character*2 atom
      character*4 ion
      double precision rstw,wl,colg,bg
*     Local:
      logical lgo
      integer l1,ln,lx
      integer jlin
      double precision b1,b2,caln
      double precision dcen,dlo,dhi,ddum,diff,deln,delx
      double precision tauc,vlo,vhi,vdum
      double precision wval,wdiff,wfik
      double precision ym,yml,yp,yxl,yx
*
*     wavelength coefficients
      integer nwco
      double precision ac(maxwco)
      common/vpc_wavl/ac,nwco
*     atomic data
      integer nz
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :     fik(maxats),asm(maxats),nz
*     upper limit allowed for guessed b value
      double precision bmaxest
      common/vp_bmaxest/bmaxest
*
*     find oscillator strength for the line
      wdiff=1.0d25
      wfik=0.4162d0
      jlin=0
      do while(jlin.lt.nz)
        jlin=jlin+1
        diff=abs(rstw-alm(jlin))
        if(atom.eq.lbz(jlin).and.ion.eq.lzz(jlin).and.
     :       diff.lt.wdiff) then
          wdiff=diff
          wfik=fik(jlin)
        end if
      end do
*     channel corresponding to cursor wavelength
      if(ac(2).ge.0.0d0) then
        ddum=(wl-ac(1))/ac(2)
       else
        ddum=1.0d3
      end if
      call chanwav(wl,ddum,1.0d-3,100)
      l1 = nint( ddum )
      if(l1.le.0) goto 991
*     starting at l1, search out to half intensity or
*     local maximum
      caln=ca(l1)
      if(caln.le.0.0d0) caln=1.0d0
      tauc=da(l1)/caln
      yp=1.0d0-tauc
*     search in both directions, and take narrowest
      lgo=.true.
      ln=l1
      yml=yp
*     search down for half power or local maximum
      do while (lgo)
        ln=ln-1
        if(ln.ge.1) then
          caln=ca(ln)
          if(caln.le.0.0d0) caln=1.0d0
          ym=1.0d0-da(ln)/caln
          deln=abs(de(ln))
          if((ym-yml.gt.de(ln)/caln.or.ym.le.0.0d0).and.
     :         de(ln).gt.0.0d0.and.da(ln).gt.1.5d0*sqrt(deln)) then
            ln=ln+1
            ym=yml
            lgo=.false.
           else
            if(ym.lt.0.5d0*yp) then
              lgo=.false.
            end if
          end if
         else
*         reached endpoint
          ln=1
          caln=ca(ln)
          if(caln.le.0.0d0) caln=1.0d0
          ym=1.0d0-da(ln)/caln
          lgo=.false.
        end if
      end do
*
*     search up for half power or local maximum
      lgo=.true.
      yxl=yp
      lx=l1
      do while (lgo)
        lx=lx+1
        if(lx.le.n) then
          caln=ca(lx)
          if(caln.le.0.0) caln=1.0
          yx=1.0-da(lx)/caln
          delx=abs(de(lx))
*         reached end, except in bottoms of saturated lines?
          if((yx-yxl.gt.de(lx)/caln.or.yx.le.0.0).and.
     :         de(lx).gt.0.0.and.da(lx).gt.1.5*sqrt(delx)) then
            lx=lx-1
            yx=yxl
            lgo=.false.
           else
            if(yx.lt.0.5*yp) then
              lgo=.false.
            end if
          end if
         else
*         reached endpoint
          lx=n
          caln=ca(lx)
          if(caln.le.0.0) caln=1.0
          yx=1.0-da(lx)/caln
          lgo=.false.
        end if
      end do
*
*     NOTE: as implemented, if you stick the cursor in the wing of a line
*     the b estimate is the minimum. This should be fixed sometime.
*
*     now have min, max in profile channels and y-values
*     convert to wavelengths and velocities
      dcen=wval(dble(l1))
      dlo=wval(dble(ln))
      dhi=wval(dble(lx))
      ddum=(dlo/dcen)**2
      vlo=-2.99792458d5*(ddum-1.0d0)/(ddum+1.0d0)
      ddum=(dhi/dcen)**2
      vhi=2.99792458d5*(ddum-1.0d0)/(ddum+1.0d0)
*     and one pixel shift is
      ddum=(wval(dble(l1+1))/dcen)**2
      vdum=2.99792458d5*(ddum-1.0d0)/(ddum+1.0d0)
      if(vlo.lt.vdum.and.vhi.lt.vdum) then
*       stick in something arbitrary
        bg=1.5d0*vdum
       else
*       have a chance of a sensible number, estimate b
        if(yp.gt.ym.and.ym.gt.0.0d0) then
          b1=vlo*sqrt(log(yp/ym))
          if(b1.lt.vdum) b1=1.0d25
         else
          b1=1.0d25
        end if
        if(yp.gt.yx.and.yx.gt.0.0d0) then
          b2=vhi*sqrt(log(yp/yx))
          if(b2.lt.vdum) b2=1.0d25
         else
          b2=1.0d25
        end if
        if(b1.gt.1.0d20.and.b2.gt.1.0d20) then
          bg=1.5d0*vdum
         else
*         choose the lesser of the two (in the absence of a better idea)
          bg=abs(b1)
          if(bg.gt.abs(b2)) bg=abs(b2)
        end if
*       last refuge of the incompetent, choose min b = 4 for metals, 20
*       for hydrogen:
*       replace by upper limit if there is a sensible one
        if(bg.gt.bmaxest.and.bmaxest.gt.5.0d0) bg=bmaxest
*
        if(atom.eq.'H ') then
          if(bg.lt.20.0d0) bg=20.0d0
         else
          if(bg.lt.4.0d0) bg=4.0d0
        end if
      end if
*     now estimate column density from central optical depth
      if(tauc.lt.1.0d0.and.tauc.gt.0.0d0) then
        tauc=-log(tauc)
       else
        if(tauc.lt.0.0d0) then
*         signal below zero, so set it to 7.5
          tauc=7.5d0
         else
*         ultra weak line
          tauc=0.005d0
        end if
      end if
      colg=11.745d0+log10(bg*tauc/wfik)  
 992  return
*     mistake in line position? No sensible values, so set skip marker
 991  colg=-1.0d25
      bg=-1.0d25
      goto 992
      end
