
      program vpgti
*     main program file - calls vpfit, and otherwise just
*     handles system things
*     see source file vpfit.f for copyright conditions

      implicit none

*     execute ./vpgti.done only if enotify = .true.
      logical enotify
      common/vpc_enotify/enotify
*
*     general control variables
      character*4 chcv(10)
      common/vpc_chcv/chcv
*     chcv(3) = 'citn' keep current iteration in junk.dat,
*     and remove the file on satisfactory exit.
*
*
*     external handler

      call vpfit

*     get rid of ieee error report
*     call ieee_flags( 'clearall', junk1, junk2 )

      if(enotify) then
*       execute whatever is in vpgti.done (eg. mail ajc1 < mail.txt)
        call system( './vpgti.done' )
      end if

      if(chcv(3).eq.'citn') then
*       remove junk.dat
        call system( ' /usr/bin/rm junk.dat' )
      end if

      end
