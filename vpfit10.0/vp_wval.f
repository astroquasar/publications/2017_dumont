      double precision function vp_wval(tin,l)
*     convert channel no (double precision) to wavelength using  
*     coefficients wcfd(n,m), using the lth coefficient set
      include 'vp_sizes.f'
      double precision t,tin,tx
      double precision d,dd,sv,y,y2
*     wavelength parameter sets
      character*8 wcfty2(maxnch)
      character*4 vacin2(maxnch)
      double precision helcf2(maxnch)
      integer nwcf2(maxnch),noffs2(maxnch)
      common/vpc_wcpars2/wcfty2,helcf2,nwcf2,noffs2,vacin2
*     general wavelength coefficients
      double precision wcfd(maxwco,maxnch)
      common/vpc_wcfd/wcfd
*
      n=nwcf2(l)
      t=tin
*     subtract IRAF offset
      t=t-dble(noffs2(l))
*
      if(wcfty2(l)(1:3).eq.'log') then
*       IRAF logarithmic bins
        vp_wval=10.0d0**(wcfd(1,l)+wcfd(2,l)*t)
       else
        if(n.gt.20) then
*	  wavelength array instead of polynomial
          i=int(t)
*	  linearly interpolate for non-integer t
          if(i.gt.n) then
            i=i-1
           else
            if(i.le.0) then
              i=i+1
            end if
          end if
          tx=t-dfloat(i)
          vp_wval=wcfd(i,l)*(1.0d0-tx) + wcfd(i+1,l)*tx
         else
          if(wcfty2(l)(1:4).eq.'cheb') then
*	    Chebyschev polynomial evaluation
*	    ac(n) contains: (1) lower limit of range
*                           (2) upper limit
*			    (3) - (n) the Cheb. coeffts
*	    so polynomial has order (in the IRAF sense) n-2
            d=0.0d0
            dd=0.0d0
            y=(2.0d0*t-wcfd(1,l)-wcfd(2,l))/(wcfd(2,l)-wcfd(1,l))
            y2=2.0d0*y
            do i=n,4,-1
              sv=d
              d=y2*d-dd+wcfd(i,l)
              dd=sv
            end do
*	    half coefft last step as usual
            vp_wval=y*d-dd+0.5d0*wcfd(3,l)
           else
*	    assume a polynomial fit of order n-1
            vp_wval = wcfd(n,l)
            do i = n-1, 1, -1
              vp_wval = vp_wval * t + wcfd(i,l)
            end do
          end if
        end if
      end if
*
*     heliocentric correct
      vp_wval=vp_wval*helcf2(l)
*
*     correct air to vacuum if necessary
      if(vacin2(l)(1:3).eq.'air') then
*       convert to vacuum wavelength
        tx=(1.0d4/vp_wval)**2
*       Morton version of Edlen formula replaced by Edlen 1996 formula
        vp_wval=vp_wval*(1.0d0+1.0d-8*(8342.13d0+
     :        2406030.d0/(130.d0-tx)+15997.d0/(38.9d0-tx)))
*           instead of (1.0d0 + 6.4328d-5 +
*    :        2.94981d-2/(146.0d0-tx) + 2.554d-4/(41.0d0-tx))
      end if
*     and apply any velocity correction via the '>>' parameter
*     if(lassoc(l).gt.0) then
*       lvl=3*lassoc(l)
*       vp_wval=vp_wval*(1.0d0+dble(parm(lvl)/3.0e5))
*     end if
      return
      end
