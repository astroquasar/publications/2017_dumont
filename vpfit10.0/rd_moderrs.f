      subroutine rd_moderrs(da,de,drms,ca,nct)
*
*     Replace error and rms by modified version
*     which has higher value in bottoms of absorption lines.
*     Fairly arbitrary function, but seems to correct UVES
*     data problem adequately with the default parameters
*     coded in here.
*
      implicit none
      include 'vp_sizes.f'
      integer nct
      double precision da(nct),de(nct),drms(nct),ca(nct)
*     local:
      integer i
      double precision sval,tval,aval,bval,cval,dval
      double precision temp,temp2
*     Functions
      double precision wval
*     common
*     input streams
      integer inputc
      common/rdc_inputc/inputc
*     workspace for character unscrambling:
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
      double precision dvstr(24)
      common/vpc_dsepspace/dvstr
*
      sval=4.0d0
      tval=4.0d0
      aval=1.0d0
      bval=15.0d0
      cval=0.0d0
      dval=0.0d0
      write(6,*) 'Modify error & expected fluctuations:'
      write(6,*) 'multiply by f=[a+B*(1.0-data/con)**s]**(1.0/t)'
      write(6,*) 'where B=b+c*log10(lambda)+d*log10(lambda)**2'
      write(6,*) 'for wavelength lambda'
      write(6,*) 'If (1.0-data/con)<0, set to zero; if >1 set to 1'
      write(6,*) 'Enter s,t,a,b,c,d (need not be integers)'
      write(6,'(a,2f4.1,4f6.2)') 'Defaults: ',
     :            sval,tval,aval,bval,cval,dval
      write(6,*) 'to do nothing, enter q or Q'
      read(inputc,'(a)',end=999) inchstr
      call dsepvar(inchstr,6,dvstr,ivstr,cvstr,nvstr)
      if(cvstr(1)(1:1).eq.'q'.or.cvstr(1)(1:1).eq.'Q') goto 999
      if(cvstr(1)(1:1).ne.' ') sval=dvstr(1)
      if(cvstr(2)(1:1).ne.' ') tval=dvstr(2)
      if(cvstr(3)(1:1).ne.' ') aval=dvstr(3)
      if(cvstr(4)(1:1).ne.' ') bval=dvstr(4)
      if(cvstr(5)(1:1).ne.' ') cval=dvstr(5)
      if(cvstr(6)(1:1).ne.' ') dval=dvstr(6)
*     de, drms held as squares, hence the 2.0d0
      tval=2.0d0/tval
      do i=1,nct
        if(ca(i).gt.0.0d0.and.drms(i).gt.0.0d0.and.
     :     de(i).gt.0.0d0) then
          temp=1.0d0-da(i)/ca(i)
          temp=min(1.0d0,temp)
          temp2=log10(wval(dble(i)))
          if(temp.gt.0.0d0) then
            temp=aval+(bval+cval*temp2+dval*temp2**2)*temp**sval
           else
            temp=aval
          end if
          temp=temp**tval
          de(i)=de(i)*temp
          drms(i)=drms(i)*temp
        end if
      end do
*          
 998  return
 999  write(6,*) 'Error arrays not modified'
      goto 998
      end
