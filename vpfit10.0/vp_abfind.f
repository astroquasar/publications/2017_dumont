      subroutine vp_abfind(da,de,ca,nl,nchunk,klo,khi,
     :              ion,level,parm,nn,nlines)
*
*     find the absorption lines in a dataset which lie between
*     klo and khi, and suggest column densities and Doppler
*     parameters for them.
*     INPUT:
*     da	object-sky array
*     de      sigma**2 array
*     ca	continuum array
*     nl	array size
*     nchunk	spectral chunk (for wavelength coefficients)
*     klo	low channel for search
*     khi	high channel for search
*     bsep	b-value to set line separation minimum
*     nn	number of parameters (so nions=nn/noppsys; noppsys common)
*
      implicit none
      include 'vp_sizes.f'
*     subroutine parameters:
      integer nl,nchunk,klo,khi,nn,nlines
      double precision da(*),ca(*),de(*)
      double precision parm(maxnpa)
      character*2 ion(*)
      character*4 level(*)
*
*     LOCAL variables
      integer ltemp,nres,minsep
      integer i,j,jh,k,kj,kg,kcai,nhi,ncen
      double precision ce,cv,chi2,chin,ewh,fwhmh,fhp
      double precision flo,fhi
      double precision dwv,sigres,sg,stemp,temp,temp2
      double precision vsep,vsepmin
      double precision wav,wlh,wvlo,wvhi,wvmeanq,wid
      double precision xc,xdd
      double precision fwhmq,ewq
*
*     FUNCTIONS:
      double precision vp_wval,vpf_dvresn
*
*     common stuff:
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     wavelength base shifts
      double precision dnshft2(maxnch)
      common/vpc_shft2/dnshft2
*     region flags
      integer linchk(maxnio)
      common/vpc_linchk/linchk
*     vp_abfind parameters
      double precision bsep,sigl,siglxd,sigpeak,bminshi
      integer minsm
      common/vpc_abfpm/bsep,sigl,siglxd,sigpeak,bminshi,minsm
      integer indush
      common/vpch_uscont/indush
*     common flags for line adding routines:
      integer nadflag
      common/vpc_autofl/nadflag(10)
*     printout variables, used between here and vp_split (below)
*     
      integer kl
      double precision wl,whs,ew,py,x,swid,fwhm,p,px
      common/vpc_absprnt/wl,whs,ew,py,x,swid,fwhm,
     :                  p,px,kl
*     pgplot flags:
      integer ipgopen,ipgflag
      common/pgplotv/ipgopen,ipgflag
*     pgplot attributes: data, error, continuum, axes, ticks, residual
*     1 color indx; 2 line style; 3 line width; 
*     4 curve(0), hist(1); 5 - 10 reserved.
      integer ipgatt(10,9)
      common/vpc_pgattrib/ipgatt
*     plot maxima and minima (from splot)
      real spxmpg,spxhpg,spympg,spyhpg
      common/vpc_splcur/spxmpg,spxhpg,spympg,spyhpg
*     printout variable
      logical verbose
      common/vp_sysout/verbose
*
*     Internal variables (as printed) for ablin were:
*     kl	line number
*     wl	mean wavelength
*     wvc	mean vacuum wavelength
*     wer	wavelength error
*     wh	peak wavelength
*     ew	equivalent width
*     py	equivalent width error estimate
*     flx	line flux
*     x	        A/channel
*     swid	FWZI
*     fwhm	FWHM
*     p	sigma - log probability each channel
*     px	overall probability
*     jmn	minimum channel for line
*     jmx	high channel for line
*     skew	measure of skew
*     chskew	descriptor
*     OTHERS:
*     ncen	channel for minimum flux
*
*     Max ew for this region, hugely negative for a start
      ewh=-1.0d30
      fwhmh=0.0d0
*     check that arguments in right order
      if(khi.lt.klo) then
        ltemp=khi
        khi=klo
        klo=ltemp
      end if
*     make sure this is worth it!
*     if no sigma, or ignore flag set, drop the point
      do while(de(khi).le.0.0d0.and.khi.gt.klo) 
        khi=khi-1
      end do
*     branch out if there is nothing left
      if(khi.eq.klo.or.indush.eq.0) goto 991
*     wavelengths:
      wvlo=vp_wval(dble(klo)+dnshft2(nchunk),nchunk)
      wvhi=vp_wval(dble(khi)+dnshft2(nchunk),nchunk)
*     to an adequate approximation the velocity increment per
*     channel, dwv, is (km/s)
      wav=0.5d0*(wvlo+wvhi)
      dwv=2.99792458d5*(wvhi-wvlo)/(wav*dble(khi-klo))
*     and require that line separation is at least 2.35*sigma
*     for the chunk
      sigres=vpf_dvresn(wav,nchunk)*2.99792458d5
      vsepmin=2.35482d0*sigres
      vsep=1.66666667d0*bsep
*     smoothing(channels)
      nres=max0(int(vsep/dwv+0.5d0),minsm)
      minsep=max0(int(vsepmin/dwv+0.5d0),int(vsep/dwv+0.5d0))
      if(nres.gt.250) nres=minsep
*     print a warning if minsep is silly:
      if(minsep.le.1) then
        write(6,*) ' WARNING:  FWHM:',vsep,' and A/channel:',dwv
        write(6,*) '             are you sure this is OK?'
        write(6,*)
        minsep=1
      end if
*     sigl = significance limit for whole line
*     siglxd = sign. for each component
*     sigpeak= sign for peak in component relative zero
*     defaults:
      if(sigl.le.0.0d0) sigl=5.0d0
      if(siglxd.le.0.0d0.or.siglxd.ge.sigl) siglxd=sigl
      if(sigpeak.le.0.0d0.or.sigpeak.ge.siglxd) sigpeak=siglxd
*
      if(verbose) then
        write(6,*) 'chunk',nchunk,' shift',dnshft2(nchunk)
        write(6,*) 'vsepmin:',vsepmin,' vsep:',vsep,
     :       ' gives separation of',minsep,' channels'
*       channel range fed in via subroutine arguments. 
        write(6,'('' Range (channels):  '',i5,'' - '',
     :        i5)') klo,khi
*	
        write(6,'('' wavelength limits:'',f10.2,
     :           '' to'',f10.2)') wvlo,wvhi
      end if
*
*     seek lines on scale of given resolution or greater
      nhi=khi
      j=klo
      kl=0
      do while (j.le.nhi)
*       absorption line   sg=1.0
*       emission line     sg=-1.0
        if(ca(j).ge.da(j)) then
          sg=1.0d0
         else
          sg=-1.0d0
        end if
        if(ca(j).lt.da(j)) sg=-1.0d0
        if(ca(j).le.0.0d0) then
          j=j+1
         else
*         possible line found
*1        continue
          ew=0.0d0
          wl=0.0d0
          xc=(ca(j)-da(j))*sg
          x=(1.0d0-da(j)/ca(j))*sg
          xdd=dble(x)
          if(de(j).gt.0.0d0) then
            ce=de(j)
            p=abs((ca(j)-da(j))*sg/sqrt(de(j)))
            chi2=p*p
            chin=1.0d0
           else
            ce=0.0d0
            p=0.0d0
            chi2=0.0d0
            chin=0.0d0
          end if
          ew=ew+x
          whs=xdd
          wl=wl+vp_wval(dble(j)+dnshft2(nchunk),nchunk)*xdd
          ncen=j
*	  cv is just a summed continuum to make sure there is
*         something to absorb against.
          cv=ca(j)
*	  find the end of the line by checking when continuum 
*	  level is next reached (or end of window)
          jh=j+1
 6        if((da(jh)-ca(jh))*sg.gt.0.0d0) goto 4
          jh=jh+1
*	  modify branch from 5 to 7, else end never used
          if(jh.ge.khi) goto 7
          goto 6
*	  check this is not just a single high (or low) point
*	  it is assumed that instrument profile is at least 2px wide
*	  jh-1 is value for continued search
 4        px=0.0d0
          if(minsep.le.1) goto 7
*	  see if it changes sign again next pixel up, so two pixels
*	  together above the continuum end an absorption line
          if(sg*(da(jh+1)-ca(jh+1)).lt.0.0d0) then
            jh=jh+1
            goto 6
          end if
*         jh -1 is now the end of the line....
 7        jh=jh-1
          k=j+1
          if(k.gt.jh) goto 5
          kcai=0
          do i=k,jh
            cv=cv+ca(i)
            if(ca(i).le.0.0d0) then
*             bad continuum flag
              kcai=1
              x=1.0d0
              xdd=1.0d0
             else
              x=sg*(1.0d0-da(i)/ca(i))
              xdd=dble(x)
            end if
            ew=ew+x
            if(de(i).gt.0.0d0) then
              ce=ce+de(i)
              xc=xc+sg*(ca(i)-da(i))
              stemp=(ca(i)-da(i))*sg/sqrt(de(i))
              p=p+abs(stemp)
              chi2=chi2+stemp*stemp
              chin=chin+1.0d0
            end if
            wl=wl+vp_wval(dble(i)+dnshft2(nchunk),nchunk)*xdd
            if(xdd.ge.whs) then
              ncen=i
              whs=xdd
            end if
          end do
          if(kcai.ne.0.or.cv.eq.0.0d0.or.ew.eq.0.0d0) goto 3
*	  all line parameters now determinable
*	  multiple lines are treated as single features for the present
 5        continue
*	  use chi^2 criterion, with the approximation that
*	  sqrt(2*chi2)-sqrt(2*chin-1) has zero mean and unit sigma.
          if(chin.gt.0.5d0.and.chi2.ge.0.0d0) then
*	    don't take abs() since not interested in the cases where
*	    the agreement is too good to be believable.
            px=sqrt(2.0d0*chi2)-sqrt(2.0d0*chin-1.0d0)
           else
            px=0.0d0
          end if
*	  moved down so that parameters for max EW line can be computed
*	  for use in those cases where none was found 
*	  if(px.lt.sigl) goto 3
          kl=kl+1
          xdd=vp_wval(dble(jh+1)+dnshft2(nchunk),nchunk) - 
     1        vp_wval(dble(j)+dnshft2(nchunk),nchunk)
          x=dble(xdd)
          py=py*x/cv
          x=x/dble(jh-j+1)
          xdd=xdd/dble(jh-j+1)
*         mean wavelength
          wl=wl/ew
*         find FWHM by working in from the edges
          fhp=whs*0.5d0
          i=j
 601      i=i+1
          if(i.ge.ncen) goto 602
          if(sg*(1.0d0-da(i)/ca(i)).lt.fhp) goto 601
*         linear interpolation for fraction of channel
 602      stemp=sg*(1.0d0-da(i)/ca(i))
          flo=dble(i)-(stemp-fhp)/(stemp-sg*(1.0d0-da(i-1)/ca(i-1)))
*	  now longer wavelength half power point
          i=jh
 603      i=i-1
          if(i.le.ncen) goto 604
          if(sg*(1.0d0-da(i)/ca(i)).lt.fhp) goto 603
 604      stemp=sg*(1.0d0-da(i)/ca(i))
          fhi=dble(i)+
     1          (stemp-fhp)/(stemp-sg*(1.0d0-da(i+1)/ca(i+1)))
*	  fwhm from wavelengths
          fwhm=vp_wval(dble(fhi)+dnshft2(nchunk),nchunk) -
     :           vp_wval(dble(flo)+dnshft2(nchunk),nchunk)
*	
          ew=ew*x
          whs=vp_wval(dble(ncen)+dnshft2(nchunk),nchunk)
          wid=x*dble(jh-j+1)
          swid=x
          if(swid.lt.wid) swid=wid
*	  line limits (i.e. continuum channels at edges) as printed
*         jmn=j-1   ! now in split
*         jmx=jh+1  ! now in split
*         set local maxima
          if(ew.gt.ewh.and.sg.gt.0.0d0) then
            ewh=ew
            wlh=wl
            fwhmh=fwhm
          end if
          if(px.lt.sigl) goto 3
*	  check for component structure if absorption
          if(sg.gt.0.0d0) then
            if(verbose) then
              write(6,*) 'Separating what was...'
              write(6,*) whs,wl,ew,nchunk
            end if
            call vp_split(da,de,ca,nl,j,jh,nres,minsep,siglxd,
     1           sigpeak,sigres,ion,level,nn,parm,nchunk)
          end if
 3        continue
          j=jh+2
        end if
      end do
 991  continue
      nlines=nn/noppsys
      if(nlines.eq.0) then
*       could not find anything, so put in weakest broad thing found
        if(ewh.lt.0.0d0) then
*	  reduced to arbitrary something in the middle, since no
*	  excursions below the continuum anywhere!
          wlh=(wvlo+wvhi)*0.5d0
          fwhmh=5.6d-6*bminshi*wlh
          ewh=0.03d0
          write(6,*) 'No absorption, arbitrary Ly-a at ',wlh
         else
          wvmeanq=wlh
          write(6,*) 'Forced Ly-a at ',wlh
        end if
        temp=5.6d-6*bminshi*wlh
        if(fwhmh.gt.temp) then
          fwhmq=fwhmh
          ewq=ewh
         else
*         adjust to min Doppler parameter
          temp2=fwhmh/temp
          if(temp2.lt.0.2d0) temp2=0.2d0
          ewq=ewh*temp/temp2
          fwhmq=temp
        end if
        call vp_abest(fwhmq,wvmeanq,ewq,sigres,ion,level,parm,nn)
        nlines=1
        write(6,*) 'One weak line inserted'
       else
        write(6,*) nlines,' features found'
        do kg=1,nlines
          kj=(kg-1)*3
          write(6,*) ion(kg),level(kg),parm(kj+1),parm(kj+2),
     :             parm(kj+3)
        end do
      end if
*
*     if plot channel open, display the tick marks
      if(ipgopen.gt.0.and.nlines.gt.0) then
        call vp_dotick(spxmpg,spxhpg,spympg,spyhpg,
     :          (ipgatt(1,5).gt.0),ion,level,parm,nlines)
      end if
      return
      end
      subroutine vp_split(da,de,ca,nl,jlo,jhi,nres,minsep,siglxd,
     1              sigpeak,sigres,ion,level,nn,parm,nchunk)
*
      implicit none
      include 'vp_sizes.f'
*
      integer nl,jlo,jhi,nres,minsep,nn,nchunk
      character*2 ion(*)
      character*4 level(*)
      double precision parm(*)
      double precision da(nl),de(nl),ca(nl)
      double precision siglxd,sigpeak,sigres
*     
      double precision yv(2000)
      integer ix(2000),im(2000),indx(2000)
*     
      integer i,iij,iprc,iw,j,ja,jb,j1,j2
      integer jbx,jql,jqh,jm,jmn,jmx,jt,jtl,jtu,jx,jz
      integer jvar,l,lv
      double precision ce,cv,xc,vn,ew
      double precision dx,px,py,sc,sca,sda,temp1,temp2,tw,tpx
      double precision p,rcen,resid,rsfac,wl,wvcq,werq
      double precision whlo,whhi,widh,x,xp
*     Functions
      double precision vp_wval,vp_dav
*     
*     called by vp_abfind only
*     suggests (and that's all) components for a complex absorption line
*     input:
*     da - signal, de - sigma, ca - continuum (as vp_abfind spec)
*     nres  is resolution in channels for finding components
*     jlo,jhi are lower,upper limits from vp_abfind
*     siglxd is log sign for inclusion (0=all)
*	
*	
*     common stuff:
*     wavelength base shifts
      double precision dnshft2(maxnch)
      common/vpc_shft2/dnshft2
*     region flags
      integer linchk( maxnio )
      common/vpc_linchk/linchk
*     printout variables
      integer klq
      double precision wlq,whq
      double precision ewq,pyq,xq,swidq,fwhmq,pq,pxq
      common/vpc_absprnt/wlq,whq,ewq,pyq,
     1     xq,swidq,fwhmq,pq,pxq,klq
*
*     temporary assignment
      rcen=0.0d0
*     
      jmn=jlo-1
      jmx=jhi+1
      if(jmx-jmn.lt.2*minsep) then
*       put in the single line only
*       write(6,100) klq,wlq,whq,ewq,pyq,
*     1       xq,swidq,fwhmq,pq,pxq,jmn,jmx
 100    format(1x,i5,f9.2,f9.2,2f8.3,0pf8.3,
     1       2f8.3,f7.1,f6.2,2i6,f7.2)
*       translate these quantities to estimated parameters:
        call vp_abest(fwhmq,wlq,ewq,sigres,ion,level,parm,nn)
       else
*       nbl=(jmx-jmn)/minsep
*       write(6,*) 'Blended: up to ',nbl,' components',nchunk
*       set wavelength increment
        dx=(vp_wval(dble(jhi+1)+dnshft2(nchunk),nchunk) - 
     1    vp_wval(dble(jlo)+dnshft2(nchunk),nchunk))
     2      /dble(jhi-jlo)
*       line is resolved --  look for local maxima and minima
        l=nres/2
*       goes down first
        sc=1.0d10
        jm=0
        jx=0
        i=jlo
 3      x=vp_dav(da,nl,i,l)
*       if local average smaller than previous value, use it
        if(x.gt.sc) goto 1
        sc=x
        i=i+1
*       check have not reached the end of the line
        if(i.gt.jhi) goto 2
*       branch back for search
        goto 3
*       average increasing again, so i-1 is a minimum
 1      jm=jm+1
        im(jm)=i-1
        sc=vp_dav(da,nl,im(jm),l)
        i=i+1
*       find the maximum locally
 5      x=vp_dav(da,nl,i,l)
        if(x.lt.sc) goto 4
        sc=x
        i=i+1
*       check for end of line
        if(i.gt.jhi) goto 2
        goto 5
*       local maximum found
 4      jx=jx+1
        ix(jx)=i-1
        sc=vp_dav(da,nl,ix(jx),l)
        i=i+1
        goto 3
*       max/min in line all known
 2      continue
        if(jx.lt.1) then
*         single line only to store
*         write(6,100) klq,wlq,whq,ewq,pyq,
*     1       xq,swidq,fwhmq,pq,pxq,jmn,jmx
          call vp_abest(fwhmq,wlq,ewq,sigres,ion,level,parm,nn)
*         so branch to return
          goto 7
         else
*	  sort out the max and minima
          if(jx.lt.jm) then
            jx=jx+1
            ix(jx)=jhi+1
          end if
        end if
*       check that the line level is not close to zero, and if it
*       is throw away components not significantly above zero
        jt=1
        do while (jt.lt.jm)
          x=vp_dav(da,nl,im(jt),l)
          xp=vp_dav(da,nl,im(jt+1),l)
          if(de(im(jt)).ge.0.0d0) then
            temp1=sqrt(de(im(jt)))
           else
            temp1=-1.0d34
          end if
          if(de(im(jt+1)).ge.0.0d0) then
            temp2=sqrt(de(im(jt+1)))
           else
            temp2=-1.0d34
          end if
          if(x.lt.temp1.and.xp.lt.temp2) then
*	    deep line -- two components down to sigma level
            tw=0.0d0
            tpx=0.0d0
*	    go to next min, and see if residual significantly above 0
            do i=im(jt),im(jt+1)
              if(de(i).gt.0.0d0) then
                tw=tw+da(i)
                tpx=tpx+de(i)
              end if
            end do
            if(tpx.gt.0.0d0) then
              tpx=sqrt(tpx)
              tpx=abs(tw/tpx)
            end if
            if(tpx.lt.sigpeak) then
*	      component not significantly above zero - remove it
               jm=jm-1
               jx=jx-1
               do i=jt,jx
                 ix(i)=ix(i+1)
               end do
               xp=vp_dav(da,nl,im(jt+1),l)
               if(x.lt.xp) im(jt+1)=im(jt)
               do i=jt,jm
                 im(i)=im(i+1)
               end do
              else
               jt=jt+1
             end if
            else
             jt=jt+1
          end if
        end do
*       now try to remove any components closer together than the 
*       smoothing length NRES, by substituting the lowest local value
*       working out from minimum values. To do this requires JM>1, else
*       it is trivial
        if(jm.gt.1) then
*
*	  store values at local minima
          do i=1,jm
            yv(i)=vp_dav(da,nl,im(i),l)
          end do
*         set up index table 
          call indexx(jm,yv,indx)
*	  flag closest values as necessary, from the minimum up
          jt=1
          do while (jt.lt.jm)
            jz=indx(jt)
            if(im(jz).gt.0) then
*             down from local minimum
              jvar=jz-1
              do while (jvar.gt.0)
                if(im(jz)-im(jvar).lt.minsep) then
*                 remove entry from table by setting IM negative
                  im(jvar)=-2000
                  jvar=jvar-1
                 else
                  jvar=0
                end if
              end do
*	      up from local minimum
              jvar=jz+1
              do while (jvar.le.jm)
                if(im(jvar)-im(jz).lt.minsep) then
*	          remove entry flag is -2000
                  im(jvar)=-2000
*	          reset max to reflect extended component width
                  ix(jz)=ix(jvar)
                  jvar=jvar+1
                 else
                  jvar=jm+1
                end if
              end do
            end if
            jt=jt+1
          end do
*	  remove the flagged values
          jt=1
          do while (jt.lt.jm)
            if(im(jt).lt.0) then
              jm=jm-1
              jx=jx-1
              do i=jt,jx
                ix(i)=ix(i+1)
              end do
              do i=jt,jm
                im(i)=im(i+1)
              end do
             else
              jt=jt+1
            end if
          end do
*	  check the last point, and drop if necessary
          if(im(jm).lt.0) then
            jm=jm-1
            jx=jx-1
          end if
*         and make sure that sequence is min, max, min, max, ...
          do i=1,jm-1
            if(im(i).gt.ix(i).or.im(i+1).lt.ix(i)) then
              write(6,100) klq,wlq,whq,ewq,pyq,
     1           xq,swidq,fwhmq,pq,pxq,jmn,jmx
              write(6,1090)jx,jm
 1090         format('   *** line decomposition failed *** ',2i5)
              write(6,'('' min:'',8i8)') (im(iw),iw=1,jm)
              write(6,'('' max:'',8i8)') (ix(iw),iw=1,jx)
              goto 7
            end if
          end do
        end if
*
*	iprc is # printed components
        iprc=0
* 	first line to first max etc. For lack of anything better
*	to do, divide the max points equally.
        jb=jlo
        do 20 j=1,jm
          ja=jb
          if(j.ge.jm) goto 8
          jb=ix(j)
          goto 9
 8        jb=jhi
          if(ca(ja).le.0.0d0) goto 270
 9        x=(1.0d0-da(ja)/ca(ja))*0.5d0
          if(de(ja).gt.0.0d0) then
            p=abs((ca(ja)-da(ja))*0.5d0/sqrt(de(ja)))
            ce=0.5d0*de(ja)
           else
            p=0.0d0
            ce=0.0d0
          end if
          xc=(ca(ja)-da(ja))*0.5d0
          vn=0.5d0
          cv=0.5d0*ca(ja)
          ew=x
          wl=vp_wval(dble(ja)+dnshft2(nchunk),nchunk)*x
          j1=ja+1
          j2=jb-1
          if(j2.ge.j1) then
            do lv=j1,j2
              if(ca(lv).le.0.0d0) goto 270
              x=1.0d0-da(lv)/ca(lv)
              if(de(lv).gt.0.0d0) then
                p=p+abs((ca(lv)-da(lv))/sqrt(de(lv)))
                ce=ce+de(lv)
              end if
              cv=cv+ca(lv)
              vn=vn+1.0d0
              xc=xc+ca(lv)-da(lv)
              ew=ew+x
              wl=wl+x*vp_wval(dble(lv)+dnshft2(nchunk),nchunk)
            end do
          end if
          if(ca(jb).le.0.0d0)  goto 270
          x=(1.0d0-da(jb)/ca(jb))*0.5d0
          if(de(jb).gt.0.0d0) then
            p=p+abs((ca(jb)-da(jb))*0.5d0/sqrt(de(jb)))
            ce=ce+0.5d0*de(jb)
          end if
          xc=xc+0.5d0*(ca(jb)-da(jb))
          cv=cv+0.5d0*ca(jb)
          vn=vn+0.5d0
          ew=ew+x
          wl=wl+vp_wval(dble(jb)+dnshft2(nchunk),nchunk)*x
          if(j.gt.1) goto 11
          if(de(jlo).gt.0.0d0) then
            p=abs((ca(jlo)-da(jlo))*0.5d0/sqrt(de(jlo)))+p
          end if
          if(ca(jlo).le.0.0d0) goto 270
          x=(1.0d0-da(jlo)/ca(jlo))*0.5d0
          if(de(jlo).gt.0.0d0) ce=ce+0.5d0*de(jlo)
          cv=cv+0.5d0*ca(jlo)
          vn=vn+0.5d0
          xc=xc+0.5d0*(ca(jlo)-da(jlo))
          ew=ew+x
          wl=wl+vp_wval(dble(jlo)+dnshft2(nchunk),nchunk)*x
 11       if(jb.ne.jhi) goto 12
          if(de(jhi).gt.0.0d0) then
            p=p+abs((ca(jhi)-da(jhi))*0.5d0/sqrt(de(jhi)))
          end if
          if(ca(jb).le.0.0d0) goto 270
          x=(1.0d0-da(jb)/ca(jb))*0.5d0
          if(de(jhi).gt.0.0d0) ce=ce+0.5d0*de(jhi)
          cv=cv+0.5d0*ca(jhi)
          vn=vn+0.5d0
          xc=xc+0.5d0*(ca(jhi)-da(jhi))
          ew=ew+x
          wl=wl+vp_wval(dble(jb)+dnshft2(nchunk),nchunk)*x
          if(ew.eq.0.0d0) goto 270
 12       wl=wl/ew
*         wh=vp_wval(dble(im(j))+dnshft2(nchunk),nchunk)
          if(ca(im(j)).gt.0.0d0) then
*           residual intensity
            rcen=da(im(j))/ca(im(j))
*	    search out to half intensity points
*	    down to ja
            jtl=im(j)
            rsfac=1.0d0+rcen
            resid=rcen
            do while (jtl.gt.2.and.jtl.gt.ja.and.
     1                        2.0d0*resid.lt.rsfac)
              jtl=jtl-1
              if(ca(jtl).gt.0.0d0) then
                resid=da(jtl)/ca(jtl)
               else
                resid=1.0d0
              end if
            end do
            whlo=dx*dble(im(j)-jtl)
*	    search up to jb
            jtu=im(j)
            resid=rcen
            do while (jtu.lt.nl.and.jtu.lt.jb.and.
     1                2.0d0*resid.lt.rsfac)
              jtu=jtu+1
              if(ca(jtu).gt.0.0d0) then
                resid=da(jtu)/ca(jtu)
               else
                resid=1.0d0
              end if
            end do
            whhi=dx*dble(jtu-im(j))
*	    for bad blends, have edges reached before the half
*	    intensity points, so choose the width depending on
*	    whether or not end is reached:
            if(jtl.eq.ja) then
              widh=2.0d0*whhi
              if(jtu.eq.jb) then
                if(widh.lt.2.0d0*whlo) widh=2.0d0*whlo
              end if
             else
              if(jtu.eq.jb) then
                widh=2.0d0*whlo
               else
                widh=whlo+whhi
              end if
            end if
           else
*	    continuum zero - no idea what to do so be arbitrary
            widh=dble(nres)*dx
          end if
*
          py=sqrt(ce)
          px=abs(xc/py)
          if(cv.eq.0.0d0) goto 270
*         wid=dx*dble(jb-ja+1)
          ew=ew*dx
          if(px.ge.siglxd) then
            if(jb.ge.jhi-1) then
              jbx=jhi+1
             else
              jbx=jb
            end if
*	    determine residual flux fraction at higher split position
            jql=max0(jbx-l,1)
            jqh=min0(jbx+l,nl)
            sda=0.0d0
            sca=0.0d0
            do iij=jql,jqh
              if(ca(iij).gt.0.0d0) then
                sda=sda+da(iij)
                sca=sca+ca(iij)
              end if
            end do
*	    sca= data/continuum fraction at break point
            sca=sda/sca
*	    write(6,101) klq,wl,wh,ew,py,dx,wid,p,
*     1	           px,jam,jbx
*101	    format(2x,'***',i4,f9.2,f9.2,2f8.3,2f8.3,
*     1         8x,f7.1,f6.2,2i6)
            call vp_abest(widh,wl,ew,sigres,ion,level,parm,nn)
            iprc=iprc+1
          end if
          goto 20
 270      call vp_abest(fwhmq,wlq,ewq,sigres,ion,level,parm,nn)
          wvcq=wlq
          werq=0.0d0
          write(6,100) klq,wlq,wvcq,werq,whq,ewq,pyq,
     1       xq,swidq,fwhmq,pq,pxq,jmn,jmx
 20     continue
        if(iprc.le.0) then
          write(6,100) klq,wlq,whq,ewq,pyq,
     1       xq,swidq,fwhmq,pq,pxq,jmn,jmx
          write(6,'(3x,''***   no significant components,'',
     1           i4,'' minima'')') jm
          call vp_abest(fwhmq,wlq,ewq,sigres,ion,level,parm,nn)
        end if
      end if
 7    return
      end
      subroutine indexx(n,arrin,indx)
*     Adapted from Press et al. 'Numerical Recipes' p233.
*     changed and corrected to allow for n=1
      implicit none
      integer n
      double precision arrin(n)
      integer indx(n)
*     Local
      integer i,ir,j,l
      integer indxt
      double precision q
      do j=1,n
        indx(j)=j
      enddo
*
      if(n.le.1) goto 99
*     
      l=n/2+1
      ir=n
 10   continue
      if(l.gt.1)then
        l=l-1
        indxt=indx(l)
        q=arrin(indxt)
       else
        indxt=indx(ir)
        q=arrin(indxt)
        indx(ir)=indx(1)
        ir=ir-1
        if(ir.eq.1)then
          indx(1)=indxt
          goto 99
        endif
      endif
      i=l
      j=l+l
 20   if(j.le.ir)then
        if(j.lt.ir)then
          if(arrin(indx(j)).lt.arrin(indx(j+1)))j=j+1
        endif
        if(q.lt.arrin(indx(j)))then
          indx(i)=indx(j)
          i=j
          j=j+j
         else
          j=ir+1
        endif
        go to 20
      endif
      indx(i)=indxt
      go to 10
 99   return
      end

      double precision function vp_dav(da,nl,i,l)
      integer nl,i,l
      double precision da(nl)
*     Local
      integer j,lo,lh
      lo=i-l
      lh=i+l
      vp_dav=0.0d0
      do j=lo,lh
        vp_dav=vp_dav+da(j)
      end do
      vp_dav=vp_dav/dble(2*l+1)
      return
      end

      subroutine vp_abest(fwhm,wvc,ew,sigres,ion,level,parm,nn)
*     translate the line finder quantities to estimated parameters:
      implicit none
      include 'vp_sizes.f'
*
      double precision fwhm,wvc,ew
      double precision sigres
      character*2 ion(*)
      character*4 level(*)
      double precision parm(maxnpa)
      integer nn
*
*     LOCAL:
      integer i,jjj,kxl,nions
      double precision tempcol,tempv,vwid,tempzed,tempbv
*
*     common stuff:
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     wavelength base shifts
      double precision dnshft2(maxnch)
      common/vpc_shft2/dnshft2
*     tied parameter characters and ill-conditioning flag
      character*2 ipind(maxnpa)
      integer isod,isodh
      common/vpc_usoind/ipind,isod,isodh
*     turbulence, mass, temperature, fixed/tied indicator
      double precision vturb,atmass,temper,fixedbth
      common/vpc_ucopder/vturb(maxnio),atmass(maxnio),
     :     temper(maxnio),fixedbth(maxnio)
*     region flags
      integer linchk( maxnio )
      common/vpc_linchk/linchk
*     Min col density for inclusion as a guess
      double precision colglo
      common/vpc_abguess/colglo
*     log or linear variable indicator
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
*     Current line parameters for guess (default = Ly-a)
      logical lgs
      character*2 ings
      character*4 lvgs
      double precision wvgs,fkgs,algs,amgs
      common/vpc_pargs/wvgs,fkgs,algs,amgs,lgs,ings,lvgs
*
*     FWHM -> b:
      vwid=1.2766d5*fwhm/wvc
*     residual Doppler parameter
      vwid=(vwid*vwid-sigres*sigres)*2.0d0
*     set an arbitrary minimum to this vsquared value
      if(vwid.lt.20.0d0) vwid=20.0d0
      tempbv=sqrt(vwid)
      parm(nn+nppbval)=tempbv
*     redshift
      tempzed=wvc/wvgs-1.0d0
      parm(nn+nppzed)=tempzed
*     log ion column density from equivalent width
*     optically thin approximation
*     parm(nn+1)=log10(ew/wvc)+17.35 becomes
      tempcol=log10(ew/(wvc*wvgs*fkgs))+20.05d0
      tempv=log10(fkgs*wvgs)
      if(tempcol.gt.16.7d0-tempv) then
*       it won't be optically thin, so use optically thick
*       approximation
        if(parm(nn+nppbval).gt.30.0d0) then
           parm(nn+nppbval)=30.0d0
        end if
*	Was for HI  parm(nn+1)=(log10(ew/wvc)+8.9414)/4.668
*	              parm(nn+1)=10.0**parm(nn+1)
        tempcol=55.95d0+11.32d0*log10(ew/wvc)-tempv
     :                -10.0d0*log10(parm(nn+nppbval)/30.0d0)
*	constrain the ranges so the line is visible, and not damped
        if(tempcol+tempv.gt.20.7d0) then
          tempcol=20.7d0-tempv
         else
          if(tempcol+tempv.lt.15.0d0) then
            tempcol=15.0d0-tempv
          end if
        end if
      end if
*     convert to internal variables
      if(indvar.eq.1) then
        parm(nn+nppcol)=tempcol*scalelog
       else
*	new line column density estimated above as log, so convert
        parm(nn+nppcol)=10.0d0**tempcol*scalefac
      end if
*     tied flags
      do i=1,noppsys
        ipind(nn+i)='  '
      end do
*     zero any extra ones above the standard three
      if(noppsys.gt.nppbval) then
        do jjj=nppbval+1,noppsys
          parm(nn+jjj)=0.0d0
        end do
      end if
      if(nn.le.0.or.tempcol.ge.colglo) then
*       special case for first line.. included at first, however weak,
*       and then replaced if an adequately strong one comes along
        if(indvar.eq.1) then
          tempcol=parm(1)/scalelog
         else
          tempcol=log10(parm(1)/scalefac)
        end if
        if(nn.eq.noppsys.and.tempcol.lt.colglo) then
*       copy down the acceptable values
          do jjj=1,noppsys
            parm(jjj)=parm(jjj+noppsys)
          end do
         else
          nn=nn+noppsys
        end if
*       associated parameters:
        nions=nn/noppsys
        ion(nions)=ings
        level(nions)=lvgs
        linchk(nions)=0
        vturb(nions)=0.0d0
        atmass(nions)=amgs
        temper(nions)=0.0d0
*       is fixedbth ever used?
        fixedbth(nions)=0.0d0
      end if
*     add in any cross-referenced new lines
      call vp_addxlink(ings,lvgs,tempzed,tempbv,tempcol,
     :       nions,ion,level,nn,parm,ipind,kxl)
*     write(6,*) ings,lvgs,tempzed,kxl,nions,nn
      return
      end
