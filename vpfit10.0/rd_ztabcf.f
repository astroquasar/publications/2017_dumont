      subroutine rd_ztabcf(da,de,ca,nct,ha,he,hc,nht,ds,chrd)
*
      implicit none
      include 'vp_sizes.f'
*
*     crosscorrelate spectrum with a line list in redshift space
*     the spectrum is assumed to be mormalized to unity
*
      integer nct,nht
      double precision da(nct),de(nct),ca(nct)
      double precision ha(*),he(*),hc(*),ds(*)
      character*(*) chrd
*     
*     LOCAL:
      integer i,ind,j,k,kcen,klo,khi
      integer llow,lhigh,nbin,nlacc,nlins
      double precision derr,dwrst,ctwrst,pin,temp
      double precision snthres,sntr2,wvl,zp1
      double precision bval,col,trstw,zval
*
*     FUNCTIONS:
      double precision wval
*
      character*4 ions
      character*2 atoms
      character*2 indb,indz,indcol
      double precision ddum
*     local linear wavelength coefficients
      double precision bc,ach,achend
*     character string input and separation
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
      double precision dvstr(24)
      common/vpc_dsepspace/dvstr
*     atomic data
      integer nz
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :     fik(maxats),asm(maxats),nz
*     h-stored wavelength coefficients
      integer nwcoh
      double precision wcfdh(maxwco)
      character*8 wcftypeh
      character*4 vacindh
      double precision helcfach
      integer*4 noffsh
      common/rdc_hwavl1/wcfdh,nwcoh
      common/rdc_hwavl2/wcftypeh,helcfach,
     :       noffsh,vacindh
*
*     Redshift range (& increment)
*     work out defaults from wavelengths:
      ach=wval(0.0d0)
      bc=(wval(dble(nct))-ach)/dble(nct)
      bc=2.0d0*bc/(ach+wval(dble(nct)))
      ach=0.1d0
      achend=bc*dble(maxfis-1)+ach
      write(6,'(a,1x,f5.3,a1,f9.6,a1,f5.3,a1)') 
     :  'Redshift start, increment, end? [',ach,',',bc,',',achend,']'
*     read in values:
      read(5,'(a)') inchstr
      call dsepvar(inchstr,3,dvstr,ivstr,cvstr,nvstr)
      if(dvstr(1).ne.0.0d0) then
        ach=dvstr(1)
      end if
      if(dvstr(2).gt.0.0d0) then
        bc=dvstr(2)
      end if
*     work out possible endpoint
      achend=bc*dble(maxfis-1)+ach
      if(dvstr(3).lt.achend.and.dvstr(3).gt.ach) then
*       adopt requested endpoint
        achend=dvstr(3)
      end if
*     number of channels
      nht=int((achend-ach)/bc)
*     write out adopted parameters:
      write(6,'(a,f7.4,a,f9.6,a,f7.4)') 'zstart ',ach,' zincrement ',
     :    bc,' zend ',achend
*     change to coefficient format
      ach=ach-bc
*     copy these to h-array
      wcftypeh='polynomi'
      nwcoh=2
      wcfdh(1)=ach
      wcfdh(2)=bc
      if(maxwco.gt.2) then
        do j=3,maxwco
          wcfdh(j)=0.0d0
        end do
      end if
      helcfach=1.0d0
      noffsh=0
      vacindh='vacu'
*
*     wavelength range of data, to allow for silly end values
      write(6,*) 'Wavelength range in data (from, to)? [all]'
      read(5,'(a)') inchstr
      call dsepvar(inchstr,2,dvstr,ivstr,cvstr,nvstr)
      if(dvstr(1).gt.0.0d0) then
        call chanwav(dvstr(1),ddum,1.0d-2,100)
        llow=int(ddum+0.5d0)
       else
        llow=1
      end if
      if(dvstr(2).gt.0.0d0) then
        call chanwav(dvstr(2),ddum,1.0d-2,100)
        lhigh=int(ddum+0.5d0)
       else
        lhigh=nct
      end if
*  
      write(6,*) 'bin width (px), S/N thres, +thres? [5, 4, 5]'
      read(5,'(a)') inchstr
      call dsepvar(inchstr,3,dvstr,ivstr,cvstr,nvstr)
*     nbin is number of pixels out from central one, so total=2*nbin+1
      nbin=ivstr(1)/2
      if(nbin.lt.0) nbin=2
      if(dvstr(2).gt.0.0d0) then
        snthres=dvstr(2)
       else
        snthres=4.0d0
      end if 
      if(dvstr(3).gt.0.0d0) then
        sntr2=dvstr(3)
       else
        sntr2=5.0d0
      end if 
*     Line list file
 901  write(6,*) 'Line list file?'
      read(5,'(a)') inchstr
      if(inchstr(1:1).eq.' ') goto 902
      open(unit=7,file=inchstr,status='old',err=901)
*     read data in to a wavelength array -- might as well be the
*     workspace ds
      if(nz.le.0) call vp_ewred(0)
      nlins=0
 1    read(7,'(a)',end=992) inchstr
      if(inchstr(1:8).ne.'        ') then
        nlins=nlins+1
        call vp_initval(inchstr,atoms,ions,trstw,
     :                       col,indcol,bval,indb,zval,indz)
*       correct trstw to nearest wavelength from the table
        dwrst=1.0d20
        ds(nlins)=trstw
        do i=1,nz
          if(lbz(i).eq.atoms.and.
     :               lzz(i).eq.ions) then
            ctwrst=abs(alm(i)-trstw)
            if(ctwrst.lt.dwrst) then
              ds(nlins)=alm(i)
              dwrst=ctwrst
            end if
          end if
        end do
      end if
      if(nlins.lt.500) goto 1
      write(6,*) ' Only first 500 lines used ',chrd
*     end of file
 992  close(unit=7)
*
*     initialize o/p arrays
      do k=1,nht
        ha(k)=0.0d0
        he(k)=0.0d0
        hc(k)=0.0d0
      end do
      write(6,*) nlins,' lines in table'
*     generate a correlation spectrum based on the line list
      do i=1,nht
        zp1=1.0d0+ach+dble(i)*bc
        pin=0.0d0
        nlacc=0
        do j=1,nlins
          wvl=zp1*ds(j)
*         convert to channel for data
          call chanwav(dble(wvl),ddum,1.0d-2,100)
          kcen=int(ddum+0.5d0)
          klo=kcen-nbin
          khi=kcen+nbin
          if(klo.le.lhigh.and.khi.gt.llow) then
*           at least some channels inside the data
            nlacc=nlacc+1
            do k=klo,khi
              if(k.ge.1.and.k.le.nct.and.de(k).gt.0.0d0) then
*	        S/N threshold
                derr=sqrt(de(k))
                if(ca(k).gt.snthres*derr) then
                  pin=pin+1.0d0
                  temp=ca(k)-da(k)
                  ha(i)=ha(i)+temp
                  if(temp.gt.sntr2*derr) then
                    he(i)=he(i)+1.0d0
                  end if
                end if
              end if
            end do
          end if
        end do
*	renormalize to reflect accepted points
        if(pin.gt.0.0d0) then
          ha(i)=ha(i)/pin
          if(he(i).gt.0.0d0) then
            he(i)=he(i)/pin
           else
            he(i)=1.0d-10
          end if
         else
          ha(i)=0.0d0
          he(i)=1.0d-10
        end if
        he(i)=he(i)*he(i)
        hc(i)=dble(nlacc)/dble(nlins)
      end do
*     swap the data so you plot what has just been calculated!
      ind=0
      call rd_daswap(da,de,ca,nct,ha,he,hc,nht,ind)
 902  return
      end
  
