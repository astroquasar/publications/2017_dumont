      subroutine dcholin(aa,ll,n,nopt)
*
      implicit none
      include 'vp_sizes.f'
*     ajc 20-nov-91  character routines
      integer n,nopt
      double precision aa(maxmas,maxmas),ll(maxmas,maxmas)
*     Local:
      integer i,ic,ir,il,it,iu,j,jj,k,kk,nsave
      double precision a(maxmas,maxmas),l(maxmas,maxmas)
      double precision sum,scf(maxmas)
      double precision aveigv,offset
*     Functions:
      logical varythis
*     Common:
      character*2 ipind(maxnpa)
      integer isod,isodh
      common/vpc_usoind/ipind,isod,isodh
*     output channels:
      integer lt(3)                     ! output channels for printing
      integer nopchan,nopchanh,nmonitor
      common/vpc_nopchan/lt,nopchan,nopchanh,nmonitor
*
* *** CHOLEsky decomposition of positive definite symmetric matrix
* ***          to invert the matrix.
      nsave=n
      if(nopt.le.0)then
*       no constrained parameters
        do ir=1,n
          do il=1,n
            a(il,ir)=aa(il,ir)
          end do
        end do
        goto 50
      endif
      kk=1
      jj=1
      do it=1,n
*       ajc 20-nov-91  changed to upper/lower case convention
*       this section removes zeros from hessian for fixed params
        if ( varythis( it, ipind ) ) then
          jj=1
          do iu=1,n
            if ( varythis( iu, ipind ) ) then
              a(jj,kk)=aa(iu,it)
              jj=jj+1
            end if
          end do
          kk=kk+1
        end if
      end do
*     offending equations now gone
      n=jj-1
      if(n.le.0) then
        write(6,*) '**** ERROR'
        write(6,*) '**** DCHOLIN: n.le.0',n
        goto 81
      end if
*
 50   continue
      do i=1,n
        scf(i)=dsqrt(a(i,i))
      end do
      do j=1,n
        do i=1,n
          a(i,j)=a(i,j)/(scf(i)*scf(j))
        end do
      end do
      l(1,1)=dsqrt(a(1,1))
      do 300 k=2,n
        do 200 j=1,k-1
          sum=a(k,j)
          if(j.eq.1) goto 200
          do i=1,j-1
            sum=sum-l(k,i)*l(j,i)
          end do
 200    l(k,j)=sum/l(j,j)
        sum=a(k,k)
        do i=1,k-1
          sum=sum-l(k,i)*l(k,i)
        end do
        if(sum.le.0.d0) then
          aveigv=a(1,1)
          do i=2,n
            aveigv=aveigv+a(i,i)
          end do
c         max eigenvalue < trace
          offset=aveigv*1.0d-15
          do i=1,n
            a(i,i)=a(i,i)+offset
          end do
          write(6,*)
     :           'WARNING - ill-conditioned matrix in DCHOLIN ****'
          write(6,*)' Offset added to diagonal =',offset
          if(nopchan.ge.2) then
            write(18,*)
     :           'WARNING - ill-conditioned matrix in DCHOLIN ****'
            write(18,*)'Offset added to diagonal =',offset
          end if
          goto 50
        end if
 300  l(k,k)=dsqrt(sum)
*
*     inverse lower triangular matrix
      do j=1,n
        do i=j,n
          if(i.eq.j)then
            sum=1.d0
            goto 500
           else
            sum=0.d0
          endif
          do k=j,i-1
            sum=sum-l(i,k)*a(k,j)
          end do
 500      a(i,j)=sum/l(i,i)
        end do
      end do
*     This looks amazingly inefficient
*     now inverse of A
      do j=1,n
        do i=j,n
          sum=0.d0
          do k=i,n
            sum=sum+a(k,i)*a(k,j)
          end do
          l(i,j)=sum
        end do
      end do
*
      do j=1,n
        do i=1,n
          l(i,j)=l(i,j)/(scf(i)*scf(j))
        end do
      end do
*     put result in L
      do j=1,n
        do i=j,n
          l(j,i)=l(i,j)
        end do
      end do
*
 81   continue
      if(nopt.le.0) then
        do i=1,nsave
          ll(i,i)=l(i,i)
        end do
       else
        ic=1
        do j=1,nsave
*         ajc 20-nov-91  again, change to new convention
          if ( varythis( j, ipind ) ) then
            ll(j,j)=l(ic,ic)
            ic=ic+1
           else
            ll(j,j)=0.d0
          end if
        end do
      end if
      n=nsave
      return
      end
