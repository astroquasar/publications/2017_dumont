      subroutine rd_splvtick(cfile)
*
*     ticks from ion & redshifts
      implicit none
      include 'vp_sizes.f'
      character*(*) cfile
*     Local:
      character*64 chsx(50)
      integer j,k,nbest,nlx
      double precision trwv,wdiff
*
*     string unscrambling common
      character*132 inchstr
      character*60 cv(24)
      real rv(24)
      integer iv(24)
      integer nv
      common/rd_chwork/inchstr,rv,iv,cv,nv
      double precision dv(24)
      common/vpc_dsepspace/dv
*     line list variables
      double precision zmark
      character*132 chmarkfl
      logical lmarkl,lmarkto
      common/rdc_lmarklv/zmark,chmarkfl,lmarkl,lmarkto
*     plot/print velocity scale?
      logical lvel
      real wcenpg,vellopg,velhipg,zp1refpg
      common/vpc_pgvels/wcenpg,vellopg,velhipg,lvel,zp1refpg
*     observed wavelengths?
      logical lobswav,ltick
      common/rdc_lobswav/lobswav,ltick
**     stack spectra variables
      character*2 atoms(64)
      character*4 ions(64)
      double precision rstwavs(64)
      integer nlins
      common/rdc_stplvar/atoms,ions,rstwavs,nlins
*     atomic data
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm
      integer m
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),m
*
      open(unit=29,file=cfile,status='old',err=987)
      nlx=1
 986  read(29,'(a)',end=985) chsx(nlx)
      if(chsx(nlx)(1:1).ne.' '.and.chsx(nlx)(1:1).ne.'!') then
        nlx=nlx+1
        goto 986
      end if
 985  nlx=nlx-1
*     set internal flags so that ticks appear
      lmarkl=.true.
      lmarkto=.true.
*     redshift outside sensible range => tick marks from file
      zmark=-1.0e20
*     but they don't with just the above, so need some rd_plcset presets
      lvel=.true.
      ltick=.true.
      nlins=nlx
      do k=1,nlx
        call dsepvar(chsx(k),3,dv,iv,cv,nv)     
        if(cv(1)(2:2).eq.'I'.or.cv(1)(2:2).eq.'J'.or.cv(1)(2:2).eq.'V'
     :   .or.cv(1)(2:2).eq.'X') then
          atoms(k)=cv(1)(1:1)//' '
          ions(k)=cv(1)(2:5)
         else
          atoms(k)=cv(1)(1:2)
          if(cv(1)(3:3).ne.' ') then
            ions(k)=cv(1)(3:6)
            trwv=dv(2)
           else
            ions(k)=cv(2)
            trwv=dv(3)
          end if
        end if
*       get closest rest wavelength
        wdiff=1.0d30
        nbest=0
        do j=1,m
          if(lbz(j).eq.atoms(k).and.lzz(j).eq.ions(k).and.
     :     abs(alm(j)-trwv).lt.wdiff) then
            nbest=j
          end if
        end do
        rstwavs(k)=alm(nbest)
      end do

 987  close(unit=29)
      return
      end
