      double precision function rd_wvsc(ddum)
*     returns wavelength for the linearized coefficients
      double precision ddum
*     common variables
*     linear wavelength coefficients
      character*4 chwts
      double precision ac,bc,ach,achend
      common/rdc_scwv/ac,bc,ach,achend,nbr,chwts
*     scrunch style flag, isclog=1 for log bins
      common/rdc_scflag/isctype,isclog
*     
      if(isclog.eq.1.or.chwts(1:3).eq.'log') then
*       log linear
        rd_wvsc=10.0d0**ach+(ddum*bc)
       else
        rd_wvsc=ach+bc*ddum
      end if
      return
      end
      double precision function rd_wvscofs(ddum)
*     returns wavelength for the linearized coefficients
*     using the offset base ac instead of ach
      double precision ddum
*     common variables
*     linear wavelength coefficients
      character*4 chwts
      double precision ac,bc,ach,achend
      common/rdc_scwv/ac,bc,ach,achend,nbr,chwts
*     scrunch style flag, isclog=1 for log bins
      common/rdc_scflag/isctype,isclog
*     
      if(isclog.eq.1.or.chwts(1:3).eq.'log') then
*       log linear
        rd_wvscofs=10.0d0**(ac+ddum*bc)
       else
        rd_wvscofs=ac+bc*ddum
      end if
      return
      end
