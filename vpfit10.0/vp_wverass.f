      subroutine vp_wverass
*     set the wavelength shift coefficients, linking parameter sets with
*     wavelength regions for the '>>' line flag
      implicit none
      include 'vp_sizes.f'
*
      integer i,item,iz,j
      double precision wvl
*
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*
*     results from here are a list of parameter lines associated with
*     the chunk .. used for wavelength shifts, held in:
      integer lassoc
      common/vpc_asschnk/lassoc(maxnch)
*
*     rfc 22.5.99: parameter arrays passed as common
      character*2 elem(maxnio)
      character*4 ionz(maxnio)
      double precision parm(maxnpa)
      integer nlines,nn
      common/vpc_parry/parm,elem,ionz,nlines,nn
      character*2 ipind(maxnpa)
      integer isod,isodh
      common/vpc_usoind/ipind,isod,isodh
*     chunk variables:
      integer ndpts(maxnch),idrun(maxnch),icrun(maxnch)
      integer indfil(maxnch)
      character*64 filename(maxnch)
      double precision wvstrt(maxnch),wvend(maxnch)
      integer nchunk
      common/vpc_chunk/wvstrt,wvend,ndpts,filename,
     :      idrun,icrun,indfil,nchunk
*     lines to dataset numbers
      integer linchk(maxnio)
      common/vpc_linchk/linchk
*     atomic parameters
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm
      integer nz 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),nz
*
*     Find '>>' parameters from atomic line list
      if(nz.le.0) call vp_ewred(0)
      iz=1
      do while (iz.lt.nz.and.lbz(iz).ne.'>>')
        iz=iz+1
      end do
      do j=1,nchunk
        lassoc(j)=0
      end do
*
      if(lbz(iz).ne.'>>') goto 999
      nlines=nn/noppsys
      do i=1,nlines
        if(elem(i).eq.'>>') then
*	  fix the redshift and column density parameters if user forgot
          item=(i-1)*noppsys+nppcol
          if(ipind(item).eq.'  ') ipind(item)='HU'
          parm(item)=1.0d0 
          item=item+nppzed-nppcol
          if(ipind(item).eq.'  ') ipind(item)='HV' 
*	  sort out wavelength region, and shift for it
          if(linchk(i).ne.0) then
            lassoc(linchk(i))=i
           else
            wvl=(1.0d0+parm(item))*alm(iz)
            do j=1,nchunk
              if(wvstrt(j).lt.wvl.and.wvend(j).gt.wvl) then
*	        this is the region
                lassoc(j)=i
              end if
            end do
          end if
        end if
      end do
 999  continue
      return
      end
