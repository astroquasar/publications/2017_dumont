      subroutine splot(y,e,rms,ca,n,ind)
*
      implicit none
      include 'vp_sizes.f'
*
*     Subroutine arguments
      integer n,ind
      double precision y(n),e(n),ca(n)
      double precision rms(n)
*     Local
      integer i,is,j,kaxis
      integer lc1v,lc2v,lm,ncvv,nv
      double precision dtemp,xtem1d,yhd,ymd
      double precision xtem2,xtem3,xtem4,xhd,xmd
      real dsmpg,dsxpg,rsclpg
      real paddpg,rtempg,stempg,xtempg
      real xlxpg,xhxpg,yhtpg,ymtpg
*     zero line array (if needed)
      real xzropg(2),yzropg(2)
*     Functions
      double precision wval

*     Common
*     nst: start channel, ncn: end channel, lc1, lc2 start, end for scale
      integer nst,ncn,lc1,lc2
      real fampg
      common/ppam/nst,ncn,lc1,lc2,fampg
*     residual parameters
      real residpg,rshifpg
      common/vpc_resid/residpg,rshifpg
*     text label for a curve
      integer icoltext
      character*24 cpgtext
      real xtextpg,ytextpg
      common/pgtexts/xtextpg,ytextpg,cpgtext,icoltext
*     pgplot attributes: data, error, continuum, axes, ticks, RESIDUAL
*     1 color indx; 2 line style; 3 line width;
*     4 curve(0), hist(1); 5 - 10 reserved.
      integer ipgatt(10,9)
      common/vpc_pgattrib/ipgatt
*     plot array pointers (consistent with ipgatt)
*     0=don't,1=do for 1:data,2:error,3:cont,6:resid,9:rms
      integer ksplot(9)
      common/vpc_ksplot/ksplot
*     min and max yscales by hand, and default lower level
      integer nyuse
      real ylowhpg,yhihpg,yminsetpg
      common/pgylims/ylowhpg,yhihpg,yminsetpg,nyuse
*     pgplot flags
      integer ipgopen,ipgflag
      common/pgplotv/ipgopen,ipgflag
*     pgplot workspace
      real dspg,repg
      common/vpc_snglwork/dspg(maxfis),repg(maxfis)
*     pgplot overplot with bias
      integer ipgov
      real pgbias
      logical plgscl
      common/pgover/ipgov,pgbias,plgscl
      integer ipgnx,ipgny
      character*60 pglchar(3)
      common/pgnumba/ipgnx,ipgny,pglchar
      integer itflag
      common/tickfile/itflag
*     plot maxima and minima
      real xmpg,xhpg,ympg,yhpg
      common/vpc_splcur/xmpg,xhpg,ympg,yhpg
*     plot/print velocity scale?
      logical lvel
      real  wcenpg,vellopg,velhipg,zp1refpg
      common/vpc_pgvels/wcenpg,vellopg,velhipg,lvel,zp1refpg
*     region variables
      logical lmarkreg
      common/vpc_markreg/lmarkreg
*     line list variables
      double precision zmark
      character*132 chmarkfl
      logical lmarkl,lmarkto
      common/rdc_lmarklv/zmark,chmarkfl,lmarkl,lmarkto
*     ASCII printout instead of plot if lascii=.true.
      logical lascii,lplottoo
      common/vpc_pgascii/lascii,lplottoo
*     file steered tick marks
      logical ltvtick
      character*132 chtvtick
      common/rdc_tvtick/chtvtick,ltvtick
*     line on plot?
      logical lgline
      real pglxy(4)
      integer kpglxy(3)
      common/vpc_plline/lgline,pglxy,kpglxy
*     file steered extras
      character*64 pgfxfile
      common/rdc_pgfxfile/pgfxfile
*
*     preset hand limits
      if(nyuse.ge.1) then
        nyuse=nyuse-1
       else
*       fresh yscale
        ylowhpg=-1.0e25
        yhihpg=-1.0e25
      end if
c     set plot control variables
      if(ind.ne.2) then
        call plset(ind)
c       flag requested quit
        if(ind.eq.-1) return
      end if
*
*     make sure variables within limits
      ncvv=ncn
      lc1v=max0(lc1,1)
      lc2v=min0(lc2,ncvv)
*
      call dcmax1(y,ncvv,lc1v,lc2v,yhd,ymd)
      ympg=real(ymd)
      yhpg=real(yhd)
      if(yhihpg.gt.-1.0e24) then
        yhpg=yhihpg
       else
        yhpg=yhpg*1.015
        if(yhpg.le.0.) yhpg=1.
      end if
      if(ylowhpg.gt.-1.0e24) then
        ympg=ylowhpg
       else
        if(ympg.gt.yminsetpg) then
          ympg=yminsetpg
         else
          if(ympg.lt.0.0) then
            ympg=1.3*ympg
            if(ympg.lt.-0.2*yhpg) ympg=-0.2*yhpg
           else
            ympg=0.95*ympg
          end if
        end if
      end if
      if(ipgopen.ne.0.and.ipgflag.eq.1) then
*       use pgplot
        yhpg=yhpg*fampg
        lm=nst-1
        ymd=dble(ympg)
        yhd=dble(yhpg)
*       PGPLOT single precision variables
        xhd=wval(dble(ncn))
        xhpg=real(xhd)
        xmd=wval(dble(nst))
        xmpg=real(xmd)
        if(lm.lt.0) lm=0
        nv=ncn-lm
*       make sure array within limits
        nv=min0(nv,n-lm)
*
*       ascii file write
        if(lascii) then
          write(17,'(a)') '! -------------------------'
          do i=1,nv
            j=lm+i
            xtem2=y(j)
            xtem3=ca(j)
            if(e(j).gt.0.0d0) then
              xtem4=sqrt(e(j))
             else
              xtem4=0.0d0
            end if
            if ( lvel .and. wcenpg.gt.0.0) then
              dtemp=dble(wcenpg)
*             convert to velocity if that is what was requested:
              xtem1d=(wval(dble(j))/dtemp-1.0d0)
     :                *2.99792458d5
              stempg=real(xtem1d)
              if(stempg.ge.vellopg.and.stempg.le.velhipg)
     :                             then
                write(17,'(f15.5,1pe16.6,2e16.6)') xtem1d,xtem2,
     :                 xtem3,xtem4
              end if
             else
*             normal wavelengths
              xtem1d=wval(dble(j))
              write(17,'(f15.5,1pe16.6,2e16.6)') xtem1d,xtem2,
     :                 xtem3,xtem4
            end if
          end do
*         branch to exit
          if(.not.lplottoo) goto 998
        end if
*
*       normal pgplot use, repg(i) is an array of wavelength values for
*       pgplot, so convert to single precision
        do i=1,nv
          j=lm+i
          repg(i)=real(wval(dble(j)))
          dspg(i)=real(y(j))
        end do
        if(xhpg.le.xmpg) goto 999
*       if velocity plot, change to appropriate variables
        if (lvel) then
          rtempg=(xmpg/wcenpg)**2
          xmpg=3.0e5*(rtempg-1.0)/(rtempg+1.0)
          rtempg=(xhpg/wcenpg)**2
          xhpg=3.0e5*(rtempg-1.0)/(rtempg+1.0)
          do i=1,nv
*           j=lm+i  is unnecessary
            rtempg=(repg(i)/wcenpg)**2
            repg(i)=3.0e5*(rtempg-1.0)/(rtempg+1.0)
          end do
*         does the data itself need rescaling?
          if(plgscl) then
            call cmax1(dspg,nv,1,nv,
     :               yhtpg,ymtpg)
            rsclpg=yhtpg-ymtpg
            if(rsclpg.gt.0.0) then
              rsclpg=0.95/rsclpg
            end if
            do i=1,nv
              dspg(i)=rsclpg*(dspg(i)-ymtpg)
            end do
          end if
        end if
        if(ipgov.le.0) then
          call pgsci(ipgatt(1,4))
          call pgsls(ipgatt(2,4))
          call pgslw(ipgatt(3,4))
          kaxis=1
          if(ipgatt(10,4).ne.11) then
            kaxis=0
          end if
          call pgenv(xmpg,xhpg,ympg,yhpg,0,kaxis)
          call pglabel(pglchar(1),pglchar(2),pglchar(3))
          if(ipgatt(10,4).eq.10) then
*           draw y=0 line only
              xzropg(1)=repg(1)
            xzropg(2)=repg(nv)
            yzropg(1)=0.0
            yzropg(2)=0.0
            call pgline(2,xzropg,yzropg)
          end if
          if(ipgatt(10,4).eq.9) then
*           draw x=0 line only
              xzropg(1)=0.0
            xzropg(2)=0.0
            yzropg(1)=ympg
            yzropg(2)=yhpg
            call pgline(2,xzropg,yzropg)
          end if
          paddpg=0.0
         else
          paddpg=pgbias
          do is=1,nv
            dspg(is)=dspg(is)+pgbias
          end do
        end if
c       1 color indx; 2 line style; 3 line width;
c       4 curve(0), hist(1); 5 - 9 reserved.
        call pgsci(ipgatt(1,1))
        call pgsls(ipgatt(2,1))
        call pgslw(ipgatt(3,1))
        if(ipgatt(4,1).eq.2) then
*         plot as horizontal bars
*         1st point
          xlxpg=1.5*repg(1)-0.5*repg(2)
          xhxpg=0.5*(repg(2)+repg(1))
          call pgerrx(1,xlxpg,xhxpg,dspg(1),0.0)
*         mid-range
          do i=2,nv-1
            xlxpg=xhxpg
            xhxpg=0.5*(repg(i+1)+repg(i))
            call pgerrx(1,xlxpg,xhxpg,dspg(i),0.0)
          end do
*         last point
          xlxpg=xhxpg
          xhxpg=1.5*repg(nv)-0.5*repg(nv-1)
          call pgerrx(1,xlxpg,xhxpg,dspg(nv),0.0)
         else
*         connected curves:
          if(ipgatt(4,1).ne.1) then
*           standard curve
            call pgline(nv,repg,dspg)
           else
c           binned data
            call pgbin(nv,repg,dspg,.true.)
          end if
        end if
c       set text on curve
        if(cpgtext(1:1).ne.' ') then
          if(icoltext.eq.0) then
*           Use the axis colour
            call pgsci(ipgatt(1,4))
           else
            if(icoltext.lt.0) then
*             Use the curve colour
              call pgsci(ipgatt(1,1))
             else
*             Use locally defined colour
              call pgsci(icoltext)
            end if
          end if
          call pgsls(ipgatt(2,4))
          call pgslw(ipgatt(3,4))
          call pgtext(xtextpg,ytextpg,cpgtext)
c         clear so don't have same annotation again
          cpgtext=' '
        end if
*       draw a line if requested
        if(lgline) then
*         use color as requested
          call pgsci(kpglxy(3))
*         and line style, width
          call pgsls(kpglxy(1))
          call pgslw(kpglxy(2))
*         goto first set of coordinates
          call pgmove(pglxy(1),pglxy(2))
*         draw a line
          call pgdraw(pglxy(3),pglxy(4))
          lgline=.false.
        end if
*       other curves
        if(ksplot(2).gt.0) then
*         plot error
          call pgsci(ipgatt(1,2))
          call pgsls(ipgatt(2,2))
          call pgslw(ipgatt(3,2))
          if(ipgatt(4,2).eq.2) then
*           plot error bars - requires different handling
            do i=1,nv
              j=lm+i
              if(e(j).gt.0.0d0) then
                xtempg=real(sqrt(e(j)))
               else
                xtempg=0.0
                if(e(j).lt.0.0d0) xtempg=yhpg
              end if
              dsxpg=xtempg+real(y(j))+paddpg
              dsmpg=real(y(j))-xtempg+paddpg
              call pgerry(1,repg(i),dsxpg,dsmpg,0.0)
            end do
           else
*           plot as histogram or curve
            do i=1,nv
              j=lm+i
              if(e(j).gt.0.0d0) then
                dspg(i)=real(sqrt(e(j)))+paddpg
               else
                if(e(j).lt.0.0d0) dspg(i)=yhpg
              end if
            end do
            if(ipgatt(4,2).ne.1) then
              call pgline(nv,repg,dspg)
             else
              call pgbin(nv,repg,dspg,.true.)
            end if
          end if
        end if
*       rms array
        if(ksplot(9).gt.0) then
          call pgsci(ipgatt(1,9))
          call pgsls(ipgatt(2,9))
          call pgslw(ipgatt(3,9))
*         plot as histogram or curve
          do i=1,nv
            j=lm+i
            if(rms(j).gt.0.0d0) then
              dspg(i)=real(sqrt(rms(j)))+paddpg
             else
              if(e(j).lt.0.0d0) dspg(i)=yhpg
            end if
          end do
          if(ipgatt(4,9).ne.1) then
            call pgline(nv,repg,dspg)
           else
            call pgbin(nv,repg,dspg,.true.)
          end if
        end if
*       continuum
        if(ksplot(3).gt.0) then
          call pgsci(ipgatt(1,3))
          call pgsls(ipgatt(2,3))
          call pgslw(ipgatt(3,3))
          do i=1,nv
            j=lm+i
            dspg(i)=real(ca(j))+paddpg
          end do
          if(ipgatt(4,3).ne.1) then
c           standard line
            call pgline(nv,repg,dspg)
           else
            call pgbin(nv,repg,dspg,.true.)
          end if
        end if
        if(ksplot(6).gt.0) then
*       ajc 24-jul-91 plot residuals
          if ( residpg .gt. 0.0 ) then
            call pgsci( ipgatt( 1, 6 ) )
            call pgsls( ipgatt( 2, 6 ) )
            call pgslw( ipgatt( 3, 6 ) )
            do i=1,nv
              j = lm + i
              if(rms(j).gt.0.0d0) then
                dspg(i)=residpg*real((y(j)-ca(j))/sqrt(rms(j)))+
     :                       0.5*(ympg+yhpg)+rshifpg
               else
                dspg(i)=0.5*(ympg+yhpg)+rshifpg
              end if
            end do
            call pgline( nv, repg, dspg )
*           one sigma deviation line
            call pgmove(xmpg, 0.5*(ympg+yhpg)+residpg+rshifpg)
            call pgdraw(xhpg, 0.5*(ympg+yhpg)+residpg+rshifpg)
            call pgmove(xmpg, 0.5*(ympg+yhpg)-residpg+rshifpg)
            call pgdraw(xhpg, 0.5*(ympg+yhpg)-residpg+rshifpg)
          end if
        end if
*       extras
        if(pgfxfile(1:1).ne.' ') then
          call rd_pgannot(pgfxfile)
          pgfxfile=' '
        end if
*       set up for the tick marks (.not.lvel requirement removed 6.11.02)
        if(lmarkl.and.(.not.ltvtick)) then
*         Line IDs from a file already read in
          call vp_lineid(xmpg,xhpg,ympg,yhpg)
         else
          call pgsci(abs(ipgatt(1,5)))
          call pgsls(ipgatt(2,5))
          call pgslw(ipgatt(3,5))
          if(ltvtick) then
            call rd_tickvpfl(chtvtick)
           else
            if(itflag.eq.1) then
*             ajc 15-nov-93  logical indicates numbers to plot
              call tickpg(xmpg,xhpg,ympg,yhpg,(ipgatt(1,5).gt.0))
             else
              call dotick(xmpg,xhpg,ympg,yhpg,(ipgatt(1,5).gt.0))
            end if
          end if
        end if
        if(lmarkreg.and.(.not.lvel)) then
          call rd_markreg(xmpg,xhpg,ympg,yhpg)
        end if
       else
        write(6,'('' What did you mean?'')')
      end if
998   continue
      call pgsci(abs(ipgatt(1,8)))
      RETURN
999   write(6,*) ' Zero wavelength range',xmpg,' - ',xhpg
      goto 998
      END
