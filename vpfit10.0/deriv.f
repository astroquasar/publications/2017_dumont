      subroutine deriv(parm,ipind,np,grad,hessian,
     :                 idiag,nchunk,ion,level,lrms)
*
*
*     IN:
*     [nd(nchunk)     int arr    No. data points per chunk NOW GONE]
*     parm(np)       dble arr   parameter values
*     ipind(np)      ch*2 arr   tied indicators
*     np             int        no of parameters
*     nchunk         int        no of chunks
*     ion            ch*2 arr   element
*     level          ch*4 arr   ionization level
*     lrms           logical    .t. fro rms, .f. for error array
*     
*     OUT:
*     grad(np)       dble arr   gradients
*     hessian(mx,mx) dble arr   hessian matrix [(np,np) used]
*
      implicit none
      include 'vp_sizes.f'
*
*     subroutine arguments
*      integer*4 nd(maxnch)
      character*2 ion(maxnio)
      character*4 level(maxnio)
      character*2 ipind(maxnpa)
      logical lrms ! .T. for rms, .F. for error array
      integer idiag,nchunk,np
*     test functions
      logical varythis
*     data(maxchs,maxnch),error(maxchs,maxnch) ! not used
      double precision parm(maxnpa)
      double precision pphi(maxchs,maxnch)
      double precision ff(maxnpa),bb(npasqu),aa(npasqu)
      double precision hessian(maxnpa,maxnpa),grad(maxnpa)
      double precision diff
*     LOCAL:
      integer i,ii,ichk,if,ij,ip,is,ix,ibase,ichunk,ichdum
      integer j,jj,jchk,j1,j2,jbase
      integer k,kk,kloc,kjb,klen,kphi
      integer l,li,lt,lkoffs
      integer n,nn,njj,nchk,nsubd,nchcen,nfunc
      integer nppxt
      double precision top,bot
      double precision bmincur
      double precision dtemp,velpch
      double precision delparm(maxnpa)
*     FUNCTIONS:
*     (none)
*
      double precision param(maxnpa)
*     subchunk workspace
      double precision contpass(maxscs),fconv(maxscs)
      common/vpc_duwork/fconv,contpass
*     ajc 25-nov-91  alternate metric added
      logical tied
      integer linchk( maxnio )
      common/vpc_linchk/linchk
*     nvdots is number of variables (=3*nlines) for dot display
      integer nvdots
      common/vpc_dots6/nvdots
*     metric flags
      integer metric
      logical even
      common / metric / metric, even
*     parameter differences
      double precision pardif(maxnpa)
      common /vpc_pardif/pardif
*     fit values
      double precision func(maxscs),phi(maxchs,maxnch)
      common/vpc_der/ func,phi
*
      integer nminchsqit
      double precision cdstsf,zstep,bstep,chsqdif1,chsqdnth
      double precision chsqdif2
      common/vpc_jkw2/cdstsf,zstep,bstep,chsqdif1,chsqdnth,
     :     chsqdif2,nminchsqit
      double precision step4,step5
      common/vpc_xvstep/step4,step5
*      integer ngd,npexsm
*      integer numpts(maxnch)
*      double precision wcoeff(maxnch)
*      common/vpc_jkw3/wcoeff,ngd,npexsm,numpts
      double precision vturb,atmass,temper,fixedbth
      common/vpc_ucopder/vturb(maxnio),atmass(maxnio),
     :                 temper(maxnio),fixedbth(maxnio)
*     Variables used: indvar=1 for logN, 0 for N, -1 for emission
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
*     wavelength shifts, held in:
*     common/vpc_asschnk/lassoc(maxnch)
*
*     ajc 3-feb-92  dimension of f altered to match phlot etc - this has to 
*               hold a value for every data point * every parameter (wow!)
*     note that maxnpo is in common block biggy -
*     used for workspace in ucoptv
*     to do global ks test
      double precision f(maxnpo,maxnpa)
      common/vpc_biggy/f
*     debugging variables -- 10 & 11 for printouts here only, 0 for all
      integer ndbranch
      common/vpc_debug1/ndbranch
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     chunk subset variables (rfc: 24.08.05)
*     only preset is nrextend - number of pixels to extend a chunk by
*     so that convolution does not have a jump at the end
      integer nrextend
      common/vpc_chunkext/nrextend
*     chunk subdivision variables
      integer nminsubd,nmaxsubd
      double precision bindop
      common/vpc_vpsubd/bindop,nminsubd,nmaxsubd
      integer npsubch
      double precision wvsubd(maxscs)
      common/vpc_wvsubd/wvsubd,npsubch
*     chunk FWHM well sampled by pixels (if this variable = .true.)
      logical lresint(maxnch)
      double precision dlgres
      common/vpc_lresint/dlgres,lresint
*     New 2D dataset variables
      integer numchunk,nchchtot ! # of chunks,total # data points
      integer kchstrt(maxnch),kchend(maxnch),nchlen(maxnch)
      common/vpc_chunkvar/kchstrt,kchend,nchlen,numchunk,nchchtot
      double precision dach(maxchs,maxnch),derch(maxchs,maxnch)
      double precision drmch(maxchs,maxnch),dcch(maxchs,maxnch)
      common/vpc_datch/dach,derch,drmch,dcch
*     chunk wavelengths
      integer nwvch(maxnch)
      double precision wavch(maxchs,maxnch),wavchlo(maxchs,maxnch)
      common/vpc_wavch/wavch,wavchlo,nwvch
*     extra ion list & parameters
      integer nionxt
      character*2 elmxt(maxxpi)
      character*4 ionxt(maxxpi)
      character*2 ipinxt(maxxpp)
      double precision bvxmin
      double precision parmxt(maxxpp)
      common/vpc_parmxt/bvxmin,parmxt,ionxt,elmxt,nionxt,ipinxt
*
*     DERIV  computes gradient vector and Hessian matrix for ls
      lt=6
      li=18
      nfunc=np/noppsys
*     clear arrays
      do j=1,np
        grad(j)=0.0d0
        do i=1,np
          hessian(i,j)=0.0d0
        end do
      end do
*
*     check the array size 
      if ( nchchtot .ge. maxnpo ) then
        write(6,*) 'Data length',nchchtot,
     :       ' is more than max # points allowed',maxnpo
        write(6,*) 'Too much data selected - deriv.f dying...'
        stop
      end if
*
*     Compute derivatives of total function
*
*     Set up the parameter increments, and find minimum current b-value
      bmincur=1.0d30
      do n=1,nfunc
        nn=noppsys*(n-1)
*       min b value should exclude special cases
        if ( ion( n ) .ne. '<<' .and.
     :       ion( n ) .ne. '<>' .and.
     :       ion( n ) .ne. '>>' .and.
     :       ion( n ) .ne. '__' ) then
          bmincur=min(bmincur,parm(nn+nppbval))
        end if
        if(indvar.eq.1) then
          pardif(nn+nppcol)=(cdstsf*scalelog)
         else
          pardif(nn+nppcol)=cdstsf*parm(nn+nppcol)
        endif
        pardif(nn+nppzed)=zstep
        if(ion(n).eq.'>>') then
          pardif(nn+nppbval)=3.0d0*bstep
         else
          pardif(nn+nppbval)=bstep
        end if
        if(noppsys.ge.4) then
          pardif(nn+4)=step4
          if(noppsys.ge.5) then
            pardif(nn+5)=step5
            if(noppsys.ge.6) then
              write(6,*) 'Too many variables per system'
              stop
            end if
          end if
        end if
      end do
      if(nionxt.gt.0) then
*       if any preset ions, need to include them as well
        bmincur=min(bmincur,bvxmin)
      end if
*
*     Initially set parameter increments to zero
*
*     MAIN LOOP
*
      if ( np .gt. nvdots ) then
        do ip = 1, np
          write ( *, '(a$)' ) '.'
        end do
      end if

      do ip=1,np
        if ( np .gt. nvdots ) then
          write ( *, '(a$)' ) char( 8 )//' '//char( 8 )
          call flush( 6 )
        end if
        do i=1,np
          delparm(i)=0.0d0
        end do
*       ajc 25-nov-91  switch as necessary 
*       standard metric if requested or switching or z or tied
*       parameter order is n, z, b
*       nppcol=1, nppzed=2 & nppbval=3 usually
        tied=.false.
        jbase=(ip-1)/noppsys
        jbase=jbase*noppsys
        j2=jbase+noppsys
        j1=jbase+1
        do j=j1,j2
          if(ipind(j).ne.'  ') then
            tied=.true.
          end if
        end do
*
        if( metric.eq.0 .or. 
     :       (metric.eq.2.and. .not.even).or.
     :       (ip-jbase.ne.nppcol.and.ip-jbase.ne.nppbval).or.
     :          tied) then
*    :     was:    mod( ip+1, 3 ) .eq. 0 .or. tied ) then
          delparm(ip)=pardif(ip)
         else
*         replace n by b+n, b by n-b
*         if ( mod( ip+2, 3 ) .eq. 0 ) then
          if(ip-jbase.eq.nppcol) then
            delparm( ip ) = pardif( ip )
            delparm( jbase+nppbval ) = pardif(jbase+nppbval )
           else
            delparm( ip ) = -1.0d0 * pardif( ip )
            delparm( jbase+nppcol ) = pardif( jbase+nppcol )
          end if
        end if
        do i=1,np
          param(i)=parm(i)+delparm(i)
        end do
*
*	check for positivity of N and b (all values must have a  
*               good outlook on life) with untied parameters
*	but exclude velocity parameter for '<>','__' & '>>',
*	which can also be negative.
*
        do i = 1, np
          if ( varythis( i, ipind ) ) then
            njj=(i-1)/noppsys+1
            ibase=noppsys*(njj-1)
*           if ( mod( i+1, 3 ) .ne. 0 .and. ( metric .eq. 0 .or. 
            if((i-ibase.eq.nppcol.or.i-ibase.eq.nppbval).and.
     :          (metric.eq.0.or.(metric.eq.2.and. .not.even))) then
              if(ion(njj).ne.'<>'.and.ion(njj).ne.'>>'.and.
     :             ion(njj).ne.'__'.and.param(i).lt.1.0d-28) then
*	        rfc 5.12.95 1.0e-28 replaces 0.001 (emission lines)
                param( i ) = 1.0d-28
                delparm( i ) = param( i ) - parm( i )
                if(i-ibase.eq.nppbval) then
                  call checkb( temper(njj), vturb(njj ),
     :                           atmass(njj ), param( i ) )
                end if
              end if
            end if
          end if
        end do

*       if there is an upper case label, then no derivatives needed
        if ( .not. varythis( ip, ipind ) ) then
          do ix=1,nchchtot
            f(ix,ip)=0.0d0
          end do
          param(ip) = parm(ip)
         else
*         if here, then may be untied or a lower case (primary)
*                system - so check for lower case letter  
          if ( varythis( ip, ipind ) ) then
            call vp_tie1val( ion, level, np, param, ipind, temper,
     :                     vturb, atmass, fixedbth, ip )
          end if

*         ajc 11-oct-93  adjustments necessary to keep total
*	  column density correct (if total goes too low)
          call vp_checkn( np, param, ipind )
*         parameters are all set up now for calculating total function
          do ichunk=1,nchunk
            ichdum=ichunk
*           no need to reset subdivision, since the derivatives are one-sided, 
*           with the increments positive - in particular the velocity one,
*           which sets the subpixel size - so can use the subdivision criteria
*           from before.
            nchcen=nchlen(ichunk)/2
            velpch=0.5d0*2.99792458d5*
     :          (wavch(nchcen+1,ichunk)-wavch(nchcen-1,ichunk))/
     :           wavch(nchcen,ichunk)
*           want at least bindop bins per Doppler parameter, so min binsize
            dtemp=bmincur/bindop
            nsubd=int(velpch/dtemp+0.9999d0)
            if(nsubd.lt.nminsubd) nsubd=nminsubd
            nsubd=min(nsubd,maxscs/nchlen(ichunk))
            nsubd=min(nsubd,nmaxsubd)
*           length of subdivided continuum array:
            klen=nchlen(ichunk)*nsubd
            npsubch=klen
            do k=1,klen
              contpass(k)=1.0d0
              fconv(k)=1.0d0
              kjb=(k-1)/nsubd
              kloc=k-nsubd*kjb
              dtemp=(dble(kloc)-0.5d0)/dble(nsubd)
              wvsubd(k)=(1.0d0-dtemp)*wavchlo(kjb+1,ichunk)+
     :                   dtemp*wavchlo(kjb+2,ichunk)
            end do
*
*           Any preset extras also included: 
            if(nionxt.gt.0) then
              nppxt=nionxt*noppsys
              do i=1,nionxt
*               k=(i-1)*noppsys
                call vp_spvoigte(func,contpass,1,klen,wvsubd,npsubch,
     :                parmxt,ipinxt,elmxt,ionxt,i,nppxt,ichdum,fconv)
                do k=1,klen
                  contpass(k)=func(k)
                end do
              end do
            end if
            do n=1,nfunc
*             Put in only if chunk number matches or is zero
              if ( linchk( n ) .eq. 0 .or.
     :               linchk( n ) .eq. ichunk ) then
                call vp_spvoigte(func, contpass,1,klen,wvsubd,
     :               npsubch,param,ipind,ion,level,n,np,ichdum,fconv)
              end if
              do k=1,klen
                contpass(k)=func(k)
              end do
            end do
*
*           convolve & rebin
*           fconv is subbin convolution; pphi is binned as for original data
*           =fconv rebinned
            call vp_chipconv(func,klen,nsubd,ichdum, fconv,pphi)
          end do

*         Calculate total function derivatives
          if=1
          do ichunk=1,nchunk
            lkoffs=kchstrt(ichunk)-1
            do k=kchstrt(ichunk),kchend(ichunk)
              kphi=k-lkoffs
              top=pphi(kphi,ichunk)-phi(kphi,ichunk)
              bot = pardif(ip)
*             write(18,*) '>>>>',kphi,pphi(kphi,ichunk),
*     1                  phi(kphi,ichunk),pardif(ip)
              f(if,ip)=top/bot
              if=if+1
            end do
          end do
          if=if-1
*	  if is the total no. of data points

          if(idiag.gt.0)then
*           rfc 9.12.03: targeted diagnostic output
            if(ndbranch.le.0.or.ndbranch.eq.10) then
              write(li,1299)
              write(lt,1299)
1299          format(/' Parameters: current, fdd incs, fdd values'/)
              do ii=1,nfunc
                is=noppsys*(ii-1)
                write(li,*)parm(is+1),delparm(is+1),param(is+1)
                write(li,*)parm(is+2),delparm(is+2),param(is+2)
                write(li,*)parm(is+3),delparm(is+3),param(is+3)
                write(lt,*)parm(is+1),delparm(is+1),param(is+1)
                write(lt,*)parm(is+2),delparm(is+2),param(is+2)
                write(lt,*)parm(is+3),delparm(is+3),param(is+3)
                if(noppsys.gt.3) then
                  write(li,*)parm(is+4),delparm(is+4),param(is+4)
                  write(lt,*)parm(is+4),delparm(is+4),param(is+4)
                end if
              end do
              write(li,1287)
              write(lt,1287)
1287          format(/' Derivatives at each channel'/)
              write(li,1289) (f(ij,ip),ij=1,if)
              write(lt,1289) (f(ij,ip),ij=1,if)
1289          format(4x,12e9.2)
             end if
            end if

*         end of main loop
          end if
        end do
        if ( np .gt. nvdots ) then
          write (6,*) ' '
        end if
*
*       RFC 18.10.05 Data loop using new 2D variables, with rms
        if(lrms) then
          k=0
          do ichk=1,nchunk
            lkoffs=kchstrt(ichk)-1
            nchk=kchend(ichk)-lkoffs
            do jchk=1,nchk
              k=k+1
*             exclude negative errors
              kloc=jchk+lkoffs
*   BUG FIX   if(derch(jchk,ichk).gt.0.0d0) then becomes (JAK 23/10/09)
              if(derch(kloc,ichk).gt.0.0d0) then
                diff=(phi(jchk,ichk)-dach(kloc,ichk))/
     :                drmch(kloc,ichk)
                do j=1,np
                  grad(j)=grad(j)+diff*f(k,j)
                  do i=1,np
                    hessian(i,j) = hessian(i,j)+ 
     :                   (f(k,i)*f(k,j))/drmch(kloc,ichk)
                  end do
                end do
              end if
            end do
          end do
         else
*         Data loop old style using the photon error array
          k=0
          do ichk=1,nchunk
            lkoffs=kchstrt(ichk)-1
            nchk=kchend(ichk)-lkoffs
            do jchk=1,nchk
              k=k+1
*             exclude negative errors
              kloc=jchk+lkoffs
              if(derch(kloc,ichk).gt.0.0d0) then
                diff=(phi(jchk,ichk)-dach(kloc,ichk))/
     :                derch(kloc,ichk)
                do j=1,np
                  grad(j)=grad(j)+diff*f(k,j)
                  do i=1,np
                    hessian(i,j) = hessian(i,j)+ 
     :                   (f(k,i)*f(k,j))/derch(kloc,ichk)
                  end do
                end do
              end if
            end do
          end do
        end if

* ajc 3-jun-92  add additional terms to the hessian matrix and direction
*               to include constraints from previously measured
*               parameters (in fort.27) 
*               call numod( hessian, grad, param )

c *** pad out rest of Hessian
      do j=2,np
        jj=j-1
        do i=1,jj
          hessian(i,j)=hessian(j,i)
        end do
      end do

 1111 format(4x,(12a9))
      if(idiag.eq.0) goto 933
*     targeted diagnostics
      if(ndbranch.ne.0.and.ndbranch.ne.11) return
*
*
*     diagnostic printout only
      write(lt,9000)
      write(li,9000)
 9000 format(/' Hessian matrix'/)
      if(np.le.12)then
        write(lt,1111)(ipind(l),l=1,np)
        write(li,1111)(ipind(l),l=1,np)
        do i=1,np
          write(lt,2000)ipind(i),(hessian(i,j),j=1,np)
          write(li,2000)ipind(i),(hessian(i,j),j=1,np)
 2000     format(1x,a3,(12e9.2))
        end do
       else
        do i=1,np
          write(li,2001) (hessian(i,j),j=1,np)
          write(lt,2001) (hessian(i,j),j=1,np)
        end do
 2001   format(4x,12e9.2)
      end if
*     compute eigenvalues and eigenvectors
      do i=1,np
        ii=(i-1)*np
        do j=1,np
          aa(ii+j)=hessian(i,j)
        end do
      end do

      call eigen(aa,bb,np)
      do i=1,np
        ff(i)=aa(i+(i-1)*np)
      end do
      write(lt,1300)
      write(li,1300)
 1300 format(/' Eigenvalues'/)
      write(lt,2222) (ff(i),i=1,np)
      write(li,2222) (ff(i),i=1,np)
      write(lt,1500) 
      write(li,1500)
 1500 format(/' Eigenvectors'/)
*
      do k=1,np
        kk=(k-1)*np
        if(np.le.12)then
          write(lt,2223) (bb(i+kk),i=1,np)
          write(li,2223) (bb(i+kk),i=1,np)
         else
          write(li,2224) (bb(i+kk),i=1,np)
          write(lt,2224) (bb(i+kk),i=1,np)
        endif
      end do
*
 2222 format(4x,3e9.2)
 2223 format(4x,(12e9.2))
 2224 format(4x,12e9.2)
 933  continue
      return
      end
