      subroutine vp_readfs(da,de,drms,ca,nc,nct,werr,
     1     norx,cparm,numdat,ichunk)
*
*     routine to take a suitable set of FITS or IRAF files, in the object &
*     error**2 format, and read it in to memory.
*
*     CFITSIO: j-I*4; e-R*4; d-R*8; l-L*4; s-string
*
*     ASCII files tried if standard format data files not available
c
c
c     INPUT:
c     cparm	character	input file name (or blank)
c     nc	integer		(was) array size for da, de, ca
c     cv	character array	command characters
c     ncv	integer		cv array size
c     norx	integer		order number
c     numdat	integer		no. datasets (1=data only,
c                               2=data+error, 3=d+e+continuum)
c     ichunk	integer		current chunk number
c
*     OUTPUT:
*     da	double array	data
*     de	double array	variance (i.e. sigma**2)
*     drms    double array    rms SQUARED
*     dc      double array    continuum
*
*     AS COMMON:
*     wcf1     double array    /vpc_wavl/ wavelength coeffts
*     nwco    integer         /vpc_wavl/   length wavelength coeffts
*
*
      implicit none
      include 'vp_sizes.f'
*
      integer nc,nct,norx,numdat,ichunk
      double precision da(*),de(*),drms(*),ca(*)
      character*(*) cparm
      double precision werr
*
*     Local:
      logical lrmsset
      integer lrps,nsigpl,nrmspl,mconpl
      integer ndim,nrespl
      integer unit1,readwrite,blocksize
      integer ncref
      integer npxstart,lsps
      integer status
*     image size variables
      integer iax(7)
      integer fpixels(2),lpixels(2)
      integer nax
      character*80 cxname,gename
      character*80 chstr,cxsig,cxrms,cxcont,cxres
      character*80 cvx(2),key
      character*30 errmsg
      character*80 rname, dtadir
      character*80 cvxname
      double precision dtemp
*     wavelengths for each pixel center filename:
      character*80 wname,comment
      double precision dvx(2)
      integer ivx(2)
      double precision yscal,yzero
*     double precision sigscl(maxwco)
*     rfc 22.3.95: double length wavelength coeffts
*     single precision sigma scale in case these are in data
*     header
      double precision sgsigscl(maxwco)
      integer nordrs
*     logical vacuum
      logical logvar
*     big enough to take maxosf orders (maxosf set in vp_sizes.f)
*     double precision coeff( maxwco, maxosf )
      logical enameok,rnameok,cnameok
*     local variables:
      logical lundef
      integer imaxd,nv,nvx,nctemp,nerx
      integer last,lastm,ijunk
*     wavelength array pointer and length
*     integer ncw
      double precision scale2,sum,temp,tempx
      double precision demed,timexp,dexptime
*     loop variables:
      integer i,ndot
*     resolution
      double precision rvalue
*     data file name parts:
      character*80 cxbase
      character*8 cxext
      integer lencxb,lencxe
*
*     Functions:
      integer lastchpos
      double precision f_sigscl
*     
*     Common:
*     miscellaneous sizes etc from input spectrum
*     integer ndim,nelm,nord
*     integer ndims(10)
*     common/fginparms/ndim,ndims,nelm,nord
*     wavelength coeffts for current set
      integer nwco
      double precision wcf1(maxwco)
      common/vpc_wavl/wcf1,nwco
*     vac/air wavelengths
      double precision avfac
      common/vpc_airvac/avfac
*
      character*8 wcftype
      character*4 vacind
      double precision helcfac
      integer noffs
      common/c8_wcftype/wcftype,helcfac,noffs,vacind
*
*     last file name, lgflnm=.true. if filename set, false otherwise
      character*80 cflname
      integer lastord
      logical lgflnm
      common/vpc_lastfn/cflname,lastord,lgflnm
*     interactive filename stuff
      integer idrn,icrn
      character*64 filnm
      common/vpc_file14/filnm,idrn,icrn
      logical verbose
      common/vp_sysout/verbose
*     sigma scale rescale for testing purposes
      double precision sigsclx
      common/vpc_sigsclx/sigsclx
*     the individual file scale coeffts
      double precision ssclcf,wvalsc
      integer nscl
      common/sigsclcft/ssclcf(maxtas,maxnch),wvalsc(maxtas,maxnch),
     1     nscl(maxnch)
*     exposure time
      common/vpc_exptime/timexp
      common/cgs4_head/dexptime
*     data scale parameters (lrescale true if data rescaled, scale is
*     number it was multiplied by)
      logical lrescale
      double precision scale
      common/vpc_dscale/scale,lrescale
*     fit to resolution, using sigmas
      integer nswres(maxnch)
      double precision swres(maxpoc,maxnch)
      common/vpc_sigwres/swres,nswres
*     character workspace
      character*132 inchstr
      character*60 cvstr(24)
      integer ivstr(24)
      real rvstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
      double precision dvstr(24)
      common/vpc_dsepspace/dvstr
*     parameter error estimates rescaled, or not? Default is yes
*     or if luvespop is true, then no
      logical lrsparmerr,luvespop
      common/vpc_rsparmerr/lrsparmerr,luvespop
*     
*     number wavelength coeffts used -- set default
      nwco=6
*     scale presets
      yscal=1.0d0
      yzero=0.0d0
*     ndims(1)=0
      imaxd=nc
      cxname=cparm
      wname=' '
      ncref=1
      logvar=.false.
*     UVES_popler variables
      luvespop=.false.
      nsigpl=0
      nrmspl=0
      mconpl=0
      enameok=.false.
*     rms values initially not set
      lrmsset=.false.
*
* 851    continue
      if(cxname(1:1).eq.' ') then
c       request file name
        write(6,*) ' Filename for data?'
        write(6,'(''> '',$)')
        read(5,'(a)') cxname
       else
*       file name is parameter to subroutine
        cxname=cparm
      end if
      filnm=cxname(1:64)
*     Is there a .fits or .imh included in the filename?
      last=lastchpos(cxname)
*     search for the last dot
      ndot=last
      do while(ndot.gt.1.and.cxname(ndot:ndot).ne.'.')
        ndot=ndot-1
      end do
*     write(6,*) cxname(ndot:ndot)
*     default to use full filename
      cxbase=cxname
      lencxb=last
      lencxe=1
      cxext=' '
*     check special cases
      if(last-ndot.eq.4.and.last.gt.5) then
*       could be .fits. Is it?
        if(cxname(last-4:last).eq.'.fits'.or.
     :         cxname(last-4:last).eq.'.FITS') then
          lencxb=last-5
          cxbase=cxname(1:lencxb)
          lencxe=5
          cxext=cxname(last-4:last)
        end if
      end if
      if(last-ndot.eq.3.and.last.gt.4) then
*       could be .imh
        if(cxname(last-3:last).eq.'.imh') then
          lencxb=last-4
          cxbase=cxname(1:lencxb)
          lencxe=4
          cxext='.imh'
        end if
      end if
*     The STATUS parameter must always be initialized.
      status=0
*     Get an unused Logical Unit Number to use to open the FITS file.
      call ftgiou(unit1,status)
*
*     DATA: ------------------------------------------------
*
*     open the FITS file, with read-only access.  The returned BLOCKSIZE
*     parameter is obsolete and is ignored. 
      readwrite=0
      status=0
      cvxname=cxname
*     check that a FITS extension has not been specified
      if(cxname(last:last).eq.']'.and.
     :     cxname(last-2:last-2).eq.'[') then
        write(6,*) 'FITS extension specified'
        norx=1
        lastord=1
        call vp_rfitsext(cxname,last,unit1,ichunk, 
     :      wcf1,nwco,da,de,ca,nct,ndim,status)
*       error check:
        if(status.ne.0) goto 9000
        goto 953
      end if
      if(verbose) write(6,*) 'Trying file ',cvxname(1:32)
      call ftopen(unit1,cvxname,readwrite, blocksize,status)
      if(status.ne.0) then
*       try with a .fits added to the filename
        cvxname=cxbase(1:lencxb)//'.fits'
        status=0
        if(verbose) write(6,*) 'Trying ',cvxname(1:32)
        call ftopen(unit1,cvxname,readwrite, blocksize,status)
        if(status.eq.0) then
*         opened as a fits file
          lencxe=5
          cxext='.fits'
         else
          status=0
          cvxname=cxbase(1:lencxb)//'.imh'
          if(verbose) write(6,*) 'Trying ',cvxname(1:32)
          call ftopen(unit1,cvxname,readwrite, blocksize,status)
          if(status.eq.0) then
            lencxe=4
            cxext='.imh'
          end if
        end if
      end if
*
      if(status.eq.0) then
        if(verbose) then
          write(6,*) ' .. and using that.'
        end if
        cflname=cvxname
        lgflnm=.true.
*       Is it a FITS file with FITS extensions?

*
*       get the image size first:
        call ftgknj(unit1,'NAXIS',1,2,iax,nax,status)
        if(status.ne.0) then
          call ftgerr(status,errmsg)
          write(6,*) ' NAXIS ERROR:',errmsg
          goto 9000
        end if
*       data length
        nct=iax(1)
        if (verbose) then
          write(6,*) 'Array length ',nct
        end if
        if(nax.ge.2) then
*         multidimensional image
          nordrs=iax(2)
          if(nct.lt.nordrs) then
*           is a column stacked image
            nordrs=iax(1)
            nct=iax(2)
          end if
*         see if it is UVES_popler format
          status=0
          call ftgkys(unit1,'UP_ARR01', cxcont,comment,status)
          if(status.eq.0) then
*           UVES_popler format
            luvespop=.true.
            norx=1
           else
*           2-D array of unknown origin, so ask
            if(norx.le.0.or.norx.gt.nordrs) then
*           ask for order number
              norx=(nordrs+1)/2
              lastord=norx
              write(6,*) ' Order number? [',norx,'], max ',nordrs
              write(6,'(''> '',$)')
              read(5,'(a)') chstr
              call dsepvar(chstr,1,dvx,ivx,cvx,nvx)
              if(ivx(1).gt.0.and.ivx(1).le.nordrs) then
                norx=ivx(1)
              end if
            end if
          end if
          lastord=norx
*         read the data
          lundef=.false.
          status=0
          if(nct.eq.iax(1)) then
            npxstart=(norx-1)*iax(1)+1
*           get data as double precision
            call ftgpvd(unit1,0,npxstart,iax(1),0.0d0, 
     :                 da,lundef,status)
           else
*           get a column
            fpixels(1)=norx
            fpixels(2)=norx
            lpixels(1)=(nct-1)*nordrs+norx
            lpixels(2)=lpixels(1)
            write(6,*) iax(1),iax(2),nordrs
            call ftgsvd(unit1,0,nax,iax,fpixels,lpixels,nordrs,
     :          0.0d0,da,lundef,status)
          end if
         else
*         single dimension image
          lundef=.false.
          status=0
          call ftgpvd(unit1,0,1,iax(1),0.0d0, da,lundef,status)
          nordrs=1
          norx=1
          lastord=1
        end if
*       branch out if data read fails
        if(status.ne.0) then
          call ftgerr(status,errmsg)
          write(6,*) 'DATA READ ERROR:',status
          goto 9000
        end if
*
*       WAVELENGTHS: ---------------------------------------------
*     
        call vp_rdwaveval(unit1,cxname,norx,nordrs,nct, status)
*
*       RESOLUTION: ---------------------------------------------- 
*
*       npres=0
        if(luvespop) then
*         UVES_popler resolution array?
          status=0
          call ftgkys(unit1,'UP_ARR10', rname,comment,status)
          if(status.eq.0) then
*           set up to read in the resolution information, and set 
*           variables to make sure it is used. Read in below after
*           data file is closed. [NOT YET WRITTEN: TO COME]
            cxres=cvxname
            nrespl=lastchpos(cxres)
            write(6,*) 'Using resolution file ',cxres(1:nrespl)
            nrespl=10
            goto 7010
           else
            rname=' '
            cxres=' '
          end if
        end if
*       No resolution by pixel information in data file
        status=0
        call ftgkys(unit1,'RESFILE', rname,comment,status)
        if (status.ne.0) then
*         Try for RESVEL
          status=0
          call ftgkyd(unit1,'RESVEL', rvalue,comment,status)
          if (status.ne.0) then
            if(verbose) then
              write (6,*) ' no resolution file - RESFILE in header'
            end if
            nswres(ichunk)=2
            do i=1,maxpoc
              swres(i,ichunk)=0.0d0
            end do
           else
*           get the value for the velocity FWHM
            if(verbose) then
              write(6,*) ' Resolution ',rvalue,' km/s'
            end if
            swres(1,ichunk)=0.0d0
*           store sigma, not FWHM
            swres(2,ichunk)=rvalue/7.05957d5
*                     R(sigma)  =   c*2.35482/rvalue
          end if 
         else
*           Have a resolution file pointer in header
*
*	  the directory may not be the data directory 
*         - assume that database is a
*         subdirectory inside the data directory
          dtadir = ' '
          last = 1
          if ( index( rname, '/' ) .ne. 0 ) then
            last = index( rname, '/' )
            do while ( index( rname( last + 1 : ), '/' ) .ne. 0 )
              last = index( rname( last + 1 : ), '/' ) + last
            end do
            dtadir = ' '//rname( 1 : last )
          end if
          dtadir=dtadir(1:last+1)//'database/'//rname(1:30)
          do while ( dtadir( 1 : 1 ) .eq. ' ' )
            dtadir = dtadir( 2 : )
          end do
          if(verbose) then
            write ( *, * ) ' resolution file : '//dtadir( 1 : 60 )
          end if
*         modify name and get values
          call vp_getres( dtadir, ichunk, norx, status )
        end if
 7010   continue
*
*       VARIOUS HEADER ITEMS: ---------------------------------------
*
*       header keywords:
*       OTIME or EXPOSED for integration time
        status=0
        call ftgkyd(unit1,'OTIME', timexp,comment,status)
        if(status.ne.0) then
          status=0
          call ftgkyd(unit1,'EXPOSED', timexp,comment,status)
        end if
        if(status.ne.0) then
*         set it to a year!
          timexp=3.0d7
        end if
*       CGS4 data header keys for exposure scaling
        status=0
        call ftgkyd(unit1,'DEXPTIME', dexptime,comment,status)
        if(status.ne.0) then
          dexptime=0.0d0
        end if
*       SIGFILE for sigma for data
        status=0
*       sigmas is now the default:
        logvar=.false.
        call ftgkys(unit1,'SIGFILE', cxsig,comment,status)
        if(verbose.and.status.eq.0) then
          write(6,*) 'Error file in header: ',cxsig(1:20)
        end if
        if(status.ne.0) then
          cxsig='    '
          call ftgkys(unit1,'VARFILE', cxsig,comment,status)
          if(status.ne.0) then
            status=0
*           use sigma array (2) from UVES_popler .fits if appropriate
            call ftgkys(unit1,'UP_ARR02', cxsig,comment,status)
            if(status.eq.0) then
              cxsig=cvxname
              nsigpl=2
             else
              cxsig='    '
            end if
           else
            logvar=.true.
          end if
        end if
*       RMSFILE for RMS estimates for the data
        status=0
*       set rmsfile as <datafile>.rms.fits as default
        cxrms='    '
        call ftgkys(unit1,'RMSFILE', cxrms,comment,status)
        if(status.ne.0.and.luvespop) then
          cxrms=cvxname
          nrmspl=3
        end if
*       CONTFILE for continuum
        status=0
        call ftgkys(unit1,'CONTFILE', cxcont,comment,status)
        if(status.ne.0) then
*         if the data is in UVES_popler format, then continuum
*         is unity unless over-ridden here
          status=0
          call ftgkys(unit1,'UP_ARR04', cxcont,comment,status)
          if(status.eq.0) then
            cxcont='unity'
           else
            cxcont='    '
          end if
        end if
        status=0
*
*       got everything, so close out the file
        call ftclos(unit1,status)
*
       else
*
*       Try as an ASCII file of wavelength, data, sigma, continuum
*
        if(verbose) then
          write(6,*) 'Try ASCII read ',cxname(1:20)
        end if
        open(unit=17,file=cxname,status='old',err=9000)
*       store filename and order info (order=1)
        cflname=cxname
        lgflnm=.true.
        nordrs=1
        norx=1
        lastord=1
*       read in the data
        write(6,*) 'ASCII file input'
        call vp_asciin(da,de,drms,ca,nct,wcf1,nwco,ichunk,norx)
        write(6,*) nct,' data values'
        close(unit=17)
        ndim=1
        cxsig='    '
        cxcont='    '
*       ASSUME the data is sensibly scaled, use awk or something to do so
*       if it is not
        scale=1.0d0
*       Just in case there might be a problem, warn user
        temp=0.0d0
        tempx=0.0d0
        do i=1,nct
          if(de(i).gt.0.0d0) then
            temp=temp+de(i)
            tempx=temp+1.0d0
          end if
        end do
        if(tempx.gt.0.0d0) then
          temp=temp/tempx
          if(temp.lt.1.0d-10) then
            write(6,*) 'Mean error is ',temp
            write(6,*) 'Should you have rescaled the data?'
          end if
        end if
*       reset the sigma scale using the global fake value (usually unity)
*       It is assumed that the ASCII sigmas have been 
*       rescaled appropriately
        nscl(ichunk)=1
        ssclcf(1,ichunk)=sigsclx
*       single column dataset, so:
        norx=1
        goto 953
      end if
      status=0
*
*     get root datafile name for later use if necessary
      gename = cxbase
      last = max( index( gename, ' ' ), 2 )
*     check that it does not have a '.ec' extension
      lastm=last
      if(last.gt.4) then
        if(gename(last-3:last-1).eq.'.ec'.or.
     1       gename(last-3:last-1).eq.'.EC') then
*         if there is a .ec, omit it:
          lastm=last-3
        end if
      end if
*     precede by current directory if none
      if ( index( gename, '/' ) .eq. 0 ) then
        gename = './'//gename(1:78)
        lastm = lastm + 2
        last=last+2
      end if
*
*

*     ndims(1)=nct
      if(verbose) then
        write ( *, * ) ' data : ', nct, ' x ', nordrs
      end if

*
*     DATA ERROR ESTIMATES: ---------------------------------------------
*     SIGMAS:
*     read in variances (or errors and convert them)
*     initially no name by default
      if (numdat.gt.1) then
        enameok = .false.
*       first try header
        if(verbose) then
          write(6,*) 'cxsig = ',cxsig(1:20)
        end if
        if(cxsig(1:4).ne.'    ') then
          cxname = cxsig
          status=0
          call ftopen(unit1,cxname,readwrite, blocksize,status)
          if ( status .eq. 0 ) then
            enameok = .true.
           else
            if(luvespop) then
*             use array#2 from the file
              status=0
              call ftopen(unit1,cxname,readwrite, blocksize,status)
              if(status.eq.0) then
                enameok=.true.
              end if
             else
*             try with same extension as the data
              lsps=lastchpos(cxsig)
              cxname=cxsig(1:lsps)//cxext(1:lencxe)
              if(verbose) then
                write(6,*) 'Trying error file ',cxname(1:20)
              end if
              status=0
              call ftopen(unit1,cxname,readwrite, blocksize,status)
              if(status.eq.0) then
                enameok=.true.
               else
                if(verbose) then
                  write(6,*) ' failed with ',cxname(1:50), '...'
                end if
              end if
            end if
          end if
          status = 0

*         if that was unsuccessful, try adding directory of data file
          if (.not. enameok) then
            ijunk = last
            do while ( ijunk .gt. 1 .and. 
     :                 gename( ijunk : ijunk ) .ne. '/' )
              ijunk = ijunk - 1
            end do
            if ( ijunk .gt. 0 ) then
              cxname = gename( 1 : ijunk ) // cxsig
              call ftopen(unit1,cxname, readwrite,blocksize,status)
              if ( status .eq. 0 ) then
                enameok = .true.
              end if
              status = 0
            end if
          end if

*         otherwise point out no sigfile
         else
          if(verbose) write(6,*) ' no SIGFILE in header'
        end if

*       if that was no good, try .var extension, with default extension
        if ( .not. enameok ) then
          cxname = gename( 1 : lastm-1 ) // '.var'//cxext(1:lencxe)
          status=0
          call ftopen(unit1,cxname,readwrite, blocksize,status)
          if ( status .eq. 0 ) then
            enameok = .true.
            logvar=.true.
          end if
          status = 0
        end if

*       if that was no good, try .sig extension
        if ( .not. enameok ) then
          cxname = gename( 1 : lastm-1 ) // '.sig'//cxext(1:lencxe)
          status=0
          call ftopen(unit1,cxname,readwrite,blocksize,status)
          if ( status .eq. 0 ) then
            logvar = .false.
            enameok = .true.
          end if
          status = 0
        end if

*       if that was no good, try .err extension
        if ( .not. enameok ) then
          cxname = gename( 1 : lastm-1 ) // '.err'//cxext(1:lencxe)
          status=0
          call ftopen(unit1,cxname,readwrite,blocksize,status)
          if ( status .eq. 0 ) then
            logvar = .false.
            enameok = .true.
          end if
          status = 0
        end if

*       give up and ask
        if ( .not. enameok ) then
          write(6,*) ' Error FULL name, type (v,def=sig)?'
          write(6,'(''> '',$)')
          read(5,'(a)') cxname
          call dsepvar(cxname,2,dvx,ivx,cvx,nv)
          cxname=cvx(1)
          if(cvx(2)(1:1).eq.'v'.or.cvx(2)(1:1).eq.'V') then
            logvar=.true.
           else
            logvar=.false.
          end if
*         attempt to open given filename
          status=0
          call ftopen(unit1,cxname,readwrite, blocksize,status)
          if ( status .eq. 0 ) then
            logvar = .false.
            enameok = .true.
          end if
          status = 0
        end if

        if(verbose) then
          write(6,*) ' error file : '//cxname
        end if
        if(enameok) then
          if ( nax.ge.2 ) then
*           multidimensional data array
*           read the data
            lundef=.false.
            status=0
            if(luvespop) then
              npxstart=iax(1)+1
             else
              npxstart=(norx-1)*iax(1)+1
            end if
            call ftgpvd(unit1,0,npxstart,iax(1),0.0d0, 
     :                     de,lundef,status)
           else
*           single dimension image
            lundef=.false.
            status=0
            call ftgpvd(unit1,0,1,iax(1),0.0d0, de,lundef,status)
          end if
          status=0
          call ftclos(unit1,status)
*         may need to scale the data -- check by examining the error
*         array median
*         search for the end of the error array first so as not to
*         get a silly result
          nctemp=nct
          do while(de(nctemp).le.0.0d0.and.nctemp.gt.10)
            nctemp=nctemp-1
          end do
          call pr_mdian1(de,ca,nctemp,demed)
          if(demed.le.1.0d-10.and.demed.gt.0.0d0) then
            scale=1.0d15
*           rfc 2.9.98 removed to avoid jumps in scalefactor
*           if(demed.le.1.0e-15) scale=1.0e15
            lrescale=.true.
            do i=1,nct
              da(i)=da(i)*scale
            end do
            if(verbose) then
              write(6,*) ' median error =',demed
              write(6,*) ' data rescaled by factor',scale
            end if
           else
            scale=1.0d0
            lrescale=.false.
          end if
*         check for zero/negative errors
          nerx=0
          do i=1,nct
            if(de(i).le.0.0d0) then
              de(i)=-1.0d15
              nerx=nerx+1
            end if
          end do
          if(nerx.gt.0 .and. verbose) then
*           report on zero errors dealt with
            write(6,'(1x,i6,'' bad pixels'')') nerx
          end if
*
*         rescale data if necessary (with error and rms)
          if(logvar) then
*           rescale data?
            if(lrescale) then
              scale2=scale*scale
              write(6,*) 'Data variance rescale by ',scale2
              do i=1,nct
                if(de(i).gt.0.0d0) then
                  de(i)=de(i)*scale2
                 else
                  de(i)=-1.0d15
                end if
              end do
            end if
           else
            if(verbose) then
              write(6,*) 'Error rescaled by ',scale
            end if
            do i=1,nct
              if(de(i).gt.0.0d0) then
                temp=de(i)*scale
                de(i)=temp*temp
               else
                de(i)=-1.0d15
*               drms(i)=-1.0d15
              end if
            end do
          end if
         else
c         determine the data mean
          write(6,*) 'Set error = sqrt(A*data + B**2)'
          write(6,*) 'Enter A,B [1.0,threshold=0.2]'
          read(5,'(a)') inchstr
          call dsepvar(inchstr,2,dvstr,ivstr,cvstr,nvstr)
          if(nvstr.ge.2) then
            temp=dvstr(2)**2
            sum=dvstr(1)
            do i=1,nct
              de(i)=sum*max(da(i),0.0d0)+temp
            end do
           else
            sum=0.0d0
            do i=1,nct
              sum=sum+da(i)
            end do
            sum=0.2d0*sum/dble(nct)
            if(sum.le.0.0d0) sum=1.0d0
            do i=1,nct
              if(da(i).gt.sum) then
                de(i)=da(i)
               else
                de(i)=sum
              end if
            end do
            write(6,*) 'Assumed error=sqrt(max(data,0.2*meandata))'
          end if
        end if
      end if
*
*     RMS: ----------------------------------------------------------
*     read in the rms estimates (if present)
      if(numdat.gt.1) then
        rnameok=.false.
*       if there is a header-based name, use it
        if(cxrms(1:4).ne.'    ') then
          if(verbose) then
            write(6,*) 'RMS file = ',cxrms(1:20)
          end if
          cxname=cxrms
          status=0
          call ftopen(unit1,cxname,readwrite, blocksize,status)
          if(status.eq.0) then
            rnameok=.true.
           else
            if(luvespop) then
*             use array#3 from the file
              status=0
              call ftopen(unit1,cxname,readwrite, blocksize,status)
              if(status.eq.0) then
                rnameok=.true.
              end if
             else
*             try with same extension as the data
              lrps=lastchpos(cxrms)
              cxname=cxrms(1:lrps)//cxext(1:lencxe)
              if(verbose) then
                write(6,*) 'Trying RMS file ',cxname(1:20)
              end if
              status=0
              call ftopen(unit1,cxname,readwrite, blocksize,status)
              if(status.eq.0) then
                rnameok=.true.
               else
                if(verbose) then
                  write(6,*) ' failed with ',cxname(1:50), '...'
                end if
              end if
            end if
          end if
          status = 0
*         if that was unsuccessful, try adding directory of data file
          if (.not. enameok) then
            ijunk = last
            do while ( ijunk .gt. 1 .and. 
     :                 gename( ijunk : ijunk ) .ne. '/' )
              ijunk = ijunk - 1
            end do
            if ( ijunk .gt. 0 ) then
              cxname = gename( 1 : ijunk ) // cxrms
              call ftopen(unit1,cxname, readwrite,blocksize,status)
              if ( status .eq. 0 ) then
                rnameok = .true.
              end if
              status = 0
            end if
          end if
*         otherwise point out no rmsfile
         else
          if(verbose) write(6,*) ' no RMSFILE in header'
        end if
*       if that was no good, try .rms extension
        if ( .not. rnameok ) then
          cxname = gename( 1 : lastm-1 ) // '.rms'//cxext(1:lencxe)
          status=0
          call ftopen(unit1,cxname,readwrite,blocksize,status)
          if ( status .eq. 0 ) then
            if(verbose) then
              ijunk=lastchpos(cxname)
              write(6,*) 'RMS from ',cxname(1:ijunk)
            end if
            rnameok = .true.
          end if
          status = 0
        end if
        if(rnameok) then
          if ( nax.ge.2 ) then
*           multidimensional data array
*           read the data
            lundef=.false.
            status=0
            if(luvespop) then
              npxstart=2*iax(1)+1
             else
              npxstart=(norx-1)*iax(1)+1
            end if
            call ftgpvd(unit1,0,npxstart,iax(1),0.0d0, 
     :                           drms,lundef,status)
            if(status.eq.0) lrmsset=.true.
           else
*           single dimension image
            lundef=.false.
            status=0
            call ftgpvd(unit1,0,1,iax(1),0.0d0, drms,lundef,status)
            if(status.eq.0) lrmsset=.true.
          endif
          status=0
          call ftclos(unit1,status)
*         If de was rescaled, then do the same to drms
          if(logvar) then
            if(lrescale) then
              do i=1,nct
                drms(i)=drms(i)*scale2*drms(i)
              end do
            end if
           else
            do i=1,nct
              if(de(i).gt.0.0d0) then
                temp=drms(i)*scale
                drms(i)=temp*temp
               else
                drms(i)=-1.0d15
              end if
            end do
          end if
        end if
      end if
*
*     need to set up drms from de if it was not done above, this uses
*     wavelengths and sigma scale stuff, so is done after sigma scale
*     read in
*
*     wavelength error
      werr=0.0d0
*
*     CONTINUUM: --------------------------------------------------

*     read in continuum
*     initially no name
      cnameok = .false.
      if(numdat.gt.2) then
*
*       first try header
        if(cxcont(1:4).ne.'    ') then
          if(cxcont(1:5).eq.'unity'.or.cxcont(1:5).eq.'UNITY') then
*           set up for unit continuum
            cxcont='unity'
            cnameok=.true.
            cxname=cxcont
           else
            cxname = cxcont
            status=0
            call ftopen(unit1,cxname,readwrite, blocksize,status)
            if ( status .eq. 0 ) then
              cnameok = .true.
             else
*             try with same extension as the data
              lsps=lastchpos(cxcont)
              cxname=cxcont(1:lsps)//cxext(1:lencxe)
              if(verbose) write(6,*) 'Trying continuum file ',
     :                         cxname(1:20)
              status=0
              call ftopen(unit1,cxname,readwrite, blocksize,status)
              if(status.eq.0) then
                cnameok=.true.
               else
                if(verbose) then
                  write(6,*) ' failed with ',cxname(1:50), '...'
                end if
              end if
            end if
          end if
          status=0
*         if that was unsuccessful, try adding directory of data file
          if ( .not. cnameok ) then
            ijunk = last
            do while ( ijunk .gt. 1 .and. 
     :                 gename( ijunk : ijunk ) .ne. '/' )
              ijunk = ijunk - 1
            end do
            if ( ijunk .gt. 0 ) then
              cxname = gename( 1 : ijunk ) // cxcont
              status=0
              call ftopen(unit1,cxname,readwrite, blocksize,status)
              if ( status .eq. 0 ) then
                cnameok = .true.
              end if
              status = 0
            end if
          end if

*         otherwise point out no contfile
         else
          if(verbose) then
            write(6,*) ' no CONTFILE in header'
          end if
        end if

*       if that was no good, try .cont extension
        if ( .not. cnameok ) then
          cxname = gename( 1 : lastm-1 ) // '.cont'//cxext(1:lencxe)
          status=0
          call ftopen(unit1,cxname,readwrite, blocksize,status)
          if ( status .eq. 0 ) then
            cnameok = .true.
          end if
          status = 0
        end if

*       give up and ask
        if ( .not. cnameok ) then
          write(6,*) ' full continuum name?'
          write(6,'(''> '',$)')
          read(5,'(a)') cxname
          call dsepvar(cxname,2,dvx,ivx,cvx,nv)
          cxname=cvx(1)
          status=0
          call ftopen(unit1,cxname,readwrite, blocksize,status)
          if(status.eq.0) then
            cnameok=.true.
           else
            status=0
          end if
        end if
*
        if(verbose) then
          write (6,*) ' continuum file : '//cxname
        end if

*
        if(cnameok.and.cxname(1:5).ne.'unity') then
          if ( nax.ge.2 ) then
*           multidimensional data array
*           read the data
            lundef=.false.
            status=0
            npxstart=(norx-1)*iax(1)+1
            call ftgpvd(unit1,0,npxstart,iax(1),0.0d0, 
     :           ca,lundef,status)
           else
*           single dimension image
            lundef=.false.
            status=0
            call ftgpvd(unit1,0,1,iax(1),0.0d0, ca,lundef,status)
          end if
*         close the file
          status=0
          call ftclos(unit1,status)
*
*         scale continuum if necessary
          if(lrescale) then
            if(verbose) then
              write(6,*) 'Continuum rescaled by ',scale
            end if
            do i=1,nct
              ca(i)=ca(i)*scale
            end do
          end if
         else
*         By entering a number can set the continuum level.
          call dsepvar(cxname,1,dvx,ivx,cvx,nvx)
          if(dvx(1).gt.0.0d0) then
            tempx=dvx(1)
           else
            tempx=1.0d0
          end if
          if(dvx(1).gt.-10.0d0) then
            write(6,'(a,f7.3,a)') 
     1           'Continuum set to ',tempx,'  everywhere'
            do i=1,nct
              ca(i)=tempx
            end do
           else
*           undocumented: ignore continuum if number is < -10
            write(6,*) 'Continuum not read'
          end if
        end if
      end if
*
*
*     SIGMA SCALE:
*
*     sigma scale coefficients vs wavelength
*     need to get header item pointing to this...
      cxname = gename( 1 : last-1 )//cxext(1:lencxe)
      write ( key( 6 : 9 ), '(i4)' ) 1000 + norx
      do i = 1, 3
        if ( key( 7 : 7 ) .eq. '0' ) key( 7 : 9 ) = key( 8 : 10 )
      end do
      key( 1 : 6 ) = 'SIGSCL'
      status=0
      call ftopen(unit1,cxname,readwrite, blocksize,status)
      call vp_gsclfts(unit1,cxname,nordrs,norx,ichunk,status)
      if(status.ne.0) then
        status=0
        call ftgkyd(unit1,key, sgsigscl(1),comment,status)
        if ( status .ne. 0 ) then
          if(verbose) write ( *, * ) ' SIGSCL not in header'
          status = 0
*         sigscl( 1 ) = 1.0d0
          ssclcf(1,ichunk)=sigsclx
          wvalsc(1,ichunk)=0.0d0
          nscl(ichunk)=1
         else
*         sigscl(1)=dble(sgsigscl(1))
          ssclcf(1,ichunk)=sgsigscl(1)
          wvalsc(1,ichunk)=0.0d0
          nscl(ichunk)=1
        end if
      end if
*
      status=0
      call ftclos(unit1,status)
      call ftfiou(unit1,status)
      status=0
*
*     FLUCTUATION array check
*     drms from de if not already set
      if(.not.lrmsset) then
        write(6,*) 'Using rescaled error array for rms'
*       current wavelength scale coefficients were set above, so
*       can use f_sigscl
        do i=1,nct
          dtemp=f_sigscl(i,ichunk)
          if(dtemp.le.0.0d0) dtemp=1.0d0
          drms(i)=de(i)/(dtemp*dtemp)
        end do
      end if
 911  if(ndim.le.0) ndim=1
*     
*     reset the sigma scale using the global fake value (usually unity)
*     do ii=1,nscl(ichunk)
*       ssclcf(ii,ichunk)=dble(sigsclx)*ssclcf(ii,ichunk)
*     end do
*
 953  return
*     bailout if file does not exist
 9000 nct=0
      write(6,*) ' No data'
      goto 911
      end
