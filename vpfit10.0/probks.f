      double precision function probks(drn)
*     K-S probability given drn (D*sqrt(N))
*     Based initially on the Press et al routine, but had to
*     be modified considerably because the Press one failed to account
*     for all possible ranges of the value
      implicit none
      double precision drn
*     Local
      integer j
      double precision a2,fac,termbf,term,xint
*     Functions
      double precision dexpf
*
      a2=-2.0d0*drn**2
      fac=2.0d0
      probks=0.0d0
      termbf=0.0d0
      term=1.0d0
      j=0
      do while(j.le.100.and.abs(term).ge.0.001d0*termbf.and.
     :       abs(term).ge.1.0d-7*probks.and.term.ne.0.0d0)
        j=j+1
        xint=dble(j*j)
        term=fac*dexpf(a2*xint)
        probks=probks+term
        fac=-fac
        termbf=abs(term)
      end do 
*     check if it failed to converge, which happens for small drn
      if(j.gt.100) then
        probks=1.d0
      end if
      return
      end
