      subroutine svdfit(x,y,sig,ndata,a,ma,u,v,w,mp,np,chisq,funcs)

      implicit none
      
      include 'vp_sizes.f'

      integer i, j
      double precision chisq
      external funcs
      integer nmax, mmax
      double precision tol
      integer ndata, ma, mp, np
      double precision tmp, thresh, sum, wmax

      parameter(nmax=maxmas,mmax=maxmas,tol=1.d-30)
      
      double precision x(ndata),y(ndata),sig(ndata),a(ma),v(np,np),
     :    u(mp,np),w(np),b(nmax),afunc(mmax)

      do i=1,ndata
        call funcs(x(i),afunc,ma)
        tmp=1.d0/sig(i)
        do j=1,ma
          u(i,j)=afunc(j)*tmp
        end do
        b(i)=y(i)*tmp
      end do
      call svdcmpd(u,ndata,ma,mp,np,w,v)
      wmax=0d0
      do j=1,ma
        if(w(j).gt.wmax)wmax=w(j)
      end do
      thresh=tol*wmax
      do j=1,ma
        if(w(j).lt.thresh) w(j)=0d0
      end do
      call svbksbd(u,w,v,ndata,ma,mp,np,b,a)
      chisq=0.0d0
      do i=1,ndata
        call funcs(x(i),afunc,ma)
        sum=0d0
        do j=1,ma
          sum=sum+a(j)*afunc(j)
        end do
        chisq=chisq+((y(i)-sum)/sig(i))**2
      end do
      return
      end
