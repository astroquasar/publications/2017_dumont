      subroutine vpfit
*     Voigt profile fit program
*     Copyright (C) 2009 R.F.Carswell, A.J.Cooke, M.J.Irwin, J.K.Webb
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     For a copy of the GNU General Public License see the information
*     which comes e.g. with the GNU Emacs editor, or write to the Free
*     Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
*
*     Contact: R.F.Carswell 
*         Institute of Astronomy, Madingley Rd, Cambridge CB30HA, UK
*         Internet: rfc(at)ast.cam.ac.uk


*     vpfit with constraints from other hessian matrices
*     Version 9.1: variable length parameter sets
*     uses chunks rather than the whole array
*     subpixels so that unresolved lines OK
*     double precision everything except PGPLOT
*     removed low frequency noise stuff..sigscale file can do same thing
*
*
      implicit none
*     array sizes file
      include 'vp_sizes.f'

*     purely local variables:
      integer i,ic,ip,ilk,ibase,ichst,idiag,indcf
      integer ind,ios,istat,ialfind
      integer icontflg,itercold,nptscold,ndftotcold
      integer ji,jop,jopv
      integer ngoes,nopth,num,nnmod,nlin,nsys,nfile
      integer nnold,nnvold,nnoldh,nlinesold,nitnxx,ntem
      integer op2it
      integer*4 ndch(maxnch)
      double precision chisqvcold
      double precision difmax,temp
      double precision dtemp
*
*     Cycle held variables
      logical lguess,lcpend
      character*2 ipindold(maxnpa)
      character*2 ionold(maxnio)
      character*4 levelold(maxnio)
      integer linchkold(maxnio)
      integer if18
*
*     FUNCTIONS:
      integer lastchpos
*
*     COMMON:
      character*2 cans,cop3ans
*     parameter list
      integer nlines,nn
      character*2 ion(maxnio)
      character*4 level(maxnio)
      double precision parm(maxnpa)
      common/vpc_parry/parm,ion,level,nlines,nn
      character*2 ipind(maxnpa)
      integer isod,isodh
      common/vpc_usoind/ipind,isod,isodh
*     chunk data arrays:
*      double precision data(maxchs,maxnch),error(maxchs,maxnch),
*     :         contin(maxchs,maxnch)
*     data file arrays for work:
      double precision fitret(maxchs,maxnch)
*
*     ks test probability for chunk
      double precision prks( maxnch )
*     total number of vp_ucoptv calls
      integer totit
*
      integer ndp(maxnfi)
*     parameter errors
      double precision parerr(maxnpa)
*     old values for looping and error rejection checks
      double precision parmold(maxnpa),parmvold(maxnpa)
      double precision parmoldh(maxnpa)
      double precision parerrold(maxnpa)
      double precision atmassold(maxnio),vturbold(maxnio)
      double precision temperold(maxnio),fixedbthold(maxnio)
*     double precision rstwav(maxats),z(maxats)
*     Date and time
*     character*10 chdate,chtime
*
*     parameter placement variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     chunk variables:
      integer ndpts(maxnch),idrun(maxnch),icrun(maxnch)
      integer indfil(maxnch)
      character*64 filename(maxnch)
      double precision wvstrt(maxnch),wvend(maxnch)
      integer nchunk
      common/vpc_chunk/wvstrt,wvend,ndpts,filename,
     :      idrun,icrun,indfil,nchunk
*
*     free input declarations
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
*
*     Variables which apply to fitted chunks of spectrum
*     wavelength coefficients or array
      integer ngd,npexsm
      integer numpts(maxnch)
      double precision wcoeff(maxnch)
      common/vpc_jkw3/wcoeff,ngd,npexsm,numpts
*
      double precision sepvd(maxnch)
      common/vpc_sepvd/sepvd
*     general wavelength coeffts
*     wavelength coeffts for each set. Note that linear store is
*     (1,1)(1,2)(1,3)..(1,6)(2,1)(2,2)... so this ordering is more 
*     efficient for accessing the array.
      double precision wcfd(maxwco,maxnch)
      common/vpc_wcfd/wcfd
*
*     chunk channel limits in data
      integer ichstrt(maxnch),ichend(maxnch)
      common/jkw4/ichstrt,ichend
*     common/vpc_vpder/errlot(maxnpo),datlot(maxnpo)
      integer ipgopen,ipgflag
      common/pgplotv/ipgopen,ipgflag
*     log or linear variable indicator,1 for logN, 0 for linear
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
*     wavelength coefficients/array etc. NOT USED IN THIS ROUTINE
*      integer nwco
*      double precision wcf1(maxwco)
*      common/vpc_wavl/wcf1,nwco
*
      character*8 wcftype
      character*4 vacind
      double precision helcfac
      integer*4 noffs
      common/c8_wcftype/wcftype,helcfac,noffs,vacind
*
*     rfc 21.6.95: wavelength parameter sets
      character*8 wcfty2(maxnch)
      character*4 vacin2(maxnch)
      double precision helcf2(maxnch)
      integer nwcf2(maxnch),noffs2(maxnch)
      common/vpc_wcpars2/wcfty2,helcf2,nwcf2,noffs2,vacin2
      double precision dnshft2(maxnch)
      common/vpc_shft2/dnshft2
*
      double precision vturb,atmass,temper,fixedbth
      common/vpc_ucopder/vturb(maxnio),atmass(maxnio),
     :                 temper(maxnio),fixedbth(maxnio)
*
*     current 1-D spectrum data, err, fluctuations, continuum -length ngp
      integer ngp
      double precision dhdata(maxfis),dherr(maxfis)
      double precision dhrms(maxfis),dhcont(maxfis)
      common/vpc_su1d/dhdata,dherr,dhrms,dhcont,ngp
*     current original continuum
      double precision dhccont(maxfis)
      common/vpc_su1dcont/dhccont
*     New 2D dataset variables 
      integer numchunk,nchchtot ! # of chunks,total # data points
      integer kchstrt(maxnch),kchend(maxnch),nchlen(maxnch)
      common/vpc_chunkvar/kchstrt,kchend,nchlen,numchunk,nchchtot
      double precision dach(maxchs,maxnch),derch(maxchs,maxnch)
      double precision drmch(maxchs,maxnch),dcch(maxchs,maxnch)
      common/vpc_datch/dach,derch,drmch,dcch
*     Old 2D dataset
*      real datss(maxfis,maxnfi),derss(maxfis,maxnfi)
*      real dconss(maxfis,maxnfi)
*      real origcntss(maxfis,maxnfi)
*      common/vpc_su2d/datss,derss,dconss,origcntss
*     current data file and order number (now in subroutines)
*      integer idrn,icrn
*      character*64 filnm
*      common/vpc_file14/filnm,idrn,icrn
*     other rest wavelength stuff
*     Fine structure & me/mp:  lqmucf if on; lchvsqmu true for region stats
      logical lqmucf,lchvsqmu
      double precision qscale
      double precision qmucf
      common/vpc_qmucf/qmucf(maxats),qscale,lqmucf,lchvsqmu
*     dropped systems
      integer ndrop,kdrop,ndroptot,ndrwhy
      common/vpc_nrejs/ndrop,kdrop(maxnio),ndroptot,ndrwhy
*     end rejection by column density threshold and error
      double precision clnemin,errlnemax
      common/vpc_endrej/clnemin,errlnemax
*     added systems block
      integer nladded
      double precision chisqvh,probchb,probksb
      common/vpc_problims/chisqvh,probchb,probksb,nladded
*     rejected by error criterion block
      double precision errbmax,errlnmax
      common/vpc_rejsys/errbmax,errlnmax
*     old file name
      integer irunold
      character*64 oldfilnm
      common/vpc_oldfilnm/oldfilnm,irunold
*     ajc 25-oct-93 rejection limit for svd
      double precision rlim
      common /vpc_svd / rlim
*     rfc 8.jan.97 option number for vp_chkion
      integer nopt
      common/vpc_option/nopt
*     input streams
      integer inputc
      common/rdc_inputc/inputc
*     printout control variables
      character*132 p13file
      common/vpc_p13out/p13file
*     Wavelength shifts, held in:
      integer lassoc
      common/vpc_asschnk/lassoc(maxnch)
*     old data file and parameters (blank if not used), including
*     accumulate flag lcuminc (07.03.01)
      integer ncumset
      character*132 comchstr
      logical ldate26
      logical lcuminc
      common/vpc_comchstr/comchstr,ldate26,lcuminc,ncumset
*     general control variables
      character*4 chcv(10)
      common/vpc_chcv/chcv
*     max number of add/remove lines iterations, bval adjust preprof.
      integer maxadrit
      logical lbadj
      common/vpc_maxits/maxadrit,lbadj
*     iteration and statistics for fit
      double precision chisqvc,prtotc
      integer iterc,nptsc,ndftotc
      common/vpc_smry/chisqvc,prtotc,iterc,nptsc,ndftotc
      integer linchk( maxnio )
      common/vpc_linchk/linchk
*     end statistics
      double precision prtot
      logical literok
      common/vpc_endstat/prtot,literok
      logical literokold
      double precision prtotcold
      common/vpc_oldendstat/prtotcold,literokold
*     Control for stopping add/remove iterations
      logical lstopit
      common/vpc_stopit/lstopit
*     summary file info
      logical lwr26s,lwr26open
      character*60 cwr26s
      integer nwr26s,ndp26s,len26tem
      common/vpc_f26hout/cwr26s,nwr26s,lwr26s,lwr26open,ndp26s,
     :     len26tem
      logical verbose
      common/vp_sysout/verbose
*     rejection of ill-constrained variables
      logical nasty
      integer knasty
      common/vpc_nasty/knasty,nasty
*     output channels:
      integer lt(3)             ! output channels for printing
      integer nopchan,nopchanh,nmonitor
      common/vpc_nopchan/lt,nopchan,nopchanh,nmonitor
*     last added ion
      character*2 lastaddion,chaddion
      common/vpc_lastaddion/lastaddion,chaddion
*     version number
      character*8 chvpvnum
      common/vpc_vpvnum/chvpvnum
*     debugging variables -- these will change
*     VP_RDSPECIAL has same common block, and ndbranch reset there
      integer ndbranch
      common/vpc_debug1/ndbranch
*
*     I/O units used:
*     1:
*     2:
*     3:
*     4:
*     5: Standard input
*     6: Standard output
*
*     11: df/dlam output [vp_flchderivs]
*     12:
*     13: Initial guess file input
*
*     18: Detailed output
*
*     Header info
      chvpvnum='v10.0'
      ndbranch=lastchpos(chvpvnum)
      write(6,*)
      write(6,*) 'VPFIT ',chvpvnum(2:ndbranch)
      write(6,*) ' '
*     Started:  15 Oct 2010
*     File creation dates give subsequent updates
*
*     Get source path. This is set up when vpfit is compiled, not
*     here - all this does is set a common block character string
*     for which the factory-supplied default is blank
      call vp_srcpath
*
      ndbranch=0
      ncumset=0
*     Preamble routines:
      call vp_usdef 
      call vp_startval
*     plot defaults
      call pldef(maxchs,'vpfit')
*     character ranges from ichar:
      call vp_charlims
*
*     wavelength coefficients polynomial refers to wavelengths vs
*     channel as the default
      wcftype='waveleng'
*     dropped system limits set so nothing dropped by default
      ndrwhy=0
      errbmax=1.0d35
      errlnmax=1.0d35
      clnemin=0.0d0
      errlnemax=1.0d35
      difmax=0.0d0
*     default data file name
      p13file=' '
      oldfilnm='   '
      irunold=0
      rlim = -1.0d0
*     Default to no diagnostics
      idiag=0
*     number of loops
      ngoes=1
*     Local presets
      nlinesold=0
*     preset old parameter files to anything - zero will do
      do i=1,maxnpa
        parmold(i)=0.0d0
        parmvold(i)=-1.0d0
      end do
*
*     Various parameters for odd preset options. Parameter is
*     printout flag
      call vp_rdspecial(2)
*
*     option number preset is equivalent to undefined
 7234 nopth=-1
*     isod is a flag for UDCHOLE to add 1 to Hessian diagonal
*     terms where ill conditioning occurs. isod = 0 is default.
      isod=0
      ngp=maxfis
*     Get the atomic data, down here since old list might have been truncated:
      call vp_ewred(0)
*
      num=1
      nchunk=0
      indcf=0
      icontflg=0
*     number data points for previous datasets
      do i=1,maxnfi
        ndp(i)=0
      end do
*     loop variables presets (should never be used)
      op2it=1
      itercold=0
      chisqvcold=1.0d20
      nptscold=0
      ndftotcold=0
*     for013 input indicator
      if18=0
*     nlines will be the total no. of lines used for the fit
      nlines=0
*     and nn the total number of variables
*     rfc 28.10.04 No variables per system 3 -> noppsys
      nn=nlines*noppsys
*     get constraints from fort.27 and fort.28 into common block
*      call vp_rdcons

*     Zero turbulent velocities:
      do ilk=1,maxnio
        vturb(ilk)=0.0d0
      end do
      nasty=.false.
      knasty=0
*     reset number of files:
      nfile=1
*     zero total iteration counter
      totit=0
*     zero wavelength shift indicators:
      do i=1,maxnch
        lassoc(i)=0
      end do
*     reset acceptance flag
      literok=.false.
      literokold=.false.
      lcpend=.false.
      lstopit=.false.
*
      if(verbose) then
*       write out a range of variables, to terminal (6) and tempresets.txt
        ios=0
        open(unit=18,file='tempresets.txt',iostat=ios)
        if(ios.eq.0) then
          do jopv=1,2
            jop=12*jopv-6
            write(jop,*) 'Presets:'
            write(jop,*) 'nlines:',nlines
            write(jop,*) 'nlinesold:',nlinesold
            write(jop,*) 'knasty:',knasty,'   nasty:',nasty
          end do
        end if
        close(unit=18)
      end if
*
*     request option
 1068 call vp_setoptn(nopt,nn,icontflg,nopth,indcf,ind,idiag,
     :            ialfind)
*
*     Option chosen carried as value 'nopt' (/vpc_option)
*     1: G: guess line parameters and then fit, interactive
*     2: S: Simulation input, multiple guess
*     3: L: estimate line parameters as HI and run from file region list
*     4: C: leave only continuum and zero level parameters free
*     5: Reserved [Resolve and finish option, when written?]
*     6: I: interactive setup and fit
*     7: F: run from an input file
*     8: Continue as before
*     9: D: display profiles from input file without fitting
*     10:E: display profiles and compute errors from input file
*
*     Skip interactive section if input file used
 1235 if(nopt.eq.1.or.nopt.eq.6) then
*       nopt=1 or 6 so
*       interactive setup of files, wavelength regions and parameters
        call vp_setintv(nopt,lguess,icontflg,indcf,nfile,
     :         dhccont,ndp)
*       Write fort.13 if interactive startup or internal guesses
        call vp_fgf13write
*       Go on to fit or set up new start values
        write(6,*)' Continue (c), fit (f), or stop (s)? [f]'
        write(6,'(''> '',$)')
        read(5,'(a)') cop3ans
        if(cop3ans.eq.'s') stop
        if(cop3ans.eq.'c') then
          write(6,*) ' Change file - 0 , Same file - 1'
          write(6,*) ' Which one? [0]'
          read(5,'(a)') inchstr
          call sepvar(inchstr,1,rvstr,ivstr,cvstr,nvstr)
          icontflg=ivstr(1)
          goto 1235
         else
*	  bypass 'if' statements which will not be satisfied
          goto 7236
        end if
       else
*       Parameters from file (fort.13 as default), no of separate fits
*       get filename [code for this shifted to subroutine in this file]
        call vp_getf13name(p13file,ngoes)
*       set multi iteration counter
        op2it=1
      end if

*     ajc 9-nov-91  option 9 added
 832  if(nopt.eq.2.or.nopt.eq.3.or.nopt.eq.4.or.nopt.eq.7.or.
     1         nopt.eq.9.or.nopt.eq.10) then
*       initialize wavelength shifts
        do i=1,maxnch
          dnshft2(i)=0.0d0
        end do
*       read region and line data from a file
*	NOTE: linchk array - chunk for line - in common
*	is only available via fort.13 option
        call vp_f13read(if18,nopt,nn,ndp,parm,ion,level)
        if(nn.le.0) then
*	  no data read, so branch to the end somewhere
*	  or, better still, guess -- for which a dataset must be chosen
          goto 9132
        end if
      end if
*     nopt=2: start with file and end with interactive input
      if(nopt.eq.2) then
        write(6,*) 'Option 2 not implemented'
*       need to display what has gone in, and go to a new file
*       was never implemented, since rdgen provides a better method
        goto 1068
      end if
*
*     get extra preset ion list, if appropriate
      call vp_presetsys
*     trim the atomic data table
      call vp_trimattab
*     remove linear mode column density sums if doing just errors 
*     or display, and modify root name to add 'E' anyway
      call vp_sumlinclear(nopt)
*     set variables for internal loop checks
 7236 ndroptot=0
      chisqvh=1.0d37
      nnold=-1
      nnvold=-1
*     iteration counter
      nitnxx=0
*     held variables
      nnoldh=0
*     reset acceptance flag
      literok=.false.
      lcpend=.false.
*
*     Forces tying before fit
*     Set up correct guesses for ions with tied b parameters using 
*     associated free b and turbulent component estimate
*     this routine also associates masses with ions, which are used by
*     vp_tsetxref so it has to come later.
      call vp_settied(nopt)
*     set cross-reference array for tied variables
*     rfc 28.10.04: Number 3 -> noppsys (nn = nlines*noppsys anyway)
      nlines=nn/noppsys
*     rfc 18.02.09 first two parameters (ion,level,) removed - not used
      call vp_tsetxref(nlines,parm,ipind,nn,.true.)
*
*     check scaling of normalization variables for emission lines
      call vp_emscchk(parm,nn)
*
*     Form a loop for the number of chunks that want fitting
 1236 lastaddion='  '
      chaddion='  '
*
*     Copy the various data quantities to chunk quantities
      do i=1,nchunk
*       ndch(i) is the no. of chans over which the current fit 
*	    is to be done
*
        ndch(i)=ichend(i)-ichstrt(i)+1
        numpts(i)=ndch(i)+(2*npexsm)
        ichst=ichstrt(i)-npexsm
        ibase=ichst-1
        wcoeff(i)=wcfd(1,i)
        sepvd(i)=wcfd(2,i)
*	set region wavelength parameters:
        dtemp=dble(ibase)
        dnshft2(i)=dtemp
*
*	Check that we are not outside the data window:
        if(ichst.le.0) then
          write(6,*)' Region extends below of start of data:'
          write(6,*) 'Chunk ',i,' Start fit chan. = ',ichstrt(i)
        endif
        if(ichend(i)+npexsm.ge.ndpts(i)) then
          write(6,*)' Region extends longwards of end of data:'
          write(6,*) 'Chunk ',i,' :'
          write(6,*) 'End fit chan= ',ichend(i),' End data=',ndpts(i)
        endif
      end do

      write(6,*)' no. of ions for fitting is ',nn/noppsys
      write(6,*)' '
*     set number of lines for option 9
      if ( nopt .eq. 9 .or. nopt.eq.10 ) nlines = nn/noppsys
      if(indvar.eq.1.and.nopt.ne.9.and.nopchan.ge.2) 
     :                write(18,*) ' Log N given'

*	
      totit = totit + 1
*
*     Wavelength shift parameters
      call vp_wverass
*
*     Initial calculations on constraints:
*     call vp_precon( nn, ipind )
*
      ndrwhy=0
*     The main routine:
*     call ucoptv( data, error, contin, fitret, ndch,  ! replaced by:
      call vp_ucoptv( fitret, ndch,  
     :         parerr,idiag,nchunk,nopt,ialfind,prks,totit)
*     check if systems dropped or added
      nlin=nn/noppsys
*     if there is a satisfactory fit, and want to remove lines
*     and try again, copy everything first
*     unless there is a threshold dropped system (ndrwhy.ne.0)
      if(prtot.gt.probchb.and.clnemin.gt.1.0d0.and.
     :         errlnemax.lt.1d20.and.ndrwhy.eq.0) then
*	check you don't want to give up!
        if(lstopit) goto 7777
        lcpend=.true.
        nnoldh=nn-nladded
        if(nnoldh.le.0) nnoldh=noppsys
        do i=1,nnoldh
          parmoldh(i)=parm(i)
          ipindold(i)=ipind(i)
          parerrold(i)=parerr(i)
        end do
        nlinesold=nnoldh/noppsys
        do i=1,nlinesold
          ionold(i)=ion(i)
          levelold(i)=level(i)
          vturbold(i)=vturb(i)
          atmassold(i)=atmass(i)
          temperold(i)=temper(i)
          fixedbthold(i)=fixedbth(i)
          linchkold(i)=linchk(i)
        end do
        itercold=iterc
        chisqvcold=chisqvc
        nptscold=nptsc
        ndftotcold=ndftotc
        prtotcold=prtotc
      end if
      call vp_syschk(ion,level,ipind,nlin,parm,nn,istat,nsys,
     :              nnmod,temper,vturb,atmass,fixedbth)
      if(istat.eq.0) then
*       rfc 01.05.02: final check to see if can drop systems
        if(clnemin.gt.1.0d0.and.errlnemax.lt.1d20) then
          literokold=literok
          if(prtot.gt.probchb) then
*	    solution was acceptable
            literok=.true.
           else
            literok=.false.
          end if
          ndrop=0
          call vp_endrej(parm,parerr,nn,istat)
          nlin=nn/noppsys
          call vp_syschk(ion,level,ipind,nlin,parm,nn,istat,nsys,
     :              nnmod,temper,vturb,atmass,fixedbth)
        end if
      end if
*
*     if an end case, see if fit still acceptable
      if(literokold.and.(.not.literok)) then
*	no point if there had not been a previous iteration
*       or previous iteration was not acceptable anyway.
        if(nitnxx.le.0.or.prtotcold.lt.probchb) goto 7777
*	restore previous values and branch out
        nn=nnold
        do i=1,nn
          parm(i)=parmoldh(i)
          ipind(i)=ipindold(i)
          parerr(i)=parerrold(i)
        end do
*	old chisq, probabilities
        nlines=nn/noppsys
        do i=1,nlines
          ion(i)=ionold(i)
          level(i)=levelold(i)
          vturb(i)=vturbold(i)
          atmass(i)=atmassold(i)
          temper(i)=temperold(i)
          fixedbth(i)=fixedbthold(i)
          linchk(i)=linchkold(i)
        end do
        iterc=itercold
        chisqvc=chisqvcold
        nptsc=nptscold
        ndftotc=ndftotcold
        prtotc=prtotcold
        write(6,*) ' -------------------------------------------'
        write(6,*) '| Convergence criterion no longer satisfied |'
        write(6,*) '| PREVIOUS ITERATION VALUES IN SUMMARY FILE |'
        write(6,*) ' -------------------------------------------'
        goto 7777
      end if  
*     check against looping, then redo if not
      if(istat.ne.0) then
*	update iteration counter
        nitnxx=nitnxx+1
        if(nitnxx.gt.maxadrit) goto 7777

*	check that new values are not the same as old
*	note nnold and nnvold initialized to -1, so skips
*	these on first pass
        if(nn.eq.nnold) then
          difmax=0.0d0
          do i=1,nn
            temp=abs(parm(i)-parmold(i))
            if(temp.gt.difmax) difmax=temp
          end do
          if(difmax.lt.1.0d-6) goto 7777
        end if
*	check that the very old values not the same as well
        if(nn.eq.nnvold) then
          difmax=0.0d0
          do i=1,nn
            temp=abs(parm(i)-parmvold(i))
            if(temp.gt.difmax) difmax=temp
          end do
          if(difmax.lt.1.0d-6) goto 7777
        end if
*	not looping at this level so copy new to old, old to vold
        if(nnold.gt.0) then
          nnvold=nnold
          do i=1,nnold
            parmvold(i)=parmold(i)
          end do
        end if
        if(lcpend) then
*	  set flag in case more lines are removed
          lcpend=.false.
          nnold=nnoldh
          do i=1,nn
            parmold(i)=parmoldh(i)
          end do
         else
          nnold=nn
          do i=1,nn
            parmold(i)=parm(i)
          end do
        end if
*
        ndroptot=ndroptot+istat
        nn=nnmod
        nlines=nsys
        do ji=1,nlines
          ip=noppsys*(ji-1)+1
          call tikset(ion(ji),level(ji),parm(ip+1),ji)
        end do

*	Restore continuum for new fit:
*	write(6,*) ' All old lines lost'
*	Is this needed? And should it be maxnfi? <---
*	do n=1,maxnfi
*	  if(ndp(n).gt.0) then
        if(ngp.le.0) then
          ntem=maxfis
         else
          ntem=ngp
        end if
        do ic=1,ntem
          dhcont(ic)=dhccont(ic)
*         was origcntss(ic,n)
        end do
*	reset the tied pointers, but not the parameters
*       rfc 18.02.09 (ion,level,) subroutine parameters removed - not used
        call vp_tsetxref(nlines,parm,ipind,nn,.false.)
*	branch to have another go
        goto 1236
      endif

 7777 continue
      if(verbose) then
*       Print a clue as to how it got here
        write(6,*) 'istat =',istat,'  nitnxx =', nitnxx,
     :    '  maxadrit =',maxadrit
        write(6,*) 'difmax=',difmax
        write(6,*) 'prtotcold =',prtotcold,'  probchb =',probchb
        write(6,*) 'literokold ',literokold,'  literok ',literok
      end if
      if(nopt.ne.9) then
*       compute chunk by chunk flux derivatives
        if(lqmucf.and.lchvsqmu) then
          write(6,*) 'Computing chunk velocity sigmas'
          call vp_flchderivs(nchunk,nlines)
        end if
*       Summary of results written out to fort.26 here:
        call vp_smry(ion,level,parm,parerr,ipind,nn)
      end if
      if(cop3ans.eq.'f') goto 833
*     was: if(cop3ans.eq.'f'.or.lstopit) goto 833
*
      if(nopt.eq.2.or.nopt.eq.3.or.nopt.eq.4.or.nopt.eq.7.or.
     :    nopt.eq.10) then
        op2it=op2it+1
*       Check that end of FOR013.DAT hasn't been reached yet
        if(op2it.le.ngoes) then
*         check you don't want to stop anyway
          open(unit=20,file='stop_series',status='old',err=693)
          close(unit=20)
          write(6,*) 'Sequence abandoned'
          call system('/bin/rm stop_series')
          goto 694
*         reset all variables - mostly done in the subroutine
*         vp_vreset, parameters (totit,num,nchunk,lcpend,nfile,parerr)
*         and several in common blocks
 693      ngp=maxfis
          indcf=0
          icontflg=0
*         and remove the old 'previous iteration' values:
          prtot=1.0d20
          prtotc=1.0d20
          itercold=0
          chisqvcold=1.0d20
          nptsc=0
          nptscold=nptsc
          ndftotc=0
          ndftotcold=ndftotc
          prtotcold=prtotc
          lstopit=.false.
          call vp_vreset(totit,num,nchunk,lcpend,nfile,parerr)
          goto 832
        end if
        if(op2it.gt.ngoes.and.nopt.eq.2) stop
*       if(op2it.gt.ngoes.and.nopt.eq.3.or.nopt.eq.4
*     :          .or.nopt.eq.7.or.nopt.eq.10) goto 833
      endif
*     display the profiles
*     ajc 8-nov-91  nd added to pass size of chunk
*     ajc 22-feb-92  nd changed to prks - ks probability
 833  if(nlines.le.0) then
*       nlines was not updated, so do so now:
        nlines=nsys
*	nlines may still be zero, if nsys was, so try the last possibility
        if(nlines.le.0) then
          nlines=nn/noppsys
        end if
      end if
      lstopit=.false.
      call vp_chdisprof(nlines,ion,level,parm,
     1      num,prks)

*     loop back from here to start another fit
 9132 continue
      if(len26tem.gt.0) then
        write(6,*) 'Summary output was to ',cwr26s(1:len26tem)
      end if
      write(6,*)' Fit more lines? [n]'
      write(6,'(''> '',$)')
      read(5,'(a)') cans
      if(cans(1:1).eq.'y'.or.cans(1:1).eq.'Y') then
        if(chcv(5)(1:1).ne.' ') then 
          write(6,*)' Restore continuum? [n]'
          write(6,'(''> '',$)')
          read(5,'(a)') cans
         else
          cans='y'
        end if
        if(cans(1:1).eq.'y'.or.cans(1:1).eq.'Y')then
          do i=1,ngp
            dhcont(i)=dhccont(i)
          end do
         else
*	  reset all variables
          ngp=maxfis
          indcf=0
          icontflg=0
*         reset values for a new dataset
          call vp_vreset(totit,num,nchunk,lcpend,nfile,parerr)
*	  if unit 13 open AND at end of requested list, close it.
          if(op2it.ge.ngoes.and.p13file(1:1).ne.' ') then
            p13file=' '
            close(unit=13)
            ngoes=0
            op2it=0
            write(6,*) 'Input file closed'
          end if
        endif
*       this replaces branch to 1234
        goto 7234
      end if
*     switch off prompt before killing pgplot
      if(ipgopen.eq.1) then
        call pgask( .false. )
        call vp_pgend
      end if

 694  continue
      stop
      end


      subroutine vp_settied(nopt)
      include 'vp_sizes.f'
*     much reduced routine - just checks for thermal and turbulent values.
*     previous work now done by tieval in ucoptv.
*
*     set the tied parameter values
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     rfc 22.5.99: parameter arrays passed as common
      character*2 ion(maxnio)
      character*4 level(maxnio)
      double precision parm(maxnpa)
      common/vpc_parry/parm,ion,level,nlines,nn
      character*2 ipind(maxnpa)
      logical lcase
      common/vpc_usoind/ipind,isod,isodh
      double precision vturb,atmass,temper,fixedbth
      common/vpc_ucopder/vturb(maxnio),atmass(maxnio),
     :                 temper(maxnio),fixedbth(maxnio)

*     set up atomic masses if constrained parameters
*     always read them now - bcheck uses atomic masses always...
*     if(nopt.ge.0) then
      nfunc=nn/noppsys
      do ir=1,nfunc
        call vp_atomass(ion(ir),atmass(ir))
      end do
*     endif

*     ajc 19-nov-91  prompt for values if interactive?
      if ( nopt .eq. 1 .or. nopt .eq. 5 .or.
     1       nopt .eq. 6 ) then
        do k = noppsys, nn, noppsys
          if ( lcase( ipind( k )(1:1) ) ) then
            write ( 6, * ) 'Entry ', k
            write ( 6, * ) 'Ion ', ion( k / noppsys )
            write(6,*)
     1            ' Turbulent component, cloud temperature ?'
            write(6,*) 
     1            ' Non-zero value of one defines it as fixed'
            write(6,*)
     1            ' Both non-zero => fixed turbulent component'
            write(6,*)
     1            ' Both zero assumes thermal broadening'
            write(6,'(''> '',$)')
            read(5,*) vturb(k/noppsys),temper(k/noppsys)
          end if
        end do  
      end if
      return
      end
      subroutine vp_getf13name(p13file,ngoes)
      character*132 p13file
      integer ngoes
*
*     common character handling variables
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
 691  write(6,
     1    '('' Parameter input file, # entries? [fort.13,1] '')')
*     read(5,'(a)') p13file
      write(6,'(''> '',$)')
      read(5,'(a)') inchstr
      call sepvar(inchstr,2,rvstr,ivstr,cvstr,nvstr)
      if(nvstr.gt.0) then
        if(cvstr(1)(1:2).ne.'  ') then
          if(nvstr.eq.1) then
*           If no parameter, allow ridiculously long file names
            p13file=inchstr
           else
            p13file=cvstr(1)
          end if
         else
          p13file='fort.13'
        end if
        if(nvstr.ge.2) then
          ngoes=ivstr(2)
          if(ngoes.le.0) ngoes=1
        end if
       else
        p13file='fort.13'
        ngoes=1
      end if
      if(p13file.ne.'    ') then
*       If fort.13 open, close it
        close(unit=13,err=6991)
 6991   open(unit=13,file=p13file,status='old',err=691)
      end if
      return
      end
