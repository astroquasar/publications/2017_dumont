      subroutine vp_abundset
*
*     read the element abundances and ionizations from a file, for
*     tieing the column densities from model data
*
*     all variables in common block vpc_abund
*
      logical abflag
      character*4 ch4roman
*     readin workspace
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
      double precision dvstr(24)
      common/vpc_dsepspace/dvstr
*
      integer nabund
      double precision abund(120)
      character*2 chaelm(120)
      character*4 chaion(120)
      common/vpc_abund/abund,chaelm,chaion,nabund
*
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
*     control for verbose output:
      logical verbose
      common/vp_sysout/verbose
      abflag=.true.
      nabund=1
      open(unit=15,file='vp_abund.dat',status='old',err=99)
*     atomic data filename is present, so read it in
 199  read(15,'(a)',end=98) inchstr
*     turn colons into spaces so it will read a Ferland line
      do i=1,132
        if(inchstr(i:i).eq.':') inchstr(i:i)=' '
      end do
      call dsepvar(inchstr,15,dvstr,ivstr,cvstr,nvstr)
*     blank line separates ionization from abundances
      if(cvstr(1)(1:1).eq.' ') then
*       blank line
        if(nabund.le.1) goto 199
        abflag=.false.
        nabund=nabund-1
        goto 199
      end if
      if(abflag) then
*       read abundances
        do i=1,nvstr
          if(dvstr(i).eq.0.0d0.and.cvstr(i)(1:2).ne.'  '.and.
     1         cvstr(i)(1:1).ne.'0'.and.cvstr(i)(1:1).ne.'.') then
*           try as an element
            chaelm(nabund)=cvstr(i)(1:2)
            if(i.lt.15) then
              abund(nabund)=dvstr(i+1)
             else
              write(6,*) ' Some elements missed - put .le.7 per line'
              goto 199
            end if
*           check space needed for various elements
            if(chaelm(nabund).eq.'H ') then
              ntemp=nabund
              goto 77
            end if
            if(chaelm(nabund).eq.'He') then
              ntemp=nabund+1
              goto 77
            end if
            if(chaelm(nabund).eq.'Li') then
              ntemp=nabund+2
              goto 77
            end if
            if(chaelm(nabund).eq.'Be') then
              ntemp=nabund+3
              goto 77
            end if
            if(chaelm(nabund).eq.'B ') then
              ntemp=nabund+4
              goto 77
            end if
            if(chaelm(nabund).eq.'C ') then
              ntemp=nabund+5
              goto 77
            end if
            if(chaelm(nabund).eq.'N ') then
              ntemp=nabund+6
              goto 77
            end if
            if(chaelm(nabund).eq.'O ') then
              ntemp=nabund+7
              goto 77
            end if
            ntemp=nabund+8
 77         continue
            do j=nabund,ntemp
              chaelm(j)=chaelm(nabund)
              chaion(j)=ch4roman(j-nabund+1)
              abund(j)=abund(nabund)
            end do
            nabund=ntemp+1
          end if
        end do
        goto 199
       else
*       ionization read and process
        nt=1
        do while (chaelm(nt).ne.cvstr(1)(1:2).and.nt.le.nabund)
          nt=nt+1
        end do
        nx=2
        do while (chaelm(nt).eq.cvstr(1)(1:2).and.nt.le.nabund)
          if(dvstr(nx).gt.0.0d0) then
            abund(nt)=abund(nt)*dvstr(nx)
           else
            abund(nt)=abund(nt)*1.0d-8
          end if
          nt=nt+1
          nx=nx+1
        end do
        goto 199
      end if
 98   continue
*     convert abundances to logs if appropriate (i.e.if indvar.eq.1)
*     Do you want the scalefactors in here? Does not seem to use it!
      if(indvar.eq.1) then
        do i=1,nabund
          if(abund(i).gt.0.0d0) then
            abund(i)=log10(abund(i))*scalelog
           else
            abund(i)=-10.0d0
          end if
        end do
       else
        do i=1,nabund
          abund(i)=abund(i)*scalefac
        end do
      end if
*     close the unit
      close(unit=15)
 96   return
 99   nabund=0
      if(verbose) write(6,*) ' No abundances file'
      goto 96
      end
      character*4 function ch4roman(i)
*     converts integer to roman numeral
      ch4roman='    '
      if(i.eq.1) ch4roman='I   '
      if(i.eq.2) ch4roman='II  '
      if(i.eq.3) ch4roman='III '
      if(i.eq.4) ch4roman='IV  '
      if(i.eq.5) ch4roman='V   '
      if(i.eq.6) ch4roman='VI  '
      if(i.eq.7) ch4roman='VII '
      if(i.eq.8) ch4roman='VIII'
      if(i.eq.9) ch4roman='IX  '
      if(i.eq.10) ch4roman='X   '
      return
      end
      double precision function vp_abund(elem,ionz)
      integer nabund,i
      character*2 elem
      character*4 ionz
*
      double precision abund(120)
      character*2 chaelm(120)
      character*4 chaion(120)
      common/vpc_abund/abund,chaelm,chaion,nabund
      i=1
      do while((elem.ne.chaelm(i).or.ionz.ne.chaion(i))
     :          .and.i.le.nabund)
        i=i+1
      end do
      if(i.le.nabund) then
        vp_abund=abund(i)
       else
        vp_abund=1.0d0
        write(6,*) 'Fraction for ',elem,ionz,' not defined!!'
      end if
      return
      end
