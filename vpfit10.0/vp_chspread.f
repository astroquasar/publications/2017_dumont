      subroutine vp_chspread(flx,nlw,nw,wavcl,nw1,nw2,
     :                       ichunk, flb)
*
      implicit none
      include 'vp_sizes.f'
*
*     Convolve array with Gaussian (or other) profile
*     using chunk wavelengths (2-D array), and the central
*     resolution for the chunk. A single value for the resolution 
*     (in velocity terms) is assumed for the ENTIRE chunk.
*     If you want variable resolution, use vp_spread.f
*     RFC 20.7.06: sighxx,res removed as arguments - now obtained
*     via (3.8.06) vpf_dvresn function common block
*     
*     
*     IN:
*     flx	r*4 array	data array for chunk
*     nlw	int		start channel for result
*     nw	int		end channel for result
*     wavcl   double 2D array wavelength array (pixel x chunk)
*     nw1     integer         first dimension wavcl
*     nw2     integer         second dimension wavcl
*     ichunk	int		chunk number (for wavelengths)
*
*     OUT:
*     flb	r*4 array	flx convolved with instrument profile
*     
*     COMMON:
*     Binned profile parameters, /vpc_profwts/, used if npinst.gt.0
*
*
      integer nlw,nw,nw1,nw2,ichunk
      double precision flx(*),flb(*)
      double precision wavcl(nw1,nw2)
*
*     LOCAL:
      integer i,j,jji,k,nd,nlo,nup
      double precision bwhvard,df,fsigd,sigd,xdb
*     data value variables
      double precision s,sx,y,sm,smn
*     FUNCTIONS:
      double precision vpf_dvresn,dexpf
*
*     binned profile
      character*2 chprof
      double precision pfinst
      double precision wfinstd
      integer npinst,npin2
      common/vpc_profwts/wfinstd(maxipp,maxnch),pfinst(maxipp,maxnch),
     :        npinst(maxnch),npin2(maxnch),chprof(maxnch)
*     chunk subdivision variables
      integer nminsubd,nmaxsubd
      double precision bindop
      common/vpc_vpsubd/bindop,nminsubd,nmaxsubd
*
*     smoothed data in array flx -> flb(nlw,nw)
*
*     use binned profile if npinst.gt.0
      if(npinst(ichunk).gt.0.and.chprof(ichunk)(1:2).ne.'wt') then
        do i=nlw,nw
          sm=0.0d0
          smn=0.0d0
          do j=1,npinst(ichunk)
            k=i+j-npin2(ichunk)
            if(k.ge.1.and.k.le.nw) then
              sm=sm+flx(k)*pfinst(j,ichunk)
              smn=smn+pfinst(j,ichunk)
            end if
          end do
          if(smn.gt.0.0d0) then
            flb(i)=sm/smn
           else
*           raw value if weight is zero
            flb(i)=flx(i)
          end if
        end do
       else
*       no pixel instrument profile, so use continuous function
*	gaussian smoothing  -set the local sigma
        xdb=wavcl((nlw+nw)/2,ichunk)
*       determining local sigma now from function
        sigd=vpf_dvresn(xdb,ichunk)
        if(sigd.le.0.0d0) then
*         don't smooth
          do i=nlw,nw
            flb(i)=flx(i)
          end do
         else
*         perform convolution
          do i=nlw,nw
            jji=max(i,nlw+1)
            jji=min(jji,nw-1)
            xdb=wavcl(i,ichunk) !/zed
            bwhvard=0.5d0*(wavcl(jji+1,ichunk)- wavcl(jji-1,ichunk)) 
!           sigd=sighd+dellmd/xdb
            fsigd=6.0d0*sigd
            df=fsigd*xdb/bwhvard
            nd=int(df)
            if(nd.le.0) nd=1
            nlo=i-nd
            nup=i+nd
            if(nlo.lt.1) nlo=1
            if(nup.gt.nw) nup=nw
            sx=0.0d0
            s=0.0d0
            do j=nlo,nup
              y=(wavcl(j,ichunk)/xdb-1.0d0)/sigd
              y=dexpf(-y*y*0.5d0)
              sx=sx+y
              s=s+flx(j)*y
            end do
            if(sx.gt.0.0d0) then
              flb(i)=s/sx
             else
              flb(i)=flx(i)
            end if
          end do
        end if
      end if
      return
      end
