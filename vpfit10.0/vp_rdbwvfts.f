      subroutine vp_rdbwvfts(im,name,nord,nech,coeff,status )

*     reads non-linear wavelengths.  REFSPEC1 in .imh must have the name
*     of the database file, and the calibration must have been done with ecid,
*     using 2-d cheb polynomials (with cross terms) and vacuum wavelengths.
*     the heliocentric velocity should be stored in the header as HELVEL.
*     (if present, it is returned as 'hvel'),
*     the resolution polynomial should be in a text file in the database,
*     with the name in the header as RESFILE.
*     rfc 22-08-94 NWOFFSET added to allow for IRAF data subsets.

*     andrew cooke 20 mar 92, rfc modified 13.6.95

*     IN:
*     im	int	data file number
*     name	ch*(*)	data file name
*     nord	int	max no wavelength coeffts
*     nech	int	number of echelle orders
*
*     OUT:
*     coeff	dble	array of coeffts
*     status	int	error flag (0=OK)

      implicit none

      include 'vp_sizes.f'

      character*(*) name
      integer nech, nord
      double precision coeff( nord, nech )
      logical verbose
      integer status

      integer im
      character*80 rname, dtadir
      integer last

      common/vp_sysout/verbose

*
*     get ref spec file for database entry
      call ftgkys(im,'REFSPEC1', rname,dtadir,status)
      if ( status .ne. 0 ) then
        if ( verbose) then
          write ( *, * ) ' no reference spectrum - REFSPEC1 in header'
        end if
        go to 191
      end if
*     the directory may not be the home directory - assume that database 
*     is a subdirectory inside the data directory
      dtadir = ' '
      last = 1
      if ( index( name, '/' ) .ne. 0 ) then
        last = index( name, '/' )
        do while ( index( name( last + 1 : ), '/' ) .ne. 0 )
          last = index( name( last + 1 : ), '/' ) + last
        end do
        dtadir = ' '//name( 1 : last )
      end if
      dtadir = dtadir( 1 : last + 1 )//'database/ec'//rname
      do while ( dtadir( 1 : 1 ) .eq. ' ' )
        dtadir = dtadir( 2 : )
      end do
      if(verbose) then
        write ( *, * ) ' wavelength file : '//dtadir( 1 : 60 )
      end if

*     modify name and get values
      call vp_getbwv( dtadir, nord, nech, coeff, status )
*
*     then get ref spec file for database entry
 191  return
      end
      subroutine vp_getbwv( name, nord, nech, coeff, status )

*     reads the database entry specified in 'name' and calculates the
*     polynomial coefficients for the wavelength, given the 2-d chebyschev
*     polynomials in the file.

*     the conversion from cheb to straight polynomials is primitive - sorry -
*     but it was a quick botch that never got altered.  the cheb is evaluated
*     and a polynomial fitted to the points.  it works well - the residual is
*     checked - and its fast enough!
*     andrew cooke 20 mar 92
*     rfc 13.6.95

      implicit none

      character*(*) name
      integer nord
      integer nech
      double precision coeff( nord, nech )
      integer status
      logical verbose

      integer ix, iy, i, j
      integer ier
      double precision junk
      character*80 line
      integer xord, yord
      double precision xlo, xhi, ylo, yhi
      integer count, order, outcnt
      integer acunit
      parameter ( acunit = 31 )

      integer k, l
      integer nmax
      parameter ( nmax = 20 )
      double precision dx( nmax, 2 ), dy, result( nmax, 3 )
      double precision de( nmax )
      double precision xmin, xmax, ymin, ymax
      integer na, slope
      parameter ( na = 100 )
      double precision da( na )
      integer nwork
      parameter ( nwork = 100 )
      double precision work( nwork )
      integer ifail
      double precision polyc( na )
      double precision res, sumres, mres, temp
      double precision prg_polyd
      logical satiate

      integer in( 3 ), nvar
      character*80 ch( 3 )
      double precision rl( 3 )

      common/vp_sysout/verbose
*
      xord=0
      write(6,*) 'USING VP_GETBWV -- CHECK RESULTS!!!'
      write(6,*) ' .. CALLED FROM VP_BASEWV'

      if ( status .ne. 0 ) then
        write ( *, * ) 'getting wavelengths, but status non-zero'
        status = 0
      end if

      do i = 1, nech
        do j = 1, nord
          coeff( j, i ) = 0d0
        end do
      end do
*     default in absence of wavelength info is channel numbers
      do i = 1, nech
        if ( nord .ge. 1 ) then
          coeff( 1, i ) = 0.0d0
        end if
        if ( nord .ge. 2 ) then
          coeff( 2, i ) = 1.0d0
        end if
      end do

      satiate = .false.
      slope = 1

      open ( file = name, err = 1, iostat = ier, 
     :       status = 'old', unit = acunit )

 10   continue
      read ( unit = acunit, err = 1, end = 1, iostat =ier, 
     :         fmt = '(a)' ) line
      if ( index( line, 'slope' ) .ne. 0 ) then
        call dsepvar( line, 2, rl, in, ch, nvar )
        slope = in( 2 )
*       read ( line, '( 7x, i2 )' ) slope
      end if
      if ( index( line, 'coefficients' ) .eq. 0 ) go to 10

      read ( unit = acunit, fmt = '(a)' ) line
      read ( unit = acunit, fmt = '(a)' ) line
      call dsepvar( line, 1, rl, in, ch, nvar )
      xord = in( 1 )
*      read ( line, '(2x,i1)' ) xord
      read ( unit = acunit, fmt = '(a)' ) line
      call dsepvar( line, 1, rl, in, ch, nvar )
      yord = in( 1 )
*      read ( line, '(2x,i1)' ) yord
      read ( unit = acunit, fmt = '(a)' ) line
      read ( unit = acunit, fmt = '(a)' ) line
      call dsepvar( line, 1, rl, in, ch, nvar )
      xlo = rl( 1 )
*      read ( line, '(2x,f8.0)' ) xlo
      read ( unit = acunit, fmt = '(a)' ) line
      call dsepvar( line, 1, rl, in, ch, nvar )
      xhi = rl( 1 )
*      read ( line, '(2x,f8.0)' ) xhi
      read ( unit = acunit, fmt = '(a)' ) line
      call dsepvar( line, 1, rl, in, ch, nvar )
      ylo = rl( 1 )
*      read ( line, '(2x,f8.0)' ) ylo
      read ( unit = acunit, fmt = '(a)' ) line
      call dsepvar( line, 1, rl, in, ch, nvar )
      yhi = rl( 1 )
*      read ( line, '(2x,f8.0)' ) yhi

*     read coefficients and convert to nag format
      do iy = 0, yord-1
        do ix = 0, xord-1
          read ( unit = acunit, fmt = '(a)' ) line
          call dsepvar( line, 1, rl, in, ch, nvar )
          junk = rl( 1 )
*	  read ( line, '(2x,f8.0)' ) junk
          count = ix * yord + iy + 1
          da( count ) = junk
          if ( iy .eq. 0 ) da( count ) = da( count ) * 2d0
          if ( ix .eq. 0 ) da( count ) = da( count ) * 2d0
          count = count + 1
        end do
      end do

*     evaluate points then least-squares fit for polynomial
      k = xord - 1
      l = yord - 1
      xmin = dble( nint( xlo ) )
      xmax = dble( nint( xhi ) )
      ymin = ylo
      ymax = yhi

      do ix = 1, nmax
        dx( ix, 1 ) = xmin + 
     :             ( xmax - xmin ) * dble( ix - 1 ) / dble( nmax )
        de( ix ) = 1.0d0
      end do
      do ix = 1, nmax - 1
        dx( ix, 2 ) = ( dx( ix, 1 ) + dx( ix + 1, 1 ) ) / 2d0
      end do
      dx( nmax, 2 ) = dx( nmax, 1 )
     
      outcnt = 0
      sumres = 0.0d0
      mres = 0.0d0
      do order = nint( ymin ), nint( ymax )
        outcnt = outcnt + 1
*       ajc 19-may-92  slope may be -ve if orders reversed
        if ( slope .eq. 1 ) then
          dy = dble( order ) 
         else
          dy = ymax - ( order - ymin )
        end if
        ifail = 0
*	calls to replace NAG e02cbf:
        call pda_che2d(nmax,xmin,xmax,dx(1,1),ymin,ymax,dy,k,l,
     :                 na,da,nwork,work,result(1,1),ifail)
*	replaces:
*        call e02cbf( 1, nmax, k, l, dx( 1, 1 ), xmin, xmax,
*     :               dy, ymin, ymax,
*     :               result( 1, 1 ), da, na, work, nwork, ifail )
*
        call pda_che2d(nmax,xmin,xmax,dx(1,2),ymin,ymax,dy,k,l,
     :                 na,da,nwork,work,result(1,2),ifail)
*	replaces:
*        call e02cbf( 1, nmax, k, l, dx( 1, 2 ), xmin, xmax,
*     :               dy, ymin, ymax,
*     :               result( 1, 2 ), da, na, work, nwork, ifail )

        do j = 1, 2
          do i = 1, nmax
            result( i, j ) = result( i, j ) / dy
          end do
        end do
     
        ier = 0
        call linfit( nmax, nmax, dx( 1, 1 ), result( 1, 1 ),
     :               de( 1 ), nord, polyc, ier )

*       test the fit by finding residual deviation at mid points
        res = 0.0d0
        do i = 1, nmax
          result( i, 3 ) = prg_polyd( polyc, nord, dx( i, 2 ) )
          res = res + ( result( i, 3 ) - result( i, 2 ) ) ** 2
        end do
        res = sqrt( res )
        sumres = sumres + res
        mres = max( mres, res / dble( nmax ) )

*	copy to output, including heliocentric correction
        do i = 1, nord
          if ( outcnt .le. nech ) then
            if ( i .le. xord ) then
              coeff( i, outcnt ) = polyc( i ) 
             else
              coeff( i, outcnt ) = 0.0d0
            end if
          end if
        end do

      end do
      temp=nmax*outcnt      
      sumres = sumres / temp
      if ( mres .gt. 1d-6 ) then
        write ( 6, * ) ' maximum conversion error : ', mres
        write ( 6, * ) ' typical conversion error : ', sumres
      end if
      satiate = .true.
*     may be another entry...
      go to 10
 1    continue
      if ( satiate ) then
        if ( xord .gt. nord ) then
           write ( 6, * ) 'higher order polynomial available'
        end if
        if ( nech .ne. outcnt ) then
          write ( 6, * ) 'wrong number of echelle orders'
          write ( 6, * ) 'wanted ', nech
          write ( 6, * ) 'found ', outcnt
        end if
       else
        status = 1
        if(verbose) then
          write ( 6, * ) ' no wavelength info found'
        end if
      end if
*
      close ( unit = acunit, iostat = ier, err = 2 )
 2    continue
      return
      end

