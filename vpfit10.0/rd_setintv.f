      subroutine rd_setintv
*
*     Set various internal flags, without explanation, from
*     current command stream.
*     Utility routine for odd extra capabilities, normally
*     undocumented, and usually for development purposes.
*
*     Parameters handed across via cv(i) etc variables in common
*     (max in line is 10, including command to get here)
*     Default is to leave them unchanged.
*
      implicit none
*     string unscrambling common
      integer nv
      character*132 inchstr
      character*60 cv(24)
      real rv(24)
      integer iv(24)
      common/rd_chwork/inchstr,rv,iv,cv,nv
*     line list variables
      double precision zmark
      character*132 chmarkfl
      logical lmarkl,lmarkto
      common/rdc_lmarklv/zmark,chmarkfl,lmarkl,lmarkto
*
      if(cv(2)(1:6).eq.'lmarkl') then
        if(cv(3)(1:1).eq.'t'.or.cv(3)(2:2).eq.'t') then
          lmarkl=.true.
         else
          if(cv(3)(1:1).eq.'f'.or.cv(3)(2:2).eq.'f') then
            lmarkl=.false.
          end if
        end if
      end if
      return
      end
