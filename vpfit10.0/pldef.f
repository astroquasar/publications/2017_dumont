      subroutine pldef(nct,chcr)
      implicit none
      include 'vp_sizes.f'
*
*     set all the plot defaults, given array length
*     chcr indicates calling program (vpfit or rdgen) for when
*     different initial variables are used
*
      integer nct
      character*(*) chcr
*     Local
      integer i,j
*
      integer nwco
      double precision wcf1(maxwco)
      common/vpc_wavl/wcf1,nwco
      integer ntick,nred
      double precision tikmk,rstwav
      common/vpc_ticks/tikmk(maxnio,maxnio+1),ntick(maxnio+1),
     :             nred,rstwav(maxnio,maxnio+1)
*     plot steering variables
*     l1,l2:   start and end channels for plotting
*     lc1,lc2: start and end channels for plot scaling
*     fampg:   yscale multiplier
      integer l1,l2,lc1,lc2
      real fampg
      common/ppam/l1,l2,lc1,lc2,fampg
*
*     lxa,lxb are start and finish channels for input data
      integer lxa,lxb
      common/vpc_misc/lxa,lxb
*     plot array pointers (consistent with ipgatt)
*     0=don't,1=do for 1:data,2:error,3:cont,6:resid,9:rms
      integer ksplot(9)
      common/vpc_ksplot/ksplot
*     text label for a curve
      integer icoltext
      character*24 cpgtext
      real xtextpg,ytextpg
      common/pgtexts/xtextpg,ytextpg,cpgtext,icoltext
c     pgplot attributes: data, error, continuum, axes, ticks, text
c     1 color indx; 2 line style; 3 line width;
c     4 curve(0), hist(1); 5 - 9 reserved; 10 special (see plset.f).
      integer ipgatt(10,9)
      common/vpc_pgattrib/ipgatt
c     min and max yscales by hand, and default lower level
      integer nyuse
      real ylowhpg,yhihpg,yminsetpg
      common/pgylims/ylowhpg,yhihpg,yminsetpg,nyuse
c     pgplot variables
      integer ipgopen,ipgflag
      common/pgplotv/ipgopen,ipgflag
c     pgplot tick flag
*     ipgtgrk=1 if Greek letters, else 0; yuptkf(def=0.9) is fract y-axis
*     for top of tick, ylentkf (def=0.07) fract y for length of tick
      integer ipgtgrk
      real yuptkpg,ylentkpg
      common/vpc_tick/ipgtgrk,yuptkpg,ylentkpg
*     pgplot overplot with bias
      integer ipgov
      logical plgscl
      real pgbias
      common/pgover/ipgov,pgbias,plgscl
      integer ipgnx,ipgny
      character*60 pglchar(3)
      common/pgnumba/ipgnx,ipgny,pglchar
      character*60 pglcapt
      real chszoldpg,chszpg
      common/rdc_pglcapt/pglcapt,chszoldpg,chszpg
*     plot/print velocity scale (yes if lvel=.true.)
      logical lvel
      real wcenpg,vellopg,velhipg,zp1refpg
      common/vpc_pgvels/wcenpg,vellopg,velhipg,lvel,zp1refpg
*     ascii prinout instead of plot
      logical lascii,lplottoo
      common/vpc_pgascii/lascii,lplottoo
*     line list variables
      double precision zmark
      character*132 chmarkfl
      logical lmarkl,lmarkto
      common/rdc_lmarklv/zmark,chmarkfl,lmarkl,lmarkto
      integer lrformt,lr1,lr2,lr3
      common/rdc_lintvar/lrformt,lr1,lr2,lr3
*     colors for stacked plots
      integer kpgcol(16),kpgcolalt(16),nkpgcol
      common/rdc_kpgcol/kpgcol,kpgcolalt,nkpgcol
*     observed wavelengths?
      logical lobswav,ltick
      common/rdc_lobswav/lobswav,ltick
*     file steered tick marks
      logical ltvtick
      character*132 chtvtick
      common/rdc_tvtick/chtvtick,ltvtick
*     line on plot?
      logical lgline
      real pglxy(4)
      integer kpglxy(3)
      common/vpc_plline/lgline,pglxy,kpglxy
      real yhihpgheld
      common/rdc_pgyscales/yhihpgheld
*     pgplot width & aspect ratio
      real widthpg,aspectpg
      common/vpc_pgpaper/widthpg,aspectpg
*     file steered extras
      character*64 pgfxfile
      common/rdc_pgfxfile/pgfxfile
*
*     default paper widths set by pgplot
*     rest in vp_pgbegin only if aspectpg > 0
      widthpg=0.0
      aspectpg=-1.0
*     ymax for interactive mode set to ignore it
      yhihpgheld=-1.0
*     lrformt=1, fort.13; 0, fort.26
      lrformt=0
      if(chcr(1:1).eq.'v') then
        lmarkl=.true.
       else
*       rdgen assumed
*       Don't get file ticks
        lmarkl=.false.
      end if
      lmarkto=.false.
      chmarkfl=' '
      zmark=-1.0d20
*     Don't show observed wavelengths
      lobswav=.false.
      ltick=.true.
c     ipgopen=0 if pgplot not yet used
      ipgopen=0
      ipgflag=0
*     default is don't rescale overplots
      plgscl=.false.
      ipgov=0
      pgbias=0.0
      yminsetpg=0.0
*     ascii printout?
      lascii=.false.
      lplottoo=.true.
*     velocity as variable?
      lvel=.false.
      vellopg=-1000.
      velhipg=1000.
*     default reference redshift is zero
      zp1refpg=1.0
*
      do i=1,3
        pglchar(i)='                        '
      end do
      pglcapt=' '
      chszoldpg=1.0
      chszpg=1.0
c     text labels for curves
      cpgtext=' '
      xtextpg=0.0
      ytextpg=0.0
      icoltext=0
c
c     default to numbers on ticks, not letters
      ipgtgrk=0
*     tick top, length [as fractions of the window height]
      yuptkpg=0.90
      ylentkpg=0.07
*     plots on a page
      ipgnx=1
      ipgny=1
*     ticks from file turned off
      chtvtick=' '
      ltvtick=.false.
*
*     pgplot attibutes - set defaults
*     ipgatt(i,j): j=1 - data; 2 - error; 3 - continuum; 4 - axes;
*                      5 - ticks; 6 - residuals; 7 - fit region;
*                      8 - default & cursor; 9 - rms
*       i=1 - color index (0-15) [white,black,red,green,dpurple,
*                 lblue,mauve,yellow,orange,lgreen,teal,dblue,purple,
*                 magenta,dgrey,lgrey; 
*         2 - line style (1-5) (solid,dash,dot-dash,dot,dash3dots)
*         3 - line width (1-211);
*         4 curve(0), hist(1), bars(2); 5 - 9 reserved;
*         10 special features:
*         (10,1) = PGPLOT PGBAND cursor mode (default is zero)
*         (10,4) = 0 suppress x=0,y=0; 10 plot x=0, not y; 11 - plot x=0 & y=0
*         (10,5) = 0  normal plot; 10 with offset residuals;
*                  11 residuals & ticks [when written]
      do j=1,9
        do i=1,3
          ipgatt(i,j)=1
        end do
        ipgatt(4,j)=0
        do i=5,10
          ipgatt(i,j)=0
        end do
      end do
*     set default cursor color to light blue. User can change it later.
      ipgatt(1,8)=5
*     special attributes
      ipgatt(10,4)=11
*     extended cursor
      ipgatt(10,1)=7
*     set up default colours (user can change them with VPFPLOTS)
*     default colour for RMS is light grey
      ipgatt(1,9)=15
*     error - red
      ipgatt(1,2)=2
*     continuum - green
      ipgatt(1,3)=3
*     ticks - light blue
      ipgatt(1,5)=5
*     residuals - light green
      ipgatt(1,6)=9
*     fit region marker - yellow
      ipgatt(1,7)=7
*     data as histograms
      ipgatt(1,4)=1
*
*     default color cycle, for screen
      kpgcol(1)=2
      kpgcol(2)=3
      kpgcol(3)=7
      kpgcol(4)=5
      kpgcol(5)=6
      kpgcol(6)=8
      kpgcol(7)=13
      kpgcol(8)=9
      nkpgcol=7
      do j=1,nkpgcol
        kpgcolalt(j)=kpgcol(j)
      end do
*     for paper, kpgcol(3)=4 is better
      kpgcolalt(3)=4
*     extra line variables
      lgline=.false.
      do j=1,3
        pglxy(j)=0.0
        kpglxy(j)=1
      end do
      pglxy(4)=0.0
*
      nred=0
      lxa=1
      lxb=nct
      l1=1
      l2=nct
      lc1=20
      lc2=l2-20
      if(lc2.le.lc1) then
        lc1=1
        lc2=nct
      end if
      fampg=1.0
*     default wavelength coeffts is for channels
      do i=1,maxwco
        wcf1(i)=0.0d0
      end do
      wcf1(2)=1.0d0
      nwco=2
      nyuse=0
*     default is plot data, error and rms
      ksplot(1)=1
      ksplot(2)=1
      ksplot(3)=1
      ksplot(9)=1
      do j=4,8
        ksplot(j)=0
      end do
*     no extras
      pgfxfile=' '  
      return
      end

