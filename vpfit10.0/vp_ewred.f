      subroutine vp_ewred(ind)
*
*     reads the atomic data from a file into common arrays
*
*       
*     IN:
*     ind	integer	If zero, use environment variable to 
*               determine data source, if non-zero, prompt for
*               filename.
      implicit none
      include 'vp_sizes.f'
*
      integer ind
*
      character*80 filnm
      character*200 atomdr
*
      integer i,j,nnpos
      integer lcsrc
      double precision tmass,xtra
      integer nvp
      character*64 cvp(2)
      integer ivp(2)
      real rvpg(2)
*
*     Functions
      integer lastchpos
*     Common:
*     character input and separation variables
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
      double precision dvstr(24)
      common/vpc_dsepspace/dvstr
*
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm 
      integer m
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),m
*     atomic mass table
      character*2 lbm
      double precision amass
      integer mmass
      common/vpc_atmass/lbm(maxats),amass(maxats),mmass
*     other rest wavelength stuff, if needed
      logical lqmucf,lchvsqmu
      double precision qscale
      double precision qmucf
      common/vpc_qmucf/qmucf(maxats),qscale,lqmucf,lchvsqmu
*     Minimum wavelength Lyman line (for interpolation to Lyman cont.)
      double precision wlsmin,vblstar,collsmin
      common/vpc_lycont/wlsmin,vblstar,collsmin
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     input streams
      integer inputc
      common/rdc_inputc/inputc
*     monitor what is going on?
      integer nverbose
      common/vp_sysmon/nverbose
*     source path
      character*132 csrcpath
      common/vpc_srcpath/csrcpath
*
*     set nvp in case it is bypassed
      nvp=0
*     default is no extra wavelength stuff
      lqmucf=.false.
*     get directory for atomic data from logical ATOMDIR or use bob's	
      if(ind.gt.0) goto 990
      atomdr=' '
      call getenv ( 'ATOMDIR', atomdr )
      if( atomdr .eq. ' ' ) then
        if(csrcpath(1:1).ne.' ') then
          lcsrc=lastchpos(csrcpath)
          if(csrcpath(lcsrc:lcsrc).eq.'/') then
            atomdr=csrcpath(1:lcsrc)//'atom.dat'
           else
            atomdr=csrcpath(1:lcsrc)//'/atom.dat'
          end if
          open(unit=18,file=atomdr,status='old',err=995)
          write(6,*) 'Using atomic data supplied with package'
          goto 11
        end if
995     continue
        write(6,*) 'Trying atom.dat in current directory'
        atomdr = 'atom.dat'
        open(unit=18,file=atomdr,status='old',err=990)
      end if
      open(unit=18,file=atomdr,status='old',err=99)
      goto 11
 99   i=lastchpos(atomdr)
      if(atomdr(i:i).eq.'/') then
        atomdr=atomdr(1:i)//'atom.dat'
       else
        atomdr=atomdr(1:i)//'/atom.dat'
      end if
      open(unit=18,file=atomdr,status='old',err=990)
      goto 11
*     every default failed, so give up and ask
 990  write(6,*) ' Atomic data filename'
      read(inputc,'(a)',end=98) filnm
      if(filnm(1:4).eq.'    ') then
        write(6,*) ' Uses Ly-a only'
        m=1
        lbz(1)='H '
        lzz(1)='I   '
        alm(1)=1215.67d0
        fik(1)=0.4162d0
        asm(1)=6.265d8
        goto 98
      end if
*     allow for shortened list input
      call sepvar(filnm,2,rvpg,ivp,cvp,nvp)
      if(nvp.gt.1) then
        filnm=cvp(1)
        if(cvp(2)(1:2).ne.'sh'.and.cvp(2)(1:2).ne.'SH') then
          nvp=1
        end if
      end if
      open(unit=18,file=filnm,status='old',err=99)
*	print name of file
 11   continue
      inquire( 18, name=filnm )
      nnpos=lastchpos(filnm)
      write(6,*) 'Using data from : '//filnm(1:nnpos)
      m=0
      mmass=0
*       data read loop
*       fixed format file no longer supported
 1    m=m+1
      tmass=0.0d0
      lqmucf=.false.
      if(noppsys.ge.4.and.maxpps.ge.4) then
        lqmucf=.true.
       else
        if(maxpps.lt.4) then
          write(6,*) '-----------------------------------------'
          write(6,*) 'Atomic data: extra parameters ignored'
          write(6,*) 'change maxpps in vp_sizes.f & recompile'
          write(6,*) '-----------------------------------------'
        end if
      end if
 301  read(18,'(a)',end=97) inchstr
*     allow for comment lines: first character ! or #
      if(inchstr(1:1).eq.'!'.or.inchstr(1:1).eq.'#') then
*       check if this file contains q or mu coefficients
*       with first line in file starting with #Q, #q, !q or !Q
        if((inchstr(2:2).eq.'Q'.or.inchstr(2:2).eq.'q').and.
     :         m.eq.1) then
*         user presumably wants the extended data table
          write(6,*) 'Extended atomic data table'
*         make sure there is enough space to expand the table
          if(noppsys.lt.4) then
            lqmucf=.false.
            write(6,*) '-----------------------------------------'
            write(6,*) 'Atomic data: extra parameters ignored'
            write(6,*) 'to read them, put NOVARS 4 in setup file'
            write(6,*) '-----------------------------------------'
            goto 301
          end if
        end if
*       shortened list
        if(nvp.ge.2.and.inchstr(1:10).eq.'! -- Suppl') then
*         do not read subsequent lines, so branch to close file bit
          goto 97
        end if
        goto 301
      end if
*     separate inchstr string (in common) to get variables
*     xtra is either q-value or K-value
      call dsepvar(inchstr,7,dvstr,ivstr,cvstr,nvstr)
      call vp_ationsep(cvstr,dvstr,ivstr,7,1,lbz(m),lzz(m))
*     _vstr arrays now 1:element/ion; 2:alm (m) etc as below
      alm(m)=dvstr(2)
      fik(m)=dvstr(3)
      asm(m)=dvstr(4)
      tmass=dvstr(5)
      xtra=dvstr(6)
*     replaces:
*      call vp_atomsep(lbz(m),lzz(m),alm(m),fik(m),asm(m),tmass,xtra)
      if(nverbose.gt.12.and.tmass.gt.0.01d0) then
*       print the ones where mass specified as a check, not full table
        write(6,*) lbz(m),lzz(m),alm(m),fik(m),asm(m),tmass,xtra
      end if
*     set up mass table if a mass is there
      if(tmass.gt.0.0d0) then
*     check have not already got a mass
        if(mmass.gt.0) then
          i=1
          do while (lbz(m).ne.lbm(i).and.i.le.mmass)
            i=i+1
          end do
*	  i>mmass => not already done
          if(i.gt.mmass) then
            mmass=mmass+1
            lbm(mmass)=lbz(m)
            amass(mmass)=tmass
          end if
         else
          mmass=mmass+1
          lbm(mmass)=lbz(m)
          amass(mmass)=tmass
        end if
      end if
*     extra coefficient set
      if(lqmucf) then
        if(lbz(m).ne.'H2'.and.lbz(m).ne.'HD') then
*         fine structure constant q, rescale so units for delta(a)/a
*         are 1E-6 [almz=alm*(1.0-qmucf*(1E6*da/a)] i.e. parm(4)=1E6*da/a
          qmucf(m)=2.0d-8*xtra*alm(m)*qscale
         else
*         me/mp ratio shift.
          qmucf(m)=xtra*qscale
        end if
      end if
*
      if(alm(m).gt.0.0d0.and.m.lt.maxats) goto 1
      if(alm(m).gt.0.0d0) then
*       must have run out of array space
        write(6,'('' WARNING: ATOMIC DATA TABLE FULL'')')
        write(6,'(''  last record: '',a2,a4,f9.3,f10.6,1pe10.2)')
     1               lbz(m),lzz(m),alm(m),fik(m),asm(m)
*       add 1 to m so it can be subtracted later.
        m=m+1
      end if
 97   m=m-1
      rewind 18
      close(unit=18)
      if(nverbose.gt.5) then
        write(6,*) m,' line wavelengths in table'
      end if
c     find shortest wavelength in Lyman series used
c     for interpolation to Lyman limit if needed
 98   wlsmin=1d20
      if(m.ge.1) then
        do j=1,m
          if(lbz(j).eq.'H ') then
            if(alm(j).lt.wlsmin) wlsmin=alm(j)
          end if
        end do
      end if
      return
      end
