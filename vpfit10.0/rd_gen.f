      program rd_gen

*     general data handling program
*     using vpfit routines where possible for general compatibility
*
      implicit none
      include 'vp_sizes.f'
*
*     data arrays
*
      double precision drv(4)
*
*     local variables:
*     
      integer i,ichunk,ind,ipk,j,jlow,jhigh,jsm
      integer k,klo,khi
      integer lench,lenchs,lenfo,lenq,losm,lhsm
      integer ncmax,nht,norx,nrow,numdat,nvxx,nwcotem
      integer nct,ngp,nctp1,nfilt,ng,nostrip,nx,nwcolow
      integer istat,status,nsubstep
      integer lcsrc
      double precision xtem1d,dtemp,temp1,temp2
*     double precision sigd,dld
      character*132 oldchstr
      character*4 arg3
      character*60 chout,fpxname
      logical lsmth,lgulio
      double precision ehigh,elow,pxdf
      double precision dssum,pssum
      double precision t,temp,wlo,whi
      double precision werr,xerr,xr
*     
      double precision da(maxfis),de(maxfis)
      double precision ca(maxfis),cad(maxfis)
      double precision ha(maxfis),he(maxfis),hc(maxfis)
      double precision dh(maxfis),dhe(maxfis)
      double precision drms(maxfis)
*     workspace
      double precision ds(maxfis),re(maxfis)
*
*     functions:
      integer lastchpos
      double precision wval
      logical ucase
*
*     various common things:
*     single precision workspace
      real dspg,repg
      common/vpc_snglwork/dspg(maxfis),repg(maxfis)
*
      character*132 inchstr
      character*60 cv(24)
      real rvss(24)
      integer iv(24)
      double precision dv(24)
      integer nv
      common/rd_chwork/inchstr,rvss,iv,cv,nv
      character*8 chwhat
      character*24 arg2
*
*      double precision wch(maxwco)
*      double precision acac
*      common/wavl/acac(maxwco)
      integer nwco
      double precision wcf1(maxwco)
      common/vpc_wavl/wcf1,nwco
*
*     fit to resolution, using sigmas since they are in expression used
      integer nswres(maxnch)
      double precision swres(maxpoc,maxnch)
      common/vpc_sigwres/swres,nswres
*
      character*8 wcftype
      character*4 vacind
      double precision helcfac
      integer*4 noffs
      common/c8_wcftype/wcftype,helcfac,noffs,vacind
*
      logical verbose
      common/vp_sysout/verbose
*     plot variables
      integer ipgopen,ipgflag
      common/pgplotv/ipgopen,ipgflag
*     nst: start channel, ncn: end channel, lc1, lc2 start, end for scale
      integer nst,ncn,lc1,lc2
      real fampg
      common/ppam/nst,ncn,lc1,lc2,fampg
*     redirect next plot variables
      logical lsnap
      integer nsnap
      character*8 pgsfile,pghfile,pglfile
      common/rdc_pgsnap/lsnap,nsnap,pgsfile,pghfile,pglfile
*     pgplot width & aspect ratio
      real widthpg,aspectpg
      common/vpc_pgpaper/widthpg,aspectpg
*     input streams
      integer inputc
      common/rdc_inputc/inputc
*     wavelength parameter sets
      character*8 wcfty2(maxnch)
      character*4 vacin2(maxnch)
      double precision helcf2(maxnch)
      integer nwcf2(maxnch),noffs2(maxnch)
      common/vpc_wcpars2/wcfty2,helcf2,nwcf2,noffs2,vacin2
*     multidimensional wavelength coefficients...
      double precision wcfd(maxwco,maxnch)
      common/vpc_wcfd/wcfd
*
*     linear wavelength coefficients
      character*4 chwts
      integer nbr
      double precision ac,bc,ach,achend
      common/rdc_scwv/ac,bc,ach,achend,nbr,chwts
*
*     rebinned data defaults
      integer lxaheld,lxbheld
      common/rd_addv/lxaheld,lxbheld
*     plot stacking
      integer ipgnx,ipgny
      character*60 pglchar(3)
      common/pgnumba/ipgnx,ipgny,pglchar
*     substepping for Voigt profile values
      integer nexpd
      common / nexpd / nexpd
*     scrunch style flag, and log bin indicator
      integer isctype,isclog
      common/rdc_scflag/isctype,isclog
*     Gaussian smoothing
      character*2 chprof
      double precision pfinst
      double precision wfinstd
      integer npinst,npin2
      common/vpc_profwts/wfinstd(maxipp,maxnch),pfinst(maxipp,maxnch),
     :     npinst(maxnch),npin2(maxnch),chprof(maxnch)
      double precision zed
      common/vpc_ewpms/zed
*     common/ewspfc/cfac,sig
      double precision dnshft2(maxnch)
      common/vpc_shft2/dnshft2
*     chunk subdivision variables
      integer nminsubd,nmaxsubd
      double precision bindop
      common/vpc_vpsubd/bindop,nminsubd,nmaxsubd
*     source path
      character*132 csrcpath
      common/vpc_srcpath/csrcpath
*
*     Get source path. This is set up when vpfit is compiled, not
*     here - all this does is set a common block character string
*     for which the factory-supplied default is blank
      call vp_srcpath
*
*     copyright section:
      write(6,*)
      write(6,*) '  ------ RDGEN 10.0 ------'
      write(6,*) 
*     Initially created: 12 Oct 2010 
      lgulio=.true.
      isctype=1
      isclog=0
*     rebinned data start
      lxaheld=1
*     input stream ID's
      inputc=5
      noffs=0
*     vpfit global defaults first
      call vp_startval
      call vp_rdspecial(1)
      call vp_charlims
*     rdgen global defaults
      call rd_compreset
*
*     control for output
      verbose=.false.
*     maximum array length
      ncmax=maxfis
      ng=maxfis
      ngp=0
*     space for only one chunk, so:
      ichunk=1
*     plot defaults
      ipgopen=0
      lsnap=.false.
      nsnap=0
      pgsfile='/xw'
      pghfile='/vcps'
      pglfile='/cps'
      call pldef(maxfis,'rdgen')
*     various default startup values
      call rd_setstart
*     input stream ID's
      inputc=5
      inchstr=' '
      call getenv('RDSTART',inchstr)
      if(inchstr.eq.' ') then
        if(csrcpath(1:1).ne.' ') then
          lcsrc=lastchpos(csrcpath)
          if(csrcpath(lcsrc:lcsrc).eq.'/') then
            inchstr='< '//csrcpath(1:lcsrc)//'rd_start.dat'
           else
            inchstr='< '//csrcpath(1:lcsrc)//'/rd_start.dat'
          end if
         else
          inchstr='?'
        end if
       else
*       have own startup file
        inchstr='< '//inchstr(1:130)
      end if
      goto 11
*	
*     Input stream exhausted section:
*     If console input exhausted, then exit program
 901  if(inputc.eq.5) goto 999
      close(unit=inputc)
      inputc=inputc+1 
*	
*     select option
 1    write(6,'(''>>'',$)')
      read(inputc,'(a)',end=901) inchstr
      if(inchstr(1:1).eq.'!') then
        if(inchstr(2:2).eq.'!') then
*         repeat last useful command
          inchstr=oldchstr
         else
*         treat as a comment - just reflect it
          lench=lastchpos(inchstr)
          write(6,'(a)') inchstr(1:lench)
          goto 1
        end if
      end if
      if(inputc.ne.5) then
*       echo significant characters of the input line
        nx=len(inchstr)
        do while(inchstr(nx:nx).eq.' '.and.nx.gt.1)
          nx=nx-1
        end do
        write(6,'(a)') inchstr(1:nx)
       else
*       store input value for possible re-use
        oldchstr=inchstr
      end if
 11   call dsepvar(inchstr,10,dv,iv,cv,nv)
      do i=1,24
        rvss(i)=dv(i)
      end do
*     deal with long names
      call rd_comalias(cv(1))
*
*     internal aliases
*     PL -> SP
      if(cv(1)(1:2).eq.'pl'.or.cv(1)(1:2).eq.'PL') cv(1)='sp'
*
*     Command section:
*
*     <: Command stream from a file
      if(cv(1)(1:1).eq.'<') then
        if(nv.ge.2) then
*         second string is filename
          inchstr=cv(2)
         else
*	  filename concatenated with command
          inchstr=cv(1)(2:60)
        end if
        if(inchstr(1:1).ne.' '.and.inputc.gt.1) then
          inputc=inputc-1
          open(unit=inputc,file=inchstr,status='old',err=902)
          goto 1
        end if
 902    continue
        if(inputc.eq.1) then
          write(6,*) 'Input nesting depth > 4'
        end if
        inputc=5
        write(6,*) 'Failed to reassign input'
        write(6,*) ' .. revert to standard input'
        goto 1
      end if
* 
*     A:
      if(cv(1)(1:1).eq.'a'.or.cv(1)(1:1).eq.'A') then
*	AB: generate absorption line list
        if(cv(1)(2:2).eq.'b'.or.cv(1)(2:2).eq.'B') then
          call rd_ablin(da,de,ca,ds,nct,cv(2),cv(3))
          goto 1
        end if
*	AD: spectrum addition
        if(cv(2)(1:1).ne.'a') cv(2)(1:2)='  '
        if(cv(1)(2:2).eq.'d'.or.cv(1)(2:2).eq.'D') then
          call rd_scrn(da,de,nct,ds,re,ncmax,dh,dhe,ng,ngp,ind,
     :          cv(2)(1:2),drv)
          goto 1
        end if
*       AR: Reverse data array, leaving wavelengths as they were
        if(cv(1)(2:2).eq.'r'.or.cv(1)(2:2).eq.'R') then
          write(6,*) 'Reversing data arrays'
          nx=nct/2
          nctp1=nct+1
          do i=1,nx
            k=nctp1-i
            temp=da(i)
            da(i)=da(k)
            da(k)=temp
            temp=de(i)
            de(i)=de(k)
            de(k)=temp
            temp=ca(i)
            ca(i)=ca(k)
            ca(k)=temp
          end do
          goto 1
        end if
*       AT: atomic data table change
        if(cv(1)(2:2).eq.'t'.or.cv(1)(2:2).eq.'T') then
          call vp_ewred(1)
          goto 1
        end if
      end if
*
*     B:
      if(cv(1)(1:1).eq.'b'.or.cv(1)(1:1).eq.'B') then
*       BB: generate a black body curve and put it in da
        if(cv(1)(2:2).eq.'b'.or.cv(1)(2:2).eq.'B') then
          call rd_fluxbb(da,nct)
          if(verbose) then
            do i=1,nct
              t=wval(dble(i))
              write(6,*) i,t,da(i)
            end do
          end if
          goto 1
        end if
      end if
*
*     C:
      if(cv(1)(1:1).eq.'c'.or.cv(1)(1:1).eq.'C') then
*       CR: control variable reset
        if(cv(1)(2:2).eq.'r'.or.cv(1)(2:2).eq.'R') then
          call rd_comreset
          goto 1
        end if
*       CY (or CP): copy data from array to array
        if(cv(1)(2:2).eq.'y'.or.cv(1)(2:2).eq.'Y'.or.
     :         cv(1)(2:2).eq.'p'.or.cv(1)(2:2).eq.'P') then
          call rd_cydata(da,de,drms,nct,dh,dhe,ngp,ca,cad,cv)
          goto 1
        end if
      end if
*
*     D:
      if(cv(1)(1:1).eq.'d'.or.cv(1)(1:1).eq.'D') then
*       DA: data to ASCII file
        if(cv(1)(2:2).eq.'a'.or.cv(1)(2:2).eq.'A') then
          if (cv(2)(1:1).ne.' ') then
            open(unit=16,file=cv(2),status='new',err=901)
          end if
          do j=1,nct
            dtemp=wval(dble(j))
            if(de(j).ge.0.0d0.and.drms(j).ge.0.0d0) then
              temp1=sqrt(de(j))
              temp2=sqrt(drms(j))
             else
              temp1=de(j)
              temp2=drms(j)
            end if
            write(16,*) dtemp,da(j),temp1,ca(j),temp2
          end do
          close(unit=16)
          goto 1
        end if
*       DC: divide the data by the continuum
        if(cv(1)(2:2).eq.'c'.or.cv(1)(2:2).eq.'C') then
          call rd_dvcont(da,de,drms,ca,cad,nct,dv,nv)
          goto 1
        end if
*       DM: data arithmetic manipulation
        if(cv(1)(2:2).eq.'m'.or.cv(1)(2:2).eq.'M') then
          call rd_dmanip(da,de,drms,ca,nct,cv(2))
          goto 1
        end if
*       DX: multiply the data, error and continuum by a constant
        if(cv(1)(2:2).eq.'x'.or.cv(1)(2:2).eq.'X'.or.
     :            cv(1)(2:2).eq.'*') then
          if(dv(2).eq.0.0d0) then
            if(cv(2)(1:1).eq.'?') then
              write(6,*) 'dx #   - data, error & continuum *#'
              write(6,*) 'dx # d - data only *#'
              write(6,*) 'dx c   - data only * continuum'
              write(6,*) 'dx c a - data & error * continuum'
              goto 1
            end if
            if(cv(2)(1:1).eq.'C'.or.cv(2)(1:1).eq.'c') then
*             multiply data by continuum, leaving error and continuum
*             unchanged
              write(6,*) 'data multiplied by continuum'
              do j=1,nct
                da(j)=da(j)*ca(j)
              end do
              if(cv(3)(1:1).ne.' ') then
                write(6,*) ' and so is the error'
                do j=1,nct
                  de(j)=de(j)*ca(j)*ca(j)
                end do
              end if
*             branch not essential, just to avoid next few lines
              goto 1
             else
              write(6,*) 'Multiply by what?'
              read(inputc,*,end=901) dv(2)
            end if
          end if
          if(dv(2).ne.0.0d0) then
            if(cv(3)(1:1).eq.'d'.or.cv(3)(1:1).eq.'D') then
*             deal with data only, leaving error and continuum
*             ONLY if single command line
              do j=1,nct
                da(j)=da(j)*dv(2)
              end do
              write(6,*) 'Error and continuum unchanged'
             else
              do j=1,nct
                da(j)=da(j)*dv(2)
                ca(j)=ca(j)*dv(2)
                de(j)=de(j)*dv(2)*dv(2)
                drms(j)=drms(j)*dv(2)*dv(2)
              end do
            end if
          end if
          goto 1
        end if
*	D+: add constant to the data and continuum
        if(cv(1)(2:2).eq.'+') then
          if(dv(2).eq.0.0d0) then
            if(cv(2)(1:1).eq.'C'.or.cv(2)(1:1).eq.'c') then
*             add continuum to data, leaving error and continuum
*             unchanged
              write(6,*) 'continuum added to data'
              do j=1,nct
                da(j)=da(j)+ca(j)
              end do
              goto 1
            end if
            write(6,*) 'Add what?'
            read(inputc,*,end=901) dv(2)
          end if
          if(dv(2).ne.0.0d0) then
            if(cv(3)(1:1).eq.'d'.or.cv(3)(1:1).eq.'D') then
*             data only, don't touch the continuum
              do j=1,nct
                da(j)=da(j)+dv(2)
              end do
              write(6,*) 'Error and continuum unchanged'
             else
              do j=1,nct
                da(j)=da(j)+dv(2)
                ca(j)=ca(j)+dv(2)
              end do
            end if
          end if
          goto 1
        end if
      end if
*
*     E:
      if(cv(1)(1:1).eq.'e'.or.cv(1)(1:1).eq.'E') then
*       EA: extended absorption (test routine only)
        if(cv(1)(2:2).eq.'a'.or.cv(1)(2:2).eq.'A') then
          call rd_extabs(da,de,ca,cad,re,ds,hc,nct)
          goto 1
        end if
*       EC: set error to a constant value (p1)
        if(cv(1)(2:2).eq.'c'.or.cv(1)(2:2).eq.'C') then
          if(dv(2).le.0.0d0) then
            write(6,*) ' What value?'
            read(5,*) temp
           else
            temp=dv(2)
          end if
          temp=temp*temp
          do j=1,nct
            de(j)=temp
          end do
          goto 1
        end if
*       EL: Error = max (da,p1*ca) if >0, left otherwise
        if(cv(1)(2:2).eq.'l'.or.cv(1)(2:2).eq.'L') then
          xr=dv(2)
          if(xr.le.0.0d0) then
            xr=0.6d0
          end if
          write(6,'(a,f5.2,a)') 'Error level (if >0) replaced by'//
     :      'max(data,',xr,'*continuum)'
          do j=1,nct
            temp=xr*ca(j)
            if(de(j).gt.0.0d0.and.da(j).lt.temp) then
              de(j)=temp*temp
            end if
          end do
          goto 1
        end if
*       ER: Error rescale using database/<SCLFILE>
        if(cv(1)(2:2).eq.'r'.or.cv(1)(2:2).eq.'R') then
*         get the scaling data
          status=0
          call rd_sclsig(de,nct,status)
          write(6,*) 'Maybe... someday...'
          goto 1
        end if
*       EM: Error (and RMS) * constant
        if(cv(1)(2:2).eq.'m'.or.cv(1)(2:2).eq.'M'.or.
     1         cv(1)(2:2).eq.'*') then
          if(dv(2).gt.0.0d0) then
            dv(2)=dv(2)*dv(2)
            do j=1,nct
              de(j)=de(j)*dv(2)
              drms(j)=drms(j)*dv(2)
            end do
           else
            write(6,*) 'Error unchanged'
          end if
          goto 1
        end if
      end if  
*
*     F:
      if(cv(1)(1:1).eq.'f'.or.cv(1)(1:1).eq.'F') then
*       FO: file open for a specific unit number
        if(cv(1)(2:2).eq.'o'.or.cv(1)(2:2).eq.'O') then
          if(iv(2).gt.0.and.iv(2).ne.5.and.iv(2).ne.6) then
            close(unit=iv(2),err=601)
 601        continue
            if(cv(3)(1:1).ne.' ') then
              lenfo=lastchpos(cv(3))
              open(unit=iv(2),file=cv(3)(1:lenfo),status='unknown',
     :               err=1)
            end if
          end if
          goto 1
        end if
*       FQ: file query - which datafiles are there?
        if(cv(1)(2:2).eq.'q'.or.cv(1)(2:2).eq.'Q') then
          call rd_qdatafiles
          goto 1
        end if
*       FT: file of optical depths
        if(cv(1)(2:2).eq.'t'.or.cv(1)(2:2).eq.'T') then
          inchstr=' '
          call rd_gpsim(da,de,nct)
*         plot range reset
          nst=1
          lc1=nst
          ncn=nct
          lc2=ncn
          goto 1
        end if
      end if
*
*     G:
      if(cv(1)(1:1).eq.'g'.or.cv(1)(1:1).eq.'G') then
*       GM: generate metal list (tempmet.junk)
        if(cv(1)(2:2).eq.'m'.or.cv(1)(2:2).eq.'M') then
          if(cv(2)(1:1).ne.' ') then
            inchstr=cv(2)
           else
            write(6,*) 'Accumulated redshift file?'
            read(inputc,'(a)') inchstr
          end if
          lenchs=lastchpos(inchstr)
*         unit 29 not used anywhere else, use as default temporary open
*         for input. Unit 16 for local output.
          open(unit=29,file=inchstr(1:lenchs),status='old',err=1)
          open(unit=16,file='tempmet.junk',status='unknown',err=968)
          cv(1)(1:1)='!'
          do while(cv(1)(1:1).ne.' ')
            read(29,'(a)',end=967) inchstr
            call dsepvar(inchstr,8,dv,iv,cv,nv)
            if(ucase(cv(2)(1:1))) then
              ipk=3
             else
              ipk=2
            end if
            if(dv(ipk).eq.0.0d0) then
              lenchs=lastchpos(cv(ipk))
              if(lenchs.gt.2) then
                inchstr=cv(ipk)(1:lenchs-1)
                call dsepvar(inchstr,1,dv,iv,cv,nv)
                ipk=1
                if(dv(1).eq.0.0d0) then
                  inchstr=inchstr(1:lenchs-2)
                  call dsepvar(inchstr,1,dv,iv,cv,nv)
                end if
              end if
            end if
            write(16,'(a,f10.6)') 'pp z ',dv(ipk)
            write(16,'(a)') 'pg'
            do j=1,3
              write(16,'(a)') ' '
            end do
          end do
 967      close(unit=16)
 968      close(unit=29)
*         transfer control
          inchstr='< tempmet.junk'
          goto 11
        end if
*       GP: generate profiles using fort.13 type data
        if(cv(1)(2:2).eq.'p'.or.cv(1)(2:2).eq.'P') then
          lsmth=.true.
          nsubstep=0
*         turn off pixel profile for now
          npinst(1)=0
          chprof(1)=' '
          if(cv(2)(1:1).eq.'s'.or.cv(2)(1:1).eq.'S'.or.
     1       cv(2)(1:1).eq.'p'.or.cv(2)(1:1).eq.'P') then
*           set subdivision within range
            if(iv(2).lt.nminsubd) then
              iv(2)=nminsubd
             else
              if(iv(2).gt.nmaxsubd) then
                iv(2)=nmaxsubd
               else
                iv(2)=11
              end if
            end if
            write(6,'(a,i4)') 'Substep default:',iv(2)
          end if
          if(nv.ge.2) then
*           pixel resolution readin
            if(cv(2)(1:1).eq.'p'.or.cv(2)(1:1).eq.'P') then
              if(cv(2)(3:3).eq.'=') then
                fpxname=cv(2)(4:60)
               else
                fpxname=cv(3)
              end if
              istat=0
              nrow=1
              call vp_getres(fpxname,1,nrow,istat)
            end if
            if(iv(2).gt.0) then
*             reset nexpd?
              if(dv(2)-dble(iv(2)).lt.1.0d-4) then
                nexpd=iv(2)
                write(6,*) 'Profile substep parameter = ',nexpd
              end if
            end if
            do j=2,nv
              if(cv(j)(1:4).eq.'nosm'.or.cv(j)(1:4).eq.'noin') then
                lsmth=.false.
              end if
            end do
*           substepping?
            if(iv(2).gt.1) then
*             is a number of substeps
              nsubstep=iv(2)
            end if
          end if
          do i=1,nct
            re(i)=cad(i)
          end do
*         print resolution variables
          if(verbose) then
            write(6,*) 'RES:',npinst(1),swres(1,ichunk),
     :            swres(2,ichunk),swres(3,ichunk),swres(4,ichunk)
          end if
          if(nsubstep.gt.1.or.cv(2)(1:1).eq.'p'.or.
     :              cv(2)(1:1).eq.'P') then
            call rd_gprofe(ca,cad,nct,lsmth,nsubstep)
           else
*           may drop this later - should be catered for by rd_gprofe
            call rd_gprof(ca,cad,re,nct,lsmth)
          end if
          goto 1
        end if
      end if
*
*     H:
*     Help list is a special case:
      if(cv(1)(1:2).eq.'he'.or.cv(1)(1:2).eq.'HE'.or.
     1       cv(1)(1:1).eq.'?') then
*       print help list
        call rd_prhelp(cv(2))
        goto 1
      end if
*
*     Rest if the 'H' commands:
      if(cv(1)(1:1).eq.'h'.or.cv(1)(1:1).eq.'H') then
*       HD: dust estimator from Balmer lines -> P-a
        if(cv(1)(2:2).eq.'d'.or.cv(1)(2:2).eq.'D') then
          chwhat=cv(2)(1:8)
          if(chwhat(1:1).eq.' ') then
            chwhat='Pa/Ha'
          end if
          call rd_hdust(da,ca,nct,dh,ng,chwhat)
          goto 1
        end if
      end if
*
*     I:
      if(cv(1)(1:1).eq.'i'.or.cv(1)(1:1).eq.'I') then
*       IE: interpolate error array between non-zero points
        if(cv(1)(2:2).eq.'e'.or.cv(1)(2:2).eq.'E') then
          jlow=0
          jhigh=1
 603      continue
          do while(de(jhigh).le.0.0d0.and.jhigh.lt.nct)
            jhigh=jhigh+1
          end do
*         set end cases
          if(jlow.le.0) then
            jlow=1
            de(jlow)=de(jhigh)
          end if
          if(jhigh.eq.nct.and.de(jhigh).le.0.0d0) then
            de(jhigh)=de(jlow)
          end if
*         PIXEL linear interpolation
          if(de(jlow).gt.0.0d0.and.de(jhigh).gt.0.0d0.and.
     :         jhigh.gt.jlow) then
            pxdf=dble(jhigh-jlow)
            elow=sqrt(de(jlow))/pxdf
            ehigh=sqrt(de(jhigh))/pxdf
            do j=jlow,jhigh
              temp=elow*dble(jhigh-j)+ehigh*(j-jlow)
              de(j)=temp*temp
            end do
          end if
          if(jhigh.lt.nct) then
            jlow=jhigh
            jhigh=jhigh+1
            goto 603
          end if 
          goto 1
        end if
      end if
*
*     L:
      if(cv(1)(1:1).eq.'l'.or.cv(1)(1:1).eq.'L') then
*       LE: set data length
        if(cv(1)(2:2).eq.'e'.or.cv(1)(2:2).eq.'E') then
          nct=iv(2)
          if(nct.le.0) nct=2000
          goto 1
        end if
      end if
*
*     M:
      if(cv(1)(1:1).eq.'m'.or.cv(1)(1:1).eq.'M') then
*       MC: multiply the data by the continuum
        if(cv(1)(2:2).eq.'c'.or.cv(1)(2:2).eq.'C') then
*         this makes sense if have a line in unit continuum
          do i=1,nct
            da(i)=da(i)*ca(i)
          end do
          goto 1
        end if
*       ME: modify error arrays
        if(cv(1)(2:2).eq.'e'.or.cv(1)(2:2).eq.'E') then
          call rd_moderrs(da,de,drms,ca,nct)
          goto 1
        end if
*       MF: median filter the data
        if(cv(1)(2:2).eq.'f'.or.cv(1)(2:2).eq.'F') then
          temp=0.5d0
          if(iv(2).gt.0.0) then
            nfilt=iv(2)
            if(dv(3).gt.0.0d0.and.dv(3).lt.1.0d0) then
              temp=dv(3)
            end if
           else
            write(6,*) 'Median length (pixels) [NO DEFAULT]'
            read(5,*) nfilt
          end if
          do j=1,nct
            ca(j)=da(j)
          end do
          call medianv(ca,ds,nct,nfilt,temp)
          goto 1
        end if
*       MU: multiprocess data files
        if(cv(1)(2:2).eq.'u'.or.cv(1)(2:2).eq.'U') then
          if(cv(2)(1:1).eq.' ') then
            write(6,*) 'File with list of filenames?'
            read(5,'(a)') cv(2)
            write(6,'(a)') 'Command template file?'
            read(5,'(a)') cv(3)
           else
            if(cv(3)(1:1).eq.' ') then
              write(6,'(a)') 'Command template file?'
              read(5,'(a)') cv(3)
            end if
          end if
*         dot handling flag, from command line only
          nostrip=0
          if(cv(4)(1:2).eq.'no'.or.cv(4)(1:2).eq.'NO') then
            nostrip=1
          end if
          open(unit=22,file=cv(2),status='old',err=930)
          open(unit=23,file=cv(3),status='old',err=931)
          open(unit=24,file='tempjunk.tmp',status='unknown',err=932)
          call rd_multiset(ind,nostrip)
*         now pretend input from tempjunk.tmp
          close(unit=24)
          close(unit=23)
          close(unit=22)
          inchstr='< tempjunk.tmp'
          goto 11
*         Error messages
 932      write(6,*) 'Failed to open temporary file'
          goto 1
 931      write(6,*) 'No command template'
          goto 1
 930      write(6,*) 'No filename list'
          goto 1
        end if
      end if
*
*     N:
      if(cv(1)(1:1).eq.'n'.or.cv(1)(1:1).eq.'N') then
*       NO: add noise to continnum and put result in workspace,
*	updating the error array
        if(cv(1)(2:2).eq.'o'.or.cv(1)(2:2).eq.'O') then
*         2nd parameter 'data' may be used in rd_adnois
          call rd_adnois(da,de,ca,nct)
          goto 1
        end if
      end if
*
*     P:
      if(cv(1)(1:1).eq.'p'.or.cv(1)(1:1).eq.'P') then
*       PC: close the plot device
        if(cv(1)(2:2).eq.'c'.or.cv(1)(2:2).eq.'C') then
          if(ipgopen.ne.0) then
            ipgopen=0
            call vp_pgend
          end if
          if(iv(2).ge.1) then
            ipgnx=iv(2)
          end if
          if(iv(3).ge.1) then
            ipgny=iv(3)
          end if
          if(dv(5).gt.0.0d0) then
            widthpg=dv(4)
            aspectpg=dv(5)
            if(widthpg.gt.0.0) then
              write(6,'(a,f5.2,f5.2)') 
     :           'Paper width, aspect ratio now ',dv(4),dv(5)
             else
              write(6,'(a,f5.2)') 
     :           'Paper width max, aspect ratio now ',dv(5)
            end if
          end if
          goto 1
        end if
*
*       PF: give list of filenames available for stacked cursor plots
        if(cv(1)(2:2).eq.'f'.or.cv(1)(2:2).eq.'F') then
          if(cv(2)(1:1).eq.'0') then
            call rd_intvtab
           else
            call rd_cufilnm(cv(2))
          end if
          goto 1
        end if
*
*       PG: plot with cursor commands
        if(cv(1)(2:2).eq.'g'.or.cv(1)(2:2).eq.'G') then
          call rd_cuplot(da,de,drms,ca,nct)
          goto 1
        end if
*
*       PP: preset parameters associated with plots
        if(cv(1)(2:2).eq.'p'.or.cv(1)(2:2).eq.'P') then
          call rd_pmpset(cv,dv,nv)
          goto 1
        end if
*
*       PW: print out wavelength coefficients
        if(cv(1)(2:2).eq.'w'.or.cv(1)(2:2).eq.'W') then
          if(iv(3).gt.0.and.iv(3).gt.iv(2)) then
            nwcotem=min(iv(3),maxwco)
            nwcolow=max(iv(2),1)
           else
            if(iv(2).gt.0) then
              nwcotem=min(iv(2),maxwco)
              nwcolow=1
             else
              nwcotem=min(nwco,10)
              nwcotem=min(nwcotem,maxwco)
              nwcolow=1
            end if
          end if
          write(6,*) wcftype(1:6),' wavelengths'
          do i=nwcolow,nwcotem
            write(6,*) i,wcf1(i),wcfd(i,1)
          end do
*         and the linear ones
          if(ach.gt.0.0d0.and.bc.ne.0.0d0) then
            write(6,*) 'Linearized coeffts:'
            write(6,*) ach,bc,' ',chwts(1:3)
            write(6,*) '   for ',ngp,' data points'
           else
            write(6,*) 'Linearized coeffts not set'
          end if
          goto 1
        end if
      end if
*
*     R:
      if(cv(1)(1:1).eq.'r'.or.cv(1)(1:1).eq.'R') then
*
*       RC: stored data into workspace
        if(cv(1)(2:2).eq.'c'.or.cv(1)(2:2).eq.'C') then
          ind=-3
          if(iv(2).gt.0) then
            ind=-iv(2)
          end if
          call rd_daswap(da,de,ca,nct,ha,he,hc,nht,ind)
          goto 1
        end if
*
*       RD: read FITS/IRAF data into workspace
        if(cv(1)(2:2).eq.'d'.or.cv(1)(2:2).eq.'D') then
*	  the second parameter, if present (and 1, 2 ,or 3), 
*	  gives the number of datasets (so can avoid reading the
*         continuum, and the error)
          numdat=3
          if(iv(2).eq.1.or.iv(2).eq.2) then
            numdat=iv(2)
            norx=0
            inchstr=' '
           else
*	    unless it is the filename, in which case 
*	    (name, order, numdat)
            if(cv(1)(1:1).ne.' ') then
              inchstr=cv(2)
              norx=iv(3)
              if(iv(4).eq.1.or.iv(4).eq.2) numdat=iv(4)
            end if
          end if
*         clear resolution information first
          chprof(1)=' '
          npinst(1)=0
          swres(1,ichunk)=0.0d0
          swres(2,ichunk)=0.0d0
          call vp_readfs(da,de,drms,ca,ncmax,nct,werr,
     1          norx,inchstr,numdat,ichunk)
          if(verbose) then
*           wcf1 from fits reader
            write(6,*) 'First two wavelength coeffts ',wcf1(1),wcf1(2)
          end if
*	  copy wavelength stuff to appropriate arrays
          do i=1,maxwco
            wcfd(i,1)=wcf1(i)
          end do
          call vp_cfcopy(1,2)
          do i=1,nct
            cad(i)=ca(i)
          end do
*	  plot range reset
          nst=1
          lc1=nst
          ncn=nct
          lc2=ncn
          goto 1
        end if
*
*       RE: REsolution reset (to filename)
        if(cv(1)(2:2).eq.'e'.or.cv(1)(2:2).eq.'E') then
          if(cv(2)(1:1).eq.' ') then
*           request filename
            write(6,*) 'Resolution file?'
            read(inputc,'(a)') cv(2)
          end if
          lenq=lastchpos(cv(2))
          write(6,*) 'Resolution file changed to ',cv(2)(1:lenq)
          nrow=1
          istat=0
          call vp_getres(cv(2),1,nrow,istat)
          if(istat.ne.0) write(6,*) 'Resolution read fail'
          goto 1
        end if
*
*	RM: read multispec unformatted
        if(cv(1)(2:2).eq.'m'.or.cv(1)(2:2).eq.'m') then
          if(cv(1)(1:1).ne.' ') then
            inchstr=cv(2)
            norx=iv(3)
          end if
          call rd_mattrd(da,nct,norx,inchstr)
          do i=1,maxwco
            wcfd(i,1)=wcf1(i)
          end do
          nwcf2(1)=nwco
          vacin2(1)=vacind
          wcfty2(1)=wcftype
          noffs2(1)=noffs
          helcf2(1)=helcfac
          do i=1,nct
            cad(i)=ca(i)
            ca(i)=1.0d0
            de(i)=1.0d0
          end do
*	  plot range reset
          nst=1
          lc1=nst
          ncn=nct
          lc2=ncn
          goto 1
        end if
*       RS: re-read setup file
        if(cv(1)(2:2).eq.'s'.or.cv(1)(2:2).eq.'S') then
          call vp_rdspecial(1)
          goto 1
        end if
      end if
*
*     S:
      if(cv(1)(1:1).eq.'s'.or.cv(1)(1:1).eq.'S') then
*       SE: set error array
        if(cv(1)(2:2).eq.'e'.or.cv(1)(2:2).eq.'E') then
          temp=dv(2)
          if(temp.ge.0.0d0) then
            temp=temp*temp
           else
            temp=-temp*temp
          end if
          do j=1,nct
            de(j)=temp
          end do
          goto 1
        end if
*       SG: Gaussian smooth the data
        if(cv(1)(2:2).eq.'g'.or.cv(1)(2:2).eq.'G') then
          swres(1,ichunk)=0.0d0
          swres(2,ichunk)=0.0d0
          zed=1.0d0
          dnshft2(1)=0.0d0
          losm=1
          lhsm=nct
          if(dv(2).gt.0.0d0.or.dv(3).gt.0.0d0) then
            swres(2,ichunk)=dv(2)/7.05957d5
            swres(1,ichunk)=dv(3)/2.354820045d0
            nswres(ichunk)=2
            if(iv(5).gt.0.and.iv(6).gt.0) then
*             restricted range smoothing
              losm=iv(5)
              lhsm=iv(6)
              if(lhsm.le.losm) then
                losm=1
                lhsm=nct
              end if
              write(6,*) 'over range',losm,lhsm
            end if
           else
*           ask
            write(6,'('' FWHM (km/s & A)'')')
            read(5,'(a)',end=1) inchstr
            call dsepvar(inchstr,2,dv,iv,cv,nv)
            swres(2,ichunk)=dv(1)/7.05957d5
            swres(1,ichunk)=dv(2)/2.354820045d0
            nswres(ichunk)=2
          end if
          npinst(1)=0
          chprof(1)=' '
          call vp_spread(da,losm,lhsm,ichunk, ds)
          do i=1,nct
            da(i)=ds(i)
          end do
          goto 1
        end if
*       SM: Boxcar smooth the data
        if(cv(1)(2:2).eq.'m'.or.cv(1)(2:2).eq.'M') then
          jsm=iv(2)/2
          iv(2)=2*jsm+1
          if(jsm.gt.0) then
            write(6,*) 'Boxcar smoothing:',iv(2),' pixels'
            do j=1,nct
              klo=j-jsm
              if(klo.lt.1) klo=1
              khi=j+jsm
              if(khi.gt.nct) khi=nct
              pssum=0.0d0
              dssum=0.0d0
              do k=klo,khi
                pssum=pssum+1.0d0
                dssum=dssum+da(k)
              end do
              if(pssum.gt.0.0d0) then
                ds(j)=dssum/pssum
               else
                ds(j)=da(j)
              end if
            end do
            do j=1,nct
              da(j)=ds(j)
            end do
          end if
          goto 1
        end if
*	SP: spectrum plot using PGPLOT
        if(cv(1)(2:2).eq.'p'.or.cv(1)(2:2).eq.'P') then
*	  need preliminaries for PGPLOT
          ind=1
          call splot(da,de,drms,ca,nct,ind)
          goto 1
        end if
*
*	ST: store spectrum 
        if(cv(1)(2:2).eq.'t'.or.cv(1)(2:2).eq.'T') then
          ind=1
          call rd_daswap(da,de,ca,nct,ha,he,hc,nht,ind)
          goto 1
        end if
*
*	SV: Set internal variables
        if(cv(1)(2:2).eq.'v'.or.cv(1)(2:2).eq.'V') then
          call rd_setintv
          goto 1
        end if
*
*	SW: swap spectrum with internal store
        if(cv(1)(2:2).eq.'w'.or.cv(1)(2:2).eq.'W') then
          ind=0
          call rd_daswap(da,de,ca,nct,ha,he,hc,nht,ind)
          goto 1
        end if
*
*	SZ: stack redshift corrected spectra
        if(cv(1)(2:2).eq.'z'.or.cv(1)(2:2).eq.'Z') then
          call rd_stackz(da,de,nct,ds,re,dh,dhe,ng,ngp)
          goto 1
        end if
*
*       SYSTEM calls
*       sys - call system p1=cmd
        if(cv(1)(1:3).eq.'sys') then
          if(cv(2)(1:1).ne.' ') then
            istat=0
            lenq=lastchpos(cv(2))
            inchstr=cv(2)(1:lenq)
            lenq=lastchpos(inchstr)
*           write(6,*) 'Trying: ',cv(2)(1:lenq)
            call system(cv(2)(1:lenq),istat)
            if(istat.gt.0) then
              write(6,*) 'Failed ',istat
              istat=0
            end if
          end if
          goto 1
        end if
      end if
*
*     T:
      if(cv(1)(1:1).eq.'t'.or.cv(1)(1:1).eq.'T') then
*
*       Threshold Error - set error negative (i.e. bad pixel) if
*       error above some threshold
        if(cv(1)(2:2).eq.'e'.or.cv(1)(2:2).eq.'E') then
          if(cv(2)(1:1).eq.'?') then
            write(6,*) 'TE: all data points where error greater than'
            write(6,*) 'the value given are flagged as bad.'
            write(6,*) 'Does nothing if value is zero or negative.'
            goto 1
          end if
          if(dv(2).gt.0.0d0) then
            temp=dv(2)*dv(2)
            do j=1,nct
              if(de(j).gt.temp) de(j)=-1.0d0
            end do
           else
            write(6,*) 'Threshold not specified - no action'
          end if
          goto 1
        end if  
*	Estimate mean (1-e**(-tau)) [can do this with m-m in pg as well]
        if(cv(1)(2:2).eq.'m'.or.cv(1)(2:2).eq.'M') then
          if(nct.gt.0) then
            wlo=dv(2)
            whi=dv(3)
            call rd_meantau(da,de,ca,nct,wlo,whi,dv(4))
           else
            write(6,*) 'Data length zero'
          end if
          goto 1
        end if
*       TP: Taesun stacked velocity plots
        if(cv(1)(2:2).eq.'p'.or.cv(1)(2:2).eq.'P') then
          call rd_tgplots
*         branch to handling preset input string
          goto 11
        end if
      end if
*
*     U:
      if(cv(1)(1:1).eq.'u'.or.cv(1)(1:1).eq.'U') then
*	UC: upper limit column densities
        if(cv(1)(2:2).eq.'c'.or.cv(1)(2:2).eq.'C') then
          if(cv(2)(1:1).ne.'s'.and.iv(2).le.0) then
            call rd_ulims(da,de,ca,cad,re,ds,nct,ichunk)
            goto 1
           else
*    was    call rd_ulimse(da,de,drms,ca,cad,re,ds,nct,ichunk)
            call rd_ulimse(da,de,ca,cad,ds,nct,ichunk)
            goto 1
          end if
        end if
*       UL: list lines with data near continuum (with fit)
        if(cv(1)(2:2).eq.'l'.or.cv(1)(2:2).eq.'L') then
          chout=cv(2)
          lgulio=.true.
*         undocumented 3rd param 'noprompt' option
          if(cv(3)(1:4).eq.'nopr') lgulio=.false.
          call rd_ulinfo(da,de,ca,nct,chout,lgulio)
          goto 1
        end if
*	UN: write unit continuum
        if(cv(1)(2:2).eq.'n'.or.cv(1)(2:2).eq.'N') then
          if(nct.gt.0) then
            do i=1,nct
              ca(i)=1.0d0
              cad(i)=1.0d0
            end do
           else
            write(6,*) 'Data length zero'
          end if
          goto 1
        end if
      end if
*
*     V:
      if(cv(1)(1:1).eq.'v'.or.cv(1)(1:1).eq.'V') then
*       VB: set verbose mode
        if(cv(1)(2:2).eq.'b'.or.cv(1)(2:2).eq.'B'.or.
     :     cv(1)(2:2).eq.'e'.or.cv(1)(2:2).eq.'E') then
          if(cv(2)(1:1).ne.'f'.and.cv(2)(1:1).ne.'F') then
            verbose=.true.
           else
            verbose=.false.
          end if
          goto 1
        end if
*       VW: convert velocities to wavelengths
        if(cv(1)(2:2).eq.'w'.or.cv(1)(2:2).eq.'W') then
          if(cv(2)(1:1).eq.' ') then
            write(6,*) 'Enter reference wavelength, offset'
            read(5,'(a)') inchstr
            call dsepvar(inchstr,2,dv,iv,cv,nv)
            dv(3)=dv(2)
            dv(2)=dv(1)
          end if
          do j=1,nct
            wcfd(j,1)=dv(2)*(1.0d0+(wval(dble(j))+dv(3))/
     :           2.99792458d5)
          end do
          do j=1,nct
            wcf1(j)=wcfd(j,1)
          end do
          nwco=nct
          goto 1
        end if
      end if
*
*
*     W:
      if(cv(1)(1:1).eq.'w'.or.cv(1)(1:1).eq.'W') then
*
*	WA: wavelength coefficient reset:
        if(cv(1)(2:2).eq.'a'.or.cv(1)(2:2).eq.'A') then
          write(6,*) 'New wavelength coefficients? (one line)' 
          read(inputc,'(a)') inchstr
          call dsepvar(inchstr,6,dv,iv,cv,nv)
          write(6,*) 'New wavelength coefficients:'
          nvxx=min0(nv,6)
          write(6,'(f15.3,f12.7,1pe12.4,3e12.4)') (dv(i),i=1,nvxx)
          if(wcftype(1:4).eq.'arra') then
            do i=1,nct
              wcf1(i)=dv(1)+dble(i)*dv(2)
            end do
           else
            do i=1,nv
              wcf1(i)=dv(i)
            end do
            if(maxwco.gt.nv) then
              do i=nv+1,maxwco
                wcf1(i)=0.0d0
              end do
            end if
          end if
          do i=1,maxwco
            wcfd(i,1)=wcf1(i)
          end do
          goto 1
        end if
*
*       WL: wavelength coefficient reset to linear:
        if(cv(1)(2:2).eq.'l'.or.cv(1)(2:2).eq.'L') then
          call rd_readlinwav(nct,da,de,ca,drms)
          goto 1
        end if
*      
*       WR: write out file (FITS)
        if(cv(1)(2:2).eq.'r'.or.cv(1)(2:2).eq.'R') then
          status=0
          arg2=cv(2)(1:24)
          arg3=cv(3)(1:4)
          if(arg3(1:4).eq.'real'.or.arg3(1:4).eq.'REAL') then
            write(6,*) 'Data written as single precision'
          end if
          call rd_wrfits(da,de,drms,ca,nct,arg2,arg3,status)
          goto 1
        end if
*      
*       WT: write out file as text (ascii)
        if(cv(1)(2:2).eq.'t'.or.cv(1)(2:2).eq.'T') then
          if(cv(2)(1:1).ne.' ') then
            open(unit=17,file=cv(2), status='unknown',err=993)
          end if
          if(cv(3)(1:1).eq.'a'.or.cv(3)(1:1).eq.'A') then
            do j=1,nct
              xtem1d=wval(dble(j))
              xerr=sqrt(de(j))
              temp=sqrt(drms(j))
              write(17,*) xtem1d,da(j),xerr,ca(j),temp
            end do
           else
            do j=1,nct
              xtem1d=wval(dble(j))
              xerr=sqrt(de(j))
              write(17,*) xtem1d,da(j),xerr,ca(j)
            end do
          end if
          close (unit=17)
          goto 1
 993      write(6,*) 'Output file not opened'
          goto 1
        end if
*	      
*       WW: write out wavelength file (FITS)
        if(cv(1)(2:2).eq.'w'.or.cv(1)(2:2).eq.'W') then
          status=0
          arg2=cv(2)(1:24)
          call rd_wrwcfits(wcf1,nct,arg2,status)
          goto 1
        end if
*
*       WX: wavelengths x constant, to reset wavelength scale
        if(cv(1)(2:2).eq.'x'.or.cv(1)(2:2).eq.'X') then
          if(dv(2).gt.0.0d0) then
            write(6,'(a,f6.3)') 
     :          'Workspace wavelengths multiplied by',dv(2)
            if(wcftype(1:3).eq.'log') then
*             if wavelengths logaritmic, just add to first term
              wcf1(1)=wcf1(1)+dlog10(dv(2))
             else
*             multiply all terms
              do j=1,nwco
                wcf1(j)=wcf1(j)*dv(2)
              end do
            end if
          end if
          goto 1
        end if

      end if
*
*     X: (experimental, undocumented)
      if(cv(1)(1:1).eq.'x'.or.cv(1)(1:1).eq.'X') then
        call rd_xcdc(da,de,ca,ds,re,nct)
        goto 1
      end if
*
*     Z:
      if(cv(1)(1:1).eq.'z'.or.cv(1)(1:1).eq.'Z') then
*
*       ZR: Redshift correct to rest frame
        if(cv(1)(2:2).eq.'r'.or.cv(1)(2:2).eq.'R') then
*         this is simply a matter of adjusting the wavelength
*	  coefficients, but depends on the type of wavelength function.
          call rd_zcorr(dv(2),cv(3)(1:4))
          goto 1
        end if
*
*       ZT: Redshift correlation into dh, dhe
        if(cv(1)(2:2).eq.'t'.or.cv(1)(2:2).eq.'T') then
          call rd_ztabcf(da,de,ca,nct,ha,he,hc,nht,ds,cv(2))
          goto 1
        end if
*
      end if
*
*     FINISH:
      if(cv(1)(1:2).eq.'lo'.or.cv(1)(1:2).eq.'LO'.or.
     1       cv(1)(1:2).eq.'ex'.or.cv(1)(1:2).eq.'EX'.or.
     2       cv(1)(1:2).eq.'qu'.or.cv(1)(1:2).eq.'QU') then
*	call closeout routines here when needed
        goto 999
      end if
*
*     If it reaches here, the command was not recognized
*     (or a 'goto 1' was omitted above!)
      if(cv(1)(1:1).ne.' ') then
*	not blank, so issue a warning
        write(6,*) 'Not recognized'
      end if
      goto 1
 999  continue
*     switch off prompt before killing pgplot
      if(ipgopen.eq.1) then
        call pgask( .false. )
        call vp_pgend
      end if
      stop
      end
      subroutine rd_setstart
      implicit none
      include 'vp_sizes.f'
*
*     atomic data table variables
      integer nz
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),nz
*     scalefactors for column density
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
*     substepping for Voigt profile values
      integer nexpd
      common / nexpd / nexpd
*     Column density for inclusion of ion in ALL regions
      double precision fcollallzn
      common/vpc_colallzn/fcollallzn
*     
      character*8 wcftype
      character*4 vacind
      double precision helcfac
      integer noffs
      common/c8_wcftype/wcftype,helcfac,noffs,vacind
*
*     list of (up to 35) filenames with line lists for stacked plots
      integer ncufil,jcufil
      character*72 cufilnm(35)
      common/rdc_cufilnm/cufilnm,ncufil,jcufil
*     maximum redshift for table match
      double precision zidmax
      common/rdc_zidmax/zidmax
*
*     print size warning:
      write(6,*) 'Maximum data array length: ',maxfis
*     write(6,*) '  If this is not enough, alter the value in'
*     write(6,*) '  vp_sizes.f and recompile'
      write(6,*)
*     atomdata in table:
      nz=0
*     column density scalefactors
      scalefac=1.0d-14
      scalelog=1.0d0
      indvar=1
*     substepping for Voigt profile values
      nexpd = 5
*     Column density for inclusion of ion in ALL regions
      fcollallzn=1.0d20
*     default various wavelength factors and indicators
*     wcftype(1:4)='poly' ! set to undefine in rd_compreset
      helcfac=1.0d0
      noffs=0
      vacind(1:3)='vac'
*     stackplot variables
      ncufil=0
      jcufil=0
*     maximum redshift for '[' option (rd_plcset)
      zidmax=1.0d30
      return
      end
      subroutine rd_comreset
      implicit none
*     monitoring variable
      integer ncallset
      common/rdc_ncallset/ncallset
      write(6,*) 'Profile generation monitor frequency?'
      read(5,*) ncallset
      return
      end
