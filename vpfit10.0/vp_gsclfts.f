      subroutine vp_gsclfts(unit1,name,nech,nrow,ichunk,status)
*     
*     get sigma scaling as a function of wavelength (may be
*     a table)
*
*     IN:
*     unit1     integer     logical unit number for the data file
*     name      char        name of scale file
*     nech      integer     echelle order number
*     nrow      integer
*     ichunk    integer
*     
*     OUT:
*     status    integer     0 if OK
      implicit none
      include 'vp_sizes.f'
*
      integer unit1
      character*(*) name
      integer nech, nrow
      integer ichunk, status
*     
      character*132 dtadir,sname
      character*80 comment
*     double precision sigscl( 6 )
      integer irow,iflg,nltem,nlr,ntemp,last
      integer acunit
      integer ier210
*     parameter ( acunit = 32 )
      integer i,j
      integer ier
      logical verbose
      common/vp_sysout/verbose
*     sigma scale coefficients or arrays
      double precision ssclcf,wvalsc
      integer nscl
      common/sigsclcft/ssclcf(maxtas,maxnch),wvalsc(maxtas,maxnch),
     1                   nscl(maxnch)
*     interactive or batch (controls prompting when things go wrong)
      logical linteract
      common/vpc_linteract/linteract
*     character separation variables
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
      double precision dvstr(24)
      common/vpc_dsepspace/dvstr
*
*     sigma scale information read in on unit=32
      acunit=32
      if ( status .ne. 0 ) then
        write(6,*) 'vp_gscl called with bad status'
        write(6,*) ' with nech =',nech 
        status = 0
      end if

*     get a free unit number
*     status=0
*     Get an unused Logical Unit Number
*     call ftgiou(acunit,status)
*     if(status.ne.0) then
*       write(6,*) 'vp_gsclfts: Failed to get logical unit number'
*       write(6,*) 'status =',status
*     end if
*     then get ref spec file for database entry
      status=0
      call ftgkys(unit1,'SCLFILE', sname,comment,status)
      if ( status .ne. 0 ) then
        if(verbose) write ( 6, * ) ' no SCLFILE in header'
        status=10
        go to 1
       else
        if(verbose) write(6,*) 'Sigma scale from ',sname(1:40)
      end if
*     the directory may not be the home directory - assume that database is a
*     subdirectory inside the data directory
      dtadir = ' '
      last = 1
      if ( index( name, '/' ) .ne. 0 ) then
        last = index( name, '/' )
        do while ( index( name( last + 1 : ), '/' ) .ne. 0 )
          last = index( name( last + 1 : ), '/' ) + last
        end do
        dtadir = ' '//name( 1 : last )
      end if
      dtadir = dtadir( 1 : last + 1 )//'database/'//sname( 1 : 30 )
      do while ( dtadir( 1 : 1 ) .eq. ' ' )
        dtadir = dtadir( 2 : )
      end do
      if(verbose) then
        write ( *, * ) ' sigma scale file : '//dtadir( 1 : 60 )
      end if

      ier = 0

      open ( file = dtadir, err = 310, iostat = ier,
     :         status = 'old', unit = acunit )
 310  continue
      if(ier.ne.0) then
        write(6,*) 'Failed to open ',sname(1:20)
*       if multiple read it can be a timing problem - have another go
        ier=0
        if(linteract) then
          write(6,*) 'Filename? <CR> to retry with old one'
          read(5,'(a)') inchstr
          if(inchstr(1:1).ne.' ') then
            dtadir=inchstr
            sname=inchstr
          end if
        end if
        open ( file = dtadir, err = 10, iostat = ier,
     :         status = 'old', unit = acunit )
      end if
*     rfc 9.1.98: generalized read section to allow for interpolation 
*     table
      nlr=0
      ntemp=0
      irow=0
 890  read(unit=acunit,fmt='(a)',err=210,iostat=ier210) inchstr
*     skip comment lines
      if(inchstr(1:1).eq.'!') goto 890
      nlr=nlr+1
      call dsepvar(inchstr,2,dvstr,ivstr,cvstr,nvstr)
      if(cvstr(1)(1:3).eq.'row') then
*       check row number
        irow=ivstr(2)
        if(irow.le.0) then
          write(6,*) '.LE.0 ROW NUMBERS IN SCLFILE'
          irow=1
        end if
       else
        if(nrow.eq.1.and.nlr.eq.1) then
*         single order system possibly, so read the table directly
          ntemp=1+ntemp
          irow=1
          if(nvstr.eq.1) then
*	    polynomial coefficient
*	    sigscl(ntemp)=dvstr(1)
            ssclcf(ntemp,ichunk)=dvstr(1)
            wvalsc(ntemp,ichunk)=0.0d0
            nscl(ichunk)=ntemp
           else
*	    interpolation table
            ssclcf(ntemp,ichunk)=dvstr(2)
            wvalsc(ntemp,ichunk)=dvstr(1)
            nscl(ichunk)=ntemp
          end if
        end if
      end if
      if(irow.ne.nrow) then
*       read until an error occurs or get to the correct data
 891    read(unit=acunit,fmt='(a)',err=22,iostat=ier) inchstr
        if(inchstr(1:1).eq.'!') goto 891
        nlr=nlr+1
        call dsepvar(inchstr,2,dvstr,ivstr,cvstr,nvstr)
        if(cvstr(1)(1:3).ne.'row') goto 891
        irow=ivstr(2)
        if(irow.ne.nrow) goto 891
      end if
*     read in the rest of the coefficients/values
      iflg=0
      do while(ntemp.le.maxtas.and.iflg.eq.0)
 892    read(unit=acunit,fmt='(a)',err=22,iostat=iflg) inchstr
        if(inchstr(1:1).eq.'!') goto 892
*       check the whole string is not blank
        nltem=len(inchstr)
        do while(nltem.gt.0.and.inchstr(nltem:nltem).eq.' ')
          nltem=nltem-1
        end do
        if(nltem.le.0) goto 22
*
        nlr=nlr+1
        call dsepvar(inchstr,2,dvstr,ivstr,cvstr,nvstr)
        if(cvstr(1)(1:3).ne.'row'.and.iflg.eq.0) then
          ntemp=1+ntemp
          if(nvstr.eq.1) then
*	    polynomial coefficient
*	    sigscl(ntemp)=dvstr(1)
            ssclcf(ntemp,ichunk)=dvstr(1)
            wvalsc(ntemp,ichunk)=0.0d0
            nscl(ichunk)=ntemp
           else
*	    interpolation table
            ssclcf(ntemp,ichunk)=dvstr(2)
            wvalsc(ntemp,ichunk)=dvstr(1)
            nscl(ichunk)=ntemp
          end if
         else
*	  end of the data which was wanted
          iflg=1
        end if
      end do
*     check with printout for the paranoid
      if(verbose) then
        write(6,*) ' Sigma scale coeffts for row ',nrow,
     :             '   chunk ',ichunk
        do j=1,ntemp
          write(6,*) j,wvalsc(j,ichunk),ssclcf(j,ichunk)
        end do
      end if
*
 210  continue
      if(ier210.gt.0) then
        ier=ier210
        write(6,*) sname(1:20),' exists, but not useful'
      end if
 22   close(unit=acunit)
*     and free up the unit number, otherwise it will run out of them!
*     status=0
*     call ftfiou(acunit,status)
 10   continue
*
*     ier.ne.0 => no sigscale data
*     iflg.ne.0 => got it OK
*
      if ( ier .ne. 0 ) then
        write(6,*) 'SCLFILE ',sname(1:20),' read failure'
        write(6,*) 'sigma scale?'
        read(5,'(a)') inchstr
        call dsepvar(inchstr,1,dvstr,ivstr,cvstr,nvstr)
        if(dvstr(1).le.0.0d0) dvstr(1)=1.0d0
*       sigscl(1)=dvstr(1)
        ssclcf(1,ichunk)=dvstr(1)
        wvalsc(1,ichunk)=0.0d0
        do i=2,6
*         sigscl(i)=0.0d0
          ssclcf(i,ichunk)=0.0d0
          wvalsc(i,ichunk)=0.0d0
        end do
        nscl(ichunk)=6
      end if

 1    return
      end
