      subroutine vp_fnzbin(if18,nopt,nn,ion,level,parm,ipind,
     :          vturb,temper,lendlist)
*
*     read in the n,z,b first guesses
*     along with chunk numbers or variation flags
*
*
*     read in the n, z, b first guess values from file,
*     along with chunk numbers or variation flags,
*     format depending on if18 flag value.
c
c     input:	if18	= 1 for if18 format, = 0 f13
c			  26 if fort.26 style
c		nopt	option number specifying variables set
c
c     output:	nn	number of d.f. removed (=3* no lines usually)
c		ion	atom (H,C,N,O....)
c		level	ionization level (I,II,III,IV...)
c		parm	n,x,b values
c		ipind	f/t or blank, depending on constraint
c		vturb	turbulent component of H velocity dispersion
*
      implicit none
      include 'vp_sizes.f'
*
*     Subroutine arguments
      integer if18,nopt,nn
      character*2 ion(*)
      character*4 level(*)
      character*2 ipind(*)
      double precision parm(maxnpa)
      double precision vturb(maxnio),temper(maxnio)
*     lendlist is .true. if a data line is already in inchstr on entry
*     and reading from fort.13 free format file
      logical lendlist
*
*     LOCAL:
      character*2 ipcolcl
      character*120 fm0,fm80,fmx
      character*132 ifile
      character*1 ipx(3), coment
      character*2 ionx
      character*4 levx
      integer i,ip,ip0,iq,ierr
      integer ipc,jix
      integer ji,jin
      integer nlines,nsyst,nit,nx
      double precision px(3)
      double precision vtb,vtmp
      double precision basecol,sumcol
*
*     functions
      logical nocgt,noceqv
*
*     readin workspace
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
      double precision dvstr(24)
      common/vpc_dsepspace/dvstr
*
*     data arrays in case program needs to find lines
*     current 1-D spectrum data, err, fluctuations, continuum -length ngp
      integer ngp
      double precision dhdata(maxfis),dherr(maxfis)
      double precision dhrms(maxfis),dhcont(maxfis)
      common/vpc_su1d/dhdata,dherr,dhrms,dhcont,ngp
*     common file variables for finding lines
*     chunk variables:
      integer ndpts(maxnch),idrun(maxnch),icrun(maxnch)
      integer indfil(maxnch)
      character*64 filename(maxnch)
      double precision wvstrt(maxnch),wvend(maxnch)
      integer nchunk
      common/vpc_chunk/wvstrt,wvend,ndpts,filename,
     :      idrun,icrun,indfil,nchunk
*     fit region limits
      integer ichstrt(maxnch),ichend(maxnch)
      common/jkw4/ichstrt,ichend
*     reserved character
      character*4 specchar
      common/vpc_specchar/specchar
*     verbose mode
      logical verbose
      common/vp_sysout/verbose
*     monitoring level 0:minimal 1:skeletal 2:standard 3: alpha
*       4: ...      16:everything
      integer nverbose
      common/vp_sysmon/nverbose
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     column densities  linear: indvar=0, log: indvar=1
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
      character*2 lastch,firstch
      common/vpc_ssetup/lastch,firstch
*     chunk list
      integer linchk(maxnio)
      common/vpc_linchk/linchk
      character*4 chlnk(maxnio)
      common/vpc_chlnchk/chlnk
*
*     common array for line fixing (nfx=0 means fix all ions)
      character*2 ifx
      character*4 lfx
      integer nfx
      common/vpc_fixlist/ifx(10),lfx(10),nfx
*     printout control
      integer lt(3)          ! output channels
      integer nopchan,nopchanh,nmonitor
      common/vpc_nopchan/lt,nopchan,nopchanh,nmonitor
*
*     printout formats depend on variable style
      fm0='(2x,i4,19x,a2,a4,1x,e10.3,a1,f11.6,a1,f10.2,a1,'//
     1     'f10.2,e10.2)'
      fm80='(2x,i4,19x,a2,a4,1x,f10.4,a1,f11.6,a1,f10.6,a1,'//
     1     'f10.6,e10.2)'
*
*     preset the value of no variables, in case of error branch
      nn=0
      if(if18.eq.1) then
*       if18 format -- requires some searching
*       set format by variable
        if(indvar.ne.0) then
*         log column
          fmx=fm80
         else
*         linear column densities
          fmx=fm0
        end if
c	read to internal file
 2      ifile='  '
        read(13,'(a)',end=99) ifile
        if(ifile(1:1).eq.'!') goto 2
c       reached end?
        if(ifile(1:10).ne.'  Paramete') then
c	  check have something to look at
          read(ifile,'(2x,i4)',err=2) nit
          if(nit.eq.0) goto 2
c	  iteration number nonzero - process record
          read(ifile,fmx,err=2) nit,ion(1),level(1),parm(1),
     :        ipind(1),parm(2),ipind(2),parm(3),ipind(3),vturb(1),
     :        temper(1)
          nx=1
          nn=3*nx
c         read until exhausted
 3        read(13,fmx,err=2) nit,ionx,levx,(px(i),ipx(i),i=1,3),
     :         vtb,vtmp
          if(ionx.ne.'  ') then
            nx=nx+1
            ion(nx)=ionx
            level(nx)=levx
            vturb(nx)=vtb
            temper(nx)=vtmp
            do i=1,3
              parm(nn+i)= px(i)
              ipind(nn+i)=ipx(i)
            end do
            nn=3*nx
            goto 3
           else
            goto 2
          end if
        end if
*
       else
*	f13 format or f26 -- use old procedure
*	number of variables to be fitted:
        if(.not.lendlist) then
 801      read(13,'(a)') inchstr
          if(inchstr(1:1).eq.'!') goto 801
*	  strip off any comment fields
          call vp_stripcmt(inchstr,'!')
        end if
        call dsepvar(inchstr,24,dvstr,ivstr,cvstr,nvstr)
        nn=ivstr(1)
        nsyst=nn/noppsys
        if(nsyst.gt.0.and.nvstr.lt.3) then
          do ji=1,nsyst
            iq=(noppsys*(ji-1))
            ip=iq+1
            if(nopt.eq.2.or.nopt.eq.3) then
              read(13,1901) ion(ji),level(ji),parm(iq+1),
     :             parm(iq+2),parm(iq+3)
1901          format(3x,a2,a4,1x,e9.3,1x,f10.6,1x,f9.2)
              ipind(iq+1)=' '
              ipind(iq+2)=' '
              ipind(iq+3)=' '
              vturb(ji)=0.0d0
              temper(ji)=0.0d0
            end if
*           ajc 12-nov-91  option 9 added
*           ajc 19-dec-91  linchk added; rfc 17.3.98 option 10 added
            if(nopt.eq.7.or.nopt.ge.9)  then
*             ajc1 3-feb-92 comment lines added
1066          read(13,1902) coment,
     :          ion(ji),level(ji),parm(iq+1),ipind(iq+1),
     :          parm(iq+2),ipind(iq+2),parm(iq+3),ipind(iq+3),
     :          vturb(ji),temper(ji), linchk( ji )
1902          format(1a,2x,a2,a4,1x,e9.3,a1,f10.6,a1,
     :               f9.2,a1,f9.2,1x,e10.2,1x,i2)
              if (coment .eq. '*') goto 1066
            end if
          end do
         else
*         free format input
*         rfc 2-Oct-92
*         assumes numbers of systems has to be counted from input lines
*         and stacks data as for options 7 & 9
          jin=0
          if(nvstr.ge.2) then
*           data on the current line
            jin=1
            ip0=(noppsys*(jin-1))
            ip=ip0+1
            if(inchstr(1:1).eq.'*') goto 1068
            call dsepvar(inchstr,24, dvstr,ivstr,cvstr,nvstr)
            call vp_f13finx(jin,if18, ion,level,parm,ipind,
     :             vturb,temper,linchk,chlnk,ierr)
            if(nverbose.ge.4) then
              write(6,*) jin,' ',ion(jin),level(jin),parm(ip0+nppcol),
     :              parm(ip0+nppzed),parm(ip0+nppbval)
            end if
            if(ierr.ne.0) goto 9032
          end if
 9033     jin=jin+1
          ip0=(noppsys*(jin-1))
          ip=ip0+1
 1068     read(13,'(a)',err=9037,end=9037) inchstr
          if(inchstr(1:1).eq.'!') goto 1068
          if(inchstr(1:1).eq.'*') goto 1068
          call vp_stripcmt(inchstr,'!')
          call dsepvar(inchstr,24,dvstr,ivstr,cvstr,nvstr)
          if(cvstr(1)(1:2).eq.'  '.or.cvstr(1)(1:2).eq.'%%') 
     :                                                 goto 9032
*         free format parameter read. Cols returned unscaled
          call vp_f13finx(jin,if18, ion,level,parm,ipind,
     :             vturb,temper,linchk,chlnk,ierr)
          if(nverbose.ge.4) then
            write(6,*) jin,' ',ion(jin),level(jin),parm(ip0+nppcol),
     :              parm(ip0+nppzed),parm(ip0+nppbval)
          end if
*         above replaces old element by element argument list
          if(ion(jin).ne.'  '.and.ion(jin).ne.'%%'
     :          .and.ierr.eq.0) goto 9033
          goto 9032
*         fill in input array, since it was not read
 9037     inchstr=' '
          cvstr(1)=' '
 9032     jin=jin-1
          nsyst=jin
          nn=nsyst*noppsys
          if(nsyst.le.0) then
            if(nchunk.gt.1) then
              write(6,*) 'Guessing Ly-a in last region only ...'
            end if
            ngp=ndpts(nchunk)
            call vp_abfind(dhdata,dherr,dhcont,ngp,nchunk,
     :               ichstrt(nchunk),ichend(nchunk),
     :              ion,level,parm,nn,nsyst)
           else
            if(verbose) then
              write(6,*) nsyst,' systems'
            end if
          end if
        end if
      end if
*
*     correct the column densities if necessary
      ipcolcl='  '
      nlines=nn/noppsys
*      ji=1
*      do while (ji.le.nlines)
      do ji=1,nlines
        iq=noppsys*(ji-1)
        ip=iq+nppcol
*        write(6,*) '>>',ji,parm(ip)
        if(ion(ji).ne.'__'.and.ion(ji).ne.'<>'.and.
     :               ion(ji).ne.'>>') then
*         check validity and then scale appropriately
          if(indvar.ne.0) then
            if(parm(ip).gt.40.0d0 .and. indvar.ge.1) then
              parm(ip)=log10(parm(ip))
            end if
            parm(ip)=parm(ip)*scalelog
           else
*           indvar=0 is linear
            if(parm(ip).le.40.0d0) parm(ip)=10.0d0**parm(ip)
            parm(ip)=parm(ip)*scalefac
          end if
*	  special case: nopt=4, fix hydrogen (may extend later)
*	  using HX,HY,HZ:
          if(nopt.eq.4) then
            if(nfx.le.0) then
              ipind(iq+1)='HX'
              ipind(iq+2)='HY'
              ipind(iq+3)='HZ'
              if(noppsys.ge.4) then
                ipind(iq+4)='HW'
                if(noppsys.ge.5) then
                  ipind(iq+5)='HV'
                end if
              end if
             else
              do iq=1,nfx
                if(ifx(iq).eq.ion(ji).and.lfx(iq).eq.level(ji)) 
     :                                   then
                  ipind(iq+1)='HX'
                  ipind(iq+2)='HY'
                  ipind(iq+3)='HZ'
*                 extended parameter set
                  if(noppsys.ge.4) then
                    ipind(iq+4)='HW'
                    if(noppsys.ge.5) then
                      ipind(iq+5)='HV'
                    end if
                  end if
*
                end if
              end do
*
            end if
          end if
         else
*	  no checks for zero or continuum levels, but fix the
*	  redshifts if the user forgot to, using the special fix
*	  character (default S). [Also velocity shift fix col]
*	  col=1,4.., z=2,5.., b=3,6 .. 
          if(ipind(iq+nppzed)(1:1).eq.' ') then
            ipind(iq+nppzed)=specchar(1:1)//'Z'
          end if
          if(ipind(iq+nppcol)(1:1).eq.' '.and.ion(ji).eq.'>>') then
            ipind(iq+nppcol)=specchar(1:1)//'C'
          end if
        end if
        if(nopt.eq.3.or.nopt.eq.4.or.nopt.eq.7.or.
     :         nopt.ge.9) then
          call tikset(ion(ji),level(ji),parm(iq+nppzed),ji)
        end if
*        ji=ji+1
      end do
*
*     sort out summed column densities
      ji=1
      do while (ji.le.nlines)
        iq=noppsys*(ji-1)
        ip=iq+nppcol
        if(ion(ji).ne.'__'.and.ion(ji).ne.'<>'.and.
     :               ion(ji).ne.'>>') then
*         Are summed column densities being used? If so, make sure the
*         first one is a sum, rescaling internally all the time
*         Check first that is > lastch in collating sequence, and not the 
*         same as has gone before
          if((ipind(ip).ne.ipcolcl
     1         .and.nocgt(ipind(ip)(1:1),lastch)).or.
     2         ipind(ip)(1:1).eq.'%'.or.ipind(ip)(1:1).eq.'@') then
*           set characters in sequence
            if(ipind(ip)(1:1).eq.'%'.or.ipind(ip)(1:1).eq.'@') then
*             Note that it is ASSUMED that a sum block quantity follows
*             the % or @. If it doesn't, then the user shouldn't be using them.
              ipcolcl=ipind(ip+noppsys)
             else
              ipcolcl=ipind(ip)
            end if
*           new base value
            if(ji.lt.nlines) then
              if(indvar.eq.1) then
                basecol=scalefac*10.0d0**(parm(ip)/scalelog)
               else
                basecol=parm(ip)
              end if
              jix=ji+1
              ipc=noppsys*(jix-1)+nppcol
              sumcol=0.0d0
              do while (jix.le.nlines.and.ipind(ipc).eq.ipcolcl)
                if(indvar.eq.1) then
                  sumcol=sumcol+scalefac*10.0d0**(parm(ipc)/scalelog)
                 else
                  sumcol=sumcol+parm(ipc)
                end if
                jix=jix+1
                ipc=noppsys*(jix-1)+nppcol
              end do
              if(sumcol*0.9999d0.gt.basecol) then
*
*               replace parm(ip) by the sum
                basecol=basecol+sumcol
                if(indvar.ne.0) then
                  parm(ip)=scalelog*log10(basecol/scalefac)
                 else
                  parm(ip)=basecol
                end if
*
*               check if the next group form follows:
                ji=jix-1
 301            continue
                if(ipind(ipc)(1:1).eq.'%'.or.
     :               ipind(ipc)(1:1).eq.'&') then
                  ip=ipc
                  ji=jix+1
                  if(indvar.eq.1) then
                    sumcol=scalefac*10.0d0**(parm(ip)/scalelog)
                   else
                    sumcol=parm(ip)
                  end if
                  jix=jix+1
                  ipc=noppsys*(jix-1)+nppcol
                  do while(jix.le.nlines.and.
     :                        noceqv(ipind(ipc),ipcolcl))
                    if(indvar.eq.1) then
                      sumcol=sumcol+scalefac*
     :                  10.0d0**(parm(ipc)/scalelog)
                     else
                      sumcol=sumcol+parm(ipc)
                    end if
                    jix=jix+1
                    ipc=noppsys*(jix-1)+nppcol
                  end do
*                 replace parm(ip) by the sum
                  if(indvar.ne.0) then
                    parm(ip)=scalelog*log10(sumcol/scalefac)
                   else
                    parm(ip)=sumcol
                  end if
*                 may have more than one form following group
                  goto 301
                end if
                ji=jix-1
              end if
            end if
          end if
        end if
        ji=ji+1
      end do
 99   return
      end
