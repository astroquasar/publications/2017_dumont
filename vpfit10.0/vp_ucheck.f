      subroutine vp_ucerck(parm,np,istat,ipind,ion)
*     ajc 19-dec-91  ipind added to keep tied parameters
*	
*     check array parm for conditions indicating system rejection
*
*     INPUT:  parm(np)
*             ipind(np)
*
*     OUTPUT: istat=no. of rejected systems
*     common block vpc_nrejs contains rejected system numbers
*
      implicit none
      include 'vp_sizes.f'
*
      character*2 ipind(*)
      double precision parm(*)
      character*2 ion(*)
      integer np,istat
*
*     local
      integer i,j,k,iop
      integer jj,kkn,kkbase,kkv
      integer nsys,ntem
*
*     functions
*     check for upper case, lower case, no case equality
      logical ucase, lcase, noceqv, varythis
*     flag for system which has alreay been rejected
      logical already
*
*     Functions
      logical nocgt
*
*     COMMON:
*     parameter variables [rfc 13.07.06]
*     noppsys=# parameters per system (max 5; 3 for standard N,z,b),
*     nppcol=col posn (1 usually); nppbval (3); nppzed (2)
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*
      integer ndrop,kdrop,ndroptot,ndrwhy
      common/vpc_nrejs/ndrop,kdrop(maxnio),ndroptot,ndrwhy
*
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
*
*     b parameter limits, conditions for dropping system
      double precision bvalmin,bvalmax,bltdrop,bgtdrop,colltdrop,
     :                     clogltdrop,cvaldrop,clvaldrop,cvalmax,
     :                     clvalmax,bvalminh,bvalmaxh,clvalmin,
     :                     cvalmin
      common/vpc_bvallims/bvalmin,bvalmax,bltdrop,bgtdrop,colltdrop,
     :                     clogltdrop,cvaldrop,clvaldrop,cvalmax,
     :                     clvalmax,bvalminh,bvalmaxh,clvalmin,
     :                     cvalmin
*     Special conditions after this character (totals for column,
*     temperature/b for bval)
      character*2 lastch,firstch
      common/vpc_ssetup/lastch,firstch
*     output control
      integer nopchan,nopchanh,nmonitor
      integer lt(3)             ! output channels for printing
      common/vpc_nopchan/lt,nopchan,nopchanh,nmonitor
*
      ndrop=0
      nsys=np/noppsys
      do kkn=1,nsys
*       check velocity dispersion
*	don't reject continuum, zero level, AD (template added component),
*	emission lines.
        kkbase=noppsys*(kkn-1)
*        kk=kkn*noppsys
        if (indvar.ne.-1.and.ion(kkn).ne.'>>'.and.
     :        ion(kkn) .ne.'<<'.and.ion(kkn).ne.'<>'.and.
     :        ion(kkn) .ne.'__'.and.ion(kkn).ne.'AD') then
*         test if b outside range (note: max will never happen)
          kkv=kkbase+nppbval
          if (varythis( kkv, ipind ) .and.
     :           .not.nocgt(ipind(kkv)(1:1),lastch).and.
     :           (parm(kkv).le.bltdrop .or.
     :            parm(kkv).gt.bgtdrop) ) then
*	    Also check for high N, low b
            if(parm(kkv).lt.bltdrop) then
              if(indvar.eq.1) then
                if(parm(kkbase+nppcol).le.clogltdrop*scalelog) then
                  ndrop=ndrop+1
                  kdrop(ndrop)=kkn
                end if
               else
                if(indvar.eq.0) then
                  if(parm(kkbase+nppcol).le.
     :                     colltdrop*scalefac) then
                    ndrop=ndrop+1
                    kdrop(ndrop)=kkn
                  end if
                end if
              end if
             else
              ndrop=ndrop+1
              kdrop(ndrop)=kkn
            end if
*	    ajc 19-dec-91  additional test
           else if ( varythis ( kkbase+nppcol, ipind ) ) then
            if(indvar.eq.1) then
              if(parm(kkbase+nppcol).le.
     :               clvaldrop*1.0001d0*scalelog) then
                ndrop=ndrop+1
                kdrop(ndrop)=kkn
              end if
             else
              if(indvar.eq.0) then
                if(parm(kkbase+nppcol).le.(cvaldrop*1.0001d0)
     :                         *scalefac) then
                  ndrop=ndrop+1
                  kdrop(ndrop)=kkn
                end if
              end if
            end if
          end if
        end if
      end do
      istat=ndrop

      if ( istat .gt. 0 ) then
*	set flag (=1 <=> threshold condition loses systems)
        ndrwhy=1
        ntem=max(1,nopchan)
*	Notify user that systems are disappearing
        if(ndrop.gt.1) then
          do iop=1,ntem
            write(lt(iop),*) ndrop,'  systems rejected'
            write(lt(iop),'(a4,20i4)')'Nos:',(kdrop(jj),jj=1,ndrop)
          end do
         else
          do iop=1,ntem
            write(lt(iop),*) ' System ',kdrop(1),' rejected'
          end do
        end if
*	now see if anything tied to rejected system
        do i = 1, ndrop
          if ( ipind( kdrop(i))(1:1) .eq. special .or.
     :         ipind( kdrop(i))(1:1) .eq. vspecial ) then
            write (6,*) 'want to drop special line!!'
            stop
          end if
*	  tied to velocity 
          kkbase=(kdrop(i)-1)*noppsys
          if(lcase(ipind(kkbase+nppbval)(1:1))) then
            do kkn=1,nsys
              j=noppsys*(kkn-1)+nppbval
              if (ucase(ipind(j)(1:1)) .and. 
     :            noceqv(ipind(j),ipind(kkbase+nppbval))) then
                already = .false.
                do k = 1, ndrop
                  if (kdrop(k) .eq.kkn ) already = .true.
                end do
                if ( .not. already ) then
                  ndrop = ndrop + 1
                  kdrop( ndrop ) = kkn
                end if
              end if
            end do
          end if
*         tied to redshift
          if (lcase(ipind(kkbase+nppzed)(1:1))) then
            do kkn=1,nsys
              j=noppsys*(kkn-1)+nppzed
              if (ucase( ipind(j)(1:1) ) .and.
     :            noceqv(ipind(j),ipind(kkbase+nppzed))) then
                already = .false.
                do k = 1, ndrop
                  if ( kdrop( k ) .eq. kkn ) already = .true.
                end do
                if ( .not. already ) then
                  ndrop = ndrop + 1
                  kdrop( ndrop )=kkn
                end if
              end if
            end do
          end if
        end do
      end if
      
      if ( ndrop .gt. istat ) then
        ntem=max(1,nopchan)
        do iop=1,ntem
          write(lt(iop),*) ndrop-istat, '  tied systems lost'
        end do
        istat=ndrop
      end if     
      return
      end

      subroutine vp_syschk(ion,level,ipind,nlines,parm,np,istat,nsys,nn,
     :                  temper,vturb,atmass,fixedbth)
*	
*     check how many systems have been dropped or added, and, if 
*     systems have been dropped then reset the
*     variables to perform a fit with only the needed systems
*
*     INPUT:  ion  - element array
*             level - ionization level
*             nlines - no of redshift systems
*	      parm - col density, redshift, vel disp array
*             np - size of array (nlines*3)
*             temper - fixed temperature
*             vturb - fixed turbulent velocity
*             atmass - atomic masses
*             fixedbth - fixed/tied indicator
*
*     OUTPUT: istat - no of systems rejected
*	      nsys  - no of systems left
*             nn    - new size of parm array
*	      ion, level, parm arrays are replaced with accepted
*		   parameters
*             same with added params
*
      implicit none
      include 'vp_sizes.f'
*
      integer istat
      integer nlines,np,nn,nsys
      character*2 ion(nlines)
      character*4 level(nlines)
      character*2 ipind(np)
      double precision temper(nlines), vturb(nlines)
      double precision atmass(nlines),fixedbth(nlines)
      double precision parm(np)
*
*     Local:
      integer i,idrop,j,jj,kj
      integer ip,ipplus
*
*     COMMON:
*     parameter variables [rfc 13.07.06]
*     noppsys=# parameters per system (max 5; 3 for standard N,z,b),
*     nppcol=col posn (1 usually); nppbval (3); nppzed (2)
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     lines may be fastened to chunks via linchk array
      integer linchk( maxnio )
      common/vpc_linchk/linchk
      character*4 chlnk(maxnio)
      common/vpc_chlnchk/chlnk
*
      integer ndrop,kdrop,ndroptot,ndrwhy
      common/vpc_nrejs/ndrop,kdrop(maxnio),ndroptot,ndrwhy
*     For NLADDED added lines
      integer nladded
      double precision chisqvh,probchb,probksb
      common/vpc_problims/chisqvh,probchb,probksb,nladded
*
      nn=np
      nsys=nlines
      if(nladded.gt.0) then
        ndrop=-nladded
        nladded=0
      end if
      istat=ndrop
*
*     check that you are not trying to drop too many systems
      if(ndrop.ge.nsys) then
*       issue a warning
        write(6,
     1   '('' Drop'',i3,'' systems out of'',i3,''!!!!'')') 
     2   ndrop,nsys
*	drop the first NSYS-1 and hope for the best
        ndrop=nsys-1
      end if
*
      if(ndrop.gt.0) then
*       some systems have to go
        do jj=1,ndrop
          kj=ndrop+1-jj
          idrop=kdrop(kj)
          nsys=nsys-1
          if(idrop.ne.nsys+1) then
            do i=idrop,nsys
              linchk( i ) = linchk( i + 1 )
              chlnk(i)=chlnk(i+1)        
              ion(i)=ion(i+1)
              level(i)=level(i+1)
*             temperature & turbulent velocities as well
              temper( i ) = temper( i + 1 )
              vturb( i ) = vturb( i + 1 ) 
*             Tied masses etc                
              atmass( i ) = atmass( i + 1 )
              fixedbth( i ) = fixedbth( i + 1 )
*
              ip=noppsys*(i-1)
              ipplus=ip+noppsys
              do j=1,noppsys
                parm(ip+j)=parm(ipplus+j)
                ipind(ip+j)=ipind(ipplus+j)
              end do
            end do
          end if
        end do
        nn=nsys*noppsys
*       check no tied bases lost if necessary
        call vp_blfixf(ion,ipind,nsys)
      end if
      return
      end
