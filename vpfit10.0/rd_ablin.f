      subroutine rd_ablin(da,dvar,ca,de,nl,cres,cvst)
*
*     to find absorption and emission features in a spectrum
*     
*     INPUT:
*     da	object-sky array
*     dvar      sigma squared (i.e. variance) array
*     ca	continuum array
*     de	workspace array (to contain sqrt(dvar))
*     nl	array size
*     cres      character string - if not blank, print lam,ew only
*               if eq '?', print help and exit
*     cvst	character string with output filename
*	
*     OUTPUT:
*     goes direct to unit 9 if cvst is blank, and is reasonably clear.
*	
*     subroutines used:
*     PLAM calculates -log probability of chance occurrence of a line
*     SPLIT suggest component structure to a feature 
*     WCOR air/vac factor determination
*     WVAL wavelength as function of double precision channel number
*	
*     it is assumed that a heliocentric correction has been applied
*     to the wavelength coefficients if it is needed.
*	
*
      implicit none
      include 'vp_sizes.f'
*
      integer nl
      character*(*) ,cres,cvst
      double precision dvar(nl)
      double precision da(nl),de(nl),ca(nl)
*
*     Local
      character*132 chform
      double precision wxdl,wxdh,dtemp
*     smoothing type
      character*4 chsmt
*
      character*1 icx,chunit
      character*80 chstr
      double precision rvs(4)
      character*36 cvs(4)
      integer ivs(4)
*      integer ifx,iform
      integer i,isupp,j,nhi
      integer jh,jmn,jmx,kjp1,kcai
      integer kspike,ncen,nvs,minsep
      double precision flo,fhi,fhp
      double precision caj,ce,csx,cv,siglxd,sigpeak,xc
      double precision sg,s2,s3,tlx,qwlo,qwhi
      double precision wvjmn,wvjmx,xtemp,temp,temp2,wid,xl
*     Functions
      double precision plam
      double precision wval
*     Common:
      integer nres,lo,lhi
      double precision sigl
      common/rdc_abcf/sigl,nres,lo,lhi
      double precision ssqrd,ssqrh
      common/wavrms/ssqrd,ssqrh
*      integer nwco
*      double precision wcf(maxwco)
*      common/vpc_wavl/wcf,nwco
*     data scale parameters (lrescale true if data rescaled, scale is
*     number it was multiplied by)
      logical lrescale
      double precision scaleff
      common/vpc_dscale/scaleff,lrescale
*     unit indicator variables
      integer mftyp,mxtyp
      double precision yscale
      common/dunit/mftyp,mxtyp,yscale
*     printout variables
      integer kl
*     ipr is line type
      character*4 ipr,chskew
      character*4 chflsig
      double precision wh
      double precision wl,wvc,wer,ew,py,flx,x,swid,fwhm,p,px,skew
      common/rdc_absprnt/wl,wvc,wer,wh,ew,py,
     :          flx,x,swid,fwhm,p,px,skew,ipr,kl,chskew,chflsig
*
      if(cres(1:1).eq.'?') then
        write(6,*) 'Wavelength, EW to output file unless 1st parameter'
        write(6,*) ' is blank or (starts with) 0'
        write(6,*) 'Output to fort.9 if file not specified as'
        write(6,*) ' 2nd parameter'
      end if
      if(cres(1:1).eq.'0') cres=' '
      if(cvst(1:2).ne.'  ') then
        close(unit=9,err=799)
 799    open(unit=9,file=cvst,status='unknown')
      end if
*	
      if(wval(1.0d0).le.10.0d0) then
*       working in microns
        chunit='m'
*        iform=180
*       assign no longer supported
        chform='(1x,a3,i5,f10.7,f7.4,f9.5,2f8.5,1pe11.3,'//
     1     '0pf8.5,2f8.5,f7.1,1x,f6.2,2f8.2,f7.2,a1)'
*        assign 180 to ifx
       else
        chunit='A'
*        iform=100
        chform='(1x,a3,i5,f10.3,f7.3,f9.2,1x,f9.4,1x,f8.4,'//
     1         '1pe11.3,0pf8.3,2f8.3,f7.1,1x,f6.2,2f8.2,f7.2,a1)'
*       assign 100 to ifx
      end if
      write(6,'('' Restricted output(r), fluxes (f)? [neither]'')')
      read(5,'(a1)',err=9999,end=9999) icx
      isupp=0
      if(icx.eq.'r'.or.icx.eq.'R') isupp=1
*
      if(icx.eq.'f'.or.icx.eq.'F') then
        chflsig='flux'
       else
        chflsig='nsig'
      end if
*	
*     scales for fluxes
      csx=1.0d0
      if(mftyp.eq.3.and.mxtyp.eq.1) csx=yscale*3.0d18
*
      do i=1,nl
        if(dvar(i).gt.0.0d0) then
          de(i)=sqrt(dvar(i))
         else
*         if negative error, copy value
*         Note that it may crash if spike near end of array
          de(i)=dvar(i)
        end if
      end do
*
      write(6,'('' Sig. lims.: line, components, cpt peak'',
     1            '' [5.0,same,as cpts] :'')')
      read(5,'(a)',end=9999) chstr
      call dsepvar(chstr,3,rvs,ivs,cvs,nvs)
      sigl=rvs(1)
      siglxd=rvs(2)
      sigpeak=rvs(3)
*     sigl = log significance limit for whole line
*     siglxd = log sign. for each component
*     sigpeak= log sign for peak in component relative zero
*     defaults:
      if(sigl.le.0.0) sigl=5.0
      if(siglxd.le.0.0.or.siglxd.ge.sigl) siglxd=sigl
      if(sigpeak.le.0.0.or.sigpeak.ge.siglxd) sigpeak=siglxd
      write(6,'('' Resolution, min sepn (chan) [3,nres/2+1] '')')
*
      read(5,'(a)',end=9999) chstr
      call dsepvar(chstr,3,rvs,ivs,cvs,nvs)
      nres=ivs(1)
      minsep=ivs(2)
      chsmt=cvs(3)(1:4)
      if(nres.le.0.or.nres.gt.100) nres=3
      if(minsep.le.0) minsep=nres/2+1
*
      wxdl=wval(dble(10))
      lhi=nl-10
      wxdh=wval(dble(lhi))
      write(6,'('' Wavelength range  -low, high ['',f7.1,'','',
     1         f7.1,'']:'')') wxdl,wxdh
      read(5,'(a)',end=9999) chstr
      call dsepvar(chstr,2,rvs,ivs,cvs,nvs)
      if(rvs(1).le.0.0) then
        dtemp=wxdl
       else
        dtemp=rvs(1)
      end if
      wxdl=10
      call chanwav(dtemp,wxdl,0.01d0,20)
      if(wxdl.le.0.0d0) then
        lo=10
       else
        lo=wxdl
      end if
      if(rvs(2).le.0.0) then
        dtemp=wxdh
       else
        dtemp=dble(rvs(2))
      end if
      wxdh=lhi
      call chanwav(dtemp,wxdh,0.01d0,20)
      lhi=wxdh
      if(lhi.gt.nl.or.lhi.le.lo) lhi=nl-10
*	
* 77   continue
*     BUG TO BE FIXED: EMISSION SET BY SPIKE?
*     IN AND FIRST AFTER EMISSION
      write(6,962) sigl,nres,minsep,lo,lhi
 962  format(' lines to level',f6.1,'  sought'/' assumed resolution',
     1  i5,' channels'/'min sepn',i6,' limits --  channel',i6,'  to',i6)
      qwlo=wval(dble(lo))
      qwhi=wval(dble(lhi))
      write(6,'('' wavelength limits:'',f10.2,
     1           '' to'',f10.2,1x,a1)') qwlo,qwhi,chunit
      if(cres(1:1).eq.' ') then
        write(9,962) sigl,nres,minsep,lo,lhi
        write(9,'('' wavelength limits:'',f10.2,
     1           '' to'',f10.2,1x,a1)') qwlo,qwhi,chunit
        if(ssqrd.gt.0.0d0.and.ssqrd.le.3.0d0) then
          write(9,*) ' corrected wavelength errors, sigma:',ssqrd
        end if
        write(9,963) chunit,chflsig
      end if
*
      if(ssqrd.gt.0.0d0.and.ssqrd.le.3.0d0) then
        write(6,*) ' Corrected errors used'
        write(9,*) ' corrected wavelength errors, sigma:',ssqrd
      end if
      write(6,963) chunit,chflsig
 963  format(/,7x,'line',3x,'mean',3x,'error',1x,
     1 'peak(vac)',4x,'ew(',a1,')',4x,'error',4x,a4,
     2  6x,'dispn',3x,'FWZI',4x,'FWHM',5x,'p',3x,'pbty',
     3  3x,'lo',6x,'hi',5x,'skew')
      write(6,'(113x,''or fchi'')')
*     rest upper range to be max containing useful data
      do while(de(lhi).le.0.0d0.and.lhi.gt.nres+1)
        lhi=lhi-1
      end do
      nhi=lhi-nres
*     seek lines on scale of given resolution or greater
      j=lo
      kl=0
      kjp1=1
      kspike=0
 2    continue
*     absorption line		sg=1.0
*     emission line		sg=-1.0
      if(ca(j).ge.da(j)) then
        sg=1.0d0
       else
        sg=-1.0d0
      end if
      if(ca(j).gt.0.0d0) goto 1
*     zero or negative continuum
      j=j+1
      if(j.lt.nhi) goto 2
      goto 8
*     possible line found
 1    ew=0.0d0
      wl=0.0d0
      xc=(ca(j)-da(j))*sg
      flx=xc*csx
      if(mftyp.eq.3.and.mxtyp.le.5) flx=flx/wval(dble(j))**2
      caj=ca(j)
      x=(1.0d0-da(j)/caj)*sg
      if(dvar(j).gt.0.0d0) then
        ce=dvar(j)
        p=plam((ca(j)-da(j))*sg/de(j))
        kspike=0
       else
        kspike=1
        ce=0.0d0
        p=0.0d0
      end if
      ew=ew+x
      wl=wl+wval(dble(j))*x
      ncen=j
      wh=x
      cv=ca(j)
*     find the end of the line by checking when continuum level is
*     next reached
      jh=j+1
      do while((da(jh)-ca(jh))*sg.lt.0.0d0.and.jh.lt.lhi)
        jh=jh+1
      end do
*     special branch out if upper end of search limit reached
      if(jh.ge.lhi) goto 5
c     **    jh -1 is now the end of the line....
      jh=jh-1
      kjp1=j+1
      if(kjp1.gt.jh) goto 5
      kcai=0
      kspike=0
      do i=kjp1,jh
        cv=cv+ca(i)
        if(ca(i).le.0.0d0) then
          kcai=1
          x=1.0
         else
          x=sg*(1.0d0-da(i)/ca(i))
        end if
        ew=ew+x
        tlx=sg*(ca(i)-da(i))*csx
        if(mftyp.eq.3.and.mxtyp.le.5) tlx=tlx/wval(dble(i))**2
        flx=flx+tlx
        if(dvar(i).gt.0.0d0) then
          ce=ce+dvar(i)
          xc=xc+sg*(ca(i)-da(i))
          p=p+plam((ca(i)-da(i))*sg/de(i))
          kspike=0
         else
          kspike=1
        end if
        wl=wval(dble(i))*x+wl
        if(x.ge.wh) then
          ncen=i
          wh=x
        end if
      end do
      if(kcai.ne.0) goto 3
c    **    all line parameters now determinable
c    **    multiple lines are treated as single features for the present
 5    continue
      py=sqrt(ce)
      px=plam(xc/py)
      if(px.lt.sigl) goto 3
      kl=kl+1
      x=wval(dble(jh+1))-wval(dble(j))
      if(cv.eq.0.0d0) goto 3
      py=py*x/cv
      x=x/dble(jh-j+1)
      if(ew.eq.0.0d0) goto 3
*     mean wavelength
      wl=wl/ew
*     Is the equivalent width negative? Might be
*     fooled by early noise spikes
      if(ew.lt.0.0d0) then
*       warning message for now
*       endpoint was probably searched for in the wrong way
        write(9,*) 'Line type corrected'
     :     //' - check results for line below'
        sg=-sg
        ew=-ew
      end if
*     line skew
      s2=0.0d0
      s3=0.0d0
      do i=kjp1,jh
        xtemp=sg*(1.0d0-da(i)/ca(i))
        temp=wval(dble(i))-wl
        temp2=temp*temp
        s2=s2+xtemp*temp2
        s3=s3+xtemp*temp*temp2
      end do
*     skew
      if(s2.gt.0.0d0) then
        chskew=' '
        skew=s3/(s2*sqrt(s2))
       else
        chskew='X'
        skew=999.99d0
      end if
*
      flx=flx*x
*     find FWHM by working in from the edges
      fhp=wh*0.5d0
      i=j
 601  i=i+1
      if(i.ge.ncen) goto 602
      if(sg*(1.0-da(i)/ca(i)).lt.fhp) goto 601
*     linear interpolation for fraction of channel
 602  temp=sg*(1.0d0-da(i)/ca(i))
      flo=dble(i)-(temp-fhp)/(temp-sg*(1.0d0-da(i-1)/ca(i-1)))
*     now longer wavelength half power point
      i=jh
 603  i=i-1
      if(i.le.ncen) goto 604
      if(sg*(1.0-da(i)/ca(i)).lt.fhp) goto 603
 604  temp=sg*(1.0-da(i)/ca(i))
      fhi=dble(i)+(temp-fhp)/(temp-sg*(1.0-da(i+1)/ca(i+1)))
*     fwhm from wavelengths
      fwhm=wval(fhi)-wval(flo)
*     
      ew=ew*x
      wh=wval(dble(ncen))
      wid=x*dble(jh-j+1)
      xl=x*dble(nres)
      swid=x
      if(swid.lt.wid) swid=wid
*     vacuum wavelength
      wvc=wl
      if(kspike.le.0) then
        wer=py*0.289d0*swid/ew
        wer=sqrt(wer*wer+ssqrd*ssqrd)
       else
*	set errors to zero if there is a noise spike in line
        wer=0.0d0
        py=0.0d0
      end if
      ipr='absn'
*     line limits (i.e. continuum channels at edges) as printed
      jmn=j-1
      jmx=jh+1
      wvjmn=wval(dble(jmn))
      wvjmx=wval(dble(jmx))
      if(sg.lt.0.0d0) then
        if(isupp.gt.0) goto 3
        ipr='em  '
      end if
*     overwrite flx if n-sigma wanted
      if(chflsig.eq.'nsig') then
        if(py.ne.0.0d0) then
          flx=ew/py
         else
          flx=0.0
        end if
       else
*       rescale fluxes if necessary
        if(lrescale.and.scaleff.gt.0.0d0) then
          flx=flx/scaleff
        end if
      end if
*     set maximum value for p if needed:
      if(p.gt.4000.0) p=4000.0
*     check for component structure if required
      if(jh-j.lt.10000) then
        if(sg.gt.0.0) then
          call split(da,de,ca,nl,j,jh,nres,minsep,siglxd,sigpeak,
     1           isupp,cres)
         else
          write(6,chform) ipr,kl,wl,wer,wh,ew,py,flx,x,swid,
     1                 fwhm,p,px,wvjmn,wvjmx,skew,chskew(1:1)
          if(cres(1:1).eq.' ') then
            write(9,chform) ipr,kl,wl,wer,wh,ew,py,flx,x,swid,
     1                 fwhm,p,px,wvjmn,wvjmx,skew,chskew(1:1)
           else
            write(9,*) ipr,kl,wl,wer,ew,py
          end if
        end if
       else
        write(6,chform) ipr,kl,wl,wer,wh,ew,py,flx,x,swid,fwhm,
     1                 p,px,wvjmn,wvjmx,skew,chskew(1:1)
        if(cres(1:1).eq.' ') then
          write(9,chform) ipr,kl,wl,wer,wh,ew,py,flx,x,swid,fwhm,
     1                 p,px,wvjmn,wvjmx,skew,chskew(1:1)
         else
          write(9,*) ipr,kl,wl,wer,ew,py
        end if
*100       format(1x,a3,i5,f10.3,f7.3,f9.2,1x,f9.4,1x,f8.4,1pe11.3,
*     1     0pf8.3,2f8.3,f7.1,1x,f6.2,2f8.2,f7.2,a1)
*180       format(1x,a3,i5,f10.7,f7.4,f9.5,2f8.5,1pe11.3,0pf8.5,
*     1     2f8.5,f7.1,1x,f6.2,2f8.2,f7.2,a1)
        write(9,'('' linewidth exceeds 10000 channels'')')
      end if
 3    continue
      j=jh+1
      if(j.lt.nhi) goto 2
 8    continue
      write(6,*) kl,' lines found'
 9000 return
 9999 write(6,*) ' ****** read error ******'
      goto 9000
      end
      subroutine split(da,de,ca,nl,jlo,jhi,nres,minsep,siglxd,sigpeak,
     1                  isupp,cres)
*
*     called by ablin only
*     suggests (and that's all) components for a complex absorption line
*     input:
*     da - signal, de - sigma, ca - continuum (as ablin spec)
*     nres  is resolution in channels for finding components
*     jlo,jhi are lower,upper limits from ablin
*     siglxd is log sign for inclusion (0=all)
*	
*     output:
*     direct to unit 9
*
      implicit none
      character*(*) cres
      double precision wval,dav
*
      integer nl,jlo,jhi,nres,minsep,isupp
      double precision da(nl),de(nl),ca(nl)
      double precision siglxd,sigpeak
*
      integer i,iprc,iij,iw,ja
      integer j,jam,jb,jbx,jql,jqh,jx,jm,jt,jvar,jz,j1,j2
      integer jmn,jmx,kspike,l,lv
      double precision ce,cv,dx,ew,fc,p,px,py
      double precision sc,sca,sda,vn
      double precision x,xc,xp,wvjbx,wvjam,wvc,wer,wid
      double precision cajb,cajlo,wh,wvjmn,wvjmx,wl
      double precision yv(10000)
      integer ix(10000),im(10000),indx(10000)
      double precision tw,tpx
*     Functions:
      double precision plam
*     Common:
      double precision ssqrd,ssqrh
      common/wavrms/ssqrd,ssqrh
*     printout variables
      integer klq
      character*4 iprq
      character*4 chskew
      character*4 chflsig 
      double precision wlq,wvcq,werq,whq,ewq,pyq,flxq
      double precision xq,swidq,fwhmq,pq,pxq,skew
      common/rdc_absprnt/wlq,wvcq,werq,whq,ewq,pyq,flxq,
     :      xq,swidq,fwhmq,pq,pxq,skew,iprq,klq,chskew,chflsig
*
      jx=jhi-jlo+1
      jmn=jlo-1
      wvjmn=wval(dble(jmn))
      jmx=jhi+1
      wvjmx=wval(dble(jmx))
      if(jx.lt.2*nres) then
        write(6,100) iprq,klq,wlq,werq,whq,ewq,pyq,flxq,
     1       xq,swidq,fwhmq,pq,pxq,wvjmn,wvjmx,skew,chskew(1:1)
        if(cres(1:1).eq.' ') then
          write(9,100) iprq,klq,wlq,werq,whq,ewq,pyq,flxq,
     1       xq,swidq,fwhmq,pq,pxq,wvjmn,wvjmx,skew,chskew(1:1)
100        format(1x,a3,i5,f10.3,f7.3,f9.2,1x,f9.4,1x,f8.4,1pe11.3,
     1       0pf8.3,2f8.3,f7.1,1x,f6.2,2f8.2,f7.2,a1)
         else
          write(9,*) iprq,klq,wlq,werq,ewq,pyq
        end if
        return
      end if
      dx=(wval(dble(jhi+1))-wval(dble(jlo)))/dble(jx)
*     line is resolved --  look for local maxima and minima
      l=nres/2
      fc=dble(2*l+1)
*     goes down first
      sc=1.0d10
      jm=0
      jx=0
      i=jlo
 3    x=dav(da,nl,i,l)
*     if local average smaller than previous value, use it
      if(x.gt.sc) goto 1
      sc=x
      i=i+1
*     check have not reached the end of the line
      if(i.gt.jhi) goto 2
*     branch back for search
      goto 3
*     average increasing again, so i-1 is a minimum
 1    jm=jm+1
      im(jm)=i -1
      sc=dav(da,nl,im(jm),l)
      i=i+1
*     find the maximum locally
 5    x=dav(da,nl,i,l)
      if(x.lt.sc) goto 4
      sc=x
      i=i+1
*     check for end of line
      if(i.gt.jhi) goto 2
      goto 5
*     local maximum found
 4    jx=jx+1
      ix(jx)=i-1
      sc=dav(da,nl,ix(jx),l)
      i=i+1
      goto 3
*     max/min in line all known
 2    continue
      if(jx.lt.1) goto 77
c    **    sort out the max and minima
      if(jx.lt.jm) then
        jx=jx+1
        ix(jx)=jhi+1
      end if
      goto 6
 77   continue
      if(cres(1:1).eq.' ') then
        write(9,100) iprq,klq,wlq,werq,whq,ewq,pyq,flxq,
     1     xq,swidq,fwhmq,pq,pxq,wvjmn,wvjmx,skew,chskew(1:1)
       else
        write(9,*) iprq,klq,wlq,werq,ewq,pyq
      end if
      write(6,100) iprq,klq,wlq,werq,whq,ewq,pyq,flxq,
     1     xq,swidq,fwhmq,pq,pxq,wvjmn,wvjmx,skew,chskew(1:1)
      goto 7
 6    continue
*     check that the line level is not close to zero, and if it
*     is throw away components not significantly above zero
      jt=1
      do while (jt.lt.jm)
        x=dav(da,nl,im(jt),l)
        xp=dav(da,nl,im(jt+1),l)
        if(x.lt.de(im(jt)).and.xp.lt.de(im(jt+1))) then
*         deep line -- two components down to sigma level
          tw=0.0d0
          tpx=0.0d0
*         go to next min, and see if residual significantly above 0
          do i=im(jt),im(jt+1)
            if(de(i).gt.0.0d0) then
              tw=tw+da(i)
              tpx=tpx+de(i)*de(i)
            end if
          end do
          if(tpx.gt.0.0d0) then
            tpx=sqrt(tpx)
            tpx=plam(tw/tpx)
          end if
          if(tpx.lt.sigpeak) then
*           component not significantly above zero - remove it
            jm=jm-1
            jx=jx-1
            do i=jt,jx
              ix(i)=ix(i+1)
            end do
            xp=dav(da,nl,im(jt+1),l)
            if(x.lt.xp) im(jt+1)=im(jt)
            do i=jt,jm
              im(i)=im(i+1)
            end do
           else
            jt=jt+1
          end if
         else
          jt=jt+1
        end if
      end do
*     now try to remove any components closer together than the 
*     smoothing length NRES, by substituting the lowest local value
*     working out from minimum values. To do this requires JM>1, else
*     it is trivial
      if(jm.gt.1) then
*
*       store values at local minima
        do i=1,jm
          yv(i)=dav(da,nl,im(i),l)
        end do
*	set up index table via 'Numerical Recipes' INDEXX
        call indexx(jm,yv,indx)
*       flag closest values as necessary, from the minimum up
        jt=1
        do while (jt.lt.jm)
          jz=indx(jt)
          if(im(jz).gt.0) then
*           down from local minimum
            jvar=jz-1
            do while (jvar.gt.0)
              if(im(jz)-im(jvar).lt.minsep) then
*               remove entry from table by setting IM negative
                im(jvar)=-2000
                jvar=jvar-1
               else
                jvar=0
              end if
            end do
*	    up from local minimum
            jvar=jz+1
            do while (jvar.le.jm)
              if(im(jvar)-im(jz).lt.minsep) then
*	        remove entry flag is -2000
                im(jvar)=-2000
*	        reset max to reflect extended component width
                ix(jz)=ix(jvar)
                jvar=jvar+1
               else
                jvar=jm+1
              end if
            end do
          end if
          jt=jt+1
        end do
*	remove the flagged values
        jt=1
        do while (jt.lt.jm)
          if(im(jt).lt.0) then
            jm=jm-1
            jx=jx-1
            do i=jt,jx
              ix(i)=ix(i+1)
            end do
            do i=jt,jm
              im(i)=im(i+1)
            end do
           else
            jt=jt+1
          end if
        end do
*	check the last point, and drop if necessary
        if(im(jm).lt.0) then
          jm=jm-1
          jx=jx-1
        end if
*	and make sure that sequence is min, max, min, max, ...
        do i=1,jm-1
          if(im(i).gt.ix(i).or.im(i+1).lt.ix(i)) then
            if(cres(1:1).eq.' ') then
              write(9,100) iprq,klq,wlq,werq,whq,ewq,pyq,flxq,
     1         xq,swidq,fwhmq,pq,pxq,wvjmn,wvjmx,skew,chskew(1:1)
             else
              write(9,*) iprq,klq,wlq,werq,ewq,pyq
            end if
            write(6,100) iprq,klq,wlq,werq,whq,ewq,pyq,flxq,
     1         xq,swidq,fwhmq,pq,pxq,wvjmn,wvjmx,skew,chskew(1:1)
            write(9,1090)jx,jm
 1090       format('   *** line decomposition failed *** ',2i5)
            write(9,'('' min:'',8i8)') (im(iw),iw=1,jm)
            write(9,'('' max:'',8i8)') (ix(iw),iw=1,jx)
            goto 7
          end if
        end do
      end if
*
*     iprc is # printed components
      iprc=0
*     first line to first max etc.    for lack of anything better
*     to do, divide the max points equally.
      jb=jlo
      do 20 j=1,jm
        kspike=0
        ja=jb
        if(j.ge.jm) goto 8
        jb=ix(j)
        goto 9
 8      jb=jhi
        if(ca(ja).le.0.0d0) goto 270
 9      x=(1.0d0-da(ja)/ca(ja))*0.5d0
        if(de(ja).gt.0.0d0) then
          p=plam((ca(ja)-da(ja))*0.5d0/de(ja))
          ce=0.5d0*de(ja)*de(ja)
         else
          p=0.0d0
          ce=0.0d0
          kspike=1
        end if
        xc=(ca(ja)-da(ja))*0.5d0
        vn=0.5d0
        cv=0.5d0*ca(ja)
        ew=x
        wl=wval(dble(ja))*x
        j1=ja+1
        j2=jb-1
        if(j2.ge.j1) then
          do lv=j1,j2
            if(ca(lv).le.0.0d0) goto 270
            x=1.0-da(lv)/ca(lv)
            if(de(lv).gt.0.0d0) then
              p=p+plam((ca(lv)-da(lv))/de(lv))
              ce=ce+de(lv)*de(lv)
             else
              kspike=1
            end if
            cv=cv+ca(lv)
            vn=vn+1.0d0
            xc=xc+ca(lv)-da(lv)
            ew=ew+x
            wl=wl+x*wval(dble(lv))
          end do
        end if
        cajb=ca(jb)
        if(cajb.le.0.0d0)  goto 270
        x=(1.0d0-da(jb)/cajb)*0.5d0
        if(de(jb).gt.0.0d0) then
          p=p+plam((ca(jb)-da(jb))*0.5d0/de(jb))
          ce=ce+0.5d0*de(jb)*de(jb)
         else
          kspike=1
        end if
        xc=xc+0.5d0*(ca(jb)-da(jb))
        cv=cv+0.5d0*ca(jb)
        vn=vn+0.5d0
        ew=ew+x
        wl=wl+wval(dble(jb))*x
        if(j.gt.1) goto 11
        if(de(jlo).gt.0.0d0) then
          p=plam((ca(jlo)-da(jlo))*0.5d0/de(jlo))+p
         else
          kspike=1
        end if
        cajlo=ca(jlo)
        if(cajlo.le.0.0d0) goto 270
        x=(1.0-da(jlo)/cajlo)*0.5d0
        if(de(jlo).gt.0.0d0) ce=ce+0.5d0*de(jlo)*de(jlo)
        cv=cv+0.5d0*ca(jlo)
        vn=vn+0.5d0
        xc=xc+0.5d0*(ca(jlo)-da(jlo))
        ew=ew+x
        wl=wl+wval(dble(jlo))*x
 11     if(jb.ne.jhi) goto 12
        if(de(jhi).gt.0.0d0) then
          p=p+plam((ca(jhi)-da(jhi))*0.5d0/de(jhi))
         else
          kspike=1
        end if
        cajb=ca(jb)
        if(cajb.le.0.0d0) goto 270
        x=(1.0d0-da(jb)/cajb)*0.5d0
        if(de(jhi).gt.0.0d0) ce=ce+0.5d0*de(jhi)*de(jhi)
        cv=cv+0.5d0*ca(jhi)
        vn=vn+0.5d0
        xc=xc+0.5d0*(ca(jhi)-da(jhi))
        ew=ew+x
        wl=wl+wval(dble(jb))*x
        if(ew.eq.0.0d0) goto 270
 12     wl=wl/ew
        wh=wval(dble(im(j)))
        py=sqrt(ce)
        px=plam(xc/py)
        if(cv.eq.0.0d0) goto 270
        wid=dx*float(jb-ja+1)
        ew=ew*dx
*       vacuum wavelength
        wvc=wl
        if(kspike.le.0) then
          py=dx*py*vn/cv
          wer=py*0.289d0*wid/ew
          wer=sqrt(wer*wer+ssqrd*ssqrd)
         else
          wer=0.0d0
          py=0.0d0
        end if
        if(px.ge.siglxd) then
          if(isupp.gt.0.or.iprc.gt.0) goto 233
*	  write extra information
          if(cres(1:1).eq.' ') then
            write(9,100) iprq,klq,wlq,werq,whq,ewq,pyq,
     1        flxq,xq,swidq,fwhmq,pq,pxq,wvjmn,wvjmx,skew,chskew(1:1)
           else
            write(9,*) iprq,klq,wlq,werq,ewq,pyq
          end if
          write(6,100) iprq,klq,wlq,werq,whq,ewq,pyq,
     1        flxq,xq,swidq,fwhmq,pq,pxq,wvjmn,wvjmx,skew,chskew(1:1)
*
 233      if(ja.eq.jlo) then
            jam=jlo-1
           else
            jam=ja
          end if
          if(jb.ge.jhi-1) then
            jbx=jhi+1
           else
            jbx=jb
          end if
*	  determine residual flux fraction at higher split position
          jql=max0(jbx-l,1)
          jqh=min0(jbx+l,nl)
          sda=0.0d0
          sca=0.0d0
          do iij=jql,jqh
            if(ca(iij).gt.0.0d0) then
              sda=sda+da(iij)
              sca=sca+ca(iij)
            end if
          end do
*	  sca= data/continuum fraction at break point
          sca=sda/sca
          wvjam=wval(dble(jam))
          wvjbx=wval(dble(jbx))
          if(cres(1:1).eq.' ') then
            write(9,101) klq,wl,wer,wh,ew,py,dx,wid,p,
     1                   px,wvjam,wvjbx,sca
           else
            write(9,*) iprq,klq,wl,wer,ew,py
          end if
          iprc=iprc+1
 101      format(2x,'***',i4,f9.3,f7.3,f9.2,2f8.3,11x,2f8.3,
     1         8x,f7.1,f6.2,2f8.2,1x,f6.2)
        end if
        goto 20
 270    continue
        if(cres(1:1).eq.' ') then
          write(9,100) iprq,klq,wlq,werq,whq,ewq,pyq,flxq,
     1     xq,swidq,fwhmq,pq,pxq,wvjmn,wvjmx,skew,chskew(1:1)
         else
          write(9,*) iprq,klq,wlq,werq,ewq,pyq
        end if
        write(6,100) iprq,klq,wlq,werq,whq,ewq,pyq,flxq,
     1     xq,swidq,fwhmq,pq,pxq,wvjmn,wvjmx,skew,chskew(1:1)
 20   continue
      if(iprc.le.0) then
*	chskew='N'
        if(cres(1:1).eq.' ') then
          write(9,100) iprq,klq,wlq,werq,whq,ewq,pyq,flxq,
     1     xq,swidq,fwhmq,pq,pxq,wvjmn,wvjmx,skew,chskew(1:1)
         else
          write(9,*) iprq,klq,wlq,werq,ewq,pyq
        end if
        write(6,100) iprq,klq,wlq,werq,whq,ewq,pyq,flxq,
     1     xq,swidq,fwhmq,pq,pxq,wvjmn,wvjmx,skew,chskew(1:1)
        write(9,'(3x,''***   no significant components,'',
     1           i4,'' minima'')') jm
      end if
 7    return
      end
      subroutine indexx(n,arrin,indx)
*     index sort.
*     corrected to allow for n=1
      implicit none
      integer n
      double precision arrin(n)
      integer indx(n)
*
      integer i,j,l,ir,indxt
      double precision q
*
      do j=1,n
        indx(j)=j
      end do
*
      if(n.le.1) goto 99
*
      l=n/2+1
      ir=n
 10   continue
      if(l.gt.1)then
        l=l-1
        indxt=indx(l)
        q=arrin(indxt)
       else
        indxt=indx(ir)
        q=arrin(indxt)
        indx(ir)=indx(1)
        ir=ir-1
        if(ir.eq.1)then
          indx(1)=indxt
          goto 99
        endif
      endif
      i=l
      j=l+l
 20   if(j.le.ir)then
        if(j.lt.ir)then
          if(arrin(indx(j)).lt.arrin(indx(j+1)))j=j+1
        end if
        if(q.lt.arrin(indx(j)))then
          indx(i)=indx(j)
          i=j
          j=j+j
         else
          j=ir+1
        endif
        go to 20
      endif
      indx(i)=indxt
      go to 10
 99   return
      end
      double precision function plam(df)
      implicit none
*     compute log probability
      double precision df
      double precision temp
      double precision erfcc
      temp=abs(df)
      if(temp.ne.0.0D0) then
        temp=0.5D0*erfcc(temp/sqrt(2.0D0))
        if(temp.le.0.0d0) then
          plam=40.0d0
         else
          plam=-log10(temp)
        end if
       else
        plam=0.0d0
      end if
      return
      end
      double precision function dav(da,nl,i,l)
      implicit none
      integer nl,i,l
      double precision da(nl)
*
      integer j,lo,lh
      lo=i-l
      lh=i+l
      dav=0.0d0
      do j=lo,lh
        dav=dav+da(j)
      end do
      dav=dav/dble(2*l+1)
      return
      end
