      subroutine vp_pgbegin(nx,ny,cpgdev)
*     open plot channel as device cpgdev. If cpgdev is blank, then
*     prompts for input via whichever is the current input channel 
      integer nx,ny
      character*(*) cpgdev
*
      integer istat,ldev
      character*32 vcpgdev,defdev
*
*     Functions:
      integer pgopen,lastchpos
*
*     input channel (normally 5)
      integer inputc
      common/rdc_inputc/inputc
*
      integer ipgopen,ipgflag
      common/pgplotv/ipgopen,ipgflag
*     pgplot current device (up to 4 allowed)
      integer npgcurr,npgdev(4)
      common/vp_pgpldev/npgcurr,npgdev
*     pgplot width & aspect ratio
      real widthpg,aspectpg
      common/vpc_pgpaper/widthpg,aspectpg
*
      if(ipgopen.le.0) then
        ipgopen=1
        ipgflag=1
        lnchx=lastchpos(cpgdev)
        if(cpgdev(1:1).eq.' '.or.lnchx.le.1) then
*         argument too short, so need to prompt for plot device
          call getenv('PGPLOT_DEV',defdev)
          ldev=lastchpos(defdev)
          if(ldev.le.1) then
*           give up and assign /xw
            ldev=3
            defdev='/xw'
          end if
 903      write(6,'(a,a,a)') 'PGPLOT device? [',defdev(1:ldev),']'
          write(6,'(''> '',$)')
          read(inputc,'(a)',end=901) vcpgdev
          goto 902
 901      write(6,*) 'Reverting to standard input'
          inputc=5
          goto 903
 902      if(vcpgdev(1:1).eq.' ') then
            vcpgdev=defdev
           else
            if(vcpgdev(1:1).ne.'?'.and.vcpgdev(1:1).ne.'/') then
              vcpgdev='/'//vcpgdev(1:31)
            end if
          end if
         else
          vcpgdev=cpgdev
        end if
        istat=pgopen(vcpgdev)
        if(istat.gt.0) then
          npgcurr=npgcurr+1
          npgdev(npgcurr)=istat
         else
          write(6,*) 'Failed to open plot channel, trying /xw'
          istat=pgopen('/xw')
          if(istat.gt.0) then
            npgcurr=npgcurr+1
            npgdev(npgcurr)=istat
           else
            write(6,*) 'Totally failed to open plot channel'
            write(6,*) '  so giving up'
          end if
        end if
        if(aspectpg.gt.0.0) then
*         set paper width & aspect ratio(=height/width)
          call pgpap(widthpg,aspectpg)
        end if
        call pgsubp(nx,ny)
      end if
      return
      end
      subroutine vp_pgend
*     close the current pgplot device, and select the 
*     previous one in the list if appropriate
*
*     no arguments, info is in common
*
*     pgplot current device (up to 4 allowed)
*     npgcurr is current one (0=not opened at all)
      integer npgcurr,npgdev(4)
      common/vp_pgpldev/npgcurr,npgdev
      if(npgcurr.gt.0) then
        call pgask(.false.)
        call pgclos
        npgcurr=npgcurr-1
        if(npgcurr.gt.0) then
*         select plot channel at new npgcurr
          call pgslct(npgdev(npgcurr))
        end if
      end if
      return
      end
