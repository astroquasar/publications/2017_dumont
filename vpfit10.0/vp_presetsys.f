      subroutine vp_presetsys
*
*     set up a list of system parameters which do not vary, as preset
*     profiles to be fed in to each fitting chunk
*
*     INPUT:
*     chstr	character*132	filename, type, limits for line parameters
*
*     OUTPUT:
*     is common block of parameters parmxt
*	
      implicit none
      include 'vp_sizes.f'
*
*     local:
*     variables for setting options
      logical laonly
      logical lnosp,lnomet,lconly,lcremove,lchion,lnohyd
      logical lallmetz
*     Limits for inclusion
      double precision collo,colhi,zedlo,zedhi
      double precision bvallo,bvalhi,bvall
*     other local variables
      logical lthis
      integer i,ichunk,ifst,ierr
      integer j,jjj
      integer nccv,nx,nxxc
      double precision zedp1,wavzx
*     vp_f13finx parameters
      character*2 chxelm(1)
      character*4 chxion(1)
      double precision pxarm(maxpps)
      character*2 ixind(maxpps)
      character*4 chlnkx(1)
      integer lnkx(1)
      double precision vtbx(1),teffx(1)
*     Functions
      integer lastchpos
*
*     extra ion list & parameters
      integer nionxt
      character*2 elmxt(maxxpi)
      character*4 ionxt(maxxpi)
      character*2 ipinxt(maxxpp)
      double precision bvxmin
      double precision parmxt(maxxpp)
      common/vpc_parmxt/bvxmin,parmxt,ionxt,elmxt,nionxt,ipinxt
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     atomic data
      integer nz
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),nz
*     chunk wavelengths
      integer nwvch(maxnch)
      double precision wavch(maxchs,maxnch),wavchlo(maxchs,maxnch)
      common/vpc_wavch/wavch,wavchlo,nwvch
*     chunk variables, so have number of chunks!
      integer ndpts(maxnch),idrun(maxnch),icrun(maxnch)
      integer indfil(maxnch)
      character*64 filename(maxnch)
      double precision wvstrt(maxnch),wvend(maxnch)
      integer nchunk
      common/vpc_chunk/wvstrt,wvend,ndpts,filename,
     :      idrun,icrun,indfil,nchunk
*     string separation variables
      integer nvstr
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
      double precision dvstr(24)
      common/vpc_dsepspace/dvstr
*     previous data file and parameters
      integer ncumset
      character*132 comchstr
      logical ldate26
      logical lcuminc
      common/vpc_comchstr/comchstr,ldate26,lcuminc,ncumset
*     Lyman limit stuff
      double precision wlsmin,vblstar,collsmin
      common/vpc_lycont/wlsmin,vblstar,collsmin
      integer nverbose
      common/vp_sysmon/nverbose
      integer nopchan,nopchanh,nmonitor
      integer lt(3)             ! output channels for printing
      common/vpc_nopchan/lt,nopchan,nopchanh,nmonitor
*     
*     Some common code with rd_gprof.f
      nionxt=0
      bvxmin=1.0d30
*     if no filename, forget this lot
      if(comchstr(1:1).eq.' ') goto 999
*     Default limits [not used here]:
      collo=0.0d0
      colhi=1.0d25
      zedlo=-1000.0d0
      zedhi=1.0d25
      bvallo=0.0d0
      bvalhi=1.0d25
      bvall=0.0d0
*     steering variables, modified by character variable 10 in list
      lnohyd=.false.
      lnomet=.false.
      laonly=.false.
      lnosp=.false.
      lconly=.false.
      lcremove=.false.
      lchion=.false.
*     put in all metals independent of z
      lallmetz=.true.
*
*     make sure the atomic data is in
      if(nz.le.0) call vp_ewred(0)
*     split the filename string
      call dsepvar(comchstr,15,dvstr,ivstr,cvstr,nvstr)
*     open file. If fails, just exit routine
      open(unit=21,file=cvstr(1),status='old',err=9991)
      goto 9992
 9991 write(6,*) 'File ',cvstr(1)(1:32),' not opened!'
      write(6,*) 'Is it there?'
      call system( 'ls -l '//cvstr(1)(1:2)//'*' )
 9992 continue
      rewind(unit=21)
*     check for type:
      ifst=26
      if(ivstr(2).eq.13) then
        write(6,*) 'fort.13 style files not supported in this mode'
        goto 999
      end if
*     further control variables may be present:
      if(nvstr.ge.2) then
*	minimum (log) column density for inclusion
        if(dvstr(3).gt.0.0d0) collo=dvstr(3)
*	and maximum
        if(dvstr(4).gt.0.0d0) colhi=dvstr(4)
*	minimum redshift included
        if(dvstr(5).gt.0.0d0) zedlo=dvstr(5)
*	and maximum
        if(dvstr(6).gt.0.0d0) zedhi=dvstr(6)
*	minimum b value included
        if(dvstr(7).gt.0.0d0) bvallo=dvstr(7)
*	and maximum
        if(dvstr(8).gt.0.0d0) bvalhi=dvstr(8)
*	with the opportunity of including every line if b < bvall
        if(dvstr(9).gt.0.0d0) bvall=dvstr(9)
*	line selection flags
        jjj=10
        do while(jjj.gt.1.and.ivstr(jjj).eq.0.and.
     :             cvstr(jjj)(1:1).eq.' ')
          jjj=jjj-1
        end do
        write(6,*) 'Preset continuum flags: ',cvstr(jjj)(1:4)
        if(cvstr(jjj)(1:1).ne.' ') then
*         how many characters?
          nccv=lastchpos(cvstr(jjj))
          j=0
          do while (j.lt.nccv)
            j=j+1
            if(cvstr(jjj)(j:j).eq.'c'.or.cvstr(jjj)(j:j).eq.'C') then
*	      plot ONLY the continuum adjustments
              lconly=.true.
            end if
            if(cvstr(jjj)(j:j).eq.'d'.or.cvstr(jjj)(j:j).eq.'D') then
*	      plot ALL BUT the continuum adjustments
              lcremove=.true.
            end if
            if(cvstr(jjj)(j:j).eq.'i'.or.cvstr(jjj)(j:j).eq.'I') then
*	      ignore special characters flag
              lnosp=.true.
            end if
            if(cvstr(jjj)(j:j).eq.'l'.or.cvstr(jjj)(j:j).eq.'L') then
*	      Ly-a only in regions specified (to speed things up)
              laonly=.true.
*	      need to suppress Lyman limit absorption with this option,
*	      so reset the internal parameter for this
              collsmin=1.0d25
            end if
            if(cvstr(jjj)(j:j).eq.'m'.or.cvstr(jjj)(j:j).eq.'M') then
*	      metals only flag
              lnohyd=.true.
            end if
            if(cvstr(jjj)(j:j).eq.'n'.or.cvstr(jjj)(j:j).eq.'N') then
*	      ignore metals flag
              lnomet=.true.
            end if
            if(cvstr(jjj)(j:j).eq.'s'.or.cvstr(jjj)(j:j).eq.'S') then
              write(6,*) 's - option not supported here'
            end if
            if(cvstr(jjj)(j:j).eq.'z'.or.cvstr(jjj)(j:j).eq.'Z') then
*	      everything strictly selected by redshift
              lallmetz=.false.
            end if
          end do
        end if
      end if
 801  read(21,'(a)',end=999) inchstr
      if(inchstr(1:1).eq.'!') goto 801
      call dsepvar(inchstr,15,dvstr,ivstr,cvstr,nvstr)
      if(cvstr(1)(1:2).eq.'%%'.or.cvstr(1)(1:2).eq.'  ') goto 801
      nx=1
      ierr=0
      call vp_f13finx(nx,ifst, chxelm,chxion,pxarm,ixind,
     :                     vtbx,teffx,lnkx,chlnkx,ierr)
      if(ierr.gt.0) then
        write(6,*) 'Error',ierr
        goto 999
      end if
*     does the ion, at this redshift, have any lines in the chunks
*     specified?
      zedp1=pxarm(nppzed)+1.0d0
*     cycle through line list looking for matches
      lthis=.false.
      do i=1,nz
        if(chxelm(1).eq.lbz(i).and.chxion(1).eq.lzz(i)) then
          wavzx=zedp1*alm(i)
          do ichunk=1,nchunk
            nxxc=nwvch(ichunk)
            if(wavzx.ge.wavchlo(1,ichunk).and.wavzx.le.
     :         2.0d0*wavch(nxxc,ichunk)-wavchlo(nxxc,ichunk)) then
              lthis=.true.
              goto 802
            end if
          end do
        end if
      end do
      if(.not.lthis) goto 801
 802  if(nionxt.lt.maxxpi) then
        nionxt=nionxt+1
        elmxt(nionxt)=chxelm(1)
        ionxt(nionxt)=chxion(1)
        j=(nionxt-1)*noppsys
        do i=1,noppsys
          parmxt(j+i)=pxarm(i)
          ipinxt(j+i)='  '
        end do
*       update minimum b-value
        if(pxarm(nppbval).lt.bvxmin.and.chxelm(1).ne.'>>'.and.
     :     chxelm(1).ne.'<>'.and.chxelm(1).ne.'__'.and.
     :     chxelm(1).ne.'<<') then
          bvxmin=pxarm(nppbval)
        end if
        goto 801
       else
        write(6,*) ' '
        write(6,*) 'WARNING: PRESET ION LIST SPACE EXCEEDED'
        write(6,*) 'ONLY FIRST',nionxt,' EXTRA IONS USED'
        write(6,*) ' '
        goto 999
      end if
*
 999  continue
      if(nionxt.gt.0) then
        write(6,'(i6,a,f8.2)') nionxt,
     :       ' fixed ion presets in list, bmin=',bvxmin
        if(nopchan.ge.2) then
          write(18,*) nionxt,' fixed ion presets in list, bmin=',
     :                bvxmin
        end if
        if(nverbose.ge.12) then
          do i=1,nionxt
            j=(nionxt-1)*noppsys+nppzed
            write(6,*) elmxt(i),ionxt(i),parmxt(j)
          end do
        end if
      end if
      return
      end
