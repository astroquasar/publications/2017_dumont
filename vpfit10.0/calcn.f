      double precision function calcn( np, n, param, ipind )

* Calculate column densities, given possibilty that total column density
* stored in one component.

* This is given in the fort.13 file as follows...  all lines for which a
* total column is specified are tied with the same character (obeying
* fixing convention similar to before - upper case not varied, lower
* case varied BUT upper case not tied to lower case) where the character
* must be later than the setup parameter `lastch' (def: v). The total is
* then given by the column density in the FIRST ion (fixed or not),
* other ions contain their own density and the value for the ion
* carrying the total is calculated here before generating the profile.

* Modification - if a lower block contains the same characters as a higher
* one then form is duplicated, only first value (total) is varied.
* The % sign must be used to mark a character at the beginning of a block
* as it might otherwise by taken as part of a larger a block and checkn
* has to know it can alter the `fixed' parameters

* Another mod - if & replaces % then it and the following value
* should agree.  This allows column densities in form following blocks
* to be tied (useful for investigating the effect of subcomponent 
* structure
*
* Hint: it is probably worth having the first one in a special tied list
* as one with a high column density. Then you are less likely to get into
* trouble because the sum of components exceeds the total given.


      implicit none
      
*     this sets special='%', vspecial='@' as well as sizes
      include 'vp_sizes.f'

      integer np, n
      double precision param( np )
      character*2 ipind( np )
      logical nocgt, noceq, form
      integer top, bot, second, j, nuntie, ntie
      double precision total, subtotal, untietot, tieval
      character*2 mainchar, secchar

*     Totals after this character 
      character*2 lastch,firstch
      common/vpc_ssetup/lastch,firstch
*     Variables used: indvar=1 for logN, 0 for N, -1 for emission
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed

      
*     ajc 21 dec 93
*     setup no form following, tieval 0
      tieval=0.0d0
      form=.false.

*     not special - return value as normal
      if ( ipind( n )(1:1) .ne. special .and.
     :     ipind( n )(1:1) .ne. vspecial .and.
     :      .not. ( nocgt( ipind( n )(1:1), lastch ) ) ) then
        calcn = param( n )
        return
      endif

*     Diagnostic:
*     write ( *, * ) n, ipind( n ), param( n )

*     find the top of this chunk
      top = n
 10   continue
      if ( ipind( top )(1:1) .eq. special ) then
        go to 20
      end if
      if ( ipind( top )(1:1) .eq. vspecial ) then
        if ( top .lt. noppsys ) then
          go to 20
        end if
        if ( ipind( top - noppsys )(1:1) .ne. vspecial ) then
          go to 20
        end if
        top = top - noppsys
        go to 10
      end if
      if ( top .lt. noppsys ) then
        go to 20
      end if
*
      if ( ipind( top - noppsys ) .eq. ipind( n ) .or.
     :       ipind( top - noppsys )(1:1) .eq. special .or.
     :       ipind( top - noppsys )(1:1) .eq. vspecial ) then
        top = top - noppsys
        go to 10
      end if
 20   continue

*     find the bottom of this chunk
      bot = top
 30   continue
*     at bottom of all entries?
      if ( bot + noppsys .gt. np ) go to 40
*     jump down one if ok
      if ( ipind( bot + noppsys )(1:1) .eq. special ) then
        go to 40
      end if
      if ( ipind( bot + noppsys )(1:1) .eq. vspecial ) then
        if ( ipind( bot )(1:1) .eq. vspecial ) then
          bot = bot + noppsys
          go to 30
         else
          go to 40
        end if
      end if
*     next must be a char
      if ( ipind( bot )(1:1) .eq. special .or.
     :       ipind( bot )(1:1) .eq. vspecial ) then
        if ( nocgt( ipind( bot + noppsys )(1:1), lastch ) ) then
          bot = bot + noppsys
          go to 30
         else
          write(6,*) 'CALCN: no label for block'
          stop
        end if
       else
        if ( ipind( bot ) .eq. ipind( bot + noppsys ) ) then
          bot = bot + noppsys
          go to 30
         else
          go to 40
        end if
      end if
 40   continue

*     now find the main character + check syntax
      mainchar = '  '
      j = top
 50   continue
      if ( j .gt. bot ) then
        write(6,*) 'CALCN: no label in totalled chunk'
        stop
      end if
      if ( ipind( j )(1:1) .eq. special ) then
        form = .true.
        if ( j .eq. top ) then
          j = j + noppsys
          go to 50
         else
          write ( *, * ) 'special character not at top'
          stop
        end if
      end if
      if ( ipind( j )(1:1) .eq. vspecial ) then
        form = .true.
        j = j + noppsys
        go to 50
      end if
      mainchar = ipind( j )
      if ( .not. nocgt( mainchar(1:1), lastch ) ) then
        write ( *, * ) 'special block with ordinary label', 
     :                   n, top, bot, j, '>', ipind( j ), '<'
      end if

*     if this is form-following then find main block
      if ( form ) then
        second = nppcol
 60     continue
*       start of a new block?
          secchar = '  '
          if ( ipind( second )(1:1) .eq. special .or.
     :          ipind( second )(1:1) .eq. vspecial .or.
     :          nocgt( ipind( second )(1:1), lastch ) ) then
            j = second
 70         continue
            if ( j .gt. np ) then
              write ( *, * ) 'no end to referenced chunk'
              stop
            end if
            if ( ipind( j )(1:1) .eq. special ) then
              if ( j .eq. second ) then
                j = j + noppsys
                go to 70
              else
                write ( *, * ) 'special character not at top'
                stop
              end if
            end if
            if ( ipind( j )(1:1) .eq. vspecial ) then
              j = j + noppsys
              go to 70
            end if
            secchar = ipind( j )
            if ( .not. nocgt( secchar(1:1), lastch ) ) then
              write ( *, * ) 'special block with ordinary label'
            end if
          end if

*         if here then found a label or not special
          if ( .not. noceq( secchar(1:1), mainchar(1:1) ) ) then
            second = second + noppsys
            if ( second .gt. np ) then
              write ( *, * ) 'have not found reference'
              stop
            end if
            go to 60
          end if
        continue
      end if

*     here - have top, bot, and second (if form)
      
*     Diagnostic:
*     write ( *, * ) '---------------------'
*     write ( *, * ) ' top ', top
*     write ( *, * ) ' bot ', bot
*     write ( *, * ) ' second ', second
      
*     need to check that tied values are, either in this block or in the
*     reference if form following
      if ( .not. form ) then
        second = top
      end if
*     so look at block referenced by second
*     rfc 16.11.98: allow for linear variables
      if(indvar.eq.1) then
*       log variables
        total = 10d0 ** param( second )
       else
*       working with linear variables
        total=param(second)
      end if

*     Diagnostic:
*     write ( *, * ) ' total ', second, log10( total )

      if ( ipind( second )(1:1) .eq. vspecial ) then
        nuntie = 0
       else
        nuntie = 1
      end if
      untietot = 0d0
      do j = second + noppsys, second - top + bot, noppsys
*       write ( *, * ) j, param( j )
        if ( ipind( j - noppsys )(1:1) .ne. vspecial ) then
          nuntie = nuntie + 1
*         rfc 16.11.98: allow for linear variables
          if(indvar.eq.1) then
            untietot = untietot + 10d0 ** param( j )
*           write ( *, * ) ' untied ', nuntie
           else
            untietot = untietot + param(j)
          end if 
        end if
      end do
      ntie = ( bot - top ) / noppsys + 1 - nuntie
*     write ( *, * ) 'ntie ', ntie
      if ( ntie .gt. 0 ) then
        tieval = ( total - untietot ) / ntie
        if ( tieval .lt. 0d0 ) then
          tieval = 1d7
        end if
        subtotal = untietot + ( ntie - 1 ) * tieval
        total = subtotal + tieval
       else
        subtotal = untietot
        if ( subtotal .gt. total ) then
          total = subtotal + 1d7
        end if
      end if

*     Diagnostic writes:
*     write ( *, * ) ' -> total ', log10( total )
*     write ( *, * ) '    sub   ', log10( subtotal )
*     if(tieval.gt.0.0d0) then
*       write ( *, * ) '    tie   ', log10( tieval )
*     end if

*     now set value...
      if ( top .eq. second ) then
        if ( n .eq. top ) then

*	  Diagnostic:
*	  write ( *, *) 'Total ',total,'; subtotal ',subtotal

          if(indvar.eq.1) then
            calcn = log10( total - subtotal )
           else
            calcn=total - subtotal
          end if
        else
          if ( ipind( n - noppsys )(1:1) .eq. vspecial ) then
            if(indvar.eq.1) then
              calcn = log10( tieval )
             else
              calcn = tieval
            end if
          else
            calcn = param( n )
          end if
        end if
      else
        if ( n .eq. top ) then
          if(indvar.eq.1) then
            calcn = log10( total-subtotal) - log10(total)
     :            + param( n )
           else
            calcn=((total-subtotal)/total)*param(n)
          end if
         else
          if(ipind(second-top+n-noppsys )(1:1).eq.vspecial) then
            if(indvar.eq.1) then
              calcn = log10( tieval ) - log10( total ) + 
     :              param( top )
             else
              calcn=(tieval/total)*param(top)
            end if
          else
            if(indvar.eq.1) then
              calcn = param(second - top + n) - log10(total) 
     :              + param(top)
             else
              calcn=param(second-top+n)/total*param(top)
            end if
          end if
        end if
      end if

*       write ( *, * ) 'calcn ', n, calcn, param( n ), log10( total )
      return
      end

