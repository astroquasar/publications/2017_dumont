      subroutine vp_f13finx(ji,ifst, elm,lion,parm,ipind,
     :                     vturb,teff,lnk,chlnk,ierr)
*     free format unscrambling of f13 file line - in inchstr 
*     dvstr, ivstr, cvstr, nvstr & dvstr (common)
*
      implicit none
      include 'vp_sizes.f'
*     Input parameters:
*     ji     integer    variable set number for parameter input (so variables
*                       go in to noppsys*(ji-1)+1, .. +2, ...)
*     ifst   integer    format indicator (26 for fort.26)
      integer ji,ifst
*
*     parameters returned are:
*     elm(ji)   array element  element ID
*     lion(ji)  array element  ionization level
*     parm(noppsys*(ji-1)+1 to +noppsys)
*               array elements parameter estimates
*     ipind(noppsys*(ji-1)+1 to +noppsys)
*               array elements parameter tied/fixed indicators
*     vturb(ji) turbulent Doppler
*     teff(ji)  temperature
*     lnk(ji)   chunk number for this one
*     chlnk(ji) link character
      character*2 elm(*)
      character*4 lion(*)
      double precision parm(*)
      character*2 ipind(*)
*     turbulent velocity, temperature
      double precision vturb(*),teff(*)
*     chunk number for which the line applies (zero if all)
      integer lnk(*)
*     character for link variables, for extra constraints this ion
      character*(*) chlnk(*)
*     error flag
      integer ierr
*
*
*     LOCAL variables
      integer i,item,ix
      integer j,jlast,jx
      integer jp,jpbtem,jpctem,jpztem
      integer lpoint,lvstr,nadj,next,nvx
      character*2 cpc(14)
      double precision xval(14)
*     forcing bturb or T variable set
      integer nstyle
      double precision vtbh,teffh
*     local sepspace
      character*24 cvxx(1)
      integer nvxx,ivxx(1)
      double precision dvxx(1)
*
*     FUNCTIONS
      logical lcase,nocgt
*
*     COMMON variables
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
      double precision dvstr(24)
      common/vpc_dsepspace/dvstr
*
*     separation is already done, so dvstr, ivstr & cvstr set
*
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     error estimates held in common, in case they are wanted
      double precision colsig,zeesig,bvsig
      common/vpc_insigest/colsig,zeesig,bvsig
*     preset limits to be applied if necessary
      double precision bvalmin,bvalmax,bltdrop,bgtdrop,colltdrop,
     :                     clogltdrop,cvaldrop,clvaldrop,cvalmax,
     :                     clvalmax,bvalminh,bvalmaxh,clvalmin,
     :                     cvalmin
      common/vpc_bvallims/bvalmin,bvalmax,bltdrop,bgtdrop,colltdrop,
     :                     clogltdrop,cvaldrop,clvaldrop,cvalmax,
     :                     clvalmax,bvalminh,bvalmaxh,clvalmin,
     :                     cvalmin
*     lastch tied for normal operation, later are special
*     depending on the variable involved
      character*2 lastch,firstch
      common/vpc_ssetup/lastch,firstch
*     rest wavelength stuff. Action may depend on lqmucf
      logical lqmucf,lchvsqmu
      double precision qmucf
      double precision qscale
      common/vpc_qmucf/qmucf(maxats),qscale,lqmucf,lchvsqmu
*     mimformat for da/a, I/O units
      double precision unitamim
      common/vpc_scalemim/unitamim
*     max number of add/remove lines iterations, bval adjust preprof.
      integer maxadrit
      logical lbadj
      common/vpc_maxits/maxadrit,lbadj
*     fix parameter flag
      character*4 specchar
      common/vpc_specchar/specchar
*     character collating sequence ranges
      integer nchlims
      common/vpc_charlims/nchlims(6)
*     monitoring level 0:minimal 1:skeletal 2:standard 3: alpha
*     4: ...      16:everything
*      integer nverbose
*      common/vp_sysmon/nverbose
*
*
*     error condition:
      ierr=0
*     array length
      lvstr=len(cvstr(1))
*     parameter pointer
      jp=(ji-1)*noppsys
*     current parameter positions
      jpbtem=jp+nppbval
      jpctem=jp+nppcol
      jpztem=jp+nppzed
*
*     deal with first character string
      if(cvstr(1)(2:2).eq.' ') then
*       concatenate first two strings to get element/ionization
        cvstr(1)=cvstr(1)(1:2)//cvstr(2)(1:58)
*       copy the rest down
        if(nvstr.ge.3) then
          do j=2,nvstr-1
            cvstr(j)=cvstr(j+1)
            dvstr(j)=dvstr(j+1)
            ivstr(j)=ivstr(j+1)
          end do
          cvstr(nvstr)=' '
          dvstr(nvstr)=0.0d0
          ivstr(nvstr)=0
        end if
        nvstr=nvstr-1
      end if
*
*     expand the element descriptor if necessary
*     nchlims used, with HD, CO exceptions allowed for
      if(ichar(cvstr(1)(2:2)).ge.nchlims(3).and.
     :     ichar(cvstr(1)(2:2)).le.nchlims(4).and.
     :     cvstr(1)(1:2).ne.'HD'.and.cvstr(1)(1:2).ne.'CO') then
        cvstr(1)=cvstr(1)(1:1)//' '//cvstr(1)(2:lvstr)
      end if
*
*     check if the first character string contains a decimal point,
*     in which case it has to be separated into ion/number
      lpoint=0
      do j=3,lvstr
        if(cvstr(1)(j:j).eq.'.') then
          lpoint=j
          goto 691
        end if
      end do
*
*     if decimal point present in extended field, look for the first number
 691  ix=6
      if(lpoint.gt.5) then
        do i=3,lpoint-1
          if(cvstr(1)(i:i).ne.' ') then
*	    nvx defined and used later, so here as a dummy variable
            read(cvstr(1)(i:i),'(i1)',err=9698) nvx
            ix=i
            goto 9699
 9698       continue
          end if
        end do
*       first number position determined, make sure it is > 5
 9699   ix=max0(6,ix)
*       move the string
        nvstr=nvstr+1
        do j=nvstr,2,-1
          cvstr(j)=cvstr(j-1)
          dvstr(j)=dvstr(j-1)
          ivstr(j)=ivstr(j-1)
        end do
*       first string to contain element/ion
        cvstr(1)=cvstr(1)(1:ix-1)
        cvstr(2)=cvstr(2)(ix:lvstr)
      end if
*
*     unpack the required values
*     element:
      elm(ji)=cvstr(1)(1:2)
*     ionization
      lion(ji)=cvstr(1)(3:ix-1)
*
*     extract parameters from the remaining character sets
      nvx=min0(nvstr,14)
      do i=2,nvx
*
*       if output overflow, then replace with some random number
        if(cvstr(i)(1:1).eq.'*') then
          cvstr(i)='-1.000'
        end if
*	seek last non-space character 
        jx=lvstr
        do while (cvstr(i)(jx:jx).eq.' '.and.jx.gt.1)
          jx=jx-1
        end do
*       special case for fort.26 upper limit style:
*       remove '<' at start of this character
        if(i.eq.6) then
          if(cvstr(i)(1:1).eq.'<') then
            cvstr(i)=cvstr(i)(2:lvstr)//' '
          end if
        end if
*       special case for nan - set to zero in absence of anything sensible
        if(cvstr(i)(1:3).eq.'nan') then
          cvstr(i)='0.0'
        end if
*
*	split into parameter and flag (up to two characters)
        if(jx.ge.2) then
*	  ipind may be two characters
          cpc(i)=cvstr(i)(jx-1:jx)
          jlast=jx-2
          if(cpc(i)(1:1).eq.'.'.or.cpc(i)(1:1).eq.'0'.or.
     :       cpc(i)(1:1).eq.'1'.or.cpc(i)(1:1).eq.'2'.or.
     :       cpc(i)(1:1).eq.'3'.or.cpc(i)(1:1).eq.'4'.or.
     :       cpc(i)(1:1).eq.'5'.or.cpc(i)(1:1).eq.'6'.or.
     :       cpc(i)(1:1).eq.'7'.or.cpc(i)(1:1).eq.'8'.or.
     :       cpc(i)(1:1).eq.'9') then
*	    move up a character
            cpc(i)=cpc(i)(2:2)//' '
            jlast=jlast+1
*	    check if it is a number
            if(cpc(i)(1:1).eq.'.'.or.cpc(i)(1:1).eq.'0'.or.
     :         cpc(i)(1:1).eq.'1'.or.cpc(i)(1:1).eq.'2'.or.
     :         cpc(i)(1:1).eq.'3'.or.cpc(i)(1:1).eq.'4'.or.
     :         cpc(i)(1:1).eq.'5'.or.cpc(i)(1:1).eq.'6'.or.
     :         cpc(i)(1:1).eq.'7'.or.cpc(i)(1:1).eq.'8'.or.
     :         cpc(i)(1:1).eq.'9') then
              cpc(i)='  '
              jlast=jx
            end if
          end if
*	  read(cvstr(i)(1:jlast),'(f15.0)',err=999) xval(i)
*	  rfc 25.08.96: free input format OK
          read(cvstr(i)(1:jlast),*,err=999) xval(i)
         else
          if(jx.eq.1) then
*	    either a single character or a number
            cpc(i)=cvstr(i)(1:1)//' '
            if(cpc(i)(1:1).eq.'.'.or.cpc(i)(1:1).eq.'0'.or.
     :         cpc(i)(1:1).eq.'1'.or.cpc(i)(1:1).eq.'2'.or.
     :         cpc(i)(1:1).eq.'3'.or.cpc(i)(1:1).eq.'4'.or.
     :         cpc(i)(1:1).eq.'5'.or.cpc(i)(1:1).eq.'6'.or.
     :         cpc(i)(1:1).eq.'7'.or.cpc(i)(1:1).eq.'8'.or.
     :         cpc(i)(1:1).eq.'9') then
              read(cvstr(i),'(i1)',err=999) item
              xval(i)=dble(item)
              cpc(i)='  '
             else
              xval(i)=0.0d0
            end if
           else
*           this bit should never be used, unless line shortened:
            xval(i)=0.0d0
            cpc(i)='  '
          end if
        end if
      end do
*     
*     conditions for fort.26:
*     real(ivstr(7)).ne.dvstr(7)
*     fort.13:
*     cvstr(8)(1:1).eq.'!'.or.nvstr.le.5
*     
*     allow fort.13 line in fort.26 file if short line
*     or 5,6,7 are 0.00   0.00E+00  0
*     or fixed format from screen with leading blanks, E, sign, and 
*     a last digit in the right place 
*     RFC 11/1/11
*     added bit to force ion data format if necessary. If a line contains '['
*     must be 26, if 'bturb=' or 'T=' style 13.
      nstyle=0
      teffh=0.0d0
      vtbh=0.0d0
*      write(6,*) nvstr
      do i=1,nvstr
        if(cvstr(i)(1:1).eq.'[') nstyle=26
        if(cvstr(i)(1:6).eq.'bturb='.or.cvstr(i)(1:2).eq.'T=') then
          nstyle=13
*         get the value
          if(cvstr(i)(1:2).eq.'T=') then
            cvstr(i)(1:2)='  '
            call dsepvar(cvstr(i),1,dvxx,ivxx,cvxx,nvxx)
            teffh=dvxx(1)
            vtbh=0.0d0
           else
            cvstr(i)(1:6)='      '
            call dsepvar(cvstr(i),1,dvxx,ivxx,cvxx,nvxx)
            teffh=0.0d0
            vtbh=dvxx(1)
          end if
*          write(6,*) teffh,vtbh
        end if
      end do
*      
      if(ifst.ne.26.or.nvstr.le.5.or.
     :  cvstr(6)(3:8).eq.'00E+00'.or.
     :  (inchstr(1:3).eq.'   '.and.inchstr(62:62).eq.'E'.and.
     :   (inchstr(63:63).eq.'+'.or.inchstr(63:63).eq.'-').and.
     :    inchstr(68:68).ne.' '.and.inchstr(69:69).eq.' ')) then
*	default fort.13 is col=2, redshift=3, b=4
        parm(jpctem)=xval(2)
        ipind(jpctem)=cpc(2)
        parm(jpztem)=xval(3)
        ipind(jpztem)=cpc(3)
        parm(jpbtem)=xval(4)
        ipind(jpbtem)=cpc(4)
*       extended dataset
        next=5
        if(noppsys.ge.4) then
          parm(jp+4)=xval(5)
          ipind(jp+4)=cpc(5)
*         need to allow for internal scaling
          if(lqmucf.and.
     :       ((cpc(5)(1:1).eq.'q'.or.cpc(5)(1:1).eq.'Q').or.
     :        (cpc(5)(1:1).eq.'m'.or.cpc(5)(1:1).eq.'M'))) then
            parm(jp+4)=xval(5)/qscale
            if(unitamim.le.1.0d-3) then
              parm(jp+4)=parm(jp+4)*unitamim
            end if
           else
            parm(jp+4)=xval(5)
*           4th parameter not used for anything else, so fix it
            if(cpc(5)(1:1).eq.' ') then
              ipind(jp+4)=specchar(1:1)//'E'
            end if
          end if
          next=6
          if(noppsys.ge.5) then
            parm(jp+5)=xval(6)
            ipind(jp+5)=cpc(6)
            next=7
          end if
        end if
*
*       further values are direct copies unless nstyle.eq.13
        if(nstyle.eq.13) then
          vturb(ji)=vtbh
          teff(ji)=teffh
          lnk(ji)=ivstr(next)
         else
          vturb(ji)=dvstr(next)
          teff(ji)=dvstr(next+1)
          next=next+2
          lnk(ji)=ivstr(next)
        end if
*       test for character
        if(lnk(ji).eq.0.and.cvstr(next)(1:1).ne.'0') then
          chlnk(ji)=cvstr(next)(1:1)
         else
          chlnk(ji)='0'
        end if
        colsig=0.0d0
        zeesig=0.0d0
        bvsig=0.0d0
       else
*	col=6, z=2, b=4
        parm(jpctem)=xval(6)
        colsig=xval(7)
        ipind(jpctem)=cpc(6)
        parm(jpztem)=xval(2)
        zeesig=xval(3)
        ipind(jpztem)=cpc(2)
        parm(jpbtem)=xval(4)
        bvsig=xval(5)
        ipind(jpbtem)=cpc(4)
        next=8
*       extended parameter format
        if(noppsys.ge.4) then
          ipind(jp+4)=cpc(8)
*         need to allow for internal scaling
          if(lqmucf.and.
     :       ((cpc(8)(1:1).eq.'q'.or.cpc(8)(1:1).eq.'Q').or.
     :        (cpc(8)(1:1).eq.'m'.or.cpc(8)(1:1).eq.'M'))) then
*           rescale printed value to true
            xval(8)=xval(8)*unitamim
            parm(jp+4)=xval(8)/qscale
           else
            parm(jp+4)=xval(8)
          end if
          next=10
          if(noppsys.ge.5) then
            parm(jp+5)=xval(10)
            ipind(jp+5)=cpc(10)
            next=12
          end if
        end if
*             
        if(nvstr.ge.next) then
          lnk(ji)=ivstr(next)
*	  test for character
          if(lnk(ji).eq.0.and.cvstr(next)(1:1).ne.'0') then
            chlnk(ji)=cvstr(next)(1:1)
           else
            chlnk(ji)='0'
          end if
          next=next+1
         else
          lnk(ji)=0
          chlnk(ji)='0'
        end if
        if(lcase(ipind(jpbtem)(1:1))) then
*	  tied b-value, so need vturb, temperature
          vturb(ji)=dvstr(next+1)
          teff(ji)=dvstr(next+3)
         else
          vturb(ji)=0.0d0
          teff(ji)=0.0d0
        end if
      end if
*     Check variable ranges acceptable, but not for
*     special cases, or for temp/turb:
      if(elm(ji).ne.'<>'.and.elm(ji).ne.'__'.and.elm(ji).ne.'>>'
     :  .and.lbadj.and.(.not.nocgt(ipind(jpbtem)(1:1),lastch))) then
        nadj=0
        if(elm(ji).eq.'H ') then
          if(parm(jpbtem).lt.bvalminh) then
            parm(jpbtem)=bvalminh
            nadj=1
          end if
          if(parm(jpbtem).gt.bvalmaxh) then
            parm(jpbtem)=bvalmaxh
            nadj=1
          end if
         else
          if(parm(jpbtem).lt.bvalmin) then
            parm(jpbtem)=bvalmin
            nadj=1
          end if
          if(parm(jpbtem).gt.bvalmax) then
            parm(jpbtem)=bvalmax
            nadj=1
          end if
        end if
        if(nadj.eq.1) then
          write(6,*) 'BVAL ADJUSTED TO BE WITHIN RANGE: '
          write(6,*) elm(ji),lion(ji),parm(jpztem),
     :                 parm(jpbtem),parm(jpctem)
        end if
      end if
*
 1000 return
*
*     disaster flag
 999  ierr=1
*     may be <EOF>, so suppress annoying error message
*     write(6,*) ' Data read error'
      goto 1000
      end

