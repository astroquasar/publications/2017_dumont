      subroutine vp_stripcmt(inchstr,chend)
      implicit none
      character*(*) inchstr
      character*(*) chend
*     strip off trailing comment fields, denoted by character string chend
*     from string inchstr  [use: e.g. call vp_stripcmt(inchstr,'!')]
      integer nvstr,nlend,nstop,ncha
      nvstr=len(inchstr)
      nlend=len(chend)
      nstop=nvstr-nlend+1
      nlend=nlend-1
      ncha=1
      do while(ncha.le.nstop.and.inchstr(ncha:ncha+nlend).ne.chend)
        ncha=ncha+1
      end do
      if(ncha.le.nstop) then
        inchstr=inchstr(1:ncha-1)
      end if
      return
      end
