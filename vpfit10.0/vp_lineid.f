      subroutine vp_lineid(xmpg,xhpg,ympg,yhpg)
*
*     searches through file for ID's in plot region (xmpg - xhpg), and 
*     marks them on the plot (vertical range ympg - yhpg).
*
      implicit none
      include 'vp_sizes.f'
*
*     variables used by PGPLOT, so real
      real xmpg,xhpg,ympg,yhpg
*
*     LOCAL variables
      character*2 eldisp
      character*4 ionzdisp
      logical llist
      integer i,ifst,iks,itemp,j,ji
      integer kend,nstar
      double precision zed
*     double precision wvdisp
*     PGPLOT variables:
      real schxpg
      real tempg,tchpg,ypospg,ylopg,yuppg,ytempg
*     restricted list variable .. not used at present
      character*2 ion
      character*4 level
      character*132 chstr
*
*     local array variables for call to vp_f13finx
      integer ierr
      integer lnk(1)
      character*2 elm(1),ipd(maxpps)
      character*4 lion(1),chlnk(1)
      double precision prm(maxpps),vtb(1),teff(1)
*     function declarations
      logical ucase
      integer lastchpos
*
*
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm
      integer m
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),m
*     line list variables
      double precision zmark
      character*132 chmarkfl
      logical lmarkl,lmarkto
      common/rdc_lmarklv/zmark,chmarkfl,lmarkl,lmarkto
*     pgplot attributes: data, error, continuum, axes, ticks, RESIDUAL
*       1 color indx; 2 line style; 3 line width; 
*       4 curve(0), hist(1); 5 - 10 reserved.
      integer ipgatt(10,9)
      common/vpc_pgattrib/ipgatt
*     character string separators
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
      double precision dvstr(24)
      common/vpc_dsepspace/dvstr
*     stack spectra variables
      character*2 atoms(64)
      character*4 ions(64)
      double precision rstwavs(64)
      integer nlins
      common/rdc_stplvar/atoms,ions,rstwavs,nlins
*     plot/print velocity scale?
      logical lvel
      real wcenpg,vellopg,velhipg,zp1refpg
      common/vpc_pgvels/wcenpg,vellopg,velhipg,lvel,zp1refpg
*     observed wavelengths?
      logical lobswav,ltick
      common/rdc_lobswav/lobswav,ltick
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     constants
      real ccvacpg
      common/vpc_sgconst/ccvacpg
*     
*     make sure the atomic data is read in, otherwise nothing
*     can be done here
      if(m.le.0) call vp_ewred(0)
*     if that fails, ask for filename
      if(m.le.0) call vp_ewred(1)
*     at this stage it is the user's fault if it goes wrong
      if(m.le.0) then
        write(6,*) 'Line IDs omitted'
        goto 978
      end if
      eldisp='  '
      ionzdisp='    '
*     wvdisp=0.0d0
*
*     write(6,*) 'Redshift is:',zmark,'  velplot:',lvel
*     write(6,*) nlins,' lines'
      if(zmark.gt.-10.0d0) then
*     redshift given, so grab all the lines from atom.dat at 
*     that redshift, and mark them on the plot!
*     set pgplot attributes
        call pgsci(abs(ipgatt(1,5)))
        call pgsls(ipgatt(2,5))
        call pgslw(ipgatt(3,5))
*       character size (relative to 1/40 of box)
        call pgqch(schxpg)
        do i=1,m
          if(lvel) then
*	    velocity scale with respect to reference redshift
*	    and sequence of reference lines. Need to get
*	    local velocity and position in sequence
            do iks=1,nlins
              tempg=real(alm(i)*(1.0d0+zmark)/rstwavs(iks))
     :                 /zp1refpg
              tempg=ccvacpg*(tempg*tempg-1.0)
     :                 /(tempg*tempg+1.0)
              if(tempg.ge.xmpg.and.tempg.le.xhpg) then
*	        sort out y positions for ticks from iks value
*	        remembering that spectra are supposed to be normalized
*	        so continuum is unity
                yuppg=real(iks-1)+0.98
                ylopg=yuppg-0.1
                call pgmove(tempg,ylopg)
                call pgdraw(tempg,yuppg)
              end if
            end do
           else
            tempg=real(alm(i)*(1.0d0+zmark))
            if(tempg.ge.xmpg.and.tempg.le.xhpg) then
*	      write a tick mark, and info
              if(lmarkto) then
*	        ticks only
                call pgmove(tempg,0.90*yhpg)
                call pgdraw(tempg,0.98*yhpg)
               else
                ypospg=0.47*yhpg
                if(zmark.ne.0.0d0) then
                  itemp=int(alm(i))
                  write(chstr,'(i5)') itemp
                  inchstr=lbz(i)//lzz(i)//chstr(1:5)
*                 redshift
                  write(chstr,'(f10.6)') zmark
                  inchstr=inchstr(1:11)//chstr(1:10)
                 else
*	          zero redshift, so give more accurate wavelengths
*	          and drop the redshift
                  write(chstr,'(f10.3)') alm(i)
                  if(lbz(i).eq.'_ ') then
*	            just a line table, so move up and down to separate
                    if(i/2*2.eq.i) then
                      inchstr=chstr(1:10)//'           '
                     else
                      inchstr='          '//chstr(1:10)//' '
                    end if
                   else
                    inchstr=lbz(i)//lzz(i)//chstr(1:10)//'     '
                  end if
                end if
                tchpg=tempg+schxpg*(xhpg-xmpg)/112.0
                call pgptext(tchpg,ypospg,90.0,0.0,inchstr(1:21))
*	        and a tick mark to guide the eye
                ytempg=max(0.37*yhpg,ympg)
                call pgmove(tempg,ytempg)
                call pgdraw(tempg,0.45*yhpg)
                call pgmove(tempg,0.90*yhpg)
                call pgdraw(tempg,0.98*yhpg)
              end if
            end if
          end if
        end do
*       done, so branch to end
        goto 978
      end if
*
*     Redshift silly (<-10), so
*     attempt to split line list info string
      call dsepvar(chmarkfl,15,dvstr,ivstr,cvstr,nvstr)
      if(cvstr(1)(1:1).eq.' ') goto 978
      if(cvstr(2)(1:4).eq.'list') then
        llist=.true.
        open(unit=20,file=cvstr(1),status='old',err=9997)
       else
        llist=.false.
      end if
 979  continue
      if(llist) then
        read(20,'(a)',end=980) cvstr(1)
      end if
      open(unit=21,file=cvstr(1),status='old',err=9997)
*     set pgplot attributes
      call pgsci(abs(ipgatt(1,5)))
      call pgsls(ipgatt(2,5))
      call pgslw(ipgatt(3,5))
      nstar=0
*     check for type:
      if(ivstr(2).eq.13) then
        ifst=13
       else
        ifst=26
      end if
*     further control variables may be present:
      if(nvstr.ge.2) then
*       search for a capitalized one, indicating a particular ion
*       will give the last one in the list
        j=1
        do while(j+1.lt.nvstr)
          j=j+1
          if(ucase(cvstr(j)(1:1))) then
*           set element, ionization
            if(ucase(cvstr(j)(2:2)).and.cvstr(j)(1:2).ne.'HD'.and.
     :           cvstr(j)(1:2).ne.'CO') then
              eldisp=cvstr(j)(1:1)//' '
              ionzdisp=cvstr(j)(2:5)
*              wvdisp=dvstr(j+1)
              j=j+1
             else
*             element (or molecule) is first two characters
              eldisp=cvstr(j)(1:2)
              ionzdisp=cvstr(j+1)(1:4)
*              wvdisp=dvstr(j+2)
              j=j+2
            end if
          end if
        end do
*       write(6,*) 'Selecting ',eldisp,ionzdisp
*       minimum (log) column density for inclusion
c       if(dvstr(3).gt.0.0d0) collo=dvstr(3)
*       and maximum
c       if(dvstr(4).gt.0.0d0) colhi=dvstr(4)
*       minimum redshift included
c       if(dvstr(5).gt.0.0d0) zedlo=dvstr(5)
*       and maximum
c       if(dvstr(6).gt.0.0d0) zedhi=dvstr(6)
*       minimum b value included
c       if(dvstr(7).gt.0.0d0) bvallo=dvstr(7)
*       and maximum
c       if(dvstr(8).gt.0.0d0) bvalhi=dvstr(8)
*       with the opportunity of including every line if b < bvall
c       if(dvstr(9).gt.0.0d0) bvall=dvstr(9)
c       if(cvstr(10)(1:1).ne.' ') then
*         Ly-a only in regions specified (to speed things up)
c	  laonly=.true.
*	  need to suppress Lyman limit absorption with this option,
*	  so reset the internal parameter for this
c	  collsmin=1.0e25
c       end if
      end if
 801  read(21,'(a)',end=9997) chstr
      if(chstr(1:1).eq.'!') goto 801
      call vp_stripcmt(chstr,'!')
*     special bit for fort.13 filenames:
      call dsepvar(chstr,1,dvstr,ivstr,cvstr,nvstr)
      if(ifst.eq.13.and.nstar.eq.0.and.cvstr(1)(1:1).eq.'*') then
        nstar=nstar+1
 909    read(21,'(a)',end=9997) chstr
        if(chstr(1:1).eq.'!') goto 909
        call vp_stripcmt(chstr,'!')
        call dsepvar(chstr,15,dvstr,ivstr,cvstr,nvstr)
        if(cvstr(1)(1:1).ne.'*') then
          goto 909
        end if
        nstar=nstar+1
      end if
      do while (chstr.ne.' ')
        inchstr=chstr
        call dsepvar(inchstr,15,dvstr,ivstr,cvstr,nvstr)
*       skip if wavelength region info.
        if(cvstr(1)(1:2).eq.'%%')  goto 996
*       array format arguments
        ji=1
        call vp_f13finx(ji,ifst, elm,lion,prm,ipd,
     :                    vtb,teff,lnk,chlnk,ierr)
*       most variables not used, so don't need to copy
        ion=elm(1)
        level=lion(1)
        zed=prm(nppzed)
*       was call vp_f13fin(ion,level,col,indcol,zed,indz,bval,indb,
*     :               vtb,teff,lnk,chlnkv,ierr,ifst)
*       As called:  c2,c4,r,c2,r,c2,r,c2,r,r,i,c4,i,i
*       In routine: c2,c4,r,c2,r,c2,r,c2,r,r,i,c*,i,i
*       lnk, chlnkv are region specific variables  & ignored.
*       if(col.gt.35.0d0) col=log10(col)
        if(ierr.ne.0) then
          goto 996
        end if
*       limits for range
        if(eldisp(1:2).ne.'  ') then
*         have further restriction to check against, only nominated ion
*         is displayed (but any lines of that ion in the region will be)
          if(ion.ne.eldisp.and.level.ne.ionzdisp) goto 996
        end if
*  981  continue
*       search for lines in region
*
        do i=1,m
          if(ion.eq.lbz(i).and.level.eq.lzz(i)) then
            if(lvel.and.ltick) then
*             velocity scale with respect to reference redshift
*	      and sequence of reference lines. Need to get
*	      local velocity and position in sequence
              do iks=1,nlins
                tempg=real(alm(i)*(1.0d0+zed)/rstwavs(iks))/zp1refpg
                tempg=ccvacpg*(tempg*tempg-1.0)
     :                 /(tempg*tempg+1.0)
                if(tempg.ge.xmpg.and.tempg.le.xhpg) then
*	          sort out y positions for ticks from iks value
*	          remembering that spectra are supposed to be normalized
*	          so continuum is unity
                  yuppg=real(iks-1)+0.98
                  ylopg=yuppg-0.1
                  call pgmove(tempg,ylopg)
                  call pgdraw(tempg,yuppg)
                end if
              end do
             else
*             Wavelength plot
              tempg=real(alm(i)*(1.0d0+zed))
              if(tempg.ge.xmpg.and.tempg.le.xhpg) then
                if(lmarkto) then
*                 ticks only
                  call pgmove(tempg,0.90*yhpg)
                  call pgdraw(tempg,0.98*yhpg)
                 else
*	          write a tick mark, and info
                  ypospg=0.47*yhpg
                  itemp=int(alm(i))
                  write(chstr,'(i5)') itemp
                  inchstr=ion//level//chstr(1:5)
*	          redshift
                  write(chstr,'(f10.6)') zed
                  inchstr=inchstr(1:11)//chstr(1:10)
                  call pgptext(tempg,ypospg,90.0,0.0,inchstr(1:21))
*	          and a tick mark to guide the eye
                  call pgmove(tempg,0.37*yhpg)
                  call pgdraw(tempg,0.45*yhpg)
                  call pgmove(tempg,0.90*yhpg)
                  call pgdraw(tempg,0.98*yhpg)
                end if
              end if
            end if
          end if
        end do
*       more data?
 996    read(21,'(a)',end=998) chstr
        if(chstr(1:1).eq.'!') goto 996
        call vp_stripcmt(chstr,'!')
        goto 990
 998    chstr=' '
 990    continue
      end do
*     close file if open
      close(unit=21)
      if(llist) then
        goto 979
      end if
*
 978  return
 9997 kend=lastchpos(cvstr(1))
      if(kend.gt.0) then
        write(6,*) 'Empty parameter file: ',cvstr(1)(1:kend)
       else
        write(6,*) 'Parameter filename not set'
      end if
      close(unit=21)
      goto 978
 980  write(6,*) 'End of file list'
      close(unit=20)
      goto 978
      end
