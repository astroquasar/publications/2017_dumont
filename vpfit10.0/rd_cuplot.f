      subroutine rd_cuplot(dh,dhe,drms,re,ngp)
c     Plot spectrum using the cursor
c     
c     INPUT:
c     dh	dble array 	data of length ngp
c     dhe	dble array	variance
c     re	dble array	continuum
c     ngp	integer
c
      implicit none
      include 'vp_sizes.f'
      integer ngp
      double precision dh(*),dhe(*),drms(*),re(*)
*     local
      character*1 chc
      integer ind,ngcount
*     Common:
*     non-linear coeffts
      double precision wcfd(maxwco,maxnch)
      common/vpc_wcfd/wcfd
      integer ipgopen,ipgflag
      common/pgplotv/ipgopen,ipgflag
      integer nst,ncn,lc1,lc2
      real fampg
      common/ppam/nst,ncn,lc1,lc2,fampg
      character*4 chcv(10)
*     chcv(1) controls use of cursor for input guesses N,b,z
      common/vpc_chcv/chcv
*     last positions of cursor, for convenience
      real wv1pg,yv1pg              ! pgplot variables
      common/vpc_gcurv/wv1pg,yv1pg
*     chunk variables:
      integer ndpts(maxnch),idrun(maxnch),icrun(maxnch)
      integer indfil(maxnch)
      character*64 filename(maxnch)
      double precision wvstrt(maxnch),wvend(maxnch)
      integer nchunk
      common/vpc_chunk/wvstrt,wvend,ndpts,filename,
     :      idrun,icrun,indfil,nchunk
*     redirect next plot variables
      logical lsnap
      integer nsnap
      character*16 chpgh
      character*8 pgsfile,pghfile,pglfile
      common/rdc_pgsnap/lsnap,nsnap,pgsfile,pghfile,pglfile
*
*     new plot, so initialize y-value in case previous scale
*     was huge
      yv1pg=0.0
      ind=1
      ngcount=0
      chc='n'
      do while (chc.ne.'y'.and.chc.ne.'Y'.and.chc.ne.'q'.and.
     :              chc.ne.'Q'.and.chc.ne.' ')
        if(chc.eq.'v'.or.chc.eq.'V') then
*         multiplot velocity stacked
          call rd_stplot(dh,dhe,drms,re,ngp,ind)
          chc='n'
         else
*         hardcopy plot?
          if(lsnap) then
            nsnap=nsnap+1
            if(nsnap.lt.10) then
              write(chpgh,'(a6,i1,a4)') '"pgp10',nsnap,'.ps"'
             else
              write(chpgh,'(a5,i2,a4)') '"pgp1',nsnap,'.ps"'
            end if
            chpgh=chpgh(1:11)//pglfile(1:5)
            write(6,*) 'Plotfile is ',chpgh(2:10)
            call pgbegin(0,chpgh,1,1)
            call pgask(.false.)
            call splot(dh,dhe,drms,re,ngp,ind)
*	    clear .snap variables
            lsnap=.false.
            call pgbegin(0,pgsfile,1,1)
            call pgask(.false.)
          endif
          call splot(dh,dhe,drms,re,ngp,ind)
        end if
        ngcount=ngcount+1
        if(ngcount.le.1) then
          write(6,*) 'Expand plot if needed:'
          write(6,*) 'Cursor ("e" to mark edges, "q" when OK)'
        end if
        ind=1
        call rd_plcset(chc,ngp,dh,dhe,drms,re)
*       write(6,*) 'code ',chc
*       suppress call to plset if buttons used:
        if(chc.eq.'n'.or.chc.eq.'v'.or.chc.eq.'y'.or.
     :       chc.eq.'V') ind=2
*       abandon this region
        if(chc.eq.'q'.or.chc.eq.'Q') then
          return
        endif
      end do
*
      return
      end
