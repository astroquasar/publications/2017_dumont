      subroutine vp_chsation(ch1,ch2,atom,ionz,next)
*     extract atom and ionization level from ch1 & ch2, assuming
*     that a capital letter is an ion separator
*     next=1 if both come from the first character string
*     next=2 if ionization level from second character string
      implicit none
      character*(*) ch1
      character*(*) ch2
      character*2 atom
      character*4 ionz
      integer next
*
*     Function
      logical ucase
*
*     allow for the two molecules
      if(ucase(ch1(2:2)).and.ch1(1:2).ne.'HD'.and.
     1                       ch1(1:2).ne.'CO') then
        atom=ch1(1:1)//' '
        ionz=ch1(2:5)
        next=1
       else
        if(ucase(ch1(3:3))) then
          atom=ch1(1:2)
          ionz=ch1(3:6)
          next=1
         else
          atom=ch1(1:2)
          ionz=ch2(1:4)
          next=2
        end if
      end if
      return
      end
