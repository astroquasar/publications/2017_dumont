      subroutine vp_rdwaveval(unit1,cxname,norx,nordrs,nct, status)
*
*     WAVELENGTHS:
*     called from vp_readfs.f. Split off to make routine less unwieldy
*
      implicit none
      include 'vp_sizes.f'
*
*     IN:
*     unit1    integer  FITSIO unit for the data file
*     cxname   char     data file name
*     norx     integer  spectrum number
*     nordrs   integer  iax(2) for the data file
*     nct      integer  data array length
*     OUT:
*     status   integer  error flag (if non-zero)
      integer unit1,norx,nordrs,nct
      character*(*) cxname
      integer status
*
*     Local
      logical lundef
      character*32 cvx(2)
      character*80 comment,wname,chstr,vacstr
      character*32 errmsg
      integer unit2
      integer i,j,lclo
      integer lchend,blocksize,readwrite
      integer nax,ncref,ncw,npxstart,nvx
      integer iax(7)
      integer fpixels(2),lpixels(2)
      integer ivx(2)
!      double precision dvx(2)
      double precision coeff( maxwco, maxosf )
      double precision hvel
*     Functions
      integer lastchpos
*     Common
*     wavelength desriptors
      character*8 wcftype
      double precision helcfac
      integer noffs
      character*4 vacind
      common/c8_wcftype/wcftype,helcfac,noffs,vacind
      double precision avfac
      common/vpc_airvac/avfac
*     number wavelength coeffts used -- set default
      integer nwco
*      common/wavlen/nwco
*     wavelength coeffts for current set
      double precision wch(maxwco)
      common/vpc_wavl/wch,nwco
*     commentary:
      logical verbose
      common/vp_sysout/verbose
*
*     several options here. Search in the following order, and
*     stop when get sensible coefficients:
*     0. WAVFILE (filename) in header, get pixel-by-pixel wavelengths
*       from a fits file.
*     1. WATnnn system for linearized or non-linear coefficients
*     2. APNUMnnn system for linearized wavelengths
*     3. IF nax.eq.1 (i.e 1-d data) any of the CVAL1, CDELT1 combinations
*     4. The database REFSPEC1 file
*
      ncref=1
*     wavelength coefft type
      status=0
      call ftgkys(unit1,'WCFTYPE', wcftype,comment,status)
      if(wcftype(1:3).eq.'log') then
        wcftype='loglin'
      end if
      status=0
*     is WAVFILE in header?
      call ftgkys(unit1,'WAVFILE', wname,comment,status)
      if(status.eq.0) then
*       Open wavelength array file
        status=0
        call ftgiou(unit2,status)
        if(status.ne.0) then
          write(6,*) 'Wavelength unit number not allocated'
          goto 9000
        end if
        readwrite=0
        status=0
        call ftopen(unit2,wname,readwrite, blocksize,status)
        if(status.eq.0) then
          lchend=lastchpos(wname)
          write(6,*) 'Wavelengths from: ',wname(1:lchend)
          iax(2)=0
          call ftgknj(unit2,'NAXIS',1,2,iax,nax,status)
*         write(6,*) nax,' ',iax(1),' ',iax(2)
          if(status.ne.0) then
            call ftgerr(status,errmsg)
            write(6,*) 'WAVELENGTH SIZE ERROR:',errmsg
            goto 9000
          end if
*         data length
          ncw=iax(1)
          if(iax(2).gt.ncw.and.nax.gt.1) ncw=iax(2)
          if(ncw.gt.maxwco) then
            write(6,*) 'wavelength array length',ncw,
     :        'is greater than space available',maxwco
            write(6,*) 'wavelength table not set'
            write(6,*) '.. using channel numbers'
            wch(1)=0.0d0
            wch(2)=1.0d0
            do i=3,maxwco
              wch(i)=0.0d0
            end do
            nwco=2
*           close wavelength file:
            call ftclos(unit2, status)
            call ftfiou(unit2, status)
            goto 8902
          end if
          if(ncw.ne.nct) then
            write(6,*) 'Wavelength array length',ncw,
     :             '  is not the same as the data',nct
          end if
          if(nax.ge.2) then
*           multidimensional image
*           read the data, temporarily to the continuum space
            lundef=.false.
            status=0
            if(ncw.eq.iax(1)) then
              npxstart=(norx-1)*iax(1)+1
              call ftgpvd(unit2,0,npxstart,iax(1),0.0d0, 
     :           wch,lundef,status)
*              write(6,*) '>>> ',(wch(i),i=1,10)
             else
*             get a column
              fpixels(1)=norx
              fpixels(2)=norx
              lpixels(1)=(ncw-1)*nordrs+norx
              lpixels(2)=lpixels(1)
              call ftgsvd(unit2,0,nax,iax,fpixels,lpixels,nordrs,
     :           0.0d0, wch,lundef,status)
            end if
           else
*           single dimension image
            lundef=.false.
            status=0
*           all wavelengths are double precision
            call ftgpvd(unit2,0,1,iax(1),0.0d0, wch,lundef,status)
          end if
          if(status.ne.0) then
            call ftgerr(status,errmsg)
            write(6,*) 'Wavelength DATA ERROR:',errmsg
*           close wavelength file:
            call ftclos(unit2, status)
            call ftfiou(unit2, status)
            goto 9000
          end if
          wcftype='array'
*         close wavelength file:
          call ftclos(unit2, status)
          call ftfiou(unit2, status)
*         got wavelength table, so branch past other wavelength
*         setting options
          nwco=iax(1)
          j=0
*         If this is needed, the user gets what they deserve
*         though they may spend some time trying to sort out
*         what has happened, so warn them!
          do i=1,nwco
            if(wch(i).le.0.0d0.and.i.gt.1) then
              j=j+1
              wch(i)=wch(i-1)+1.0d0
            end if
          end do
          if(j.gt.1) then
            write(6,*) j,' bad values in wavelength table'
            write(6,*) ' please check it'
          end if
          goto 8901
         else
          write(6,*) 'FAILED TO OPEN WAVELENGTH FILE'
          write(6,*) ' .. this one needs .fits or .imh?'
        end if
      end if
*
 8902 status=0
*     Global linear (set up for simulations, could also be multispec)
      call vp_gglwcoeff(unit1,maxwco, wch,nwco,wcftype,status)
      if(status.eq.0) goto 8901
      status=0
*     WATnnn
      call vp_gwatfits(unit1,wch,maxwco,nwco,norx,wcftype,status)
      if(status.ne.0) then
        status=0
        if(verbose) write(6,*) '..trying APNUMnn'
*
*         APNUMnnn:
        call vp_gapnumfits(unit1,wch,maxwco,nwco,norx,
     :       wcftype,status)
*    
        if(status.ne.0) then
*         CTYPEi etc:
          status=0
          if(verbose) write(6,*) '..trying CTYPEnn'
          call vp_gwclinfits(unit1,wch,maxwco,nwco,norx,wcftype,
     :         ncref,status)
          if(status.ne.0) then
            status=0
*           REFSPEC1 file in database
            call vp_rdbwvfts(unit1,cxname,maxwco,nordrs,coeff,
     :           status)
            if(status.eq.0) then
*             copy the wavelength coeffts
              do i=1,maxwco
                wch(i)=coeff(i,norx)
              end do
              nwco=maxwco
              wcftype='poly'
            end if
          end if
          if(status.ne.0.and.wname(1:1).ne.' ') then
*           all possibilities now exhausted. Give up and ask:
            write(6,*) ' FAILED TO FIND WAVELENGTH INFORMATION'
            write(6,*) ' Linear wavelength coeffts? [0,1]'
            write(6,'(''> '',$)')
            read(5,'(a)') chstr
            if(chstr(1:1).eq.'!') then
*             Don't change anything
              write(6,*) 'Using old wavelengths'
             else
              call dsepvar(chstr,2,wch,ivx,cvx,nvx)
              if(wch(2).le.0.0d0) then
                wch(1)=0.0d0
                wch(2)=1.0d0
                do i=3,maxwco
                  wch(i)=0.0d0
                end do
                nwco=2
                wcftype='linear'
              end if
            end if
          end if
        end if
      end if
 8901 continue
*
*     defaults for vacuum/air and heliocentric correction:
      vacstr='  '
      vacind='vacu'
      avfac=1.0d0
      hvel=0.0d0
      helcfac=1.0d0
      if(status.eq.0) then
*       Has wavelength coeffts, so ....
*       search for heliocentric velocity correction
*       then get heliocentric velocity, trying IRAF value first
        call ftgkyd(unit1,'VHELIO', hvel,comment,status)
*       desperation is setting in....
        if(status.ne.0) then
*         try value dibbled in by hand
          status=0
          call ftgkyd(unit1,'HELVEL', hvel,comment,status)
          if (status.ne.0) then
            if (verbose) then
               write (6,*) ' no heliocentric correction'
            end if
            hvel = 0.0d0
            status = 0
           else
            hvel=-hvel
          end if
        end if
        if(verbose) write (6,*) ' heliocentric vel : ',
     :       hvel,'km/s'
        helcfac=1.0d0+hvel/2.99792458d5
*       correct coeffts to solar
        if(wcftype(1:4).eq.'cheb') then
          lclo=3
         else
          lclo=1
        end if
        if(wcftype(1:4).eq.'logl') then
*         heliocentric shift is add constant in log
          wch(1)=wch(1)+dlog10(helcfac)
         else
          do i=lclo,maxwco
            wch(i)=wch(i)*helcfac
          end do
        end if
*       since coeffts adjusted, don't want to apply correction later
        helcfac=1.0d0
*
*       and air/vacuum indicator
*       vacuum wavelengths?
        vacstr='  '
        vacind='vacu'
        avfac=1.0d0
        status=0
        call ftgkys(unit1,'VACUUM', vacstr,comment,status)
*       rfc 7/98 Bizarre header format
        if(status.eq.0.and.(vacstr(1:2).eq.'no'.or.
     :       vacstr(1:2).eq.'NO')) then
          write(6,*) ' Air wavelengths (vacuum=no)'
          vacstr='no'
          vacind='air'
         else
          vacstr='yes'
*         unless 'AIR' present in header
          status=0
          call ftgkys(unit1,'AIR', vacstr,comment,status)
          if(status.eq.0) then
            vacind='air'
            vacstr='no'
          end if
        end if
        if(vacind(1:3).eq.'air') then
          avfac=1.00028d0
*         DON'T correct the wavelength coeffts to vacuum:
          if(verbose) then
            write(6,*) nwco,' wavelength coefficients'
            write(6,*) ' wcft air ',wch
          end if
*         vacuum=.false.
*         wcftype='poly'
        end if
      end if
*
*     Is the data a subset of a larger one?
      noffs=0
      status=0
      call ftgkyj(unit1,'LTV1', noffs,comment,status)
*     note that nwoffset is -nnn if the first data channel is nnn+1
      if( status .ne. 0 ) then
        noffs=0
       else
        if(ncref.ne.1) then
          write(6,*) ' '
          write(6,*) 'Offset ',ncref,' applied from'//
     :        ' CRPIXnn in header. LTV1 ignored'
          noffs=0
        end if 
        if(noffs.ne.0) then
          write(6,*) ' Data is subset of larger one, wavelengths'
          write(6,*) '  shifted by LTV1 =', noffs, ' channels'
        end if
      end if
*
 9001 return
*     disaster bailout
 9000 status=1
      goto 9001
      end

      subroutine vp_gapnumfits(im,wcft,maxwco,nwco,norx,wcftype,ier)
*
*     get APNUMnnn linear wavelengths from FITS header
*
*     IN:
*     im	integer	unit number
*     maxwco	integer	max number wavelength coeffts
*     norx	integer	order number
*
*     OUT:
*     wcft	r*8	wavelength coeffts
*     nwco	integer	no wavelength coeffts
*     wcftype	c*8	type of wavelength function
*     ier	int	error flag (0=OK)
*
      character*8 apname,wcftype
      character*90 errmsg,apnum
      double precision wcft(maxwco)
      double precision drv(4)
      integer iv(4)
      character*20 cv(4)
      integer lastchpos
*     verbose output control:
      logical verbose
      common/vp_sysout/verbose
*
      write(apname(5:8),'(i4)') 1000+norx
      do ii=1,3
        if (apname(6:6).eq.'0') apname(6:8)=apname(7:8)
      end do
      apname(1:5)='APNUM'
*     does this exist in the header?
      lchend=lastchpos(apname)
      call ftgkys(im,apname(1:lchend), apnum,errmsg,ier)
      if ( ier .eq. 0 ) then
        if(verbose) write(6,*) apname(1:lchend),'  ',apnum
*       error catches for squiffy apnum entry in header
*       read(apnum,*,err=100) dmy1,dmy2,wcft(1),wcft(2) ! REPLACED BY
        call dsepvar(apnum,4,drv,iv,cv,nv)
        if(nv.ge.4) then
          wcft(1)=drv(3)
          wcft(2)=drv(4)
         else
          wcft(1)=0.0d0
          wcft(2)=0.0d0
          if(verbose) then
            write(6,*) apname(1:lchend),
     :          '  -- no associated wavelengths'
          end if
        end if
*       check that the wavelength coeffts are not totally silly
        if(wcft(1).eq.0.0d0.and.wcft(2).eq.0.0d0) goto 100
*       copy to array, noting that IRAF 1st coefft is for channel 1
        wcft(1)=wcft(1)-wcft(2)
        if(maxwco.gt.2) then
          do ii=3,maxwco
            wcft(ii)=0.0d0
          end do
        end if
        nwco=2
        if(wcft(1).gt.5.0d0) then
*         if first coefft large enough, then polynomial, else logs
          wcftype='poly'
         else
          if(wcft(2).gt.wcft(1).or.wcft(1).lt.0.0d0) then
*           this is unlikely to be a spectrum if dlam > lam,
*	    so set error flag 
            if(verbose) then
*	      and print a warning if in verbose mode:
              write(6,*) 'APNUM error ',cv(1)(1:8),wcft(1),wcft(2)
              write(6,*) 'will take wavelengths from CRVAL1 etc.'
            end if
            wcftype='poly'
            ier=1
           else 
            wcftype='loglin'
          end if
        end if
      end if
 5000 return
*
*     Assorted error conditions
 100  ier=1
      goto 5000
*     9000	call imemsg(ier,errmsg)
*     write(*,'(''Error: '',a90)') errmsg
      end
      subroutine vp_gwclinfits(im,wch,maxwco,nwco,norx,wcftype,ncref,
     :                       status)
*     rfc 22.3.95: double length wavelengths throughout
*     rfc 7.8.95: absence of DC-FLAG no longer gives exit with bad status
      integer status 
      double precision wch(maxwco)
      character*8 wcftype
      character*2 ch2
      character*80 comment
*     verbose output control:
      logical verbose
      common/vp_sysout/verbose
*
*     convert norx to character
      write(ch2,'(i2)') norx
*     base wavelength
      xtemp=0.0
      ncref=1
*
      if(norx.le.9) then
        status=0
        call ftgkyj(im,'CRPIX'//ch2(2:2), ncref,comment,status)
        if(status.eq.0.and.verbose) then
          write(6,*) ' CTYPEi order ',ch2,' CRPIX'//ch2(2:2),ncref
        end if
*       double precision wavelength
        call ftgkyd(im,'CRVAL'//ch2(2:2), wch(1),comment,status)
        if(verbose) then
          write(6,*) ' CTYPEi order ',ch2,' CRVAL'//ch2(2:2),xtemp
        end if
       else
        call ftgkyj(im,'CRPIX'//ch2(1:2), ncref,comment,status)
        call ftgkyd(im,'CRVAL'//ch2(1:2), wch(1),comment,status)
        if(verbose) then
          write(6,*) ' CTYPEi order ',ch2,' CRVAL'//ch2(1:2),xtemp
        end if
      end if
      if (status.ne.0) then
        call ftgerr(status,comment)
        if(verbose) write(6,*) 'Did not get CRVAL'//ch2,' ',comment
        if(norx.le.1) then
          status=0
          call ftgkyd(im,'W0', wch(1),comment,status)
        end if
      end if
      if (status.ne.0) then
*	write(6,'(a)') 'cannot get lam0, using (0,1).'
*	wch(1)=0.0d0
*	wch(2)=1.0d0
*	nwco=2
*	if(nwco.lt.maxwco) then
*	  do i=3,maxwco
*	    wch(i)=0.0d0
*	  end do
*	end if
        goto 900
      end if

*     wavelength per channel:
      xtemp=0.0
      if(norx.le.9) then
        call ftgkyd(im,'CD'//ch2(2:2)//'_1', wch(2),comment,status)
       else
        call ftgkyd(im,'CD'//ch2(1:2)//'_1', wch(2),comment,status)
      end if
      if (status.ne.0) then
        if(verbose) write(6,*) 'CD1_1 absent, trying CDELT1 then WPC'
        status=0
        if(norx.le.9) then
          call ftgkyd(im,'CDELT'//ch2(2:2), wch(2),comment,status)
          if(verbose) then
            write(6,*) ' CTYPEi order ',ch2,' CDELT'//ch2(2:2),wch(2)
          end if
         else
          call ftgkyd(im,'CDELT'//ch2(1:2), wch(2),comment,status)
          if(verbose) then
            write(6,*) ' CTYPEi order ',ch2,' CDELT'//ch2(1:2),wch(2)
          end if
        end if
      end if
      if (status.ne.0.and.norx.le.1) then
        status=0
        call ftgkyd(im,'WPC', wch(2),comment,status)
      end if 
      if (status.ne.0) then
        write(6,*) 'cannot get dlam, using 1.'
        wch(2)=1.0d0
        nwco=2
        if(nwco.lt.maxwco) then
          do i=3,maxwco
            wch(i)=0.0d0
          end do
        end if
        goto 900
      end if
*
*     shift to zero wavelength coefft
      wch(1)=wch(1)-wch(2)*dble(ncref)
      nwco=2
      if(nwco.lt.maxwco) then
        do i=3,maxwco
          wch(i)=0.0d0
        end do
      end if
*
*     wavelength type:
      ierx=0
      call ftgkyj(im,'DC-FLAG', ival,comment,ierx)
      if(verbose) write(6,*) 'DC: ',ival,ierx,wch(1),wch(2)
*     RFC 27.06.06 WCH(2) CONDITION ADDED
      if((ival.ne.1.or.ierx.ne.0).and.
     :     wch(1).gt.5.0d0) then
        wcftype='linear'
       else
        wcftype='loglin'
      end if
      if(verbose) write(6,*) 'Wavelengths are ',wcftype(1:6)
 900  return
      end
