*	gen_vpin
*	take a fort.9 linelist file from rd_gen, and turn it into
*	a 26-format fort.13 file for vpfit.
	logical linord
	character*132 inchstr, chold
	real rv(24)
	integer iv(24)
	character*24 cv(24)
	real wrlo(5000),wrhi(5000)
*
	write(6,*) 'Spectral data filename?'
	read(5,'(a)') chold
	lench=lastchpos(chold)
	chold='%% '//chold(1:lench)//'  1 '
	lench=lench+7
*
101	write(6,*) 'linelist filename? [fort.9]'
	read(5,'(a)') inchstr
	lenin=lastchpos(inchstr)
	if(inchstr(1:1).eq.' ') then
	  inchstr='fort.9'
	  lenin=6
	end if
	open(unit=9,file=inchstr(1:lenin),status='old',err=101)
102	write(6,*) 'output filename? [vpin.dat]'
	read(5,'(a)') inchstr
	lenin=lastchpos(inchstr)
	if(inchstr(1:1).eq.' ') then
	  inchstr='vpin.dat'
	  lenin=8
	end if
	open(unit=13,file=inchstr(1:lenin),status='unknown',err=102)
*
*	presets:
*	number of regions
	nreg=0
	write(6,*) 'List order as input, or reversed? (i/r) [r]'
	read(5,'(a)') inchstr
	if(inchstr(1:1).ne.'r'.and.inchstr(1:1).ne.'R') then
	  linord=.true.
	 else
	  linord=.false.
	end if
*	
*	read in the line list
	pbtymx=0.0
	sigmx=0.0
	write(6,*) 'sigmathres,pbtythres,maxext,minext'
	write(6,*) '[4.5,0.0,20,5]'
	read(5,'(a)') inchstr
	call sepvar(inchstr,4,rv,iv,cv,nv)
	if(cv(1)(1:1).ne.' ') then
	  sigthres=rv(1)
	 else
	  sigthres=4.5
	end if
	if(cv(2)(1:1).ne.' ') then
	  pthres=rv(2)
	 else
	  pthres=0.0
	end if
	if(cv(3)(1:1).ne.' ') then
	  maxext=iv(3)
	 else
	  maxext=20
	end if
	if(cv(4)(1:1).ne.' ') then
	  minext=iv(4)
	 else
	  minext=5
	end if
903	read(9,'(a)',end=902) inchstr
	call sepvar(inchstr,16,rv,iv,cv,nv)
*	look for wavelength limits
	if(cv(1)(1:4).eq.'wave') then
	  wvlot=rv(3)
	  wvhit=rv(5)
*	  x=values from table, a=adopted values
	  wvlox=wvlot
	  wvhix=wvlot
	  wvhiax=wvlot
	  wvloax=wvlot
	  goto 903
	end if	
	if(cv(1)(1:3).ne.'abs') goto 903
*	Have an absorption line
	wvlo=rv(14)
	wvhi=rv(15)
	if(wvhi.le.wvlo.or.wvlo.eq.0.0.or.nv.lt.16.or.
     :        rv(8).le.0.0) goto 903
	if(sigmx.lt.rv(8)) sigmx=rv(8)
	if(pbtymx.lt.rv(13)) pbtymx=rv(13)
	aperch=rv(9)
*	look down in wavelength to see if should use the previous one
*	for lower wavelength limit.
	if(wvlo-wvhix.lt.aperch*real(2*minext+1)) then
*	  not enough continuum between, so adopt the old values
	  wvlo=wvlox
	  wvloa=wvloax
	  goto 903
	 else
*	  adequate separation between lines
	  dchan=(wvlo-wvhix)/aperch
	  nchan=int(dchan)
	  next=(nchan-1)/2
	  next=min0(next,maxext)
	  wvloa=wvlo-aperch*real(next)
	  wvhiax=wvhix+aperch*real(next)
	  if(pbtymx.gt.pthres.and.sigmx.gt.sigthres) then
	    nreg=nreg+1
	    if(nreg.gt.5000) then
	      write(6,*) 'Exceeded region number limit of 5000'
	      goto 902
	    end if
	    if(linord) then
	      write(13,*) chold(1:lench),wvloax,wvhiax
	      write(13,*) ' '
	     else
	      wrlo(nreg)=wvloax
	      wrhi(nreg)=wvhiax
	    end if
	  end if
	  pbtymx=0.0
	  sigmx=0.0
	  wvlox=wvlo
	  wvloax=wvloa
	  wvhix=wvhi
	  goto 903
	end if
	write(6,*) 'Check program -- something is wrong!'
902	continue
*	put in something to cover last region
	if(linord) then
	  write(13,*) chold(1:lench),wvloax,wvhi
	  write(13,*) ' '
	 else
	  nreg=nreg+1
	  wrlo(nreg)=wvloax
	  wrhi(nreg)=wvhiax
*	  reverse order output
	  kreg=nreg+1
	  do j=1,nreg
	    k=kreg-j
	    write(13,*) chold(1:lench),wrlo(k),wrhi(k)
	    write(13,*) ' '
	  end do
	end if
	close(unit=9)
	close(unit=13)
	stop
	end
	include 'sepvar.f'


