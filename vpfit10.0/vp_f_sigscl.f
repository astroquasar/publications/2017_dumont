      double precision function f_sigscl(j,nf)
*     result is the sigma scale for channel j
*     of dataset nf
*
      implicit none
      include 'vp_sizes.f'
      integer j,nf
*
      integer i,nvx
      double precision temp,t
      double precision wval
*     need the individual file scale coeffts
*     Alter to maxnch files - hold for each chunk, even if
*     repeated values
      integer nscl
      double precision ssclcf,wvalsc
      common/sigsclcft/ssclcf(maxtas,maxnch),wvalsc(maxtas,maxnch),
     1                   nscl(maxnch)

      t=wval(dble(j))
      if(wvalsc(1,nf).lt.1.0d0) then
*       polynomial
        nvx=nscl(nf)
        temp=ssclcf(nvx,nf)
        if(nvx.gt.1) then
          do i=nvx-1,1,-1
            temp=temp*t+ssclcf(i,nf)
          end do
        end if
        f_sigscl=temp
*       rfc 9.1.98: replaces:
*       f_sigscl=ssclcf(1,nf)+t*(ssclcf(2,nf)+t*(ssclcf(3,nf)+
*     1        t*(ssclcf(4,nf)+t*(ssclcf(5,nf)+t*ssclcf(6,nf)))))
*
       else
*
*	interpolation table
        nvx=1
        do while(t.gt.wvalsc(nvx,nf).and.nvx.lt.nscl(nf))
          nvx=nvx+1
        end do
        if(t.gt.wvalsc(nvx,nf)) then
*         wavelength above maximum value in table, so set f_sigscl
*	  to that at max wavelength
          f_sigscl=ssclcf(nvx,nf)
         else
          if(nvx.eq.1) then
*	    wavelength less than lower limit -- use lowest wavelength 
*	    value
            f_sigscl=ssclcf(nvx,nf)
           else
*	    wavelength within range, so interpolate linearly
            f_sigscl=(ssclcf(nvx,nf)*(wvalsc(nvx,nf)-t) +
     :               ssclcf(nvx-1,nf)*(t-wvalsc(nvx-1,nf)))/
     :               (wvalsc(nvx,nf)-wvalsc(nvx-1,nf))
          end if
        end if
*
      end if
      return
      end
