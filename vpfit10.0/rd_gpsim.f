      subroutine rd_gpsim(da,de,nct)
*
*     make a spectrum using data from a velocity separated optical
*     depth list for a single line. Multiplets are supported, but 
*     infinite series (Lyman series) are truncated. Uses atom.dat
*     or equivalent for the atomic data information.
*
*     Spectrum is in velocity bins, where the middle of the array of
*     input values is put at the nominal redshift. Wavelengths are
*     specified as a table, rather than log uniform.
*     
*     READ IN:
*     inchstr	char	filename for optical depths (optional)
*	
*     OUT:
*     da	darray	spectrum
*     nct	integer	length of spectrum
*
      implicit none
      include 'vp_sizes.f'
*
      integer nct
      double precision da(*),de(*)
      character*(80) inchstr
*
      integer nv
      real rv(3)
      double precision dv(3)
      integer iv(3)
      character*24 cv(3)
      integer i,j
      integer nbin,nar,nflin,nstart,ncount
      double precision abase, vinc, zedr
      double precision taux
      double precision wvrat, wvbase, xbin
      double precision tau(1200,3)
      double precision tautot(3)
*
*     general wavelength coefficients
      integer nwco
      double precision wcf1(maxwco)
      common/vpc_wavl/wcf1,nwco
      character*8 wcftype
      character*4 vacind
      double precision helcfac
      integer*4 noffs
      common/c8_wcftype/wcftype,helcfac,noffs,vacind
*     wavelength parameter sets
      character*8 wcfty2(maxnch)
      character*4 vacin2(maxnch)
      double precision helcf2(maxnch)
      integer nwcf2(maxnch),noffs2(maxnch)
      common/vpc_wcpars2/wcfty2,helcf2,nwcf2,noffs2,vacin2
*     multidimensional wavelength coefficients...
      double precision wcfd(maxwco,maxnch)
      common/vpc_wcfd/wcfd
*
      integer nz
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :     fik(maxats),asm(maxats),nz
*     atomic mass table
      integer mmass
      character*2 lbm
      double precision amass
      common/vpc_atmass/lbm(maxats),amass(maxats),mmass
*     last file name
      integer lastord
      character*80 cflname
      logical lgflnm
      common/vpc_lastfn/cflname,lastord,lgflnm
*     
      if(nz.le.0) call vp_ewred(0)
*
*     prompt for filename
      write(6,*) 'Optical depths file?'
      read(5,'(a)') inchstr
      open(unit=11,file=inchstr,status='old',err=991)
      lastord=1
      cflname=inchstr
      lgflnm=.true.
*     Use the format provided by tt for the moment
*     With info on which lines!
*
*     Need base wavelength, redshift
      write(6,*) 'Base wavelength (rest), increment (km/s), redshift?'
      read(5,'(a)') inchstr
      call dsepvar(inchstr,3,dv,iv,cv,nv)
      if(dv(1).le.0.0d0) then
        abase=911.7633d0
       else
        abase=dv(1)
      end if
      if(dv(2).le.0.0d0) then
        vinc=0.557d0
       else
        vinc=dv(2)
      end if
      if(cv(3)(1:1).ne.' ') then
        zedr=dv(3)
       else
        zedr=3.0d0
      endif
      wvbase=(1.0d0+zedr)*abase
      wvrat=sqrt((2.99792458d5+vinc)/(2.99792458d5-vinc))
      write(6,*) 'Spectrum number? [1]'
      read(5,'(a)') inchstr
      call sepvar(inchstr,1,rv,iv,cv,nv)
      if(iv(1).gt.0) then
        write(6,*) 'Spectrum ',iv(1)
        nstart=1000*(iv(1)-1)+1
       else
        nstart=1
      endif
*     set up wavelength table
      wcftype='table'
      vacind='vacu'
      helcfac=1.0d0
      noffs=0
      wcfty2(1)=wcftype
      helcf2(1)=1.0d0
      nwcf2(1)=maxwco
      noffs2(1)=0
      vacin2(1)=vacind
      nwco=maxwco
      nct=nwco
      do j=1,maxwco
        wvbase=wvbase*wvrat
        wcf1(j)=wvbase
        wcfd(j,1)=wvbase
        da(j)=1.0d0
        de(j)=0.00000001d0
      end do
*     Optical depths per unit frequency just in ratio of 
*     oscillator strengths if the velocity points are far enough apart --
*     so ignore differential natural line broadening.
      nar=0
      ncount=0
      do j=1,3
        tautot(j)=0.0d0
      end do
 901  read(11,'(a)',err=902) inchstr
      if(inchstr(1:1).eq.'!') goto 901
      call sepvar(inchstr,3,rv,iv,cv,nv)
      if(cv(1)(1:3).eq.'Set') goto 901
      ncount=ncount+1
      if(ncount.lt.nstart) goto 901
      nar=nar+1
      do j=1,nv
        tau(nar,j)=rv(j)
        tautot(j)=tautot(j)+rv(j)
      end do
      if(nar.gt.1000) goto 902
      goto 901
*     got some data, assume that ref is channel 500
*     Hydrogen
 902  close(unit=11)
      nar=nar-1
      write(6,*) 'Optical depth file read OK',nar
*
*     convert summed taus to total column densities
*     the 1.576 is pi e^2/mc, the 
      tautot(1)=log10(tautot(1)*vinc/(0.4164*1215.7d-13))+1.576d0
      tautot(2)=log10(tautot(2)*vinc/(0.1908*1548.2d-13))+1.576d0
      tautot(3)=log10(tautot(3)*vinc/(0.1329*1031.7d-13))+1.576d0
      write(6,*) 'HI',tautot(1),'  CIV',tautot(2),'  OVI',
     :                  tautot(3)
      nflin=-500
      do i=1,nar-1
        nflin=i-499
        do j=1,nz
          if(lbz(j).eq.'H '.and.lzz(j).eq.'I   ') then
*           get nearest wavelength bin
            xbin=alm(j)/abase
            xbin=log(xbin)/log(wvrat)+dble(nflin)
*	    if(nflin.eq.0) write(6,*) xbin,alm(j)
            nbin=int(xbin)
            if(nbin.gt.0.and.nbin.le.nct) then
*	      interpolate
              taux=(dble(nbin+1)-xbin)*tau(i,1)+
     :              (xbin-dble(nbin))*tau(i+1,1)
              taux=taux*fik(j)/0.4164d0
              if(taux.lt.50.0d0) then
                da(nbin)=da(nbin)*exp(-taux)
               else
                da(nbin)=0.0d0
              end if
            end if
          end if
          if(lbz(j).eq.'C '.and.lzz(j).eq.'IV  ') then
*	    get nearest wavelength bin
            xbin=alm(j)/abase
            xbin=log(xbin)/log(wvrat)+dble(nflin)
*	    if(nflin.eq.0) write(6,*) xbin,alm(j)
            nbin=int(xbin)
            if(nbin.gt.0.and.nbin.le.nct) then
*	      interpolate
              taux=(dble(nbin+1)-xbin)*tau(i,2)+
     :              (xbin-dble(nbin))*tau(i+1,2)
              taux=taux*fik(j)/0.1908d0
              if(taux.lt.50.0d0) then
                da(nbin)=da(nbin)*exp(-taux)
               else
                da(nbin)=0.0d0
              end if
            end if
          end if
          if(lbz(j).eq.'O '.and.lzz(j).eq.'VI  ') then
*	    get nearest wavelength bin
            xbin=alm(j)/abase
            xbin=log(xbin)/log(wvrat)+dble(nflin)
*	    if(nflin.eq.0) write(6,*) xbin,alm(j)
            nbin=int(xbin)
            if(nbin.gt.0.and.nbin.le.nct) then
*	      interpolate
              taux=(dble(nbin+1)-xbin)*tau(i,3)+
     :              (xbin-dble(nbin))*tau(i+1,3)
              taux=taux*fik(j)/0.1329d0
              if(taux.lt.50.0d0) then
                da(nbin)=da(nbin)*exp(-taux)
               else
                da(nbin)=0.0d0
              end if
            end if
          end if
        end do
      end do
 900  continue
      return
*
*     assorted failure messages
 991  write(6,*) 'File not found'
      goto 900
      end
