*     program to take a set of fits from a forest series, and based
*     on HI column density thresholds, strip out unnecessary high
*     order regions so that maximum sensitivity in the Ly-a region
*     is achieved.
*
*     designed for use as a sequence rdgen (abs) -> autovpin -> vpfit
*     -> lyregset -> vpfit
*
      implicit none
      include 'vp_sizes.f'
*
      integer i,j,jb,k,ki,jt,kt
      integer istat,ind,lch,ncx,nxsys
      integer lrj,nfar,nrj,npa
      double precision colx,dwvx,wvxx,xwv,zp1,dxerr,wdiff,twdiff
      double precision pzed,pcol
      double precision wv(24),fk(24),colthres(24)
      integer nreg(24)
*     function declarations
      integer lastchpos
*
*     parameter variables
      character*2 element(maxnio)
      character*4 ionz(maxnio)
      character*2 ipind(maxnpa)
      double precision parm(maxnpa)
*     file and region variables
      character*64 filename(maxnch)
      integer norder(maxnch)
      double precision wvstart(maxnch),wvend(maxnch)
*     character handling
      integer nv
      character*256 inchstr
      double precision dv(24)
      real rv(24)
      integer iv(24)
      character*64 cv(24)
      common/lyc_charh/inchstr,dv,rv,iv,cv,nv
*
      call vp_charlims
      dxerr=0.0d0
*
 901  write(6,*) 'Filename for preliminary fits [fort.26]'
      read(5,'(a)') inchstr
      lch=lastchpos(inchstr)
      if(inchstr(1:1).eq.' ') then
        inchstr='fort.26'
        lch=7
      end if
*     reads to unit 13, since that is the standard vpfit input
      open(unit=13,file=inchstr(1:lch),status='old',err=901)
 902  write(6,*) 'Output file? [fort.17]'
      read(5,'(a)') inchstr
      lch=lastchpos(inchstr)
      if(inchstr(1:1).eq.' ') then
        inchstr='fort.17'
        lch=7
      end if
      open(unit=17,file=inchstr(1:lch),status='unknown',err=902)
*
*     hardwire in the Lyman series
      wv(1)=1215.6701d0
      fk(1)=0.416400d0
      wv(2)=1025.7223d0
      fk(2)=0.079120d0
      wv(3)= 972.5368d0
      fk(3)=0.029000d0
      wv(4)= 949.7431d0
      fk(4)=0.013940d0
      wv(5)= 937.8035d0
      fk(5)=0.007799d0
      wv(6)= 930.7483d0
      fk(6)=0.004814d0
      wv(7)= 926.2257d0
      fk(7)=0.003183d0
      wv(8)= 923.1504d0
      fk(8)=0.002216d0
      wv(9)= 920.9631d0
      fk(9)=0.001605d0
      wv(10)=919.3514d0
      fk(10)=0.00120d0
      wv(11)=918.1294d0
      fk(11)=0.000921d0
      wv(12)=917.1806d0
      fk(12)=0.0007226d0
      wv(13)=916.429d0
      fk(13)=0.000577d0
      wv(14)=915.824d0
      fk(14)=0.000469d0
      wv(15)=915.329d0
      fk(15)=0.000386d0
      wv(16)=914.919d0
      fk(16)=0.000321d0
      wv(17)=914.576d0
      fk(17)=0.000270d0
      wv(18)=914.286d0
      fk(18)=0.000230d0
      wv(19)=914.039d0
      fk(19)=0.000197d0
      wv(20)=913.826d0
      fk(20)=0.000170d0
      wv(21)=913.641d0
      fk(21)=0.000148d0
      wv(22)=913.480d0
      fk(22)=0.000129d0
      wv(23)=913.339d0
      fk(23)=0.000114d0
      wv(24)=913.215d0
      fk(24)=0.000101d0
*     set thresholds by oscillator strength
      colthres(1)=8.0d0
      colthres(2)=14.25d0
      do j=3,24
        colthres(j)=colthres(2)+log10(fk(2)/fk(j))
      end do
      write(6,*) 'HI log col thresholds for using Ly-beta, gamma ....'
      write(6,'(8f9.2,a)') (colthres(j),j=2,9),' ....'
      read(5,'(a)') inchstr
      call sepvar(inchstr,24,dv,iv,cv,nv)
      if(rv(1).gt.0.0) then
        colthres(2)=dv(1)
        j=2
*       put in specified values
        do while(dv(j).gt.0.0d0.and.j.lt.24)
          colthres(j+1)=dv(j)
          j=j+1
        end do
*       and propagate the last one using oscillator strengths
        if(j.lt.23) then
          do i=j,23
            colthres(i+1)=colthres(j)+log10(fk(j)/fk(i))
          end do
        end if
        write(6,*) 'Using logN(HI) thresholds'
        write(6,'(8f9.2,a)') (colthres(j),j=2,9),' ....'
      end if
*     data read
      ncx=0
      istat=-1
      do while (istat.le.0)
        call ly_f13read(istat, filename,norder,wvstart,wvend,
     :           nrj,element,ionz,parm,ipind,npa)
*       find longest wavelength region
        lrj=1
        if(nrj.gt.1) then
          do j=2,nrj
            if(wvstart(j).gt.wvstart(lrj)) then
              lrj=j
            end if
          end do
        end if
        ncx=ncx+1
*       determine highest column density of Ly-a in this region
        colx=0.0d0
        do j=1,npa
          jb=(j-1)*3
          xwv=1215.67d0*(1.0d0+parm(jb+1))
          if(xwv.ge.wvstart(lrj).and.xwv.le.wvend(lrj).and.
     :       parm(jb+3).gt.colx) then
            colx=parm(jb+3)
          end if
        end do
*       so how far down the series should you go?
        nfar=1
        do i=2,24
          if(colx.gt.colthres(i)) then
            nfar=i
          end if
        end do
*       which region is associated with which line?
        nreg(1)=lrj
        do i=2,24
          nreg(i)=0
        end do
        zp1=0.5d0*(wvstart(lrj)+wvend(lrj))/1215.67d0
        do j=1,nrj
          ind=0
          i=2
          do while(i.lt.24.and.ind.eq.0)
            wvxx=zp1*wv(i)
            if(wvxx.ge.wvstart(j).and.wvxx.le.wvend(j)) then
              nreg(i)=j
              ind=1
             else
              i=i+1
            end if
          end do
        end do
*       nreg(1) is Ly-a, (2) Ly-b region, (3) gamma and so on
*       the last useful one is min(nfar,nrj)
        if(nfar.gt.nrj) then
          nfar=nrj
        end if
*       write out the relevant region parameters to file
        write(6,*) 'Using',nfar,' regions'
        do i=1,nfar
          k=nreg(i)
          lch=lastchpos(filename(k))
          write(6,'(a2,2x,a,2x,i4,1x,f10.3,1x,f10.3)')
     :         '%%',filename(k)(1:lch),norder(k),wvstart(k),
     :         wvend(k)
          write(17,'(a2,2x,a,2x,i4,1x,f10.3,1x,f10.3)')
     :         '%%',filename(k)(1:lch),norder(k),wvstart(k),
     :         wvend(k)
        end do
*       now write out first guess for lines where Ly-a fall in any
*       of the accepted regions
        if(npa.gt.0) then
          nxsys=0
          do k=1,npa
            j=(k-1)*3+1
            dwvx=(parm(j)+1.0d0)*1215.67d0
            ind=0
            do i=1,nfar
              ki=nreg(i)
              if(dwvx.ge.wvstart(ki).and.dwvx.le.wvend(ki)) then
                ind=1
              end if
            end do
            if(ind.eq.1) then
*             accepted system - write it out
              nxsys=nxsys+1
              write(17,
     :   '(1x,a2,a4,f10.6,1x,f10.6,1x,f8.2,1x,f8.2,1x,f8.3,1x,f8.3)') 
     :   element(k),ionz(k),parm(j),dxerr,parm(j+1),dxerr,parm(j+2),
     :   dxerr
          write(6,'(1x,a2,a4,f10.6,a3,f8.2,a3,f8.3,a3)') 
     :   element(k),ionz(k),parm(j),' 0 ',parm(j+1),' 0 ',parm(j+2),
     :   ' 0 '
             else
*             system not accepted, but could be a spurious one close
*             to highest redshift region, in which case need to replace
*             whatever the responsible higher order line is with Ly-a
*             with suitable rescaling of column density
*             start with wavelength difference from longest region
              wdiff=abs(dwvx-wvstart(lrj))
              ind=0
              do i=1,nfar
                if(i.ne.lrj) then
                  twdiff=abs(dwvx-wvend(nreg(i)))
                  if(twdiff.lt.wdiff) then
                    ind=i
                  end if
                end if
              end do
              if(ind.le.0) then
*               Ly-a nearest to, but not in, longest region
*               so replace any Lyman series lines which fall
*               in any region with rescaled column Ly-a
                write(6,*) 'Replacing z=',parm(j)
                do jt=2,24
                  twdiff=dwvx*wv(jt)/wv(1)
                  do kt=1,nfar
                    if(nreg(kt).ne.lrj.and.twdiff.ge.wvstart(nreg(kt))
     :                 .and.twdiff.le.wvend(nreg(kt))) then
                      pzed=twdiff/wv(1)-1.0d0
                      pcol=parm(j+2)-log(fk(jt)/fk(1))
                      write(17,'(1x,a2,a4,f10.6,a3,f8.2,a3,f8.3,a3)') 
     :                   element(k),ionz(k),pzed,' 0 ',parm(j+1),
     :                   ' 0 ',pcol,' 0'
                      write(6,'(1x,a2,a4,f10.6,a3,f8.2,a3,f8.3,a3)') 
     :                   element(k),ionz(k),pzed,' 0 ',parm(j+1),
     :                   ' 0 ',pcol,' 0'
                    end if
                  end do
                end do
              end if
            end if
          end do
          if(nxsys.le.0) then
*           put some random thing in the middle of the longest region
            parm(1)=0.5d0*(wvstart(lrj)+wvend(lrj))/1215.67d0
            parm(2)=30.0d0
            parm(3)=12.1d0
            write(17,'(1x,a2,a4,f10.6,a3,f8.2,a3,f8.3,a3)') 
     :      'H ','I   ',parm(1),' 0 ',parm(2),' 0 ',parm(3),
     :       ' 0 '
          end if
         else
*         put some random thing in the middle of the longest region
          parm(1)=0.5d0*(wvstart(lrj)+wvend(lrj))/1215.67d0
          parm(2)=30.0d0
          parm(3)=12.1d0
          write(17,'(1x,a2,a4,f10.6,a3,f8.2,a3,f8.3,a3)') 
     :    'H ','I   ',parm(1),' 0 ',parm(2),' 0 ',parm(3),
     :   ' 0 '
        end if
      end do
      stop
      end
      subroutine ly_f13read(istat, filename,norder,wvstart,wvend,
     :           nrj,element,ionz,parm,ipind,npa)
*
*     istat=-1 signals first file read
*
      character*64 filename(*)
      integer norder(*)
      double precision wvstart(*),wvend(*)
      character*2 element(*)
      character*4 ionz(*)
      double precision parm(*)
      character*2 ipind(*)
*
*     local
      character*2 atom
      character*4 ion
      double precision dvx(1)
      integer ivx(1)
      character*64 cvx(1)
      character*64 chvx
      character*1 ch1,ch2
*     character handling
      character*256 inchstr
      double precision dv(24)
      real rv(24)
      integer iv(24)
      character*64 cv(24)
      common/lyc_charh/inchstr,dv,rv,iv,cv,nv
*    
*     internal data order is z,b,logN
      if(istat.lt.0) then
 901    read(13,'(a)',end=905) inchstr
        call dsepvar(inchstr,24,dv,iv,cv,nv)
        if(cv(1)(1:2).ne.'%%') goto 901
      end if
*     character string is a %% one - copy to the holding arrays
      nrj=1
 903  filename(nrj)=cv(2)
      norder(nrj)=iv(3)
      wvstart(nrj)=dv(4)
      wvend(nrj)=dv(5)
 902  read(13,'(a)',end=905) inchstr
      call dsepvar(inchstr,24,dv,iv,cv,nv)
*     skip comment lines
      if(cv(1)(1:1).eq.'!') goto 902
      if(cv(1)(1:2).eq.'%%') then
        nrj=nrj+1
        goto 903
      end if
*     reaches here if it is atomic parameter set (which may be blank)
      npa=0
      if(cv(1)(1:1).eq.' ') then
        npa=0
 904    read(13,'(a)',end=905) inchstr
        call dsepvar(inchstr,24,dv,iv,cv,nv)
*       read through to next region set
        if(cv(1)(1:2).ne.'%%') goto 904
       else
*       have some useful data
 907    npa=npa+1
        npam=3*npa
        call vp_ationsep(cv,dv,iv,24,1,atom,ion)
        element(npa)=atom
        ionz(npa)=ion
*       now need the values, and there may be tied/fixed characters
        nvx=min(nv,6)
        if(nvx/2*2.ne.nvx) nvx=nvx+1
        do i=2,nvx,2
          lvx=lastchpos(cv(i))
*         treat upper limits as values
          if(cv(i)(1:1).eq.'<') then
            cv(i)=cv(i)(2:lvx)//' '
            lvx=lvx-1
*           need to insert the actual number
            call dsepvar(cv(i),1,dvx,ivx,cvx,nvx)
            dv(i)=dvx(1)
            iv(i)=ivx(1)
          end if
          if(lvx.ge.2) then
*           split into parameter and flag (up to two characters)
            ch1=cv(i)(lvx:lvx)
            if(ch1.eq.'.'.or.ch1.eq.'0'.or.ch1.eq.'1'.or.
     :         ch1.eq.'2'.or.ch1.eq.'3'.or.ch1.eq.'4'.or.
     :         ch1.eq.'5'.or.ch1.eq.'6'.or.ch1.eq.'7'.or.
     :         ch1.eq.'8'.or.ch1.eq.'9') then
*             whole thing to be treated as a number
              parm(npam-3+i/2)=dv(i)
             else
              ch2=cv(i)(lvx-1:lvx-1)
              if(ch2.eq.'.'.or.ch2.eq.'0'.or.ch2.eq.'1'.or.
     :           ch2.eq.'2'.or.ch2.eq.'3'.or.ch2.eq.'4'.or.
     :           ch2.eq.'5'.or.ch2.eq.'6'.or.ch2.eq.'7'.or.
     :           ch2.eq.'8'.or.ch2.eq.'9') then
*               strip off a single character and use as a number
                chvx=cv(i)(1:lvx-1)
                ipind(npam-3+i/2)=cv(i)(lvx:lvx)//' '
               else
                if(lvx.eq.2) then
                  chvx=' '
                 else
                  chvx=cv(i)(1:lvx-2)
                end if
                ipind(npam-3+i/2)=cv(i)(lvx-1:lvx)                 
              end if
              call dsepvar(chvx,1,dvx,ivx,cvx,nvx)
              parm(npam-3+i/2)=dvx(1)
            end if
           else
*           only one character, so call it a number!
            parm(npam-3+i/2)=dv(i)
            ipind(npam-3+i/2)=' '
          end if
        end do
*       read in the next line
 906    read(13,'(a)',end=905) inchstr
        call dsepvar(inchstr,24,dv,iv,cv,nv)
        if(cv(1)(1:1).eq.'!') goto 906
        if(cv(1)(1:2).ne.'%%') goto 907
      end if
      istat=0
 908  return
 905  istat=1
      goto 908
      end

          
