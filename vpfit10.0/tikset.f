      subroutine tikset(lby,lzy,zzc,nsys)
*
      implicit none
      include 'vp_sizes.f'
*
      character*2 lby
      character*4 lzy
      double precision zzc
      integer nsys
*
*     set up marker positions
      integer i
      double precision wx,x
*     FUNCTIONS:
      double precision wcor
*     Common
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm
      integer nz 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),nz
      integer nred,ntick
      double precision tikmk,rstwav
      common/vpc_ticks/tikmk(maxnio,maxnio+1),ntick(maxnio+1),
     :             nred,rstwav(maxnio,maxnio+1)
c     read atomic data if not already done
      if(nz.le.0) call vp_ewred(0)
      nred=nsys
c     disaster bailout
      if(nred.le.0) return
      if(nred.gt.maxnio+1) then
        write(6,*) ' More than ',maxnio+1,' systems'
        nred=maxnio+1
      end if
c     set wavelengths for tick marks corresponding to lines
      ntick(nred)=0
      do i=1,nz
        if(lbz(i).eq.lby.and.lzz(i).eq.lzy.and.
     1                ntick(nred).lt.maxnio) then
          wx=(zzc+1.0d0)*alm(i)
          x=wcor(wx)
          wx=wx/x           ! air wavelength
          ntick(nred)=ntick(nred)+1
          tikmk(ntick(nred),nred)=wx
          rstwav(ntick(nred),nred)=alm(i)
        end if
      end do
      return
      end
