
      subroutine vp_pgcurs( xpg, ypg, ch )

      implicit none
      integer istat
      real xpg, ypg, xrefpg, yrefpg
      character*(*) ch
*
*     10 special features:
*     (10,1) = PGPLOT mode for PGBAND (0 is default)
      integer ipgatt(10,9)
      common/vpc_pgattrib/ipgatt
*
*     anchor point is last position
      xrefpg=xpg
      yrefpg=ypg
*     X-windows cursor: L=A, mid= X, R=D:
      istat= pgband(ipgatt(10,1),1,xrefpg,yrefpg, xpg,ypg,ch)
*
      return
      end
