      subroutine vp_ucprinit(elm,ionz,parm,ipind,np)
*
*     parameter printout for each iteration in ucoptv
*     [statistics for fit not printed here though]
*
      implicit none
      include 'vp_sizes.f'
*
*     subroutine arguments
      character*2 elm(maxnio)
      character*4 ionz(maxnio)
      character*2 ipind(maxnpa)
      double precision parm(maxnpa)
      integer np
*
*     Local
      character*2 chopt
      integer nset
      integer i,jbase,jb,jc,jz,j4
      integer jbx,ierst,iop
      double precision colpt,vtbpt,temppt,xtpt,bvparm
*     
*     Functions
      logical lcase,nocgt
      double precision vpf_bvalsp
*
*     Common
*     parameter placement variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     log or linear variable indicator,1 for logN, 0 for linear
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
*     lastch tied for normal operation, later are special
*     depending on the variable involved
      character*2 lastch,firstch
      common/vpc_ssetup/lastch,firstch
*     turb/temperature special cross-reference list
      integer mtxref(maxnpa)
      common/vpc_txref/mtxref
*     atomic mass and related
      double precision vturb,atmass,temper,fixedbth
      common/vpc_ucopder/vturb(maxnio),atmass(maxnio),
     :         temper(maxnio),fixedbth(maxnio)
*     miscellaneous control variable array
      character*4 chcv(10)
      common/vpc_chcv/chcv
*     chunk list special variables
      integer linchk( maxnio )  ! ties lines to chunks
      common/vpc_linchk/linchk
      character*4 chlnk(maxnio)
      common/vpc_chlnchk/chlnk
*
      integer nopchan,nopchanh,nmonitor
      integer lt(3)             ! output channels for printing
      common/vpc_nopchan/lt,nopchan,nopchanh,nmonitor
*     temperature units
      double precision thunit,thmax
      common/vpc_thscale/thunit,thmax
*     rest wavelength stuff. Action may depend on lqmucf
      logical lqmucf,lchvsqmu
      double precision qmucf
      double precision qscale
      common/vpc_qmucf/qmucf(maxats),qscale,lqmucf,lchvsqmu
*     ucoptv format specifier
      character*132 chformuc,chformer
      character*164 chformet
      common/vpc_ucformat/chformuc,chformer,chformet
*     mimformat for da/a, I/O units
      double precision unitamim
      common/vpc_scalemim/unitamim
*
*     sort out format first, of not already done
      if(chformuc(1:1).ne.'(') then
        if(indvar.eq.1) then
*         log column densities
          if(noppsys.le.3) then
*           N,b,z
            if(chcv(9).ne.'c26f') then
*             original o/p format
              chformuc='(3x,a2,a4,1x,f10.5,a2,1x,f11.7,a2,1x,'//
     :            'f9.4,a2,1x,f9.2,1x,1pe10.2,1x,a2,1x,a2,i4)'
              chformer='(3x,a2,a4,1x,f10.5,a2,1x,f11.7,a2,1x,'//
     :            'f9.4,a2,1x,f9.2,1x,1pe10.2,4x,a2,i4)'
             else
*             fort.26 style, high precision o/p
              chformuc='(1x,a2,a4,1x,f10.6,1a,a,f9.2,a,a,'//
     :            'f7.3,a,a,a,i3)'
              chformer='(1x,a2,a4,1x,f14.10,a2,1x,f13.10,1x,'//
     :           'f11.5,a2,1x,f11.5,1x,f10.6,a2,f10.6)'
            end if
           else
*           N,b,z,+extra
            if(chcv(9).ne.'c26f') then
*             extended original o/p format
              if(unitamim.gt.1.0d-3) then
                chformuc='(3x,a2,a4,1x,f10.5,a2,1x,f11.7,a2,1x,'//
     :            'f9.4,a2,1x,1pe10.2,a2,1x,'//
     :            '0pf9.2,1x,1pe10.2,1x,a2,1x,a2,i4)'
                chformer='(3x,a2,a4,1x,f10.5,a2,1x,f11.7,a2,1x,'//
     :            'f9.4,a2,1x,1pe10.2,a2,1x,'//
     :            '0pf9.2,1x,1pe10.2,4x,a2,i4)'
               else
*               fourth variable rescaled I/O
                chformuc='(3x,a2,a4,1x,f10.5,a2,1x,f11.7,a2,1x,'//
     :            'f9.4,a2,1x,f9.3,a2,1x,'//
     :            'f9.2,1x,1pe10.2,1x,a2,1x,a2,i4)'
                chformer='(3x,a2,a4,1x,f10.5,a2,1x,f11.7,a2,1x,'//
     :            'f9.4,a2,1x,f9.3,a2,1x,'//
     :            'f9.2,1x,1pe10.2,4x,a2,i4)'
              end if
             else
*             extended fort.26 style, high precision
              chformuc='(1x,a2,a4,1x,f10.6,a2,1x,a,1x,f8.2,a2,'//
     :               '1x,a,1x,f7.3,a2,a,1x,1pe10.2,a2,a,i3)'
              chformer='(1x,a2,a4,1x,f14.10,a2,1x,f13.10,1x,f11.5,'//
     :           'a2,1x,f10.5,1x,f10.6,a2,f10.6,1x,1pe12.4,a2,e12.4)'
            end if
          end if
         else
*         linear column densities
          if(noppsys.le.3) then
*           N,b,z
            if(chcv(9).ne.'c26f') then
*             original o/p format (N,b,z)
              chformuc='(3x,a2,a4,1x,1pe13.5,a2,1x,0pf11.7,a2,1x,'//
     :            'f9.4,a2,1x,f9.2,1x,1pe10.2,1x,a2,1x,a2,i4)'
              chformer='(3x,a2,a4,1x,1pe13.5,a2,1x,0pf11.7,a2,1x,'//
     :            'f9.4,a2,1x,f9.2,1x,1pe10.2,4x,a2,i4)'
             else
*             fort.26 style [needs checking]
              chformuc='(1x,a2,a4,1x,f10.6,1a,f10.6,1a,f9.2,1a,'//
     :            'f9.2,1x,1pe10.2,1x,a2,4x,a2,i3)'
              chformer='(1x,a2,a4,1x,f10.4,1a,f10.6,1a,f9.2,1a,'//
     :            'f9.2,1x,1pe10.2,1x,a2,4x,a2,i3)'
            end if
           else
*           N,b,z,+extra
            if(chcv(9).ne.'c26f') then
*             extended original o/p format
              chformuc='(3x,a2,a4,1x,1pe13.5,a2,1x,0pf11.7,a2,1x,'//
     :            'f9.4,a2,1x,1pe10.2,a2,1x,'//
     :            '0pf9.2,1x,1pe10.2,1x,a2,1x,a2,i4)'
              chformer='(3x,a2,a4,1x,1pe13.5,a2,1x,0pf11.7,a2,1x,'//
     :            'f9.4,a2,1x,1pe10.2,a2,1x,'//
     :            '0pf9.2,1x,1pe10.2,4x,a2,i4)'
             else
*             extended fort.26 style [check]
              chformuc='(1x,a2,a4,1x,f10.6,a2,1x,a,1x,f8.2,a2,'//
     :               '1x,a,1x,f7.3,a2,a,1x,1pe10.2,a2,a)'
              chformer='(1x,a2,a4,1x,f10.6,a2,1x,f9.6,1x,f8.2,a2,'//
     :               '1x,f7.2,1x,f7.3,a2,f7.3,1x,1pe10.2,a2,e10.2)'
            end if
          end if
        end if
      end if
*
*     prepare the parameters, and print
      nset=np/noppsys
      do i=1,nset
        jbase=noppsys*(i-1)
*       column densities
        jc=jbase+nppcol
        if(elm(i).ne.'<>'.and.elm(i).ne.'__'.and.elm(i).ne.'>>') then
*         rescale column densities into ptemcol
          if(indvar.eq.1) then
            colpt=parm(jc)/scalelog
           else
            colpt=parm(jc)/scalefac
          end if
         else
          colpt=parm(jc)
        end if
*       Doppler parameters
        jb=jbase+nppbval
*        bvparm=parm(jb)
        if(lcase(ipind(jb)(1:1))) then
*         lower case tied velocity
          vtbpt=vturb(i)
          temppt=temper(i)
          if(nocgt(ipind(jb)(1:1),lastch)) then
*           special tied velocity - turb & temperature used as base parameters
*           find cross-referenced dataset
            jbx=mtxref(jb)
            if(jbx.gt.jb) then
              vtbpt=parm(jb)
              temppt=parm(jbx)*thunit
             else
              vtbpt=parm(jbx)
              temppt=parm(jb)*thunit
            end if
            bvparm=vpf_bvalsp(i,parm,ipind)
           else
            bvparm=vpf_bvalsp(i,parm,ipind)
*           actual values turb, temp used, if wanted.
*            if(vtbpt.le.0.0d0) then
*              if(temppt.le.0.0d0) then
*               all thermal, so estimate temperature
*                xtemppt=60.135795d0*atmass(i)*bvparm**2
*               else
*               temperature given, so estimate turbulent component
*                xvtbpt=sqrt(bvparm**2-temppt/60.135795d0*atmass(i))
*              end if
*             else
*             Fixed turbulent component, ALWAYS estimate temperature
*              xtemppt=60.135795d0*atmass(i)*(bvparm**2-vtbpt**2)
*            end if
          end if
         else
*         either no turb/temperature tied values, or refer to base variable
          vtbpt=0.0d0
          temppt=0.0d0
          bvparm=vpf_bvalsp(i,parm,ipind)
        end if
*       chunk link stuff
        if(chlnk(i)(1:1).eq.'A'.or.chlnk(i)(1:1).eq.'B'.or.
     :        chlnk(i)(1:1).eq.'C'.or.chlnk(i)(1:1).eq.'D') then
          chopt=chlnk(i)(1:2)
         else
          ierst=0
          write(chopt,'(i2)',iostat=ierst) linchk(i)
          if(ierst.ne.0) chopt=' 0'
        end if
*       redshift
        jz=jbase+nppzed
*       printout section - depends on requested format & noppsys
        if(noppsys.le.3) then
*         standard printout
          if(chcv(9).ne.'c26f') then
*           old fort.13 ordering
            do iop=1,nopchan
              write(lt(iop),chformuc) elm(i),ionz(i),
     1              colpt,ipind(jc),parm(jz),ipind(jz),bvparm,
     2              ipind(jb),vtbpt,temppt,chopt,'! ',i 
            end do
           else
*           fort.26 ordering
            do iop=1,nopchan
              write(lt(iop),chformuc) elm(i),ionz(i),
     1          parm(jz),ipind(jz),' 0 ',bvparm,ipind(jb),' 0 ',
     2          colpt,ipind(jc),' 0  !',i
            end do
          end if
         else
*         noppsys>3: some extra parameters may need rescaling
          j4=jbase+4
          if(ipind(j4)(1:1).eq.'q'.or.ipind(j4)(1:1).eq.'Q'.or.
     :       ipind(j4)(1:1).eq.'m'.or.ipind(j4)(1:1).eq.'M') then
            xtpt=qscale*parm(j4)
            if(unitamim.le.1.0d-3) then
              xtpt=xtpt/unitamim
            end if
           else
            xtpt=parm(j4)
          end if
*         print out
          if(chcv(9).ne.'c26f') then
*           old fort.13 ordering
            do iop=1,nopchan
              write(lt(iop),chformuc) elm(i),ionz(i),
     1           colpt,ipind(jc),parm(jz),ipind(jz),bvparm,
     2           ipind(jb),xtpt,ipind(j4),
     3            vtbpt,temppt,chopt,'! ',i 
            end do
           else
*           fort.26 ordering
            do iop=1,nopchan
              write(lt(iop),chformuc) elm(i),ionz(i),
     :              parm(jz),ipind(jz),' 0 ',
     :              bvparm,ipind(jb),' 0 ',
     :              colpt,ipind(jc),' 0',xtpt,ipind(j4),' 0  !',i
            end do
          end if          
        end if
      end do
*     blank line
      do iop=1,nopchan
        write(lt(iop),'(a)') ' '
      end do
      return
      end
