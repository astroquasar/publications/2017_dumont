      subroutine vp_linlim(ngp,nrun,lguess)
*
*     sets lower,upper limits for a line, requesting the line number
*     as set by tikset.
*
      implicit none
      include 'vp_sizes.f'
*
      integer ngp,nrun
      logical lguess
*
*     Local
      integer ii,nch,nrang,nsys,nw
      character*2 lby
      character*4 lzy
      integer nv
      character*60 chstr,chtemp
      character*10 cv(4)
      double precision drv(4)
      integer iv(4)
      double precision delt,deltw
      double precision wrh,wrest,wtry,xtemp
      double precision zzzc
      double precision ced
*     function declarations
      logical lcase
*     Common
*     chunk limits
      integer ichstrt(maxnch),ichend(maxnch)
      common/jkw4/ichstrt,ichend
*     guess at dimensions meaning
      integer ntick,nred
      double precision tikmk,rstwav
      common/ticks/tikmk(maxnio,maxnio+1),ntick(maxnio+1),
     :               nred,rstwav(maxnio,maxnio+1)
*     wavelength coefficients
      integer nwco
      double precision ac(maxwco)
      common/vpc_wavl/ac,nwco
*
      integer nst,ncn,lc1,lc2
      real fampg
      common/ppam/nst,ncn,lc1,lc2,fampg
*
*     best guess at plot range is the last one used:
      nrang=(ncn-nst+1)/2
      if(nrang.le.5.or.nrang.gt.ngp/2) then
        nrang = 75
      end if
*
 2    if(lguess) then
*       internal guesses available, so use them
        chstr='  '
       else
        write(6,*) ' Line, system number:'
        write(6,'(''> '',$)')
        read(5,'(a)',end=4,err=5) chstr
      end if
      call dsepvar(chstr,4,drv,iv,cv,nv)
      if((cv(1)(1:1).eq.'*'.or.cv(1)(1:1).eq.' ').and.lguess) then
*	set low and high channels to be the chunk limits as a default
        nst=max0(ichstrt(nrun)-20,1)
        lc1=nst
        ncn=min0(ichend(nrun)+20,ngp)
        ncn=max0(nst,ncn)
        lc2=ncn
        goto 4
      end if
*     Some steering characters in string, so:
      nw=iv(1)
      nsys=iv(2)
*     exit if first parameter is blank, or if first character is
*     lower case (which means you've entered a plot command)
      if(cv(1)(1:1).eq.' '.or.lcase(cv(1)(1:1))) goto 4
      if(nw.eq.0) then
*       character may have been entered -- try a new ion
        lby=cv(1)(1:2)
        if(cv(1)(3:3).ne.' ') then
*	  ion not space separated
          chtemp=chstr(3:60)
          call dsepvar(chtemp,3,drv,iv,cv,nv)
          lzy=cv(1)(1:4)
          nw=iv(2)
          xtemp=iv(3)
          zzzc=drv(3)
         else
c         space between element and ion
          lzy=cv(2)(1:4)
          nw=iv(3)
          xtemp=iv(4)
          zzzc=drv(4)
        end if
        nred=nred+1
        nsys=nred
        write(6,*) ' z= ',zzzc
        if(xtemp.eq.zzzc) then
          write(6,*) ' OK? <CR> or new value'
          write(6,'(''> '',$)')
          read(5,'(a)') chstr
          call dsepvar(chstr,1,drv,iv,cv,nv)
          if(drv(1).gt.0.0d0) then
            zzzc=drv(1)
          end if
        end if
        call tikset(lby,lzy,zzzc,nsys)
      end if
      if(nsys.le.0.or.nsys.gt.nred) nsys=1
c     change range if nw is negative
      if(nw.le.0) then
        nw=-nw
        write(6,*) ' Half-range (channels) '
        write(6,'(''> '',$)')
        read(5,*,err=4,end=4) nrang
        if(nrang.le.2) nrang = 50
      end if
      if(nw.le.ntick(nsys)) goto 1
      if(nw.gt.100.and.zzzc.gt.-1.0d0) then
c       try the line number input as a nearest integer wavelength
        wtry=dble(nw)
        deltw=1.0d20
        do ii=1,ntick(nsys)
          wrest=rstwav(ii,nsys)
          delt=abs(wrest-wtry)
          if(delt.lt.deltw) then
            deltw=delt
            nw=ii
            wrh=wrest
          end if
        end do
c       check that deltw resulting is small enough
        if(deltw.le.1.0d0) then
          write(6,100) nw,wrh
 100      format(1x,'Line',i4,'  wavelength',f10.2)
          goto 1
         else
          write(6,*) ' Nearest line too far away - closest is:'
          write(6,100) nw,wrh
          goto 2
        end if
       else
c       line number outside range
        write(6,*) ntick(nsys),' lines available '
        goto 2
      end if
c     determine center
 1    write(6,*) ' Wavelength',tikmk(nw,nsys)
      write(6,*) ac(1),ac(2)
      nch=int((tikmk(nw,nsys)-ac(1))/ac(2))
      if(nch.le.0.or.nch.ge.ngp) then
        nch=ngp/2
      end if
      ced=dble(nch)
      call vp_chanwav(dble(tikmk(nw,nsys)),ced,5.0d-2,20,nrun)
      nch=int(ced+0.5d0)
c     check it is in range
      if(nch.le.1.or.nch.ge.ngp) goto 3
c     lower limit
      nst=nch-nrang
      if(nst.lt.1) nst=1
c     upper limit
      ncn=nch+nrang
      if(ncn.gt.ngp) ncn=ngp
*     reset scaling ranges
      lc1=nst
      lc2=ncn
*     write out plot channel ranges
      write(6,*) ' lo =',nst,'   hi =',ncn
 4    return
 3    write(6,*) ' ** line outside range - channel ',nch
      goto 4
 5    write(6,*) ' ** Format is (2i8)  --  try again'
      goto 2
      end
