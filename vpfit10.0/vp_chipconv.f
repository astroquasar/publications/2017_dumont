      subroutine vp_chipconv(func,klen,nsubd,ichunk, fconv,phi)
*
*     Convolve func with instrumental profile, using
*     instrument function appropriate for this chunk. 
*     Result in phi in original bins, fconv for sub-bins
*     This need not be by subpixels if the instrument profile
*     is adequately sampled, but is here by default since
*     almost invariably it is only 2 or 3 pixels. May change
*     later to speed things up.
*     If instrument profile not well resolved, use subpixels
*
      implicit none
      include 'vp_sizes.f'
*
      integer klen,ichunk,nsubd
      double precision func(*)
      double precision fconv(*),phi(maxchs,maxnch)
*
      integer k,kktem,jks,jjs,jis
      integer lkoffs,numinch
      double precision ddscl,temp
*
      integer numchunk,nchchtot ! # of chunks,total # data points
      integer kchstrt(maxnch),kchend(maxnch),nchlen(maxnch)
      common/vpc_chunkvar/kchstrt,kchend,nchlen,numchunk,nchchtot
      logical lresint(maxnch)
      double precision dlgres
      common/vpc_lresint/dlgres,lresint
      integer nwvch(maxnch)
      double precision wavch(maxchs,maxnch),wavchlo(maxchs,maxnch)
      common/vpc_wavch/wavch,wavchlo,nwvch
      double precision dach(maxchs,maxnch),derch(maxchs,maxnch)
      double precision drmch(maxchs,maxnch),dcch(maxchs,maxnch)
      common/vpc_datch/dach,derch,drmch,dcch
*     chunk sub wavelengths, set up for this chunk outside this routine
      integer npsubch
      double precision wvsubd(maxscs)
      common/vpc_wvsubd/wvsubd,npsubch
*
      if(lresint(ichunk)) then
*       set back to data binsize, then convolve
        ddscl=dble(nsubd)
        numinch=kchend(ichunk)-kchstrt(ichunk)+1
        lkoffs=kchstrt(ichunk)-1
        do k=1,nchlen(ichunk)
          jks=nsubd*k
          jjs=jks-nsubd+1
          temp=0.0d0
          do jis=jjs,jks
            temp=temp+func(jis)
          end do
          fconv(k)=temp/ddscl
        end do
*       convolve with the instrument profile using original
*       binsize
        call vp_chspread(fconv,1,nchlen(ichunk),
     :       wavch,maxchs,maxnch,ichunk, func)
*       multiply by continuum and store
        do k=1,numinch
          kktem=k+lkoffs
          phi(k,ichunk)=dcch(kktem,ichunk)*func(kktem)
        end do
       else
*       instrument profile not well resolved, so do everything
*       in sub-bins         
        call vp_subchspread(func,1,klen,ichunk,wvsubd,npsubch, fconv)
*       need to set back to old binsize, and multiply by the continuum
        ddscl=dble(nsubd)
        numinch=kchend(ichunk)-kchstrt(ichunk)+1
        lkoffs=kchstrt(ichunk)-1
        do k=1,numinch
          jks=nsubd*(k+lkoffs)
          jjs=jks-nsubd+1
          temp=0.0d0
          do jis=jjs,jks
            temp=temp+fconv(jis)
          end do
          phi(k,ichunk)=dcch(k+lkoffs,ichunk)*temp/ddscl
        end do
      end if
      return
      end
