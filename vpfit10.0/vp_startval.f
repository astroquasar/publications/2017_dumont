      subroutine vp_startval
*     set the common parameters to the default initial values
*     so that nothing remains undefined (hopefully),
*     and print out various initial values
*
*     array sizes file
      include 'vp_sizes.f'
*
*     parameter input character string
      character*32 chclparm
      real rv(2)
      integer iv(2)
      character*8 cv(2)
*     Current line parameters for guess (default = Ly-a)
      logical lgs
      character*2 ings
      character*4 lvgs
      double precision wvgs,fkgs,algs,amgs
      common/vpc_pargs/wvgs,fkgs,algs,amgs,lgs,ings,lvgs
*     default line parameters
      character*2 ingsdef
      character*4 lvgsdef
      double precision wvgsdef,fkgsdef,algsdef,amgsdef
      common/vpc_pargsdef/wvgsdef,fkgsdef,algsdef,amgsdef,
     :                       ingsdef,lvgsdef
*
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*
*     PRESETS (which used to be in main VPFIT routine)
*     in same order as they are set below:
*     
*     number of sub-bins (init to 5)
      integer nexpd
      common / nexpd / nexpd
*     printout variable
      logical verbose
      common/vp_sysout/verbose
*     monitoring level 0:minimal - 16:everything
      integer nverbose
      common/vp_sysmon/nverbose
*     log or linear Doppler parameters ibvar=1 for logb, 0 linear
*     redshift z: 0-norm, 1-log(1+z), 2 velocity bins
      integer ibdvar,izdvar
      common/vp_bzstyle/ibdvar,izdvar
*     air to vacuum wavelength factor (so can use vac wavelengths)
      double precision avfac
      common/vpc_airvac/avfac
*     rejection of ill-constrained variables
      logical nasty
      common/vpc_nasty/knasty,nasty
*     last line info for cursor (used in vp_rdlines)
      character*80 chline
      common/vpc_hline/chline
*     iteration parameters
      integer nminchsqit
      double precision cdstsf,zstep,bstep,chsqdif1,chsqdnth
      double precision chsqdif2
      common/vpc_jkw2/cdstsf,zstep,bstep,chsqdif1,chsqdnth,
     :       chsqdif2,nminchsqit
*     iteration parameter limits
      double precision chsqdlim1
      common/vpc_jkw2lim/chsqdlim1
*     sig lim factor
      integer nptype
      double precision csiglfac
      common/vpc_nptype/csiglfac,nptype
*     chunk wavelength variables, normally wavelength array & lengths
      integer nwvch(maxnch)
      double precision wavch(maxchs,maxnch),wavchlo(maxchs,maxnch)
      common/vpc_wavch/wavch,wavchlo,nwvch
*     chunk subset variables (rfc: 24.08.05)
*     only preset is nrextend - number of pixels to extend a chunk by
*     so that convolution does not have a jump at the end
      integer nrextend
      common/vpc_chunkext/nrextend
*     chunk subdivision variables
      integer nminsubd,nmaxsubd
      double precision bindop
      common/vpc_vpsubd/bindop,nminsubd,nmaxsubd
      integer nxsubcht
      double precision subchthres
      common/vpc_vpsubd2/subchthres,nxsubcht
*     chunk FWHM well sampled by pixels (if this variable = .true.)
      logical lresint(maxnch)
      double precision dlgres
      common/vpc_lresint/dlgres,lresint
*     other rest wavelength stuff. Action may depend on lqmucf
      logical lqmucf,lchvsqmu
      double precision qmucf
      double precision qscale
      common/vpc_qmucf/qmucf(maxats),qscale,lqmucf,lchvsqmu
*     starter table position
      integer nwhere
      common/vpc_resposn/nwhere
*
*     pgplot current device (up to 4 allowed)
*     npgcurr is current one (0=not opened at all)
      integer npgcurr,npgdev(4)
      common/vp_pgpldev/npgcurr,npgdev
*     ucoptv format specifier
      character*132 chformuc,chformer
      character*164 chformet
      common/vpc_ucformat/chformuc,chformer,chformet
*     summary printout precision thresholds
      double precision dmer6,dmer8
      common/vpc_dminerval/dmer6,dmer8
*     mimformat for da/a
      double precision unitamim
      common/vpc_scalemim/unitamim
*     temperature units 
      double precision thunit,thmax
      common/vpc_thscale/thunit,thmax
*     extra ion list & parameters
      integer nionxt
      character*2 elmxt(maxxpi)
      character*4 ionxt(maxxpi)
      character*2 ipinxt(maxxpp)
      double precision bvxmin
      double precision parmxt(maxxpp)
      common/vpc_parmxt/bvxmin,parmxt,ionxt,elmxt,nionxt,ipinxt
*     last file name, lgflnm=.true. if filename set, false otherwise
      character*80 cflname
      integer lastord
      logical lgflnm
      common/vpc_lastfn/cflname,lastord,lgflnm
*     input streams
      common/rdc_inputc/inputc
*     print counter, with nrcck=no. of 'ions' with region #s specified
      integer kprcck,nrcck
      common/vpc_kprcck/kprcck,nrcck
*
      kprcck=0
      nrcck=0
*     no last filename
      lgflnm=.false.
      lastord=0
      cflname=' '
*     pgplot device not opened at start
      npgcurr=0
*     thunit is units for temperature (in K). Preset is 1000K.
      thunit=1.0d3
*     default max temperature so high as to be no restriction:
      thmax=1.0d25
*     units for da/a I/O. Default is unity, i.e. real da/a not scaled.
      unitamim=1.0d0
*     ucoptv o/p format not set
      chformuc=' '
      chformer=' '
      chformet=' '
*     default input is from keyboard:
      inputc=5
*     chunk extension default is 20 pixels that should be enough in general
*     make larger if instrument fwhm > 20 or so pixels, which should be
*     pretty rare!
      nrextend=20
*     Note that maxchs should be expected maximum + 40 at least.
*     set all chunk wavelength array lengths to zero (= not used)
      do j=1,maxnch
        nwvch(j)=0
*       assume the worst case - instrument profile poorly sampled
        lresint(j)=.false.
      end do
*     default number of pixels across a FWHM for treating instrument
*     profile as well-sampled
      dlgres=7.0d0
*     defaults for subdivision of fitting regions
      bindop=5.0d0
      nminsubd=1
      nmaxsubd=11
*     chi^2 threshold for sub-bins
      subchthres=1.0d30
      nxsubcht=1
*     preset system number
      nionxt=0
*     print type for linear variables
      nptype=0
      csiglfac=1.0d0
*     minimum possible chi-squared difference
      chsqdlim1=1.0d-10
*     number of fit variables per system, set to Voigt profile default
      noppsys=3
*     internal ordering
      nppcol=1
      nppbval=3
      nppzed=2
*     Current line parameters for guess (default = Ly-a)
      lgs=.true.
      ingsdef='H '
      lvgsdef='I   '
      wvgsdef=1215.6701d0
      fkgsdef=0.4164d0
      algsdef=6.265d8
      amgsdef=1.00794d0
      ings=ingsdef
      lvgs=lvgsdef
      wvgs=wvgsdef
      fkgs=fkgsdef
      algs=algsdef
      amgs=amgsdef
*
*     Don't print region statistics for da/a in summary file
      lchvsqmu=.false.
*
*     size constraint warning
      write(6,*)
      write(6,*) 'maximum file size      : ', maxfis
      write(6,*) 'maximum chunk size     : ', maxchs
      write(6,*) 'maximum no. parameters : ', maxnpa
      write(6,*) 'maximum no. chunks     : ', maxnch
      write(6,*) 'exceeding above will corrupt results '//
     :             'probably without warning'
      write(6,*)
      write(6,*)
*
*
*     PRESETS:
*     starter resolution table position
      nwhere=1
*     number of sub-bins
      nexpd = 5
*     running commentary on input files as default
      verbose=.false.
      nverbose=2
*     btype (0-linear, 1-log):
      ibdvar=0
*     ztype (0-linear)
      izdvar=0
*     air-vac factor, set initially to the vacuum default
      avfac=1.0d0
*     Ill-conditioned pointers:
      nasty=.false.
      knasty=0
*     summary printout precision thresholds
      dmer6=1.0d-6
      dmer8=1.0d-8
*     fine structure q scale
      qscale=1.0d-6
*     
*     default ion for cursor input
      chline='HI'
      chsqdif1=1.0d-3
      chsqdif2=1.0d-2
      chsqdnth=4.0d0
      nminchsqit=0
*     as a development aid, can use verbose=n as a parameter
      call getarg(1, chclparm)
      if(chclparm(1:6).eq.'verbos') then
        verbose=.true.
        nverbose=8
        nvb=lastchpos(chclparm)
        if(nvb.gt.7) then
          chclparm(8:8)=' '
          call sepvar(chclparm,2,rv,iv,cv,nv)
          if(nv.gt.1.and.iv(2).gt.0) then
            nverbose=iv(2)
          end if
        end if
      end if
      return
      end
