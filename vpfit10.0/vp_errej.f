      subroutine vp_errej(parm,parerr,np,istat)

      implicit none
      include 'vp_sizes.f'

*
*     check array parm for conditions indicating system rejection
*     
*     INPUT:  parm(np)
*             parerr(np)
*             np
*
*     OUTPUT: istat=no. of rejected systems
*             common block vpc_nrejs contains rejected system numbers
*
      integer np,istat
      double precision parm(np), parerr(np)
*
*     LOCAL:
        
      integer kk,kkh,kbdiff,kcdiff
      double precision compval,temp
*
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*
      integer ndrop,kdrop,ndroptot,ndrwhy
      common/vpc_nrejs/ndrop,kdrop(maxnio),ndroptot,ndrwhy
*
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
*
      double precision errbmax,errlnmax
      common/vpc_rejsys/errbmax,errlnmax
*	rfc 1.5.97 rejection of ill-constrained variables
      logical nasty
      integer knasty
      common/vpc_nasty/knasty,nasty
*
*     ndrop is preset in subroutine UCERCK
      compval=0.0d0
      kkh=0
*     drop the ill-conditioned one first -- that has effectively
*     infinite errors
      if(nasty.and.knasty.gt.0) then
        kkh=knasty*noppsys
        ndrwhy=1
       else
        open ( 20, file = 'stoprej', status = 'old', err = 501 )
        call system( '/bin/rm stoprej' )
        if(errbmax.lt.1.0d20.or.errlnmax.lt.1.0d20) then
*         search by error
          kbdiff=nppbval-noppsys
          kcdiff=nppcol-noppsys
          do kk=noppsys,np,noppsys
*	    check velocity dispersion
            if(parerr(kk+kbdiff).gt.errbmax) then
*	      check the column density error
              if(indvar.eq.1) then
                if(parerr(kk-2)/scalelog.gt.errlnmax) then
                  if(parerr(kk+kbdiff)*parerr(kk+kcdiff).gt.
     :                compval) then
                    kkh=kk
                    compval=parerr(kk+kbdiff)*parerr(kk+kcdiff)
                  end if
                end if
               else
                temp=log10(parerr(kk+kcdiff)/parm(kk+kcdiff))
                if(temp.gt.errlnmax) then
                  if(parerr(kk+kbdiff)*temp.gt.compval) then
                    kkh=kk
                    compval=parerr(kk+kbdiff)*temp
                  end if
                end if
              end if
            end if
          end do
         else
*         error limits large so nothing dropped
          kkh=0
        end if
      end if
      if(kkh.gt.0.and.np.gt.noppsys) then
*       the system at kkh to be dropped, if there is
*       more than one system present.
        ndrop=ndrop+1
        kdrop(ndrop)=kkh/noppsys
        istat=ndrop
        nasty=.false.
        knasty=0
        ndrwhy=0
       else
        istat=0
      end if
 502  return
 501  istat=0
      kkh=0
      goto 502
      end
