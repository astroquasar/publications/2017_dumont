      logical function ucase( char )
*     true if char is an uppercase character      
      implicit none
      character*1 char
      ucase = ( char .ge. 'A' .and. char .le. 'Z' )
      end

      logical function lcase( char )
*     true if char is lowercase character
      implicit none
      character*1 char
      lcase = ( char .ge. 'a' .and. char .le. 'z' )
      end

      logical function noceq( char1, char2 )
*     true if characters same, regardless of case - actually true for 
*     some pairs of non-alphabetic characters too
      implicit none
      character*1 char1, char2
      noceq = ( mod( ichar( char1 ) - ichar( char2 ),
     :               ichar( 'A' ) - ichar( 'a' ) ) .eq. 0 )
      end

      logical function noceqv(chstr1,chstr2)
*     true if all characters in a string are the same, regardless of case.
*     string can be any length, if lengths disparate noceqv = .false.
*     rfc  3.1.95
      implicit none
      character*(*) chstr1,chstr2
      logical noceq
      integer lens,i
*     strings the same length?
      lens=len(chstr1)
      if(lens.eq.len(chstr2)) then
        noceqv=.true.
        do i=1,lens
          noceqv = ( noceqv .and. noceq(chstr1(i:i),chstr2(i:i)) )
        end do
       else
        noceqv=.false.
      end if
      return
      end


      logical function nocgt( char1, char2 )
*
*     true if first (not second, as originally commented!)
*     character greater in ascii collating sequence, after
*     removing offset for upper / lower case
*
      implicit none
*
      character*1 char1, char2
      integer ic1, ic2
*
      logical ucase

      if ( ucase( char1 ) ) then
         ic1 = ichar( char1 ) - ichar( 'A' )
      else
         ic1 = ichar( char1 ) - ichar( 'a' )
      end if
      if ( ucase( char2 ) ) then
         ic2 = ichar( char2 ) - ichar( 'A' )
      else
         ic2 = ichar( char2 ) - ichar( 'a' )
      end if

      nocgt = ( ic1 .gt. ic2 )

      end
