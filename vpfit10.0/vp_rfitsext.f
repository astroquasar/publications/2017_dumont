      subroutine vp_rfitsext(cxname,last,unit1,ichunk, 
     :                        wch,nwco,da,de,ca,nct,ndim,status)
*
*     read in fits extension (table) data
*
      implicit none
      include 'vp_sizes.f'
*     subroutine arguments
      character*(*) cxname
      integer last, unit1,ichunk
      double precision wch(maxwco)
      integer nwco
      double precision da(maxfis),de(maxfis),ca(maxfis)
      integer nct
      integer ndim
      integer status
*     Local
      logical lundef
      integer j,jtemp
      integer kcode,krepeat,kwidth
      integer ncols
*     integer nordrs
      integer nrows,nrowsp1,nx
      double precision wdtemp,dtemp
*
*     COMMON:
*     miscellaneous sizes etc from input spectrum (as arg now)
*     integer ndim,nelm,nord
*     integer ndims(10)
*     common/fginparms/ndim,ndims,nelm,nord
*     vac/air wavelengths
      double precision avfac
      common/vpc_airvac/avfac
*
*     Wavelength control
      character*8 wcftype
      character*4 vacind
      double precision helcfac
      integer noffs
      common/c8_wcftype/wcftype,helcfac,noffs,vacind
*
*     last file name, lgflnm=.true. if filename set, false otherwise
      character*80 cflname
      integer lastord
      logical lgflnm
      common/vpc_lastfn/cflname,lastord,lgflnm
      logical verbose
      common/vp_sysout/verbose
*     sigma scale rescale for testing purposes
      double precision sigsclx
      common/vpc_sigsclx/sigsclx
*     the individual file scale coeffts
      double precision ssclcf,wvalsc
      integer nscl
      common/sigsclcft/ssclcf(maxtas,maxnch),wvalsc(maxtas,maxnch),
     1                   nscl(maxnch)
*     exposure time
      double precision timexp
      common/vpc_exptime/timexp
*     double precision dexptime
*     common/cgs4_head/dexptime
*     data scale parameters (lrescale true if data rescaled, scale is
*     number it was multiplied by)
      logical lrescale
      double precision scale
      common/vpc_dscale/scale,lrescale
*
      call ftnopn(unit1,cxname,0, status)
      if(status.ne.0) then
        write(6,*) 'Failed to open FITS extension',cxname(1:last)
        goto 903
      end if
*
*     get number of rows and columns
      call ftgnrw(unit1, nrows,status)
      if(status.ne.0) then
        write(6,*) 'Failed to get NROWS',cxname(1:last)
        goto 903
      end if
      call ftgncl(unit1, ncols,status)
      if(status.ne.0) then
        write(6,*) 'Failed to get NCOLS',cxname(1:last)
        goto 903
      end if
      if(nrows.gt.maxwco) then
        write(6,*) 'Data truncated from',nrows,' to',maxwco
        nrows=max(nrows,maxwco)
      end if
      write(6,*) nrows,' rows X',ncols,' columns'
*     data type column 1: (82=dble; 42=real; 41=integer; 16=ASCII)
      call ftgtcl(unit1,1, kcode, krepeat,kwidth,status)
      write(6,*) kcode,krepeat,kwidth
      if(kcode.eq.82) write(6,*) ' First column double precision'
      if(kcode.eq.42) write(6,*) ' First column real'
      if(kcode.eq.41) write(6,*) ' First column integer'
      if(kcode.eq.16) write(6,*) ' First column ASCII'
      if(kcode.eq.82.or.kcode.eq.42) then
        lundef=.false.
*       get first column as wavelengths
        if(kcode.eq.42) write(6,*) 'Single precision wavelengths'
        call ftgcvd(unit1,1,1,1,nrows,-1.0d34, wch,lundef,status)
        if(status.ne.0) then
          write(6,*) 'Binary table wavelength read fail'
          goto 903
        end if
        if(lundef) then
          write(6,*) 'Undefined wavelength values!'
          status=1
          goto 903
        end if
      end if
*     column 2 should be the data
      call ftgtcl(unit1,2, kcode, krepeat,kwidth,status)
      if(kcode.eq.82.or.kcode.eq.42) then
        lundef=.false.
        call ftgcvd(unit1,2,1,1,nrows,-1.0d34, da,lundef,status)
        if(status.ne.0) then
          write(6,*) 'Binary table data read fail'
          goto 903
        end if
      end if
*     If it has reached this stage, then set length and control
*     data variables:
      nct=nrows
*     nord=1
      ndim=1
*     ndims(1)=nct
      cflname=cxname
      lgflnm=.true.
*     nordrs=1
      write(6,*) 'Sigma scaling ignored'
      nscl(ichunk)=1
      ssclcf(1,ichunk)=sigsclx
      wvalsc(1,ichunk)=0.0d0
*     for wavelengths:
      wcftype='array'
      nwco=nrows
      helcfac=1.0d0
      noffs=0
      vacind='vacu'
      avfac=1.0d0
*     column 3 should be the error, if present
      if(ncols.ge.3) then
        call ftgtcl(unit1,3, kcode, krepeat,kwidth,status)
        if(kcode.eq.82.or.kcode.eq.42) then
          lundef=.false.
          call ftgcvd(unit1,3,1,1,nrows,-1.0d34, de,lundef,status)
          if(status.ne.0) then
            write(6,*) 'Binary table error read fail'
            write(6,*) 'Errors set to 1.'
            do j=1,nrows
              de(j)=1.0d0
            end do
            status=0
            goto 9031
           else
*           error read in OK, so make variances
            do j=1,nrows
              if(de(j).ge.0.0d0) then
                de(j)=de(j)*de(j)
              end if
            end do
          end if
         else
          do j=1,nrows
            de(j)=1.0d0
          end do
          write(6,*) 'Error estimates not dble/real variables'
          write(6,*) 'Errors set to 1.'
          goto 9031
        end if
       else
        do j=1,nrows
          de(j)=1.0d0
        end do
        write(6,*) 'Error estimates absent'
        write(6,*) 'Errors set to 1.'
        goto 9031
      end if
*     If column 4 is present, then it should be the continuum
      if(ncols.ge.4) then
        call ftgtcl(unit1,4, kcode, krepeat,kwidth,status)
        if(kcode.eq.82.or.kcode.eq.42) then
          lundef=.false.
          call ftgcvd(unit1,4,1,1,nrows,-1.0d34, ca,lundef,status)
          if(status.ne.0) then
            write(6,*) 'Binary table continuum read fail'
            write(6,*) '... set to 1.'
            do j=1,nrows
              ca(j)=1.0d0
            end do
            status=0
            goto 9031
          end if
         else
          do j=1,nrows
            ca(j)=1.0d0
          end do
          write(6,*) 'continuum estimates not dble/real variables'
          write(6,*) '... set to 1.'
          goto 9031
        end if
       else
        do j=1,nrows
          ca(j)=1.0d0
        end do
        write(6,*) 'Continuum absent'
        write(6,*) '... set to 1.'
        goto 9031
      end if
 9031 continue
*     check if wavelengths the right way round, and swap data order
*     if they are not.
      if(wch(1).gt.wch(nrows)) then
*       swap everything
        write(6,*) ' Inverting data order'
        nx=nrows/2
        nrowsp1=nrows+1
        do j=1,nx
          jtemp=nrowsp1-j
          wdtemp=wch(j)
          wch(j)=wch(jtemp)
          wch(jtemp)=wdtemp
          dtemp=da(j)
          da(j)=da(jtemp)
          da(jtemp)=dtemp
          dtemp=de(j)
          de(j)=de(jtemp)
          de(jtemp)=dtemp
          dtemp=ca(j)
          ca(j)=ca(jtemp)
          ca(jtemp)=dtemp
        end do
      end if
 903  return
      end
