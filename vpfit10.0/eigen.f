      subroutine eigen(aa,vv,m)
*
      implicit none
      include 'vp_sizes.f'
      double precision aa(*),vv(*)
*     was :       aa(1),vv(1),
      integer m
*
*     Local
      integer i,ii,iu,j,jl,k,n,icnt
      double precision a(maxmas,maxmas),v(maxmas,maxmas)
      double precision ratio,t,ta,aii,aij,ajj
      double precision ajk,aik,aki,akj,rmaxd,rmaxo
      double precision g,sn,cs,vki,vkj
      double precision tol
*
*     Jacobi method for calculating eigenvalues and eigenvectors
*     of a real symmetric matrix. (Serial-threshold variant)
      do i=1,m
        do j=1,m
          ii=(i-1)*m+j
          a(i,j)=aa(ii)
        end do
      end do
      n=m-1
      tol=1.0d-6
      icnt=0
*     initialise arrays
      do i=1,m
        do j=1,m
          v(i,j)=0.d0
        end do
        v(i,i)=1.d0
      end do
*
*     find maximum diagonal and off-diagonal elements
 200  rmaxd=dabs(a(1,1))
      rmaxo=0.d0
      do i=2,m
        rmaxd=max(rmaxd,dabs(a(i,i)))
        iu=i-1
        do j=1,iu
          rmaxo=max(rmaxo,abs(a(i,j)))
        end do
      end do
      if(rmaxo.lt.dble(tol)*rmaxd) goto 900
      ratio=rmaxo/rmaxd
      if(icnt.ge.30) then
        write(6,1000) icnt,ratio
 1000   format(' Iteration failed to converge in',i5,' cycles',/,
     :      ' Ratio of maximum off-diagonal to diagonal term',e15.5/)
        return
      end if
*
*     apply rotation matrix to minimise off-diagonal terms
      do 500 i=1,n
      jl=i+1
      aii=a(i,i)
      do 400 j=jl,m
      aij=a(i,j)
      if(dabs(aij).lt.0.2d0*rmaxo)goto 400
      ajj=a(j,j)
      if(ajj.eq.aii)goto 300
      t=aij/(ajj-aii)
      ta=dabs(t)
      if(ta.gt.1.d0)t=t/ta
      goto 310
  300 t=1.d0
      if(aij.lt.0.d0)t=-t
  310 g=t/(2.d0*(1.d0+t*t))
      sn=2.d0*g/(1.d0+g*g)
      cs=1.d0-g*sn
      do 320 k=1,m
      akj=a(k,j)
      aki=a(k,i)
      a(k,i)=cs*aki-sn*akj
  320 a(k,j)=sn*aki+cs*akj
      do 340 k=1,m
      aik=a(i,k)
      ajk=a(j,k)
      a(i,k)=cs*aik-sn*ajk
      a(j,k)=sn*aik+cs*ajk
      vki=v(k,i)
      vkj=v(k,j)
      v(k,i)=cs*vki-sn*vkj
  340 v(k,j)=sn*vki+cs*vkj
  400 continue
  500 continue
      icnt=icnt+1
      goto 200
  900 continue
c *** put answers in output arrays
      do 950 i=1,m
      do 950 j=1,m
      ii=(i-1)*m+j
      aa(ii)=a(i,j)
  950 vv(ii)=v(i,j)
      return
      end
