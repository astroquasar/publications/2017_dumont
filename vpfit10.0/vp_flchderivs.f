      subroutine vp_flchderivs(nchunk,nfunc)
*
*     flux derivatives for each chunk
*
      implicit none
      include 'vp_sizes.f'
*
      integer nchunk,nfunc
*     Local:
      integer ichunk,ichdum,ioffset
      integer k,klen,kjb,kloc,kpl
      integer n,nchcen,nsubd,nlen
      double precision bmincur,bxcur
      double precision dtemp,sigv,wtvi,wtvtot
      double precision velpch
*     Functions
      double precision vpf_bvalsp
*
*     Common:
*     dphi/dlambda values
      double precision dphi(maxchs,maxnch)
      common/vpc_dphidlam/dphi
*     chunk subdivision variables
      integer nminsubd,nmaxsubd
      double precision bindop
      common/vpc_vpsubd/bindop,nminsubd,nmaxsubd
*     subchunk wavelengths
      integer npsubch
      double precision wvsubd(maxscs)
      common/vpc_wvsubd/wvsubd,npsubch
*     chunk wavelengths
      integer nwvch(maxnch)
      double precision wavch(maxchs,maxnch),wavchlo(maxchs,maxnch)
      common/vpc_wavch/wavch,wavchlo,nwvch
*     func may be subchunk, phi is chunk
      double precision func(maxscs),phi(maxchs,maxnch)
      common/vpc_der/func,phi
*     subchunk workspace
      double precision contpass(maxscs),fconv(maxscs)
      common/vpc_duwork/fconv,contpass
*     chunk list special variables
      integer linchk(maxnio)  ! ties lines to chunks
      common/vpc_linchk/linchk
*     fit & line parameters
      character*2 ion(maxnio)
      character*4 level(maxnio)
      double precision parm(maxnpa)
      integer nlines,np
      common/vpc_parry/parm,ion,level,nlines,np
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     tied indicators
      integer isod,isodh
      character*2 ipind(maxnpa)
      common/vpc_usoind/ipind,isod,isodh
*     New 2D dataset variables
      integer numchunk,nchchtot ! # of chunks,total # data points
      integer kchstrt(maxnch),kchend(maxnch),nchlen(maxnch)
      common/vpc_chunkvar/kchstrt,kchend,nchlen,numchunk,nchchtot
      double precision dach(maxchs,maxnch),derch(maxchs,maxnch)
      double precision drmch(maxchs,maxnch),dcch(maxchs,maxnch)
      common/vpc_datch/dach,derch,drmch,dcch
*     monitor progress?
      integer nverbose
      common/vp_sysmon/nverbose
*     mimformat for da/a, I/O units
      double precision unitamim
      common/vpc_scalemim/unitamim
*
*     first get current minimum Doppler parameter - bmincur - so can
*     determine subdivisions needed. 
      bmincur=1.0d30
      np=nfunc*noppsys
      do n=1,nfunc
*       skip special cases
        if (ion(n) .ne. '<<' .and.
     :        ion(n) .ne. '<>' .and.
     :        ion(n) .ne. '>>' .and.
     :        ion(n) .ne. '__' ) then
          bxcur=vpf_bvalsp(n,parm,ipind)
          bmincur=min(bmincur,bxcur)
        end if
      end do
      do ichunk=1,nchunk
        ichdum=ichunk
*       This section copied from vp_ucoptv. Not done there because
*       it would slow down an already slow process
*       set subdivision value from chunk velocity interval near middle
        nchcen=nchlen(ichunk)/2
        velpch=0.5d0*2.99792458d5*
     :       (wavch(nchcen+1,ichunk)-wavch(nchcen-1,ichunk))/
     :       wavch(nchcen,ichunk)
*       want at least bindop bins per Doppler parameter, so min binsize
        dtemp=bmincur/bindop
        nsubd=int(velpch/dtemp+0.9999d0)
        if(nsubd.lt.nminsubd) nsubd=nminsubd
        nsubd=min(nsubd,maxscs/nchlen(ichunk))
        nsubd=min(nsubd,nmaxsubd)
*       length of subdivided continuum array:
        klen=nchlen(ichunk)*nsubd
        npsubch=klen
        do k=1,klen
          contpass(k)=1.0d0
          func(k)=1.0d0
          fconv(k)=1.0d0
          kjb=(k-1)/nsubd
          kloc=k-nsubd*kjb
          dtemp=(dble(kloc)-0.5d0)/dble(nsubd)
          wvsubd(k)=(1.0d0-dtemp)*wavchlo(kjb+1,ichunk)+
     :         dtemp*wavchlo(kjb+2,ichunk)
        end do
        do n=1,nfunc
*	  Include only if chunk matches or general 
          if (linchk(n).eq.0 .or.
     :          linchk(n).eq.ichunk ) then
            call vp_spvoigte(func,contpass,1,klen,wvsubd,npsubch,
     :            parm,ipind,ion,level,n,np,ichdum,fconv)
            do k=1,klen
              contpass(k)=func(k)
            end do
          end if
        end do
*
*       convert func to d(func)/d(lambda)
        do k=2,klen-1
          func(k)=(contpass(k+1)-contpass(k-1))/
     :            (wvsubd(k+1)-wvsubd(k-1))
        end do
*       endpoints:
        func(1)=(contpass(2)-contpass(1))/
     :            (wvsubd(2)-wvsubd(1))
        func(klen)=(contpass(klen)-contpass(klen-1))/
     :            (wvsubd(klen)-wvsubd(klen-1))
*       Convolve func with instrument profile & rebin
*       fconv is subbin convolution; dphi is binned as for original data
        call vp_chipconv(func,klen,nsubd,ichdum, fconv,dphi)
*
*       compute sigma_v for the chunk
        wtvtot=0.0d0
        ioffset=kchstrt(ichunk)-1
        nlen=kchend(ichunk)-ioffset
*       sigv is in units of c, and error held as square anyway
        do k=1,nlen
          kpl=k+ioffset
          wtvi=(wavch(kpl,ichunk)*dphi(k,ichunk))**2/derch(kpl,ichunk)
          wtvtot=wtvtot+wtvi
        end do
*       given the sigma_v for this chunk, can extract associated q
*       and wavelength for transition in that chunk. If more than
*       one then result ambiguous.
*       TO COME
        sigv=2.99792458d5/sqrt(wtvtot)
*
        write(26,'(a,i3,a,f16.8)') '! chunk',ichunk,'  sigma_v=',sigv
        write(18,*) '! chunk',ichunk,'  sigma_v=',sigv
*       put units for da/a in summary file
*       write out results pixel by pixel
        if(nverbose.ge.8) then
          write(11,*) 'Chunk',ichunk
          do k=1,nlen
            write(11,*) wavch(k+ioffset,ichunk),
     :       phi(k,ichunk),dphi(k,ichunk)
          end do
        end if
      end do
      if(abs(unitamim-1.0d0).gt.1.0d-2) then
*       Put da/a units in summary file
        write(26,'(a,1pe12.2)') '! da/a in units of',unitamim
      end if
*     close diagnostic write file if it was open
      if(nverbose.ge.8) then
        close(unit=11)
      end if
      return
      end
