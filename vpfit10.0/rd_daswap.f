      subroutine rd_daswap(da,de,ca,nct,ha,he,hc,nht,ind)
*     swap data between two arrays, along with wavelengths
      implicit none
      include 'vp_sizes.f'
*     
      integer nct,nht,ind
      double precision da(*),de(*),ca(*),ha(*),he(*),hc(*)
*     
      integer i,ntemp
      double precision ddum,temp
      character*8 cht8
      character*4 cht4
*
*     general wavelength coefficients
      integer nwco
      double precision wcf1(maxwco)
      common/vpc_wavl/wcf1,nwco
      character*8 wcftype
      character*4 vacind
      double precision helcfac
      integer noffs
      common/c8_wcftype/wcftype,helcfac,noffs,vacind
*     
*     h-stored wavelength coefficients
      integer nwcoh
      double precision wcf1h(maxwco)
      common/rdc_hwavl1/wcf1h,nwcoh
      character*8 wcftypeh
      character*4 vacindh
      double precision helcfach
      integer*4 noffsh
      common/rdc_hwavl2/wcftypeh,helcfach,
     :       noffsh,vacindh
*
      if(ind.gt.0) then
*       copy from da -> ha, etc
        do i=1,nct
          ha(i)=da(i)
          he(i)=de(i)
          hc(i)=ca(i)
        end do
        do i=1,maxwco
          wcf1h(i)=wcf1(i)
        end do
        nwcoh=nwco
        wcftypeh=wcftype
        helcfach=helcfac
        noffsh=noffs
        vacindh=vacind
        nht=nct
        goto 1
      end if
*
      if(ind.lt.0) then
*       copy to da <- ha, etc
*       which variables are copied is set by ind: 1=data only
*       2=data+error, 3:data/error/continuum
        do i=1,nht
          da(i)=ha(i)
        end do
        if(ind.lt.-1) then
          do i=1,nht
            de(i)=he(i)
          end do
        end if
        if(ind.lt.-2) then
          do i=1,nht
            ca(i)=hc(i)
          end do
        end if
        do i=1,maxwco
          wcf1(i)=wcf1h(i)
        end do
        nwco=nwcoh
        wcftype=wcftypeh
        helcfac=helcfach
        noffs=noffsh
        vacind=vacindh
        nct=nht
        goto 1
      end if
*
      if(ind.eq.0) then
*       copy da <-> ha
        ntemp=max0(nct,nht)
        do i=1,ntemp
          temp=ha(i)
          ha(i)=da(i)
          da(i)=temp
          temp=he(i)
          he(i)=de(i)
          de(i)=temp
          temp=hc(i)
          hc(i)=ca(i)
          ca(i)=temp
        end do
        do i=1,maxwco
          ddum=wcf1h(i)
          wcf1h(i)=wcf1(i)
          wcf1(i)=ddum
        end do
        ntemp=nwcoh
        nwcoh=nwco
        nwco=ntemp
        cht8=wcftypeh
        wcftypeh=wcftype
        wcftype=cht8
        ddum=helcfach
        helcfach=helcfac
        helcfac=ddum
        ntemp=noffsh
        noffsh=noffs
        noffs=ntemp
        cht4=vacindh
        vacindh=vacind
        vacind=cht4
        ntemp=nht
        nht=nct
        nct=ntemp
      end if
 1    return
      end
