      subroutine vp_tsetxref(nlines,parm,ipind,nn,lparch)
*
*     rfc 18.02.09
*     subroutine vp_tsetxref(elmnt,ionzl,nlines,parm,ipind,nn,lparch)
*     replaced since first two parameters were never used
*
*     set up a cross-reference list for tied parameters
*     in the array mtxref, and reassign parameters if lparch=.true.
      implicit none
*
      include'vp_sizes.f'
*
*     functions:
      logical lcase,noceqv,nocgt
*
*     variables:
      logical lparch
      integer nlines,nn
*      character*2 elmnt(maxnio)
*      character*4 ionzl(maxnio)
      double precision parm(maxnpa)
      character*2 ipind(maxnpa)
*
*     Local:
      integer i,ik,im,j,jjj,jk,k,mpos
      double precision bturb,bthermh,thermt
      double precision tprint
*
*     common:
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     Special conditions after this character (totals for column,
*     temperature/b for bval)
      character*2 lastch,firstch
      common/vpc_ssetup/lastch,firstch
*     output array from here is x-reference tied list
      integer mtxref
      common/vpc_txref/mtxref(maxnpa)
*     monitor level
      integer nverbose
      common/vp_sysmon/nverbose
*     atomic mass data for each ion
      double precision vturb,atmass,temper,fixedbth
      common/vpc_ucopder/vturb(maxnio),atmass(maxnio),
     :         temper(maxnio),fixedbth(maxnio)
*     temperature units
      double precision thunit,thmax
      common/vpc_thscale/thunit,thmax
*
*     initialize as zero
      do i=1,nn
        mtxref(i)=0
      end do
*     cycle through, checking for lower case first letter
*     in ipind array
      do i=1,nn
        if(lcase(ipind(i)(1:1))) then
*         have something to point to.
*         make sure you look at the same variable:
          k=mod(i,noppsys)
          if(k.eq.0) k=noppsys
          do j=1,nlines
            jk=(j-1)*noppsys+k
            if(noceqv(ipind(i),ipind(jk)).and.
     :           jk.ne.i.and.mtxref(i).eq.0) then
              mtxref(jk)=i
            end if
          end do
        end if
      end do
*     now have an array which points to the first lowercase
*     value in the common letter sequence
*
*     special case: fitted b/bturb should point to each other
*     need to link the Doppler parameters, so use pointer
      do j=1,nlines
        jk=noppsys*(j-1)+nppbval
        if(lcase(ipind(jk)(1:1)).and.mtxref(jk).ne.0.and.
     :       nocgt(ipind(jk)(1:1),lastch) )then
*         adjust the one pointed to now to point at the
*	  current variable
          mpos=mtxref(jk)
          if(mtxref(mpos).eq.0) then
            mtxref(mpos)=jk
            if(lparch) then
*             and change parameters to bturb (for lower) and temperature 
*             thermt (rescaled by thermsc) via bthermh (higher)
              im=(mpos-nppbval)/noppsys+1
              ik=(jk-nppbval)/noppsys+1
*             squares of basic widths
              bturb=(atmass(im)*parm(mpos)**2-atmass(ik)*parm(jk)**2)/
     :          (atmass(im)-atmass(ik))
*             bthermh is SQUARE of thermal width
              bthermh=(parm(mpos)**2-parm(jk)**2)/
     :          (1.0d0/atmass(im)-1.0d0/atmass(ik))
*             sqrt if > 0
              if(bturb.le.0.0d0) then
                bturb=0.0d0
               else
                bturb=sqrt(bturb)
              end if
              if(bthermh.le.0.0d0) then
                bthermh=0.0d0
                thermt=0.0d0
               else
*               for 1amu=1.66531-27kg,k=1.3806503-23J/K T=1K<->b=0.1289536 km/s
*               thunit is temperature units (1.0d4 for 10^4K for example)
                thermt=(60.135795d0*bthermh)/thunit
*               restricted range?
                if(thermt.gt.thmax) thermt=thmax
*               actual termal width for 1amu
                bthermh=sqrt(bthermh)
              end if
              if(mpos.lt.jk) then
                parm(mpos)=bturb
                parm(jk)=thermt
               else
                parm(mpos)=thermt
                parm(jk)=bturb
              end if
              if(nverbose.gt.8) then
                write(6,*) im,ik,mpos,jk
                tprint=thermt*thunit
                write(6,*) ' bturb=',bturb,' bhtherm=',bthermh,tprint
                write(6,*) atmass(im),atmass(ik),parm(mpos),parm(jk)
              end if
            end if
          end if
        end if
      end do
      if(nverbose.ge.12) then
        write(6,*) 'bval xref array'
        do j=1,nlines
          jjj=noppsys*(j-1)+nppbval
          write(6,*) jjj,mtxref(jjj)
        end do
      end if
      return
      end
