      double precision function vp_wvalsubch(tin)
*     convert sub chunk channel number (double precision) to wavelength using  
*     wavelengths wvsubd(n), using the lth wavelength set.
*     no need for air<->vacuum, heliocentric etc - was done before wavch
*     was set up.
      implicit none
      include 'vp_sizes.f'
      integer i
      double precision tin,tx
*     chunk wavelengths
      integer npsubch
      double precision wvsubd(maxscs)
      common/vpc_wvsubd/wvsubd,npsubch
*
*     wavelength array
      i=int(tin)
*     linearly interpolate for non-integer tin, making sure don't go beyond
*     ends. If tin outside range, extrapolate from nearest two points.
      if(i.ge.npsubch) then
        i=i-1
       else
        if(i.le.0) then
          i=i+1
        end if
      end if
      tx=tin-dble(i)
      vp_wvalsubch=wvsubd(i)*(1.0d0-tx) + wvsubd(i+1)*tx
      return
      end
