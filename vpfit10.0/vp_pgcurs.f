      subroutine vp_pgcurs( xpg, ypg, c )
*
      implicit none
      integer istat
      real xpg, ypg, xpgref, ypgref
      character*(*) c
*     function declaration
      integer pgband
*
*     10 special features:
*     (10,1) = PGPLOT mode for PGBAND (0 is default)
      integer ipgatt(10,9)
      common/vpc_pgattrib/ipgatt
*
*     anchor point is last position
      xpgref=xpg
      ypgref=ypg
      c=' '
*     X-windows cursor: L=A, mid= X, R=D:
      istat= pgband(ipgatt(10,1),1,xpgref,ypgref, xpg,ypg,c)
*     istat=1 if call successful, 0 otherwise. Values still
*     best possible, so ignore istat except to give warning.
      if(istat.gt.100) write(6,*) 'vp_pgcurs:',istat
*
      return
      end
