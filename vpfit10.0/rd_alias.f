      subroutine rd_alias(chstrin,ch2out)
*     Use a file of aliases for two-letter action commands,
*     so that you can type something which is vaguely memorable
*     when starting to us the program
      implicit none
*
      character*(*) chstrin
      character*2 ch2out
*     local
      integer lps,lpx,nvx
      integer ivx(2)
      double precision dvx(2)
      character*96 cvx(2)
      character*132 inchx
*     function declarations
      integer lastchpos
*     alias file
      character*96 chalias
      common/rdc_alias/chalias
*
      lps=lastchpos(chstrin)
      ch2out=chstrin(1:2)
      if(lps.ge.3) then
*       extended command name, so look for alias
        if(chalias(1:1).ne ' ') then
          open(unit=18,file=chalais,err=99)
 11       read(18,'(a)',end=99) inchx
          call dsepvar(inchx,2,dvx,ivx,cvx,nvx)
          lpx=lastchpos(cvx(1))
          if(chstrin(1:lps).eq.cvx(1:lpx)) then
            ch2out=cvx(2)(1:2)
          end if
          goto11
        end if
      end if
 99   return
      end
