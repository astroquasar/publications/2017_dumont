      subroutine vp_dotick(xmpg,xhpg,ympg,yhpg,donum,
     :                  ion,level,parm,nsys)
*
*     put tick marks above fitted lines in a region
*
      implicit none
      include 'vp_sizes.f'
*
      logical donum
      integer nsys
      real xmpg,xhpg,ympg,yhpg
      character*2 ion(maxnio)
      character*4 level(maxnio)
      double precision parm(*)
*
*     LOCAL:
      character*132 chstr,inchstr
      character*3 ic
      character*2 chbs
      integer i,j,ios,iline
      integer nchic,nnzzz,itemp
      real xpg,ypg,ylopg,yupg
      real yulopg,yloupg,xlxpg,xhxpg,schxpg,tchpg,ypospg
      real xxpg(2),yypg(2)
      double precision fl,fin,wx,zzzz
*
*     functions
      double precision wval
*
*     COMMON:
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     plot parameters
      integer nst,ncn,lc1,lc2
      real fampg
      common/ppam/nst,ncn,lc1,lc2,fampg
*     atomic constants
      character*2 lbz,lby
      character*4 lzz,lzy
      double precision alm,fik,asm
      integer nz
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),nz
*
      logical lmarkl,lmarkto
      character*132 chmarkfl
      double precision zmark
      common/rdc_lmarklv/zmark,chmarkfl,lmarkl,lmarkto
*     ipgtgrk=1 if Greek letters, else 0; yuptkf(def=0.9) is fract y-axis
*     for top of tick, ylentkf (def=0.07) fract y for length of tick
      integer ipgtgrk
      real yuptkpg,ylentkpg
      common/vpc_tick/ipgtgrk,yuptkpg,ylentkpg
*     velocity variables
      logical lvel
      real wcenpg,vellopg,velhipg,zp1refpg
      common/vpc_pgvels/wcenpg,vellopg,velhipg,lvel,zp1refpg
*     light speed
      real ccvacpg
      common/vpc_sgconst/ccvacpg
*     plots tickmarks on pgplot device, using the data arrays
*
      ios=0
*     backslash is an escape on some compilers, not on others.
*     to get around this:
      chbs='\\'
*     wavelength limits:
      fl=wval(dble(nst))
      xlxpg=real(fl)
      fin=wval(dble(ncn))
      xhxpg=real(fin)
      iline=1
*
*     tick ranges
      if(lmarkl) then
*       full information limits
        call pgqch(schxpg)
        yupg=0.98*(yhpg-ympg)+ympg
        yulopg=0.90*yupg
        ylopg=0.37*yupg
        yloupg=0.45*yupg
        ypospg=0.47*yupg
       else
        if(ipgtgrk.eq.-1 .or. .not. donum) then
c         no symbols above tick marks, so move them up
          yupg=0.96*(yhpg-ympg)+ympg
         else
          yupg=yuptkpg*(yhpg-ympg)+ympg
        end if
        ylopg=yupg-ylentkpg*(yhpg-ympg)
      endif
*
      do j=1,nsys
        nnzzz=(j-1)*noppsys+nppzed
*       redshift
        zzzz=parm(nnzzz)
        lby='  '
        lzy='    '
*
        do i=1,nz
*
          if(lbz(i).eq.ion(j).and.lzz(i).eq.level(j)) then
*           line counter for above tick:
*
            if(lby.eq.lbz(i).and.lzy.eq.lzz(i)) then
              iline=iline+1
             else
              iline=1
              lby=lbz(i)
              lzy=lzz(i)
            end if
*
*           see if the wavelength is in the range:
            wx=(zzzz+1.0d0)*alm(i)
*
            if(wx.ge.fl.and.wx.le.fin) then
*             draw tick mark & info
              xxpg(1)=real(wx)
*             correct to velocities if required:
              if(lvel .and. wcenpg.gt.0.0 ) then
                xxpg(1)=(xxpg(1)/wcenpg-1.0)*ccvacpg
*               branch out if tick not in velocity range
                if(xxpg(1).lt.xmpg.or.
     :               xxpg(1).gt.xhpg) goto 931
              end if
              xxpg(2)=xxpg(1)
*             position of tick depends on other info to go in
              if(lmarkl) then
*               tick, ID, z, tick
*               ticks first
                call pgmove(xxpg(1),ylopg)
                call pgdraw(xxpg(1),yloupg)
                call pgmove(xxpg(1),yulopg)
                call pgdraw(xxpg(1),yupg)
*               line ID
                itemp=int(alm(i))
                write(chstr,'(i5)') itemp
                inchstr=lbz(i)//lzz(i)//chstr(1:5)
                write(chstr,'(f10.6)') zzzz
                inchstr=inchstr(1:11)//chstr(1:10)
                tchpg=xxpg(1)+schxpg*(xhxpg-xlxpg)/112.0
                call pgptxt(tchpg,ypospg,90.0,0.0,inchstr(1:21))
               else
                yypg(1)=yupg
                yypg(2)=ylopg
                call pgline(2,xxpg,yypg)
c               identify line by number in list, or, if ipgtgrk=1, greek letter
                xpg=xxpg(1)
                if(ipgtgrk.ne.-1) then
c               numbers over tick marks
                  if(ipgtgrk.ne.1.or.iline.gt.16.or.
     1                  (lby.ne.'H '.and.lby.ne.'D ')) then
                    ic='   '
                    if(iline.lt.10) then
                      write(ic(1:1),'(i1)',iostat=ios) iline
                      nchic=1
                     else
                      if(iline.lt.100) then
                        write(ic(1:2),'(i2)',iostat=ios) iline
                        nchic=2
                       else
                        write(ic,'(i3)',iostat=ios) iline
                        nchic=3
                      end if
                    end if
                    if(ios.ne.0) then
                      ic='  '
                      nchic=1
                    end if
                   else
                    if(iline.eq.1) ic=chbs(1:1)//'ga'
                    if(iline.eq.2) ic=chbs(1:1)//'gb'
                    if(iline.eq.3) ic=chbs(1:1)//'gg'
                    if(iline.eq.4) ic=chbs(1:1)//'gd'
                    if(iline.eq.5) ic=chbs(1:1)//'ge'
                    if(iline.eq.6) ic=chbs(1:1)//'gz'
                    if(iline.eq.7) ic=chbs(1:1)//'gy'
                    if(iline.eq.8) ic=chbs(1:1)//'gh'
                    if(iline.eq.9) ic=chbs(1:1)//'gi'
                    if(iline.eq.10) ic=chbs(1:1)//'gk'
                    if(iline.eq.11) ic=chbs(1:1)//'gl'
                    if(iline.eq.12) ic=chbs(1:1)//'gm'
                    if(iline.eq.13) ic=chbs(1:1)//'gn'
                    if(iline.eq.14) ic=chbs(1:1)//'gc'
                    if(iline.eq.15) ic=chbs(1:1)//'go'
                    if(iline.eq.16) ic=chbs(1:1)//'gp'
                    nchic=3
                  end if
                  ypg=yupg+0.01*(yhpg-ympg)
                  if ( donum ) then
*                   rfc 7.5.95 removed annoying offset between label and tick
                    call pgptext(xpg,ypg,0.,.5,ic(1:nchic))
                  end if
                  ic='   '
                  nchic=1
                  if(j.lt.10) then
                    write(ic(1:1),'(i1)',iostat=ios) j
                    nchic=1
                   else
                    if(j.lt.100) then
                      write(ic(1:2),'(i2)',iostat=ios) j
                      nchic=2
                     else
                      write(ic,'(i3)',iostat=ios) j
                      nchic=3
                    end if
                  end if
                  if(ios.ne.0) ic='  '
                  ypg=yupg+0.5*(1.0-yuptkpg)*(yhpg-ympg)
                  if(donum) call pgptext(xpg,ypg,0.,0.5,ic(1:nchic))
*
                end if
              end if
*
            end if
*
*           label for velocity range breakout
 931        continue
*
          end if
*
        end do
*
      end do
*
      return
      end
