*	sort a fort.26 table, or set of tables, optionally omitting
*	wavelength region information and selected special line sets.
*
*	uses rd_gprof routine, with spvoigt call replaced by sort 
*
*	array sizes file
        include 'vp_sizes.f'
*
	logical llist
*	free input format stuff
	character*132 chstr,outfile
	character*2 indcol,indb,indz
*
*	local variables
	character*4 chlnkv
	logical laonly
	logical lnosp
	character*2 ion
	character*4 level
*
*	sort variables (sort uses double precision, so redshift dble)
	character*2 elmval(100000)
	character*4 ionval(100000)
	double precision zdeeval(100000)
	real colval(100000),bdopval(100000)
	real csigval(100000),zsigval(100000),bsigval(100000)
	character*2 indcval(100000),indbval(100000),indzval(100000)
*	pointers for sorted array
	integer*4 ipntval(100000)
*	printout select variables:
	logical indtied
	character*2 chcol,chzval,chbval
*
*	error estimates held in common, in case they are wanted
	common/vpc_insigest/colsig,zeesig,bvsig
*
*	regions
	integer kalow(maxpreg),kahiw(maxpreg),nreg
	double precision wvlo(maxpreg),wvhi(maxpreg)
	common/rdc_regions/wvlo,wvhi,kalow,kahiw,nreg
*
	character*2 lbz
	character*4 lzz
	double precision alm,fik,asm 
	common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),nz
*	scalefactors for column density
	integer indvar
	double precision scalelog,scalefac
	common/vpc_varstyle/scalelog,scalefac,indvar
*
*	common variables
	character*132 inchstr
	character*60 cvstr(24)
	real rvstr(24)
	integer ivstr(24)
        integer nvstr
	common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
	double precision dvstr(24)
	common/vpc_dsepspace/dvstr
*	Lyman limit variables
	double precision wlsmin,vblstar,collsmin
	common/vpc_lycont/wlsmin,vblstar,collsmin
*	line list variables
	double precision zmark
	character*132 chmarkfl
	logical lmarkl,lmarkto
	common/rdc_lmarklv/zmark,chmarkfl,lmarkl,lmarkto
*
*	only add lines to one region, so:
	ichunk=1
*	need to set wavelength coefficients so that vp_wval
*	will give the wavelength -- since it is the routine 
*	used by pvoigt.
*
*
*	Default limits:
	collo=0.0
	colhi=1.0e25
	zedlo=-1000.
	zedhi=1.0e25
	bvallo=0.0
	bvalhi=1.0e25
	bvall=0.0
*	counter for number of regions dealt with
	nreg=0
	laonly=.false.
*	ignore special elements (<>, __, >>)
	lnosp=.true.
*	print tied indicators 
	indtied=.true.
*	line counters:
	nchyd=0
	ncall=0
*
	if(nz.le.0) call vp_ewred(0)
*
	write(6,*) 'output file name?'
	read(5,*) outfile
	kout=6
	if(outfile(1:8).ne.'        ') then
	  open(unit=17,file=outfile,status='unknown',err=9998)
	  kout=17
	end if
9998	write(6,*) 'ion,level,col,bval,zed?    ..  or ...'
*	list indicator (.true. if filename contains list of files)
	llist=.false.
	write(6,*) '<filename> (fmt,clo,chi,zlo,zhi,blo,bhi,binc,laonly)'
	write(6,*) '              [26,everything,everywhere]'
	read(5,'(a)') chstr
*	store for use as source of tick marks
	chmarkfl=chstr
*	attempt to split this
	call dsepvar(chstr,15,dvstr,ivstr,cvstr,nvstr)
*	check that file does not contain a list of filenames!
	if(cvstr(2)(1:4).eq.'list') then
	  llist=.true.
	  open(unit=20,file=cvstr(1),status='old',err=9998)
	end if
979	continue
	if(llist) then
	  read(20,'(a)',end=980) cvstr(1)
	end if
	open(unit=13,file=cvstr(1),status='old',err=997)
*	could open the file, so use it
	write(6,*) 'File opened: ',cvstr(1)(1:20)
*	check for type:
	if(ivstr(2).eq.13) then
	  write(6,*) 'fort.13 style file'
	  ifst=13
	  nstar=0
	 else
	  write(6,*) 'fort.26 style file'
	  ifst=26
	end if
*	further control variables may be present:
	if(nvstr.gt.2) then
*	  minimum (log) column density for inclusion
	  if(dvstr(3).gt.0.0) collo=dvstr(3)
*	  and maximum
	  if(dvstr(4).gt.0.0) colhi=dvstr(4)
*	  minimum redshift included
	  if(dvstr(5).gt.0.0) zedlo=dvstr(5)
*	  and maximum
	  if(dvstr(6).gt.0.0) zedhi=dvstr(6)
*	  minimum b value included
	  if(dvstr(7).gt.0.0) bvallo=dvstr(7)
*	  and maximum
	  if(dvstr(8).gt.0.0) bvalhi=dvstr(8)
*	  with the opportunity of including every line if b < bvall
	  if(dvstr(9).gt.0.0) bvall=dvstr(9)
	  if(cvstr(10)(1:1).ne.' ') then
*	    how many characters?
	    nccv=lastchpos(cvstr(10))
	    j=0
	    do while (j.lt.nccv)
	      j=j+1
	      if(cvstr(10)(j:j).eq.'l'.or.cvstr(10)(j:j).eq.'L') then
*	        Ly-a only in regions specified (to speed things up)
	        laonly=.true.
*	        need to suppress Lyman limit absorption with this option,
*	        so reset the internal parameter for this
	        collsmin=1.0e25
	      end if
	      if(cvstr(10)(j:j).eq.'i'.or.cvstr(10)(j:j).eq.'I') then
*	        ignore special characters flag
	        lnosp=.true.
	      end if
	      if(cvstr(10)(j:j).eq.'s'.or.cvstr(10)(j:j).eq.'S') then
*	        ignore special characters flag
	        lnosp=.false.
	      end if
	      if(cvstr(10)(j:j).eq.'t'.or.cvstr(10)(j:j).eq.'T') then
*	        tied indicators printed
	        indtied=.true.
	      end if
	      if(cvstr(10)(j:j).eq.'x'.or.cvstr(10)(j:j).eq.'X') then
*	        tied indicators not printed
	        indtied=.false.
	      end if
	    end do
	  end if
	end if
801	read(13,'(a)',end=9997) chstr
	if(chstr(1:1).eq.'!') goto 801
	jpin=13
*	special bit for fort.13 filenames:
	call dsepvar(chstr,1,dvstr,ivstr,cvstr,nvstr)
	if(ifst.eq.13.and.nstar.eq.0.and.cvstr(1)(1:1).eq.'*') then
	  nstar=nstar+1
909	  read(13,'(a)',end=9997) chstr
	  if(chstr(1:1).eq.'!') goto 909
	  call dsepvar(chstr,15,dvstr,ivstr,cvstr,nvstr)
	  if(cvstr(1)(1:1).ne.'*'.and.nreg.lt.1500) then
	    nreg=nreg+1
	    wvlo(nreg)=dvstr(3)
	    wvhi(nreg)=dvstr(4)
*	    write(6,'(a)') chstr(1:60)
	    goto 909
	  end if
	  nstar=nstar+1
	end if
	goto 991
*	type in line parameters
997	jpin=5
	write(6,*) 'Terminates on <CR>'
991	continue
	do while (chstr.ne.' ')
*	  long process, so just to monitor progress:
*	  if(jpin.eq.13) write(6,'(a)') chstr(1:60)
*
	  if(jpin.eq.5) then
	    call vp_initval(chstr,ion,level,wavel,col,indcol,bval,
     1               indb,zed,indz)
	   else
	    inchstr=chstr
	    call dsepvar(inchstr,15,dvstr,ivstr,cvstr,nvstr)
	    if(cvstr(1)(1:2).eq.'%%') then
*	      is filename, order number, wavelength limits
	      if(nreg.eq.1500) then
*	        if this happens, message is worth repeating!
	        write(6,*) 'Exhausted region space'
	        write(6,*) ' .. no more copied'
	       else
	        nreg=nreg+1
	        wvlo(nreg)=dvstr(4)
	        wvhi(nreg)=dvstr(5)
*	        write(6,*) nreg,'  ',wvlo(nreg),' < lambda <',wvhi(nreg)
	      end if
	      goto 996
	     else
	      call vp_f13fin(ion,level,col,indcol,zed,indz,bval,indb,
     :               vtb,teff,lnk,chlnkv,ierr,ifst)
*	      internal checks are logs, so..
	      if(col.gt.35.0) col=log10(col)
	      if(ierr.ne.0) then
	        write(6,*) ' Unrecognized format:',chstr(1:40),'....'
	        goto 996
	      end if
	    end if
	  end if
*	  see if 'no special' flag set, and if it is, and the ion is
*	  a reserved one, skip it
	  if(lnosp) then
	    if(ion.eq.'<>'.or.ion.eq.'__'.or.ion.eq.'>>') goto 996
	  end if
*	  if not HI, put the line in anyway
	  if(ion.ne.'H ') goto 981
*	  check the parameters are within the limits, and skip if they are not
	  if(bval.ge.bvall.and.(zed.lt.zedlo.or.zed.gt.zedhi.or.
     :       bval.lt.bvallo.or.bval.gt.bvalhi.or.col.lt.collo.or.
     :       col.gt.colhi)) goto 996
 
*
*	  put info in sort arrays, unless excluded
981	  ncall=ncall+1
	  if(ion.eq.'H ') nchyd=nchyd+1
	  elmval(ncall)=ion
	  ionval(ncall)=level
	  zdeeval(ncall)=dble(zed)
	  colval(ncall)=col
	  bdopval(ncall)=bval
*	  tied indicators, if wanted, should go here
	  indcval(ncall)=indcol
	  indbval(ncall)=indb
	  indzval(ncall)=indz
	  csigval(ncall)=colsig
	  zsigval(ncall)=zeesig
	  bsigval(ncall)=bvsig
*
*	  more data?
996	  read(jpin,'(a)',end=998) chstr
	  if(chstr(1:1).eq.'!') goto 996
	  goto 990
998	  chstr=' '
990	  continue
	end do
*	close file if open
	if(jpin.ne.5) close(unit=jpin)
	if(llist) then
	  goto 979
	end if
978	write(6,*) ncall,' ions, ',nchyd,' HI'
*	sort the data into redshift order, index sort
	call pda_qsiad(ncall,zdeeval,ipntval)
	if(kout.ne.6) then
	  write(6,*) 'sorted data to ',outfile(1:20)
	end if
	do i=1,ncall
	  j=ipntval(i)
	  if(indtied) then
	    chcol=indcval(j)
	    chzval=indzval(j)
	    chbval=indbval(j)
	   else
	    chcol='  '
	    chzval='  '
	    chbval='  '
	  end if
*	  fort.26 format write:
	  write(kout,
     :    '(2x,a2,a4,f10.6,a2,f9.6,f8.2,a2,f7.2,f8.3,a2,f7.3)') 
     :    elmval(j),ionval(j),zdeeval(j),chzval,zsigval(j),
     :    bdopval(j),chbval,bsigval(j),colval(j),chcol,csigval(j)
	end do  
	stop
9997	write(6,*)' Empty parameter file'
	goto 9998
980	write(6,*) 'End of file list'
	close(unit=20)
	goto 978
	end
