      subroutine rd_gprof(ca,cad,re,nct,lsmth)
*
*     Put model absorption profiles into the continuum array
*     IN:
*     ca(nct)    dble array  continuum
*     cad(nct)   dble array  continuum copy
*     re(nct)    dble array  workspace
*     nct        integer     array length used
*     lsmth      logical     false: don't smooth, otherwise do.
* 
      implicit none
*     array sizes file
      include 'vp_sizes.f'
*
*     returned array is input continuum
      integer nct
      double precision ca(nct)
      double precision cad(nct),re(nct)
*     smoothing by instrument profile?
      logical lsmth
*
      logical llist,lsums,lsums1,lsumprev
*     free input format stuff
      character*132 chstr
      character*2 indcol,indb,indz
*
*     variables for setting options
      logical laonly
      logical lnosp,lnomet,lconly,lcremove,lchion,lnohyd
      logical lallmetz
      character*2 chpelm
      character*4 chpion
*     LOCAL variables
      character*4 chlnkv
      character*2 ion,ionsh
      character*4 level,levelsh
      character*2 chsumcol
      integer i,ichunk,ifst,ierr,j,jjj,jpin
      integer ichunksh
      integer klow,khiw,klowsh,khiwsh,lnk,nstar
      integer nchyd,ncall,nccv
      double precision collo,colhi,zedlo,zedhi
      double precision bvallo,bvalhi,bvall
      double precision bval,col,wavel,zed
      double precision bvalsh,colsh,zedsh,sumcol
      double precision teff,vtb
      double precision dtemp,temp,xlamx
*     functions
      logical nocgt,lcase
      integer lastchpos
*
*     COMMON variables
*     resolution previous values:
      double precision sigmah,dlamh
      common/rdc_poldres/sigmah,dlamh
*
*     fitted regions (each of the %% lines in fort.26 style)
      integer nreg
      integer kalow(maxpreg),kahiw(maxpreg)
      double precision wvlo(maxpreg),wvhi(maxpreg)
      common/rdc_regions/wvlo,wvhi,kalow,kahiw,nreg
*
      integer nz
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),nz
*     scalefactors for column density
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
*
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
      double precision dvstr(24)
      common/vpc_dsepspace/dvstr
*     Lyman limit variables
      double precision wlsmin,vblstar,collsmin
      common/vpc_lycont/wlsmin,vblstar,collsmin
*     line list variables
      double precision zmark
      character*132 chmarkfl
      logical lmarkl,lmarkto
      common/rdc_lmarklv/zmark,chmarkfl,lmarkl,lmarkto
*     fit to resolution, using sigmas
      integer nswres(maxnch)
      double precision swres(maxpoc,maxnch)
      common/vpc_sigwres/swres,nswres
*     input streams
      integer inputc
      common/rdc_inputc/inputc
*     character collating sequence limits
      integer nchlims
      common/vpc_charlims/nchlims(6)
      character*2 lastch,firstch
      common/vpc_ssetup/lastch,firstch
*     monitoring variable
      integer ncallset
      common/rdc_ncallset/ncallset
*
*     only add lines to one region, so:
      ichunk=1
*     summed column quantities set to zero initially
      bvalsh=0.0d0
      colsh=0.0d0
      zedsh=0.0d0
      ichunksh=1
*     not a summed column density initially
      lsums=.false.
      lsums1=.false.
      lsumprev=.false.
      chsumcol='  '
*     need to set wavelength coefficients so that vp_wval
*     will give the wavelength -- since it is the routine 
*     used by pvoigt.
*
*     add in the lines
*     ca comes out, cad,re go in
*     
*     Default limits:
      collo=0.0d0
      colhi=1.0d25
      zedlo=-1000.0d0
      zedhi=1.0d25
      bvallo=0.0d0
      bvalhi=1.0d25
      bvall=0.0d0
*     counter for number of regions dealt with
      nreg=0
*     steering variables, modified by character variable 10 in list
      lnohyd=.false.
      lnomet=.false.
      laonly=.false.
      lnosp=.false.
      lconly=.false.
      lcremove=.false.
      lchion=.false.
*     put in all metals independent of z
      lallmetz=.true.
*     line counters:
      nchyd=0
      ncall=0
*     absorption default
      indvar=1
*
      if(nz.le.0) call vp_ewred(0)
 9998 write(6,*) 'ion,col,bval,zed?    ..  or ...'
*     list indicator (.true. if filename contains list of files)
      llist=.false.
      write(6,*) '<filename> (fmt,clo,chi,zlo,zhi,blo,bhi,binc,type)'
      write(6,*) '              [26,everything,everywhere]'
*     if input redirected, use it
      read(inputc,'(a)',end=9981) chstr
      if(chstr(1:1).eq.'?') then
        call rd_gphelp
        goto 9998
      end if
*     store for use as source of tick marks
      chmarkfl=chstr
*     attempt to split this
      call dsepvar(chstr,15,dvstr,ivstr,cvstr,nvstr)
*     check that file does not contain a list of filenames!
      if(cvstr(2)(1:4).eq.'list') then
        llist=.true.
        open(unit=20,file=cvstr(1),status='old',err=9998)
       else
        llist=.false.
      end if
 979  continue
      if(llist) then
        read(20,'(a)',end=980) cvstr(1)
      end if
      open(unit=13,file=cvstr(1),status='old',err=997)
*     could open the file, so use it
      write(6,*) 'File opened: ',cvstr(1)(1:20)
      nstar=0
*     check for type:
      if(ivstr(2).eq.13) then
        write(6,*) 'fort.13 style file'
        ifst=13
       else
        write(6,*) 'fort.26 style file'
        ifst=26
      end if
*     further control variables may be present:
      if(nvstr.ge.2) then
*	minimum (log) column density for inclusion
        if(dvstr(3).gt.0.0d0) collo=dvstr(3)
*	and maximum
        if(dvstr(4).gt.0.0d0) colhi=dvstr(4)
*	minimum redshift included
        if(dvstr(5).gt.0.0d0) zedlo=dvstr(5)
*	and maximum
        if(dvstr(6).gt.0.0d0) zedhi=dvstr(6)
*	minimum b value included
        if(dvstr(7).gt.0.0d0) bvallo=dvstr(7)
*	and maximum
        if(dvstr(8).gt.0.0d0) bvalhi=dvstr(8)
*	with the opportunity of including every line if b < bvall
        if(dvstr(9).gt.0.0d0) bvall=dvstr(9)
*	line selection flags
        jjj=10
        do while(jjj.gt.1.and.ivstr(jjj).eq.0.and.
     :             cvstr(jjj)(1:1).eq.' ')
          jjj=jjj-1
        end do
        write(6,*) 'Preset continuum flags: ',cvstr(jjj)(1:4)
        if(cvstr(jjj)(1:1).ne.' ') then
*         how many characters?
          nccv=lastchpos(cvstr(jjj))
          j=0
          do while (j.lt.nccv)
            j=j+1
            if(cvstr(jjj)(j:j).eq.'c'.or.cvstr(jjj)(j:j).eq.'C') then
*	      plot ONLY the continuum adjustments
              lconly=.true.
            end if
            if(cvstr(jjj)(j:j).eq.'d'.or.cvstr(jjj)(j:j).eq.'D') then
*	      plot ALL BUT the continuum adjustments
              lcremove=.true.
            end if
            if(cvstr(jjj)(j:j).eq.'i'.or.cvstr(jjj)(j:j).eq.'I') then
*	      ignore special characters flag
              lnosp=.true.
            end if
            if(cvstr(jjj)(j:j).eq.'l'.or.cvstr(jjj)(j:j).eq.'L') then
*	      Ly-a only in regions specified (to speed things up)
              laonly=.true.
*	      need to suppress Lyman limit absorption with this option,
*	      so reset the internal parameter for this
              collsmin=1.0d25
            end if
            if(cvstr(jjj)(j:j).eq.'m'.or.cvstr(jjj)(j:j).eq.'M') then
*	      metals only flag
              lnohyd=.true.
            end if
            if(cvstr(jjj)(j:j).eq.'n'.or.cvstr(jjj)(j:j).eq.'N') then
*	      ignore metals flag
              lnomet=.true.
            end if
            if(cvstr(jjj)(j:j).eq.'s'.or.cvstr(jjj)(j:j).eq.'S') then
*	      choose a particular species
              lchion=.true.
*	      ask which one:
              write(6,*) 'Which ion?'
              read(5,'(a)') inchstr
              if(inchstr(1:1).eq.' ') then
*	        turn it off again!
                lchion=.false.
               else
*               determine the element 
*               nchlims used, with HD, CO exceptions allowed for
                if(ichar(inchstr(2:2)).ge.nchlims(3).and.
     :             ichar(inchstr(2:2)).le.nchlims(4).and.
     :             inchstr(1:2).ne.'HD'.and.
     :             inchstr(1:2).ne.'CO') then
                  chpelm=inchstr(1:1)//' '
                  chpion=inchstr(2:5)
                 else
                  chpelm=inchstr(1:2)
                  chpion=inchstr(3:6)
                end if
              end if
            end if
            if(cvstr(jjj)(j:j).eq.'z'.or.cvstr(jjj)(j:j).eq.'Z') then
*	      everything strictly selected by redshift
              lallmetz=.false.
            end if
            if(cvstr(jjj)(j:j).eq.'e'.or.cvstr(jjj)(j:j).eq.'E') then
*	      use emission lines only
              write(6,*) 'Emission lines'
              indvar=-1
            end if
          end do
        end if
      end if
*     read in from the file
 801  read(13,'(a)',end=9997) chstr
      if(chstr(1:1).eq.'!') goto 801
*     strip comments
      call vp_stripcmt(chstr,'!')
      jpin=13
*     special bit for fort.13 filenames:
      call dsepvar(chstr,1,dvstr,ivstr,cvstr,nvstr)
      if(ifst.eq.13.and.nstar.eq.0.and.cvstr(1)(1:1).eq.'*') then
        nstar=nstar+1
 909    read(13,'(a)',end=9997) chstr
        if(chstr(1:1).eq.'!') goto 909
        call dsepvar(chstr,15,dvstr,ivstr,cvstr,nvstr)
        if(cvstr(1)(1:1).ne.'*'.and.nreg.lt.1500) then
          nreg=nreg+1
          wvlo(nreg)=dvstr(3)
          wvhi(nreg)=dvstr(4)
*         channel numbers for region limits
          dtemp=dble(nct/2)
          call chanwav(wvlo(nreg),dtemp,5.0d-2,20)
          kalow(nreg)=int(dtemp+1.0d0)
          call chanwav(wvhi(nreg),dtemp,5.0d-2,20)
          kahiw(nreg)=int(dtemp)
          write(6,'(a)') chstr(1:60)
          goto 909
        end if
        nstar=nstar+1
      end if
      goto 991
*     type in line parameters
 997  jpin=5
      write(6,*) 'Terminates on <CR>'
 991  continue
      do while (chstr.ne.' ')
*	long process, so just to monitor progress:
        if(ncallset.gt.0) then
          if(jpin.eq.13.and.(ncall/ncallset)*ncallset.eq.ncall.and.
     :       ncall.gt.0.and.chstr(2:2).ne.'%') then
            write(6,'(i4,2x,a)') ncall,chstr(1:60)
          end if
         else
          write(6,'(i4,2x,a)') ncall,chstr(1:60)
        end if
*
        if(jpin.eq.5) then
          call vp_initval(chstr,ion,level,wavel,col,indcol,bval,
     1               indb,zed,indz)
         else
          inchstr=chstr
          call dsepvar(inchstr,15,dvstr,ivstr,cvstr,nvstr)
          if(cvstr(1)(1:2).eq.'%%') then
*	    is filename, order number, wavelength limits
            if(nreg.eq.1500) then
*	      if this happens, message is worth repeating!
              write(6,*) 'Exhausted region space'
              write(6,*) ' .. no more copied'
             else
              nreg=nreg+1
              wvlo(nreg)=dvstr(4)
              wvhi(nreg)=dvstr(5)
*             channel numbers for region limits
              dtemp=dble(nct/2)
              call chanwav(wvlo(nreg),dtemp,5.0d-2,20)
              kalow(nreg)=int(dtemp+1.0d0)
              call chanwav(wvhi(nreg),dtemp,5.0d-2,20)
              kahiw(nreg)=int(dtemp)
            end if
            goto 996
           else
*           uncomment if want to check if gets here:
            if(ncallset.le.0) write(6,*) col,zed,bval
*           unscramble common character string in cvstr etc
            call vp_f13fin(ion,level,col,indcol,zed,indz,bval,indb,
     :               vtb,teff,lnk,chlnkv,ierr,ifst)
*	    internal checks are logs, so..
            if(col.gt.35.0.and.indvar.ge.0) col=log10(col)
*           special summed column densities need to be unravelled
*           but not if zero level, continuum or velocity shift
            if((indcol(1:1).eq.'%'.or.(lcase(indcol(1:1)).and.
     :                     nocgt(indcol(1:1),lastch)))
     :           .and.indcol.ne.chsumcol.and.(.not.lsums).and.
     :           ion.ne.'__'.and.
     :           ion.ne.'<>'.and.ion.ne.'>>') then
*             is a summed column density - hold this one until
*             block completed, then subtract off the sum of the rest
*             of the block
*             was the previous one the end of a sum??
              write(6,*) 'Summed column density',col
              lsums=.true.
              lsums1=.true.
              bvalsh=bval
              colsh=col
              zedsh=zed
              ionsh=ion
              levelsh=level
              ichunksh=ichunk
              klowsh=klow
              khiwsh=khiw
              sumcol=0.0
              lsumprev=.true.
*             chsumcol is lower case letter(s), or '% '. If '% ' 
*             then need to get letter set from the next input line.
              chsumcol=indcol
            end if
            if(ierr.ne.0) then
              write(6,*) ' Unrecognized format:',chstr(1:40),'....'
              goto 996
            end if
          end if
        end if
*
*       continuum only? If so, skip if not '<>'
        if(lconly.and.(ion.ne.'<>')) goto 996
*       Or let through all which is not continuum:
        if(lcremove.and.(ion.eq.'<>')) goto 996
*
*       see if 'no special' flag set, and if it is, and the ion is
*       a reserved one, skip it
        if(lnosp) then
          if(ion.eq.'<>'.or.ion.eq.'__') goto 996
        end if
*       If hydrogen to be skipped, do so
        if(ion.eq.'H '.and.lnohyd) goto 996
*       If metals to be skipped, do so:
        if(ion.ne.'H '.and.ion.ne.'<>'.and.ion.ne.'__'.and.
     :        lnomet) goto 996
*
*       if reaches here, and not HI, put the line in anyway
        if(ion.ne.'H '.and.lallmetz) goto 981
*       check the parameters are within the limits, and skip if they are not
        if(bval.ge.bvall.and.(zed.lt.zedlo.or.zed.gt.zedhi.or.
     :       bval.lt.bvallo.or.bval.gt.bvalhi.or.col.lt.collo.or.
     :       col.gt.colhi)) goto 996
 
*       limits for range
 981    continue
*       skip if particular ion flag set, and ion not the one
        if(lchion.and.((chpelm.ne.ion).or.(chpion.ne.level))) goto 996
        klow=1
        khiw=nct
        if(ion.eq.'<>'.or.ion.eq.'__'.or.
     :               (ion.eq.'H '.and.laonly)) then
*         apply only to the region containing the "line"
          temp=1215.67d0*(1.0d0+zed)
          dtemp=dble(nct/2)
          do i=1,nreg
            if(temp.gt.wvlo(i).and.temp.lt.wvhi(i)) then
              klow=kalow(i)
              khiw=kahiw(i)
            end if
          end do
        end if
*       write out limits for fitting
*       write(6,*) ion,'  ',klow,khiw
        if(ion.eq.'H ') nchyd=nchyd+1
        ncall=ncall+1
*       uncomment if happy that lines are in
        if(ncallset.le.0) write(6,*) col,zed,bval
        if(lsums1) then
          lsums1=.false.
         else
          call spvoigt(ca,cad,klow,khiw,
     :            col,zed,
     :            bval,ion,level,ichunk,re)
*         copy continuum for next line to go in
          do i=1,nct
            cad(i)=ca(i)
          end do
          if(lsums) then
*           check this is still part of the sum
            if(chsumcol(1:1).eq.'%') then
              chsumcol=indcol
            end if
            if(indcol.eq.chsumcol) then
*             and subtract the current column density from the total
              if(col.lt.25.0d0) then
                dtemp=10.0d0**colsh-10.0d0**col
                if(dtemp.gt.0.0d0) then
                  colsh=log10(dtemp)
                 else
                  colsh=-20.0d0
                end if
               else
                dtemp=colsh-col
                if(dtemp.gt.0.0d0) then
                  colsh=dtemp
                 else
                  colsh=1.0d-20
                end if
              end if
             else
*             this line is not part of the sum, so close down sum part
              lsums=.false.
              call spvoigt(ca,cad,klowsh,khiwsh,
     :            colsh,zedsh,
     :            bvalsh,ionsh,levelsh,ichunksh,re)
*             copy continuum for next line to go in
              do i=1,nct
                cad(i)=ca(i)
              end do
*             if this is the beginning of a new sum, then reset things
              if((indcol(1:1).eq.'%'.or.(lcase(indcol(1:1)).and.
     :                     nocgt(indcol(1:1),lastch)))
     :           .and.indcol.ne.chsumcol.and.(.not.lsums).and.
     :           ion.ne.'__'.and.
     :           ion.ne.'<>'.and.ion.ne.'>>') then
*               is a summed column density - hold this one until
*               block completed, then subtract off the sum of the rest
*               of the block
*               was the previous one the end of a sum??
                write(6,*) 'Summed column density',col
                lsums=.true.
                lsums1=.true.
                bvalsh=bval
                colsh=col
                zedsh=zed
                ionsh=ion
                levelsh=level
                ichunksh=ichunk
                klowsh=klow
                khiwsh=khiw
                sumcol=0.0
*               chsumcol is lower case letter(s), or '% '. If '% ' 
*               then need to get letter set from the next input line.
                chsumcol=indcol
              end if
            end if
          end if
        end if
*       more data?
 996    read(jpin,'(a)',end=998) chstr
        if(chstr(1:1).eq.'!') goto 996
        call vp_stripcmt(chstr,'!')
        goto 990
 998    chstr=' '
 990    continue
      end do
*     close file if open
      if(jpin.ne.5) close(unit=jpin)
      if(llist) then
        goto 979
      end if
*     instrument profile convolution
 978  write(6,*) ncall,' ions, ',nchyd,' HI'
      if(lsmth) then
        dlamh=0.0d0
*       suppress local polynomial fit by setting terms to zero:
        xlamx=5000.d0
        swres(1,ichunk)=0.0d0
        swres(2,ichunk)=0.0d0
        nswres(ichunk)=2
*       get instrument profile if needed:
        call vp_dlset(ichunk,xlamx,sigmah,dlamh)
*
        call vp_spread(ca,1,nct,ichunk, cad)
        do i=1,nct
          ca(i)=cad(i)
        end do
       else
*       don't smooth - just leave continuum as is, and say so
        write(6,*) 'No convolution with instrument profile'
        do i=1,nct
          cad(i)=ca(i)
        end do
      end if
 9980 return
 9981 write(6,*) 'End of input stream - reset to terminal'
      inputc=5
      goto 9980
 9997 write(6,*)' Empty parameter file'
      goto 9998
 980  write(6,*) 'End of file list'
      close(unit=20)
      goto 978
      end
      subroutine rd_gphelp
*     does no more than print out some help details, but
*     not very full ones.
      write(6,*) 'Control characters:'
      write(6,*) 'c - continuum adjustments only'
      write(6,*) 'd - do all but continuum adjustments'
      write(6,*) 'e - emission lines'
      write(6,*) 'i - ignore special (i.e. <>, __, >>)'
      write(6,*) 'l - Ly-a only'
      write(6,*) 'm - metals only'
      write(6,*) 'n - no metals'
      write(6,*) 's - single ion (prompts)'
      write(6,*) 'z - strict z range for everything'
      return
      end
