      subroutine cmax1(dpg,n,l1,l2,cxpg,cnpg)
*     max and min of array d between l1 and l2 inclusive.
*     used by plot routine to set ranges
      integer n,l1,l2
      real cxpg,cnpg,tpg
      real dpg(n)
      cxpg=-1.0e25
      cnpg=1.0e25
      do i=l1,l2
        tpg=dpg(i)
        if(tpg.gt.cxpg) cxpg=tpg
        if(tpg.lt.cnpg) cnpg=tpg
      end do
      return
      end
      subroutine dcmax1(d,n,l1,l2,cx,cn)
*     max and min of array d between l1 and l2 inclusive.
*     used by plot routine to set ranges
      integer n,l1,l2
      double precision cx,cn,t
      double precision d(n)
      cx=-1.0d25
      cn=1.0d25
      do i=l1,l2
        t=d(i)
        if(t.gt.cx) cx=t
        if(t.lt.cn) cn=t
      end do
      return
      end
