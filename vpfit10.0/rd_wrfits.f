      subroutine rd_wrfits(da,de,drms,ca,nct,arg2,arg3,status)
*     write out FITS files
      implicit none
      include 'vp_sizes.f'
*
      integer nct
      double precision da( nct )            ! data array
      double precision de( nct )            ! variance array
      double precision drms(nct)            ! fluctuations array
      double precision ca( nct )            ! continuum array
      character*(*) arg2                    ! file from earlier
      character*(*) arg3
      integer status
*     Local
      integer i,npixel,ngroup
      integer idum,nchlen
      character*64 file         ! output name
      character*32 errmsg
      character*5 chsubs
      double precision dtemp,crpix1,wtemp
      integer psize
      parameter ( psize = 6 )   ! size of wavelength poly
*     FITSIO stuff
      integer unitout
      logical simple,extend
      integer bitpix
      integer naxis,nblock
      integer naxes(2)
      integer fpixel(2),lpixel(2)
*     Functions
      integer lastchpos
      double precision wval
*
      integer nwco
      double precision wcf1(maxwco)
      common/vpc_wavl/wcf1,nwco
      character*80 chstr        ! input arrays
      real rvstr( 5 )
      integer ivstr( 5 ),len(7)
      character*60 cvstr( 5 )
*     common:
      double precision avfac
      common/vpc_airvac/avfac
      character*8 wcftype
      character*4 vacind
      double precision helcfac
      integer noffs
      common/c8_wcftype/wcftype,helcfac,noffs,vacind
*     workspace for double precision data
      double precision ds(maxfis),re(maxfis)
      common/vpc_dblwork/ds,re
*
*     get the file name
      if ( arg2 .ne. ' ') then
        file = arg2
       else
        write ( 6, * ) 'data file name?'
        read ( 5, '(a)', end = 99 ) chstr
        call sepvar( chstr, 1, rvstr, ivstr, cvstr, idum )
        file = cvstr( 1 )
      end if
*     does it have the FITS extension?
      nchlen=lastchpos(file)
      chsubs='     '
      if(nchlen.ge.6) then
        chsubs=file(nchlen-4:nchlen)
      end if
*     add '.fits' if not there already
      if(chsubs.ne.'.fits'.and.chsubs.ne.'.FITS'.and.
     :     nchlen.lt.60) then
        file=file(1:nchlen)//'.fits'
        write(6,*) ' Writing to file: ',file(1:nchlen+5)
      end if
*     set dimensions
      len(1)=nct
      do i=2,7
        len(i)=0
      end do
      naxis=1
*     Get an unused Logical Unit Number to use to open the FITS file.
      status=0
      call ftgiou(unitout,status)
*     create the new image
      nblock=1
      call ftinit(unitout,file,nblock, status)
      if(status.ne.0) then
        call ftgerr(status,errmsg)
        write(6,*) ' Failed to create data file: ',file(1:16)
        write(6,*) ' ',errmsg
        write(6,*) ' does it already exist?'
        goto 99
      end if

*     set up the required header elements
      simple=.true.
      if(arg3(1:4).eq.'real'.or.arg3(1:4).eq.'REAL') then
*       real arrays in output fits file
        bitpix=-32
       else
*       write data as double precision
        bitpix=-64
      end if
*     data lengths
      naxes(1)=nct
      if(arg3(1:2).ne.'UV'.and.arg3(1:2).ne.'uv') then
        naxis=1
        naxes(2)=1
       else
*       UVES_popler style
        naxis=2
        naxes(2)=3
      end if
      extend=.true.
      call ftphpr(unitout,simple,bitpix,naxis,naxes,0,1,extend,status)
*
      if(status.ne.0) then
        call ftgerr(status,errmsg)
        write(6,*) ' header write fail: ',errmsg(1:24)
        goto 99
      end if
*     use arg3 to determine what is written - copied to ds
      if(arg3(1:2).eq.'er'.or.arg3(1:2).eq.'ER'.or.
     :   arg3(1:2).eq.'si'.or.arg3(1:2).eq.'SI') then
*       error array
        do i=1,nct
          if(de(i).gt.0.0d0) then
            ds(i)=sqrt(de(i))
           else
            ds(i)=de(i)
          end if
        end do
       else if(arg3(1:2).eq.'fl'.or.arg3(1:2).eq.'FL'.or.
     :   arg3(1:2).eq.'rm'.or.arg3(1:2).eq.'RM') then
*       rms fluctuation estimate
        do i=1,nct
          if(drms(i).gt.0.0d0) then
            ds(i)=sqrt(drms(i))
           else
            ds(i)=drms(i)
          end if
        end do
       else if(arg3(1:2).eq.'co'.or.arg3(1:2).eq.'CO') then
*       continuum
        do i=1,nct
          ds(i)=ca(i)
        end do
       else
*       anything else by default is data, at least first
        do i=1,nct
          ds(i)=da(i)
        end do
      end if
*     write the array to it
      ngroup=0
      npixel=1
      if(arg3(1:4).eq.'real'.or.arg3(1:4).eq.'REAL') then
*       real arrays in output fits file
        call ftppre(unitout,ngroup,npixel,nct,ds, status)
       else
*       write data as double precision
        if(naxis.eq.1) then
*         one dimensional FITS array
          call ftpprd(unitout,ngroup,npixel,nct,ds, status)
         else
*         first set of a two dimensional array, use
*         FITS subsection input
          fpixel(1)=1
          fpixel(2)=1
          lpixel(1)=nct
          lpixel(2)=1
          status=0
          call ftpssd(unitout,ngroup,naxis,naxes,
     :       fpixel,lpixel,da, status)
        end if
      end if
      if(status.ne.0) then
        call ftgerr(status,errmsg)
        write(6,*) ' data write fail: ',errmsg(1:24)
        goto 99
      end if

*     add important things like where it has come from
      call ftpkyj(unitout,'DISPAXIS',1,' ', status)
      status=0
      call ftpkys(unitout,'REFSPEC1',file(1:10),' ', status)
      status=0
*     vacuum or not?
      if ( avfac .lt. 1.00001d0 ) then
        call ftpkys(unitout,'VACUUM','yes',' ',status)
       else
        call ftpkys(unitout,'VACUUM','no',' ',status)
      end if
      status=0
*
*     wavelength coeffts:
      if(nwco.lt.nct/2) then
*       not a wavelength array
        dtemp=wcf1(1)+wcf1(2)
        if(wcftype(1:3).ne.'log') then
          call ftpkys(unitout,'CTYPE1','linear',' ',status)
        end if
        call ftpkys(unitout,'CUNIT1','Angstroms',' ',status)
        call ftpkyd(unitout,'CRVAL1',dtemp,-14,' ',status)
        call ftpkyd(unitout,'CD1_1',wcf1(2),-14,' ',status)
        call ftpkyd(unitout,'CDELT1',wcf1(2),-14,' ',status)
*       offset if necessary
        if(noffs.ne.0) then
          call ftpkyd(unitout,'LTV1',dble(noffs),-6,' ',status)
        end if
*
        write (6,*) ' Wavelength coefficients to header'
*       linear header fillers:
*       call ftpkyd(unitout,'LTM1_1',1.0d0,-2,' ',status)
        call ftpkyj(unitout,'WCSDIM',1,' ',status)
        crpix1=1.0+dble(noffs)
        call ftpkyd(unitout,'CRPIX1',crpix1,-3,' ',status)
        if(wcftype(1:3).eq.'log') then
          call ftpkyj(unitout,'DC-FLAG',1,
     :    'Wavelengths: Linear=0, Log-linear=1',status)
         else
*         obselete ones commented out
*         call ftpkys(unitout,'APNUM1','1 1 25. 30.',' ',status)
          call ftpkyj(unitout,'DC-FLAG',0,' ',status)
*         call ftpkys(unitout,'WAT0_001','system=equispec',' ',status)
*	  call ftpkys(unitout,'WAT1_001',
*     :      'wtype=linear label=Wavelength units=Angstroms',
*     :      ' ',status)
        end if
       else
        write(6,*) ' wavelength fits file needed'
        write(6,*) ' associate the input one to this new data'
      end if
*
      if(arg3(1:2).eq.'UV') then
*       write out the subset of a UVES_popler file which is
*       read and used by VPFIT
*       extra header items
        call ftpkys(unitout,'CONTFILE','unity','Continuum=1',status)
        wtemp=wval(1.0d0)
        call ftpkyd(unitout,'UP_WLSRT',wtemp,-14,
     :    'First pixel center',status)
        wtemp=wval(dble(nct))
        call ftpkyd(unitout,'UP_WLEND',wtemp,-14,
     :    'Last pixel center',status)
        call ftpkys(unitout,'UP_ARR01','Normalized flux',' ',status)
        call ftpkys(unitout,'UP_ARR02','Normalized error',' ',status)
        call ftpkys(unitout,'UP_ARR03','Norm expected fluctuations'
     :              ,' ',status)
        call ftpkys(unitout,'ARRAY1','Normalized flux',' ',status)
        call ftpkys(unitout,'ARRAY2','Normalized error',' ',status)
        call ftpkys(unitout,'ARRAY3','Norm expected fluctuations'
     :              ,' ',status)
*       other data arrays
*       write error as double precision
        do i=1,nct
          if(de(i).gt.0.0d0) then
            ds(i)=sqrt(de(i))
           else
            ds(i)=de(i)
          end if
        end do
        fpixel(1)=1
        fpixel(2)=2
        lpixel(1)=nct
        lpixel(2)=2
        status=0
        call ftpssd(unitout,ngroup,naxis,naxes,
     :       fpixel,lpixel,ds, status)
        if(status.ne.0) write(6,*) 'Error write fail'
*       same for the fluctuations
        fpixel(1)=1
        fpixel(2)=3
        lpixel(1)=nct
        lpixel(2)=3
        do i=1,nct
          if(drms(i).gt.0.0d0) then
            ds(i)=sqrt(drms(i))
           else
            ds(i)=drms(i)
          end if
        end do
        call ftpssd(unitout,ngroup,naxis,naxes,
     :       fpixel,lpixel,ds, status)
*       notify the user
        nchlen=lastchpos(file)
        write(6,*) ' Wrote UVES_popler file ',file(1:nchlen)
      end if
*     close the FITS file
      call ftclos(unitout,status)
      call ftfiou(unitout,status)
      
 99   return
      end
