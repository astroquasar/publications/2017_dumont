      subroutine vp_minpol( n, x, y, xmin )

      implicit none
      integer n
      double precision x( n ), y( n ), xmin
      double precision x1, x2, y1, y2

*     rfc 21.07.99 replaced somewhat unstable pda_ routine
      call vp_minquad(n,x,y,x1,y1)
*     write(6,*) 'minquad : ',x1,y1
      call minpol2( n, x, y, x2, y2 )
*     write (6, * ) 'minpol 2 : ', x2, y2
      if ( y1 .lt. y2 ) then
        xmin = x1
       else
        xmin = x2
      end if
      return
      end


      subroutine minpol2( n, x, y, xmin, ymin )

*     vp_minquad routine fitted a quadratic to the points obtained - this fails
*     for chisq profiles which are not globally (within the search) quadratic.
*     such profiles do occur, so this routine uses only the 3 points nearest the
*     minimum.

      implicit none

      integer n
      double precision x( n ), y( n )
      double precision xmin, ymin
      double precision x1, x2, x3, y1, y2, y3

      integer ilo, i, ileft, irght

*     first find lowest point and two brackets
      ilo = 1
      do i = 1, n
*        write ( *, * ) i, x(i), y(i)
        if ( y( i ) .lt. y( ilo ) ) ilo = i
      end do
      ileft = 0
      irght = 0
      do i = 1, n
        if ( i .ne. ilo ) then
          if ( x( i ) .lt. x( ilo ) ) then
            if ( ileft .eq. 0 ) then
              ileft = i
            else if ( x( ilo ) - x( i ) .lt. 
     :                x( ilo ) - x( ileft ) ) then
              ileft = i
            end if
          else
            if ( irght .eq. 0 ) then
              irght = i
            else if ( x( i ) - x( ilo ) .lt.
     :                x( irght ) - x( ilo ) ) then
              irght = i
            end if
          end if
        end if
      end do

*     if not bracketed...
      if ( ileft .eq. 0 .or. irght .eq. 0 ) then
        xmin = x( ilo )
        ymin = y( ilo )
*       otherwise quadratic interpolation - from mathematica
      else
        x1 = x( ileft )
        y1 = y( ileft )
        x2 = x( ilo )
        y2 = y( ilo )
        x3 = x( irght )
        y3 = y( irght )
*        ajc 5-jul-93: 
        xmin = x2 - 0.5d0 * (
     :         ( (x2-x1)**2*(y2-y3) - (x2-x3)**2*(y2-y1) ) /
     :         ( (x2-x1)*(y2-y3) - (x2-x3)*(y2-y1) ) )
        ymin = ( ( (xmin-x2)*(xmin-x3) ) / ( (x1-x2)*(x1-x3) ) )*y1 +
     :         ( ( (xmin-x1)*(xmin-x3) ) / ( (x2-x1)*(x2-x3) ) )*y2 +
     :         ( ( (xmin-x1)*(xmin-x2) ) / ( (x3-x1)*(x3-x2) ) )*y3
      end if
      return
      end
