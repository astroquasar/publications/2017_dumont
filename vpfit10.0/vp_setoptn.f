      subroutine vp_setoptn(nopt,np,icontflg,nopth,indcf,ind,
     1           idiag,ialfind)
      implicit none
      include 'vp_sizes.f'
*
*     parameters
      integer nopt,np,icontflg,nopth,indcf,ind,idiag,ialfind
*     local:
      character*60 cvstrx
      integer i,iskip,jv,l
*
*     free input declarations
*
*     air/vac wavelength parameter
      double precision avfac
      common/vpc_airvac/avfac
*     tied/fixed indicators
      character*2 ipind(maxnpa)
      integer isod,isodh
      common/vpc_usoind/ipind,isod,isodh
*     character workspace
      character*132 inchstr
      character*60 cvstr(24)
      integer ivstr(24)
      real rvstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
      double precision dvstr(24)
      common/vpc_dsepspace/dvstr
*     run numbers requested here, so
*     current data file and order number
      integer idrn,icrn
      character*64 filnm
      common/vpc_file14/filnm,idrn,icrn
*     probability limits for adding extra lines
      double precision chisqvh,probchb,probksb
      integer nladded
      common/vpc_problims/chisqvh,probchb,probksb,nladded
*     add fixed Doppler parameter variables
      double precision bvaladd
      character*2 charbfix
      common/vpc_addbfix/bvaladd,charbfix
*     rejected systems by error limits
      double precision errbmax,errlnmax
      common/vpc_rejsys/errbmax,errlnmax
*     end rejection by column density threshold and error
      double precision clnemin,errlnemax
      common/vpc_endrej/clnemin,errlnemax
*     common flags and variables for line adding routines:
      integer nadflag
      common/vpc_autofl/nadflag(10)
      integer indus
      common/vpc_uscont/indus
*     common array for line fixing
      character*2 ifx
      character*4 lfx
      integer nfx
      common/vpc_fixlist/ifx(10),lfx(10),nfx
*     old data file and parameters
      integer ncumset
      character*132 comchstr
      logical ldate26
      logical lcuminc
      common/vpc_comchstr/comchstr,ldate26,lcuminc,ncumset
*     Upper limit for redshift of Ly-a
      double precision zedqso
      common/vpc_zedqso/zedqso
*     Give detailed commentary?
      logical verbose
      common/vp_sysout/verbose
*     first key word for file input - reset here
      character*32 chkey(maxnch)
      common/vpc_chunkey/chkey
*     printout control variables
      logical lwrsum,lwropen,lerrsum
      character*60 cwrsum
      character*16 chext13
      integer nwrsum,lext13
      common/vpc_f13hout/cwrsum,nwrsum,lwrsum,lwropen,lerrsum,
     :                chext13,lext13
      logical lwr26s,lwr26open
      character*60 cwr26s
      integer nwr26s,ndp26s,len26tem
      common/vpc_f26hout/cwr26s,nwr26s,lwr26s,lwr26open,ndp26s,
     :       len26tem
*     
*     blank any chunk keywords
      do i=1,maxnch
        chkey(i)=' '
      end do
*
*     Set option, and decode option line:
*     Option chosen carried as value 'nopt'
*     1: G: guess line parameters and then fit, interactive
*     2: S: Simulation input, multiple guess
*     3: L: estimate line parameters as HI and run from file region list
*     4: C: leave only continuum and zero level parameters free
*     5: Reserved [Resolve and finish option, when written?]
*     6: I: interactive setup and fit
*     7: F: run from an input file
*     8: Continue as before
*     9: D: display profiles from input file without fitting
*     10:E: display profiles and compute errors from input file
*     This should be a safe default upper limit redshift!
      zedqso=1.0d20
*     Default is to remove ill-conditioned systems only, ignoring
*     error selection criterion
      errbmax=1.0d30
      errlnmax=1.0d30
*
*     nfx=0 means fix all ions (default) for 'C' option
      nfx=0
*     Don't try to read old data
      comchstr=' '
*     initialize autoadd flags and variables
*     nadflag: (1)=0 => nearest, =1 => Lya
*              (2)=0 => if nearest is HI, use Lya =1 => use nearest HI
*     defaults are nearest, and if HI use Ly-a
      do i=1,10
        nadflag(i)=0
      end do
      charbfix='  '
      bvaladd=0.0d0
 1234 continue
*
      write(6,*) ' '
      write(6,*) ' options:   <CR> for previous value'
      write(6,*) ' I - interactive setup and fit'
      write(6,*) ' F - run from an input file'
      write(6,*) ' D - display profiles from input file'
      write(6,*) ' ? for help'
      write(6,*) ' option (key) (key)...'
      write(6,'(''> '',$)')
      read(5,'(a)') inchstr
      call dsepvar(inchstr,15,dvstr,ivstr,cvstr,nvstr)
*     if first character is a '<', then use redirection for this line
*     only
      if(inchstr(1:1).eq.'<') then
        if(cvstr(1)(2:2).ne.' ') then
          cvstrx=cvstr(1)(2:60)
         else
          cvstrx=cvstr(2)
        end if
*       attempt to open file
        open(unit=2,file=cvstrx,status='old',err=933)
        read(2,'(a)') inchstr
        call dsepvar(inchstr,15,dvstr,ivstr,cvstr,nvstr)
        close(unit=2)
      end if
 933  continue          
*
*     character steered options:
*     I: interactive setup and fit
      if(cvstr(1)(1:1).eq.'i'.or.cvstr(1)(1:1).eq.'I') then
        ivstr(1)=6
      end if
*     F: run from an input file
      if(cvstr(1)(1:1).eq.'f'.or.cvstr(1)(1:1).eq.'F') then
        ivstr(1)=7
      end if
*     D: display profiles from input file without fitting
      if(cvstr(1)(1:1).eq.'d'.or.cvstr(1)(1:1).eq.'D') then
        ivstr(1)=9
      end if
*     E: display profiles and compute errors from input file 
*     without fitting (to get upper limits)
      if(cvstr(1)(1:1).eq.'e'.or.cvstr(1)(1:1).eq.'E') then
        ivstr(1)=10
      end if
      if(indus.eq.0) goto 801
*     development and restricted options:
*     S: Simulation input, multiple guess
      if(cvstr(1)(1:1).eq.'s'.or.cvstr(1)(1:1).eq.'S') then
        ivstr(1)=2
      end if
*     C: leave only continuum and zero level parameters free,
*     fixing all atomic values (for analysis of systematics)
      if(cvstr(1)(1:1).eq.'c'.or.cvstr(1)(1:1).eq.'C') then
        ivstr(1)=4
        nfx=0
*       Can add information to this one. At present can be only HI,
*       but could extend this to a list (of 10 in common above)
        if(cvstr(1)(2:3).eq.'HI'.or.cvstr(1)(2:3).eq.'hi') then
          nfx=1
          ifx(1)='H '
          lfx(1)='I   '
        end if
      end if
*     G: guess line parameters and then fit, interactive
      if(cvstr(1)(1:1).eq.'g'.or.cvstr(1)(1:1).eq.'G') then
        ivstr(1)=1
      end if
*     L: estimate line parameters as HI and run from file region list
*     containing data file name and wavelength regions only
      if(cvstr(1)(1:1).eq.'l'.or.cvstr(1)(1:1).eq.'L') then
*       superseded by blank list option with 'F'
        ivstr(1)=3
      end if
*
*     if help is needed:
 801  if(cvstr(1)(1:1).eq.'?'.or.cvstr(1)(1:1).eq.'h'.or.
     :      cvstr(1)(1:1).eq.'H') then
        write(6,*) ' D  Display fit from file'
        write(6,*) ' E  Display and compute errors from file'
        write(6,*) ' F  File start, fit parameters'
        write(6,*) ' I  Interactive start, fit parameters'
        write(6,*) ' G  Provide initial guesses and fit'
        if(indus.gt.0) then
*         development?
          write(6,*) ' S unpredictable'
          write(6,*) 
     :      ' C fit cont/shift from file ions fixed (CHI -HI only)'
*         write(6,*) ' L Use list of regions (maybe)'
        end if
        write(6,*) '  '
        write(6,*) ' Key words checked are:'
        write(6,*) ' add (p) (p)  -  add new lines if needed to'
        write(6,*) '      get an acceptable fit, the values (p)'
        write(6,*) '      are threshold probabilities for CHI^2'
        write(6,*) '      and KS test respectively [0.01,1E-8]'
        write(6,*) '      OR, threshold normalized CHI^2 (if >1)'
        write(6,*) ' Ly-a all added lines are assumed to be Ly-a'
        write(6,*) ' unknown, or ??, all added lines ?? (1215.67)'
        write(6,*) 
     :   ' zup (p1) add Ly-a if its redshift < p1, nearest otherwise'
        write(6,*) ' ecol (p1) (p2) - remove lines at end if'
        write(6,*) '      log col < (p1) AND log col err > (p2)'
        write(6,*) ' fixb (p) - added lines have fixed b [20]'
*        write(6,*) ' remove (p1) (p2)  - remove lines if error in'
*        write(6,*) '      b exceeds (p1) AND error in logN exceeds'
*        write(6,*) '      (p2) [30.,2.]'
        write(6,*) ' diagnostics  -  write diagnostic o/p'
*	  write(6,*) ' vac  use vacuum wavelengths'
*	  write(6,*) ' air  use air wavelengths'
        write(6,*) ' inc  filename - include earlier data'
        write(6,*) 
     :   ' cum  filename - cumulatively include earlier data'
        goto 1234
      end if
*
      nopt=ivstr(1)
*
*     continue as before
      if(cvstr(1)(1:2).eq.'co'.or.cvstr(1)(1:2).eq.'CO') then
        nopt=8
      end if
*
*
*     Keywords on the same line:
*     default keys & related variables
      charbfix=' '
*     idiag=0
      probchb=-1.0d0
      probksb=-1.0d0
      nladded=0
      iskip=0
*
      if(cvstr(2)(1:1).ne.' ') then
        do i=2,nvstr
*
*         check if substring already used
          if(iskip.gt.0) then
            iskip=iskip-1
            goto 951
          end if
*
*         disentangle character input
*         AD:
          if(cvstr(i)(1:2).eq.'ad'.or.cvstr(i)(1:2).eq.'AD') then
*           set up for automatic addition of lines
*           to underfitted blends
            if(nvstr.gt.i) then
*             probability thresholds may follow
              jv=i+1
              probchb=dvstr(jv)
              if(probchb.le.0.0d0) probchb=0.01d0
              if(nvstr.gt.jv.and.dvstr(jv).gt.0.0d0) then
                probksb=dvstr(jv+1)
                if(probksb.le.0.0d0) then
                  if(probchb.gt.1.0d0) then
*	            forget K-S test
                    probksb=0.0d0
                   else
*	            K-S probability low, effectively ignore unless
*	            it is outrageous.
                    probksb=1.0d-8
                  end if
                end if
               else
                if(probchb.gt.1.0d0) then
*	          forget K-S test
                  probksb=0.0d0
                 else
                  probksb=1.0d-8
                end if
              end if
             else
              probchb=0.01d0
              probksb=1.0d-8
            end if
            write(6,'('' '')')
            write(6,'('' Add lines if prob(tot) <'',f9.6,
     1          ''   or prob(KS) <'',f11.8)') probchb,probksb
*           if this is happening, then ill-conditioned matrices
*           are likely, so set ill-conditioned flag, and the reset
*           value
            isod=1
            isodh=1
          end if
*         AI:
          if(cvstr(i)(1:2).eq.'ai'.or.cvstr(i)(1:2).eq.'AI')
     1                               then
*           set common parameters for air wavelengths
            avfac=1.00028d0
            write(6,*) 'Air wavelengths'
          end if
*
*         CU:
          if(cvstr(i)(1:3).eq.'cum'.or.cvstr(i)(1:3).eq.'CUM')
     :        then
*           Put in earlier data, from named file, and append results of
*           each dataset to it cumulatively
            if(nvstr.gt.i) then
              jv=i+1
              comchstr=cvstr(jv)
              iskip=1
              lcuminc=.true.
             else
              comchstr=' '
            end if
            goto 951 
          end if   
*
*         DI:
          if(cvstr(i)(1:2).eq.'di'.or.cvstr(i)(1:2).eq.'DI') 
     1                               then
*	    diagnostic printout needed
            idiag=1
           else
            idiag=0
          end if
*
*         EC:
          if(cvstr(i)(1:2).eq.'ec'.or.cvstr(i)(1:2).eq.'EC') 
     1                               then
*           remove lines if logN small enough and lognerr too large
            if(nvstr.gt.i) then
*	      probability thresholds follow
              jv=i+1
              clnemin=dvstr(jv)
              if(clnemin.le.0.0d0) clnemin=14.5d0
              if(nvstr.gt.jv.and.dvstr(jv).gt.0.0d0) then
                errlnemax=dvstr(jv+1)
                if(errlnemax.le.0.0d0) errlnemax=2.0d0
               else
                errlnemax=2.0d0
              end if
             else
              clnemin=14.5d0
              errlnemax=2.0d0
            end if
            write(6,'('' '')')
            write(6,'('' Try removing lines at end if logN <'',
     :          f7.2,'' and err(logN) >'',f7.2)') clnemin,errlnemax
          end if
*
*         FI:
          if(cvstr(i)(1:2).eq.'fi'.or.cvstr(i)(1:2).eq.'FI')
     1         then
*           fix the doppler parameter for the added Ly-a lines
            charbfix='SB'
            if(dvstr(i+1).gt.0.0d0) then
              bvaladd=dvstr(i+1)
             else
              bvaladd=20.0d0
            end if
          end if
*
*         IL:
          if(cvstr(i)(1:2).eq.'il'.or.cvstr(i)(1:2).eq.'IL')
     :                  then
*           This is the default anyway, so just write a comforting
*           message for anybody who bothers to use this.
            write(6,*) 'Removes ill-conditioned lines'
          end if
*
*         IN:
          if(cvstr(i)(1:3).eq.'inc'.or.cvstr(i)(1:3).eq.'INC')
     :                  then
*	    Put in earlier data, from named file, no accumulation
            if(nvstr.gt.i) then
              jv=i+1
              comchstr=cvstr(jv)
              iskip=1
*             in case this is a recycle through the lot:
              lcuminc=.false.
             else
              comchstr=' '
            end if
            goto 951 
          end if   
*
*         Ly:
          if(cvstr(i)(1:2).eq.'Ly'.or.cvstr(i)(1:2).eq.'ly') then
            nadflag(1)=1
            write(6,*) 'Added lines are Ly-a'
            goto 951
          end if
*
*         RE:
          if(cvstr(i)(1:2).eq.'re'.or.cvstr(i)(1:2).eq.'RE') 
     1                               then
*           remove lines if berr, lognerr too large
            if(nvstr.gt.i) then
*             probability thresholds follow
              jv=i+1
              errbmax=dvstr(jv)
              if(errbmax.le.0.0d0) errbmax=30.0d0
              if(nvstr.gt.jv) then
                errlnmax=dvstr(jv+1)
                if(errlnmax.le.0.0d0) errlnmax=2.0d0
               else
                errlnmax=2.0d0
              end if
             else
              errbmax=30.0d0
              errlnmax=2.0d0
            end if
            write(6,'('' '')')
            write(6,'('' Try removing lines if err(b) >'',f7.2,
     1          ''   and err(logN) >'',f7.2)') errbmax,errlnmax
            write(6,*) 'or if a parameter for a component is'//
     :            ' ill-conditioned'
          end if
*
*         UNknown line, or ??:
          if(cvstr(i)(1:2).eq.'un'.or.cvstr(i)(1:2).eq.'UN'.or.
     :         cvstr(i)(1:2).eq.'??') then
            nadflag(1)=2
            write(6,*) 'Added lines are unknown (i.e. ??)'
            goto 951
          end if
*
*         VA:
          if(cvstr(i)(1:2).eq.'va'.or.cvstr(i)(1:2).eq.'VA') 
     1                               then
            avfac=1.0d0
            write(6,*) ' Vacuum wavelengths'
          end if
*
*	  ZU: Set upper limit redshift
          if(cvstr(i)(1:2).eq.'zu'.or.cvstr(i)(1:2).eq.'ZU') 
     1                               then
            jv=i+1
            if(dvstr(jv).gt.0) then
              zedqso=dvstr(jv)
*             There is no point in doing this if not adding Ly-a below
*             the specified redshift, so set the Ly-a flag
              if(nadflag(1).le.0) then
                nadflag(1)=1
              end if
             else
              zedqso=1.0d20
            end if
          end if
 951      continue
        end do
      end if
*
      if(nopt.gt.0.and.nopt.ne.8) then
*       initialize variables for new loop
        nopth=nopt
        icontflg=0
        idrn=0
        icrn=0
        call vp_close14
        indcf=1
        ind=4
       else
*       continue as before
        if(nopth.le.0) goto 1234
        nopt=nopth
        icontflg=1
      end if
      if(icontflg.ne.1) then
        if(nopt.gt.0.and.verbose) then
*         a reminder which now appears in the instructions for use:
          write (6,*) ' lower case characters following values '//
     :                     'are treated as labels.  if the same'
          write (6,*) ' upper case character follows another '//
     :                  'value then it will be tied to the lower'
          write (6,*) ' case label. '//
     :                     ' isolated (un-paired) upper case '//
     :                     'characters fix values.'
        endif

*       Reset last indicator array
*       RFC 28.10.96 Do this unconditionally 
*       (since won't matter if it is not used)
        if(np.gt.0) then
          do l=1,np
            ipind(l)='  '
          end do
        end if
*
*       set common parameters
        call cvalset(nopt,ialfind)
*       reset the output summary filename for 'E' option
        if(nopt.eq.10) then
*         since displaying errors only, modify root output name:
          cwr26s=cwr26s(1:nwr26s)//'E'
          nwr26s=nwr26s+1
          cwrsum=cwrsum(1:nwrsum)//'E'
          nwrsum=nwrsum+1
        end if
      end if
*
      return
      end
