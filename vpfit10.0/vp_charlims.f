      subroutine vp_charlims
*
*     no arguments, just setting the range of numbers, lower case,
*     and upper case letters and holding in a common block. These
*     numbers are the host processors collating sequence, and are
*     set here so we never have to think about them again. 
*     1: lowest integer,    2: highest integer,
*     3: lowest upper case, 4: highest u.c.,  
*     5: lowest l.c.,       6: highest lower case.
*
*     also sets up common arrays of physical constants for the same
*     reason
* 
      implicit none
      integer j,k
*
*     character collating sequence ranges
      integer nchlims
      common/vpc_charlims/nchlims(6)
*
*     constants
      real ccvacpg
      common/vpc_sgconst/ccvacpg
      double precision ccvac
      common/vpc_dbconst/ccvac
*
      j=ichar('1')
      k=ichar('0')
      nchlims(1)=min(j,k)
      j=ichar('9')
      nchlims(2)=max(j,k)
*     
      nchlims(3)=ichar('A')
      nchlims(4)=ichar('Z')
      nchlims(5)=ichar('a')
      nchlims(6)=ichar('z')
*     write(6,*) 'ICHAR',nchlims
*
      ccvac=2.99792458d5
      ccvacpg=real(ccvac)
      return
      end
