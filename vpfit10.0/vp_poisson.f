      double precision function vp_poisson(n,plambda)
*
*     return the probability of count n from Poisson distribution with
*     mean plambda. If n > TBD, call it a Gaussian [NOT YET DONE].
*
      implicit none
*
      integer n
      double precision plambda
*
*     local
      integer i,nh,nx
      double precision x
*
      x=1.0
      if(n.gt.1) then
        do i=2,n
          x=x*dble(i)
        end do
      end if
*     vague attempt to keep things in range for a bit longer
      nh=n/2
      nx=n-nh
      vp_poisson=((plambda**nh/x)*exp(-plambda))*plambda**nx      
      return
      end
