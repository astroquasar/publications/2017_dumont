      subroutine rd_cuchstr(inchstr,nxxc,nxlen)
*
*     read in character string from cursor window,
*     with a maximum of nxlen characters
*
      implicit none
*
      integer nxxc,nxlen
      character*(*) inchstr
*
*     Local:
      integer ntemp
      real wv1pg,y1pg
      character*1 chc,chcold
      nxxc=0
      chc=' '
      chcold=' '
      inchstr=' '
*     input from cursor window, terminate on <CR>
*     or 'DD' = double center click
      do while (ichar(chc).ne.13.and.nxxc.lt.nxlen.and.
     :            (chc.ne.'D'.and.chcold.ne.'D'))
        chcold=chc
        nxxc=nxxc+1
        call vp_pgcurs(wv1pg,y1pg,chc)
        if(ichar(chc).eq.127.or.ichar(chc).eq.8) then
*	  delete previous character if <DEL> or <BS>
          if(nxxc.ge.2) nxxc=nxxc-2
          if(nxxc.ge.1) then
            write(6,'(/,a,$)') inchstr(1:nxxc)
            call flush(6)
           else
            write(6,*)
          end if
         else
          inchstr(nxxc:nxxc)=chc
          ntemp=ichar(chc)
          if(ntemp.ne.13) then
            write(6,'(a1,$)') chc
            call flush(6)
           else
            write(6,*)
            call flush(6)
          end if
        end if
      end do
      if(ichar(chc).eq.13) then
        nxxc=nxxc-1
        inchstr(nxxc:nxxc)=' '
      end if
      return
      end
