      subroutine vp_getres(name,ichunk,nrow, status)

      implicit none
      include 'vp_sizes.f'
*     IN: 
*     name   ch*(*)  resolution file name
*     ichunk int     current chunk number
*     nrow   int     order number(in file)
*     OUT:
*     status int     status (0=OK)
*
*     Wavelength information written into common blocks below.
*     
      character*(*) name
      integer ichunk, nrow
      integer status
*     local
      integer acunit
      parameter (acunit = 31)
      integer i,j,lenx
      character*80 line,cdum,cvx(2)
      double precision dvx(2)
      integer ivx(2),nvx
      integer ier
      double precision dtemp
*
*     functions
      integer lastchpos
*
*     COMMON:
*
*     binned profile
      character*2 chprof
      double precision wfinstd,pfinst
      integer npinst,npin2
      common/vpc_profwts/wfinstd(maxipp,maxnch),pfinst(maxipp,maxnch),
     :        npinst(maxnch),npin2(maxnch),chprof(maxnch)

      logical verbose
      common/vp_sysout/verbose
*     fit to resolution, using sigmas
      integer nswres(maxnch)
      double precision swres(maxpoc,maxnch)
      common/vpc_sigwres/swres,nswres
*
*
      if (status.ne.0) then
        write (6,*) 'vp_getres called with bad status'
        status = 0
      end if
      ier=0
      open (file=name,err=10,iostat=ier,status='old',unit=acunit)
*     rfc 27.03.01: Allow for pixel resolution files
      read(unit=acunit,err=10,iostat=ier,fmt='(a)') line
      call dsepvar(line,2,dvx,ivx,cvx,nvx)
      if(cvx(1)(1:1).eq.'r'.or.cvx(1)(1:1).eq.'R'.or.
     :     cvx(1)(1:1).eq.'t'.or.cvx(1)(1:1).eq.'T') then
        if(cvx(1)(1:3).eq.'row') then
*	  First line is 'row 1', so is an old style fit to the resolution
*         read past unwanted ones
          do i=1,nrow-1
            do j=1,6
              read(unit=acunit,err=10,iostat=ier, 
     :           fmt =*) swres(j,ichunk)
            end do
            read(unit=acunit,err=10,iostat=ier,fmt='(80a)') line
          end do
*         coefficient readin and rescale to sigmas
          nswres(ichunk)=2
          do j=1,6
            read (unit=acunit,err=10,iostat=ier,
     :                    fmt=*) dtemp
            swres(j,ichunk)=dtemp/2.35482d0
            if(dtemp.ne.0.0d0.and.j.gt.2) then
              nswres(ichunk)=j
            end if
          end do
         else
*        Have a wavelength vs resolution table, so read it to the end
          npinst(ichunk)=0
*         set resolution vs wavelength table indicator
          chprof(ichunk)=cvx(1)(1:2)
          if(verbose) then
            write(6,*) 'chprof',ichunk,'  ',chprof(ichunk)
          end if
 881      read(unit=acunit,end=882,fmt='(a)') line
          call dsepvar(line,2,dvx,ivx,cvx,nvx)
*         blank line also terminates
          if(cvx(1)(1:1).eq.' ') goto 882
*         skip comment lines
          if(cvx(1)(1:1).eq.'!'.or.cvx(1)(1:1).eq.'#') goto 881
          if(npinst(ichunk).ge.maxipp) then
            write(6,*) 'Wavelength/resolution table full '
            write(6,*) ' using first ',npinst(ichunk),' for region',
     :                   ichunk
            goto 882
          end if
          npinst(ichunk)=npinst(ichunk)+1
          if(chprof(ichunk)(1:1).eq.'R'.or.
     :         chprof(ichunk)(1:1).eq.'r') then
*           R (or r) for resolution as a number (FWHM)
            pfinst(npinst(ichunk),ichunk)=1.0d0/(dvx(2)*2.35482d0)
           else
*           t for table of FWHM, convert to dimensionless sigma
            pfinst(npinst(ichunk),ichunk)=dvx(2)/7.0451d5
          end if
          wfinstd(npinst(ichunk),ichunk)=dvx(1)
          goto 881
 882      continue
*         wavelength/resolution table 'wt'
          chprof(ichunk)='wt'
          if(verbose) then
            write(6,*) 'Resolution table has ',npinst(ichunk),
     :                   ' values for region # ',ichunk
            do j=1,min(npinst(ichunk),10)
              write(6,*) j,wfinstd(j,ichunk),pfinst(j,ichunk)
            end do
          end if
        end if
       else
*	Pixel by pixel resolution file, forced for the WHOLE FILE,
*	however many orders.
*	If you don't like it, split up the data or specify on the
*	file/region input line. So interactive mode is more restricted
*	unless you don't specify a resolution file at all.
        chprof(ichunk)=' '
        if(nvx.eq.2) then
          pfinst(1,ichunk)=dvx(2)
         else
          pfinst(1,ichunk)=dvx(1)
        end if
        npinst(ichunk)=1
*	read the rest of the file until the end
 790    read(unit=acunit,fmt='(a)',end=791) line
        call dsepvar(line,2,dvx,ivx,cvx,nvx)
        npinst(ichunk)=npinst(ichunk)+1
        if(nvx.eq.2) then
          pfinst(npinst(ichunk),ichunk)=dvx(2)
         else
          pfinst(npinst(ichunk),ichunk)=dvx(1)
        end if
        goto 790
 791    npin2(ichunk)=npinst(ichunk)/2+1
      end if
*     close the resolution data file
      close(unit=acunit)
*
 10   continue
*     
      if (ier .ne. 0) then
*       Resolution file does not exist, so ask for a value
        lenx=lastchpos(name)
        write(6,*) 'Resolution file ',name(1:lenx),
     :              ' not found, or corrupt'
        write(6,*) 'FWHM (A)?'
        read(5,'(a)') cdum
        call dsepvar(cdum,1,dvx,ivx,cvx,nvx)
        swres(1,ichunk)=dvx(1)/2.35482d0
        swres(2,ichunk)=0.0d0
        nswres(ichunk)=2
        do i=3,6
          swres(i,ichunk)=0.0d0
        end do
      end if
      return
      end
