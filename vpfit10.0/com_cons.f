
*     ajc 3-jun-92
*     common blocks for constraint of parameters using fort.27 file
*     rfc 8.12.04: 3parameters/line changed to 5
      double precision parm27( 3*maxnco ), dparm27( 3*maxnco )
      character*2 ipind27( 3 * maxnco )
      integer cnt27

      common / cons_27 / cnt27, ipind27, parm27, dparm27
