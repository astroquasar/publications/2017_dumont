      subroutine vp_spvoigte(flx, contin,nlw,nw,wvsubd,npsubch,
     :       param,ipind,elm,ionz,nset,np,ichunk,cont)
*
*     SPVOIGTE  computes Voigt profile
*
*     22.08.06: arguments after nw were:
*        cxxd,zedy,velyb,
*     :	parm4,lby,lzy,ichunk,cont)
*
*     Input:
*               flx(nw)         continuum(emission) or anything(absn)
*		contin(nw)	continuum level
*		nlw	integer	lower channel limit for profile generation
*		nw	integer	array size for data
*               wvsubd(npsubch) double array  wavelengths for each (sub)bin
*               npsubch integer length of wavelength array
*               param(np) consists of:
*		cxxd	dble	column density (may be log) (and scaled!!)
*		zedy	dble	redshift
*		velyb	dble	Doppler parameter (may be log)
*               parm4   dble    4th parameter
*               ipind(np) ch*2    tied/fixed indicators
*		lby  ch*2	element
*		lzy  ch*4	ionization
*               nset integer    element from list (does one at a time)
*               np   int        length of parm array
*		ichunk		chunk number
*		cont(nw)	continuum used for zero level adjustment
*     Output:
*		flx(nw)		absorption on continuum
*
*
*     substepped version
*     Wavelength scale need not be linear
*
      implicit none
      include 'vp_sizes.f'
*     subroutine arguments
      integer nlw,nw,ichunk
      double precision contin(nw),flx(nw),cont(nw)
      integer npsubch
      double precision wvsubd(npsubch)
      double precision param(*)
      character*2 ipind(*)
      character*2 elm(*)
      character*4 ionz(*)
      integer np,nset

      double precision cxxd,zedy,velyb
*
*     LOCAL variables
      logical lgionok
      integer i
      integer j,jmid,jstep
      integer k
      integer nn,nn4,nxx
*     rest wavelength of test line for inclusion (may be modified
*     if using qmucf coefficients)
      double precision almt0
*
      double precision v,a,cne
      double precision zdp1,vely,veld
      double precision bl,cns
      double precision wvd,chand
      double precision awbasd,axku,vxbl
      double precision wtemp,xd
      double precision xtxx,cold
      double precision ww,wv
*     rest wavelength limit check variables
      double precision awbasdchk,axkuchk
*     variables for working with flux values:
      double precision e,s,stau,zeshft
      double precision fluxcur,tauchan
*
*     FUNCTIONS:
      double precision voigt,dexpf
      double precision calcn,vpf_bvalsp
      double precision vp_empval
*     
      character*2 lby
*     lzy is the 'level' i.e. ionization stage
      character*4 lzy
*     double precision vp_wval - no longer used
*     mim 13/6/00 The following variables are not needed anymore since
*     the interpolation procedure has been replaced by a subdivision of
*     pixels and the continuum is only multiplied at the end of the
*     routine.
*
*     double precision afd, bfd, cfd, ymin, xmin
*
*     basic atomic data
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm
      integer nz
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :             fik(maxats),asm(maxats),nz
*     parameters for smoothing
      double precision zed
      common/vpc_ewpms/zed
*     indvar=1 for log N, 0 linear, -1 for emission lines 
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
      integer ibdvar,izdvar
      common/vp_bzstyle/ibdvar,izdvar
*     Lyman series minimum wavelength in table
      double precision wlsmin,vblstar,collsmin
      common/vpc_lycont/wlsmin,vblstar,collsmin
*     Column density for inclusion of ion in ALL regions
      double precision fcollallzn
      common/vpc_colallzn/fcollallzn
      double precision bvalmin,bvalmax,bltdrop,bgtdrop,colltdrop,
     :                     clogltdrop,cvaldrop,clvaldrop,cvalmax,
     :                     clvalmax,bvalminh,bvalmaxh,clvalmin,
     :                     cvalmin
      common/vpc_bvallims/bvalmin,bvalmax,bltdrop,bgtdrop,colltdrop,
     :                     clogltdrop,cvaldrop,clvaldrop,cvalmax,
     :                     clvalmax,bvalminh,bvalmaxh,clvalmin,
     :                     cvalmin
*     printout variable
      logical verbose
      common/vp_sysout/verbose
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     Special conditions after this character (totals for column,
*     temperature/b for bval)
*      character*2 lastch,firstch
*      common/vpc_ssetup/lastch,firstch
*     wavelength shifts, held in:
      integer lassoc
      common/vpc_asschnk/lassoc(maxnch)
*     other rest wavelength stuff, if needed - q-coeffts
      logical lqmucf,lchvsqmu
      double precision qscale
      double precision qmucf
      common/vpc_qmucf/qmucf(maxats),qscale,lqmucf,lchvsqmu
*     chunk variables:
      integer ndpts(maxnch),idrun(maxnch),icrun(maxnch)
      integer indfil(maxnch)
      character*64 filename(maxnch)
      double precision wvstrt(maxnch),wvend(maxnch)
      integer nchunk
      common/vpc_chunk/wvstrt,wvend,ndpts,filename,
     :      idrun,icrun,indfil,nchunk
*     chunk to region link list
      integer linchk(maxnio)
      common/vpc_linchk/linchk
*     print counter
      integer kprcck,nrcck
      common/vpc_kprcck/kprcck,nrcck
*
*     initialize veld (does not matter)
      veld=0.0d0
*
*     base parameter value
      nn=noppsys*(nset-1)
*     rfc 24.05.99: check if redshift shifted ..
*     by a velocity specified in the bval field
      zedy=param(nn+nppzed)
      if(lassoc(ichunk).ne.0.and.elm(nset).ne.'>>') then
        nxx=noppsys*(lassoc(ichunk)-1)+nppbval
        zedy=(1.0d0+zedy)*
     :       (1.0d0+param(nxx)/2.99792458d5)-1.0d0
      end if
*     column density was a parameter, this computes total column density
      cxxd=calcn(np,nn+nppcol,param,ipind)
*     Doppler parameter
      velyb=vpf_bvalsp(nset,param,ipind)
*     ion
      lby=elm(nset)
      lzy=ionz(nset)
*
*     set velocity parameter for internal use
      if(ibdvar.ne.1) then
*       straight wavelengths used in program
        vely=velyb
       else
*       1 => log Doppler parameters used
        vely=10.0d0**velyb
      end if
*     antilog cxxd if necessary, but not if added component
      if(indvar.eq.1.and.lby.ne.'AD'.and.lby.ne.'__'.and.
     :       lby.ne.'<>'.and.lby.ne.'>>') then
        xtxx=cxxd/scalelog
*	mim@phys.unsw.edu.au: 17.10.01 Old default of 10.0
*	replaced by clvaldrop
        if(xtxx.gt.34.0d0.or.xtxx.lt.clvaldrop) then
          xtxx=clvaldrop
*         cxxd=clvaldrop*scalelog ! not used subsequently
        end if
        cold=10.0d0**xtxx
       else
        if(lby.eq.'AD'.or.lby.eq.'__'.or.lby.eq.'<>'.or.
     :          lby.eq.'>>') then
          cold=cxxd
          veld=vely
         else
          cold=cxxd/scalefac
        end if
      end if
      zdp1=1.0d0+zedy
      zed=zdp1
*     set up parameters
*
      if(indvar.ne.-1.and.lzy(4:4).ne.'e'.and.
     :         lby.ne.'AD'.and.lby.ne.'__') then
*       absorption line
        do i=1,nw
*         rfc 24.2.95: replace by unit continuum and multiply later,
*         so that can interpolate from continuous absorption to
*         highest series line:
          flx(i)=1.0d0
        end do
       else
*       emission line or added component
        do i=1,nw
          flx(i)=contin(i)
        end do
      end if
*
      if(nz.le.0) call vp_ewred(0)
*     nl=0
*     lower channel number for inclusion now a routine parameter
*     nlw=1
*
*     base wavelength in rest frame for later use in line inclusion check
      awbasd=(2.0d0*wvsubd(1)-wvsubd(2))/zdp1
*     end check value depends on line type
      awbasdchk=awbasd
*     upper rest wavelength for line inclusion check:
      axku=wvsubd(nw)/zdp1
      axkuchk=axku
*     special 'lines' have narrower limits for inclusion
      if(lby.eq.'<>'.or.lby.eq.'__'.or.lby.eq.'>>') then
        awbasdchk=wvstrt(ichunk)/zdp1
        axkuchk=wvend(ichunk)/zdp1
      end if
      if(vely.eq.0.0d0.or.vely.eq.veld) goto 302
      veld=vely
*	
 302  if(nw/2*2.eq.nw) then
        vxbl=wvsubd(nw/2)/zdp1
       else
        vxbl=0.5d0*(wvsubd(nw/2)+wvsubd(nw/2+1))/zdp1
      end if
*     velocity units cm/s internal, while km/s used!
      vxbl=veld*vxbl/1.0d5
*
*     now find the lines and slot them in
*     
      do k=1,nz
*       ajc 8-apr-92  include high column density lines always
*       [rfc 18-Nov-97: Was as well as continuum altering thing - for 
*       that order (otherwise, this routine not called) - but now uses
*       redshift as well]
*
        almt0=alm(k)
        if(lqmucf) then
          nn4=nn+4
          if(ipind(nn4)(1:1).eq.'q'.or.
     :       ipind(nn4)(1:1).eq.'Q') then
*           for small fine structure shifts (from frequency formula) 
*           almt0=alm(k)/(1.0d0+2.0d0*q*alm(k)*delta(a)/a)
            almt0=almt0/(1.0d0+qmucf(k)*param(nn4))
          end if
          if((ipind(nn4)(1:1).eq.'m'.or.ipind(nn4)(1:1).eq.'M')
     :      .and.(lby.eq.'H2'.or.lby.eq.'HD')) then
*           sensitivity coeffts - molecular hydrogen only
*           qmucf is the K_i, rescaled as necessary
            almt0=almt0*(1.0d0+qmucf(k)*param(nn4))
          end if
        end if
*       include line if ion right, region right, or include everywhere
*       condition satisfied .OR. region for that ion specified.
        lgionok=(lby.eq.lbz(k).and.lzy.eq.lzz(k))
*       rfc 01/03/10: Modified so only do something if fik > 0
        if( (linchk(nset).le.0.and.lgionok
     :         .and.((almt0.ge.awbasdchk.and.almt0.le.axkuchk.and.
     :                      fik(k).gt.0.0d0) .or.
     :                cold*fik(k).gt. fcollallzn )).or.
     :         ((linchk(nset).eq.ichunk).and.lgionok) ) then
          if(linchk(nset).eq.ichunk.and.kprcck.lt.nrcck) then
*           print warning for first times round only
            write(6,'(a,i3,a,i3)') 'Ion #',nset,
     1         ' forced into region',ichunk
*           and turn off later prints after all listed
            kprcck=1+kprcck
          end if
*         for each bin, starting from the line center
*         first get channel number for line center from wavelength array
          chand=(almt0-awbasd)*dble(nw)/(axku-awbasd)
          wvd=almt0*zdp1
          call vp_archwav(wvd,chand,wvsubd,nw)
          jmid=int(chand)
          tauchan=1.0d25
          jstep=-1
          j=jmid
*
          do while (tauchan.gt.1.0d-6.and.j.le.nw)
            if(jstep.eq.-1) then
              if(j.gt.nlw) then
                j=j-1
               else
                tauchan=1.0d-7
                if(jmid.le.0) jmid=1
                goto 901
              end if
             else
              if(j.lt.nw) then
                j=j+1
               else
                tauchan=1.0d-7
                goto 901
              end if
            end if
            stau=10.0d0
*
*           is this absorption or continuum or emission?
            if ( lby .eq. '>>' ) then
*             do nothing - now wavelength shift only
              continue
*             rfc 26.9.02: '<<' option (vp_cshift) removed.
             else if ( lby .eq. '<>' ) then
*             linear continuum adjust, uses unshifted rest wavelength
              xd=wvsubd(j)/zdp1-alm(k)
              xd=xd/alm(k)
              flx(j)=flx(j)*(cold + xd*veld)
*             ajc 18-jul-93 zero level - must be the last thing called 
*             (shift rather than scale).  
             else if ( lby .eq. '__' ) then
*	      Apply stretch and shift
              xd=wvsubd(j)/zdp1-alm(k)
*	      for this part as well, x=dlambda/lambda
              xd=xd/alm(k)
              zeshft=(cold+xd*veld)
              flx(j)=cont(j)*zeshft+flx(j)*(1.0d0-zeshft)
             else
*             only want to be here if normal ion 
*             line parameters. Note that wavelengths for this
*             bit are in cm. Use possibly adjusted wavelength from above.
              wv=almt0*1.0d-8
              bl=veld*wv/2.99792458d5
              a=asm(k)*wv*wv/(3.7699d11*bl)
              cns=wv*wv*fik(k)/(bl*2.00213d12)
              cne=cold*cns
              ww=wvsubd(j)*1.0d-8/zdp1
              stau=0.0d0
              fluxcur=flx(j)
*             ww=wlod+(dble(klv)-0.5d0)*dxd
              v=wv*wv*(1.0d0/ww-1.0d0/wv)/bl
              if(indvar.eq.-1.or.lby(1:2).eq.'AD'.or.
     :              lzy(4:4).eq.'e') then
*               term to be added
                if(lby(1:2).eq.'AD') then
                  s=fluxcur+(cold*fik(k))*vp_empval(ww)
                 else
                  s=fluxcur+(cold*fik(k)*dexpf(-v*v))
                end if
                stau=1.0d0
               else
*               absorb...
*               this seems to be voigt profile...
                stau=cne*voigt(v,a)
                e = dexpf(-stau)
                if(e.gt.1d-30.and.fluxcur.gt.1d-30)then
                  if ( log10(e) + log10( fluxcur ) 
     :                      .lt. -30.0d0 ) then
                    s=0.0d0
                   else
                    s=fluxcur*e
                  end if 
                 else
                  s=0.0d0
                end if
              end if
*             end of voigt profile
              flx(j)=s
            end if
            tauchan=stau
 901        continue
            if(jstep.lt.0.and.(tauchan.lt.1.0d-6.or.j.le.nlw)) then
              jstep=1
              j=jmid-1
              tauchan=1.0d25
            end if
          end do
        end if
      end do
*     CHECK:   Ly-c routine
*
*     rfc 24.2.95:
*
      if(indvar.ne.-1.and.lzy(4:4).ne.'e'.and.lby.ne.'AD'.and.
     :         lby.ne.'__') then
*       absorption line
*
*       add Lyman continuum absorption if hydrogen, and if in the
*       right wavelength region:
        if(lby.eq.'H '.and.lzy.eq.'I   ') then
*	  hydrogen, so check if need Lyman limit:
*	  wavelength limit is set by Doppler parameter (vblstar=3 default)
          wtemp=wlsmin*(1.0d0+vblstar/2.99792458d5)
*         column density limit is logN(HI)=15.0 by default 
*         (i.e. collsmin=1.0e15)
          if(awbasd.le.wtemp.and.dble(cold).gt.collsmin) then
            if(verbose) then
              write(6,*) 'Lycont:',zdp1,awbasd,wtemp,cold,ichunk
            end if
            call vp_lyconte(cold,veld,flx,nw,zdp1)
*           if(verbose) write(6,*) 'vp_lyconte exit OK'
          end if
        end if
*
*       multiply by the continuum which came in, to get result:
        do i=1,nw
          flx(i)=flx(i)*contin(i)
        end do
      end if
*
      return
      end
