      subroutine vp_trimattab
*     remove unused lines from the atomic data table, to speed
*     up the search when fitting.
      implicit none
      include 'vp_sizes.f'
*     Local:
      integer index(maxats)
      integer i,j,k,ktop
*
*     atomic data
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm 
      integer m
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),m
*     atomic mass table
      character*2 lbm
      double precision amass
      integer mmass
      common/vpc_atmass/lbm(maxats),amass(maxats),mmass
*     other rest wavelength stuff, if needed
      logical lqmucf,lchvsqmu
      double precision qscale
      double precision qmucf
      common/vpc_qmucf/qmucf(maxats),qscale,lqmucf,lchvsqmu
*
*     parameter stuff, including ions
*     parameter array
      integer nlines,nn
      character*2 elmt(maxnio)
      character*4 ionz(maxnio)
      double precision parm(maxnpa)
      common/vpc_parry/parm,elmt,ionz,nlines,nn
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     extra ion list & parameters
      integer nionxt
      character*2 elmxt(maxxpi)
      character*4 ionxt(maxxpi)
      character*2 ipinxt(maxxpp)
      double precision bvxmin
      double precision parmxt(maxxpp)
      common/vpc_parmxt/bvxmin,parmxt,ionxt,elmxt,nionxt,ipinxt
*     Current line parameters for guess (default = Ly-a)
      logical lgs
      character*2 ings
      character*4 lvgs
      double precision wvgs,fkgs,algs,amgs
      common/vpc_pargs/wvgs,fkgs,algs,amgs,lgs,ings,lvgs
*     verbose mode
      integer nverbose
      common/vp_sysmon/nverbose
*
*     Go up the lbz/lzz list, and see if there is a match
*     in the elem/ionz list.
*
*     is nlines set? If not, do so
      if(nlines.le.0) then
        nlines=nn/noppsys
      end if
      i=0
      j=0
 201  j=j+1
*     Keep special & Hydrogen & default guess
      if(lbz(j).eq.'<>'.or.lbz(j).eq.'__'.or.lbz(j).eq.'??'
     :    .or.lbz(j).eq.'>>'.or.
     :     (lbz(j).eq.'H '.and.lzz(j).eq.'I   ').or.
     :     (lbz(j).eq.ings.and.lzz(j).eq.lvgs)) then
        i=i+1
        index(i)=j
        goto 203
       else
*       need to see if this ion is in the parameter list.
*       and if it is then set index(i)
        k=0
 202    k=k+1
        if(k.le.nlines) then
          if(lbz(j).eq.elmt(k).and.lzz(j).eq.ionz(k)) then
            i=i+1
            index(i)=j
            goto 203
          end if
        end if
*       may be in preset set
        if(k.le.nionxt) then
          if(lbz(j).eq.elmxt(k).and.lzz(j).eq.ionxt(k)) then
            i=i+1
            index(i)=j
            goto 203
          end if
        end if
        ktop=max(nionxt,nlines)
        if(k.lt.ktop) goto 202
      end if
 203  continue
      if(j.lt.m) goto 201
*     now have index array, from 1 to i values
*     reset total number, and copy appropriate locations - in order 
*     OK because of the way things were set up, so always copies down
*     array
      m=i
      do i=1,m
        if(index(i).ne.i) then
          lbz(i)=lbz(index(i))
          lzz(i)=lzz(index(i))
          alm(i)=alm(index(i))
          fik(i)=fik(index(i))
          asm(i)=asm(index(i))
          qmucf(i)=qmucf(index(i))
*          leave mass table - it is short anyway
*          lbm(i)=lbm(index(i))
*          amass(i)=amass(index(i))
        end if
      end do
      if(nverbose.gt.6) then
        write(6,*) m,' atomic data lines remain in table'
        do i=1,m
          write(6,*) i,' ',lbz(i),lzz(i),alm(i),fik(i),asm(i),
     :         qmucf(i)
        end do
      end if
      return
      end
