
      logical function varythis( i, ipind )
*
        implicit none
        include 'vp_sizes.f'
*
        integer i
        character*2 ipind( * )
*       functions
        logical ucase
*
*       parameter variables
        integer noppsys,nppcol,nppbval,nppzed
        common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*
*       should this parameter be varied?
        varythis = .true.

*       first character upper case - so no
        if ( ucase( ipind( i )(1:1) ) ) then
          varythis = .false.
        end if

*       part of a tied form-following group (N)
        if ( i .gt. noppsys .and. mod( i, noppsys ) .eq. 1 ) then
          if ( ipind( i - noppsys )(1:1) .eq. vspecial ) then
            varythis = .false.
          end if
        end if
        return
        end


