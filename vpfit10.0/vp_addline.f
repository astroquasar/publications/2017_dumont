      subroutine vp_addline(ick,work,zed,bval,aln,
     1            phi,ion,level,parm,
     :            ipind,np,nfunc,chisqv)
*
*     add a new line to the worst fitted complex, ready to
*     have another go at the fit. Note 
*        phi(i,k)  is for fitted region only
*        dach(i,k) extends by npexsm each end
*     
*     IN:
*     ick	integer	chunk containing worst KS deviant
*     chisqv	double	normalized chisq for fit (for printing only)
*
*     WORKSPACE:
*     work	array	workspace
*     
*     UPDATED:
*     ion	ch*2 array	element
*     level	ch*4 array	ionization level
*     parm	array	fit parameters
*     ipind	ch*2 array	parm flag
*     np	integer	number of parameters
*     nfunc	integer	number of ions fitted
*
*     OUT [MAYBE NOT NEEDED AS ARGUMENTS]
*     zed	double	redshift
*     bval    dble
*     aln     dble
*
**     da	array	data for region ! dach in common
**     er	array	error array     ! dech in common
**     cn	array	continuum       ! dcch in common
*     phi	array	fit
*    [ REMOVED:
*    [ numpts	integer	number data points used		
*    [ npexsm	integer	nchan expansion beyond fit region limits
*
*
      implicit none
      include 'vp_sizes.f'
*
      integer ick
      double precision work(*)
      double precision zed,bval,aln,chisqv
*      double precision da(maxchs,maxnch),er(maxchs,maxnch)
*      double precision cn(maxchs,maxnch),
      double precision phi(maxchs,maxnch)
*      integer numpts,npexsm
      character*2 ion(maxnio)
      character*4 level(maxnio)
      double precision parm(maxnpa)
      character*2 ipind(maxnpa)
      integer np,nfunc
*
*     LOCAL:
      character*4 chlin
      character*2 ionx
      character*4 levx
      integer jj,jmass,kclosh,kxl,nfion
      double precision atmx,ewm,wlm,wlmx
      double precision bvalx,colx,fwhmm
*
*     COMMON:
*
*     New 2D dataset variables
      integer numchunk,nchchtot ! # of chunks,total # data points
      integer kchstrt(maxnch),kchend(maxnch),nchlen(maxnch)
      common/vpc_chunkvar/kchstrt,kchend,nchlen,numchunk,nchchtot
      double precision dach(maxchs,maxnch),derch(maxchs,maxnch)
      double precision drmch(maxchs,maxnch),dcch(maxchs,maxnch)
      common/vpc_datch/dach,derch,drmch,dcch
*     chunk wavelengths
      integer nwvch(maxnch)
      double precision wavch(maxchs,maxnch),wavchlo(maxchs,maxnch)
      common/vpc_wavch/wavch,wavchlo,nwvch
*     chunk limits
      integer ichstrt(maxnch),ichend(maxnch)
      common/jkw4/ichstrt,ichend
*     Log N if indvar=1, N otherwise (-1 for emission)
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
      double precision vturb,atmass,temper,fixedbth
      common/vpc_ucopder/vturb(maxnio),atmass(maxnio),
     :                 temper(maxnio),fixedbth(maxnio)
*     probability limits for adding lines
      integer nladded
      double precision chisqvh,probchb,probksb
      common/vpc_problims/chisqvh,probchb,probksb,nladded
*     region flags
      integer linchk( maxnio )
      common/vpc_linchk/linchk
*     last tied character - NOT USED HERE
*      character*2 lastch,firstch
*      common/vpc_ssetup/lastch,firstch
*
*     additional info returned to calling routine via common
*     iclosh	integer	ion which is closest in parm list
*     rwclosh	double	rest wavelength of closest line
*     fikclh	double	oscillator strength for this line
      integer iclosh
      double precision rwclosh,fikclh
      common/vpc_closelin/rwclosh,fikclh,iclosh
*     line list
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm
      integer nz 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :     fik(maxats),asm(maxats),nz
*     atomic mass table
      character*2 lbm
      double precision amass
      integer mmass
      common/vpc_atmass/lbm(maxats),amass(maxats),mmass

*     common flags for line adding routines:
      integer nadflag
      common/vpc_autofl/nadflag(10)
*
      integer ndrop,kdrop,ndroptot,ndrwhy
      common/vpc_nrejs/ndrop,kdrop(maxnio),ndroptot,ndrwhy
*     
      double precision zedqso
      common/vpc_zedqso/zedqso
*     Control for stopping add/remove iterations
      logical lstopit
      common/vpc_stopit/lstopit
*     output channels:
      integer nopchan,nopchanh,nmonitor
      integer lt(3)             ! output channels for printing
      common/vpc_nopchan/lt,nopchan,nopchanh,nmonitor
*     last added ion
      character*2 lastaddion,chaddion
      common/vpc_lastaddion/lastaddion,chaddion
*     forced cross-linked ions, with given guessed column density offsets
      integer nionxl
      character*2 ielxl(8)
      character*4 ionxl(8)
      double precision crlogxl(8)
      common/vpc_xlion/ielxl,ionxl,crlogxl,nionxl
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     variables in common blocks set in vp_rdspecial:
      double precision bvalmin,bvalmax,bltdrop,bgtdrop,colltdrop,
     :                     clogltdrop,cvaldrop,clvaldrop,cvalmax,
     :                     clvalmax,bvalminh,bvalmaxh,clvalmin,
     :                     cvalmin
      common/vpc_bvallims/bvalmin,bvalmax,bltdrop,bgtdrop,colltdrop,
     :                     clogltdrop,cvaldrop,clvaldrop,cvalmax,
     :                     clvalmax,bvalminh,bvalmaxh,clvalmin,
     :                     cvalmin
*     add fixed Doppler parameter variables
      double precision bvaladd
      character*2 charbfix
      common/vpc_addbfix/bvaladd,charbfix
*     other rest wavelength stuff. Action may depend on lqmucf
      logical lqmucf,lchvsqmu
      double precision qmucf
      double precision qscale
      common/vpc_qmucf/qmucf(maxats),qscale,lqmucf,lchvsqmu
*     rejection cycle parameters
      double precision clnemin,errlnemax
      common/vpc_endrej/clnemin,errlnemax
*
*     If drop flag set, don't try to add a line!
      if(ndrop.gt.0) goto 500
*
*     Redshift based on closest line or Ly-a.
*     Method is to search for absorption lines relative to the locally
*     fitted profile, and use as a first guess an additional line 
*     based on the EW needed to correct for the maximum flux residual.
*
*     check to see if the file 'stopit' has been created, if so 
*     then exit with nladded=0
      open ( 20, file = 'stopit', status = 'old', err = 501 )
      close(20)
      write(6, * ) ' autofit iterations stopped early'
*     delete stopit file so that it isn't there next time...
      call system( '/bin/rm stopit' )
*     and adjust the ec parameters so that it does not go through a
*     cycle rejecting lines
      clnemin=0.0d0
      errlnemax=1.0d35
      nladded=0
      lstopit=.true.
      goto 500
*     gets here if file 'stopit' does not exist
 501  write(6,'('' '')')
      write(6,*) ' Normalized chi-squared: ',chisqv,
     :     '  worst region is:',ick
*     if data is plotted, overplot the fit so far
      call vp_distry(phi,ick)
*
      call vp_xcmp(phi,work,ick,
     :           ewm,wlm,fwhmm,chlin)
      if(chlin.eq.'abso') then
        call vp_nearlin(parm,ion,level,np,wlm,kclosh)
*       add an absorption line Ly-a if the closest
*       line is hydrogen, or force it if nadflag.ge.1
*       Unless the redshift is too high or less than -900 km/s, 
*       in which case need to revert to closest
        zed=wlm/1215.67d0-1.0d0
        if((nadflag(1).eq.1.and.zed.le.zedqso.and.zed.ge.-3.0d-3).or.
     :       (nadflag(1).ne.2.and.lbz(kclosh).eq.'H '.and.
     :          zed.ge.-3.0d-3)) then
*         Put in Ly-a if nadflag(1) = 1 and redshift less than that
*         of the quasar, or if nearest line is hydrogen and nadflag(2)=0.
          ionx='H '
          levx='I   '
          atmx=1.0d0
          rwclosh=1215.67d0
          fikclh=0.4162d0
         else
          if(nadflag(1).eq.2) then
*           choose the unknown line labelled ??
            ionx='??'
            levx='    '
            atmx=1.0d0
*           at the wavelength of Ly-a (so the atom.dat table contents
*           should be the same or this will not work)
            rwclosh=1215.67d0
            fikclh=0.4162d0
           else
*           choose same ion and wavelength as closest line
            ionx=lbz(kclosh)
            levx=lzz(kclosh)
            rwclosh=alm(kclosh)
            fikclh=fik(kclosh)
            jmass=1
            do while(jmass.le.mmass.and.lbm(jmass).ne.ionx) 
              jmass=jmass+1
            end do
            if(jmass.le.mmass) then
              atmx=amass(jmass)
             else
              write(6,*) 'atomic mass new line set to 1.0'
              atmx=1.0d0
            end if
          end if
        end if
*       returns vacuum wavelength
        zed=wlm/rwclosh-1.0d0
*       Doppler parameter from FWHM, unless bvalue fixed from 'fixb'
        if(charbfix.ne.'  '.and.bvaladd.gt.0.0d0) then
          bval=bvaladd
         else
          bval=1.8d5*fwhmm/wlm
        end if
*       and assume the line is optically thin
        if(nopchan.gt.0) then
          write(6,*) ' WLM:',wlm,' EW:',ewm
        end if
        if(ewm.gt.0.0d0) then
*         column density
          aln=17.36d0+log10(0.4162d0*ewm/(wlm*fikclh))
          if(aln.lt.10.5d0) aln=10.5d0
         else
*         no absorption anywhere of any consequence -- so add something
*	  weak and arbitrary in the hope it sorts it out.
          aln=10.5d0
        end if
*       check bval is within range, adjust it and column density if necessary
        if(ionx.eq.'H ') then
          if(bval.lt.bvalminh) bval=bvalminh
          if(bval.gt.bvalmaxh) then
*           rescale column density so it is not too large
            aln=aln+log10(bvalmaxh/bval)
*           and reset Doppler parameter
            bval=bvalmaxh
          end if
         else
          if(bval.lt.bvalmin) bval=bvalmin
          if(bval.gt.bvalmax) then
*           rescale column density so it is not too large
            aln=aln+log10(bvalmax/bval)
*           and reset Doppler parameter
            bval=bvalmax
          end if
        end if
       else
*       emission peak dominates, so now have to sort out what to do
*       with existing lines.
        call vp_nrlinad(wlm,fwhmm,ion,level,parm,np,
     :         wlmx,bvalx,colx,ick)
        if(rwclosh.ne.0.0d0) then
          zed=wlmx/rwclosh-1.0d0
          ionx=ion(iclosh)
          levx=level(iclosh)
          jmass=1
          do while(jmass.le.mmass.and.lbm(jmass).ne.ionx) 
            jmass=jmass+1
          end do
          if(jmass.le.mmass) then
            atmx=amass(jmass)
           else
            write(6,*) 'atomic mass new line set to 1.0'
            atmx=1.0d0
          end if
          kclosh=iclosh
         else
          zed=wlmx/1215.67d0-1.0d0
          ionx='H '
          levx='I   '
          atmx=1.0d0
          rwclosh=1215.67d0
          kclosh=iclosh
        end if
        bval=bvalx
        if(colx.gt.37.0d0) then
          aln=log10(colx)
         else
          aln=colx
        end if
        if(ionx.eq.'H ') then
          if(bval.lt.bvalminh) bval=bvalminh
          if(bval.gt.bvalmaxh) then
*           rescale column density so it is not too large
            aln=aln+log10(bvalmaxh/bval)
*           and reset Doppler parameter
            bval=bvalmaxh
          end if
         else
          if(bval.lt.bvalmin) bval=bvalmin
          if(bval.gt.bvalmax) then
*           rescale column density so it is not too large
            aln=aln+log10(bvalmax/bval)
*           and reset Doppler parameter
            bval=bvalmax
          end if
        end if
      end if
      write(6,'(''  Try adding '',a2,a4,f9.2,'' at z='',
     :        f10.7,'' b='',f5.1,'' logN='',f7.2)') 
     :         ionx,levx,rwclosh,zed,bval,aln
*     put in last one added identifiers
      lastaddion=ionx
      chaddion=ionx
*     add the line in the parameter list:
      if(np.lt.maxnpa) then
        if(indvar.ne.0) then
*         column density is logs, other values unchanged
*         convert cgs col values to internal ones
          parm(np+nppcol)=aln*scalelog
         else
*         /cm^2 units for column density
          parm(np+nppcol)=scalefac*10.0d0**aln
        end if
        parm(np+nppzed)=zed
        parm(np+nppbval)=bval
*       lines not tied, so set ipind to blank
        ipind(np+nppcol)='  '
        ipind(np+nppzed)='  '
        if(charbfix.ne.'  '.and.bvaladd.gt.0.0d0) then
          ipind(np+nppbval)=charbfix
         else
          ipind(np+nppbval)='  '
        end if
*       subsequent parameters zero (& fixed if lqmucf is .true.)
        if(noppsys.gt.3) then
          do jj=4,noppsys
            parm(np+jj)=0.0d0
            if(lqmucf) then
              ipind(np+jj)='SQ'
             else
              ipind(np+jj)='  '
            end if
          end do
        end if
*
*       update number of parameters
        nfion=nfunc
        np=np+noppsys
*       add specified ion
        nfunc=nfunc+1
        ion(nfunc)=ionx
        level(nfunc)=levx
        atmass(nfunc)=atmx
        nladded=1
*       is the one to be added in the cross-link list?
        call vp_addxlink(ionx,levx,zed,bval,aln,nfunc,ion,level,
     :             np,parm,ipind, kxl)
*       write(6,*) ' exited vp_addxlink, nfion= ',nfion
        nladded=nladded+kxl
        if(nfion.gt.1) then
*         search for last non __ or AD record, since if these were
*         at the end they need to stay there.
          do while (nfion.gt.1.and.(ion(nfion).eq.'AD'.or.
     :              ion(nfion).eq.'__'))
            nfion=nfion-1
          end do
*          write(6,*) ' entering vp_pareorder, kxl=',kxl
*         nfion is last multiplicative parameter, apart from added-in
*         lines at nfunc to nfunc+1-nionxl. Now put that block starting
*         at nfion+1, and move the special lines to the end.
          call vp_pareorder(nfion,nfunc,kxl,ion,level,atmass,linchk,
     :              parm,ipind)
        end if
       else
        nladded=0
        write(6,*) 'Ran out of line parameter space..'
        write(6,*) '   .. recompile program with more space'
        write(6,*) '      if you want to add yet more lines!' 
      end if
 500  return
      end


      subroutine vp_xcmp(caff,derv,ick,
     1         ewm,wlm,fwhmm,chlin)
*     to find absorption and emission features in a spectrum
*
*     input:
*     da	object-sky array
*     re        sigma squared array
*     cont	continuum array (raw)
*     ca	continuum array (fitted)
*     derv	workspace array (to contain sqrt(re))
*     nl	array size
*     ny	2nd dimension
*     ick	2nd dimension used this time
*
*     output:
*     ewm	dble	max ew
*     wlm	dble	wavelength corresponding
*     fwhmm	dble	fwhm of this feature
*
      implicit none
      include 'vp_sizes.f'
      character*4 chlin
      double precision derv(*)
      double precision caff(maxchs,maxnch)
      double precision colthres,ewm,fwhmm,wlm
*
*     LOCAL:
      integer nl,ick
      integer i,ipl,ioffset
      integer j,jj,jelo,jehi,jhlo,jhhi,jpeak,jstart,jx
      integer jp,jstartpp
      integer lhi,lo,nchok
      double precision caj,contm,contn,contlo,contrmax
      double precision pxhh,pxhup,flx,pnsig,pntemp
      double precision dtemp,wl,sg,x,xc
      double precision hptemp,ptemp,temp,dx,fwhm
*
*     FUNCTIONS:
*      double precision vp_wval
*
*     Common:
*     internal channel shifts:
*      double precision dnshft2(maxnch)
*      common/vpc_shft2/dnshft2
*     New 2D dataset variables
      integer numchunk,nchchtot ! # of chunks,total # data points
      integer kchstrt(maxnch),kchend(maxnch),nchlen(maxnch)
      common/vpc_chunkvar/kchstrt,kchend,nchlen,numchunk,nchchtot
      double precision dach(maxchs,maxnch),derch(maxchs,maxnch)
      double precision drmch(maxchs,maxnch),dcch(maxchs,maxnch)
      common/vpc_datch/dach,derch,drmch,dcch
*     chunk wavelengths
      integer nwvch(maxnch)
      double precision wavch(maxchs,maxnch),wavchlo(maxchs,maxnch)
      common/vpc_wavch/wavch,wavchlo,nwvch
*     chunk limits
      integer ichstrt(maxnch),ichend(maxnch)
      common/jkw4/ichstrt,ichend
      double precision almv,ew,wlmup
*     local wavelengths
      double precision whlo,whhi
      common/vpc_addln/whlo,whhi
*     vp_addline parameters
      double precision addrat
      common/vp_adprm/addrat
*
*
      jelo=0
      jehi=0
      jpeak=0
*     presets
      pxhh=-1.0d37
      pxhup=-1.0d37
*     dshift2=dnshft2(ick)+dble(npexsm)
*     wlm=vp_wval(dble(nl/2)+dshift2,ick)
*     wlm initialized to middle somewhere
      wlm=0.5d0*(wavch(kchend(ick),ick)+wavch(kchstrt(ick),ick))
      ewm=-1.0d37
      colthres=11.0d0
      fwhmm=0.01d0
      contm=0.0d0
      contn=0.0d0
*
*     map on to phi array
      nl=kchend(ick)-kchstrt(ick)+1
      ioffset=kchstrt(ick)-1
*      write(18,*) 'Region',ick,'  Offset:',ioffset
      do i=1,nl
*       allow for bad pixels
        ipl=i+ioffset
*        write(18,*) i,wavch(ipl,ick),dach(ipl,ick),derch(ipl,ick),
*     :        caff(i,ick),dcch(ipl,ick)
        temp=min(derch(ipl,ick),drmch(ipl,ick))
        if(temp.gt.0.0d0) then
*         error array for this chunk
          derv(i)=sqrt(derch(ipl,ick))
         else
*	  if negative error, copy value
          derv(i)=temp
        end if
        if(dcch(ipl,ick).gt.0.0d0) then
          contm=contm+dcch(ipl,ick)
          contn=contn+1.0d0
        end if
      end do
      if(contn.gt.0.0d0) then
*       mean continuum for end of line checks
        contm=contm/contn
       else
        contm=1.0d0
      end if
*     if fitted continuum less than this value don't bother
*     looking for lines there
      contlo=1.0d-3*contm
*       
*     start and end channels to avoid being outside fit range
      lhi=nl-1
      lo=2
*     seek lines on any scale
      jstart=lo
      jstartpp=jstart+ioffset
*     pass up beyond noise spikes
      do while ((jstart.lt.lhi).and.
     : (derch(jstartpp,ick).le.0.0d0.and.drmch(jstartpp,ick).le.0.0d0))
        jstart=jstart+1
        jstartpp=jstartpp+1
      end do
 1    j=jstart
      jp=j+ioffset
*     absorption line		sg=1.0
*     emission line		sg=-1.0
      if(caff(j,ick).gt.dach(jp,ick)) then
        sg=1.0d0
       else
        sg=-1.0d0
      end if
*     initialize sum and max measured quantities
      ew=0.0d0
      wl=0.0d0
      flx=0.0d0
      pnsig=0.0d0
      nchok=0
      contrmax=-1.0d37
*     add in the appropriate incremental values
 101  xc=(caff(j,ick)-dach(jp,ick))*sg
*     include in count unless ignore flag set
      if(derv(j).gt.0.0d0) then
        flx=flx+xc
        caj=caff(j,ick)
        if(caj.lt.contlo) caj=contlo
        x=(1.0d0-dach(jp,ick)/caj)*sg
        ew=ew+x
        nchok=nchok+1
        dtemp=wavch(jp,ick)
*       was = vp_wval(dble(j)+dshift2,ick)
        wl=wl+dtemp*x
        pntemp=(caff(j,ick)-dach(jp,ick))*sg/derv(j)
        pnsig=pnsig+pntemp**2
        if(caff(j,ick).gt.contrmax) contrmax=caff(j,ick)
      end if
 102  j=j+1
      jp=jp+1
      if(derv(j).le.0.0d0.and.j.le.lhi) goto 102
*     check that sign is unchanged, so still the same line
      if((caff(j,ick)-dach(jp,ick))*sg.gt.0.0d0.and.
     :       j.lt.lhi) goto 101
*     sign has changed, so end of one line is j-1, start of next is j
*     determine the quantities which matter for the line just ended:
      if(ew.gt.0.0d0) then
        wl=wl/ew
       else
*       guess a wavelength near the middle
        wl=0.5d0*(wavch(jp-1,ick)+wavch(jstartpp,ick))
*       replaces:
*	  wl=vp_wval(0.5d0*(dble(j-1)+dble(jstart))
*     :                            +dshift2,ick)
      end if
*     wavelength range:
      if(j.gt.jstart) then
        jx=j
       else
        jx=jstart+1
      end if
      x=wavch(jx+ioffset,ick)-wavch(jstartpp,ick)
*     was:
*     x=vp_wval(dble(jx)+dshift2,ick)-vp_wval(dble(jstart)+dshift2,ick)
      temp=dble(jx+ioffset-jstartpp)
      dx=x/temp
*      write(6,*) 'A/ch:',dx
      flx=flx*dx
      ew=ew*dx
*     guess at the FWHM, from the simplest possible approach 
      fwhm=0.6d0*x
      pnsig=sqrt(pnsig)
*     list results (comment out when developed)
*      write(6,*) '%%% ',wl,ew,flx,fwhm,pnsig,sg
*     store extreme values
      if(sg.gt.0.0d0) then
*	absorption line
        if(ew.gt.0.0d0) then
*	  optically thin column density
          almv=17.36d0+log10(ew/wl)
         else
          almv=0.0d0
        end if
*	update stored absorption maximum values, skipping
*	where continuum too low
        if(pnsig.ge.pxhh.and.sg.gt.0.0d0.and.almv.gt.colthres
     :        .and.contrmax.gt.contlo) then
*	  new most significant feature
*	  jalo=jstart
*	  jahi=j-1
          ewm=ew
          wlm=wl
          fwhmm=fwhm
          pxhh=pnsig
        end if
       else
*	and keep an eye on the upward excursions as well, since may need
*	to divide some of the broader features:
        if(pnsig.ge.pxhup.and.sg.lt.0.0d0) then
          jelo=jstart
          jehi=j-1
          wlmup=wl
          pxhup=pnsig
        end if
      end if
*     reset jstart for the next line
      jstart=j
      jstartpp=j+ioffset
*     and branch to the top
      if(j.lt.lhi) goto 1
      chlin='abso'
      if(pxhup.gt.pxhh*addrat) then
        chlin='emis'
*       search for the peak
        ptemp=-1.0d37
        if(jelo.le.jehi) then
          jpeak=jelo
         else
          do jj=jelo,jehi
            temp=dach(jj+ioffset,ick)-caff(jj,ick)
            if(temp.gt.ptemp) then
              ptemp=temp
              jpeak=jj
            end if
          end do
        end if
*	now find the half power points, with j values just inside
*	the outermost points satisfying the half power criterion
        hptemp=0.5d0*ptemp
*	lower one:
        if(jelo.lt.jpeak) then
          jj=jelo
          do while (dach(jj+ioffset,ick)-caff(jj,ick).lt.hptemp
     :             .and.jj.lt.jpeak)
            jj=jj+1
          end do
          jhlo=jj
         else
          jhlo=jpeak
        end if
*	upper one:	    
        if(jehi.gt.jpeak) then
          jj=jehi
          do while (dach(jj+ioffset,ick)-caff(jj,ick).lt.hptemp
     :             .and.jj.gt.jpeak)
            jj=jj-1
          end do
          jhhi=jj
         else
          jhhi=jpeak
        end if
*
*	whlo=vp_wval(dfloat(jhlo-1)+dshift2,ick)
*	whhi=vp_wval(dfloat(jhhi+1)+dshift2,ick)
*	wpeak=vp_wval(dfloat(jpeak)+dshift2,ick)
        whlo=wavch(jhlo-1+ioffset,ick)
        whhi=wavch(jhhi+1+ioffset,ick)
*        wpeak=wavch(jpeak+ioffset,ick)
        wlm=wlmup
        fwhmm=abs(whhi-whlo)
        write(6,'(a,f10.2,a,1pe12.2)') 
     :       ' Divide line at ', wlmup,'  ::',pxhup
      end if
      return
      end

      subroutine vp_distry(phi,ick)
*     display current fit
      implicit none
      include 'vp_sizes.f'
      integer numpts,ick
      double precision phi(maxchs,maxnch)
*     Local
      integer i,ioffset
*     New 2D dataset variables
      integer numchunk,nchchtot ! # of chunks,total # data points
      integer kchstrt(maxnch),kchend(maxnch),nchlen(maxnch)
      common/vpc_chunkvar/kchstrt,kchend,nchlen,numchunk,nchchtot
      double precision dach(maxchs,maxnch),derch(maxchs,maxnch)
      double precision drmch(maxchs,maxnch),dcch(maxchs,maxnch)
      common/vpc_datch/dach,derch,drmch,dcch
*     chunk wavelengths
      integer nwvch(maxnch)
      double precision wavch(maxchs,maxnch),wavchlo(maxchs,maxnch)
      common/vpc_wavch/wavch,wavchlo,nwvch
*     double precision workspace
      real dspg,repg
      common/vpc_snglwork/dspg(maxfis),repg(maxfis)
*     pgplot flags:
      integer ipgopen,ipgflag
      common/pgplotv/ipgopen,ipgflag
*
*     If something plotted, then overplot curve
      if(ipgopen.gt.0) then
        ioffset=kchstrt(ick)-1
        numpts=kchend(ick)-ioffset
        do i=1,numpts
          dspg(i)=real(phi(i,ick))
          repg(i)=real(wavch(i+ioffset,ick))
        end do
*	plot the curve .. dotted
        call pgsls(4)
        call pgline(numpts,repg,dspg)
      end if
      return
      end


