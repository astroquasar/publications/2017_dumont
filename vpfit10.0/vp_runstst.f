      subroutine vp_runstst(x,y,n,nplus,nnt,nrun,prob,ifail)
*     to replace NAG g08aaf with something freely available
*     Runs test on x-y(n), returns no plus, no nonties, probability
*
      implicit none
      integer n,nplus,nnt,nrun,ifail
      double precision x(n),y(n),prob
*     
      integer ij,ik,j
      integer isg,isgv,nminus
      double precision temp,exn,rnn,exm,exr,var
      double precision gerf
*
      ifail=0
      nplus=0
      nrun=1
      nnt=1
*     Search for first non-tie, and start there
      ij=1
      do while (x(ij).eq.y(ij).and.ij.le.n)
        ij=ij+1
      end do
      if(ij.le.n) then
*       something worth doing, ij is first non-tied element
        nminus=0
        if(x(ij).gt.y(ij)) then
          isg=1
          nplus=nplus+1
         else
          isg=-1
          nminus=nminus+1
        end if
*       start point for the rest
        ik=ij+1
        if(ik.le.n) then
          do j=ik,n
            if(x(j).ne.y(j)) then
*             include ths one
              nnt=nnt+1
              if(x(j).gt.y(j)) then
                isgv=1
                nplus=nplus+1
               else
                isgv=-1
                nminus=nminus+1
              end if
              if(isgv.ne.isg) then
                nrun=nrun+1
              end if
              isg=isgv
            end if
          end do
        end if
*       determine probability, if enough values!
        if(nnt.gt.1) then
*         if a good fit, expect half positive, half negative. This
*         need not be built in, but is appropriate to do so here.
          exn=dble(nnt/2)
          rnn=dble(nnt)
          exm=rnn-exn
*         exr=1.0+2.0*nplus*nminus/nnt
          exr=1.0d0+2.0d0*exm*exn/rnn
*         var=2.0*nplus*nminus*(2.0*nplus*nminus - nplus - nminus)/
*     :         (nnt**2*(nnt-1))
          var=2.0d0*exn*exm*(2.0d0*exm*exn-rnn)/
     :                            (rnn*rnn*(rnn-1.0d0))
*         How does result (nrun) compare with expectation?
          temp=(dble(nrun)-exr)/sqrt(var)
*         temp Normal with zero mean, unit standard deviation
          prob=gerf(temp)
         else
          prob=1.0d0
        end if
       else
        ifail=1
        prob=1.0d0
      end if
      return
      end
