      Program bintofits
*     This program should be compiled and run on the type of machine
*     where the binary data was created.
*     Requirements: CFITSIO library, sepvar.f
*
      implicit none
      integer nofpixels,nofspec,npixext
      real*8 flux(7000000)
      real*4 fluxout(8000000)
*
      integer nbase
*     common variables
      character*64 inchstr
      real*4 rv(4)
      integer iv(4),nv
      character*16 cv(4)
      common/bnc_sepvar/inchstr,rv,iv,cv,nv
*
*     get lengths
      write(6,*) 'spectrum length (pixels), # of spectra'
      write(6,*) '  .. and extension length each end (pixels)'
      write(6,*) '[1000,1000,150]'
      read(5,'(a)') inchstr
      call sepvar(inchstr,3,rv,iv,cv,nv)
      if(iv(1).gt.10) then
        nofpixels=iv(1)
       else
        nofpixels=1000
      end if
      if(iv(2).gt.0) then
        nofspec=iv(2)
       else
        nofspec=1000
      end if
      if(iv(3).gt.0) then
        npixext=nofpixels+2*iv(3)
        nbase=iv(2)
       else
        npixext=nofpixels+300
        nbase=150
      end if
      write(6,*) nofspec,' spectra, each with',nofpixels,' pixels'
      call bfdata(flux,fluxout,nofspec,nofpixels,npixext,nbase)
      stop
      end
*
      subroutine bfdata(flux,fluxout,nofspec,nofpixels,npixext,nbase)
      implicit none
      integer nofpixels,nofspec,npixext
      integer nbase
*     parameter (nofpixels = 1000,nofspec = 1000,npixext=1300)
      real*8 flux(nofspec,nofpixels)
      real*4 fluxout(npixext,nofspec)
*
      integer i,j,k,kk,len,len2,lenout,lenerr
      integer lopixb,nhipixb
*     FITS handler declarations
      double precision bscale,bzero,dvalue,dvalue2
      logical simple,extend
      integer unit,status,blocksize,bitpix,naxis,naxes(2)
      integer nelements,group,fpixel,decimals
      character*64 chinfile,choutfile,cherrfile
*     data variables
      double precision dvelrange,dnopix,dwstart,dwend
      integer nlev
      real flev(50),sig(50)
*     function declarations
      integer lastchpos
*     common variables
      character*64 inchstr
      real*4 rv(4)
      integer iv(4),nv
      character*16 cv(4)
      common/bnc_sepvar/inchstr,rv,iv,cv,nv
*
      write(6,*) 'Binary data filename?'
      read(5,'(a)') chinfile
      open (10,file=chinfile,form='unformatted',status='old',err=999)
      read (10) flux

*     create a FITS primary array containing the data
      status=0
*     filename to be created
      len=lastchpos(chinfile)
      do while (len.gt.1.and.chinfile(len:len).ne.'.')
        len=len-1
      end do
      if(len.gt.1) then
*       found a dot
        chinfile=chinfile(1:len)//'fits'
      end if
      write(6,*) 'FITS filename? [',chinfile(1:len+4),']'
      read(5,'(a)') choutfile
      if(choutfile(1:1).eq.' ') then
        choutfile=chinfile
        lenout=len+4
       else
*       add a '.fits' if necessary
        lenout=lastchpos(choutfile)
        if(lenout.gt.5) then
          if(choutfile(lenout-4:lenout).ne.'.fits') then
            choutfile=choutfile(1:lenout)//'.fits'
            lenout=lenout+5
          end if
         else
          choutfile=choutfile(1:lenout)//'.fits'
          lenout=lenout+5
        end if
      end if
      cherrfile=choutfile(1:lenout-4)//'sig.fits'
      lenerr=lenout+4
*     get a unit number to use
      call ftgiou(unit,status)
*     create the new empty FITS file
      blocksize=1
      call ftinit(unit,choutfile,blocksize,status)
*     parameters
      simple=.true.
      bitpix=-32
      naxis=2
      naxes(1)=npixext
      naxes(2)=nofspec
      extend=.true.
*     write the required header keywords
      call ftphpr(unit,simple,bitpix,naxis,naxes,0,1,extend,status)
*     make sure xcale factors OK
      bscale=1.0d0
      bzero=0.0d0
      call ftpscl(unit,bscale,bzero, status)
*     set values
      lopixb=(npixext-nofpixels)/2
      nhipixb=lopixb+nofpixels
      do j=1,nofspec
*       pad out lower and higher wavelengths for point spread function
*       to be catered for correctly        
        do i=1,lopixb
          kk=nofpixels-lopixb+i
          fluxout(i,j)=flux(j,kk)
        end do
*       data section to be fitted
        do i=1,nofpixels
          kk=lopixb+i
          fluxout(kk,j)=flux(j,i)
        end do
*       upper end padding
        do i=1,lopixb
          kk=i+nhipixb
          fluxout(kk,j)=flux(j,i)
        end do
      end do
*     write out to FITS file
      nelements=npixext*nofspec
      group=1
      fpixel=1
      call ftppre(unit,group,fpixel,nelements,fluxout, status)
      if(status.ne.0) then
        write(6,*) 'Data write status=',status
        goto 302
      end if
*     put in various header keywords to save having to do it later
      decimals=6
      call ftpkye(unit,'RESVEL',6.0,decimals,'km/s FWHM', status)
*     wavelength information
*     base is Ly-a at input redshift, increment is 1563.35/999 km/s
*     Note - need new header, since this applies to ALL spectra
      call ftpkys(unit,'WCFTYPE','log','wavelength coefft type',
     :  status)
      decimals=12
      write(6,*) 'redshift? [3.0]'
      read(5,'(a)') inchstr
      call sepvar(inchstr,1,rv,iv,cv,nv)
      if(cv(1)(1:1).ne.' ') then
        dwstart=dble((1.0+rv(1))*1215.67)
       else
        dwstart=4862.68d0
      end if
      dvalue=log10(dwstart)
      write(6,*) 'Velocity range [1563.35 km/s]?'
      read(5,'(a)') inchstr
      call sepvar(inchstr,2,rv,iv,cv,nv)
      if(cv(1)(1:1).eq.' ') then
        dvelrange=1563.35d0
       else
        dvelrange=dble(rv(1))
      end if
*     wavelength end for fitting
      dwend=dvelrange/2.997925d5
      dwend=sqrt((1.0d0+dwend)/(1.0d0-dwend))*dwstart
      dnopix=dble(nofpixels-1)
*     Used to be queried, and dnopix=dble(iv(2)-1) [default result 999]
      dvalue2=log10(1.0d0 + dvelrange/(2.997925d5*dnopix))
      dvalue=dvalue-dble(nbase)*dvalue2
      call ftpkyd(unit,'GLWBASE',dvalue,decimals,'log base wavelength',
     :     status)
      call ftpkyd(unit,'GLWINCR',dvalue2,decimals,
     :     'log increment per pixel', status)
*
      call ftpkys(unit,'CONTFILE','unity','continuum filename',
     :  status)     
      call ftpkys(unit,'SIGFILE',cherrfile(1:lenerr),
     :  'error filename',status)      
*
 302  call ftclos(unit, status)
      call ftfiou(unit, status)
      close(10)
      len2=lastchpos(choutfile)
*     write out a fort.13 file if you want one
      write(6,*) 'Write VPFIT fort.13 file? If not, type n  [y]'
      read(5,'(a)') inchstr
      if(inchstr(1:1).eq.'n'.or.inchstr(1:1).eq.'N') then
        write(6,*) ' fort.13 file not written'
       else
        do j=1,nofspec
          write(13,'(a,a,a,i4,f12.2,a,f12.2)') 
     :     '%% ',choutfile(1:len2),' ',j,dwstart,' ',dwend
          write(13,*) ' '
        end do
      end if
*     and make the sigma file. This is flux dependent, so just modify
*     the flux values according to an interpolation table
      write(6,*) 'Error table filename? (<blank> or none to skip)'
*     choutfile no longer used, so now contains error table name
      read(5,'(a)') choutfile
*     terminate if error file not wanted, normally because it
*     has been made directly
      if(choutfile(1:1).eq.' '.or.choutfile(1:4).eq.'none')
     :     goto 999
      open(unit=7,file=choutfile,status='old',err=999)
      i=1
      do while(i.lt.50) 
        read(7,'(a)',end=998) inchstr
        call sepvar(inchstr,2,rv,iv,cv,nv)
        flev(i)=rv(1)
        sig(i)=rv(2)
        i=i+1
      end do
 998  nlev=i
      if(nlev.gt.1) then
        flev(i)=2.0*flev(i-1)
        sig(i)=sig(i-1)
      end if
*     replace each flux value by the error estimate, using local
*     linear interpolation
      do j=1,nofspec
        do i=1,npixext
          k=nlev
          do while (fluxout(i,j).lt.flev(k).and.k.gt.1) 
            k=k-1
          end do
*         k is now such that flev(k) .le. fluxout(i,j)
*         so interpolate sig(k) and sig(k+1), after bypassing special cases
          if(k.eq.1.and.fluxout(i,j).le.flev(k)) then
            fluxout(i,j)=sig(k)
           else if (k.eq.nlev.and.fluxout(i,j).ge.flev(k)) then
            fluxout(i,j)=sig(k)
           else
*           interpolate
            fluxout(i,j)=(sig(k+1)*(fluxout(i,j)-flev(k))+sig(k)*
     :       (flev(k+1)-fluxout(i,j)))/(flev(k+1)-flev(k))
          end if
        end do
      end do
*     fluxout now contains sigmas, so write to a sigma fits file
*     get a unit number to use
      call ftgiou(unit,status)
*     create the new empty FITS file
      blocksize=1
      call ftinit(unit,cherrfile(1:lenerr),blocksize,status)
*     parameters
      simple=.true.
      bitpix=-32
      naxis=2
      naxes(1)=npixext
      naxes(2)=nofspec
      extend=.true.
*     write the required header keywords
      call ftphpr(unit,simple,bitpix,naxis,naxes,0,1,extend,status)
*     make sure scale factors OK
      bscale=1.0d0
      bzero=0.0d0
      call ftpscl(unit,bscale,bzero, status)
*     write out to FITS file
      nelements=npixext*nofspec
      group=1
      fpixel=1
      call ftppre(unit,group,fpixel,nelements,fluxout, status)
      if(status.ne.0) then
        write(6,*) 'Error write status=',status
      end if
      call ftclos(unit, status)
      call ftfiou(unit, status)
      close(unit=7)
 999  return
      end
