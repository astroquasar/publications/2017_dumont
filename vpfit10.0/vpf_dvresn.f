      double precision function vpf_dvresn(xdb,ichunk)
*     
*     determine Gaussian sigma (=1.0/R*2.35) at wavelength xdb
*     [equivalent to FWHM(km/s)/2.99792458d5*2.35
*
      implicit none
*
      include 'vp_sizes.f'
*
*     IN: wavelength of interest
      double precision xdb
*     chunk number
      integer ichunk
*
*     Local variables
      integer j
      double precision drange
*
*     COMMON:
*     binned profile
      character*2 chprof
      double precision pfinst
      double precision wfinstd
      integer npinst,npin2
      common/vpc_profwts/wfinstd(maxipp,maxnch),pfinst(maxipp,maxnch),
     :        npinst(maxnch),npin2(maxnch),chprof(maxnch)
*     starter table position
      integer nwhere
      common/vpc_resposn/nwhere
*     fit to resolution, using sigmas since they are in expression used
      integer nswres(maxnch)
      double precision swres(maxpoc,maxnch)
      common/vpc_sigwres/swres,nswres
*     wavelength base shift
      double precision dnshft2(maxnch)
      common/vpc_shft2/dnshft2
*
      if(chprof(ichunk)(1:2).eq.'wt') then
*        write(6,*) 'vpf_dvresn: n',nwhere
*       search up or down the table
*       nwhere is s.t. wfinstd(nwhere,ichunk).le.xdb<wfinstd(nwhere+1,ichunk)
*
*       could be new, smaller dataset, so make sure in range
        if(nwhere.gt.npinst(ichunk)) nwhere=1
*       go down if necessary
        do while(xdb.lt.wfinstd(nwhere,ichunk).and.
     :               nwhere.gt.1)
          nwhere=nwhere-1
        end do
*       go up if necessary
        do while(xdb.ge.wfinstd(nwhere+1,ichunk)
     :              .and. nwhere.lt.npinst(ichunk))
          nwhere=nwhere+1
        end do
*       now xdb is in required range, interpolate resolution
        if(xdb.le.wfinstd(1,ichunk)) then
          vpf_dvresn=pfinst(1,ichunk)
         else
          if(xdb.ge.wfinstd(npinst(ichunk),ichunk)) then
            vpf_dvresn=pfinst(npinst(ichunk),ichunk)
           else
*           interpolate
            drange=wfinstd(nwhere+1,ichunk)-wfinstd(nwhere,ichunk)
            vpf_dvresn=((wfinstd(nwhere+1,ichunk)-xdb)*
     :       dble(pfinst(nwhere+1,ichunk))+
     :       (xdb-wfinstd(nwhere,ichunk))*dble(pfinst(nwhere,ichunk)))
     :       /drange
          end if
        end if
       else
*       use the swres set, which are sigma (1/R). 
*       So e.g. swres(2,ichunk)=(velocity sigma)/2.99792458d5
        vpf_dvresn=swres(2,ichunk)+swres(1,ichunk)/xdb
*       and higher order polynomial if necessary
        if(nswres(ichunk).gt.2) then
          do j=3,nswres(ichunk)
            vpf_dvresn=vpf_dvresn+swres(j,ichunk)*xdb**(j-2)
          end do
        end if
      end if
      return
      end
