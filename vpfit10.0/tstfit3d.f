      subroutine tstfit3d(n,dta,var,mdl,wlevel, p,stat )

*     calculate the goodness of fit statistic for a variable scaled section.
*     points with -ve variance are ignored.
*     uses mike's method
*     argument order rationalized 14.10.05

      implicit none
*     IN
      integer n                 ! number of points
      double precision dta( n ) ! data points
      double precision var( n ) ! variances
      double precision mdl( n ) ! model values
      integer wlevel            ! amount of output, 0=no printed output
*     OUT
      double precision p        ! probability
      double precision stat     ! ks statistic

      integer i                 ! loop counter
      double precision dtasum   ! sum of data
      double precision mdlsum   ! sum of model
      double precision scale    ! scaling for that bin
      double precision delta    ! cumulative difference
      double precision bigdel   ! largest delta (absolute)
      integer numbin            ! number of bins
*     Functions
      double precision probks   ! KS probability routine
*     check that the model is the same integral as the data

      dtasum = 0d0
      mdlsum = 0d0
      do i = 1, n
        if ( var( i ) .gt. 0d0 ) then
          dtasum = dtasum + dta( i )
          mdlsum = mdlsum + mdl( i )
        end if
      end do

*     now calculate the statistic

      delta = 0d0
      bigdel = 0d0
      numbin = 0

      do i = 1, n
        if ( var( i ) .gt. 0d0 ) then
          scale = 1d0 / sqrt( var( i ) )
          delta = delta + scale * 
     :          ( dta( i ) - mdl( i ) * dtasum / mdlsum )
          bigdel = max( bigdel, abs( delta ) )
          numbin = numbin + 1
        end if
      end do

      if ( wlevel .gt. 1 ) then
        write ( *, * ) ' tstfit3 : variable scaling - mike'
        write ( *, * ) '           number of bins ', numbin
        write ( *, * ) '           unscaled delta ', bigdel
      end if

*     divide delta by sqrt number of bins (normalisation)

      bigdel = bigdel / sqrt( dble( numbin ) )

      if ( wlevel .gt. 1 ) then
        write ( *, * ) '           statistic ', bigdel
      end if

*     calculate probability

      stat = bigdel
      p = probks( stat )

      if ( wlevel .gt. 0 ) then
        write ( *, * ) ' tstfit3   probability ', p
      end if
      return
      end
