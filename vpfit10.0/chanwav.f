      subroutine chanwav(wvd,ced,errtol,nitnx)
*     Purpose: determine the value CED satisfying 
*     WVD=wlamd(CED)
*     wlamd is usually either a straight or Cheb poly.
*     INPUT:
*		wvd	double precision	value
*		ced	double precision	guesses solution
*		errtol	double precision	tolerance
*		nitnx	integer	max number of iterations
*     OUTPUT:
*		ced	double precision	solution
*
      include 'vp_sizes.f'
      integer nitnx
      double precision wval,chebd
      double precision wvd,ced,errtol,cex
      double precision dxd,derav,t1,t2,t3,t4
*     general wavelength coefficients
      integer nwco
      double precision wcf1(maxwco)
      common/vpc_wavl/wcf1,nwco
*
      character*8 wcftype
      character*4 vacind
      double precision helcfac
      double precision cprime(maxwco),dnorm
      integer*4 noffs
      common/c8_wcftype/wcftype,helcfac,noffs,vacind
*
*     solve directly, or use Newton-Raphson
*     on wvd=f(ced)*helfac*vacair(wvd)
*	  so Dwvd/Dced=helfac*Df/Dced*(vacair+Dvacair/Df), and
*	  make the approx that f=wvd for the (vac...   ) term.
*	  Also, precise channel number not needed, so Edlen formula
*	  (with coeffts used by Morton) will be adequate.
*
      n=nwco
      if(vacind(1:3).eq.'air') then
*       set up air term (vacair+dvacair/dlamda)
        t1=1.0d4/wvd
        t2=t1*t1
        t3=2.94981d-2/(146d0-t2)
        t4=2.554d-4/(41.0d0-t2)
        derav=1.0d0 + 6.4328d-5 + 
     :            t3*(1.0d0+2.0d0*t2/((146.0d0-t2)*wvd)) +
     :            t4*(1.0d0+2.0d0*t2/((41.0d0-t2)*wvd))
        else
         derav=1.0d0
      end if
*
      if(wcftype(1:3).eq.'log'.or.wcftype(1:4).eq.'arra'.or.
     :        wcftype(1:4).eq.'tabl') then
        if(wcftype(1:3).eq.'log') then
*         log linear wavelengths 
          t1=helcfac
          if(vacind(1:3).eq.'air') then
            t2=(1.0d4/wvd)**2
            t1=t1*(1.0d0 + 6.4328d-5 +
     :          2.94981d-2/(146.0d0-t2) + 2.554d-4/(41.0d0-t2))
          end if
*         can convert straight to channel number
*         by inverting wav = vacair*helcf* 10**(cf1 + cf2*n)
          ced=(dlog10(wvd/t1)-wcf1(1))/wcf1(2)+dble(noffs)
         else
*         straight array interpolation
          call vp_archwav(wvd,ced,wcf1,nwco)
        end if
       else
*       actual wavelengths, not logs, and not table (i.e not array)
        dxd=1.0d0
        nitn=0
        if(wcftype(1:4).eq.'cheb') then
*         set up cheb derivative coeffts as for NAG routine E02AHF
*         for method see Modern Comp Methods Ch8, NPL Notes on Applied Science
*         No 16 (2nd ed) HMSO 1961. Numerical recipes is similar.
          nprime=n-2
          cprime(n)=0.0d0
          dnorm=2.0d0/(wcf1(2)-wcf1(1))
          cprime(n-1)=dble(2*(nprime-1))*wcf1(n)*dnorm
          if(nprime.ge.3) then
*           use recursion
            do i=nprime-2,1,-1
              j=i+2
              cprime(j)=cprime(j+2)+dble(2*i)*wcf1(j+1)*dnorm
            end do
          end if
*         first two are endpoints
          cprime(2)=wcf1(2)
          cprime(1)=wcf1(1)
        end if
*     
*       if(wcftype(1:4).eq.'line') then
*         linear, so no need to iterate
        do while (dabs(dxd).gt.errtol.and.nitn.le.nitnx)
          nitn=nitn+1
          cex=ced-dble(noffs)
          if(wcftype(1:4).eq.'cheb') then
*	    Chebyschev polynomial evaluation of derivative
            t1=chebd(cprime,n,cex)
           else
*	    assume a polynomial fit of order nwco-1
            t1 = wcf1(n)*dble(n-1)
            do i = n-1, 2, -1
              t1 = t1 * cex + wcf1(i)*dble(i-1)
            end do
          end if
          dxd=(wval(ced)-wvd)/(t1*helcfac*derav)
          ced=ced-dxd
        end do
        if(nitn.ge.nitnx) then
          write(6,'('' Exceeded  '',i6,'' iterations in CHANWAV'')') 
     1        nitnx
          write(6,*) wcftype(1:4),'  ',wvd,ced,nwco
          write(6,*) wcf1(1),wcf1(2)
        end if
      end if
      return
      end
      double precision function chebd(cs,n,x)
      implicit none
      integer n
      double precision cs(n),x
*     Cheb polynomial evaluation using Clenshaw's formula
*     NAG routine E02 de-nagged again
      integer i
      double precision d1,d2,v1,temp
      v1=(2.0d0*x-cs(2)-cs(1))/(cs(2)-cs(1))
      d1=0.0d0
      d2=0.0d0
      do i=n,4,-1
        temp=d1
        d1=2.0d0*v1*d1-d2+cs(i)
        d2=temp
      end do
      chebd=v1*d1-d2+0.5d0*cs(3)
      return
      end
