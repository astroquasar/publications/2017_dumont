      subroutine vp_plcset(chc,ngp)
*
      implicit none
      include 'vp_sizes.f'
*     IN:
*     ngp	integer	data array length
*     OUT
*     chc	char*1	plot control (n if replot, y if OK)
      character*1 chc
      integer ngp
*     Local
      character*1 chcx
      integer ntot,ntx
      double precision ddum,wl
*     
      real x1pg,y1pg            ! pgplot variables
      common/vpc_gcurv/x1pg,y1pg
      integer nwco
      double precision ac(maxwco)
      common/vpc_wavl/ac,nwco
      integer l1,l2,lc1,lc2
      real fampg
      common/ppam/l1,l2,lc1,lc2,fampg
c     min and max yscales by hand, and default lower level
      integer nyuse
      real ylowhpg,yhihpg,yminsetpg
      common/pgylims/ylowhpg,yhihpg,yminsetpg,nyuse
*
*
 1    call vp_pgcurs(x1pg,y1pg,chc)
*
*     help requested, so:
      if(chc.eq.'?') then
        write(6,*) 'Left mouse button, or "e", expands plot'
        write(6,*) 'Center button, or "r" replots'
        write(6,*) 'Right button, or "q" to exit'
        write(6,*) '"." shift range up'
        write(6,*) '"," shift range down'
        write(6,*) '"a" plot whole array'
        write(6,*) '"d" demagnify by factor 2'
        write(6,*) '"y" max y from cursor'
        write(6,*) '"Q" abandon this region'
        write(6,*) '.. any other lower case letter for'//
     :           'command line prompt'
        goto 1
      end if
c     replot is centre button
      if(chc.eq.'D'.or.chc.eq.'r') then
        chc='n'
        goto 3
      end if
c     Exit is right button:
      if(chc.eq.'X'.or.chc.eq.'q') then
        chc='y'
        goto 3
      end if
*	
c     e: set edges of display from cursor & left button, and replot
      if(chc.eq.'A'.or.chc.eq.'e') then
c       wavelength to channel conversion, rfc 13.4.92
        wl=dble(x1pg)
        if(ac(2).ge.0.0d0) then
          ddum=(wl-ac(1))/ac(2)
         else
          ddum=1.0d3
        end if
        call chanwav(wl,ddum,1.0d-3,100)
        l1 = nint( ddum )
        write(6,*) ' Channel',l1
        if(chc.eq.'A') then
*       set scale range as well
          lc1=l1
        end if
        write(6,*) 'right edge..'
        call vp_pgcurs(x1pg,y1pg,chcx)
        wl=dble(x1pg)
        if(ac(2).ge.0.0d0) then
          ddum=(wl-ac(1))/ac(2)
         else
          ddum=1.0d3
        end if
        call chanwav(wl,ddum,1.0d-3,100)
        l2 = nint( ddum )
        write(6,*) ' Channel',l2
        if(chc.eq.'A') then
*       set max for scaling as well
          lc2=l2
        end if
        chc='n'
        goto 3
      end if
*
*     other commands similar to IRAF splot
*
*     a: plot full range
      if(chc.eq.'a') then
        l1=1
        lc1=1
        l2=ngp
        lc2=ngp
        chc='n'
        goto 3
      end if
*
*     .: step up
      if(chc.eq.'.') then
        if(l2.ge.ngp) then
*         no point in doing this, so don't
          write(6,*) 'Max channel already plotted'
         else
          ntot=l2-l1
          ntx=ntot/10
          l1=l2-ntx
          l2=l1+ntot
          if(l2.gt.ngp) then
            l2=ngp
            l1=l2-ntot
            write(6,*) 'Max channel displayed'
          end if
          lc1=l1
          lc2=l2
        end if
        chc='n'
        goto 3
      end if
*
*     ,: step down
      if(chc.eq.',') then
        if(l1.le.1) then
*         no point in doing this, so don't
          write(6,*) 'Min channel already plotted'
         else
          ntot=l2-l1
          ntx=ntot/10
          l2=l1+ntx
          l1=l2-ntot
          if(l1.lt.1) then
            l1=1
            l2=ntot+1
            write(6,*) 'Minimum channel displayed'
          end if
          lc1=l1
          lc2=l2
        end if
        chc='n'
        goto 3
      end if
*
*     d: plot twice the range, centered on old value
      if(chc.eq.'d') then
        lc1=l1
        l1=3*l1/2-l2/2
        if(l1.lt.1) l1=1
        l2=3*l2/2-lc1/2+2
        if(l2.gt.ngp) l2=ngp
        lc1=l1
        lc2=l2
        chc='n'
        goto 3
      end if
*
*     y: set y max scale
      if(chc.eq.'y') then
        yhihpg=y1pg
        nyuse=1
        chc='n'
        goto 3
      end if
*	
*     Q: quit region
      if(chc.eq.'Q') then
*       character passed top calling routine to quit region
         goto 3
      end if
*
*     exit section
 3    return
      END
