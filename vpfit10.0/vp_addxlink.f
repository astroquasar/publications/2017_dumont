      subroutine vp_addxlink(ionx,levx,zed,bval,aln,nfunc,ion,level,
     :            np,parm,ipind, kxl)
*
*     add lines if appropriate from the cross-link list
*     IN:
*     ionx    ch*2    base element
*     levx    ch*4    ionization level
*     zed     dbl     redshift for added system
*     bval    dbl     Doppler parameter for added system
*     alm     dbl     column density for added system
*     IN & OUT:
*     nfunc   int     number of systems (including the base new one)
*     np      int     number of parameters (=nfunc*noppsys)
*     ion     ch*2 array   element list
*     level   ch*4 array   ionization level list
*     parm    dble array   parameters
*     ipind   ch*2 array   tie indicators
*     OUT:
*     kxl     int     number of extra systems added
*
*     rfc 23.03.05
*
      implicit none
      include 'vp_sizes.f'
*     subroutine parameters
      character*2 ionx
      character*4 levx
      integer np,nfunc,kxl
      double precision zed,bval,aln
      character*2 ion(maxnio)
      character*4 level(maxnio)
      double precision parm(*)
      character*2 ipind(*)
*     local variables
      logical lionok
      character*2 ipvz,ipvb
      integer j,jxl,jxm,npx,nmm,ipbvind
*
*     forced cross-linked ions, with given guessed column density offsets
      integer nionxl
      character*2 ielxl(8)
      character*4 ionxl(8)
      double precision crlogxl(8)
      common/vpc_xlion/ielxl,ionxl,crlogxl,nionxl
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     atomic mass table
      character*2 lbm
      double precision amass
      integer mmass
      common/vpc_atmass/lbm(maxats),amass(maxats),mmass
*     Log N if indvar=1, N otherwise (-1 for emission)
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
*     supplementary fit variables
      double precision vturb,atmass,temper,fixedbth
      common/vpc_ucopder/vturb(maxnio),atmass(maxnio),
     :                 temper(maxnio),fixedbth(maxnio)
*     general control variables
      character*4 chcv(10)
      common/vpc_chcv/chcv
*
      kxl=0
      if(nionxl.gt.0) then
        do jxl=1,nionxl
          if(ionx.eq.ielxl(jxl).and.levx.eq.ionxl(jxl)) then
*           find next tied indicators in ascending alphabetical order
            call vp_newtfxl(ipind,nfunc, ipvz,ipvb,ipbvind)
*           update tied flags
            npx=np-noppsys
            ipind(npx+nppzed)=ipvz
            ipind(npx+nppbval)=ipvb
*           convert ipvz to upper case
            ipvz(1:1)=char(ichar(ipvz(1:1))+ichar('A')-ichar('a'))
            ipvz(2:2)=char(ichar(ipvz(2:2))+ichar('A')-ichar('a'))
            if(ipbvind.eq.1) then
              ipvb(1:1)=char(ichar(ipvb(1:1))+ichar('A')-ichar('a'))
              ipvb(2:2)=char(ichar(ipvb(2:2))+ichar('A')-ichar('a'))
            end if
*           add in the others in the list with appropriate
*           Doppler parameters and column densities, and adjust
*           tied parameters accordingly
            if(chcv(7)(1:4).eq.'turb') then
              temper(nfunc)=0.1d0
             else
              temper(nfunc)=0.0d0
            end if
*
            kxl=0
            do jxm=1,nionxl
              if(ionx.ne.ielxl(jxm).or.levx.ne.ionxl(jxm)) then
                call vp_ionregcheck(ionx,levx,zed,lionok)
                if(lionok) then
                  kxl=kxl+1
                  nfunc=nfunc+1
                  ion(nfunc)=ielxl(jxm)
                  level(nfunc)=ionxl(jxm)
*                 need to determine atmass
                  nmm=1
                  do while (nmm.lt.mmass.and.lbm(nmm).ne.ion(nfunc))
                    nmm=nmm+1
                  end do
*                 correct ion, or run out of ions so use last
                  atmass(nfunc)=amass(nmm)
                  vturb(nfunc)=0.0d0
                  if(chcv(7)(1:4).eq.'turb') then
                    temper(nfunc)=0.1d0
                   else
                    temper(nfunc)=0.0d0
                  end if
*                 Other parameters
                  parm(np+nppzed)=zed
                  ipind(np+nppzed)=ipvz
                  parm(np+nppbval)=bval
                  ipind(np+nppbval)=ipvb
                  if(indvar.ne.0) then
*                 column density is logs
*                   convert 'real' values to internal ones
                    parm(np+nppcol)=scalelog*
     :                        (aln+crlogxl(jxm)-crlogxl(jxl))
                   else
*	            /cm^2 units for column density
                    parm(np+nppcol)=scalefac*10.0d0**
     :                    (aln+crlogxl(jxm)-crlogxl(jxl))
                  end if
                  ipind(np+nppcol)='  '
                  if(noppsys.gt.3) then
                    do j=4,noppsys
                      parm(np+j)=0.0d0
                      ipind(np+j)='  '
                    end do
                  end if
                  if(kxl.eq.1.and.nionxl.gt.2.and.ipbvind.eq.0) then
*                   capitalize ipvb if not already done
                    ipvb(1:1)=
     :                char(ichar(ipvb(1:1))+ichar('A')-ichar('a'))
                    ipvb(2:2)=
     :                char(ichar(ipvb(2:2))+ichar('A')-ichar('a'))
                  end if
                  np=np+noppsys
                end if
              end if
            end do
          end if
        end do
        if(kxl.le.0) then
*         no accepted extra lines, so remove tie characters
          ipind(np-noppsys+nppzed)='  '
          ipind(np-noppsys+nppbval)='  '
        end if
      end if
      return
      end
