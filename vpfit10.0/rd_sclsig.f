      subroutine rd_sclsig(de,nct,status)
*
      implicit none
      include 'vp_sizes.f'
*     
*     rescale the sigma array using the SIGSCL parameters
*
      integer nct,status
      double precision de(*)
*     Local:
      integer j
      double precision temp
*     Functions
      double precision f_sigscl
*     individual file scale coeffts
      integer nscl
      double precision ssclcf,wvalsc
      common/sigsclcft/ssclcf(maxtas,maxnch),wvalsc(maxtas,maxnch),
     1                   nscl(maxnch)
      if(status.eq.0) write(6,*) nscl(1),nct
*
      do j=1,nct
        temp=f_sigscl(j,1)
        if(temp.le.0.0d0) temp=1.0d0
        de(j)=de(j)/(temp*temp)
      end do
      return
      end


