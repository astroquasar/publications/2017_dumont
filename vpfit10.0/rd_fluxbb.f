      subroutine rd_fluxbb(da,n)
*     generate a bb spectrum given mag and temperature
*     for use in IR flux calibration, result is fnu
*     in mJy
*
      implicit none
*     variables
      integer n
      double precision da(n)
*
      integer i,nv
      double precision flam,fref,wave,temp,texp,twave
      character*132 inchstr
      character*40 cv(4)
      double precision rv(4)
      integer iv(4)
*     functions
      double precision wval
*
 1    write(6,*) 'Magnitude: band, value?'
      read(5,'(a)') inchstr
      call dsepvar(inchstr,2,rv,iv,cv,nv)
      if(rv(1).ne.0.0) then
        rv(2)=rv(1)
        cv(1)='k'
      end if
      if(cv(1)(1:1).eq.'J'.or.cv(1)(1:1).eq.'j') then
        flam=164d4*10.0d0**(-0.4d0*rv(2))
        wave=12000.0d0
       else
        if(cv(1)(1:1).eq.'H'.or.cv(1)(1:1).eq.'h') then
          flam=103d4*10.0d0**(-0.4d0*rv(2))
          wave=16400.0d0
         else
          if(cv(1)(1:1).eq.'K'.or.cv(1)(1:1).eq.'k') then
            flam=65d4*10.0d0**(-0.4d0*rv(2))
            wave=22000.0d0
           else
            goto 1
          end if
        end if
      end if
 2    write(6,*) 'Temperature (K) ?'
      read(5,'(a)') inchstr
      call dsepvar(inchstr,1,rv,iv,cv,nv)
      temp=rv(1)
      if(temp.le.100.0d0) goto 2
      texp=1.43883d8/(wave*temp)
      fref=1.0d0/(wave*wave*wave*(exp(texp)-1.0d0))
*	
 4    write(6,*) 'Form for calculated spectrum [mJy]'
      read(5,'(a)') inchstr
      if(inchstr(1:1).eq.'?') then
        write(6,*) 'Can be: mJy, fnu (cgs), flam (cgs)'
        goto 4
      end if
*     cycle through the wavelengths
      do i=1,n
        twave=wval(dble(i))
        if(twave.le.0.0d0) goto 3
        if(twave.lt.10.0d0) twave=twave*1.0d4
        da(i)=flam/(fref*twave*twave*twave*
     1          (exp(1.43883d8/(twave*temp))-1.0d0))
      end do
*
*     optional scales
      if(inchstr(1:3).eq.'fnu'.or.inchstr(1:3).eq.'FNU') then
        do i=1,n
          da(i)=da(i)*1.0d-26
        end do
      end if
      if(inchstr(1:3).eq.'fla'.or.inchstr(1:3).eq.'FLA') then
        do i=1,n
          twave=wval(dble(i))
          da(i)=da(i)*3.0d-8/(twave*twave)
        end do
      end if
 3    continue
      return
      end
