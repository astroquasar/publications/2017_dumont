      subroutine rd_ulinfo(da,de,ca,nct,chout,lgulio)
*     using the data, continuum and error arrays, and a line list chlist
*     write out the number of pixels for each line in the list for which
*     the data is used in rd_ulims(e).
*     IN:
*     da(nct) double  data
*     de(nct) double  error^2
*     ca(nct) double  continuum
*     nct     integer array length used
*     chout   char    file for output
*     lgulio   logical .true. for user I/O, .false. => all internal
*
      implicit none
      include 'vp_sizes.f'
*
*     subroutine parameters
      integer nct
      double precision da(nct),de(nct),ca(nct)
      character*(*) chout
      logical lgulio
*
*     local variables
      character*2 elvb
      character*4 levb
      character*64 chlist
      integer nopch,ntemp
      integer nend,nrest,nexts,nchlob,nchhib
      integer i,j,ntlo,nthi,nrglo,nrghi,ncloc
      double precision dtemp,ddmax,ddmaxh,ddtemp
      double precision wlo,whi,wvlob,wvhib
      double precision bwidth,wvmid,chsultn
      double precision wvrest(120)
*
*     functions
      integer lastchpos
*
*     character string handling and unpacking
      character*132 inchstr
      character*60 cv(24)
      real rv(24)
      integer iv(24)
      integer nv
      common/rd_chwork/inchstr,rv,iv,cv,nv
      double precision dv(24)
      common/vpc_dsepspace/dv
*     input streams
      integer inputc
      common/rdc_inputc/inputc
*     atomic data
      integer m
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),m
*     upper limit defaults, updated by routines which use them
      character*4 ctypeul
      character*60 chwv1
      logical lcminul
      integer nminfd,nminfdef
      double precision xnul,xbul,pchslim,dfwhmkms,bvalmins 
      double precision wloddr,whiddr
      common/rdc_ulvsets/xnul,xbul,pchslim,dfwhmkms,bvalmins,
     :     wloddr,whiddr,nminfd,nminfdef,ctypeul,chwv1,lcminul
*     upper limit results
      character*6 ionul
      double precision zvalul,bvalul,cvalul
      common/rdc_ulrparm/zvalul,bvalul,cvalul,ionul
*     upper limit internal communication
      logical lulprint
      integer nctul
      double precision chsult
      common/rdc_ulintp/chsult,nctul,lulprint
*
*     help
      if(chout(1:1).eq.'?') then
*       print help and exit only
        write(6,*) 
     :   'To see which lines contribute to the column density upper'
        write(6,*) 'limit determination:'
        write(6,*) 'Determine the upper limit ("uc s") Doppler'
        write(6,*) '  parameter and column density using a file of'
        write(6,*) '  absorption line rest wavelengths'
        write(6,*) 'Insert those lines in the continuum ("gp s")'
        write(6,*) '  for that column density and Doppler parameter'
        write(6,*) 'Use this ("ul") to list the number of pixels'
        write(6,*) '  used per line in determining the constraint'
        write(6,*) '  (lines with zero pixels are not listed)'
        write(6,*) ' '
        write(6,*) '"ul ?" for help, "ul <filename>" for o/p to file'
        write(6,*) '"ul" for o/p to screen'       
*       branch to exit
        goto 261
      end if
*     defaults
      if(lgulio) then
        write(6,'(a)') 'Variable names as for "uc"'
        write(6,'(a,3f6.1,a)')
     1    'xn,xb,vresfwhm [',xnul,xbul,dfwhmkms,']'
        read(inputc,'(a)') inchstr
        call dsepvar(inchstr,3,dv,iv,cv,nv)
        if(dv(1).gt.0.0d0) then
          xnul=dv(1)
        end if
        if(dv(2).gt.0.0d0) then
          xbul=dv(2)
        end if
        if(dv(3).gt.0.0d0) then
          dfwhmkms=dv(3)
        end if
       else
        write(6,'(a,3f6.1)') 'xn,xb,fwhm ',xnul,xbul,dfwhmkms
      end if
*
201   continue
      if(lgulio) then
        write(6,'(a)') 'ion,redshift,bval,linefile'
        nend=lastchpos(chwv1)
        write(6,'(a,a,f12.8,f8.2,a,a)') '[',ionul,zvalul,bvalul,
     :    ' '//chwv1(1:nend),']'
        read(inputc,'(a)') inchstr
        call dsepvar(inchstr,4,dv,iv,cv,nv)
        if(cv(1)(1:1).eq.' ') then
          cv(1)=ionul
        end if 
        call vp_chsation(cv(1),cv(2),elvb,levb,nexts)
        if(nexts.gt.1) then
          write(6,*) 'No space between atom and ion please'
          goto 201
        end if
        ionul=cv(1)(1:6)
        if(dv(2).gt.0.0d0) then
          zvalul=dv(2)
        end if
        if(dv(3).gt.0.0d0) then
          bvalul=dv(3)
        end if
        if(cv(4)(1:1).ne.' ') then
          chwv1=cv(4)
        end if
        chlist=chwv1
        if(chlist(1:1).eq.' ') then
          write(6,*) 'No line list file'
          goto 201
        end if
        nend=lastchpos(chlist)
       else
        chlist=chwv1
        nend=lastchpos(chlist)
        write(6,'(a,a,f12.8,f8.2,a,a)') 
     :       'using: ',ionul,zvalul,bvalul,' ',chlist(1:nend)
      end if
      wlo=0.0d0
      whi=1.0d25
      if(lgulio) then
        if(wloddr.le.1.0d-3.and.whiddr.gt.1.0d24) then
          write(6,*) 'observed wavelength range (low, high) [all]'
         else 
          write(6,*) 'observed wavelength range (low, high)'
          write(6,'(a,2f10.2,a)') '         [',wloddr,whiddr,']'
        end if
        read(inputc,'(a)') inchstr
        call dsepvar(inchstr,2,dv,iv,cv,nv)
        if(cv(1)(1:1).ne.' ') then
          wloddr=dv(1)
        end if
        if(cv(2)(1:1).ne.' '.and.dv(2).gt.wlo) then
          whiddr=dv(2)
        end if
       else
        write(6,'(a,2f10.2)') 'wavelength range',wloddr,whiddr
      end if
      wlo=wloddr
      whi=whiddr
      if(wlo.gt.1.0d-3) then
        call vp_chanwav(wlo,ddtemp,5.0d-2,20,1)
        nrglo=max(1,int(ddtemp))
       else
        nrglo=1
      end if
      if(whi.lt.1.0d24) then
        call vp_chanwav(whi,ddtemp,5.0d-2,20,1)
        nrghi=min(nct,int(ddtemp)+1)
       else
        nrghi=nct
      end if
*
*     read in the line list, including only those within range
      open(unit=18,file=chlist(1:nend),status='old',err=201)
      rewind(unit=18)
      nrest=0
202   read(18,'(a)',end=203) inchstr
      call dsepvar(inchstr,2,dv,iv,cv,nv)
      if(dv(1).le.1.0d-3) then
        if(dv(2).gt.1.0d-3) then
          dtemp=dv(2)*(1.0d0+zvalul)
          if(dtemp.ge.wlo.and.dtemp.le.whi) then
            nrest=nrest+1
            wvrest(nrest)=dv(2)
           else
            goto 202
          end if
         else
          goto 203
        end if
       else
        dtemp=dv(1)*(1.0d0+zvalul)
        if(dtemp.ge.wlo.and.dtemp.le.whi) then
          nrest=nrest+1
          wvrest(nrest)=dv(1)
         else
          goto 202
        end if
      end if
      if(nrest.lt.120) goto 202
*     convert possibly approximate wavelengths to accurate ones
*     first set table limits so don't search everything!
203   ntlo=0
      nthi=m
      close(unit=18)
      do i=1,m
        if(elvb.eq.lbz(i).and.levb.eq.lzz(i)) then
          if(ntlo.eq.0) then
            ntlo=i
          end if
          nthi=i
        end if
      end do
*     now convert to accurate wavelengths
      do j=1,nrest
        ddmaxh=1.0d20
        ddmax=1.0d21
        do i=ntlo,nthi
          if(elvb.eq.lbz(i).and.levb.eq.lzz(i)) then
            ddmax=abs(alm(i)-wvrest(j))
            if(ddmax.lt.ddmaxh) then
              ddmaxh=ddmax
              ddtemp=alm(i)
            end if
          end if
        end do
        wvrest(j)=ddtemp
      end do
*
*     channels included in statistic:
      bwidth=sqrt(bvalul*bvalul+0.36d0**dfwhmkms*dfwhmkms)
      ntemp=0
      nopch=6
      if(chout(1:1).ne.' ') then
        ntemp=lastchpos(chout)
        write(6,*) 'Output to: ',chout(1:ntemp)
        open(unit=19,file=chout(1:ntemp),status='unknown',err=231)
        nopch=19
      end if
231   nctul=0
      chsult=0.0d0
      do j=1,nrest
        wvmid=(1.0d0+zvalul)*wvrest(j)
        wvlob=(1.0d0-xbul*bwidth/2.99792458d5)*wvmid
        wvhib=(1.0d0+xbul*bwidth/2.99792458d5)*wvmid
        call vp_chanwav(wvlob,ddtemp,5.0d-2,20,1)
        nchlob=max(nrglo,int(ddtemp)-1)
        call vp_chanwav(wvhib,ddtemp,5.0d-2,20,1)
        nchhib=min(nrghi,int(ddtemp)+1)
        ncloc=0
        if(nchlob.le.nchhib) then
          do i=nchlob,nchhib
            if(de(i).gt.0.0d0) then
              if(ca(i)-da(i).le.xnul*sqrt(de(i))) then
                ncloc=ncloc+1
                nctul=nctul+1
                chsult=chsult+(ca(i)-da(i))**2/de(i)
              end if
            end if
          end do
        end if
        if(ncloc.gt.0) then
          nchlob=nchhib-nchlob+1
          write(nopch,'(a,a,2f9.2,i5,a,i5)') 
     :         elvb,levb,wvrest(j),wvmid,ncloc,' /',nchlob
        end if
      end do
      write(nopch,'(a,i6,a,f10.2)') 'Total data channels:',nctul,
     :     '    chisq=',chsult
      chsultn=chsult/dble(max(nctul-1,1))
      write(6,'(a,f10.2)') 'Normalized chisq=',chsultn
      if(nopch.eq.19) close(unit=19)
*
261   return
      end
