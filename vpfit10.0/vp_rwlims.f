      subroutine vp_rwlims(dh,dhe,drms,re,ngp,klow,khiw,wvmean)
*     subroutine to set wavelength limits to a region to
*     be fitted, using the cursor

c     INPUT:
c     dh	dble array 	data of length ngp
c     dhe	dble array	variance
c     re	dble array	continuum
c     ngp	integer
c
c     OUTPUT:
c     klow	integer		start channel for region
c     khiw	integer		end channel
c     wvmean	dble		mean wavelength
*
*
      implicit none
      include 'vp_sizes.f'
*
*     subroutine arguments:
      integer ngp,klow,khiw
      double precision dh(*),dhe(*),re(*),drms(*)
      double precision wvmean

*     PGPLOT variables
      real wv2pg                  ! pgplot variable
*
*     Local
      character*1 icx,chc
      integer ind,ngcount
      double precision wvd,chand
*
*     Common:
      integer ipgopen,ipgflag
      common/pgplotv/ipgopen,ipgflag
*     wavelengths
      integer nwco
      double precision ac(maxwco)
      common/vpc_wavl/ac,nwco
      character*4 chcv(10)
*     chcv(1) controls use of cursor for input guesses N,b,z
      common/vpc_chcv/chcv
*     last positions of cursor, for convenience
      real wv1pg,yvlpg              ! pgplot variables
      common/vpc_gcurv/wv1pg,yvlpg
*     nst: start channel, ncn: end channel, lc1, lc2 start, end for scale
      integer nst,ncn,lc1,lc2
      real fampg
      common/ppam/nst,ncn,lc1,lc2,fampg
*     chunk variables:
      integer ndpts(maxnch),idrun(maxnch),icrun(maxnch)
      integer indfil(maxnch)
      character*64 filename(maxnch)
      double precision wvstrt(maxnch),wvend(maxnch)
      integer nchunk
      common/vpc_chunk/wvstrt,wvend,ndpts,filename,
     :      idrun,icrun,indfil,nchunk
*
      write(6,'('' Plot? [y] '')')
      write(6,'(''> '',$)')
      read(5,'(a)') icx
      ind=1
      ngcount=0
*     restrict plot range to be sensible, i.e. not beyond the data!
      if(ncn.gt.ngp) then
        ncn=ngp
        if(nst.ge.ncn) then
          nst=1
        end if
      end if
*
      if(icx.ne.'n'.and.icx.ne.'N') then
        chc='n'
        do while (chc.ne.'y'.and.chc.ne.'Y'.and.chc.ne.'q'.and.
     :            chc.ne.'Q'.and.chc.ne.' ')
          call splot(dh,dhe,drms,re,ngp,ind)
          if(chcv(1).eq.'gcur') then
            ngcount=ngcount+1
            if(ngcount.le.1) then
              write(6,*) 'Expand plot if needed:'
              write(6,*) 'Cursor ("e" or left button at edges,'
              write(6,*) ' "q" or right button when OK)'
            end if
            write(6,*) 'Cursor ("e" edges, "q" OK)'
            ind=1
            call vp_plcset(chc,ngp)
*           write(6,*) 'code ',chc
*           suppress call to plset if buttons used:
            if(chc.eq.'n'.or.chc.eq.'y') ind=2
*           abandon this region
            if(chc.eq.'q'.or.chc.eq.'Q') then
              klow=-1
              khiw=-1
              return
            end if
           else
            write(6,*) ' correct wavelength region?'
            write(6,*) ' y(es), n(o) or q(uit) [y]'
            read(5,'(a)') chc
            if(chc.eq.'q'.or.chc.eq.'Q') then
              klow=-1
              khiw=-1
              return
            endif
          end if
        end do
      end if
c
      if(ipgopen.ne.0.and.ipgflag.eq.1) then
*       use cursor
        write(6,*) 'Mark region in which data is to be fitted:'
        write(6,
     :    '('' left limit (space, or left button, when ready)'')')
        call vp_pgcurs(wv1pg,yvlpg,chc)
        write(6,'('' right limit'')')
        wv2pg=wv1pg
        call vp_pgcurs(wv2pg,yvlpg,chc)
       else
        write(6,'('' Data not plotted'')')
        write(6,'('' enter left and right limit wavelengths'')')
        write(6,'(''> '',$)')
        read(5,*) wv1pg,wv2pg
      end if
*     store these limits against chunk variables
      wvstrt(nchunk)=dble(wv1pg)
      wvend(nchunk)=dble(wv2pg)
*
*     klow & khiw are channel limits for chi**2 determination
*     compute from the input wavelengths:
      wvd=dble(wv1pg)
      chand=(wvd-ac(1))/ac(2)
      call vp_chanwav(wvd,chand,5.0d-2,20,nchunk)
      wvmean=wvd
      klow=int(chand+1.0d0)
      if(klow.le.0) then
        write(6,*) 'Lower limit set to first data point'
        klow=1
      end if
      wvd=dble(wv2pg)
      wvmean=0.5d0*(wvmean+wvd)
      chand=(wvd-ac(1))/ac(2)
      call vp_chanwav(wvd,chand,5.0d-2,20,nchunk)
      khiw=int(chand)
      if(khiw.gt.ngp) then
        write(6,*) 'Upper limit set to array maximum',ngp
        khiw=ngp
      end if
      write(6,'('' Region limits:'',f10.3,''A (channel'',i6,
     1   '') - '',f10.3,''A ('',i6,'')'')') wv1pg,klow,wv2pg,khiw
*
*
      return
      end
