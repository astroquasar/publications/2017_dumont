      subroutine vp_listfiles(pchstr,intwav,zed,dfilnm,ndord)
*
*     list the files for a specified ion using the contents of
*     vp_files.dat. Exit if this file does not exist.
*
      implicit none
      include 'vp_sizes.f'
*
      character*(*) pchstr,dfilnm
      integer intwav
      double precision zed
      integer ndord
*
*     LOCAL declarations:
      integer i,iw,j,jb,jjj,next,nflin,nl
      character*2 atom
      character*4 ion
      logical fthere
      double precision wavx,zp1
*
*     common declarations:
      integer nvstr
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)            ! sepspace variable
      integer ivstr(24)
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
      double precision dvstr(24)
      common/vpc_dsepspace/dvstr
      character*60 cvhold
      common/vpc_cvhold/cvhold
*
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm
      integer nz 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),nz
*
      integer idwk(74)
      common/works/idwk
*     character collating sequence ranges
      integer nchlims
      common/vpc_charlims/nchlims(6)
*     
*     open file as unit 14, and branch to end if it does not exist.
      inchstr=' '
      call getenv ('VPFFILES',inchstr)
      if(inchstr.eq.' ') then
        inchstr='vp_files.dat'
       else
        inquire(file=inchstr,exist=fthere)
        if( .not. fthere) then
*         file does not exist, so set the default and warn user:
          write(6,*) ' Could not find VPFFILES file ',inchstr(1:40)
          write(6,*) '    trying vp_files.dat'
          inchstr='vp_files.dat'
        end if
      end if
      open(unit=14,file=inchstr,status='old',err=91)
*
*     file exists, so ask for ion and redshift
      if(pchstr(1:1).ne.' ') then
        inchstr=pchstr
       else
        write(6,*) ' Ion, redshift?'
        read(5,'(a)') inchstr
*       strip out any spare commas and replace by spaces: 17.11.94 rfc
        do jjj=1,132
          if(inchstr(jjj:jjj).eq.',') inchstr(jjj:jjj)=' '
        end do
      end if
*
*     determine the element 
*     nchlims used, with HD, CO exceptions allowed for
      if(ichar(inchstr(2:2)).ge.nchlims(3).and.
     :     ichar(inchstr(2:2)).le.nchlims(4).and.
     :     inchstr(1:2).ne.'HD'.and.inchstr(1:2).ne.'CO') then
        atom=inchstr(1:1)//' '
        next=2
       else
        atom=inchstr(1:2)
        next=3
      end if
*
*     Now look for the ion:
      j=next
      do while(inchstr(j:j).ne.' '.and.j.lt.132)
        j=j+1
      end do
*
      jb=j-1
*
      if(jb.ge.next) then
        ion=inchstr(next:jb)
       else
        ion='    '
      end if
      next=j
*
      if(pchstr(1:1).ne.' ') then
*       redshift was a parameter to the subroutine
        zp1=1.0d0+zed
       else
*       separate the rest of the string
        call dsepvar(inchstr(next:132),1,dvstr,ivstr,cvstr,nvstr)
*
*       redshift should be dvstr(1)
        zp1=1.0d0+dvstr(1)
      end if
*     write(6,*) atom,ion,intwav,zp1
*
*     search for appropriate lines
      nflin=0
      do i=1,nz
        if(atom.eq.lbz(i).and.ion.eq.lzz(i)) then
          nflin=nflin+1
          idwk(nflin)=i
        end if
      end do
      if(nflin.le.0) goto 91
*
*     search for and list lines
      nl=0
 1    read(14,'(a)',end=92) inchstr
*     1: datafile, 2: order #, 3: wlow, 4: whigh
      call dsepvar(inchstr,4,dvstr,ivstr,cvstr,nvstr)
      if(nvstr.lt.4) then
        cvstr(1)=cvhold
        ivstr(2)=ivstr(1)
        if(nvstr.eq.2) then
          ivstr(2)=1
          dvstr(4)=dvstr(2)
          dvstr(3)=dvstr(1)
         else
          if(ivstr(2).eq.0) then
            ivstr(2)=1
            cvhold=cvstr(1)
          end if
          dvstr(4)=dvstr(3)
          dvstr(3)=dvstr(2)
        end if
       else
        cvhold=cvstr(1)
      end if
*
      do j=1,nflin
        wavx=zp1*alm(idwk(j))
        iw=int(alm(idwk(j)))
        if(wavx.ge.dvstr(3).and.wavx.le.dvstr(4)) then
          if(intwav.lt.1.or.iw.eq.intwav) then
*           found one! so print it out
            nl=nl+1
            if(nl.eq.1) then
*             print header
              write(6,
     1            '(''  File'',15x,''order ion      lam0      f'''//
     2            '''    @lam   low    high'')')
            end if
            write(6,
     1          '(1x,a20,i3,3x,a2,a4,1x,f8.2,1x,f6.3,1x,3f7.1)')
     2          cvstr(1),ivstr(2),atom,ion,alm(idwk(j)),fik(idwk(j)),
     3          wavx,dvstr(3),dvstr(4)
            dfilnm=cvstr(1)
            ndord=ivstr(2)
          end if
        end if
      end do
      goto 1
*
 92   close(unit=14)
      if(nl.le.0) write(6,*) ' .. no lines found'
      goto 93
 91   write(6,*) ' vp_files.dat does not exist'
 93   return
      end
