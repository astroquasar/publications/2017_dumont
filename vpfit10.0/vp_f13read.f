      subroutine vp_f13read(if18,nopt,nn,ndp,parm,ion,level)
      implicit none
      include 'vp_sizes.f'
*     read data from file (unit 13) for datafiles, chunks,
*     initial guesses etc
*     local variables
*     resnf13 = .true. if resn set from fort.13 and want to over-ride
*     presets/ header determined values
*     
*     IN:
*     if18    integer         for.18 indicator
*     nopt	integer		option number for read type
*
*     OUT:
*     nn	integer		number of parameters
*     ndp	integer array	array lengths for data
*     parm	dble array	parameter list
*     ion	char*2 array	atomic species list
*     level	char*4 array	ionization level list
*
*     subroutine arguments
      integer if18,nopt
      integer nn
      integer ndp(maxnfi)
      double precision parm(maxnpa)
      character*2 ion(maxnio)
      character*4 level(maxnio)
*     Local:
      integer iii,ik,ich,iftemp,ios
      integer j,jjk,k,ki,kich,koffset,ktemp
      integer lchstrt,lchend,lfl,lensig
      integer nlines
      integer nchk,nfl,nindex,nvarin,nvlim,nvtot
      logical resnf13 
      double precision ddum,dtemp,xlamx
      double precision tempxx
*     region start and end wavelengths (and local work value)
      double precision wdrend,wdrstart,w1
      double precision oldsig,olddl
      double precision dlinst,dlpix
      double precision sigmak,dlamk
*     local over-ride resolution variables
      integer nwrtemp
      double precision swrtemp(maxpoc)
*
*     free input declarations
      logical lendlist
*     FUNCTIONS:
      double precision wval,vp_wval,f_sigscl
*
*     COMMON variables
      logical verbose
      common/vp_sysout/verbose
      integer nverbose
      common/vp_sysmon/nverbose
*     chunk variables:
      integer ndpts(maxnch),idrun(maxnch),icrun(maxnch)
      integer indfil(maxnch)
      character*64 filename(maxnch)
      double precision wvstrt(maxnch),wvend(maxnch)
      integer nchunk
      common/vpc_chunk/wvstrt,wvend,ndpts,filename,
     :      idrun,icrun,indfil,nchunk
*     hold first key word as well
      character*32 chkey(maxnch)
      common/vpc_chunkey/chkey
*     general string handling space
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
      double precision dvstr(24)
      common/vpc_dsepspace/dvstr
*
      character*132 chstrtem
      common/septemp/chstrtem
*     fit to resolution, using sigmas
      integer nswres(maxnch)
      double precision swres(maxpoc,maxnch)
      common/vpc_sigwres/swres,nswres
*     chunk start and end wavelengths
      double precision wvchlo(maxnch),wvchhi(maxnch)
      common/vpc_chunklims/wvchlo,wvchhi
*     
      integer ngd,npexsm
      integer numpts(maxnch)
      double precision wcoeff(maxnch)
      common/vpc_jkw3/wcoeff,ngd,npexsm,numpts
      double precision sepvd(maxnch)
      common/vpc_sepvd/sepvd
      integer ichstrt(maxnch),ichend(maxnch)
      common/jkw4/ichstrt,ichend
      integer isod,isodh
      character*2 ipind(maxnpa)
      common/vpc_usoind/ipind,isod,isodh
*
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     wavelengths
      integer nwco
      double precision wcf1(maxwco)
      common/vpc_wavl/wcf1,nwco
      double precision vturb,atmass,temper,fixedbth
      common/vpc_ucopder/vturb(maxnio),atmass(maxnio),
     :                 temper(maxnio),fixedbth(maxnio)
*     current data file and order number
      integer idrn,icrn
      character*64 filnm
      common/vpc_file14/filnm,idrn,icrn
      logical lflrdin
      character*132 ch26flln
      common/vpc_f26rmult/ch26flln,lflrdin
*     old data file and parameters (blank if not used)
      integer ncumset
      character*132 comchstr
      logical ldate26
      logical lcuminc
      common/vpc_comchstr/comchstr,ldate26,lcuminc,ncumset
*     output channels:
      integer nopchan,nopchanh,nmonitor
      integer lt(3)             ! output channels for printing
      common/vpc_nopchan/lt,nopchan,nopchanh,nmonitor
*
*     current 1-D spectrum data, err, fluctuations, continuum -length ngp
      integer ngp
      double precision dhdata(maxfis),dherr(maxfis),dhrms(maxfis)
      double precision dhcont(maxfis)
      common/vpc_su1d/dhdata,dherr,dhrms,dhcont,ngp
*     chunk FWHM well sampled by pixels (if this variable = .true.)
      logical lresint(maxnch)
      double precision dlgres
      common/vpc_lresint/dlgres,lresint
*     New 2D dataset variables 
      integer numchunk,nchchtot ! # of chunks,total # data points
      integer kchstrt(maxnch),kchend(maxnch),nchlen(maxnch)
      common/vpc_chunkvar/kchstrt,kchend,nchlen,numchunk,nchchtot
      double precision dach(maxchs,maxnch),derch(maxchs,maxnch)
      double precision drmch(maxchs,maxnch),dcch(maxchs,maxnch)
      common/vpc_datch/dach,derch,drmch,dcch
*     chunk wavelengths
      integer nwvch(maxnch)
      double precision wavch(maxchs,maxnch),wavchlo(maxchs,maxnch)
      common/vpc_wavch/wavch,wavchlo,nwvch
*     chunk extension
      integer nrextend
      common/vpc_chunkext/nrextend
*     chunk to region link list
      integer linchk(maxnio)
      common/vpc_linchk/linchk
*     print counter, with nrcck=no. of 'ions' with region #s specified
      integer kprcck,nrcck
      common/vpc_kprcck/kprcck,nrcck
*
*	   	  
      if18=0
      iftemp=0
      nchunk=maxnch
      if(nopt.ne.9) then
*       check if fort.18 exists, and if it does overwrite any
*       previous stuff from this series of fits
        open(unit=18,file='fort.18',status='old',iostat=ios)
        if(ios.eq.0) then
          rewind 18
        end if
      end if
*
      if(lflrdin.and.ch26flln(1:4).ne.'    ') then
        inchstr=ch26flln
        lflrdin=.false.
       else
 801    read(13,'(a)',end=9999) inchstr
*       skip comment lines
        if(inchstr(1:1).eq.'!'.or.inchstr(1:1).eq.'#') goto 801
      end if
*     determine nchunk first
      call dsepvar(inchstr,15,dvstr,ivstr,cvstr,nvstr)
      if(cvstr(1)(1:2).eq.'* ') then
        nvarin=0
       else
        nvarin=nvstr
        if(cvstr(1)(1:2).eq.'%%') then
          if18=26
        end if
      end if
      nfl=0
*     several variables stored and read back from FOR013 files are
*     now redundant. Dummy variables are read in to keep same file format.
*     ajc 2-aug-91
*     this format is now altered (for vpgti) so that wdrstart and wdrend are 
*     the start and end wavelengths of the block.
      ich=1
      nchchtot=0
      do while (ich.le.nchunk)
*       resolution (rfc 6.9.94)
        resnf13=.false.
*       over-ride resolution set (0=none)
        nwrtemp=0
*       first line: filename, order number, continuum order (never used now)
*       provided data was not read, read it
        if(nvarin.le.2) then
 802      read(13,'(a)',end=9999) inchstr
*         skip comment lines
          if(inchstr(1:1).eq.'!'.or.inchstr(1:1).eq.'#') goto 802
        end if
*       reset nvarin so it does not skip the read next time
        nvarin=0
        call vp_stripcmt(inchstr,'!')
*       now separate the string for needed variables
        call dsepvar(inchstr,15,dvstr,ivstr,cvstr,nvstr)
*
*       check if end of files list is reached
*       this can be '*' as separator, or a change to the ion list
        lendlist=.false.
        if(cvstr(1)(1:2).eq.'%%') goto 2163
*       fort.26 input check
        if(if18.eq.26.and.cvstr(1)(1:2).ne.'%%') then
          lendlist=.true.
         else
*         fort.13 style
*	  order number unreasonable? (also caters for blank line)
          if(ivstr(2).gt.100.or.ivstr(2).le.0) then
            if(cvstr(1)(1:1).ne.'*') lendlist=.true.
           else
*	    wavelength limits reasonable?
            nchk=2
            w1=-1.0d0
            nvlim=min0(nvstr,5)
            do while (nchk.le.nvlim)
              if(dvstr(nchk).gt.1.0d2.and.w1.le.0.0d0) then
                w1=dvstr(nchk)
                nchk=nchk+1
               else
*	        next value should be upper limit to wavelength region
                if(dvstr(nchk).le.w1) then
*	          at worst have b, bturb here for ion parameters
*	          condition not satisfied for wavelength limits
                  lendlist=.true.
                end if
                nchk=20
              end if
            end do
          end if
        end if
*       remember that if lendlist is .true., have to pass info to  
*       parameter reading routine -- lendlist and inchstr will be enough
*
 2163   continue
        if(cvstr(1)(1:1).eq.'*'.or.lendlist) then
          nchunk=ich-1
          numchunk=nchunk
          if(nchunk.gt.0) then
            write(6,*) nchunk,' regions fitted'
            if(nchunk.gt.maxnch) then
              write(6,*) ' THIS IS MORE THAN CAN BE HANDLED!'
              write(6,*) ' REDUCE NUMBER TO ',maxnch,' OR CHANGE ',
     1                   'ARRAY SIZE IN PROGRAM'
              stop
            end if
           else
            write(6,*) nchunk,' regions, program terminates'
            stop
          end if
*         set variables for fort.26 restart if necessary
          if(if18.eq.26) then
            lflrdin=.true.
          end if
          goto 1315
        end if
*
        if(cvstr(1)(1:2).eq.'%%'.and.nvstr.gt.1) then
*         shuffle down the variables for fort.26 style input
          do j=2,nvstr
            cvstr(j-1)=cvstr(j)
            dvstr(j-1)=dvstr(j)
            ivstr(j-1)=ivstr(j)
          end do
          nvstr=nvstr-1
        end if 
*
        if(nvstr.ge.4) then
*         free format input (filename, order, fileno, wvlow, wvhigh)
          filename(ich)=cvstr(1)
          idrun(ich)=ivstr(2)
          icrun(ich)=0
          wvstrt(ich)=0.0d0
          ichstrt(ich)=0
          ichend(ich)=0
*         strip off keys
          nvtot=0
          chkey(ich)=' '
          if(nvstr.gt.4) then
*           seek '='
            nindex=index(cvstr(5),'=')
            nvtot=nvstr
            if(nindex.eq.0) then
              nvstr=5
             else
              nvstr=4
              chkey(ich)=cvstr(5)(1:32)
            end if
          end if
          if(nvstr.eq.4) then
            wdrstart=dvstr(3)
            wdrend=dvstr(4)
*	    set chunk number
            iftemp=ich
           else
            wdrstart=dvstr(4)
            wdrend=dvstr(5)
            iftemp=ivstr(3)
          end if
          if(nvtot.gt.nvstr) then
*	    have keyword(s), so separate
*	    Note: inchstr,dvstr,ivstr,cvstr & nvstr are common variables,
*	          and they are used by vp_f13keyset
            call vp_f13keyset(nvtot,resnf13,ich)
            nwrtemp=nswres(ich)
            do jjk=1,nwrtemp
              swrtemp(jjk)=swres(jjk,ich)
            end do
            if(nwrtemp.lt.maxpoc) then
              do jjk=nwrtemp+1,maxpoc
                swrtemp(jjk)=swres(jjk,ich)
              end do
            end if
          end if
*         rfc 16.12.96: set chunk beginning and end points:
          wvstrt(ich)=wdrstart
          wvend(ich)=wdrend
          wvchlo(ich)=wdrstart
          wvchhi(ich)=wdrend
*
         else
*
          write(6,*) 'Input format no longer supported'
          write(6,*) 'Needs: filename, order, wlo, whi (or variant)'
        end if
*
*       reassignments:
        wcoeff(ich)=wdrstart
        sepvd(ich)=wdrend
        indfil(ich)=iftemp
        filnm=filename(ich)
        idrn=idrun(ich)
        icrn=icrun(ich)
        ik=indfil(ich)
        if(ik.ne.nfl) then
*         call vp_close14
          close(unit=14)
          filnm=filename(ich)
          idrn=idrun(ich)
          icrn=icrun(ich)
*         read in the data from fits file(s) or wherever
          call vp_spdatin(ik,ich)
          if(nwrtemp.gt.0) then
            nswres(ich)=nwrtemp
            do jjk=1,maxpoc
              swres(jjk,ich)=swrtemp(jjk)
            end do
          end if
        end if
*       define wavelengths for the chunk
*	using all parameters (copy 1 -> 2):
        call vp_cfcopy(ich,2)
        nfl=indfil(ich)
        ndpts(ich)=ngp
        ndp(ik)=ngp
*       if ichstrt(ich) and ichend(ich) undefined (i.e. zero) then
*       determine them from start and end wavelengths wdrstart and wdrend
        if(ichstrt(ich).eq.0) then
          if(wcf1(2).gt.0.0d0) then
            ddum=(wdrstart-wcf1(1))/wcf1(2)
           else
            ddum=1.0d3
          end if
          call chanwav(wdrstart,ddum,1.0d-2,100)
          if(ich.eq.1 .and. verbose) then
            dtemp=vp_wval(ddum,ich)
            write(6,*) ' chanwav in:',wdrstart,' out:',ddum,
     :             ' check:',dtemp
          end if
          ichstrt(ich)=int(ddum+0.99d0)
        end if
        if(ichend(ich).eq.0) then
          if(wcf1(2).gt.0.0d0) then
            ddum=(wdrstart-wcf1(1))/wcf1(2)
           else
            ddum=1.0d3
          end if
          call chanwav(wdrend,ddum,1.0d-2,100)
          ichend(ich)=int(ddum)
          write(6,*) 'Start & end chans: ',ichstrt(ich),ichend(ich)
          write(6,*) ' '
*
*         extend these by nrextend for chunk copy limits (rfc: 24.08.05)
          lchstrt=max(ichstrt(ich)-nrextend,1)
          lchend=min(ichend(ich)+nrextend,ngp)
*         length of chunk
          nchlen(ich)=lchend-lchstrt+1
*         nchchtot is total number of chunk datapoints in all chunks
          nchchtot=nchchtot+ichend(ich)-ichstrt(ich)+1
          koffset=lchstrt-1
*         set start and end values for fitting region within the
*         chunk array
          kchstrt(ich)=ichstrt(ich)-koffset
          kchend(ich)=ichend(ich)-koffset
*         copy the data arrays 
          do ki=1,nchlen(ich)
*           data
            dach(ki,ich)=dhdata(ki+koffset)
*           error (held as square)
            derch(ki,ich)=dherr(ki+koffset)
*           fluctuations array (held as square)
            drmch(ki,ich)=dhrms(ki+koffset)
*           continuum
            dcch(ki,ich)=dhcont(ki+koffset)
*           set the wavelengths on a pixel-by-pixel basis, to cover
*           all possibilities. Use wval function, since that applies
*           to the last dataset read in and that is the one being used.
            wavch(ki,ich)=wval(dble(ki+koffset))
*           and, in case channels are subdivided, set the channel edges
*           wavchlo(ki,ich) is lower edge of ki'th channel 
            wavchlo(ki,ich)=wval(dble(ki+koffset)-0.5d0)
          end do
          wavchlo(nchlen(ich)+1,ich)=
     :           wval(dble(nchlen(ich)+koffset)+0.5d0)
*         set wavelength array length (could have fit coefficients,
*         so fewer values, but not as set up here).
          nwvch(ich)=nchlen(ich)
*
          if(nopchan.gt.0) then
            kich=max(1,(ichstrt(ich)+ichend(ich))/2)
            tempxx=f_sigscl(kich,ich)
            if(abs(tempxx-1.0d0).gt.1.0d-3) then
*             Information only if different from unity
              write(6,*) 'sigma scale at chan',kich,' is',tempxx
            end if
          end if
        end if
        if(ik.ne.nfl.or.
     :       (swres(1,ich).le.0.0d0.and.swres(2,ich).le.0.0d0)) then
*         start/end channels not set always.
          xlamx=0.5d0*(wavch(kchstrt(ich),ich)+wavch(kchend(ich),ich))
          oldsig = swres(2,ich)*2.99792458d5
          olddl = swres(1,ich)
          if(.not.resnf13) then
            call vp_dlset(ich,xlamx,oldsig,olddl)
          end if
        end if
*       is the instrument profile well sampled? [for sigma,dlam]
        dlinst=swres(1,ich)+xlamx*swres(2,ich)
        if(kchend(ich).eq.kchstrt(ich)) then
          ktemp=kchend(ich)-1
         else
          ktemp=kchstrt(ich)
        end if
        dlpix=(wavch(kchend(ich),ich)-wavch(ktemp,ich))/
     :           dble(kchend(ich)-ktemp)
*       dlinst=0.0 if pixel profile, so lresint then remains unchanged
        if(dlpix*dlgres.le.dlinst*2.35482d0) then
          lresint(ich)=.true.
        end if
        if(verbose) then
          write(6,*) 'Chunk',ich,'  sigma=',swres(2,ich),
     :                    '   dlam=',swres(1,ich)
          if(lresint(ich)) then
            write(6,*) 'Instrument profile applied on original pixels'
          end if
        end if
        ich=ich+1  
      end do
*
*     write file use information to for018 file, unless just looking
*     at the fit to the data (option 9)
 1315 if(nopt.ne.9) then
        if(nopchan.ge.2) then
          write(18,'('' *****'')')
          write(18,*) nchunk
        end if
*       store string temporarily
        chstrtem=inchstr
        do k=1,nchunk
          lfl=len(filename(k))
          do while (lfl.gt.12.and.filename(k)(lfl:lfl).eq.' ')
            lfl=lfl-1
          end do
*         resolution stuff for output:
          sigmak=swres(2,k)*2.99792458d5
          dlamk=swres(1,k)
          call vp_hpakres(sigmak,dlamk,inchstr,lensig)
*	    
*         if requested, to fort.13 style summary file:
          call vp_f13hout(filename,lfl,idrun,k,wcoeff(k),sepvd(k))
          if(nopchan.ge.2) then
            write(18,'(a,i5,2f12.3,a1,a)') 
     :           filename(k)(1:lfl),idrun(k),wcoeff(k),
     :           sepvd(k),' ',inchstr(1:lensig)
          end if
        end do
*       copy input string back
        inchstr=chstrtem
      end if
      if(nverbose.ge.5) then
        write(6,*) 'Resolution parameters by chunk:'
        do iii=1,nchunk
          write(6,*) iii,swres(1,iii),swres(2,iii),nswres(iii)
        end do
      end if
*
*     read in the first fit parameter guesses from file
      call vp_fnzbin(if18,nopt,nn,ion,level,parm,ipind,vturb,temper,
     1                   lendlist)
      ch26flln=inchstr
*     set number ofions with specified regions
      nlines=nn/noppsys
      nrcck=0
      kprcck=0
      do k=1,nlines
        if(linchk(k).gt.0) then
          nrcck=nrcck+1
        end if
      end do
      if(nrcck.gt.0) then
        write(6,*) nrcck,' ions with specific regions assigned'
      end if
*
 9889 return
 9999 write(6,*) ' no more data'
      nn=-1
      goto 9889
      end
      subroutine vp_f13keyset(nvtot,resnf13,ich)
*
      implicit none
      include 'vp_sizes.f'
*     rfc 8.9.94
*     separate fort.13 key values on file lines, resolution ones
*     changed in appropriate common block
*     IN:
*     nvtot	integer	total number of variables in input line
*     ich	chunk number
*     COMMON IN:
*     /vpc_sepspace/
*     nvstr	integer		number of non-key variables
*     cvstr	ch array	separated input line
*     OUT:			(which should be zero on input)
*     resnf13	logical		true if keyword applies to resolution
*     
      integer nvtot,ich
      logical resnf13
*     Local
      integer ierr,lvtmp
      integer nindex,nindexp1,nlen,nvtemp,nvtmp,nvstx,nvt
      double precision sigval,dlamval
*
*     local character handling:
      character*60 chtmp
      character*10 cvt(2)
      double precision rvt(2)
      integer ivt(2)
      character*30 cxst(6)
*
*     Functions
      integer lastchpos
*
*     Common
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
      double precision dvstr(24)
      common/vpc_dsepspace/dvstr
*
*     output variables
      logical lwrsum,lwropen,lerrsum
      character*60 cwrsum
      character*16 chext13
      integer nwrsum,lext13
      common/vpc_f13hout/cwrsum,nwrsum,lwrsum,lwropen,lerrsum,
     :                chext13,lext13
*     binned profile
      character*2 chprof
      double precision pfinst
      double precision wfinstd
      integer npinst,npin2
      common/vpc_profwts/wfinstd(maxipp,maxnch),pfinst(maxipp,maxnch),
     :     npinst(maxnch),npin2(maxnch),chprof(maxnch)
*     fit to resolution, using sigmas
      integer nswres(maxnch)
      double precision swres(maxpoc,maxnch)
      common/vpc_sigwres/swres,nswres
*
*     think you have extra info on line, so write it out to units 6 & 18
      nvtemp=len(inchstr)
      do while(nvtemp.gt.1.and.inchstr(nvtemp:nvtemp).eq.' ')
        nvtemp=nvtemp-1
      end do
*     Copy out [write(6,*) and write(18,*)] the fort.13 input line
      write(6,*) inchstr(1:nvtemp)
*
*     keyword actions:
*     lower or upper case checks for: 
*     pfin: pixel file specifies instrument profile
*     vfwh: velocity FWHM
*     vsig: velocity sigma read in
*     wfwh: wavelength FWHM
*     wsig: wavelength sigma
*     wf13: write file 13 root name
      nvtemp=nvstr
      do while (nvtemp.lt.nvtot)
        nvtemp=nvtemp+1
*
*       INSTRUMENT PROFILE PIXEL SPECIFIED - FROM FILE
*       uses unit=21 (opened and closed locally)
        if(cvstr(nvtemp)(1:4).eq.'pfin'.or.
     :       cvstr(nvtemp)(1:4).eq.'PFIN') then
          nindex=index(cvstr(nvtemp),'=')
*         find last non-blank character
          nlen=lastchpos(cvstr(nvtemp))
          if(nlen.gt.nindex) then
*           have a filename to try so use it!
            nindexp1=nindex+1
            open(unit=21,file=cvstr(nvtemp)(nindexp1:nlen),
     :        status='old',iostat=ierr)
            if(ierr.ne.0) then
*             file not opened -- request filename
 601          write(6,'('' Filename? (<CR> to abandon everything)'')')
              read(5,'(a)') inchstr
              if(inchstr(1:1).ne.' ') then
                open(unit=21,file=inchstr,status='old',
     :            err=601)
               else
*	        We really meant abandon it!!
                write(6,*) 'TERMINATED AS REQUESTED'
                stop
              end if
            end if
*           read file in to pfinst array for the dataset
            npinst(ich)=0
 790        read(21,'(a)',end=791) chtmp
*           skip comments
            if(chtmp(1:1).eq.'!'.or.chtmp(1:1).eq.'#') goto 790
            call dsepvar(chtmp,2,rvt,ivt,cvt,nvt)
            npinst(ich)=npinst(ich)+1
            if(nvt.eq.2) then
              pfinst(npinst(ich),ich)=rvt(2)
             else
              pfinst(npinst(ich),ich)=rvt(1)
            end if
            goto 790
 791        close(unit=21)
            npin2(ich)=npinst(ich)/2+1
*           set indicator variable
            chprof(ich)=' '
          end if
        end if
*
*       VELOCITY SIGMA SPECIFIED
        if(cvstr(nvtemp)(1:4).eq.'vsig'.or.
     1            cvstr(nvtemp)(1:4).eq.'VSIG'.or.
     :            cvstr(nvtemp)(1:4).eq.'vfwh'.or.
     :            cvstr(nvtemp)(1:4).eq.'VFWH') then
          nindex=index(cvstr(nvtemp),'=')
          cvstr(nvtemp)(nindex:nindex)=' '
*         strip off the units (if any)
          nindex=index(cvstr(nvtemp),'k')
          if(nindex.ne.0) then
            nlen=len(cvstr(nvtemp))
            cvstr(nvtemp)(nindex:nlen)=' '
          end if
*         copy and separate
          inchstr=cvstr(nvtemp)
          call dsepvar(inchstr,6,dvstr,ivstr,cxst,nvstx)
          if(dvstr(2).gt.0.0d0) then
            resnf13 = .true.
            if(cxst(1)(1:4).eq.'vfwh'.or.
     :            cxst(nvtemp)(1:4).eq.'VFWH') then
*             Velocity FWHM was read, so convert to sigma
              sigval=dvstr(2)/2.354820045d0
             else
              sigval=dvstr(2)
            end if
            write(6,*) 'Resolution sigma = ',sigval,' km/s'
            write(6,*) 'overrides any values from header info.'
*           set indicator variable
            swres(2,ich)=sigval/2.99792458d5
            swres(1,ich)=0.0d0
            nswres(ich)=2
            chprof(ich)=' '
          end if
        end if
*
*       WAVELENGTH RESOLUTION SPECIFIED
        if(cvstr(nvtemp)(1:4).eq.'wsig'.or.
     1            cvstr(nvtemp)(1:4).eq.'WSIG'.or.
     :            cvstr(nvtemp)(1:4).eq.'wfwh'.or.
     :            cvstr(nvtemp)(1:4).eq.'WFWH') then
          nindex=index(cvstr(nvtemp),'=')
          cvstr(nvtemp)(nindex:nindex)=' '
*	  strip off the units (if any)
          nindex=index(cvstr(nvtemp),'A')
          if(nindex.ne.0) then
            nlen=len(cvstr(nvtemp))
            cvstr(nvtemp)(nindex:nlen)=' '
          end if
*	  copy and separate
          inchstr=cvstr(nvtemp)
          call dsepvar(inchstr,6,dvstr,ivstr,cxst,nvstx)
          if(dvstr(2).gt.0.0d0) then
            resnf13 = .true.
            if(cxst(1)(1:4).eq.'wfwh'.or.
     :            cxst(nvtemp)(1:4).eq.'WFWH') then
*	      Velocity FWHM was read, so convert to sigma
              dlamval=dvstr(2)/2.354820045d0
             else
              dlamval=dvstr(2)
            end if
            write(6,*) 'Wavelength resolution sigma = ',dlamval,' A'
            write(6,*) 'overrides any values from header info.'
            swres(1,ich)=dlamval
            swres(2,ich)=0.0d0
            nswres(ich)=2
*           set indicator variable
            chprof(ich)=' '
          end if
        end if
*
*       SUMMARY FILE NAMES DETERMINED BY DATA FILE NAME
        if(cvstr(nvtemp)(1:4).eq.'wf13'.or.
     :            cvstr(nvtemp)(1:4).eq.'WF13') then
*         format is assumed to be wf13[mm:nnn], and takes
*         characters mm to nnn from the input file name, and
*         appends them to the root filename for the fort.13 output
*         from the vp_setup.dat file. This over-rides the wavelength
*         region output specification, if used. Default is 'wf13.'.
          chtmp=cvstr(nvtemp)
          chtmp(1:4)='    '
          nvtmp=5
          lvtmp=len(cvstr(nvtemp))
          do while(chtmp(nvtmp:nvtmp).ne.' '.and.nvtmp.le.lvtmp)
            if(chtmp(nvtmp:nvtmp).eq.'['.or.chtmp(nvtmp:nvtmp).eq.':'
     :           .or.chtmp(nvtmp:nvtmp).eq.']') then
              chtmp(nvtmp:nvtmp)=' '
            end if
            nvtmp=nvtmp+1
          end do
*	  in what is left, separate out the two numbers
          call dsepvar(chtmp,2,rvt,ivt,cvt,nvt)
          if(ivt(1).le.0) ivt(1)=1
          if(ivt(2).lt.ivt(1)) ivt(2)=ivt(1)
          if(ivt(2).gt.ivt(1)+15) ivt(2)=ivt(1)+15
          chext13=cvstr(1)(ivt(1):ivt(2))
          lext13=ivt(2)-ivt(1)+1
          if(cwrsum(1:1).eq.' ') then
            cwrsum='wf13.'
            nwrsum=5
            lwrsum=.true.
          end if
        end if
      end do
      return
      end
      subroutine vp_hpakres(sigma,dlam,cstr,lensig)
*
      implicit none
      double precision sigma,dlam
*     pack cstr with vsig=nnn and dlam=nnn as appropriate
      character*(*) cstr
      integer lensig
*
*     Local
      integer ic,lench,lentem
*
      lensig=0
      lench=len(cstr)
      if(sigma.gt.0.0d0) then
*       put in vsig=
        cstr(1:5)='vsig='
        write(cstr(6:lench),*) sigma
*       trim the string of trailing spaces and trailing zeros
        lensig=lench
        do while (lensig.gt.5.and.(cstr(lensig:lensig).eq.' '
     :         .or.cstr(lensig:lensig).eq.'0'))
          cstr(lensig:lensig)=' '
          lensig=lensig-1
        end do
        if(cstr(lensig:lensig).eq.'.') then
*         put back a zero after the decimal point
          lensig=lensig+1
          cstr(lensig:lensig)='0'
        end if
*       remove any embedded spaces
        ic=6
        do while (ic.le.lensig)
          if(cstr(ic:ic).eq.' ') then
            cstr(ic:lensig-1)=cstr(ic+1:lensig)
            cstr(lensig:lensig)=' '
            lensig=lensig-1
           else
            ic=ic+1
          end if
        end do
      end if
*
      if(dlam.gt.0.0d0) then
*       add in the wsig= term, which may be pasted in after the 
*       vsig= term
        if(lensig.gt.0) then
*         add a couple of spaces
          lensig=lensig+1
          cstr(lensig:lensig)=' '
          lensig=lensig+1
          cstr(lensig:lensig)=' '
        end if
        cstr(lensig+1:lensig+5)='wsig='
        lensig=lensig+5
        write(cstr(lensig+1:lench),*) dlam
        lentem=lench
        do while(lentem.gt.lensig.and.(cstr(lentem:lentem).eq.' '
     :        .or.cstr(lentem:lentem).eq.'0'))
          cstr(lentem:lentem)=' '
          lentem=lentem-1
        end do

        if(cstr(lentem:lentem).eq.'.') then
*         put back a zero after the decimal point
          lentem=lentem+1
          cstr(lentem:lentem)='0'
        end if
*       remove any embedded spaces
        ic=lensig+1
        do while (ic.le.lentem)
          if(cstr(ic:ic).eq.' ') then
            cstr(ic:lentem-1)=cstr(ic+1:lentem)
            cstr(lentem:lentem)=' '
            lentem=lentem-1
           else
            ic=ic+1
          end if
        end do
        lensig=lentem
      end if
      return
      end
