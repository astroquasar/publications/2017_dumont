      subroutine rd_markreg(xlpg,xhpg,ylpg,yhpg)
*
*     searches through a file for wavelength regions within the plot limits
*     xlo-xhi, and marks them 1/3 of the way up the plot (range ylo-yhi)
*
*     variables used by PGPLOT, so real
      real xlpg,xhpg,ylpg,yhpg
*
*     local
      real tempg,ypospg,yposmpg,ypostpg
      character*132 chstr
*
*     line list variables
      double precision zmark
      character*132 chmarkfl
      logical lmarkl,lmarkto
      common/rdc_lmarklv/zmark,chmarkfl,lmarkl,lmarkto
*     character string separators
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
      double precision dvstr(24)
      common/vpc_dsepspace/dvstr
*     pgplot attributes: data, error, continuum, axes, ticks, text/RESIDUAL
*     1 color indx; 2 line style; 3 line width; 
*     4 curve(0), hist(1); 7 - 10 reserved.
      integer ipgatt(10,9)
      common/vpc_pgattrib/ipgatt
*
*     get filename
      call dsepvar(chmarkfl,15,dvstr,ivstr,cvstr,nvstr)
      if(cvstr(1)(1:1).eq.' ') goto 978
      open(unit=21,file=cvstr(1),status='old',err=978)
*     set pgplot attributes
      call pgsci(abs(ipgatt(1,7)))
      call pgsls(ipgatt(2,7))
      call pgslw(ipgatt(3,7))
*     works ONLY for fort.26 style files
      ypospg=0.29*yhpg+0.71*ylpg
*     put little ticks on the ends
      yposmpg=ypospg-0.005*(yhpg-ylpg)
      ypostpg=ypospg+0.005*(yhpg-ylpg)
 801  read(21,'(a)',end=9997) chstr
      if(chstr(1:1).eq.'!') goto 801
      call sepvar(chstr,6,rvstr,ivstr,cvstr,nvstr)
      if(cvstr(1)(1:2).ne.'%%') goto 801
      if(rvstr(4).ge.xhpg.or.rvstr(5).le.xlpg) goto 801
*     region is at least partially within the plot range, so plot it
*     lower end tick down, upper wavelength tick up
      tempg=max(rvstr(4),xlpg)
      call pgmove(tempg,yposmpg)
      call pgdraw(tempg,ypospg)
      tempg=min(rvstr(5),xhpg)
      call pgdraw(tempg,ypospg)
      call pgdraw(tempg,ypostpg)
      goto 801
 9997 close(unit=21)
 978  return
      end
