      subroutine rd_qdatafiles
*
*     list available data files
*
      implicit none
      integer len,n
*     Functions:
      integer lastchpos
*     common things:
      integer nv
      character*132 inchstr
      character*60 cv(24)
      real rv(24)
      integer iv(24)
      common/rd_chwork/inchstr,rv,iv,cv,nv
*     set up filenames for the data
      call system('ls -1 *.fits > temptmpfitsfiles')
      open(unit=3,file='temptmpfitsfiles',status='old',err=981)
      n=1
      write(6,*) 'Data files in this directory are:'
 983  read(3,'(a)',end=982) inchstr
      len=lastchpos(inchstr)
      write(6,*) n,'  ',inchstr(1:len)
      n=n+1
      goto 983
 982  close(unit=3)
      call system('/bin/rm temptmpfitsfiles')
*     don't bother looking for IRAF .imh - hardly used
 981  continue
      return
      end
