*
*     supplementary size file for some of the extended capabilities
*     now included in RDGEN
*
      integer maxcubex
      parameter ( maxcubex = 200)
      integer maxcubey
      parameter ( maxcubey = 200)
      integer maxcubev
      parameter ( maxcubev = 120)
