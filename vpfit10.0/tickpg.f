      subroutine tickpg(xm,xh,ym,yh,donum)
*     subroutine to add tick marks to a pgplot graph
*     using x (or wavelength) positions stored on a disk file
*
      logical donum
*
*     LOCAL:
      real rv(20)
      real wvp(4096),x(2),y(2)
      integer iv(20)
      character*4 chnmb
      character*30 cv(20)
      character*132 filnm,formt
      double precision wvxx
*
*     FUNCTIONS:
      double precision wcor
*     input stream
      integer inputc
      common/rdc_inputc/inputc
 2    write(6,*) 'Tick top, length as fractions of y axis?'
      read(inputc,'(a)',err=905) formt
      call sepvar(formt,2,rv,iv,cv,nvx)
      ttop=rv(1)
      tlen=rv(2)
      if(ttop.gt.1.0.or.tlen.le.0.0.or.ttop-1.5*tlen.le.0.0) goto 2
c
      write(6,*) ' Filename?'
      read(inputc,'(a)',err=14) filnm
c
      open(unit=19,status='old',file=filnm,
     1           err=991)
c
c     read data from first record in file onwards
 799  write(6,'('' Variable number? (lambda as data)'')')
      write(6,'(''     (or redshift, lambda vac rest)'')')
      write(6,'(''     (or lambda, redshift, lambda vac rest ..'')')
      write(6,'(''       .. where the last two, if present,'',
     1      '' over-ride the first)'')')
      read(inputc,'(a)',err=799) formt
      call sepvar(formt,3,rv,iv,cv,nvx)
      if(nvx.gt.1) then
        ivar2=iv(2)
       else
        ivar2=0
      end if
      if(nvx.le.0) goto 799
      ivar=iv(1) 
      if(ivar.le.0.or.ivar.gt.10) goto 799
      nv=0
 1    read(19,'(a)',end=993,err=992) formt
      call sepvar(formt,10,rv,iv,cv,nvx)
      if(nvx.lt.ivar) goto 1
c
c     read x-positions (wavelengths) in to array wvp
      if(rv(ivar).le.0.0001) goto 1
      nv=nv+1
      if(nv.gt.4096) goto 993
      if(rv(ivar).gt.10.0.or.ivar2.eq.0) then
*       wavelength may be in microns, but do not cater for z>10!
        wvp(nv)=rv(ivar)
       else
        wvxx=dble(1.0+rv(ivar))
        if(ivar2.gt.0) then
          wvxx=dble(rv(ivar2))*wvxx
         else
          wvxx=wvxx*1215.67d0
        end if
c       correct to air wavelengths
        wvp(nv)=real(wvxx/wcor(wvxx))
      end if
      goto 1
c
 993  if(nv.le.0) goto 992
c
c     set up tick heights
      yup=ttop*(yh-ym) + ym
      ydec=tlen*(yh-ym)
      ylo=yup-ydec
      ylod=yup-1.5*ydec
c
c     write tick marks loop
      i=0
 11   i=i+1
      if(i.gt.nv.or.wvp(i).gt.xh) goto 12
      if(wvp(i).lt.xm) goto 11
      x(1)=wvp(i)
      x(2)=x(1)
      y(1)=yup
      y(2)=ylo
      if(i/10*10.eq.i) then
        ytup=yup+0.2*tlen
        y(2)=ylod
        if(donum) then
          if(i.lt.100) then 
            write(chnmb,'(i2)') i
            call pgptxt(x(1),ytup,0.0,0.5,chnmb(1:2))
           else
            if(i.lt.1000) then
              write(chnmb,'(i3)') i
              call pgptxt(x(1),ytup,0.0,0.5,chnmb(1:3))
             else
              write(chnmb,'(i4)') i
              call pgptxt(x(1),ytup,0.0,0.5,chnmb)
            end if
          end if
*         write(6,*) i,'  ',chnmb(1:4)
        end if
      end if
      call pgline(2,x,y)
      goto 11
 12   continue
 13   close(unit=19)
 14   return
c     assorted errors
 991  write(6,*) ' ********* File not found'
      goto 14
 992  write(6,*) ' ********* File read error'
      goto 13
 905  write(6,*) ' Error - reverting to standard input'
      inputc=5
      goto 14
      end
