      subroutine lm_udchole(aa,bb,n,nopt,npts,idiag,lambda)
*     rfc 13.12.01 argument list was (aa,bb,n,nopt,npts,idiag,glen), 
*     replaced since glen neither used nor set here
*
*     CHOLEsky decomposition of positive definite symmetric matrix
*	       to solve Ax = b.
*
      implicit none
      include 'vp_sizes.f'
*     rfc 16.3.95: general tidyup to remove most branches, and to
*     indent loops so can see where they finish
*     ajc 6-jul-93  length of gradient vector
*
*     arguments
      integer n,nopt,npts,idiag
      double precision aa(maxmas,maxmas),bb(maxmas)
*     
      double precision a(maxmas,maxmas),b(maxmas)
      double precision sum,lmax,lmin,scf(maxmas)
      double precision l(maxmas,maxmas),y(maxmas)
      double precision  cc(maxmas)
      double precision lambda
*     local variables
      integer i,ic,il,iop,ir,it,iu
      integer j,jj,k,kk
      integer nsave,nvbopchan
      double precision  aveigv,offset
*     ajc 5-dec-91  external function to test for upper case
      logical varythis
      integer isod,isodh
      character*2 ipind(maxnpa)
      common/vpc_usoind/ipind,isod,isodh
*     output channels:
      integer lt(3)             ! output channels for printing
      integer nopchan,nopchanh,nmonitor
      common/vpc_nopchan/lt,nopchan,nopchanh,nmonitor
*     debug information -3x for udchole
      integer ndbranch
      common/vpc_debug1/ndbranch
*     
*     JAK INSERTION
c     Remove rows and columns of zeros corresponding
c     to constrained parameters
      nsave=n
      if(nopt.le.0) then
c       no constrained parameters if nopt zero or negative
        do ir=1,n
          b(ir)=bb(ir)
          do il=1,n
            a(il,ir)=aa(il,ir)
          end do
        end do
        goto 50
      endif
      jj=1
      do it=1,n
*       ajc 5-dec-91  do not include fixed values in the hessian
        if ( varythis( it, ipind ) ) then
          kk=1
          b(jj)=bb(it)
          do iu=1,n
            if ( varythis( iu, ipind ) ) then
              a(kk,jj)=aa(iu,it)
              kk=kk+1
            endif
          end do
          jj=jj+1
        endif
      end do
      n=jj-1
*     rescale Hessian and gradient vector
 50   continue
      do i=1,n
        scf(i)=dsqrt(a(i,i))
      end do
      do j=1,n
        b(j)=b(j)/scf(j)
        cc(j)=b(j)
        do i=1,n
          a(i,j)=a(i,j)/(scf(i)*scf(j))
        end do
      end do
      do i=1,n
* ajc 6-jul-93  this seems to be where unity is added to the diagonal if
*               the solution is ill-conditioned
*               change it now so that instead of 1 it is the maximum of
*               1 and the length of the gradient vector - allows faster
*               convergence with a bit of luck (?)
c      JAK Dec-06 -- Levenberg-Marquardt method is here. Multiple Hessian
c      diagonal by (1+lambda)
        a(i,i)=a(i,i)*(1.d0+lambda)
c       End JAK insert
*	    that looks like another idea that failed, so:
*	    a(i,i) = a(i,i) + min( 1.0, glen )
      end do
*     offending equations now gone
      l(1,1)=dsqrt(a(1,1))
      do k=2,n
        do j=1,k-1
          sum=a(k,j)
          if(j.ne.1) then
            do i=1,j-1
              sum=sum-l(k,i)*l(j,i)
            end do
          end if
          l(k,j)=sum/l(j,j)
        end do
        sum=a(k,k)
        do i=1,k-1
          sum=sum-l(k,i)*l(k,i)
        end do
        if(sum.le.0.d0) then
          aveigv=a(1,1)
          do i=2,n
            aveigv=aveigv+a(i,i)
          end do
c     ***       max eigenvalue < trace
          offset=aveigv*1.0d-15
          do i=1,n
            a(i,i)=a(i,i)+offset
          end do
          write(6,*)' WARNING - ill-conditioned matrix in UDCHOLE'
          write(6,*)' Offset added to diagonal =',offset
          if(nopchan.ge.2) then
            write(18,*)'WARNING- ill-conditioned matrix in UDCHOLE'
            write(18,*)' Offset added to diagonal =',offset
          end if
          goto 50
        end if
        l(k,k)=dsqrt(sum)
      end do
      if(idiag.gt.0.and.nopchan.gt.0) then
        if(ndbranch.eq.0.or.ndbranch.eq.31) then
            nvbopchan=min(2,nopchan)
          do iop=1,nvbopchan
            write(lt(iop),*) ' '
            write(lt(iop),*) ' Lower triangular decomposition'
            write(lt(iop),*) ' '
            write(lt(iop),'(8e9.2)') (l(i,i),i=1,n)
            write(lt(iop),*) ' '
          end do
        end if
      end if
      lmax=l(1,1)
      lmin=l(1,1)
      do j=1,n
        lmax=max(lmax,l(j,j))
        lmin=min(lmin,l(j,j))
      end do
c     JAK added next few lines as test
c     end JAK
      lmax=lmax/lmin
      lmax=lmax*lmax
c     JAK altered so always writes condition number
      if(idiag.gt.0) then
        write(6,'(/,'' Condition number ='',e9.2/)') lmax
      end if
c     write(6,'(/,'' Condition number ='',e9.2/)') lmax
c     ***   solve Ly = b
      y(1)=b(1)/l(1,1)
      do i=2,n
        sum=b(i)
        do k=1,i-1
          sum=sum-l(i,k)*y(k)
        end do
        y(i)=sum/l(i,i)
      end do
c     ***   solve L(T)x = y
      b(n)=y(n)/l(n,n)
      do i=n-1,1,-1
        sum=y(i)
        do k=i+1,n
          sum=sum-l(k,i)*b(k)
        end do
        b(i)=sum/l(i,i)
      end do
c     ***   now rescale back solution
      sum=0.0d0
      do i=1,n
        sum=sum+cc(i)*b(i)
        b(i)=b(i)/scf(i)
      end do
      sum=sum/dfloat(npts)
      if(idiag.gt.0) write(6,1113) sum
 1113 format(/,' Predicted statistic change for alpha = 1',f10.4/)
      if(nopt.le.0)then
c       unconstrained parameters
        do ir=1,nsave
          bb(ir)=b(ir)
        end do
       else
C       constrained parameters
        ic=1
        do ir=1,nsave
          if ( varythis ( ir, ipind ) ) then
            bb(ir)=b(ic)
            ic=ic+1
          end if
          if(.not.varythis(ir,ipind)) bb(ir)=0.0d0
        end do
      end if
      n=nsave
      return
      end
