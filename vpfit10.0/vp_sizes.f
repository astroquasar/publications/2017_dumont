     
* this include file defines the sizes of the main arrays within vpfit
* Initially set up: ajc1 9-may-92
*
* NOTE: Some systems do not automatically recompile routines which
*     use this file via an include statement. To guard against this
*     you should 'touch *.f' if any of the sizes in this file are 
*     changed, and then use the makefile.
*
*
* MAXimum Wavelength Coefficient Order
* This is also the array size for wavelength tables.
      integer maxwco
      parameter ( maxwco = 320000)
* MAXimum Orders in Spectrum File
* NB  this is used in wavelength coefficient readin only.
* If single spectrum per file then maxosf=1 is fine.
      integer maxosf
      parameter ( maxosf = 1)
* MAXimum Number FIles (old value: 64)
      integer maxnfi
      parameter ( maxnfi = 64 )
* MAXimum Number CHunks
* NB this should be the same as maxnfi for now
      integer maxnch
      parameter ( maxnch = 64 )
* MAXimum FIle Size (default: 20800)
      integer maxfis
      parameter ( maxfis = 300000 )
* MAXimum CHunk Size
      integer maxchs
      parameter ( maxchs = 15000 )
* MAXimum SubChunk Size (default 128000)
      integer maxscs
      parameter ( maxscs = 128000 )
* MAXimum Substepped Array Length (RDGEN only)
      integer maxsal
      parameter ( maxsal = 932000 )
* MAXimum Wavelength Coefficents per Chunk
* Set as maxwco pending full separation in program
      integer maxwcc
      parameter ( maxwcc = 300000 )
* MAXimum Atomic Table Size
      integer maxats
      parameter ( maxats = 2500 )
* MAXimum Number IOns (old default:100)
*     This number should be even to avoid common block alignment warnings
      integer maxnio
      parameter ( maxnio = 300 )
* MAXimum Parameters Per System
      integer maxpps
      parameter ( maxpps = 5 )
* MAXimum Number PArameters
      integer maxnpa
      parameter ( maxnpa = maxpps * maxnio )
* Number PArameters SQUared
      integer npasqu
      parameter ( npasqu = maxnpa ** 2 )
* MAXimum eXtra Preset Ions
      integer maxxpi
      parameter (maxxpi=500)
* MAXimum eXtra Preset Parameters
      integer maxxpp
      parameter (maxxpp=maxxpi*maxpps)
* MAXimum MAtrix Size
      integer maxmas
      parameter ( maxmas = maxnpa )
* MAXimum Number POints (default: 20000) 
* total number of data points from all regions used in fit
      integer maxnpo
      parameter ( maxnpo = 20000 )
* MAXimum Number PLotted points (default: 8000)
      integer maxnpl
      parameter ( maxnpl = 20000 )
* MAXimum Number COnstraints
      integer maxnco
      parameter ( maxnco = 120 )
* MAXimum TAble Size (default: 200)
      integer maxtas
      parameter ( maxtas = 2000 )
* MAXimum Instrument Profile Points (default:512)
      integer maxipp
      parameter ( maxipp = 555 )
* MAXimum number of Prefit REGions (default:1500)
      integer maxpreg
      parameter ( maxpreg = 1500 )
* MAXimum POlynomial Coefficients (hardwired to 6 for swres readin)
      integer maxpoc
      parameter ( maxpoc = 6)
* Characters for form following
      character*1 special, vspecial
      parameter (special = '%', vspecial='@')
