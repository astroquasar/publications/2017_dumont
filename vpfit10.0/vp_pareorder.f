      subroutine vp_pareorder(nfion,nfunc,kxl,ion,level,atmass,linchk,
     :              parm,ipind)
*     
*     reorder parameter list so that the last ones are the
*     additive parameters.
*
*     nfion is last multiplicative parameter, apart from added-in
*     lines at nfunc to nfunc+1-nionxl. Now put that block starting
*     at nfion+1, and move the special lines to the end.
*
*     IN:
*     nfion   int   last multiplicative ion before any added
*     nfunc   int   total number of ions
*     kxl     int   number of extra ions added
*
      implicit none
      include 'vp_sizes.f'
*
      integer nfion,nfunc,kxl
      character*2 ion(*)
      character*4 level(*)
      double precision atmass(*)
      integer linchk(*)
      double precision parm(*)
      character*2 ipind(*)
*
*     local variables
      integer nfn2,ja,la,jb,lb,nb,np,lap,lap3,jap,jap3
      integer nap,nap3,naxl
      integer j,k
*
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     verbose mode
      logical verbose
      common/vp_sysout/verbose
*
      nfn2=nfion+1+kxl
      if(nfn2.lt.nfunc) then
*       do need to shuffle things around. Need to put last kxl+1, now held in
*       nfunc-kxl to nfunc into nfion+1 to nfion+kxl+1, and those in
*       nfion+1 to nfunc-kxl-1 to nfion+kxl+2 to nfunc, retaining the order
*       so just copy the kxl+1 new ions to the top of the relevant arrays,
*       move the additive ones up to end at nfunc, and then slot the
*       ions in.
*       .. provided there is enough space to do this!
        if(maxnio.gt.nfunc+kxl) then
          naxl=kxl+1
          la=maxnio-naxl
          ja=nfunc-naxl
          if(verbose) then
            write(6,*) naxl,' added lines, starting after',ja
          end if
          do j=1,naxl
            ion(la+j)=ion(ja+j)
            level(la+j)=level(ja+j)
            atmass(la+j)=atmass(ja+j)
            linchk(la+j)=linchk(ja+j)
          end do
*         parameters, np=noppsys*(kxl+1) of them which are in 
*         nfunc*noppsys down
          nb=noppsys*naxl
          lb=maxnpa-nb
          jb=noppsys*nfunc-nb
          do j=1,nb
            parm(lb+j)=parm(jb+j)
            ipind(lb+j)=ipind(jb+j)
          end do
*         now copy up the additive set to end at nfunc (nap of them)
          nap=nfunc-naxl-nfion
          lap=nfion+naxl
          jap=nfion
          if(verbose) then
            write(6,*) nap,' special cases, new starting after',lap
            write(6,*) 'from after',jap
          end if
*         start at the top, else might just propagate the same one
          do k=1,nap
            j=nap-k+1
            ion(lap+j)=ion(jap+j)
            level(lap+j)=level(jap+j)
            atmass(lap+j)=atmass(jap+j)
            linchk(lap+j)=linchk(jap+j)
          end do
          nap3=nap*noppsys
          np=nfunc*noppsys
          lap3=np-nap3
          jap3=nfion*3
          do k=1,nap3
            j=nap3-k+1
            parm(lap3+j)=parm(jap3+j)
            ipind(lap3+j)=ipind(jap3+j)
          end do
*         and copy the line parameters down
          do j=1,naxl
            ion(nfion+j)=ion(la+j)
            level(nfion+j)=level(la+j)
            atmass(nfion+j)=atmass(la+j)
            linchk(nfion+j)=linchk(la+j)
          end do
          do j=1,nb
            parm(jap3+j)=parm(lb+j)
            ipind(jap3+j)=ipind(lb+j)
          end do
         else
          kxl=-1
          write(6,*) 'Ran out of space while re-ordering parameters ..'
          write(6,*) '   .. so gave up the attempt' 
        end if
      end if
      return
      end
