      subroutine rd_cufilnm(inchstr)
*     list of (up to 35) filenames with line lists for stacked plots
      implicit none
      character*(*) inchstr
*     Local:
      integer jfst,jlst,lcsrc,nculen
*     Functions
      integer lastchpos
      integer ncufil,jcufil
      character*72 cufilnm(35)
      common/rdc_cufilnm/cufilnm,ncufil,jcufil
*     input streams
      integer inputc
      common/rdc_inputc/inputc
*     source path
      character*132 csrcpath
      common/vpc_srcpath/csrcpath
*
      if(ncufil.lt.35) then
        ncufil=ncufil+1
        if(inchstr(1:1).eq.' ') then
          write(6,*) 'Filename?'
          read(inputc,'(a)',end=999) cufilnm(ncufil)
          if(cufilnm(ncufil)(1:8).eq.'        ') then
            ncufil=ncufil-1
           else
*           strip off the path bit
            jlst=lastchpos(cufilnm(ncufil))
            jfst=jlst
            do while (jfst.gt.0.and.
     :            cufilnm(ncufil)(jfst:jfst).ne.'/')
*              write(6,*) jfst,cufilnm(ncufil)(jfst:jfst),jlst
              jfst=jfst-1
            end do
            jfst=max(1,jfst)
            write(6,'(i3,2x,a)') ncufil,cufilnm(ncufil)(jfst:jlst)
          end if
         else
*         see if file exists
          jfst=lastchpos(inchstr)
          if(inputc.eq.5) then
            write(6,'(i3,2x,a)') ncufil,inchstr(1:jfst)
          end if
          open(unit=7,file=inchstr,status='old',err=993)
          cufilnm(ncufil)=inchstr
          close(unit=7)
          goto 991
*         Or add path to filename
 993      continue
          if(csrcpath(1:1).ne.' ') then
            lcsrc=lastchpos(csrcpath)
            if(csrcpath(lcsrc:lcsrc).eq.'/') then
              cufilnm(ncufil)=csrcpath(1:lcsrc)//'pgfiles/'//
     :             inchstr(1:jfst)
             else
              cufilnm(ncufil)=csrcpath(1:lcsrc)//'/pgfiles/'//
     :             inchstr(1:jfst)
            end if
*           better check if that exists
            nculen=lastchpos(cufilnm(ncufil))
            open(unit=7,file=cufilnm(ncufil)(1:nculen),
     :             status='old',err=994)
            rewind(unit=7)
            close(unit=7)
            goto 991
          end if
 994      write(6,*) 'file ',inchstr(1:jfst),' does not exist'
          write(6,*) 'Path: ',csrcpath
          write(6,*) 'full name: ',cufilnm(ncufil)
          goto 999
        end if
      end if
 991  return
 999  ncufil=ncufil-1
      goto 991
      end 
