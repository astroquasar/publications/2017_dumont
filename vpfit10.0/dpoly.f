
      subroutine dpoly(x,p,np)
      integer np
      double precision p(np), x

      integer j

      p(1)=1.0d0
      do  j=2,np
        p(j)=p(j-1)*x
      end do

      end
