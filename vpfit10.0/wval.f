      double precision function wval(tin)
*     convert channel no (double precision) to wavelength using vpc_wavl
      implicit none
      include 'vp_sizes.f'
      double precision tin
*
*     Local
      integer i,n
      double precision t,tx
      double precision d,dd,sv,y,y2
*     general wavelength coefficients
      integer nwco
      double precision wcf1(maxwco)
      common/vpc_wavl/wcf1,nwco
*
      character*8 wcftype
      character*4 vacind
      double precision helcfac
      integer*4 noffs
      common/c8_wcftype/wcftype,helcfac,noffs,vacind
*     general wavelength coefficients
      n=nwco
      t=tin
*     subtract IRAF offset
      t=t-dble(noffs)
*
      if(n.gt.20.or.wcftype(1:3).eq.'arr') then
*       wavelength array instead of polynomial or log set
        i=int(t)
*     linearly interpolate for non-integer t'
*     extrapolating if outside range!
        if(i.gt.n) then
          i=n-1
         else
          if(i.le.0) then
            i=1
          end if
        end if
        tx=t-dble(i)
        wval=wcf1(i)*(1.0d0-tx) + wcf1(i+1)*tx
       else
        if(wcftype(1:3).eq.'log') then
*         IRAF logarithmic bins
          wval=10.0d0**(wcf1(1)+wcf1(2)*t)
         else
          if(wcftype(1:4).eq.'cheb') then
*           Chebyschev polynomial evaluation
*           ac(n) contains: (1) lower limit of range
*		      (2) upper limit
*		      (3) - (n) the Cheb. coeffts
*           so polynomial has order (in the IRAF sense) n-2
            d=0.0d0
            dd=0.0d0
            y=(2.0d0*t-wcf1(1)-wcf1(2))/(wcf1(2)-wcf1(1))
            y2=2.0d0*y
            do i=n,4,-1
              sv=d
              d=y2*d-dd+wcf1(i)
              dd=sv
            end do
*           half coefft last step as usual
            wval=y*d-dd+0.5d0*wcf1(3)
           else
*           assume a polynomial fit of order n-1
            wval = wcf1(n)
            do i = n-1, 1, -1
              wval = wval * t + wcf1(i)
            end do
          end if
        end if
      end if
*
*     heliocentric correct
      wval=wval*helcfac
*
*     correct air to vacuum if necessary
      if(vacind(1:3).eq.'air') then
        tx=(1.0d4/wval)**2
*       Morton version of Edlen formula replaced by Edlen 1996 formula
        wval=wval*(1.0d0+1.0d-8*(8342.13d0+
     :        2406030.d0/(130.d0-tx)+15997.d0/(38.9d0-tx)))
*       instead of (1.0d0 + 6.4328d-5 +
*    :        2.94981d-2/(146.0d0-tx) + 2.554d-4/(41.0d0-tx))
      end if
      return
      end
