      subroutine rd_intvtab
*     set up internal table, in common, of ions and wavelengths for
*     use with cursor mode velocity plotting
      implicit none
      include 'vp_sizes.f'
*     local:
      character*2 indcol,indb,indz
      integer i
      double precision trstw,col,bval,zval
      double precision dwrst,ctwrst
*     atomic data
      integer nz
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),nz
*     readin workspace
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
*     double precision version
      double precision dvstr(24)
      common/vpc_dsepspace/dvstr
*     ion/wavelength list
      integer ntrwave
      character*2 tatom(16)
      character*4 tion(16)
      double precision trwave(16)
      common/rdc_intvtab/trwave,ntrwave,tion,tatom
*
*     get atomic data if it is not already read
      if(nz.le.0) call vp_ewred(0)
      write(6,*) 'ion, wavelength <CR> to end'
      ntrwave=0
 301  read(5,'(a)') inchstr
      if(inchstr(1:8).ne.'        ') then
        ntrwave=ntrwave+1
        call vp_initval(inchstr,tatom(ntrwave),tion(ntrwave),trstw,
     :                       col,indcol,bval,indb,zval,indz)
*	correct trstw to nearest wavelength from the table
        dwrst=1.0d20
        trwave(ntrwave)=trstw
        do i=1,nz
          if(lbz(i).eq.tatom(ntrwave).and.
     :             lzz(i).eq.tion(ntrwave)) then
            ctwrst=abs(alm(i)-trstw)
            if(ctwrst.lt.dwrst) then
              trwave(ntrwave)=alm(i)
              dwrst=ctwrst
            end if
          end if
        end do
      end if
      if(inchstr(1:8).ne.'        '.and.ntrwave.lt.16) goto 301
      return
      end
