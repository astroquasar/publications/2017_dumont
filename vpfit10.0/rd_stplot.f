      subroutine rd_stplot(dh,dhe,drms,re,ngp,ind)
*
*     setup for multiline velocity plot
*
      implicit none
      include 'vp_sizes.f'
*     parameters passed to splot
      integer ngp,ind
      double precision dh(maxnpl),dhe(maxnpl),re(maxnpl)
      double precision drms(*)
*
*     LOCAL variables
      character*16 chpgh
      character*8 chtemp
      integer i,jlins,jtype,js
      real rpg
      double precision bval,col,trstw,zval
      double precision ddum,r,temp
      double precision dwrst,ctwrst
      character*2 oldatom
      character*4 oldion
      character*2 indcol,indb,indz
*
*     COMMON:
*     stack spectra variables
      character*2 atom(64)
      character*4 ion(64)
      double precision rstwav(64)
      integer nlins
      common/rdc_stplvar/atom,ion,rstwav,nlins
*
*     list of (up to 35) filenames with line lists for stacked plots
      integer ncufil,jcufil
      character*72 cufilnm(35)
      common/rdc_cufilnm/cufilnm,ncufil,jcufil
*     limit information
      integer l1,l2,lc1,lc2
      real fampg
      common/ppam/l1,l2,lc1,lc2,fampg
*     min and max yscales by hand, and default lower level 
*     nyuse is no times to use set before reset (cursor use)
      integer nyuse
      real ylowhpg,yhihpg,yminsetpg
      common/pgylims/ylowhpg,yhihpg,yminsetpg,nyuse
*     pgplot overplot with bias
      integer ipgov
      logical plgscl
      real pgbias
      common/pgover/ipgov,pgbias,plgscl
*     plot/print velocity scale?
      logical lvel
      real wcenpg,vellopg,velhipg,zp1refpg
      common/vpc_pgvels/wcenpg,vellopg,velhipg,lvel,zp1refpg
*     observed wavelengths?
      logical lobswav,ltick
      common/rdc_lobswav/lobswav,ltick
*     text label for a curve
      integer icoltext
      character*24 cpgtext
      real xtextpg,ytextpg
      common/pgtexts/xtextpg,ytextpg,cpgtext,icoltext
*     pgplot attributes: data, error, continuum, axes, ticks, RESIDUAL
*	  1 color indx; 2 line style; 3 line width; 
*	  4 curve(0), hist(1); 5 - 10 reserved.
      integer ipgatt(10,9)
      common/vpc_pgattrib/ipgatt
*     plot array pointers (consistent with ipgatt)
*     0=don't,1=do for 1:data,2:error,3:cont,6:resid,9:rms
      integer ksplot(9)
      common/vpc_ksplot/ksplot
*     redirect next plot variables
      logical lsnap
      integer nsnap
      character*8 pgsfile,pghfile,pglfile
      common/rdc_pgsnap/lsnap,nsnap,pgsfile,pghfile,pglfile
      character*4 plsnaptype
      common/rdc_snaptype/plsnaptype
*     colors for stacked plots
      integer kpgcoltmp(16)
      integer kpgcol(16),kpgcolalt(16),nkpgcol
      common/rdc_kpgcol/kpgcol,kpgcolalt,nkpgcol
*     atomic data
      integer nz
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),nz
      character*60 pglcapt
      real chszoldpg,chszpg
      common/rdc_pglcapt/pglcapt,chszoldpg,chszpg
*     character variable space
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
      double precision dvstr(24)
      common/vpc_dsepspace/dvstr
*     ion/wavelength list
      integer ntrwave
      character*2 tatom(16)
      character*4 tion(16)
      double precision trwave(16)
      common/rdc_intvtab/trwave,ntrwave,tion,tatom
*
      if(nz.le.0) call vp_ewred(0)
      icoltext=0
      oldatom='  '
      oldion='    '
      nlins=0
      if(jcufil.gt.0) then
        open(unit=7,file=cufilnm(jcufil),status='old',err=993)
*       read in the number of lines
 1      read(7,'(a)',end=992) inchstr
        if(inchstr(1:8).ne.'        ') then
          nlins=nlins+1
          call vp_initval(inchstr,atom(nlins),ion(nlins),trstw,
     :                       col,indcol,bval,indb,zval,indz)
*         correct trstw to nearest wavelength from the table
          dwrst=1.0d20
          rstwav(nlins)=trstw
          do i=1,nz
            if(lbz(i).eq.atom(nlins).and.
     :               lzz(i).eq.ion(nlins)) then
              ctwrst=abs(alm(i)-trstw)
              if(ctwrst.lt.dwrst) then
                rstwav(nlins)=alm(i)
                dwrst=ctwrst
              end if
            end if
          end do
        end if
        if(nlins.lt.50) goto 1
        write(6,*) ' Only first 50 lines used'
*       end of file
 992    close(unit=7)
 993    continue
       else
        if(jcufil.eq.0) then
*         use internal table
          nlins=ntrwave
          do i=1,nlins
            atom(i)=tatom(i)
            ion(i)=tion(i)
            rstwav(i)=trwave(i)
          end do
        end if
      end if
      if(nlins.gt.0) then
*       hardcopy plot?
        if(lsnap) then
*         switch colors
          do js=1,nkpgcol
            kpgcoltmp(js)=kpgcolalt(js)
          end do
          nsnap=nsnap+1
          if(nsnap.lt.10) then
            write(chpgh,'(a6,i1,a4)') '"pgp10',nsnap,'.ps"'
           else
            write(chpgh,'(a5,i2,a4)') '"pgp1',nsnap,'.ps"'
          end if
          chpgh=chpgh(1:11)//pghfile(1:5)
          write(6,*) 'Plotfile is ',chpgh(2:10)
          call pgbegin(0,chpgh,1,1)
          call pgask(.false.)
         else
*	  screen colors
          do js=1,nkpgcol
            kpgcoltmp(js)=kpgcol(js)
          end do
        end if
*       set plot height assuming normalized to unity
 905    yhihpg=real(nlins)+0.5
        nyuse=1
        ylowhpg=-1.0e25
        jlins=1
        jtype=-1
*       xtext position
        xtextpg=0.9*vellopg+0.1*velhipg
*       cycle through the lines
        do while (jlins.le.nlins)
          if(jlins.gt.1) then
*           overlay plot
            ipgov=1
            pgbias=real(jlins-1)
          end if
*         have atom,ion,wavelength
          wcenpg=rstwav(jlins)*zp1refpg
*
          if (lvel) then
*           sort out plot limits for the velocity parameters
            rpg=vellopg/2.99792458e5
            r=wcenpg*sqrt((1.0+rpg)/(1.0-rpg))
            call chanwav(r,ddum,1.0d-3,100)
            l1=int(ddum)
            rpg=velhipg/3.0e5
            r=wcenpg*sqrt((1.0+rpg)/(1.0-rpg))
            call chanwav(r,ddum,1.0d-3,100)
            l2=int(ddum)
          end if
*
*         labels to each line
          ytextpg=0.5+real(jlins-1)
          write(cpgtext,'(f8.2)') rstwav(jlins)
          cpgtext=atom(jlins)//ion(jlins)//cpgtext(1:8)
          if(lobswav) then
*           For those who want observed wavelengths as well
*           (this should completely clutter the plot!)
            temp=rstwav(jlins)*zp1refpg
            write(chtemp,'(f8.1)') temp
            cpgtext=cpgtext(1:14)//' @'//chtemp(1:8)
          end if
*         set curve colour cyclically
          if(atom(jlins).ne.oldatom.or.
     :         ion(jlins).ne.oldion) then
            jtype=jtype+1
            oldatom=atom(jlins)
            oldion=ion(jlins)
          end if
 67       ipgatt(1,1)=kpgcoltmp(1+(jtype-(jtype/nkpgcol)*nkpgcol))
*         check this colour is not in use:
*         if other lines plotted, avoid their colours
          if(ksplot(2).gt.0.and.ipgatt(1,1).eq.ipgatt(1,2)) then
*           avoid error colour
            jtype=jtype+1
            goto 67
          end if
          if(ksplot(3).gt.0.and.ipgatt(1,1).eq.ipgatt(1,3)) then
*           avoid continuum colour
            jtype=jtype+1
            goto 67
          end if
          ind=2
          call splot(dh,dhe,drms,re,ngp,ind)
          jlins=jlins+1
        end do
*       finished, so unset overlay and bias
        ipgov=0
        pgbias=0.0
        if(lsnap) then
          lsnap=.false.
          call pgbegin(0,pgsfile,1,1)
          call pgask(.false.)
          oldatom='  '
          oldion='    '
          goto 905
        end if
       else
*       don't know why this was called, so plot as is
        call splot(dh,dhe,drms,re,ngp,ind)
      end if
*     reset so don't rescale variables
      plgscl=.false.
      return
      end
