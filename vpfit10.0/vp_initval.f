      subroutine vp_initval(chstr,atom,ion,wavel,col,indcol,bval,
     1                       indb,zval,indz)
*
*     To unscramble the input string CHSTR, to give initial guesses
*     for atom, ion, wavel,....
*
      implicit none
*     Input:
      character*132 chstr
*
*     Output:
      character*2 atom
      character*4 ion
      double precision wavel,col,bval,zval
      character*2 indcol,indb,indz
*
*     workspace
      integer istat,nv,nvtemp
      integer i,j,jb,jm,jmp,lench,next
      character*32 cv(4),ctemp,cvtemp(2)
      double precision drv(4),drvtemp(2)
      integer iv(4)
      character*2 inddrv(4),indc
*     character collating sequence ranges
      integer nchlims
      common/vpc_charlims/nchlims(6)
*
*     strip out any initial blanks
      lench=len(chstr)
      do while (chstr(1:1).eq.' '.and.lench.gt.1)
        chstr=chstr(2:lench)
        lench=lench-1
      end do
*
*     determine the element
*     nchlims used, with HD, CO exceptions allowed for
      if(ichar(chstr(2:2)).ge.nchlims(3).and.
     :     ichar(chstr(2:2)).le.nchlims(4).and.
     :     chstr(1:2).ne.'HD'.and.chstr(1:2).ne.'CO') then
        atom=chstr(1:1)//' '
        next=2
       else
        atom=chstr(1:2)
        next=3
      end if
*
*     Now look for the ion, unless it is a special character:
      if(atom.eq.'<>'.or.atom.eq.'__'.or.atom.eq.'>>'.or.
     :     atom.eq.'<<'.or.atom.eq.'AD'.or.atom.eq.'??') then
        ion='    '
        j=3
       else
        j=next
        do while(chstr(j:j).ne.' '.and.j.lt.132)
          j=j+1
        end do
*
        jb=j-1
*
        if(jb.ge.next) then
          ion=chstr(next:jb)
         else
          ion='    '
        end if
      end if
*     check the ion is in the list, and replace it with something
*     sensible if it is not
      call vp_chkion(atom,ion,istat)
*
      next=j
*
*     unscramble free format
*
*     set defaults
      indcol=' '
      indb=' '
      indz=' '
      wavel=0.0d0
      col=0.0d0
      bval=0.0d0
*     ajc 12-7-93 initial silly to force cursor
      zval=-1.0d3
*     separate string
      call dsepvar(chstr(next:132),4,drv,iv,cv,nv)
*
*     for any zero values try dropping the last non-blank character and
*     trying again..
      if(nv.gt.0) then
        do i=1,4
          inddrv(i)='  '
        end do
        do i=1,nv
          j=32
*         find last non-blank character in the string (j)
          do while(j.gt.1.and.cv(i)(j:j).eq.' ')
            j=j-1
          end do
*	  back off up to two characters to find a potential number
          jm=j-1
          if(jm.gt.0) then
            jmp=jm+1
            indc=cv(i)(jmp:jmp)//' '
*	    is this a number, or '.'?
            if(indc.ne.'.'.and.indc.ne.'0'.and.
     1        indc.ne.'1'.and.indc.ne.'2'.and.indc.ne.'3'.and.
     2        indc.ne.'4'.and.indc.ne.'5'.and.indc.ne.'6'.and.
     3        indc.ne.'7'.and.indc.ne.'8'.and.indc.ne.'9') then
              ctemp=cv(i)(1:jm)
              call dsepvar(ctemp,1,drvtemp,iv,cvtemp,nvtemp)
              if(drvtemp(1).ne.0.0d0) then
                drv(i)=drvtemp(1)
                inddrv(i)=indc
               else
*	        still not a number, so try stripping another character:
                jm=jm-1
                if(jm.ge.0) then
                  jmp=jm+1
                  indc=cv(i)(jmp:j)
                  if(jm.gt.0) then
                    ctemp=cv(i)(1:jm)
                    call dsepvar(ctemp,1,drvtemp,iv,cvtemp,nvtemp)
                    drv(i)=drvtemp(1)
                    inddrv(i)=indc
                   else
                    drv(i)=0.0d0
                    inddrv(i)=indc
                  end if
                 else
                  drv(i)=0.0d0
                  inddrv(i)=cv(i)(j:j)//' '
                end if
              end if
            end if
          end if
        end do
*       check that the first value is a sensible wavelength
*	and if it is not, just set wavelength to appropriate
*	value for special cases and zero otherwise.
        if(drv(1).lt.25.0d0.or.drv(1).gt.1.0d7) then
          if(atom.eq.'<>'.or.atom.eq.'__'.or.atom.eq.'>>'.or.
     :       atom.eq.'<<') then
            wavel=1215.67d0
           else
            wavel=0.0d0
          end if
          if(drv(1).gt.30.0d0) then
            col=log10(drv(1))
           else
            col=drv(1)
          end if
          indcol=inddrv(1)
          bval=drv(2)
          indb=inddrv(2)
*	  ajc 12-7-93 see note above
          if ( nv .ge. 3 ) then
            zval=drv(3)
            indz=inddrv(3)
          end if
         else
          wavel=drv(1)
          if(drv(2).gt.30.0d0) then
            col=log10(drv(2))
           else
            col=drv(2)
          end if
          indcol=inddrv(2)
          bval=drv(3)
          indb=inddrv(3)
*	  ajc 12-7-93 see note above
          if ( nv .ge. 4 ) then
            zval=drv(4)
            indz=inddrv(4)
          end if
        end if
      end if
      return
      end


      subroutine vp_chkion(atval,ival,istat)
*     checks that atom and ion are in the list of available
*     ones, and replaces if not
      implicit none
      include 'vp_sizes.f'
      character*2 atval
      character*4 ival
      integer istat
*
      integer i
      character*6 chsx
*     
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm
      integer nz
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),nz
      integer nopt
      common/vpc_option/nopt
*     character collating sequence ranges
      integer nchlims
      common/vpc_charlims/nchlims(6)
*     output channels:
      integer nopchan,nopchanh,nmonitor
      integer lt(3)             ! output channels for printing
      common/vpc_nopchan/lt,nopchan,nopchanh,nmonitor
*
      istat=0
 1    if(atval.ne.'  '.or.ival.ne.'   ') then
        i=1
        do while (atval.ne.lbz(i).or.ival.ne.lzz(i))
          i=i+1
          if(i.gt.nz) goto 2
        end do
      end if
*     atom/ion combination exists, so no problem -- exit
      goto 999
*
 2    write(6,*) atval,ival,' not in table, (length',nz,')'
      istat=1
      if(nopt.eq.6.or.nopt.eq.5.or.nopt.eq.1) then
*	interactive mode, so prompt for new values
*	(not in the graphics window this time -- the user
*	might notice nothing is happening after a while if
*	in that mode and wonder why. This so they think
*	about what they are doing, and because it is easier
*	to program!)
        write(6,*) 'What should the atom and ion have been?'
        write(6,'(''> '',$)')
        read(5,'(a)') chsx
*	unscramble if necessary:
        atval(1:1)=chsx(1:1)
*       nchlims used, with HD, CO exceptions allowed for
        if(ichar(chsx(2:2)).ge.nchlims(3).and.
     :       ichar(chsx(2:2)).le.nchlims(4).and.
     :       chsx(1:2).ne.'HD'.and.chsx(1:2).ne.'CO') then
          atval(2:2)=' '
          ival(1:4)=chsx(2:5)
         else
          ival(1:4)=chsx(3:6)
          atval(2:2)=chsx(2:2)
        end if
*	branch back to check the new value is OK
        goto 1
       else
*	impossible task -- trying to strip the typos
*	so don't even bother -- just write a warning everywhere
*	and expect the program to crash later.
        write(6,*) ' RESULTS NOW UNPREDICTABLE, '
        write(6,*) '   AND THE PROGRAM MAY CRASH'
        if(nopchan.ge.2) then
          write(18,*) ' ',atval,ival,' NOT IN TABLE'
          write(18,*) ' RESULTS NOW UNPREDICTABLE'
        end if
      end if
*
 999  return
      end


