      subroutine pr_mdian1(x,y,n,xmed)
*     determine the median XMED of an array X of N numbers
*     Y(N) is used as workspace, and on exit contains the sorted X array
      implicit none
      integer n
      double precision x(n),y(n)
      double precision xmed
*
      integer i,n2
*     copy X array
      do i=1,n
        y(i)=x(i)
      end do
      call pr_sort(n,y)
      n2=n/2
      if(2*n2.eq.n) then
        xmed=0.5d0*(y(n2)+y(n2+1))
       else
        xmed=y(n2+1)
      end if
      return
      end
      subroutine pr_sort(n,ra)
*     sorts an array RA of length N into ascending numerical order
*     using the Heapsort algorithm. RA is replaced by its sorted version.
      implicit none
      integer n
      double precision ra(n)
*     Local
      integer l,ir,i,j
      double precision rra
      l=n/2+1
      ir=n
      i=l
 10   continue
      if(l.gt.1) then
        l=l-1
        rra=ra(l)
       else
        rra=ra(ir)
        ra(ir)=ra(1)
        ir=ir-1
        if(ir.eq.1) then
          ra(i)=rra
          return
        end if
      end if
      i=l
      j=l+l
 20   if(j.le.ir) then
        if(j.lt.ir) then
          if(ra(j).lt.ra(j+1)) j=j+1
        end if
        if(rra.lt.ra(j)) then
          ra(i)=ra(j)
          i=j
          j=j+j
         else
          j=ir+1
        end if
        goto 20
      end if
      ra(i)=rra
      goto 10
      end
