      subroutine rd_readlinwav(nct,da,de,ca,drms)
*     read in new linear wavelength coeffts, and array length
      implicit none
      include 'vp_sizes.f'
*     subroutine parameter list
      integer nct
      double precision da(*),de(*),ca(*),drms(*)
*     Local
      integer i,nvxx
      double precision emax
*     Common
*     input stream
      integer inputc
      common/rdc_inputc/inputc
*     variable separation space
      character*132 inchstr
      character*60 cv(24)
      real rvss(24)
      integer iv(24)
      integer nv
      common/rd_chwork/inchstr,rvss,iv,cv,nv
      double precision dv(24)
      common/vpc_dsepspace/dv
*     single array wavelength parameter set
      character*8 wcftype
      character*4 vacind
      double precision helcfac
      integer*4 noffs
      common/c8_wcftype/wcftype,helcfac,noffs,vacind
*     single array wavelengths
      integer nwco
      double precision wcf1(maxwco)
      common/vpc_wavl/wcf1,nwco
*     wavelength parameter sets
      character*8 wcfty2(maxnch)
      character*4 vacin2(maxnch)
      double precision helcf2(maxnch)
      integer nwcf2(maxnch),noffs2(maxnch)
      common/vpc_wcpars2/wcfty2,helcf2,nwcf2,noffs2,vacin2
*     multidimensional wavelength coefficients...
      double precision wcfd(maxwco,maxnch)
      common/vpc_wcfd/wcfd
*
      write(6,*) 'base, increment,length[,log]? (one line)' 
      read(inputc,'(a)') inchstr
      call dsepvar(inchstr,6,dv,iv,cv,nv)
      write(6,*) 'New base, increment, length:'
      nvxx=min0(nv,3)
      write(6,'(f15.3,f12.7,1pe12.4,3e12.4)') (dv(i),i=1,nvxx)
      if(dv(2).ne.0.0d0) then
        do i=1,2
          wcf1(i)=dv(i)
        end do
        do i=3,maxwco
          wcf1(i)=0.0d0
        end do
        do i=1,maxwco
          wcfd(i,1)=wcf1(i)
        end do
        nwcf2(1)=2
        noffs2(1)=0
        if(cv(4)(1:3).eq.'log') then
*         log wavelengths, so set indicator
          wcftype='loglin'
          wcfty2(1)='loglin'
         else
          wcfty2(1)='poly'
          wcftype='poly'
        end if
        noffs=0
        helcfac=1.0d0
        vacind='vacu'
        vacin2(1)='vacu'
        nwco=2
        if(iv(3).eq.0) then
          iv(3)=nct
        end if
        nct=max(200,iv(3))
       else
*       increment per channel is zero, so don't change anything
        write(6,*) 'Wavelength coefficients not reset'
      end if
      if(iv(3).gt.200) then
        nct=iv(3)
      else
        write(6,*) 'Data length set to 200'
        nct=200
      end if
*     if there is no data at all, fill the arrays with something
      emax=-1.0e30
      do i=1,nct
        if(de(i).gt.emax) emax=de(i)
      end do
      if(emax.le.0.0d0) then
        da(i)=1.0d0
        de(i)=1.0d0
        ca(i)=1.0d0
        drms(i)=1.0d0
      end if
      return
      end
