      subroutine vp_emscchk(parm,nparm)
*     check the scale variable for the emission line normalization
*     parameter, and reset it if necessary. Uses nppcol position.
*
      implicit none
*
      integer nparm
      double precision parm(nparm)
*
*     LOCAL:
      integer i,j,nlines
      double precision parmin,parmax
*     COMMON:
*     scale factor
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*
      if(indvar.ne.-1) goto 901
*     dealing with emission line only option
      parmin=1d30
      parmax=-1d30
      nlines=nparm/noppsys
      do j=1,nlines
        i=noppsys*(j-1)+nppcol
        if(parm(i).gt.parmax) parmax=parm(i)
        if(parm(i).lt.parmin) parmin=parm(i)
      end do
*     set scale by looking at absolute value
      if(parmin.lt.0.0d0) parmin=-parmin
      if(parmax.lt.0.0d0) parmax=-parmax
      parmax=0.5d0*(parmax+parmin)
      if(parmax.le.0.0d0) parmax=1.0d0
*     rescale the variables
      do j=1,nlines
        i=noppsys*(j-1)+nppcol
        parm(i)=parm(i)/parmax
      end do
*     and make sure the other routines know you've done it
      scalefac=scalefac/parmax
      write(6,*) 'scalefac= ',scalefac,nparm,nlines
 901  return
      end
