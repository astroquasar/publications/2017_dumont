      subroutine vp_sysdel(elm,ionz,parm,ipind,np,krej)
*
*     remove system # krej from the list
*
      implicit none
      character*2 elm(*)
      character*4 ionz(*)
      double precision parm(*)
      character*2 ipind(*)
      integer np,krej
*     local
      integer nsys
*     Common:
*     parameter variables [rfc 13.07.06]
*     noppsys=# parameters per system (max 5; 3 for standard N,z,b),
*     nppcol=col posn (1 usually); nppbval (3); nppzed (2)
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*
*
      nsys=np/noppsys
      return
      end
