      subroutine rd_meantau(da,de,ca,nct,wlo,whi,cthres)
*
*     IN:
*     da	data array
*     de	variance array
*     ca	continuum array
*     nct array length
*     wlo	lower wavelength limit for average
*     whi	upper limit
*     cthres	continuum threshold (fraction). unused if zero
*
      implicit none
      integer nct
      double precision da(nct),de(nct),ca(nct)
      double precision wlo,whi,cthres
*
*     Local:
      integer j,nlo,nhi
      double precision ddum
      double precision rmsx,sumc,rmsc,chc
*     convert wavelength to channel limits:
      call chanwav(wlo,ddum,1.0d-3,30)
      nlo=int(ddum+0.5d0)
      if(nlo.lt.1) nlo=1
      call chanwav(whi,ddum,1.0d-3,30)
      nhi=int(ddum+0.5d0)
      if(nhi.gt.nct) then
         nhi=nct
      end if
      write(6,*) nlo,nhi,'  (',nct,')'
      if(nlo.le.nhi) then
        rmsx=0.0d0
        sumc=0.0d0
        rmsc=0.0d0
        chc=0.0d0
        do j=nlo,nhi
          if(de(j).gt.0.0d0.and.ca(j).gt.cthres) then
*	    DON'T INVERSE VARIANCE WEIGHT -- VARIANCE IN
*	    BOTTOMS OF LINES IS LOW, SO THEY ARE WAY OVERWEIGHTED
*	    raw average
            sumc=sumc+1.0d0-da(j)/ca(j)
            rmsc=rmsc+de(j)/(ca(j)**2)
            chc=chc+1.0d0
          end if
        end do
        if(chc.gt.0.0d0) then
          sumc=sumc/chc
          rmsc=rmsc/chc
          rmsc=sqrt(rmsc)
          rmsx=rmsc/sqrt(chc)
          write(6,*) 'Straight average of acceptable data points:'
          write(6,*) '<1-data/cont>=',sumc,',  err=',rmsx
          write(6,*) '   mean error/pix=',rmsc,' npts=',chc
        end if
       else
        write(6,*) 'High channel',nhi,' < low',nlo
      end if
      return
      end
