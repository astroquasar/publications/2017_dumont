      subroutine vp_rdlines(nopt,nn,nlines,ion,level,trstw,
     1       parm,rstwav,z,contav)
*
      implicit none
      include 'vp_sizes.f'
*
*     IN:
*     nopt       int    option number
*     contav     dble	average continuum for chunk, for zero estimator
*     
      integer nopt,nn,nlines
      character*2 ion(maxnio)
      character*4 level(maxnio)
      double precision trstw,parm(*)
      double precision rstwav(*),z(*)
      double precision contav
*
*     LOCAL:
      character*132 inchstr
      integer i,ielmnt,lb
      integer nchline,nxxc,ntemp,nntemp
      double precision ctwrst,dwrst,fval,obswav,ptempt
      double precision vacwav,zlo,zhi
*
*     FUNCTIONS:
      double precision vp_wval,wcor
*
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     chunk variables (just for nchunk here):
      integer ndpts(maxnch),idrun(maxnch),icrun(maxnch)
      integer indfil(maxnch)
      character*64 filename(maxnch)
      double precision wvstrt(maxnch),wvend(maxnch)
      integer nchunk
      common/vpc_chunk/wvstrt,wvend,ndpts,filename,
     :      idrun,icrun,indfil,nchunk
*     log or linear variable indicator
      integer indvar
      double precision scalelog,scalefac
      common/vpc_varstyle/scalelog,scalefac,indvar
*     general control variables
      character*4 chcv(10)
      common/vpc_chcv/chcv
*     chcv(1) controls use of cursor for input guesses N,b,z
*
*     cursor positional information
      character*1 chc,chcold
      real wv1pg,y1pg           ! pgplot variables
      common/vpc_gcurv/wv1pg,y1pg
*
*     last line info for cursor
      character*80 chline
      common/vpc_hline/chline
*
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm
      integer nz 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),nz
*
      character*2 ipind(maxnpa)
      integer isod,isodh
      common/vpc_usoind/ipind,isod,isodh
*     region flags
      integer linchk( maxnio )
      common/vpc_linchk/linchk
*
*
      ielmnt=0
 989  continue
      if(nopt.ne.7.and.nopt.ne.4.and.nopt.ne.2) then
*       data order if keyboard: ..---wwww.ww_n.nnnnn_bb.bIz.zzzzzzI
*       cursor input was requested
        if(chcv(1).eq.'gcur') then
          write(6,*) ' In the graphics window ...'
          write(6,'(a,i4,a)') ' Line ',nlines,
     :     ' : ion, lamda0? or <CR> to end list'
          nxxc=0
          chc=' '
          chcold=' '
          inchstr=' '
*         input from cursor window, terminate on <CR>
*         or 'XX' = double right click
          do while (ichar(chc).ne.13.and.
     :               (chc.ne.'X'.and.chcold.ne.'X'))
            chcold=chc
            nxxc=nxxc+1
            call vp_pgcurs(wv1pg,y1pg,chc)
            if(ichar(chc).eq.127.or.ichar(chc).eq.8) then
*	      delete previous character if <DEL> or <BS>
              if(nxxc.ge.2) nxxc=nxxc-2
              if(nxxc.ge.1) then
                write(6,'(/,a,$)') inchstr(1:nxxc)
                call flush(6)
               else
                write(6,*)
              end if
             else
              inchstr(nxxc:nxxc)=chc
              ntemp=ichar(chc)
              if(ntemp.ne.13) then
                write(6,'(a1,$)') chc
                call flush(6)
               else
                write(6,*)
                call flush(6)
              end if
            end if
          end do
          inchstr(nxxc:nxxc)=' '
*         monitor character codes (no longer needed):
*         write(6,'(20i4)') (ichar(inchstr(i:i)),i=1,nxxc)
         else
          write(6,'(a,i4,a)') ' Line ',nlines,
     :     ' : ion, lamda0, N,b,z? <CR> to end'
          read(5,'(a)') inchstr
        end if
*       or right mouse button repeated for repeated ion:
        if(inchstr(1:1).eq.'*'.or.inchstr(1:2).eq.'XX') then
          nchline=80
          do while(chline(nchline:nchline).eq.' '.and.
     :            nchline.gt.3)
            nchline=nchline-1
          end do
          write(6,*) chline(1:nchline)
          inchstr=chline
         else
          chline=inchstr(1:80)
        end if
*       decode the character input
        call vp_initval(inchstr,ion(nlines),level(nlines),trstw,
     2      parm(nn+1),ipind(nn+1),parm(nn+3),ipind(nn+3),
     3      parm(nn+2),ipind(nn+2))
      endif
*
      obswav=0.0d0
      if(ion(nlines).ne.' ') then
c       default ionization level for hydrogen only
        if(ion(nlines).eq.'H ') then
          if(level(nlines).eq.'   ') then
            level(nlines)='I  '
          end if
          if(trstw.le.0.0d0) then
            trstw=1215.67d0
          end if
        end if
c
c       correct trstw to nearest wavelength from the table,
c       result rstwav(nlines)
        dwrst=1.0d20
        rstwav(nlines)=trstw
        do i=1,nz
          if(lbz(i).eq.ion(nlines).and.lzz(i).eq.level(nlines))then
            ctwrst=abs(alm(i)-trstw)
            if(ctwrst.lt.dwrst) then
*	      and keep the table position for the other variables
              ielmnt=i
              rstwav(nlines)=alm(i)
              dwrst=ctwrst
            end if
          end if
        end do
*
        write(6,*) 'Wavelength used:',rstwav(nlines)
        if(ion(nlines).eq.'__') then
*	  call vp_emtset('__')
*	  set up zero level variable parameters
          if(chcv(1).eq.'gcur') then
*	    from the cursor position
            write(6,*) 'Set cursor on estimated zero level'
            call vp_pgcurs(wv1pg,y1pg,chc)
*	    need to compare y1pg with local continuum
            if(contav.gt.0.0d0) then
              parm(nn+1)=dble(y1pg)/contav
              if(parm(nn+nppcol).lt.1.0d0) then
                parm(nn+nppcol)=1.0d0
              end if
              write(6,*) 'Continuum: ',contav,'  zero fraction ',
     :               parm(nn+nppcol)
             else
              parm(nn+nppcol)=0.01d0
            end if
            parm(nn+nppzed)=dble(wv1pg)/rstwav(nlines)-1.0d0
            parm(nn+nppbval)=100.0d0
            ipind(nn+nppzed)='SY'
            ipind(nn+nppbval)='SZ'
           else
*	    from the command line, which is sorted out below
            ipind(nn+nppzed)='SY'
            ipind(nn+nppbval)='SZ'
          end if
*	  associate appropriate chunk
          linchk(nlines)=nchunk
        end if
*
*	continuum variables
        if(ion(nlines).eq.'<>') then
          if(chcv(1).eq.'gcur') then
*	    using cursor
            write(6,*) 
     :       'Set cursor on estimated continuum near center'
            write(6,*) '[left button: zero slope; right: variable]'
            call vp_pgcurs(wv1pg,y1pg,chc)
            parm(nn+nppcol)=dble(y1pg)/contav
            if(parm(nn+nppcol).lt.0.5d0) then
              parm(nn+nppcol)=0.5d0
            end if
            parm(nn+nppzed)=dble(wv1pg)/rstwav(nlines)-1.0d0
            parm(nn+nppbval)=0.0d0
            ipind(nn+nppzed)='SW'
*	    X is code returned for right button
            if(chc.eq.'X') then
              ipind(nn+nppbval)='  '
             else
              ipind(nn+nppbval)='SX'
            end if
           else
*	    from the command line, so up to user to fix slope
            ipind(nn+nppzed)='SW'
          end if
*	  associate appropriate chunk
          linchk(nlines)=nchunk
        end if
*
*	column density - defaults, logs or linear
        nntemp=nn+nppcol
*	special treatment for zero level
        if(ion(nlines).eq.'__'.or.ion(nlines).eq.'<>'.or.
     :       ion(nlines).eq.'>>') goto 3041
        if(parm(nntemp).gt.34.0d0) then
c	  input col density values
          if(indvar.eq.1) then
c	    progam uses logs
            parm(nntemp)=scalelog*log10(parm(nntemp))
           else
c	    program uses linear values
            parm(nntemp)=scalefac*parm(nntemp)
          end if
         else
c	  input values are logs
          if(indvar.ne.-1) then
            if(parm(nntemp).le.5.0d0) then
c	      value too low to be sensible, so insert default
*	      unless cursor input was requested
              if(chcv(1).eq.'gcur') then
                fval=fik(ielmnt)
                call vp_parmcest(parm,nn,rstwav(nlines),fval,
     :                           obswav,nchunk)
               else
                parm(nntemp)=13.5d0
                write(6,*) ' LogN=13.5 assumed'
              end if
            end if
c	    check if prog is using log or linear variables
            if(indvar.eq.1) then
c	      log
              parm(nntemp)=scalelog*parm(nntemp)
             else
c	      linear in N
              parm(nntemp)=scalefac*10.0d0**parm(nntemp)
            end if
          end if
        end if
c	velocity dispersion check
 3041   continue
        if(parm(nn+nppbval).le.0.0d0) then
          if(ion(nlines).eq.'__') then
*	    special zero level adjustment
            parm(nn+nppbval)=100.0d0
            if(ipind(nn+nppbval).eq.'  ') then
              ipind(nn+nppbval)='SZ'
            end if
           else if (ion(nlines).ne.'<>'.and.
     :                 ion(nlines).ne.'>>') then
            parm(nn+nppbval)=30.0d0
            write(6,*) ' Doppler parameter b=30 km/s assumed'
          end if
        end if
c
        if(nn.gt.maxnpa)write(6,*)' Too many lines in this feature'
*	non-linear wavelength version:
        zlo=(vp_wval(0.0d0,nchunk))/rstwav(nlines)-1.0d0
        zhi=(vp_wval(dble(ndpts(nchunk)),nchunk))/
     :                 rstwav(nlines)-1.0d0
        if(parm(nn+nppcol).ne.0.0d0.or.ion(nlines).eq.'__') then 
*	  column density nonzero, so find a line               ! 15.3.91
          if(parm(nn+nppzed).lt.zlo.or.parm(nn+nppzed).gt.zhi) then
c	    cursor returns the vac wavelength of the line centre
*	    .. stick the cursor in the middle somewhere:
*	    second if condition for when cursor used for N,b 23.10.96
            if(obswav.gt.0.0d0.and.chcv(1).eq.'gcur') then
              vacwav=obswav*wcor(obswav)
             else
              vacwav=(vp_wval(dble(ndpts(nchunk))*0.5d0,nchunk))
*	      moved from below the 'end if' 23.10.96
              call vp_wvacurs(vacwav)
            end if
            z(nlines)=(vacwav/rstwav(nlines))-1.0d0
            write(6,*)' Estimated vac wavelength & z are',vacwav,
     1            z(nlines)
            parm(nn+nppzed)=z(nlines)
           else
            z(nlines)=parm(nn+nppzed)
            vacwav=(z(nlines)+1.0d0)*rstwav(nlines)
          end if
        end if
*
        if(ion(nlines).ne.'<>'.and.ion(nlines).ne.'__'.and.
     :         ion(nlines).ne.'>>') then
          if(indvar.eq.1) then
            ptempt=parm(nn+nppcol)/scalelog
           else
            ptempt=parm(nn+nppcol)/scalefac
          end if
         else
          ptempt=parm(nn+nppcol)
        end if
        write(6,*) ptempt,parm(nn+nppbval),parm(nn+nppzed)
        if(parm(nn+nppcol).ne.0.0d0) nn=nn+3
        lb=nlines
        call tikset(ion(lb),level(lb),z(lb),lb)
*	update nlines ready for next read
        nlines=nlines+1
        goto 989
      end if
      return
      end
      SUBROUTINE vp_close14
*     current data file and order number
      integer idrn,icrn
      character*64 filnm
      common/vpc_file14/filnm,idrn,icrn
      filnm='  '
      idrn=1
      close(unit=14)
      return
      end
