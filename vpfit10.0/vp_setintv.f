      subroutine vp_setintv(nopt,lguess,icontflg,indcf,nfile,
     :             dhccont,ndp)
*    
*     interactive setup for vpfit, lifted out of the main routine and
*     adapted for new style files. [7.6.06]
*
      implicit none
      include 'vp_sizes.f'
*
*     subroutine parameters:
      integer nopt      ! option number
      logical lguess    ! guess parameter flag
      integer icontflg  
      integer indcf     
      integer nfile     ! number of files
      double precision dhccont(*) ! input continuum
      integer ndp(maxnfi)
*
*     local variables:
      integer ib,ichunk,ie,j,jq,kav
      integer nnzzz
      integer klow,khiw
      character*2 cans
      double precision dswk(maxfis),contv(maxfis)
      double precision sigmah,dlamh
      double precision trstw,vacend,vacwav,wvmean,xlamx
      double precision rstwav(maxats),z(maxats)
      double precision contav
*     
      integer ich,koffset,ki,lchstrt,lchend
*
*     functions
      double precision vp_wval,wval
*
*     common block variables:
*
*     parameter placement variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     parameter list
      integer nlines,nn
      character*2 ion(maxnio)
      character*4 level(maxnio)
      double precision parm(maxnpa)
      common/vpc_parry/parm,ion,level,nlines,nn
*     current 1-D spectrum data, err, fluctuations, continuum -length ngp
      integer ngp
      double precision dhdata(maxfis),dherr(maxfis)
      double precision dhrms(maxfis),dhcont(maxfis)
      common/vpc_su1d/dhdata,dherr,dhrms,dhcont,ngp
*     current data file and order number
      integer idrn,icrn
      character*64 filnm
      common/vpc_file14/filnm,idrn,icrn
*     chunk extension
      integer nrextend
      common/vpc_chunkext/nrextend
*     New 2D dataset variables 
      integer numchunk,nchchtot ! # of chunks,total # data points
      integer kchstrt(maxnch),kchend(maxnch),nchlen(maxnch)
      common/vpc_chunkvar/kchstrt,kchend,nchlen,numchunk,nchchtot
      double precision dach(maxchs,maxnch),derch(maxchs,maxnch)
      double precision drmch(maxchs,maxnch),dcch(maxchs,maxnch)
      common/vpc_datch/dach,derch,drmch,dcch
*     chunk wavelengths
      integer nwvch(maxnch)
      double precision wavch(maxchs,maxnch),wavchlo(maxchs,maxnch)
      common/vpc_wavch/wavch,wavchlo,nwvch
*     wavelength coefficients or array
      integer ngd,npexsm
      integer numpts(maxnch)
      double precision wcoeff(maxnch)
      common/vpc_jkw3/wcoeff,ngd,npexsm,numpts
*     wavelength coefficients/array etc.
      integer nwco
      double precision wcf1(maxwco)
      common/vpc_wavl/wcf1,nwco
*     various wavelength-related things
      character*8 wcftype
      character*4 vacind
      double precision helcfac
      integer*4 noffs
      common/c8_wcftype/wcftype,helcfac,noffs,vacind
*
      double precision sepvd(maxnch)
      common/vpc_sepvd/sepvd
*     fit to resolution, using sigmas
      integer nswres(maxnch)
      double precision swres(maxpoc,maxnch)
      common/vpc_sigwres/swres,nswres
*     chunk variables:
      integer ndpts(maxnch),idrun(maxnch),icrun(maxnch)
      integer indfil(maxnch)
      character*64 filename(maxnch)
      double precision wvstrt(maxnch),wvend(maxnch)
      integer nchunk
      common/vpc_chunk/wvstrt,wvend,ndpts,filename,
     :      idrun,icrun,indfil,nchunk
*     chunk channel limits in data
      integer ichstrt(maxnch),ichend(maxnch)
      common/jkw4/ichstrt,ichend
*     general wavelength coeffts
*     wavelength coeffts for each set. Note that linear store is
*     (1,1)(1,2)(1,3)..(1,6)(2,1)(2,2)... so this ordering is more 
*     efficient for accessing the array.
      double precision wcfd(maxwco,maxnch)
      common/vpc_wcfd/wcfd
*     wavelength parameter sets
      character*8 wcfty2(maxnch)
      character*4 vacin2(maxnch)
      double precision helcf2(maxnch)
      integer nwcf2(maxnch),noffs2(maxnch)
      common/vpc_wcpars2/wcfty2,helcf2,nwcf2,noffs2,vacin2
*     plot variables
*     nst: start channel, ncn: end channel, lc1, lc2 start, end for scale
      integer nst,ncn,lc1,lc2
      real fampg
      common/ppam/nst,ncn,lc1,lc2,fampg
*
      klow=-999999
      khiw=-999999
      sigmah=0.0d0
      dlamh=0.0d0
*
      nchchtot=0
      nlines=1
      nchunk=0
      nn=0
 1067 if(icontflg.eq.0) then
*     read in the spectral data
        call vp_spdatin(nfile,nchunk+1)
      end if
*     put tick marks over lines if new file:
      if(nlines.gt.1) then
        do ib = 1, nlines
*         rfc 28.10.04: No set to 3 -> noppsys
          nnzzz=(ib-1)*noppsys+2
          call tikset(ion(ib),level(ib),parm(nnzzz),ib)
        end do
      end if
*     copy file coefficients to storage areas:
*
 333  nchunk=nchunk+1
 332  filename(nchunk)=filnm
      idrun(nchunk)=idrn
      icrun(nchunk)=icrn
      ndpts(nchunk)=ngp
      ndp(nfile)=ngp
*     non-linear wavelength coeffts
      do ie=1,maxwco
        wcfd(ie,nchunk)=wcf1(ie)
      end do
      vacin2(nchunk)=vacind
      wcfty2(nchunk)=wcftype
      nwcf2(nchunk)=nwco
      noffs2(nchunk)=noffs
      helcf2(nchunk)=helcfac
*     linear coeffts retained temporarily %%%%
      sepvd(nchunk)=wcf1(2)
      wcoeff(nchunk)=wcf1(1)
*	                                      %%%%
      if(nchunk.gt.1) then
        lguess=.false.
*       rest to plot full range
        nst=1
        lc1=1
        ncn=ngp
        lc2=ngp
*        call vp_linlim(ndpts(nchunk),nchunk,lguess)
*       add the lines in to the displayed continuum
        do jq=1,ngp
          contv(jq)=dhcont(jq)
        end do
        if(swres(2,nchunk).le.0.0d0.and.
     :          swres(1,nchunk).le.0.0d0) then
          if(khiw.eq.-999999) then
*           resolution used here only for display purposes,
*           so can be fairly arbitrary about it.
            xlamx=vp_wval(dble(ngp/2),nchunk)
           else   
            vacwav=vp_wval(dble(klow),nchunk)
            vacend=vp_wval(dble(khiw),nchunk)
            xlamx=0.5d0*(vacwav+vacend)
          end if
          call vp_dlset(nchunk,xlamx,sigmah,dlamh)
        end if
        call vp_dlinin(dswk,contv,ngp,nchunk,
     :         dhccont)
       else
*       simply copy continuum (for first chunk display)
        do jq=1,ngp
          contv(jq)=dhcont(jq)
        end do
      end if
*     rfc 22.12.97: with nchunk now in common
*     rfc 07.01.02: dskwk workspace array removed from arg list
      call vp_rwlims(dhdata,dherr,dhrms,contv,ngp,klow,khiw,
     :                wvmean)
      if(klow.le.0.or.khiw.le.0.or.klow.eq.khiw) then
*       region not used
        nchunk=nchunk-1
        nfile=nfile-1
*       always want more if have not got anything yet, so:
        if(nfile.le.0) goto 333
        goto 898
      end if
*
*     gets to here if region used
      write(6,*)' Channels ', klow,khiw
      ichstrt(nchunk)=klow
      ichend(nchunk)=khiw
      kav=(klow+khiw)/2
      contav=dhcont(kav)
*     rfc 22.6.95: rationalized to vacuum only, 
*     and wlamd replaced by vp_wval
      vacwav=vp_wval(dfloat(klow),nchunk)
      vacend=vp_wval(dfloat(khiw),nchunk)
*     xlamx is wavelength used to determine resolution via fit
      xlamx=0.5d0*(vacwav+vacend)
      wvstrt(nchunk)=vacwav
      write(6,*)' Vacuum wavelength for start of chunk is ',
     :    vacwav
*
*     One value should have been set, but just in case ...
      if(swres(1,nchunk).le.0.0d0.and.swres(2,nchunk).le.0.0d0)then
*       set spectral resolution
        call vp_dlset(nchunk,xlamx,sigmah,dlamh)
      end if
*     read in the line data
      if(nopt.eq.1) then
        call vp_abfind(dhdata,dherr,contv,ngp,nchunk,klow,khiw,
     :       ion,level,parm,nn,nlines)
       else
        call vp_rdlines(nopt,nn,nlines,ion,level,trstw,
     :        parm,rstwav,z,contav)
      end if
*
 898  indfil(nchunk)=nfile
      ich=nchunk
*     copy relevant parts to new chunk variables
*     extend these by nrextend for chunk copy limits (rfc: 24.08.05)
      lchstrt=max(ichstrt(ich)-nrextend,1)
      lchend=min(ichend(ich)+nrextend,ngp)
*     length of chunk
      nchlen(ich)=lchend-lchstrt+1
*     nchchtot is total number of chunk datapoints in all chunks
      nchchtot=nchchtot+ichend(ich)-ichstrt(ich)+1
      koffset=lchstrt-1
*     set start and end values for fitting region within the
*     chunk array
      kchstrt(ich)=ichstrt(ich)-koffset
      kchend(ich)=ichend(ich)-koffset
*     copy the data arrays 
      do ki=1,nchlen(ich)
*       data
        dach(ki,ich)=dhdata(ki+koffset)
*       error (held as square)
        derch(ki,ich)=dherr(ki+koffset)
*       fluctuations array (held as square)
        drmch(ki,ich)=dhrms(ki+koffset)
*       continuum
        dcch(ki,ich)=dhcont(ki+koffset)
*       set the wavelengths on a pixel-by-pixel basis, to cover
*       all possibilities. Use wval function, since that applies
*       to the last dataset read in and that is the one being used.
        wavch(ki,ich)=wval(dble(ki+koffset))
*       and, in case channels are subdivided, set the channel edges
*       wavchlo(ki,ich) is lower edge of ki'th channel 
        wavchlo(ki,ich)=wval(dble(ki+koffset)-0.5d0)
      end do
      wavchlo(nchlen(ich)+1,ich)=
     :     wval(dble(nchlen(ich)+koffset)+0.5d0)
*     set wavelength array length (could have fit coefficients,
*     so fewer values, but not as set up here).
      nwvch(ich)=nchlen(ich)
      write(6,*)' Include other data? y, n (def),'
      write(6,*) '  (or sf for more from the same file)'
      write(6,'(''> '',$)')
      read(5,'(a)') cans
      if(cans(1:1).eq.'s'.or.cans(1:1).eq.'S') then
*       copy various things which are usually set at readin
*       resolution
        ichunk=nchunk
        nchunk=nchunk+1
        nswres(nchunk)=nswres(ichunk)
        do j=1,nswres(nchunk)
          swres(j,nchunk)=swres(j,ichunk)
        end do
        goto 332
      end if
      if(cans(1:1).eq.'c'.or.cans(1:1).eq.'C'.or.
     :       cans(1:1).eq.'y'.or.cans(1:1).eq.'Y') then
        call vp_close14
*       ind=4   ! no other reference to this in vpfit routine
        nfile=nfile+1
        indcf=1
        icontflg=0
        idrn=0
        icrn=0
        goto 1067
       else
        indcf=0
      endif
      numchunk=nchunk
*     
      return
      end
