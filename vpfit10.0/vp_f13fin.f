      subroutine vp_f13fin(elm,lion,col,ipc,zee,ipz,bv,ipb,
     1                     vtb,teff,lnk,chlnk,ierr,ifst)
*     free format unscrambling of f13 file line for ion, z, b, N
*     data line in inchstr dvstr, ivstr, cvstr, nvstr & dvstr (common)
*
      implicit none
*
*     Input parameters:
*     format indicator (26 for fort.26)
      integer ifst
*     
*     parameters returned are:
*     tied indicators for col,z,b
      character*2 ipc,ipz,ipb
*     element
      character*2 elm
*     ionization level
      character*4 lion
*     col density, redshift, b-value, turbulent velocity, temperature 
      double precision col,zee,bv
      double precision vtb,teff
*     chunk number for which the line applies (zero if all)
      integer lnk
*     error flag
      integer ierr
*     character for link variables, for extra constraints this ion
      character*(*) chlnk
*
*
*     LOCAL variables
      character*2 cpc(14)
      character*2 ip4,ip5
      integer i,item,ix
      integer j,jlast,jx,lpoint,lvstr
      integer nadj,next,nvx,nstyle
      double precision xval(14)
      double precision x4val,x5val
      double precision x4err,x5err,vtbh,teffh
*     local sepspace
      character*24 cvxx(1)
      integer nvxx,ivxx(1)
      double precision dvxx(1)
*     
*     FUNCTIONS
      logical lcase
*
*     COMMON variables
      character*132 inchstr
      character*60 cvstr(24)
      real rvstr(24)
      integer ivstr(24)
      integer nvstr
      common/vpc_sepspace/inchstr,rvstr,ivstr,cvstr,nvstr
      double precision dvstr(24)
      common/vpc_dsepspace/dvstr

*     separation is already done, so dvstr, ivstr & cvstr set
*     
*     parameter variables
      integer noppsys,nppcol,nppbval,nppzed
      common/vpc_noppsys/noppsys,nppcol,nppbval,nppzed
*     error estimates held in common, in case they are wanted
      double precision colsig,zeesig,bvsig
      common/vpc_insigest/colsig,zeesig,bvsig
*     preset limits to be applied if necessary
      double precision bvalmin,bvalmax,bltdrop,bgtdrop,colltdrop,
     :                     clogltdrop,cvaldrop,clvaldrop,cvalmax,
     :                     clvalmax,bvalminh,bvalmaxh,clvalmin,
     :                     cvalmin
      common/vpc_bvallims/bvalmin,bvalmax,bltdrop,bgtdrop,colltdrop,
     :                     clogltdrop,cvaldrop,clvaldrop,cvalmax,
     :                     clvalmax,bvalminh,bvalmaxh,clvalmin,
     :                     cvalmin
*     max number of add/remove lines iterations, bval adjust preprof.
      integer maxadrit
      logical lbadj
      common/vpc_maxits/maxadrit,lbadj
*     character collating sequence ranges
      integer nchlims
      common/vpc_charlims/nchlims(6)
*
*
*     error condition:
      ierr=0
*     array length
      lvstr=len(cvstr(1))
*
*     deal with first character string
      if(cvstr(1)(2:2).eq.' ') then
*       concatenate first two strings to get element/ionization
        cvstr(1)=cvstr(1)(1:2)//cvstr(2)(1:58)
*       copy the rest down
        if(nvstr.ge.3) then
          do j=2,nvstr-1
            cvstr(j)=cvstr(j+1)
            dvstr(j)=dvstr(j+1)
            ivstr(j)=ivstr(j+1)
          end do
          cvstr(nvstr)=' '
          dvstr(nvstr)=0.0d0
          ivstr(nvstr)=0
        end if
        nvstr=nvstr-1
      end if
*
*     expand the element descriptor if necessary
*     nchlims used, with HD, CO exceptions allowed for
      if(ichar(cvstr(1)(2:2)).ge.nchlims(3).and.
     :     ichar(cvstr(1)(2:2)).le.nchlims(4).and.
     :     cvstr(1)(1:2).ne.'HD'.and.cvstr(1)(1:2).ne.'CO') then
        cvstr(1)=cvstr(1)(1:1)//' '//cvstr(1)(2:lvstr)
      end if
*
*     check if the first character string contains a decimal point,
*     in which case it has to be separated into ion/number
      lpoint=0
      do j=3,lvstr
        if(cvstr(1)(j:j).eq.'.') then
          lpoint=j
          goto 691
        end if
      end do
*
*     if decimal point present in extended field, look for the first number
 691  ix=6
      if(lpoint.gt.5) then
        do i=3,lpoint-1
          if(cvstr(1)(i:i).ne.' ') then
*           nvx defined and used later, so here as a dummy variable
            read(cvstr(1)(i:i),'(i1)',err=9698) nvx
            ix=i
            goto 9699
 9698       continue
          end if
        end do
*       first number position determined, make sure it is > 5
 9699   ix=max0(6,ix)
*       move the string
        nvstr=nvstr+1
        do j=nvstr,2,-1
          cvstr(j)=cvstr(j-1)
          dvstr(j)=dvstr(j-1)
          ivstr(j)=ivstr(j-1)
        end do
*       first string to contain element/ion
        cvstr(1)=cvstr(1)(1:ix-1)
        cvstr(2)=cvstr(2)(ix:lvstr)
      end if
*
*     unpack the required values
*     element:
      elm=cvstr(1)(1:2)
*     ionization
      lion=cvstr(1)(3:ix-1)
*
*     extract parameters from the remaining character sets
      nvx=min0(nvstr,14)
      do i=2,nvx
*
*       if output overflow, then replace with some random number
        if(cvstr(i)(1:1).eq.'*') then
          cvstr(i)='-1.000'
        end if
*       seek last non-space character 
        jx=lvstr
        do while (cvstr(i)(jx:jx).eq.' '.and.jx.gt.1)
          jx=jx-1
        end do
*       special case for fort.26 upper limit style:
*       remove '<' at start of this character
        if(i.eq.6) then
          if(cvstr(i)(1:1).eq.'<') then
            cvstr(i)=cvstr(i)(2:lvstr)//' '
          end if
        end if
*       special case for nan - set to zero in absence of anything sensible
        if(cvstr(i)(1:3).eq.'nan') then
          cvstr(i)='0.0'
        end if
*
*       split into parameter and flag (up to two characters)
        if(jx.ge.2) then
*         ipind may be two characters
          cpc(i)=cvstr(i)(jx-1:jx)
          jlast=jx-2
          if(cpc(i)(1:1).eq.'.'.or.cpc(i)(1:1).eq.'0'.or.
     :         cpc(i)(1:1).eq.'1'.or.cpc(i)(1:1).eq.'2'.or.
     :         cpc(i)(1:1).eq.'3'.or.cpc(i)(1:1).eq.'4'.or.
     :         cpc(i)(1:1).eq.'5'.or.cpc(i)(1:1).eq.'6'.or.
     :         cpc(i)(1:1).eq.'7'.or.cpc(i)(1:1).eq.'8'.or.
     :         cpc(i)(1:1).eq.'9') then
*	    move up a character
            cpc(i)=cpc(i)(2:2)//' '
            jlast=jlast+1
*	    check if it is a number
            if(cpc(i)(1:1).eq.'.'.or.cpc(i)(1:1).eq.'0'.or.
     :           cpc(i)(1:1).eq.'1'.or.cpc(i)(1:1).eq.'2'.or.
     :           cpc(i)(1:1).eq.'3'.or.cpc(i)(1:1).eq.'4'.or.
     :           cpc(i)(1:1).eq.'5'.or.cpc(i)(1:1).eq.'6'.or.
     :           cpc(i)(1:1).eq.'7'.or.cpc(i)(1:1).eq.'8'.or.
     :           cpc(i)(1:1).eq.'9') then
              cpc(i)='  '
              jlast=jx
            end if
          end if
*	  rfc 25.08.96: free input format OK
          read(cvstr(i)(1:jlast),*,err=999) xval(i)
         else
          if(jx.eq.1) then
*	    either a single character or a number
            cpc(i)=cvstr(i)(1:1)//' '
            if(cpc(i)(1:1).eq.'.'.or.cpc(i)(1:1).eq.'0'.or.
     :           cpc(i)(1:1).eq.'1'.or.cpc(i)(1:1).eq.'2'.or.
     :           cpc(i)(1:1).eq.'3'.or.cpc(i)(1:1).eq.'4'.or.
     :           cpc(i)(1:1).eq.'5'.or.cpc(i)(1:1).eq.'6'.or.
     :           cpc(i)(1:1).eq.'7'.or.cpc(i)(1:1).eq.'8'.or.
     :           cpc(i)(1:1).eq.'9') then
              read(cvstr(i),'(i1)',err=999) item
              xval(i)=dble(item)
              cpc(i)='  '
             else
              xval(i)=0.0d0
            end if
           else
*	    this bit should never be used, unless line shortened:
            xval(i)=0.0d0
            cpc(i)='  '
          end if
        end if
      end do
*
*     conditions for fort.26:
*     real(ivstr(7)).ne.dvstr(7)
*     fort.13:
*     cvstr(8)(1:1).eq.'!'.or.nvstr.le.5
*
*     allow fort.13 line in fort.26 file if short line
*     or 5,6,7 are 0.00   0.00E+00  0
*     or fixed format from screen with leading blanks, E, sign, and 
*     a last digit in the right place
*    
*     added bit to force format if necessary. If a line contains '['
*     must be 26, if 'bturb=' or 'T=' style 13.
      nstyle=0
      teffh=0.0d0
      vtbh=0.0d0
      do i=1,nvstr
        if(cvstr(i)(1:1).eq.'[') nstyle=26
        if(cvstr(i)(1:6).eq.'bturb='.or.cvstr(i)(1:2).eq.'T=') then
          nstyle=13
*         get the value
          if(cvstr(i)(1:2).eq.'T=') then
            cvstr(i)(1:2)='  '
            call dsepvar(cvstr(i),1,dvxx,ivxx,cvxx,nvxx)
            teffh=dvxx(1)
            vtbh=0.0d0
           else
            cvstr(i)(1:6)='      '
            call dsepvar(cvstr(i),1,dvxx,ivxx,cvxx,nvxx)
            teffh=0.0d0
            vtbh=dvxx(1)
          end if
        end if
      end do
*      
      if(ifst.ne.26.or.nvstr.le.5.or.nstyle.eq.13.or.
     :    cvstr(6)(3:8).eq.'00E+00'.or.
     :    (inchstr(1:3).eq.'   '.and.inchstr(62:62).eq.'E'.and.
     :     (inchstr(63:63).eq.'+'.or.inchstr(63:63).eq.'-').and.
     :      inchstr(68:68).ne.' '.and.inchstr(69:69).eq.' ')) then
*       col=2, redshift=3, b=4
        col=xval(2)
        ipc=cpc(2)
        zee=xval(3)
        ipz=cpc(3)
        bv=xval(4)
        ipb=cpc(4)
*
*       further values are direct copies unless nstyle=13
        if(nstyle.eq.13) then
          vtb=vtbh
          teff=teffh
          lnk=ivstr(5)
*         test for character, OK still because blanked off
          if(lnk.eq.0.and.cvstr(5)(1:1).ne.'0'.and.
     :      cvstr(5)(1:1).ne.' ') then
            chlnk=cvstr(5)(1:1)
           else
            chlnk='0'
          end if
         else
          vtb=dvstr(5)
          teff=dvstr(6)
          lnk=ivstr(7)
*         test for character
          if(lnk.eq.0.and.cvstr(7)(1:1).ne.'0') then
            chlnk=cvstr(7)(1:1)
           else
            chlnk='0'
          end if
        end if
        colsig=0.0d0
        zeesig=0.0d0
        bvsig=0.0d0
       else
*	col=6, z=2, b=4
        col=xval(6)
        colsig=xval(7)
        ipc=cpc(6)
        zee=xval(2)
        zeesig=xval(3)
        ipz=cpc(2)
        bv=xval(4)
        bvsig=xval(5)
        ipb=cpc(4)
        next=8
*       extended parameter format
        x4val=0.0d0
        ip4='  '
        x4err=0.0d0
        x5val=0.0d0
        ip5='  '
        x5err=0.0d0
        if(noppsys.ge.4) then
          x4val=xval(8)
          ip4=cpc(8)
          x4err=xval(9)
          next=10
          if(noppsys.ge.5) then
            x5val=xval(10)
            ip5=cpc(10)
            x5err=xval(11)
            next=12
          end if
        end if
*             
        if(nvstr.ge.next) then
          lnk=ivstr(next)
*	  test for character
          if(lnk.eq.0.and.cvstr(next)(1:1).ne.'0') then
            chlnk=cvstr(next)(1:1)
           else
            chlnk='0'
          end if
         else
          lnk=0
          chlnk='0'
        end if
        if(lcase(ipb(1:1))) then
*	  tied b-value, so need vturb, temperature
          vtb=dvstr(9)
          teff=dvstr(10)
         else
          vtb=0.0d0
          teff=0.0d0
        end if
      end if
*     Check variable ranges acceptable, but not for
*     special cases:
      if(elm.ne.'<>'.and.elm.ne.'__'.and.elm.ne.'>>'.and.
     :        lbadj) then
        nadj=0
        if(elm.eq.'H '.or.elm.eq.'??') then
          if(bv.lt.bvalminh) then
            bv=bvalminh
            nadj=1
          end if
          if(bv.gt.bvalmaxh) then
            bv=bvalmaxh
            nadj=1
          end if
         else
          if(bv.lt.bvalmin) then
            bv=bvalmin
            nadj=1
          end if
          if(bv.gt.bvalmax) then
            bv=bvalmax
            nadj=1
          end if
        end if
        if(nadj.eq.1) then
          write(6,*) 'BVAL ADJUSTED TO BE WITHIN RANGE: '
          write(6,*) elm,lion,zee,bv,col
        end if
      end if
*
 1000 return
*
*     disaster flag
 999  ierr=1
*     may be <EOF>, so suppress annoying error message
*     write(6,*) ' Data read error'
      goto 1000
      end

