      subroutine svbksbd(u,w,v,m,n,mp,np,b,x)
      implicit none
      include 'vp_sizes.f'
      integer nmax
      parameter (nmax=maxmas)
      integer m,n,mp,np
      double precision u(mp,np),w(np),v(np,np)
      double precision b(mp),x(np),tmp(nmax)
      double precision s
*
      integer i,j,jj
*
      do j=1,n
        s=0.0d0
        if(w(j).ne.0.0d0)then
          do i=1,m
            s=s+u(i,j)*b(i)
          end do
          s=s/w(j)
        end if
        tmp(j)=s
      end do
      do j=1,n
        s=0.0d0
        do jj=1,n
          s=s+v(j,jj)*tmp(jj)
        end do
        x(j)=s
      end do
      return
      end
