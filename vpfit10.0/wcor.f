      double precision function wcor(x)
      implicit none
      double precision x
      double precision t
*
      double precision avfac
      common/vpc_airvac/avfac
c     air to vacuum multiplier
      if(avfac.le.1.000001d0) then
        wcor=1.0d0
       else
        t=1.0d4/x
        t=t*t
*       Edlen 1966:
        t=1.0d-8*(8342.13d0
     :        +2406030.0d0/(130.0d0-t)+15997.0d0/(38.9d0-t))
*       instead of 6.4328e-5+2.94981e-2/(146.-t)+2.554e-4/(41.-t)
        wcor=1.0d0+t
      end if
      return
      end
