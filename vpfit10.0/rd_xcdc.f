      subroutine rd_xcdc(da,de,ca,re,ds,nct)
*
*     cross-correlate 1.0-da and 1.0-ca on a pixel scale, with
*     (biassed) normalization, no average subtraction. Just to test
*     a method for system searching in normalized data where a model
*     is put into the continuum array ca.
*
      implicit none
      include 'vp_sizes.f'
*     subroutine arguments
      integer nct
      double precision da(maxfis),de(maxfis),ca(maxfis)
      double precision ds(maxfis),re(maxfis)
*
*     local variables
      integer i,j,k
      integer joff,nms
      integer nps,nplo,nphi,nxtot
      double precision sum
*
*     common
*     character separation variables
      character*132 inchstr
      character*60 cv(24)
      real rvss(24)
      integer iv(24)
      integer nv
      common/rd_chwork/inchstr,rvss,iv,cv,nv
      double precision dv(24)
      common/vpc_dsepspace/dv
*     wavelength stuff
      integer nwco
      double precision wcf1(maxwco)
      common/vpc_wavl/wcf1,nwco
      character*8 wcftype
      character*4 vacind
      double precision helcfac
      integer*4 noffs
      common/c8_wcftype/wcftype,helcfac,noffs,vacind
*
*     print wavelength information
      write(6,*) wcf1(1),wcf1(2),' ',wcftype
*     set up ranges
      write(6,'(a)') 'Pixel shift (half-range), pxmin, pxmax'
      read(5,'(a)') inchstr
      call dsepvar(inchstr,3,dv,iv,cv,nv)
      nps=iv(1)
      nplo=iv(2)
      nphi=iv(3)
*     make sure that have enough data points beyond the ends to
*     cover the shift
      i=nplo-1
      j=nct-nphi
      k=min(i,j)
      if(k.lt.nps) then
        nps=k-1
        write(6,'(a,i6,a)') 'Shift range set to',nps,' pixels'
      end if
      nxtot=2*nps+1
      write(6,*) 'Range is',nxtot
*
*     copy variables
      do i=1,nct
        ds(i)=1.0d0-da(i)
        re(i)=1.0d0-ca(i)
      end do
*
      do k=1,nxtot
        sum=0.0d0
        nms=0
        joff=k-nps-1
        do j=nplo,nphi
          if(de(j).gt.0.0d0) then
            sum=sum+ds(j)*re(j-joff)
            nms=nms+1
          end if
        end do
        if(nms.gt.0) then
          da(k)=sum/dble(nms)
         else
          da(k)=0.0d0
        end if
        ca(k)=0.0d0
        de(k)=1.0d-20
      end do
      nct=nxtot
*     sort out x-axis
      if(wcftype(1:3).eq.'log') then
        wcf1(2)=2.99792458d5*(10.0d0**wcf1(2)-1.0d0)
        wcf1(1)=-wcf1(2)*dble(nps+1)
        nwco=2
        wcftype='velocity'
       else
        wcf1(2)=1.0
        wcf1(1)=-dble(nps+1)
        nwco=2
        wcftype='pixel'
      end if
      write(6,*) 'Data and wavelengths replaced'
      return
      end
