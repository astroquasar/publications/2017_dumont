      subroutine rd_multiset(ind,nostrip)
*     take a file list, and set up a multiprocess file
      implicit none
      integer ind,nostrip
*     template commands:23
*     file list:22
*     temporary command file:24
      integer j,jpos,jx,lxd,lend,lenx
      integer n23
      character*132 chtem
      character*80 ch23(80)
      integer len23(80)
*     Functions
      integer lastchpos
*     various common things:
      character*132 inchstr
      character*60 cv(24)
      real rvss(24)
      integer iv(24)
      integer nv
      common/rd_chwork/inchstr,rvss,iv,cv,nv
      double precision dv(24)
      common/vpc_dsepspace/dv
*
*     status variable
      ind=0
*     read in the command list
      n23=1
 2    read(23,'(a)',end=21) ch23(n23)
      len23(n23)=lastchpos(ch23(n23))
*     search for replace pattern ($$$$)
      if(len23(n23).ge.4) then
        do j=1,len23(n23)-3
          if(ch23(n23)(j:j+3).eq.'$$$$') then
            len23(n23)=-j
            goto 201
          end if
        end do
      end if
 201  n23=n23+1
      if(n23.lt.80) then
        goto 2
       else
        ind=1
        write(6,*) 'Too many command lines in template'
        write(6,*) ' should be < 80'
        goto 902
      end if
 21   n23=n23-1
 1    read(22,'(a)',end=901) inchstr
      lend=lastchpos(inchstr)
      if(lend.le.0) goto 901
      if(nostrip.eq.1) then
*       don't strip the last part of the string
        j=0
       else
        j=lend
        do while (j.ge.1)
          if(inchstr(j:j).eq.'.') goto 222
          j=j-1
        end do
      end if
 222  continue
      if(j.gt.0) then
        inchstr(j:j)=' '
        call dsepvar(inchstr,2,dv,iv,cv,nv)
*       cv(1) is root, cv(2) is extension
*       restore inchstr as input filename
        inchstr(j:j)='.'
        jpos=j-1
       else
        cv(1)=inchstr(1:60)
        jpos=lend
      end if
      lxd=0
      do jx=1,n23
        if(len23(jx).ge.0) then
          write(24,'(a)') ch23(jx)(1:len23(jx))
         else
          if(lxd.eq.0) then
*           replace $$$$ by full filename
            j=-len23(jx)
            if(j.gt.1) then
              chtem=ch23(jx)(1:j-1)//inchstr(1:lend)//
     :             ch23(jx)(j+4:80)
             else
              chtem=inchstr(1:lend)//ch23(jx)(j+4:80)
            end if
            lxd=lxd+1
           else
*           use root
            j=-len23(jx)
            if(j.gt.1) then
              chtem=ch23(jx)(1:j-1)//cv(1)(1:jpos)//
     :                 ch23(jx)(j+4:80)
             else
              chtem=cv(1)(1:jpos)//ch23(jx)(j+4:80)
            end if
          end if
          lenx=lastchpos(chtem)
          write(24,'(a)') chtem(1:lenx)
        end if
      end do
      goto 1
 901  write(6,*) 'Multifile setup OK'
      write(24,'(a)') ' '
 902  return
      end
