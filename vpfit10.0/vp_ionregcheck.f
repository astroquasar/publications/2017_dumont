      subroutine vp_ionregcheck(elm,ionz,zed, lionok)
*     checks to see if ion has any lines in the regions set out
*     lionok=.true. if it has, .false. otherwise
*     IN:
*       elm     ch*2   element identifier
*       ionz    ch*4   ionization level (by letter)
*       zed     dbl    redshift
*     OUT:
*       lionok  logc   true if a line falls in some chunk, false otherwise
*
      include 'vp_sizes.f'
      character*2 elm
      character*4 ionz
      double precision zed
      logical lionok
*     local variables:
      double precision dzp1,dtemp
*     chunk variables:
      integer ndpts(maxnch),idrun(maxnch),icrun(maxnch)
      integer indfil(maxnch)
      character*64 filename(maxnch)
      double precision wvstrt(maxnch),wvend(maxnch)
      integer nchunk
      common/vpc_chunk/wvstrt,wvend,ndpts,filename,
     :      idrun,icrun,indfil,nchunk
      character*2 lbz
      character*4 lzz
      double precision alm,fik,asm 
      common/vpc_ewllns/lbz(maxats),lzz(maxats),alm(maxats),
     :                fik(maxats),asm(maxats),m
*     atomic data

*     make sure there is something to do!
      if(nchunk.le.0) return
*     and that the atomic data is in:
      if(m.le.0) call vp_ewred(0)
      lionok=.false.
      dzp1=1.0d0+zed
*     only need one region, so set up loops to bail out as soon as
*     lionok is true.
      j=0
      do while((.not.lionok).and.j.lt.m)
        j=j+1
        if(lbz(j).eq.elm.and.lzz(j).eq.ionz) then
          dtemp=alm(j)*dzp1
          k=0
          do while(k.lt.m.and.(.not.lionok))
            k=k+1
            if(dtemp.ge.wvstrt(k).and.dtemp.le.wvend(k)) then
              lionok=.true.
            end if
          end do
        end if
      end do
      return
      end
          
